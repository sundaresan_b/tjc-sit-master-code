var dwSystem = require('dw/system'),
    Class = require("bc_library/cartridge/scripts/object-handling/libInheritance").Class,
    ServiceRegistry = require('dw/svc').ServiceRegistry;

var AbstractService = Class.extend (

{
	init : function(serviceName : String, serviceURL : String) {
		this.serviceName = serviceName;
		this.serviceURL = serviceURL;
		this.service = ServiceRegistry.get( this.serviceName );
		this.response;
		this.mockCall = false;
		this.pipelineError = false; 
		this.httpResult = null;
		this.result = null;
		this.service.URL += this.serviceURL;
	},

	addParam : function(paramName, paramValue){
		this.service.addParam(paramName, paramValue);
	},

	makeCall : function() {
		if ( this.mockCall ) {
		  this.response = this.service.setMock().call();
		} else if ( this.pipelineError ) {
		  this.response = this.service.setThrowOnError().call();	
		} else {
		  this.response = this.service.call();
		}
	     
	    if ( this.response == null || this.service == null ){
			return PIPELET_ERROR;
	    }
	    
	    this.httpResult = this.response;
		this.svcConfig = this.service;
		if ( this.response.status === "ERROR" || this.response.status === "SERVICE_UNAVAILABLE" ){
			this.result = null;
			return PIPELET_NEXT;	
		}
		
		if (typeof this.processResponse != 'function') { 
	    	throw "The subclass has to implement a processResponse method!"; 
		}
		
	    var resultObject : Object = this.processResponse(this.response); 
		this.result = resultObject;
		
		return PIPELET_NEXT;	
	}
});

module.exports = AbstractService;
