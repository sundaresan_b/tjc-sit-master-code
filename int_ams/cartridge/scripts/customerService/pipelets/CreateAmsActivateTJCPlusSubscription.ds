/**
* Script file for use in the Script pipelet node.
* To define input and output parameters, create entries of the form:
*
* @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
*
* where
*   <paramUsageType> can be either 'input' or 'output'
*   <paramName> can be any valid parameter name
*   <paramDataType> identifies the type of the parameter
*   <paramComment> is an optional comment
*
* For example:
*
* @input Order : dw.order.Order
* @input Customer : dw.customer.Customer
* @output SubscriptionCustomerObj : Object
*
*/
importPackage( dw.web );
importPackage( dw.util );
var Logger = require("dw/system/Logger");

function execute( args : PipelineDictionary ) : Number
{	
	var amsService = webreferences2.amsServices;
	var amsActivateSubscription = new amsService.ActiveSubscriptionRequest();
	
	var customer = args.Customer;
	var customerProfile = args.Customer.profile;
	
	var customerAddresses = customer.addressBook.addresses;
	var tjcplusAddress = args.Order.billingAddress;
	
	for each(var address in customerAddresses){
		if(address.custom.isTJCPlusBillingAddress){
			tjcplusAddress = address;
		}
	}
	
	var tjcplusCard = JSON.parse(customerProfile.custom.selectedcardforTJCPlus);
	
    var currentCard = {
        "id": tjcplusCard.id,
        "lastDigits": tjcplusCard.lastDigits,
        "cardExpiry": {
            "year": tjcplusCard.cardExpiry.year,
            "month": tjcplusCard.cardExpiry.month
        },
        "paymentToken": tjcplusCard.paymentToken,
        "cardType": tjcplusCard.cardType
    };
    
    var cardExpiry = currentCard.cardExpiry.month + '/' + currentCard.cardExpiry.year;
	
	var amscardType = currentCard.cardType;
	var subscriptionPlan = customerProfile.custom.currentPlusMembershipPlan;
	var subscriptionDuration;
	var subscriptionItem;
	
	if(subscriptionPlan == 'MONTHLY'){
		subscriptionItem = dw.system.Site.getCurrent().getCustomPreferenceValue('tjcPlus_monthlyitemCode');
	}else if(subscriptionPlan == 'YEARLY'){
		subscriptionItem = dw.system.Site.getCurrent().getCustomPreferenceValue('tjcPlus_yearlyitemCode');
	}
	
	var calendar = new dw.util.Calendar();
	calendar.setTime(customerProfile.custom.tjcPlusLastPaymentDate);
	
	amsActivateSubscription.addressAlias = 'ID' in tjcplusAddress ? tjcplusAddress.ID : 'TJCPlusBillingAddress';
	amsActivateSubscription.AMSCustomerID = customerProfile.custom.amsCustomerId;
	amsActivateSubscription.billingAddress1 = tjcplusAddress.address1;
	amsActivateSubscription.billingAddress2 = tjcplusAddress.address2;
	amsActivateSubscription.billingAddress3 = '';
	amsActivateSubscription.billingAddress4 = '';
	amsActivateSubscription.billingCity = tjcplusAddress.city;
	amsActivateSubscription.billingCountryCode = tjcplusAddress.countryCode.value;
	amsActivateSubscription.billingPostCode = tjcplusAddress.postalCode;
	amsActivateSubscription.cardType = amscardType;
	amsActivateSubscription.creditCardHolderName = customerProfile.firstName + ' ' + customerProfile.lastName;
	amsActivateSubscription.DWOrderCode = args.Order.orderNo;
	amsActivateSubscription.expiry = cardExpiry;
	amsActivateSubscription.isTJCPlusAutoRenewal = customerProfile.custom.isTJCPlusAutoRenewal;
	amsActivateSubscription.optimalToken = currentCard.paymentToken;
	amsActivateSubscription.TJCPlusDurationInMonths = customerProfile.custom.tjcplus_durationInMonths;
	amsActivateSubscription.TJCPlusPaidAmount = customerProfile.custom.tjcPlusAmountPaid;
	amsActivateSubscription.setTJCPlusPaymentDate(calendar);
	amsActivateSubscription.TJCPlusSubscriptionCode = subscriptionItem;
	amsActivateSubscription.setTJCPlusSubscriptionStartDate(calendar);
	amsActivateSubscription.tokenNumber = currentCard.lastDigits;
	
	args.SubscriptionCustomerObj = amsActivateSubscription;
	
	Logger.error('checking output object'+ amsActivateSubscription);
	
   return PIPELET_NEXT;
}
