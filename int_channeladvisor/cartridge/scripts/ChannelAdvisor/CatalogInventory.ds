/**
* Demandware ChannelAdvisor Catalog Extract File v15.1.0
* Modified to avoid timeout.
* @input Products : dw.util.Iterator
* @input StartOffset : Number
* @output EndOffset : Number
*/

importPackage( dw.system );

importPackage( dw.util );

importPackage( dw.catalog );

importPackage( dw.customer );

importPackage( dw.io );

importPackage( dw.net );

importPackage( dw.object );

importPackage( dw.campaign );

importPackage( dw.web );


/**
 * Iterates through all site products, creates an XML representation of the price and quantity data, and FTP's that data based on the settings in the custom preferences.
 *
 * @param PipelineDictionary pdict
 * @return Number  The next Pipelet to fire
 */

var logger = Logger.getLogger( "CALogging", "CALogging" ); 
 
function execute( pdict : PipelineDictionary ) : Number { 
	
	var caStringWriter : StringWriter = new StringWriter();
	
	var productCount : Number = 0; 
	
	(new dw.io.File(dw.io.File.IMPEX +'/src/channeladvisor/')).mkdirs();
	
	caStringWriter.write('<?xml version="1.0" encoding="iso-8859-1"?>\n');
	
	caStringWriter.write('<Offers xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://ssc.channeladvisor.com/files/cageneric.xsd">\n');
	
	caStringWriter.flush();
	
	if (!writeToFile(caStringWriter.toString(), false))	{
		
		throw("Cannot write file");
		
	}
	
	caStringWriter.close();

	var prodHits : SeekableIterator = pdict.Products;
	
	logger.info("Starting CA Feed");  
	
	while(prodHits.hasNext())
	{
		var productHit = prodHits.next(); 
		
		var product : Product = productHit.getProduct();
		
		//Export everything but masters since they dont have stock 
		if ( product.isOnline() && !product.isProductSet())  {
			productCount++; 

			for(var x : Number = 0; x < productHit.representedProducts.size(); x++) {
				
				var currentRepresentedProduct = productHit.representedProducts[x];
				
				var CurrentOffer : XML = buildProductNode(currentRepresentedProduct); 
				
				var xmlString : String = CurrentOffer.toXMLString();
				
				if (!writeToFile(xmlString, true))	{ 
	
					throw("Cannot write file");
	
				}
				
			}	
			
		}
	}
	//prods.close(); //close the seekable iterator
	
	logger.info("Total products in CA Inventory feed: "+ productCount);
	
	if (!writeToFile('</Offers>\n', true))	{
		
		throw("Cannot write Offers");
		
	}
	
	if(empty(dw.system.Site.getCurrent().preferences.custom.CA_FTP_Hostname)){
		logger.error( "Cannot upload to FTP. The host is empty");
	}
	else {
		var ftp : FTPClient = new FTPClient(); 
	
		ftp.setTimeout(60*1000*30);
		
		if(ftp.connect(dw.system.Site.getCurrent().preferences.custom.CA_FTP_Hostname, 
		dw.system.Site.getCurrent().preferences.custom.CA_FTP_Username, 
		dw.system.Site.getCurrent().preferences.custom.CA_FTP_Password)) {
			
			logger.info('Connected to Channel Advisor FTP');
			
			var uploadDirectory = dw.system.Site.getCurrent().preferences.custom.CA_FTP_Upload_Directory; //'Inventory/Transform'
		
			ftp.cd(uploadDirectory);
			
			//Override the base settings in CA such that only updates are processed using this file, no new inventory is created
			var targetFileName : String = 'Inventory.2020.xml';
	
			var file : File = new dw.io.File(dw.io.File.IMPEX + '/src/channeladvisor/Inventory.xml');
		
			if (!file.exists()) {
				throw("Cannot write file");
				
			} else {
				
				ftp.putBinary(targetFileName, file);
				
			}
			ftp.disconnect();
		} else { 
			logger.error('FTP Connection failed');
		}
		
	}
    return PIPELET_ERROR;
    
}


/**
 * Given a product,  extract the data and create the XML representation of that product.
 *
 * @param Product product  The content to be written to the file
 * @return XML the full product XML representation
 */
function buildProductNode(product : Product) : XML
{
	var Offer : XML = <Offer></Offer>;
	
	Offer.SKU = !empty(product.ID) ? product.ID : '' ; //Model is a required field, so this should never be empty

	var pb : String = '';  

	if(('listPriceDefault' in dw.system.Site.current.preferences.custom) && !empty(dw.system.Site.current.preferences.custom.listPriceDefault)){
		
		pb = dw.system.Site.current.preferences.custom.listPriceDefault;
		
	} else	{
		
	    pb = 'list-prices';
	    
	}
	
	//Offer.RegularPrice = !empty(product.priceModel.getPriceBookPrice(pb).toNumberString()) && product.priceModel.getPriceBookPrice(pb).toNumberString() != 'N/A' ? product.priceModel.getPriceBookPrice(pb).toNumberString() : ''; 
	
	Offer.Price = !empty(product.getPriceModel().getPrice().toNumberString()) && product.getPriceModel().getPrice().toNumberString() != 'N/A' ? product.getPriceModel().getPrice().toNumberString() : '';
	
	Offer.Quantity = product.availabilityModel.inventoryRecord != null ? StringUtils.stringToXml(product.availabilityModel.inventoryRecord.ATS.value) : StringUtils.stringToXml(0);
	if(!empty(product.custom.laAuctionCode)){
		Offer.Quantity = '0';		
	}
	return Offer;

}


/**
 * Writes the content to an xml file (Inventory.xml) in the Impex /src/channeladvisor/ folder.
 *
 * @param String content  The content to be written to the file
 * @param Boolean isAppend  Should the string be appended, or replace existing content
 * @return Boolean returns true if the write succeeded, false if it failed
 */
function writeToFile(content : String, isAppend : Boolean) : Boolean {
	
	try	{		
		
		var file : File = new dw.io.File(dw.io.File.IMPEX + '/src/channeladvisor/Inventory.xml');
		
		if(!file.exists()) {
			
			if(!file.createNewFile()) {
				
				logger.error("File "+file.name+" could not be created!");
				
				return false;
				
			}
			
		} 
		
		var out : FileWriter = new FileWriter(file, isAppend);
		
		out.write(content);
		
		out.flush();
		
		out.close();
		
	} catch(e) {
		
		logger.error("An error occured while exporting channel advisor XML Inventory {0}.", e);
		
		return false;
		
	}	
	
	return true;
	
}