/**
* Copies all files in the source folder to a destination folder. The files will
* have the same name after copying.
*
* @input SrcFolderPath : String
* @input DstFolderPath : String 
*/
importPackage( dw.io );
importPackage( dw.util );
importScript('bc_jobframework:job/JobMonitor.ds');

function execute( args : PipelineDictionary ) : Number
{
	var cvLogger : Object = getJobMonitor() && getJobMonitor().getCurrentTask() ? getJobMonitor().getCurrentTask() : Logger;
	
	var srcFolderPath : String = args.SrcFolderPath;
	var dstFolderPath : String = args.DstFolderPath;	

	var srcFolder : File = new File( srcFolderPath );
	if( !srcFolder.exists() ) {
		return PIPELET_ERROR;
	}
	
	var dstFolder : File = new File( dstFolderPath );
	dstFolder.mkdirs();
	
	var srcFiles : List = srcFolder.listFiles();
	var srcFile : File;
	
	cvLogger.info("Found " + srcFiles.size() + " to copy.");
	for each (srcFile in srcFiles) {

		var dstFile : File = new File( dstFolderPath + File.SEPARATOR + srcFile.name );
	
		var fileLengthReader : FileReader = new FileReader( srcFile );
		var reader : FileReader = new FileReader( srcFile );
		var writer : FileWriter = new FileWriter( dstFile );
		
		cvLogger.info("Copy File " + srcFile.getFullPath() + " to " + dstFile.getFullPath());
	
		try {
			
			// file.length() returns the file size in bytes, which differs to 
			// the no of chars in the file, especially for DE or FR files because of the
			// encoding. This is a fix to count the chars in the file and copy them safely.
			var fileCharsCounter = 0;
			while (fileLengthReader.read() != null) {
				fileCharsCounter++;
			}
			
			var pageSize : Number = 25000;
			var pages : Number = Math.ceil( fileCharsCounter / pageSize );
			for( var page : Number = 0; page < pages; page ++ ) {
				var dummy : String = reader.read( pageSize );
				writer.write( dummy );
			}
		}
		catch(e) {
			cvLogger.error("Error copying file " + srcFile.getFullPath() + " to " + dstFile.getFullPath());
			cvLogger.error("Exception: " + e);
			//return PIPELET_ERROR;
		}
		finally {	
			fileLengthReader.close();
			reader.close();
			writer.close();
		}
	}
	
	return PIPELET_NEXT;
}
