/**
* Gets the list of feeds which should be uploaded to the conigured server.
*
* @input IncludeFeedIds : dw.util.List A list of IDs which should be included to the feed upload (if empty, all feeds will be uploaded).
* @input ExcludeFeedIds : dw.util.List A list of IDs which should be excluded from the feed upload (if empty, no feed will be excluded).
* @output Feeds : dw.util.List - The feeds which should be uploaded
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.object );
importScript( 'bc_jobframework:job/JobMonitor.ds' );

function execute( args : PipelineDictionary ) : Number
{
	var cvLogger : Object = getJobMonitor() && getJobMonitor().getCurrentTask() ? getJobMonitor().getCurrentTask() : Logger;    
    
    var includeFeedIds : ArrayList = args.IncludeFeedIds;
    var excludeFeedIds : ArrayList = args.ExcludeFeedIds;
    
    var feedConfigurations : SeekableIterator = CustomObjectMgr.queryCustomObjects("ProductFeed", "custom.enabled=true", "custom.ID");	
    var resultFeedConfigurations : ArrayList = new ArrayList();
    
    var productFeedCO : CustomObject; 
    for each (productFeedCO in feedConfigurations) {
		
		// Check for includes / excludes for this upload
		var uploadFeed = true;
		if (!empty(includeFeedIds) && !includeFeedIds.contains(productFeedCO.custom['ID'])) {
			// if the include parameter is not empty, we only upload the included feeds
			cvLogger.info("Exclude Product Feed {0}, because it is not in the list of includes.", productFeedCO.custom['ID']);
			uploadFeed = false;
		}			
		if (!empty(excludeFeedIds) && excludeFeedIds.contains(productFeedCO.custom['ID'])) {
			cvLogger.info("Exclude Product Feed {0}, because it is in the list of excludes.", productFeedCO.custom['ID']);
			uploadFeed = false;
		}
		
		if (uploadFeed) {
			cvLogger.info("Add Product Feed {0} to upload", productFeedCO.custom['ID']);
			resultFeedConfigurations.add(productFeedCO);
		}    		
    }
    feedConfigurations.close();	
    
    args.Feeds = resultFeedConfigurations;

    return PIPELET_NEXT;
}
