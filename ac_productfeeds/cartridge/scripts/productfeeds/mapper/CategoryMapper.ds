importPackage( dw.catalog );
importPackage( dw.content );
importPackage( dw.order );
importPackage( dw.value );
importPackage( dw.util );
importPackage( dw.web );
importPackage( dw.system );
importScript('ac_productfeeds:productfeeds/mapper/MapperUtil.ds' );
importScript( 'bc_jobframework:job/JobMonitor.ds' );

function CategoryMapper( feedConfig : Object ) : Function {
	if( 'categoryMapper' in feedConfig ) {
		return feedConfig['categoryMapper']; // only initialize once per feed
	}

	var cvLogger : Object = getJobMonitor() && getJobMonitor().getCurrentTask() ? getJobMonitor().getCurrentTask() : Logger;
	
	var api : Object = MapperUtil();
	feedConfig['categoryMapper'] = api;
	var columnTypeMap : Object = api.columnTypeMap; // for each type a function that returns the mapper for each type
	var keyToTypeMap : Object = api.keyToTypeMap; // for each keys that indicate a type
	var fetch : Function = api.fetch;
	var getEmpty : Function = api.getEmpty;
	var getConst : Function = api.getConst;

	/** the final method, that extracts all column values and writes it out */
	function getColumnWriter( writer : Object ) : Function {
		var columnMappers : Function = api.getColumnMappers( feedConfig['columnDefinition'] );
		var customFilter : Function = api.getCustomFilter( feedConfig['customFilter'] ); 
		return function( categorySO : Object ) {
			try {			
				if( customFilter( categorySO ) ) {
					var columnData : Array = columnMappers( categorySO );
					writer.writeData( columnData );
				}
			}
			catch(e) {
				cvLogger.error( 'Exception in Feed {0} with Category {1}', feedConfig.ID, categorySO.ID );
				throw e;
			}
		}
	}
	api.getColumnWriter = getColumnWriter;
	
	/** @reurn a function that returns the value of a resource bundle or a default string */
	function getResource( columnDefinition : Object ) : Function {
		var resources : Object = feedConfig['resources'];
		return api.getResource( columnDefinition, resources );
	}
	columnTypeMap['resource'] = getResource;
	keyToTypeMap['resource'] = 'resource';
	
	/** @return a function that extracts a property from the category */
	function getProperty( columnDefinition : Object ) : Function {
		var key : String = fetch( columnDefinition, 'property', null );
		var defaultValue : String = fetch( columnDefinition, 'default', '' );
		var mapping : Object = fetch( columnDefinition, 'mapping', null );
		if( !key ) {
			return getConst( defaultValue );
		}
		return function( categorySO : Object ) : String {
			var value : Object = categorySO.property( key, defaultValue );
			if( value && mapping ) {
				return value in mapping ? mapping[value] : defaultValue;
			}
			return value ? value.toString() : defaultValue;
		}
	}
	columnTypeMap['property'] = getProperty;
	keyToTypeMap['property'] = 'property';
	
	/** @return a function that extracts a property from the category */
	function getParentProperty( columnDefinition : Object ) : Function {
		var key : String = fetch( columnDefinition, ['parentProperty', 'property'], null );
		var defaultValue : String = fetch( columnDefinition, 'default', '' );
		var mapping : Object = fetch( columnDefinition, 'mapping', null );
		if( !key ) {
			return getConst( defaultValue );
		}
		return function( categorySO : Object ) : String {
			var parentSO : Object = categorySO.getParentCategorySO();
			var value : Object = parentSO.property( key, defaultValue );
			if( value && mapping ) {
				return value in mapping ? mapping[value] : defaultValue;
			}
			return value ? value.toString() : defaultValue;
		}
	}
	columnTypeMap['parentproperty'] = getParentProperty;
	keyToTypeMap['parentProperty'] = 'parentproperty';

	/** @return a function that extracts a link to the given category */
	function getLink( columnDefinition : Object ) : Function {
		var urlArgs : Object = fetch( columnDefinition, ['args', 'link'], null );
		var pipeline : String = fetch( columnDefinition, 'pipeline', 'Search-Show' );
		
		var urlArgMapper : Object = {};
		for( let argKey in urlArgs ) {
			let argValue : Object = urlArgs[argKey];
			urlArgMapper[ argKey ] = api.getMapper( argValue );
		}
		
		return function( categorySO : Object ) : String {
			var category : Category = categorySO.category;
			var link : URL = URLUtils.https( pipeline, 'cgid', category.ID );
			
			if( !empty(urlArgMapper) ) {
				for( let argKey in urlArgMapper ) {
					let mapper : Function = urlArgMapper[argKey];
					let value = mapper( categorySO );
					link.append( argKey, value );
				}
			}
			return link.toString();
		}
	}
	columnTypeMap['link'] = getLink;
	keyToTypeMap['link'] = 'link';

	return api;
}