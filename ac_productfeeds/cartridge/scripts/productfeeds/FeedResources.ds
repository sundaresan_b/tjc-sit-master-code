importPackage( dw.util );
importPackage( dw.system );

/** @const */ var splitRegExp : RegExp = /(?:[\r]?[\n])+/;
/** @const */ var lineRegExp : RegExp = /^(?:\[(?:([^\/]+)\/)?(\w+)\s*\])?([^#=]+)=(.*[^\\]|)(\\)?$/;
/** @const */ var multilineRegExp : RegExp = /^(.*[^\\]|)(\\)?$/;

function FeedResources() {
	var api : Object = {};
	
	var keyValueMap : Map = new HashMap();
	
	function init( resources : String ) : Object {
		if( empty( resources ) ) {
			return api;
		}
		
		var currentSiteID : String = Site.current.ID;
		var currentLocale : String = request.locale;
		var currentLocaleClass : String = -1 != currentLocale.indexOf('_') ? currentLocale.substr( 0, currentLocale.indexOf('_') ) : null;
		var siteLocales : Map = new HashMap();

		var lines : Array = StringUtils.trim( resources ).split( splitRegExp );
		for( let i = 0; i < lines.length; i++ ) {
			var line : String = lines[i],
				_, 
				siteID : String,
				locale : String, 
				key : String, 
				value : String,
				multiline : String,
				match : Array = line.match( lineRegExp );
			if( !match ) {
				continue;
			}
			[ _, siteID, locale, key, value, multiline ] = match;
			if( !key ) {
				continue; // skip not parsable lines				
			}
			if( multiline ) {
				var values : Array = [value];
				while( multiline && i+1 < lines.length ) {
					i++;
					line = lines[i];
					[ _, value, multiline ] = line.match( multilineRegExp );
					if( value ) {
						values.push( value );
					}
				}
				value = values.join('');
			}
			if( null==siteID ) {
				siteID = '';
			}
			if( null==locale || 'default'==locale ) {
				locale = '';
			}
			if( siteID!='' && siteID != currentSiteID ||  // not used in current site
				locale!='' && locale!=currentLocaleClass && locale!=currentLocale ) { // not used in current locale
				continue;
			}
			var siteLocale : Array = siteLocales.get( key );
			if( siteLocale ) {
				// key was already stored, replace with new value only if localization is more specific
				var existingSiteID : String,
					existingLocale : String;
				[ existingSiteID, existingLocale ] = siteLocale;
				if( existingSiteID.length > siteID.length || // existing siteID is more specific
					existingSiteID==siteID && existingLocale.length > locale ) { // same siteID but existing locale is more specific
					continue;
				}
			}
			siteLocales.put( key, [ siteID, locale ] );
			keyValueMap.put( key, value );
		}
		return api;
	}
	api.init = init;
	
	function fetch( key : String ) : String {
		var value : String = keyValueMap.get( key );
		if( null==value ) {
			value = keyValueMap.get( key.replace( /\.[^\.]*$/, '.*' ) ); // replace last portion of the key with "*" 
		}
		return value;
	}
	api.fetch = fetch;
	
	function message( key : String, defaultValue : String, args : Array ) : String {
		if( empty(key) ) {
			return defaultValue;
		}
		var value : String = fetch( key );
		if( null==value ) {
			value = defaultValue;
		}
		if( empty(args) ) {
			return value;
		}
		args.unshift( value );
		value = StringUtils.format.apply( StringUtils, args );
		args.shift();
		return value;
	} 
	api.message = message;

	return api;	
}