/**
* @output ProductCount : Number
* @output ErrorCount : Number
*/
importPackage( dw.object );
importPackage( dw.io );
importPackage( dw.util );
importPackage( dw.value );
importPackage( dw.system );

importPackage( dw.content );
importPackage( dw.catalog );
importPackage( dw.web );
importScript('bc_jobframework:job/JobMonitor.ds');

function execute( args : PipelineDictionary ) : Number
{
	var cvLogger : Object = getJobMonitor() && getJobMonitor().getCurrentTask() ? getJobMonitor().getCurrentTask() : Logger;
	var products : SeekableIterator = ProductMgr.queryAllSiteProductsSorted();
	var productCount : Number = 0;
	var errorCount : Number = 0;
	
	while (products.hasNext()) {
		var messages : Array = [];
		var product : Product = products.next();

		try {
			checkImages();
			checkSwatchImage();
			checkPrices();
			checkMaster();
			checkSet();
			checkMasterProduct();
			checkProduct();
			if ( !empty(messages) ) {
				errorCount++;
			}
		} catch (e) {
			cvLogger.error( 'Exception within product {0} throws: {1}', product.ID, e.message );
			errorCount++;
		}
		
		if( !empty(messages) ) {
			cvLogger.error( 'Product ID {0} has issues: {1}', product.ID, messages.join(', ') );
		}
		
		productCount++;
	}
	
	products.close();
	args.ProductCount = productCount;
	args.ErrorCount = errorCount;
	
	return PIPELET_NEXT;
	
	function checkProduct() {
		if( product.master || product.variant || product.productSet ) {
			return;
		}
		if( isAvailable( product ) && product.priceModel.price.available && isOnlinePath() ) {
			logError( 'orderable simple product' );
		}
	}

	/**
	* @return {boolean} if product is assigned to an online category where all ancestors are online 
	*/	
	function isOnlinePath( product : Product ) : Boolean {
		for each( var category : Category in product.onlineCategories ) {
			var pathOnline : Boolean = true;
			while( !category.root ) {
				category = category.parent;
				if( !category.online ) {
					pathOnline = false;
					break;
				}
			}
			if( pathOnline ) {
				return true;
			}
		}
		return false;
	}
	
	function checkMasterProduct() {
		if( product.variant || product.productSet ) { // only check master or standalone products
			return;
		}
		if( empty( product.name ) ) {
			logError( 'empty name' );
		}
		if( empty( product.longDescription.markup ) ) {
			logError( 'empty long description' );
		}
		if( !product.pageURL.match( /^accessoir/ ) && // hack to filter out accessoirs
			empty( product.custom['materialValue'] ) ) {
			logError( 'empty material value' );
		}
	}
	
	function checkMaster() {
		if( !product.master ) {
			return;
		}
		if( null==product.variationModel ) {
			logError( 'master misses variationModel' );
		}
		if( empty(product.variationModel.variants) ) {
			logError( 'master misses variants' );
		}
		
		var variationAttributes : Array = ['color', 'size', 'accessorySize'];
		for each( var variationAttribute : ProductVariationAttribute in product.variationModel.productVariationAttributes ) {
			if( -1==variationAttributes.indexOf( variationAttribute.ID ) ) {
				logError( 'unknown variation attribute: ' + variationAttribute.ID );
				return;
			}
			for each( var value : ProductVariationAttributeValue in product.variationModel.getAllValues(variationAttribute) ) {
				if( empty(value.value) ) {
					logError( 'missing variation value on ' + variationAttribute.ID );
				}
				if( empty(value.displayValue) || value.displayValue=='.'  ) {
					logError( 'missing displayValue on ' + variationAttribute.ID + ' and value ' + value.ID );
				}
			}
		}
	}
	
	function checkSet() {
		if( !product.productSet ) {
			return;
		}
		if( product.searchable ) {
			logError( 'product set is searchable' );
		}
		if( empty( product.productSetProducts ) ) {
			logError( 'product set misses some set products' );
		}
		if( product.online ) {
			var hasAvailable : Boolean = false;
			for each( var setProduct : Product in product.productSetProducts ) {
				if( isAvailable( setProduct ) ) {
					hasAvailable = true;
					break;
				}
			}
			logError( 'online set misses available products' );
		}
	}
	
	/**
	* @return {boolean} if product is online and orderable at current site
	*/
	function isAvailable( product : Product ) : Boolean {
		return product.online && product.assignedToSiteCatalog && product.availabilityModel.orderable;
	}
	
	function checkPrices() {
		if( product.master || product.productSet ) {
			return;
		}
		var price : Money = product.priceModel.price;
		if( !price.available ) {
			logError( 'missing a price' );
			return;
		}
		
		var priceBookID : String = Site.current.getCustomPreferenceValue('listPriceDefault');
		var list_price : Money = product.priceModel.getPriceBookPrice(priceBookID);
		// we do not know the sales pricebook
		// TODO: check if list price is larger than sale price
	}
	
	function checkSwatchImage() {
		if( !product.variant ) {
			return;
		}
		var swatchID : String = product.masterProduct.ID.split( '-', 1 )[0];
		var color : String = product.custom['color'];
		if (color.indexOf("-") != -1) {
			color = color.split("-")[0];
		}		
		var path : String = 'images/swatch/swatch_'+swatchID+'_'+color+'.jpg';
		var image : File = new File( '/CATALOGS/' + CatalogMgr.siteCatalog.ID + '/default/' + path );
		if( !image.exists() ) {
			logError( 'missing image file: ' + path );
		}
	}
	
	function checkImages() {
		var viewtypes : Array = ['small', 'medium', 'large', 'zoom'];
		
		if( !product.variant ) {
			return;
		}
		for each( var viewtype : String in viewtypes ) {
			var images : Collection = product.getImages(viewtype);
			if( empty(images) ) {
				logError( 'missing images: ' + viewtype );
			}
			for each( var image : MediaFile in images ) {
				checkExistance( image );
			}
		}
	}
	
	function checkExistance( mediaFile : MediaFile ) {
		var path : String = mediaFile.URL.toString().split( /\/v\d+\// )[1];
		var image : File = new File( '/CATALOGS/' + CatalogMgr.siteCatalog.ID + '/default/' + path );
		if( !image.exists() ) {
			logError( 'missing image file: ' + path );
		}
	}
	
	function logError( message : String ) {
		messages.push( message );
	}
}
