/**
 * Returns a ProgramGuide object which contains the structure of the TV guide to be used in the template.
 * The program guide is retrieved from a third party system, calling 3 different feeds. The channel must
 * be given as an input parameter (tjc or tjcchoice).
 *
 * Details for the web service:
 *
 * The exact URL for TJC Program Guide is 
 * https://digigate-exports.ebsnewmedia.com/channel_70/ through which we have to pass a dynamic file name 
 * for fetching respective details different columns in program guide page.
	
 * Algorithm to get display details are like.
 * A. At the beginning we pass the current date.xml (YYYYMMDD.xml) into the above domain name to load the start time/
 * end time and episode id. Example: https://digigate-exports.ebsnewmedia.com/channel_70/20150625.xml
 *
 * B. After that we call episodes.xml and fetch program id respective of episode id. Example: 
 * https://digigate-exports.ebsnewmedia.com/channel_70/episodes.xml
 * 
 * C. At last we call programmes.xml and fetch program title/synopsis & presenter name respective of 
 * program ID from above XML. Example: https://digigate-exports.ebsnewmedia.com/channel_70/programmes.xml
 *
 * Note: Same procedure is applied on TJC-Choice only the difference is URL is changed 
 * https://digigate-exports.ebsnewmedia.com/channel_865/
 *
 * @input Channel : String - the channel to get the program guide for. Could be tjc or tjcchoice.
 * @output ProgramGuide : Object - the program guide object.
 */
var dwSystem = require("dw/system"),
	ProgramGuideService = require("../services/ProgramGuideService"),
	ProgramGuideXMLService = require("../services/ProgramGuideXMLService"),
	ProgramGuide = require("../objects/ProgramGuide");

function execute( args : PipelineDictionary ) : Number
{
	var channel = args.Channel || 'tjc';
	
	// Get the programme feed
	var programGuideXMLService = new ProgramGuideXMLService("http.programguide.programme." + channel);
	var programme = programGuideXMLService.getResult();

	// Get the episodes feed
	programGuideXMLService = new ProgramGuideXMLService("http.programguide.episodes." + channel);
	var episodes = programGuideXMLService.getResult();

	// Get the program guide feed
	var programGuideService = new ProgramGuideService("http.programguide." + channel);
	var programguide = programGuideService.getResult();
	
	if (programme != null && episodes != null && programguide != null) {
		args.ProgramGuide = new ProgramGuide(channel, programguide, episodes, programme);
	}

    return PIPELET_NEXT;
}
