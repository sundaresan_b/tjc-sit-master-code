/**
* Creates new order in Netbanx;  only three parameters are required (merchantRefNum, currencyCode, totalAmount),
* the rest of the information is optional but recommended to improve the user experience. 
* Returns an response object. 
*
* @input Basket : dw.order.LineItemCtnr Either Basket or Order object
* @input OrderOwnerIp : String Ip address to attach to transaction
* @input Locale : String The locale of the current site
* @input CustomerProfile : Object Profile of current customer
* @input BillingDetails : Object Address of billing
* @input ShippingDetails : Object Address of shipping
* @input CVV : String CV Code of the credit card
* @input RememberCard : String CV Code of the credit card 
* @input ExistingCardSelected : Object The card selected on the dropdown
* @input ForgetCard : String CV Code of the credit card
* @input isMobileOrder : Boolean
*
* @output Response : Object Text of response with it's type (json, html or undefined)
*
*/
importPackage( dw.system );
importClass( dw.order.LineItemCtnr );
importScript( 'int_paysafe:/lib/libPaysafe.ds' );

var logger = Logger.getLogger('CreateOrder', 'CreateOrder'); 

function execute( args ) {
	// Get and configure request parameters
	var customerProfile = args.CustomerProfile;
	var orderOwnerIp =  args.OrderOwnerIp;
	var basket = args.Basket;
	var billingDetails = args.BillingDetails;
	var shippingDetails = args.ShippingDetails; 
	var requestLocale = args.Locale || '';
	var cardDetails = !empty(args.CurrentForms.billing.paymentToken.value) ? args.CurrentForms.billing.paymentToken.value : args.ExistingCardSelected.value;
	
	var cardCVCode = args.CVV;
	var rememberCard = args.RememberCard;
	var forgetCard = args.ForgetCard;
	var isMobileOrder= args.isMobileOrder ? args.isMobileOrder : false;
	
	// Localisation
	var locale;	
	if ( (requestLocale == 'fr_CA') || (requestLocale == 'en_US')){
		locale = requestLocale;	
	}else if ( requestLocale == 'fr' ){
		locale = 'fr_FR';	
	}else if ( requestLocale == 'en' ) {
		locale = 'en_GB';	
	}else { 
		locale = 'en_US';	// default value
	}
	
	try {
		logger.info("CreateOrder.ds - execute(args) - cardDetails : " + cardDetails);
	} catch (e) { 
		logger.info("cardDetails not available");
	}
 	   
	var createOrderResponse = createCustomerVault(basket, orderOwnerIp, customerProfile, billingDetails, shippingDetails, cardDetails, locale, cardCVCode, rememberCard, forgetCard,isMobileOrder);
	 
	 try {
		logger.info("CreateOrder.ds - execute(args) - createOrderResponse : " + JSON.stringify(createOrderResponse));
	} catch (e) {	}
	  
	if (createOrderResponse.contentType == 'json') {
		args.Response = createOrderResponse;
	} else {
		// Error handling 
		Logger.error("[" + createOrderResponse.scriptFileName + "] Error while processing transaction CreateOrder: " + createOrderResponse.errorText);
		args.Response = createOrderResponse;
		if (empty(createOrderResponse.content)){
			createOrderResponse.content={};
			createOrderResponse.content.id = basket.orderNo;
		}
		//return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}
