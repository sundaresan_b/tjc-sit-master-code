/**
* This script consolidates the ended auctions using the RisingAuctionMgr class
* @input ProcessStartTimestamp : Number - the epoch timestamp of the begin of the consolidation process
* @input ProductArray : Array - Array of the processed product IDs
* @input MinimumExecutionTime : String - the number of seconds of the minimum execution time
*/
importPackage( dw.system );
var RisingAuctionMgr = require("app_tjc/cartridge/scripts/risingauctions/manager/RisingAuctionMgr");
var cvLogger : Logger = Logger.getLogger('auctions', 'JobInfo');

function execute( args : PipelineDictionary ) : Number
{
    //Send the update notification on the rising auction channel
    if (!empty(args.ProductArray)) {
    	try {
    		var mgr = new RisingAuctionMgr();
    		var productArray : Array = args.ProductArray;
	   		mgr.sendUpdateNotification(productArray, 'consolidateEndedAuction');
    	} catch (e) {
    		cvLogger.error("Error in sending the update notification for the products {0} : ", args.ProductArray.toString());
    	}
	}
   
   //Wait until the minimum time is reached
   var execTime : Number = args.MinimumExecutionTime ? Number(args.MinimumExecutionTime) * 1000 : 30000;
   var timestamp : Number = args.ProcessStartTimestamp;
   var currentTime : Number = new Date().getTime();
   while (currentTime - timestamp < execTime) {
   		currentTime = new Date().getTime();
   }
  
   return PIPELET_NEXT;
}
