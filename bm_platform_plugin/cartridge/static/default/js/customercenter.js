document.observe("dom:loaded", function() {
	
	UI.init();
	
});
//---------- complex data types ------------------------------------

//The Url class
var URL = function(url){
	if (! url) return null;
	url = url.replace(/\+/gi, ' '); // replace + with a ' '
	
	this.parameters = {};
	this.baseURL = url
	
	// get the hash
	var split = url.split('#');
	this.hash = (split.length > 1) ? split[1] : false;
	
	// get the body
	var split = split[0].split('?');
	this.urlBody = split[0];
	
	// split the parameters
	if (split.length > 1) {
	
		var params = split[1].split('&');
		var cLen = params.length;
		for(var cnt = 0; cLen--; cnt++) {
			var cStr = new String(params[cnt]).split('=');
			this.parameters[cStr[0]] = decodeURIComponent(cStr[1]);
		}
	}
	
	this.setHash = function(newHash){ this.hash = newHash;  }
	
	this.hasParameters = function(){
		for (var p in this.parameters ){
			if (p) return true;
		}
		return false;
	};
	
	this.hasParameter = function(pName){
		return (this.parameters[pName]) ? true : false;
	};
	
	this.getParameter = function(pName){
		return this.parameters[pName];
	};
	
	this.addParameter = function (name, value) {
		
		if (name) {
			this.parameters[name] = ((value !== undefined) ? value : '');
		}
	};
	
	this.addParameters = function (parameterMap) {
		for (param in parameterMap){
			this.addParameter(param, parameterMap[param]);
		}
	};
	
	this.removeParameter = function(name){
		delete this.parameters[name];
	};
	
	this.replacePipeline = function(newPipe){
		var parts = this.urlBody.split('/');
		parts[parts.length - 1 ] = newPipe;
		
		this.urlBody = parts.join('/');
		
	};
	
	this.toString = function() {
		var url = this.urlBody;
		
		if (this.hasParameters()){
			var first = true;
			for (p in this.parameters){
				url += (first) ? '?' : '&';
				url += p + '=' + encodeURIComponent(this.parameters[p]);
				first = false;
			}
		}
		
		if (this.hash) {
			url += '#' + this.hash;
		}
		
		return url;
	};
	
};

// The UI Class
var UI = {
		searchButton : null
		,viewOrdersUrl : new URL(app.URLS.VIEW_ORDERS_URL)
		,createOrderUrl : new URL(app.URLS.CREATE_ORDER_URL)
		,submitActor : null
		
		,init : function() {
			
			this.searchButton = $('search_customers');
			
			// store clicked button to exclude submit buttons in search_customer_result later on
			$$("#search_customer_result input[type=submit]").invoke('observe','click', function(event) {
				UI.submitActor = this;
			});
			
			// catch search and paging submit and thread this via ajax
			// we want to add some additional stuff here
			$$('#search_customer_form, #search_customer_result').invoke('observe','submit', function(event) {
					
				//console.info(UI.submitActor);
				
				// exclude submit button in search_customer_result
				if (UI.submitActor == null || (UI.submitActor.name != 'EditAll' && UI.submitActor.name != 'process'  && UI.submitActor.name != 'create'  && UI.submitActor.name != 'confirmDelete')) {
					
					// show loading
					$('search_customer_result').addClassName('loading');
					
					// put submit action on top to ensure it is submitted - otherwise problems with pagingsize
					if (UI.submitActor != null) {
						$('search_customer_result').insert({top: '<input style="display:none" type="submit" value="' + UI.submitActor.value + '" name="' + UI.submitActor.name + '">'});
					}
					
					// get search result HTML
					this.request({
				        onFailure: function() {$('search_customer_result').removeClassName('loading');},
				        onSuccess: function(t) {
				        
						// use wrapper to only get search result
						var wrapper = new Element('div');
						wrapper.update(t.responseText);
						var form = wrapper.select('form[name="customerListBottom"]');
						//var pagingLinks = wrapper.select('table.pagecursor')[0].select('a');
						wrapper = wrapper.select('form[name="customerListBottom"] table');
						
						/*pagingLinks.each(function(paginLink){
							paginLink.setAttribute('href', paginLink.href.replace(/document.customerListBottom.submit/g, "test"))
						});*/
						
						//console.info(wrapper);
						
						if (wrapper != null && wrapper.size() > 0) {
							// get search result rows
							var rows = wrapper[1].down().childElements();
								   
							var customerNoIdx = getTableEntryIdx(rows[0], 'Customer Number');
							var loginIdx = getTableEntryIdx(rows[0], 'Login');
						
							if (customerNoIdx != null && loginIdx != null) {
								
								rows[0].insert({bottom: '<td nowrap="nowrap" class="table_header n e s">View Orders</td><td nowrap="nowrap" class="table_header n e s">Create Order</td>'});
								
								var login = null;   
								var column = null;
									
								// set actions for each row
								rows.slice(1).each(function(row){
									row.select('a').invoke('setAttribute', 'target', '_blank');
									
									// set action URLs
									UI.viewOrdersUrl = new URL(app.URLS.VIEW_ORDERS_URL);
									UI.createOrderUrl = new URL(app.URLS.CREATE_ORDER_URL);
									
									column = row.childElements();
									
									customerNo = $(column[customerNoIdx]).select('a')[0].innerHTML;
									login = column[loginIdx].select('a')[0].innerHTML;
									
									UI.viewOrdersUrl.addParameters({"OrderSearchForm_CustomerNo":customerNo});
									UI.createOrderUrl.addParameters({"custid":login});
									
									// insert new actions into HTML
									row.insert({bottom: '<td class="table_detail e s"><a href="' + UI.viewOrdersUrl + '" target="_blank">view</a></td><td class="table_detail e s"><a href="' + UI.createOrderUrl + '" target="_blank">create</a></td>'});
								});
							}
							    
						    // update new HTML
						    $('search_customer_result').update(wrapper[0]);	
						    // update action since it could be got changed
						    $('search_customer_result').setAttribute('action', form[0].action);
						   
						   	// store clicked button to exclude submit buttons in search_customer_result later on
							$$("#search_customer_result input[type=submit]").invoke('observe','click', function(event) {
								UI.submitActor = this;
							});
				        }
						$('search_customer_result').removeClassName('loading');
					}
			    });
			    Event.stop(event); // stop the form from submitting
			}
			UI.submitActor = null;
		});
	}
};

// Helper Functions

// Get Index of Table Header
function getTableEntryIdx(tr, name) {
	var td = tr.select('td').filter(function(elem){
        return elem.innerHTML == name;
    });

	return td.size() > 0 ? Element.previousSiblings($(td[0])).size() : null;
}

//this supports trigger native events such as 'onchange'
//whereas prototype.js Event.fire only supports custom events
function triggerEvent(element, eventName) {
	// safari, webkit, gecko
	if (document.createEvent)
	{
		var evt = document.createEvent('HTMLEvents');
		evt.initEvent(eventName, true, true);
		return element.dispatchEvent(evt);
	}

	// Internet Explorer
	if (element.fireEvent) {
		return element.fireEvent('on' + eventName);
	}
}

function fakeSubmit(event) {
	//alert("FF Submit");
    var target = event ? event.target : this;
    /* Fire a submit event */
    triggerEvent(target, "submit");
    /* Call the real submit function */
   // this._submit();
}

// make submit events observable
if(window.HTMLElement){ 
    /* DOM-compliant Browsers */
    HTMLFormElement.prototype._submit = HTMLFormElement.prototype.submit;
    HTMLFormElement.prototype.submit = fakeSubmit;
} else { 
    /* IE does not expose it's HTMLElement object or its children
        Let the document load so all forms are accounted for */
    document.observe("dom:loaded", function(){
        for(var i=0; i<document.forms.length; i++){
            document.forms[i]._submit= document.forms[i].submit;
            document.forms[i].submit = function(event) {
            	//alert("IE Submit");
                var target = event ? event.target : this;
                triggerEvent(target, "submit");
                //$(target).fire("fake:submit")
                /* Call the real submit function */
               // this._submit();
            };
        }
    });
}
