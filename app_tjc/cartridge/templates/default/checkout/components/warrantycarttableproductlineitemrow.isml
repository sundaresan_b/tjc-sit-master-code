<!--- TEMPLATENAME: carttableproductlineitemrow.isml --->
	<isset name="budgetPayItemsInCart" value="${false}" scope="page" />

	<isloop items="${Shipment.items}" alias="FormLi" status="loopstate">
		<isset name="lineItem" value="${FormLi.object}" scope="page" />
		<isset name="isTJCPlusItem" value="${lineItem.product.custom.isTJCPlusProduct}" scope="page" />
		
		<isif condition="${lineItem.custom.iswarrantySelected}">
			<isscript>
				var isInWishList = false;
				if (pdict.WishList) {
					var wishListItems =	pdict.WishList.items;
					var wishListItemsSize = wishListItems.size();
					for each( var wishListItem in wishListItems ) {
						if (lineItem.productID	== wishListItem.product.ID) {
							isInWishList = true;
							break;
						}
					}
				}
				var availabilityLevels = lineItem.product.availabilityModel.getAvailabilityLevels(lineItem.quantityValue);
			</isscript>
			<isscript>
				var category = 'primaryCategory' in lineItem.product && lineItem.product.primaryCategory && 'displayName' in lineItem.product.primaryCategory ? lineItem.product.primaryCategory.displayName : '';
				var gtm_data = {
					name: lineItem.product.name,       /* Name or ID is required.*/
					id: '' + lineItem.productID,
					price: '' + (lineItem.product.priceModel.price.value || ''),
					brand: lineItem.product.brand || '',
					category: category,
					variant: '' + (lineItem.product.custom.size || ''),
					quantity: lineItem.quantity.decimalValue.toString() || ''
				};
				
				var avm = lineItem.product.availabilityModel;
				pdict.available = avm.availability>0;
		
				var availableCount = "0";
				if (pdict.available && !empty(avm.inventoryRecord)) {
					availableCount = avm.inventoryRecord.perpetual ? "999" : avm.inventoryRecord.ATS.value.toFixed();
				}
				
			</isscript>
				
				<iscomment>	Skip bonus-choice products in first pass.	</iscomment>
				<isif condition="${lineItem.bonusDiscountLineItem == null}">
					<tr class="cart-row clearfix tableafterwarrantyadded ${availabilityLevels.notAvailable.value > 0 ? 'no-border' : ''} <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>" data-uuid="${lineItem.getUUID()}" data-gtm="${JSON.stringify(gtm_data)}">
						<td class="item-image <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
							<div class="product-image">
							<isif condition="${lineItem.product.custom.isWarrantyProduct}">
								<img class="warranty-icon-image" src="${URLUtils.staticURL('/images/warranty-badge.svg')}" alt="TJC -Warranty Program" title="TJC -Warranty Program"/>
								<!-- <div class="warranty-shield-icon <isif condition="${!isCartPage}">warranty-shield-icon-noCart</isif>">
									<i class="shield-icon" ></i>
									<i class="check-icon" ></i>
								</div> -->
							<iselse/>
								<isproductimage product="${lineItem.product}" imgsize="cart" lazyloading="${false}" />
							</isif>
							</div>
						</td>

						<td class="item-details <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
							<iscomment>Call module to render product</iscomment>
							<isif condition="${!empty(isCartPage)}">
								<isdisplayliproduct page_ns="${isCartPage}" p_productli="${lineItem}" p_formli="${FormLi}" p_hidepdplinks="${readonly}" p_editable="${!readonly}" p_hideprice="${true}" p_hidepromo="${true}" p_istjcplusitem="${isTJCPlusItem}"/>
							<iselse>
								<isdisplayliproduct p_productli="${lineItem}" p_formli="${FormLi}" p_hidepdplinks="${readonly}" p_editable="${!readonly}" p_hideprice="${true}" p_hidepromo="${true}" p_istjcplusitem="${isTJCPlusItem}" />	
							</isif>
							<iscomment>Product Existence and Product Availability</iscomment>
							<div class="availability-msg">
								<isif condition="${!lineItem.bonusProductLineItem || lineItem.getBonusDiscountLineItem() != null}">
									<isif condition="${lineItem.product == null}">
										<div class="not-available-msg">
											${Resource.msgf('cart.removeditem','checkout',null,dw.system.System.preferences.custom['raExpirationTimeDwr']) }
										</div>
                                    <iselseif condition="${lineItem.custom.itemSource == 'RISING' && risingAuctionMgr.isAuctionExpiredInAMS(lineItem.productID)}"/>
                                        <div class="not-available-msg">
                                            ${Resource.msg('cart.risingauctionproductexpired','checkout',null)}
                                        </div>
									<iselse/>
										<isset name="product" value="${lineItem.product}" scope="page" />
										<isif condition="${pdict.Basket.getAllProductQuantities().get(lineItem.product) == null}">
											<div class="not-available-msg">
												${Resource.msgf('cart.removeditem','checkout',null,dw.system.System.preferences.custom['raExpirationTimeDwr']) }
											</div>
										<iselse/>
											<isset name="quantity"	value="${pdict.Basket.getAllProductQuantities().get(lineItem.product).value}" scope="page" />
										</isif>
										<isscript>
											 var allLiveTvBidders = pdict.LiveTvBidderDetails,
											 	 isLiveTVProduct = false;

											 for each (var bidder in allLiveTvBidders)
											 {
											 	 if(lineItem.productID == bidder.stockCode)
											 	 {
											 	 	isLiveTVProduct = true;
											 		break;
											 	 }
											 }
										</isscript>
										<isif condition="${isLiveTVProduct}">
											<div class="not-available-msg">
												${Resource.msg('cart.store.productinlivetv','checkout', null)}
											</div>
										</isif>
									</isif>
								</isif>
							</div>
							
							
							
							<div class="item-price hidden <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
								<div class="price-budgetpay-details">
									<isif condition="${lineItem.product != null}">
										<iscomment>
											StandardPrice: quantity-one unit price from the configured list price
											book. SalesPrice: product line item base price. If these are
											different, then we display crossed-out StandardPrice and also
											SalesPrice.
										</iscomment>
		
		
										<iscomment>Get the price model for this	product.</iscomment>
										<isset name="PriceModel" value="${lineItem.product.getPriceModel()}" scope="page" />
		
										<iscomment>Get StandardPrice from list price book.</iscomment>
										<isinclude template="product/components/standardprice" />
		
		
										<iscomment>Get SalesPrice from line item itself.</iscomment>
										<isset name="SalesPrice" value="${lineItem.basePrice}" scope="page" />
										<isset name="StandardPrice" value="${CartUtils.getVATAdjustedPrice(StandardPrice, lineItem)}" scope="page" />
										<isif condition="${StandardPrice.available && StandardPrice > SalesPrice && (empty(lineItem.custom.itemSource) || (lineItem.custom.itemSource !== 'TV' && lineItem.custom.itemSource !== 'TGTV'))}">
											<iscomment>StandardPrice and SalesPrice are different, show standard</iscomment>
											<div class="price price-promotion">
												<div class="price-sales"><isprint value="${SalesPrice}" /></div>
												<div class="price-standard"><isprint value="${StandardPrice}" /></div>
											</div>
										<iselse/>
											<div class="price">
												<div class="price-sales"><isprint value="${SalesPrice}" /></div>
											</div>
										</isif>
									</isif>
									
									<isif condition="${!empty(lineItem.custom.installments) && lineItem.custom.installments > 0}">
										<div class="budgetpay">
											<img src="${URLUtils.staticURL('/images/budgetpay-logo.png')}">
										</div>
									</isif> 
									
								</div>								
							</div>
							
							
							<isif condition="${lineItem.custom.itemSource == 'RISING' || lineItem.custom.itemSource == 'TV' || lineItem.custom.itemSource == 'TGTV'}">
							<div class="item-total <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
                            <isif condition="${lineItem.bonusProductLineItem}">
								<div class="bonus-item">
									${Resource.msg('global.bonus','locale',null)}
								</div>
							<iselse/>                               
								<div class="price-budgetpay-details">
	                                <iscomment>Display the unadjusted price if the line item has price adjustments.</iscomment>
									<isif condition="${lineItem.priceAdjustments.length > 0}">
	                                    <div class="price price-promotion">
	                                        <div class="price-total price-sales"><!---${Resource.msg('cart.itemtotal','checkout',null)}---><isprint value="${lineItem.getAdjustedPrice()}" /></div>
	                                        <div class="price-total price-standard"><!---${Resource.msg('cart.listprice','checkout',null)}---><isprint value="${lineItem.getPrice()}" /></div>
	                                    </div>
	                                    
										<iscomment>Display the promotion name for each price adjustment.</iscomment>
										<isloop	items="${lineItem.priceAdjustments}" var="pa" status="prAdloopstatus">
											<div class="promo hidden">
												<isif condition="${lineItem.quantityValue > 1 && lineItem.quantityValue != pa.quantity}">
													<isprint value="${pa.quantity}" /> x
												</isif>
												<isprint value="${pa.promotion.calloutMsg}" encoding="off"/>
											</div>
										</isloop>
									<iselse/>
										<iscomment>Display non-adjusted item total.</iscomment>
										<div class="price price-promotion">
	                                        <span class="price price-total price-sales">
	                                            <isprint value="${lineItem.getAdjustedPrice()}" />
	                                        </span>
	                                        <isif condition="${lineItem.getAdjustedPrice().value < StandardPrice.value}"> 
	                                        	<div class="price-total price-standard"><isprint value="${StandardPrice}" /></div>
	                                        </isif>
	                                    </div>
									</isif>
									
									<isif condition="${!empty(lineItem.custom.installments) && lineItem.custom.installments > 0}">
										<div class="budgetpay">
											<img src="${URLUtils.staticURL('/images/budgetpay-logo.png')}">
										</div>
									</isif>
									
								</div>
							</isif>


							<iscomment>Options</iscomment>
							<isif condition="${lineItem.optionProductLineItems.size() > 0}">
								<isloop items="${lineItem.optionProductLineItems}" var="oli">
									<isif condition="${oli.price > 0}">
										<isif condition="${oli.price > oli.adjustedPrice}">
											<div class="price-option">
												<span class="label">
													${Resource.msg('cart.option','checkout',null)}:
												</span>
												<span class="value">
													<strike>
														<isprint value="${oli.price}" />
													</strike>
													+ <isprint value="${oli.adjustedPrice}" />
												</span>
											</div>
										<iselse/>
											<div class="price-option">
												<span class="label">
													${Resource.msg('cart.option','checkout',null)}:
												</span>
												<span class="value">
													+ <isprint value="${oli.price}" />
												</span>
											</div>
										</isif>
									</isif>
								</isloop>
							</isif>
							
						</div>
						<iselse/>
						
						<div class="cartpricing">
							<div class="price-budgetpay-details">
								<div class="cartpricingtemplate">
									<isset name="Product" value="${lineItem.product}" scope="pdict" />
									<isinclude template="product/components/cartpricing"/>
								</div>								
								
								<isif condition="${!empty(lineItem.custom.installments) && lineItem.custom.installments > 0}">
									<div class="budgetpay">
										<img src="${URLUtils.staticURL('/images/budgetpay-logo.png')}">
									</div>
								</isif>
							</div>							
						</div>	
						</isif>
						
						
						<div class="item-quantity <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
							<isif condition="${empty(isTJCPlusItem) && !isTJCPlusItem}">								
	                            <isif condition="${lineItem.product.custom.isWarrantyProduct}">
								    <isscript>
								        var warrantyParentProducts = pdict.Basket.getProductLineItems(lineItem.custom.linkedWarrantyProduct);
								        var warrantyParentProduct = warrantyParentProducts[0];
								        var warrantyParentProductQuantity = warrantyParentProduct.quantityValue;
								    </isscript>
								</isif>
	
	                            <div class="quantity-value">
	                                <isif condition="${lineItem.bonusProductLineItem}">
									    <isprint value="${lineItem.quantity}" />
								    <iselse/>
	                                    <isset name="IsMissedAuctionProduct" value="${(lineItem.custom.itemSource == 'WEBC' || lineItem.custom.itemSource == 'TGWEBC')}" scope="page"/>
										<isif condition="${readonly || (!empty(lineItem.custom.auctionCode) && !IsMissedAuctionProduct)}">
											
											<div class="quantity-input-container">
												<div class="cartquantitybox">
			                                		<span>Qty:</span><span style="width:46px;text-align:center;"><isprint value="${lineItem.quantity}" /></span>	
			                                	</div>
											</div>
										<iselse>
											<isif condition="${lineItem.product.custom.isWarrantyProduct}">
												<isset name="limit" value="${warrantyParentProductQuantity}" scope="page"/>
	                                        <iselseif condition="${IsMissedAuctionProduct}" />
	                                            <isset name="limit" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('missedAuctionProductLimit')}" scope="page"/>
	                                        <iselse/>
	                                            <isset name="limit" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('fixedPriceProductLimit')}" scope="page"/>
	                                        </isif>
	
	                                        <div class="quantity-input-container clearfix">
	                                        	<isscript>
	                                        		var selectableQuantities = new Number(15);
									               		if(availableCount == 0){
									               			selectableQuantities = 0;
									               		}
									               		else if(selectableQuantities < availableCount) {
									               			selectableQuantities = selectableQuantities;
									               		}
									               		else{
									               			selectableQuantities = new Number(availableCount);
									               		} 
			                                		var productquantity = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
			                                		quantity = lineItem.quantity.value.toFixed();
			                                		
			                                		var basketQuantity = new Number(quantity),
				                                		selectQty = new Number(selectableQuantities.toFixed());
				                                		
				                                	if(basketQuantity > selectQty)
				                                		selectableQuantities = new Number(quantity);				                                	
			                                	</isscript>
			                                	<div class="cartquantitybox">
			                                	<span>Qty:</span>
			                                	<isif condition="${selectableQuantities == 0}">
			                                		<span style="width:46px;text-align:center;"><isprint value="${quantity}"/></span>		                                		
		                                		<iselse>
		                                			<div class="form-row required validation-success cartquantity">	
			                                			<div class="field">
				                                		<select name="${FormLi.quantity.htmlName}" class="input-select non-custom-select" <isif condition="${lineItem.product.custom.isEngravingItem}">disabled</isif>>
				                                			<isloop items="${productquantity}" var="optionValue" status="loopstatus" begin="1" end="${selectableQuantities}">
						                            			<option value="${optionValue.toFixed()}" <isif condition="${optionValue.toFixed() == quantity}">selected="selected"</isif>>${optionValue.toFixed()}</option>
						                            		</isloop>														  
														</select>
                                           				</div>
	                                             	</div>
			                                	</isif>			
	                                             </div>
	                                            <button class="button button-icon button-transparent button-update hidden" type="submit" title="${Resource.msg('global.update','locale',null)}" name="${pdict.CurrentForms.cart.updateCart.htmlName}" <isif condition="${lineItem.product.custom.isEngravingItem}">disabled="disabled"</isif>>
	                                                <i class="fa fa-sync-alt"><!-- icon --></i>
	                                            </button>
	                                            
	                                            <isset name="productLineItem" value="${lineItem}" scope="page" />
	                                            <isset name="p_editable" value="${!readonly}" scope="pdict" />
	                                            <isdisplayvariationvalues product="${productLineItem.product}"/>
	                                            
	                                        </div>
	                                    </isif>
	                                </isif>
	                            </div>
                            </isif>                                                                                 
						</div>
							
							
						
						<div class="item-pnp hidden <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
							<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('newPnpLogicEnabled')}">
								<div class="mobile-label only-for-mobile">
                                   ${Resource.msg('global.pnpcart','locale',null)}
                                </div>

                                <isif condition="${reachedPnpMax || !isStandardShipment}">
                                    <div class="item-pnp-value">-</div>
                                <iselse/>
									<div class="item-pnp-value"><isprint value="${dw.util.StringUtils.formatMoney(new dw.value.Money(lineItem.custom.pnp, session.getCurrency().getCurrencyCode()))}" /></div>
                                </isif>
							</isif>
						</div>						
						
						
						
						<isif condition="${availabilityLevels.notAvailable.value > 0}">
	                        <div class="cart-row clearfix cart-row-availability">
	                           <isinclude template="checkout/cart/cartavailability" />
	                        </div>
	                    </isif>
	                    
	                    <div class="stockstatus">
		                    <isscript>
							  var todaysDate = new Date();
							  var stockDate = product.availabilityModel.inventoryRecord.inStockDate;
							</isscript>
							     
								<isif condition="${product.availabilityModel.inventoryRecord != null && product.availabilityModel.inventoryRecord.inStockDate != null}">
									<isset name="inStockDate" value="${product.availabilityModel.inventoryRecord.inStockDate.toDateString().split(' ').slice(1).join(' ')}" scope="page"/>		
								<iselse/>
									<isset name="inStockDate" value="${null}" scope="page"/>
								</isif> 
								<isif condition="${product.availabilityModel.availabilityStatus =='PREORDER' && !product.custom.isEngravingItem}">  
								<isif condition="${inStockDate != null}">
								<isif condition ="${todaysDate <= stockDate}">
										<isset name="stockDate" value="${inStockDate.split(' ')}" scope="page"/>
										<isset name="lastChar" value="${stockDate[1].slice(-1)}" scope="page" />
										<isset name="lastSecondChar" value="${stockDate[1].charAt(stockDate[1].length-2)}" scope="page" />
										<isset name="suffix" value="${lastChar == '1' ? 'st' : (lastChar == '2' ? 'nd' : (lastChar == 3 ? 'rd' : 'th'))}" scope="page" />
										<isset name="suffix" value="${lastSecondChar == '1' ? 'th' : suffix}" scope="page" />
							    		<isset name="stockmsg" value="${StringUtils.format(Resource.msgf('global.inStockDatePreorder','locale',null), stockDate[1],suffix,stockDate[0],stockDate[2])}" scope="page"/>
							    		<div class="stockstatus-box">
								    		<span class="expected <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">Preorder- ${stockmsg} 
								    		</span>
							    		</div>
							    </isif>
							    		 <iselse/>
						    		 	<div class="stockstatus-box">
										    <span class="expected <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>"><Strong>Pre-Order</Strong> 
								    		</span>
							    		</div>
								</isif>		 
							    </isif>
	    				</div>
	    				
	    				<isset name="basket" value="${pdict.Basket}" scope="page"/>
							<isscript>
							var test = productLineItem.product.ID;
							var producterror;
							var productExistence = true; 
							var pricevalid;
							var pricesAvailable;
							
							if(productLineItem.product == null || !productLineItem.product.online) {
										productExistence = false; 
									}
									
									var mgr = new RisingAuctionMgr();
									var test = mgr.isAuctionExpiredInAMS(productLineItem.product.ID);
									if (productLineItem.custom.itemSource == 'RISING' && mgr.isAuctionExpiredInAMS(productLineItem.product.ID)) {
										/*productExistence = false;*/
									}
									var qty = basket.getAllProductQuantities().get(productLineItem.product).value;
									var availabilityLevels : ProductAvailabilityLevels = productLineItem.product.getAvailabilityModel().getAvailabilityLevels(productLineItem.quantityValue);
									if (availabilityLevels.getNotAvailable().value == qty)  {
										productExistence = false;
									}
									pricesAvailable = productLineItem.basePrice.available;
								
							</isscript> 
							<isif condition="${!(pricesAvailable) || !(productExistence)}" > 
								<div class="producterror" style="display:none">This item has an error,Please remove it to proceed with the checkout</div>  
							</isif>
							
							
						</td>												
						

						<isif condition="${!readonly}">
							<td class="xlt-removeBasketItem item-remove <isif condition="${lineItem.product.custom.isWarrantyEligible && (!empty(lineItem.custom.warrantyQty) && !empty(lineItem.custom.warrantyUnitPrice) && !empty(lineItem.custom.linkedWarrantyProduct))}">no-border</isif>">
								<isif condition="${!lineItem.bonusProductLineItem || lineItem.getBonusDiscountLineItem() != null}">
									<button class="button button-icon button-transparent button-remove-item" type="submit" value="${Resource.msg('global.remove','locale',null)}" name="${FormLi.deleteProduct.htmlName}">
										<i class="fa fa-times"><!-- icon --></i>
									</button>
								</isif>
							</td>
						</isif>
					</tr>

                    

					<iscomment>Bundles</iscomment>
					<isif condition="${lineItem.bundledProductLineItems.size() > 0}">
						<isloop items="${lineItem.bundledProductLineItems}" var="bli">

							<iscomment>same color</iscomment>
							<tr class="cart-row cart-row-bundle clearfix">
								<td class="item-image"><!-- Blank to create bundle indent --></td>

								<td class="item-details">
									<isif condition="${bli.product != null}">
										<a href="${URLUtils.https('Product-Show','pid', bli.productID)}" title="${bli.productName}">
											<isif condition="${bli.product.getImage('small',0) != null}">
												<img src="${bli.product.getImage('small',0).getURL()}" alt="${bli.product.getImage('small',0).alt}" title="${bli.product.getImage('small',0).title}" />
											<iselse/>
												<img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${bli.productName}" title="${bli.productName}" />
											</isif>
										</a>
									<iselse/>
										<img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${bli.productName}" title="${bli.productName}" />
									</isif>
									<div class="name">
										<a href="${URLUtils.https('Product-Show','pid', bli.productID)}" title="${bli.productName}">
											<isprint value="${bli.lineItemText}" />
										</a>
									</div>
									<div class="itemnumber">
										<span class="label">
											${Resource.msg('global.itemno','locale',null)}
										</span>
										<span class="value">
											<isprint value="${bli.productID}" />
										</span>
									</div>

									${Resource.msg('global.included','locale',null)}

									<iscomment>Product Existence and Product Availability</iscomment>
									<isif condition="${!bli.bonusProductLineItem}">
										<isif condition="${bli.product == null}">
											<span class="notavailable">
												${Resource.msgf('cart.removeditem','checkout',null,dw.system.System.preferences.custom['raExpirationTimeDwr']) }
											</span>
											here3
										<iselse/>
											<isset name="product" value="${bli.product}" scope="page" />
											<isset name="quantity" value="${pdict.Basket.getAllProductQuantities().get(bli.product).value}" scope="page" />
											<isinclude template="checkout/cart/cartavailability" />
										</isif>
									</isif>
								</td>

								<td class="item-price"><!-- empty --></td>

								<td class="item-quantity">
                                                   <div class="mobile-label only-for-mobile">
                                                       ${Resource.msg('global.qty','locale',null)}
                                                   </div>

                                                   <div class="quantity-value">
										<isprint value="${bli.quantity}" />
									</div>
								</td>

								<td class="item-total"><!-- empty --></td>

								<isif condition="${!readonly}">
								    <td class="item-remove"><!-- empty --></td>
                                </isif>
							</tr>
						</isloop>

					</isif>
				</isif>
		</isif>
		<isif condition="${lineItem.custom.iswarrantySelected}">
			<isif condition="${lineItem.custom.iswarrantySelected}">
				<isloop items="${Shipment.items}" alias="FormLis" status="loopstate">
					<isif condition="${FormLis.object.productID.equals(lineItem.custom.linkedWarrantyProduct)}"> 
						<isset name="warrantyItem" value="${FormLis.object}" scope="page" />
						<isinclude template="checkout/components/warrantyproductlineitemrow"/>
					</isif>
				</isloop>
			</isif>
		</isif>
	</isloop>
	
<iscomment>If we are in the budgetPay product loop and basket contains both BP and NonBP Products, show total</iscomment>	
<isif condition="${budgetPayItemsInCart && BudgetPayUtils.containsBudgetPayAndNonBudgetPayProducts(pdict.Basket) && readonly && !empty(budgetPay) && budgetPay.available && pdict.CurrentCustomer.authenticated}">
	<tr class="order-totals-budgetpay">
		<td class="budget-pay-container">
			<div class="place-order-totals">
				<table class="order-totals-table">
					<isbudgetpayordertotals p_budgetpay="${budgetPay}" />
				</table>
			</div>		
		</td>
	</tr>
</isif>	