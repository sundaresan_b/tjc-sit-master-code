<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isscript>
	importScript( "cart/CartUtils.ds" );
    var RisingAuctionMgr = require("~/cartridge/scripts/risingauctions/manager/RisingAuctionMgr"),
        risingAuctionMgr =  new RisingAuctionMgr();
</isscript>
<isscript>
	importScript( "cart/BudgetPayUtils.ds" );
	var budgetPay = BudgetPayUtils.getInstallments(pdict.Basket),
	budgetPayAvailable = !empty(budgetPay) && budgetPay.available,
	budgetPayEligible = BudgetPayUtils.isEligible(pdict.Basket, pdict.CurrentCustomer);
</isscript>

<isset name="readonly" value="${pdict.readonly}" scope="page" />
<isset name="reachedPnpMax" value="${pdict.Basket.defaultShipment.getStandardShippingLineItem().custom.reachedPnpMax}" scope="page" />
<isset name="isStandardShipment" value="${pdict.Basket.defaultShipment.shippingMethod.ID == 'gb_standard'}" scope="page" />

<table id="cart-table" class="xlt-cartTable item-list cart-table<isif condition="${readonly}"> read-only</isif>">
	<tbody>
		<iscomment>	LOOP TO SEPARATE DIFFERENT SHIPMENTS ex: Gift Registry Shipments Etc.</iscomment>
		<isloop items="${pdict.CurrentForms.cart.shipments}" var="Shipment"	status="loopstate">
			
			<isinclude template="checkout/components/warrantycarttableproductlineitemrow"/>
			<isinclude template="checkout/components/carttableproductlineitemrow"/>

			<iscomment>Gift Certificates</iscomment>
			<isloop items="${Shipment.giftcerts}" var="GiftCertificate" status="loopstate">

				<tr class="cart-row cart-row-giftcert clearfix">

					<td class="item-image">
						<img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="${GiftCertificate.object.lineItemText}" />
					</td>

					<td class="item-details">
						<div class="name">
							<isprint value="${GiftCertificate.object.lineItemText}" />
						</div>

						<p class="gift-cert-delivery">
							${Resource.msg('cart.giftcertdelivery','checkout',null)}
						</p>

						<div class="giftcertamount clearfix">
							<span class="label">
								${Resource.msg('global.price','locale',null)}:
							</span>
							<span class="value">
								<isprint value="${GiftCertificate.object.basePrice}" />
							</span>
						</div>

						<div class="attribute">
							<isif condition="${GiftCertificate.object.recipientName!=null}">

								<span class="label">
									${Resource.msg('global.to','locale',null)}:
								</span>
								<span class="value">
									<isprint value="${GiftCertificate.object.recipientName}" />,
									<isprint value="${GiftCertificate.object.recipientEmail}" />
								</span>
							</isif>
						</div>

						<div class="attribute">
							<isif condition="${GiftCertificate.object.senderName!=null}">
								<span class="label">
									${Resource.msg('global.from','locale',null)}:
								</span>
								<span class="value">
									<isprint value="${GiftCertificate.object.senderName}" />
								</span>
							</isif>
						</div>


						<iscomment>product list info</iscomment>
						<isif condition="${GiftCertificate.object.productListItem != null}">
							<span class="product-list-item">
								<isif condition="${GiftCertificate.object.productListItem.list.type == dw.customer.ProductList.TYPE_WISH_LIST}">
									<a href="${URLUtils.url('Wishlist-ShowOther','WishListID', GiftCertificate.object.productListItem.list.ID)}" title="${Resource.msg('cart.wishlistitem','checkout',null)}">
										${Resource.msg('product.display.wishlistitem','product',null)}
									</a>
								<iselseif condition="${GiftCertificate.object.productListItem.list.type == dw.customer.ProductList.TYPE_GIFT_REGISTRY}">
									<a href="${URLUtils.https('GiftRegistryCustomer-Show','ID', GiftCertificate.object.productListItem.list.ID)}" title="${Resource.msg('cart.registryitem','checkout',null)}">
										${Resource.msg('cart.registryitem','checkout',null)}
									</a>
								</isif>
							</span>
						</isif>

						<a href="${URLUtils.url('GiftCert-Edit','GiftCertificateLineItemID', GiftCertificate.object.UUID)}" title="${Resource.msg('global.editdetails','locale',null)}" >${Resource.msg('global.editdetails','locale',null)}</a>
					</td>

					<td class="item-price"><!-- empty --></td>

					<td class="item-quantity">
                        <div class="mobile-label only-for-mobile">
                           ${Resource.msg('global.qty','locale',null)}
                        </div>
                        <div class="quantity-value">
                           1
                        </div>
					</td>

					<td class="item-total">
                        <div class="mobile-label only-for-mobile">
                           ${Resource.msg('global.subtotal','locale',null)}
                        </div>

                        <div class="price-total">
                           <isprint value="${GiftCertificate.object.price}" />
                        </div>
					</td>

					<isif condition="${!readonly}">
    					<td class="item-remove">

    						<button class="xlt-removeBasketItem button button-icon button-transparent button-remove-item" type="submit" value="${Resource.msg('global.remove','locale',null)}" name="${GiftCertificate.deleteGiftCertificate.htmlName}">

    							<i class="fa fa-times-circle"><!-- icon --></i>
    						</button>
    					</td>
                    </isif>
				</tr>

			</isloop>

		</isloop>


		<iscomment>Coupons</iscomment>
		<isloop items="${pdict.CurrentForms.cart.coupons}" var="FormCoupon" status="loopstateCoupons">
		<isif condition="${FormCoupon.object.applied}">
			<tr class="cart-row cart-row-coupons clearfix hidden">
				<td class="item-image item-coupon-image"><!-- empty --></td>

				<td class="item-details item-coupon-details" colspan="3">

					<!---<div class="name">${Resource.msg('cart.coupon','checkout',null)}</div>--->

		           <div class="cart-coupon">
						<isif condition="${FormCoupon.object.valid}">
						<isif condition="${FormCoupon.object.applied}">
                            <div class="name">
                                <span class="label">${Resource.msg('cart.couponcode','checkout',null)}</span>
                                <span class="value">
                                    <isprint value="${FormCoupon.object.couponCode}" />
                                </span>
                            </div>

                            <isloop items="${FormCoupon.object.priceAdjustments}" var="Promo" status="loopstate">
                                <div class="coupon-discount">
                                    <span class="label">
                                        <isprint value="${Promo.lineItemText}" />
                                    </span>
                                    <span class="value">(<isprint value="${Promo.price}" />)</span>
                                </div>
                            </isloop>  
                            </isif>                        
						</isif>  
		            </div> 
				</td>
 
				<td class="item-total item-coupon-total">
					<isif condition="${FormCoupon.object.applied}">
						<div class="coupon-applied">${Resource.msg('global.applied','locale',null)}</div>
					</isif>
				</td>
				<isif condition="${FormCoupon.object.applied}">
                <isif condition="${!readonly}">
    				<td class="item-remove item-coupon-remove">
    					<button class="button button-icon button-transparent" type="submit" value="${Resource.msg('global.remove','locale',null)}" name="${FormCoupon.deleteCoupon.htmlName}">
    						<i class="fa fa-times-circle"><!-- icon --></i>
    					</button>
    				</td>
                </isif>
                </isif>

			</tr>
		</isif>
		</isloop>

		<iscomment>Bonus discount line items</iscomment>

		<isloop items="${pdict.Basket.bonusDiscountLineItems}" var="bonusDiscountLineItem" status="loopstate">


			<iscomment>Display appropriate text based on status of bonus discount and number of selected bonus products.</iscomment>
			<isif condition="${bonusDiscountLineItem.getBonusProductLineItems().size() > 0}">
				<isif condition="${bonusDiscountLineItem.getMaxBonusItems() > 1}">
					<isset name="bonusButtonText" value="${Resource.msg('cart.updatebonusproducts','checkout',null)}" scope="page" />
				<iselse/>
					<isset name="bonusButtonText" value="${Resource.msg('cart.updatebonusproduct','checkout',null)}" scope="page" />
				</isif>
			<iselse/>
				<isif condition="${bonusDiscountLineItem.getMaxBonusItems() > 1}">
					<isset name="bonusButtonText" value="${Resource.msg('cart.selectbonusproducts','checkout',null)}" scope="page" />
				<iselse/>
					<isset name="bonusButtonText" value="${Resource.msg('cart.selectbonusproduct','checkout',null)}" scope="page" />
				</isif>
			</isif>

			<tr class="cart-row cart-row-bonus clearfix">

				<td colspan="3" class="bonus-item-details">
					<div class="bonus-item-promo-name"><isprint value="${bonusDiscountLineItem.getPromotion().getName()}" /></div>
					<div class="bonus-item-promo-callout"><isprint value="${bonusDiscountLineItem.getPromotion().getCalloutMsg()}" encoding="off"/></div>
					<div class="bonus-item-details">
						<isprint value="${bonusDiscountLineItem.getPromotion().getDetails()}" />
						${StringUtils.format(Resource.msg('cart.bonusmaxitems','checkout',null), bonusDiscountLineItem.getMaxBonusItems())}
						${StringUtils.format(Resource.msg('cart.bonusnumselected','checkout',null),bonusDiscountLineItem.getBonusProductLineItems().size())}
					</div>
				</td>

				<td <isif condition="${!readonly}">colspan="3"<iselse/>colspan="2"</isif> class="bonus-item-actions">
					<a class="select-bonus" href="${URLUtils.url('Product-GetBonusProducts','bonusDiscountLineItemUUID', bonusDiscountLineItem.UUID)}" title="${bonusButtonText}">
						${bonusButtonText}
					</a>
				</td>
			</tr>

			<iscomment>
				Display the selected bonus products for this bonus discount line item.
				This requires looping through all form items and matching by UUID.

				Note: we display these bonus products outside of any shipment even
				though each one is contained in a shipment. This display does not make
				sense in multiple-shipment scenario.
			</iscomment>

			<isloop items="${pdict.CurrentForms.cart.shipments}" var="Shipment" status="loopstate">

				<isloop items="${Shipment.items}" alias="FormLi" status="loopstate">

					<isset name="lineItem" value="${FormLi.object}" scope="page" />

					<isif condition="${lineItem.getBonusDiscountLineItem() != null && lineItem.getBonusDiscountLineItem().getUUID().equals(bonusDiscountLineItem.getUUID())}">

						<tr class="cart-row clearfix BBB">
							<td class="item-image">
								<isproductimage product="${lineItem.product}" imgsize="cart" lazyloading="${false}" />
							</td>

							<td class="item-details">
								<iscomment>Call module to render product</iscomment>
								<isdisplayliproduct p_productli="${lineItem}" p_formli="${FormLi}" p_editable="${false}" p_hideprice="${true}" p_hidepromo="${true}" />
								<div class="bonusproducts">
									<a href="${URLUtils.url('Product-GetBonusProducts','bonusDiscountLineItemUUID', bonusDiscountLineItem.UUID)}" title="${bonusButtonText}">
										${bonusButtonText}
									</a>
								</div>

								<iscomment>Product Existence and Product Availability</iscomment>
								<isif condition="${lineItem.product == null}">
									<span class="not-available">
										${Resource.msgf('cart.removeditem','checkout',null,dw.system.System.preferences.custom['raExpirationTimeDwr']) }
									</span>
								<iselse/>
									<isset name="product" value="${lineItem.product}" scope="page" />
									<isset name="quantity" value="${pdict.Basket.getAllProductQuantities().get(lineItem.product).value}" scope="page" />
									<isinclude template="checkout/cart/cartavailability" />
								</isif>
							</td>

							<td class="item-price">
								${new dw.value.Money(0.0, session.getCurrency().symbol)}
							</td>

							<td class="item-quantity">
                               <div class="mobile-label only-for-mobile">
                                   ${Resource.msg('global.qty','locale',null)}
                               </div>
                               <div class="quantity-value">
                                   <isprint value="${lineItem.quantity}" />
                               </div>
							</td>

							<td class="item-total">
								<div class="bonus-item">
									${Resource.msg('global.bonus','locale',null)}
								</div>
							</td>

                            <isif condition="${!readonly}">
    							<td class="item-remove">

    								<button class="xlt-removeBasketItem button button-icon button-transparent button-remove-item" type="submit" value="${Resource.msg('global.remove','locale',null)}" name="${FormLi.deleteProduct.htmlName}">

    									<i class="fa fa-times-circle"><!-- icon --></i>
    								</button>
    							</td>
                            </isif>

						</tr>


						<iscomment>Bundles</iscomment>
						<isif condition="${lineItem.bundledProductLineItems.size() > 0}">
							<isloop items="${lineItem.bundledProductLineItems}" var="bli">

								<iscomment>same color</iscomment>
								<tr class="cart-row cart-row-bundle clearfix">
									<td class="item-image"><!-- empty --></td>

									<td class="item-details">
										<isif condition="${bli.product != null}">
											<a href="${URLUtils.https('Product-Show','pid', bli.productID)}" title="${bli.productName}">
												<isif condition="${bli.product.getImage('small',0) != null}">
													<img src="${bli.product.getImage('small',0).getURL()}" alt="${bli.product.getImage('small',0).alt}" title="${bli.product.getImage('small',0).title}" />
												<iselse/>
													<img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${bli.productName}" title="${bli.productName}" />
												</isif>
											</a>
										<iselse/>
											<img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${bli.productName}" title="${bli.productName}" />
										</isif>

										<div class="name">
											<a href="${URLUtils.https('Product-Show','pid', bli.productID)}" title="${bli.productName}">
												<isprint value="${bli.lineItemText}" />
											</a>
										</div>

										<div class="sku">
											<span class="label">
												${Resource.msg('global.itemno','locale',null)}
											</span>
											<span class="value">
												<isprint value="${bli.productID}" />
											</span>
										</div>

										${Resource.msg('global.included','locale',null)}

										<iscomment>Product Existence and Product Availability</iscomment>
										<isif condition="${!bli.bonusProductLineItem}">
											<isif condition="${bli.product == null}">
												<span class="not-available">
													${Resource.msgf('cart.removeditem','checkout',null,dw.system.System.preferences.custom['raExpirationTimeDwr'] )}
												</span>
												here1
											<iselse/>
												<isset name="product" value="${bli.product}" scope="page" />
												<isset name="quantity" value="${pdict.Basket.getAllProductQuantities().get(bli.product).value}" scope="page" />
												<isinclude template="checkout/cart/cartavailability" />
											</isif>
										</isif>
									</td>
									<td class="item-price"><!-- empty --></td>
									<td class="item-quantity">
                                        <div class="mobile-label only-for-mobile">
                                            ${Resource.msg('global.qty','locale',null)}
                                        </div>
                                        <div class="quantity-value">
								            <isprint value="${bli.quantity}" />
                                        </div>
									</td>

									<td class="item-total"><!-- empty --></td>

									<isif condition="${!readonly}">
                                        <td class="item-remove"><!-- empty --></td>
                                    </isif>
								</tr>
							</isloop>
						</isif>
					</isif>
				</isloop>
			</isloop>
			<iscomment>Done looping selected bonus products.</iscomment>
		</isloop>
		<iscomment>Done looping selected bonus discount line items.</iscomment>			
            <isloop items="${pdict.Basket.priceAdjustments}" var="priceAdjustment" status="cliloopstate">
               <tr class="cart-row cart-row-promo clearfix hidden">
                   <td class="item-image">
                       <!-- empty -->
                   </td>
                   <td <isif condition="${!readonly}">colspan="5"<iselse/>colspan="4"</isif> class="item-promo">
                       <div class="promo-container clearfix">
                           <isif condition="${priceAdjustment.promotionID === 'TJC Credit'}">
                            <div class="label">${Resource.msg('cart.tjccreditdiscount','checkout',null)}</div>
                           <iselse>
                            <div class="label">${Resource.msg('cart.orderdiscount','checkout',null)}</div>
                           </isif>
                           <div class="value">
                               <isprint value="${priceAdjustment.promotion.calloutMsg}" encoding="off" />
                           </div>
                       </div>
                   </td>
               </tr>
            </isloop>
	</tbody>
</table>