<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules" />

<isdecorate template="pagetypes/pt_cart">

    <isscript>
    	var isTJCPlusContext = require("~/cartridge/scripts/modules/tjcplus/TJCPlusHelper").containsTJCPlusProduct(pdict.Basket).containsTJCPlusProduct;
    	
		var mentionMeParameters = {
            method: 'refereefind',
            situation: 'checkout',
            implementation: 'link'
        };
        
		if (!empty(pdict.CurrentCustomer.profile)) {
        	let profile = pdict.CurrentCustomer.profile;
        	mentionMeParameters.email = profile.email;
            mentionMeParameters.customer_id = profile.custom.amsCustomerId;
            mentionMeParameters.firstname = pdict.CurrentCustomer.profile.firstName ? pdict.CurrentCustomer.profile.firstName : " ";
            mentionMeParameters.surname = pdict.CurrentCustomer.profile.lastName ? pdict.CurrentCustomer.profile.lastName : " ";
        }
    </isscript> 
    <isibmpageviewtag type="mapping" arguments="${'cart'}"/>

    <isif condition="${!empty(pdict.CurrentSession.custom.liveTVBidTag)}">
        <script type="text/javascript">${pdict.CurrentSession.custom.liveTVBidTag}</script>
        <isscript>pdict.CurrentSession.custom.liveTVBidTag = null;</isscript>
    </isif>
    <isif condition="${!empty(pdict.Basket) && !empty(pdict.Basket.productLineItems)}">
        <isibmshop5tag productlineitems="${pdict.Basket.productLineItems}"/>
    </isif>
    
	<isinclude template="util/reporting/ReportBasket.isml" />	
	<isset name="enableCheckout" value="${pdict.EnableCheckout}" scope="page" />
	<isset name="isCartPage" value="${true}" scope="page" />

	<div class="cart">
		<!---<isslot id="cart-banner" description="Banner for Cart page"	context="global" />--->

		<iscomment>Display selected bonus products Order Promotion message</iscomment>
		<isif condition="${!(pdict.Basket == null)}">
			<isloop items="${pdict.Basket.bonusDiscountLineItems}"	var="bonusDiscountLineItem">
				<isif condition="${bonusDiscountLineItem.getPromotion().getPromotionClass() == dw.campaign.Promotion.PROMOTION_CLASS_ORDER && bonusDiscountLineItem.getBonusProductLineItems().size() == 0}">
					<div class="bonus-item-promo">
	
						<iscomment>Display appropriate text based on status of bonus discount and number of selected bonus products.</iscomment>
						<isif condition="${bonusDiscountLineItem.getBonusProductLineItems().size() > 0}">
							<isif condition="${bonusDiscountLineItem.getMaxBonusItems() > 1}">
								<isset	name="bonusButtonText"	value="${Resource.msg('cart.updatebonusproducts','checkout',null)}"	scope="page" />
							<iselse/>
								<isset name="bonusButtonText" value="${Resource.msg('cart.updatebonusproduct','checkout',null)}" scope="page" />
							</isif>
						<iselse/>
							<isif condition="${bonusDiscountLineItem.getMaxBonusItems() > 1}">
								<isset name="bonusButtonText" value="${Resource.msg('cart.selectbonusproducts','checkout',null)}" scope="page" />
							<iselse/>
								<isset name="bonusButtonText" value="${Resource.msg('cart.selectbonusproduct','checkout',null)}" scope="page" />
							</isif>
						</isif>
						<div class="bonus-item-details">
							<div class="bonus-item-promo-name"><isprint value="${bonusDiscountLineItem.getPromotion().getName()}" /></div>
							<div class="bonus-item-promo-callout"><isprint value="${bonusDiscountLineItem.getPromotion().getCalloutMsg()}" encoding="off"/></div>

							<div class="bonus-item-info">
								<isprint value="${bonusDiscountLineItem.getPromotion().getDetails()}" />
								${StringUtils.format(Resource.msg('cart.bonusmaxitems','checkout',null),bonusDiscountLineItem.getMaxBonusItems())}
								${StringUtils.format(Resource.msg('cart.bonusnumselected','checkout',null),bonusDiscountLineItem.getBonusProductLineItems().size())}
								<a class="select-bonus" href="${URLUtils.url('Product-GetBonusProducts','bonusDiscountLineItemUUID', bonusDiscountLineItem.UUID)}" title="${bonusButtonText}">${bonusButtonText}</a>
							</div>

						</div>
					</div>
				</isif>
			</isloop>
			
			<div id="bonus-product-dialog" class="bonus-product-dialog" style="display:none;"><!--  empty --></div>
		</isif>	

		<isif condition="${!empty(pdict.CustomCheckoutErrorRedirectMessage)}">
			<div class="error-form">${pdict.CustomCheckoutErrorRedirectMessage}</div>
		</isif>
		<isif condition="${pdict.Basket == null || (empty(pdict.Basket.productLineItems) && empty(pdict.Basket.giftCertificateLineItems))}">
			<div class="cart-empty">
				<iserrormessage form="${pdict.CurrentForms.cart}"/>
				
				<isslot id="cart-empty" description="Empty Cart page slot." context="global" />             
                <isif condition="${pdict.Basket == null || (empty(pdict.Basket.productLineItems) && empty(pdict.Basket.giftCertificateLineItems))}">
					<div class="cart-recommendations-section">
						<div class="cart-section cart-recommendations">
			                 <isinclude template="recommendations/cartrecommendations"/>
			            </div>
			            <div class="cart-section cart-lastvisited">
			                <isinclude url="${URLUtils.url('Product-IncludeLastVisited')}"/>
			            </div>
		            </div>
				</isif>
                <!---<isslot id="cart-empty-footer" description="Empty Cart Footer page slot." context="global" />--->
			</div>
		<iselse/>                        

            <isscript>
				importScript( "cart/BudgetPayUtils.ds" );
				var budgetPay = BudgetPayUtils.getInstallments(pdict.Basket),
					budgetPayAvailable = !empty(budgetPay) && budgetPay.available,
					budgetPayEligible = BudgetPayUtils.isEligible(pdict.Basket, pdict.CurrentCustomer);
				
			</isscript>

            <iserrormessage form="${pdict.CurrentForms.cart}"/>              
	                
            
            <div class="cart-wrapper">
				<div class="cart-shopping-info">
					
					<div class="page-title">
						<span class="page-title-inner">
							${Resource.msg('cart.title','checkout',null)}
						</span>
						
						<iscomment>Starting of Delivery Meter</iscomment>
						<isif condition="${dw.campaign.ABTestMgr.isParticipant('CartMeterChanges', 'CartMeterChanges')}">
							<isset name="bannerCustGrps" value="${dw.system.Site.current.preferences.custom.cartCustomerGroup}" scope="page" />
							<isif condition="${!empty(bannerCustGrps)}"> 
								<isscript>
									var isPartofCustomerGrp = false;
									var cusomterGrps = bannerCustGrps.split(',');
									for each(var cusomterGrp in cusomterGrps){
										if(pdict.CurrentCustomer.isMemberOfCustomerGroup(cusomterGrp)){
											isPartofCustomerGrp = true;
											break;
										}
									}
								</isscript>
								<isif condition="${isPartofCustomerGrp}"> 
									<iscontentasset aid="cart_deliveryslider_customergrps"/>
								</isif>
							
							<iselseif condition="${('isTJCPlusMember' in pdict.CurrentCustomer.profile.custom && pdict.CurrentCustomer.profile.custom.isTJCPlusMember)}">
								<iscontentasset aid="cart_deliveryslider_tjcplus"/>
							<iselse>
								<isscript>
									var deliveryMeterThreshold = dw.system.Site.current.preferences.custom.deliveryMeterThreshold;
									var totalCartValue = pdict.Basket.getAdjustedMerchandizeTotalPrice(false).add(pdict.Basket.giftCertificateTotalPrice);
									var deliveryMeterThresholdBalance = (deliveryMeterThreshold - totalCartValue).toFixed(2);
									var deliveryMeterThresholdPercentage, deliveryMeterThresholdBalancePercentage, deliveryMeterQualified = false;
									if(deliveryMeterThresholdBalance > 0){
										deliveryMeterThresholdPercentage = (deliveryMeterThresholdBalance/deliveryMeterThreshold)*100;
										deliveryMeterThresholdBalancePercentage = (100 - deliveryMeterThresholdPercentage);
										deliveryMeterQualified = false;
									}else{
										deliveryMeterThresholdPercentage = 0;
										deliveryMeterThresholdBalancePercentage = 100;
										deliveryMeterQualified = true;
									}
								</isscript>
								<div class="delivery-meter <isif condition="${deliveryMeterQualified}">qualified</isif>">
									<table>
										<tr class="no-border">
											<td class="delivery-meter-image">
												<img src="${URLUtils.staticURL('/images/delivery-icon.png')}" alt="TJC -Delivery Icon" title="TJC -Delivery Icon" width="70px"/>
											</td>
											<td class="delivery-meter-detail">
												<span class="delivery-meter-progress-detail">
													<isif condition="${!deliveryMeterQualified}">
														Spend <span class="delivery-meter-balance">${Resource.msg('currency.pound','checkout',null)}${deliveryMeterThresholdBalance}</span> more for FREE standard delivery
													<iselse/>
														You've qualified for FREE standard delivery!
													</isif>
												</span>
												<div class="delivery-meter-progress-bar">
													<div class="delivery-meter-progress-bar-progress <isif condition="${deliveryMeterQualified}">completed</isif>" style="width:${deliveryMeterThresholdBalancePercentage}%"></span>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</isif>
						</isif>
						<iscomment>Ending of Delivery Meter</iscomment>
						
					</div>
                    
					
					<div class="cart-shopping-table">
						<!---<isinclude template="checkout/components/approachingdiscounts"/>--->
						<form action="${URLUtils.httpContinue()}" method="post"	name="${pdict.CurrentForms.cart.htmlName}" id="cart-items-form">
							<fieldset>
								<iscomment>This button is hidden but required to ensure that update cart is called whenever the "enter" key is pressed in an input field</iscomment>
								<button class="visually-hidden" type="submit" value="${pdict.CurrentForms.cart.updateCart.htmlName}" name="${pdict.CurrentForms.cart.updateCart.htmlName}"></button>
								<!--- Cart Table --->
			                    <iscarttable />                     
			               </fieldset>
			            </form>
		            </div>
				</div>
				<div class="cart-summary">
					<div class="cart-summary-inner">
					<div class="cart-coupon-container clearfix">
						<iscomment>
		                <div class="cart-continue not-for-mobile">
		                    <form class="cart-action-continue-shopping" action="${URLUtils.httpContinue()}" method="post" name="${pdict.CurrentForms.cart.dynamicHtmlName}" id="continue-shopping">
		                        <fieldset>                                
		                            <i class="fa fa-chevron-left "><!-- icon --></i>
		                            <button class="xlt-continueShopping button button-text" type="submit" value="${Resource.msg('global.continueshopping','locale',null)}" name="${pdict.CurrentForms.cart.continueShopping.htmlName}">
		                                ${Resource.msg('global.continueshopping','locale',null)}
		                            </button> 
		                        </fieldset>
		                    </form>  
		                </div>
                		</iscomment>
                		
                		<isslot id="cart-content-summary-top" description="Footer for Cart page" context="global" />
               			
               			<isscript>
							var couponExist = typeof pdict.CurrentForms.cart.coupons === 'object' && !empty(pdict.CurrentForms.cart.coupons);
						</isscript>
               			
                		
                		<div class="coupon-and-cart-bottom">
	                		<div class="cart-coupon-code-button">
								<span class="text">Have a promotional code?</span>
								<span class="symbol-down"></span>
							</div> 
                
			               	<div class="cart-coupon-code"> 
			                    <isset name="hasError" value="${(pdict.CouponError && pdict.CouponError=='COUPON_CODE_MISSING') || (pdict.CouponError && pdict.CouponError=='NO_ACTIVE_PROMOTION') || (pdict.CouponStatus != null && pdict.CouponStatus.error)}" scope="page" />
			                    <form action="${URLUtils.httpContinue()}" method="post" name="${pdict.CurrentForms.cart.htmlName}" id="cart-coupon-form">
			                        <div class="form-row optinal-field form-row-no-margin form-row-auto form-row-no-indent<isif condition="${hasError}"> validation-error</isif>">                        
			                            <div class="field">
											<input class="input-text" type="text" name="${pdict.CurrentForms.cart.couponCode.htmlName}" id="${pdict.CurrentForms.cart.couponCode.htmlName}" class="input-text" placeholder ="${Resource.msg('cart.entercouponcode','checkout',null)}" />			                                
			                              <label  class="label new-label" for="${pdict.CurrentForms.cart.couponCode.htmlName}">Promotional Code</label>
			                            </div>
			                            <button type="submit" value="${pdict.CurrentForms.cart.addCoupon.htmlName}" name="${pdict.CurrentForms.cart.addCoupon.htmlName}" id="add-coupon" class="button button-coupon" >
			                                ${Resource.msg('global.apply','locale',null)}
			                            </button> 
			                        </div>
			                        <div class="error-msg"> 
	                                	<span class ="couponstatus" style="display:none">${pdict.CouponStatus}</span>
			                             <isif condition="${pdict.CouponError && pdict.CouponError =='COUPON_CODE_MISSING'}">
											${Resource.msg('cart.COUPON_CODE_MISSING','checkout', null)}																
											<iselseif condition="${pdict.CouponError}"/>
												<span class="already" style="display:none">${Resource.msg("cart.COUPON_CODE_ALREADY_REDEEMED", "checkout", null)}</span>
												<span class="active" style="display:none">${Resource.msgf("cart.NO_ACTIVE_PROMOTION", "checkout", "", pdict.CurrentForms.cart.couponCode.htmlValue)}</span>
											<iselseif condition="${pdict.CouponStatus != null && pdict.CouponStatus.error}">
			                                 	${Resource.msgf('cart.' + pdict.CouponStatus.code,'checkout', null, pdict.CurrentForms.cart.couponCode.htmlValue)}
										</isif>						
	                              </div>
			                    </form>
							</div>
						</div>
						
						<isif condition="${couponExist}">
							<form action="${URLUtils.httpContinue()}" method="post"	name="${pdict.CurrentForms.cart.htmlName}" class="xlt-coupon-items-list">
								<div class="couponsList">
									<isloop items="${pdict.CurrentForms.cart.coupons}" var="FormCoupon" status="loopstateCoupons">
										<div class="crow">
											<span class="couponStatus"><isif condition="${FormCoupon.object.applied === true}"><span class="icons couponApplied"></span><iselse/><span class="icons couponNotApplied"></span></isif></span>
											<span class="couponCode"><isprint value="${FormCoupon.object.couponCode}" /></span>
											<input type="hidden" name="${FormCoupon.deleteCoupon.htmlName}" />
											<form action="${URLUtils.httpContinue()}" method="post"	name="${pdict.CurrentForms.cart.htmlName}" id="cart-items-form">
												<fieldset>
													<button class="button button-icon button-transparent hidden hiddenActionButton" type="submit" value="${Resource.msg('global.remove','locale',null)}" name="${FormCoupon.deleteCoupon.htmlName}">
							    					</button>                  
								               </fieldset>
								            </form>
								            <div class="hiddenActionButtonTrigger"><span class="icons couponAction "></span></div>
										</div>
									</isloop>
								</div>
							</form>
						</isif>
						
						
					</div>
					
					

            		<iscomment> <div id="mmWrapper" class="mm-wrapper-link"></div> </iscomment>
                    
		            <div class="cart-footer clearfix">		                
		                <div class="cart-footer-right">
		                    <isscript>
		                        var approachingShippingDiscounts = dw.campaign.PromotionMgr.getDiscounts(pdict.Basket).getApproachingShippingDiscounts(pdict.Basket.defaultShipment).iterator(),
		                            approachingFreeShippingDiscount;
		                        while(approachingShippingDiscounts.hasNext()) {
		                            var approachingDiscount = approachingShippingDiscounts.next();
		                            if (approachingDiscount.discount.type == dw.campaign.Discount.TYPE_FREE){
		                                approachingFreeShippingDiscount = approachingDiscount;
		                                break;
		                            }
		                        }
		                    </isscript>                   		                    
		                	
		                    <isset name="reachedPnpMax" value="${pdict.Basket.defaultShipment.getStandardShippingLineItem().custom.reachedPnpMax}" scope="page" />
		                    <isif condition="${!reachedPnpMax}">
			                    <isif condition="${!empty(approachingFreeShippingDiscount)}"> 
			                     	<div class="free-delivery-hint">
			                       	 	 ${Resource.msgf('cart.free.delivery-hint','checkout',null, approachingFreeShippingDiscount.distanceFromConditionThreshold.toFormattedString())}
			                    	 </div>
			                    </isif>
		                  	</isif>
		                    <input type="hidden" name="${pdict.CurrentForms.cart.updateCart.htmlName}" value="${pdict.CurrentForms.cart.updateCart.htmlName}"/>
		                    
				                    <div class="cart-order-totals">
				                        <form action="${URLUtils.httpContinue()}" method="post" name="${pdict.CurrentForms.cart.htmlName}" id="cart-shippingmethod-form">
				                            <iscartordertotals p_lineitemctnr="${pdict.Basket}" p_totallabel="${Resource.msg('global.estimatedtotal','locale',null)}"/>
				                            <input type="hidden" name="${pdict.CurrentForms.cart.updateShippingMethod.htmlName}" value="true"/>
				                        </form>		                        
				                    </div>
		                    
				                    <div class="cart-checkout sticky-cart-checkout">
			                            <iscomment>continue shop url is a non-secure but checkout needs a secure and that is why separate forms!</iscomment>
			                            <isset name="checkoutLoginClass" value="${!pdict.CurrentCustomer.isAuthenticated() ? 'checkout-login-class' : ''}" scope="page" />
			                            <form class="xlt-continueCheckout" action="${URLUtils.httpsContinue()}" method="post" name="${pdict.CurrentForms.cart.dynamicHtmlName}" id="checkout-form">
			                                <fieldset>
			                                    <isif condition="${enableCheckout}">
			                                    	<isif condition="${dw.system.Site.current.preferences.custom.LoginPopUpActive}">
				                                        <button class="button button-green button-larger button-wider xlt-continueCheckout ${checkoutLoginClass}" type="submit" value="${Resource.msg('global.checkout','locale',null)}" name="${pdict.CurrentForms.cart.checkoutCart.htmlName}">
				                                            ${Resource.msg('cart.btn.checkout','checkout',null)}
				                                        </button>
			                                        <iselse/>
			                                        	<isif condition="${pdict.CurrentCustomer.isAuthenticated()}">
			                                        		<a class="button button-green button-larger button-wider xlt-continueCheckout ${checkoutLoginClass}" value="${Resource.msg('global.checkout','locale',null)}" name="${pdict.CurrentForms.cart.checkoutCart.htmlName}" href="${dw.web.URLUtils.https('COShippingAndBilling-Start', '', '','key',!empty(request.httpCookies['dwsid'])?request.httpCookies['dwsid'].value.substring(0,10): 'new')}">
			                                        			${Resource.msg('cart.btn.checkout','checkout',null)}
			                                        		</a>
			                                        	<iselse>
			                                        		<a class="button button-green button-larger button-wider xlt-continueCheckout ${checkoutLoginClass}" value="${Resource.msg('global.checkout','locale',null)}" name="${pdict.CurrentForms.cart.checkoutCart.htmlName}" href="${dw.web.URLUtils.https('COShippingAndBilling-Start', 'newSignin', 'newSignin','key',!empty(request.httpCookies['dwsid'])?request.httpCookies['dwsid'].value.substring(0,10): 'new')}">
			                                        			${Resource.msg('cart.btn.checkout','checkout',null)}
			                                        		</a>
			                                        	</isif>
			                                        </isif>
			                                    <iselse/>
			                                        <button class="button button-green button-larger button-wider xlt-continueCheckout ${checkoutLoginClass}" disabled="disabled" type="submit" value="${Resource.msg('global.checkout','locale',null)}"   name="${pdict.CurrentForms.cart.checkoutCart.htmlName}">
			                                            ${Resource.msg('cart.btn.checkout','checkout',null)}
			                                        </button>
			                                    </isif>
			                                </fieldset>
			                            </form>
			                        </div>
			                        
			                        <isif condition="${budgetPayAvailable && !budgetPayEligible}">
				                    	<isinclude template="components/budgetpay/budgetpaycarterror"/>
				                    <iselseif condition="${!empty(customer.profile) && !customer.profile.custom.budgetPayAvailable}"/>
				                    	<isif condition="${budgetPayAvailable}">
				                   		 	<isinclude template="components/budgetpay/budgetpaycarterror"/>
				                   		 </isif>
				                    </isif>
			                        
			                        
			                        <div class="cart-footer-left">
					                    <isif condition="${budgetPayAvailable && budgetPayEligible}">
					                		<isinclude template="components/budgetpay/budgetpaycartmessage"/>
					                    </isif>
					                    <isslot id="cart-content-left" description="Content top left to the order totals section." context="global" />
					                    	<iscontentasset aid="cart-content-left-budgetpay" />
					                    <isif condition="${!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('PaypalCreditThreshold')) && pdict.Basket.totalGrossPrice.value > dw.system.Site.getCurrent().getCustomPreferenceValue('PaypalCreditThreshold')}">
					                    	<iscontentasset aid="cart-content-left-paypal" />
					                    </isif>
					                    <isslot id="cart-content-left-2" description="Content bottom left to the order totals section." context="global" />
					                </div>
					                
					                
	                        
	                        	</div>		            		                    
																			
		                	</div>
		                
		                </div>
		                
		                
		                <isset name="redirecttjcplusactivation" value="${pdict.CurrentHttpParameterMap.redirecttjcplusactivation}" scope="page" />
						<isset name="tjcpluspid" value="${pdict.CurrentHttpParameterMap.tjcpluspid}" scope="page" />
							
						<div class="sticky-desktop-check">
							<isinclude url="${URLUtils.url('TJCPlus-ShowSubscription','context','cart','redirecttjcplusactivation', redirecttjcplusactivation, 'tjcpluspid',tjcpluspid)}"/>
						</div>
						
						
						
		                
		            </div>
		            
		            
					
				</div>
			</div>			
		</isif>
	</div>

</isdecorate>