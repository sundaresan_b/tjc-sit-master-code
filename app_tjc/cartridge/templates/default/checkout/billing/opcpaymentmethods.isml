<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment> TEMPLATENAME: opcpaymentmethod.isml </iscomment>
<isinclude template="util/modules"/>

<isif condition="${pdict.OrderTotal > 0}">
	<isscript>
    var site = dw.system.Site.getCurrent();
    var showBudgetPay = !empty(budgetPay) && budgetPay.available && BudgetPayUtils.isEligible(pdict.Basket, pdict.CurrentCustomer);
    var showPayPalCredit = !empty(site.getCurrent().getCustomPreferenceValue('PaypalCreditThreshold')) && pdict.OrderTotal > site.getCurrent().getCustomPreferenceValue('PaypalCreditThreshold');
    var payPalEnabled = !empty(pdict.CurrentSession.custom.paypalDeclineCount) && pdict.CurrentSession.custom.paypalDeclineCount >= 3 ? false : true;
    var showPaysafePayment = site.getCustomPreferenceValue('NetbanxApiType');
    var tjcCreditApplied = !empty(pdict.CurrentForms.billing.tjcCredit.htmlValue);
    var tjcResult = require("~/cartridge/scripts/modules/tjcplus/TJCPlusHelper").containsTJCPlusProduct(pdict.Basket);
    var isTJCPlusContext = !empty(tjcResult) ? tjcResult.containsTJCPlusProduct : false;
  </isscript>
    <fieldset>
		<div class="payment-methods-headline">
			${Resource.msg('billing.selectpayment','checkout',null)}
		</div>
		
		<div class="payment-methods-warning">
			<div class="payment-methods-warning-inner">
				${Resource.msg('billing.selectpayment.warning','checkout',null)} <iscomment>this will be error msg displayed when user clicks on PayNow without selecting any card details</iscomment>
			</div>					
		</div>

		<div class="payment-method-wrapper">
			<isif condition="${site.getCustomPreferenceValue('NetbanxApiType').value === "hosted"} ">
				<div class="payment-method netbanx form-row form-row-no-indent form-row-radio clearfix">
					<div class="field">
						<input class="input-radio netbanx" id="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_netbanx'}" type="radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="Netbanx" title="${Resource.msg('billing.netbanx.title','checkout',null)}"/>
						<label for="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_netbanx'}">
							<span class="label-title">
								<span class="label-title-head">Credit / Debit Card</span>
								<span class="label-title-desc">Use your card to pay</span>
							</span>
							<span class="label-image">
								<img src="${URLUtils.staticURL('/images/payment-creditcard-new.png')}" alt="${Resource.msg('billing.netbanx.title','checkout',null)}">
							</span>
						</label>
					</div>
				</div>
				<div class="payment-method-description netbanx">
			        <iscontentasset aid="payment-method-description-netbanx" />
			    </div>
			</isif>
			<isscript>
				var IsCreditAvailable = false;
				if(pdict.CurrentCustomer.authenticated && pdict.CurrentCustomer.profile.custom.storeCredit > 0)
					IsCreditAvailable = true;
			</isscript>
			<isif condition="${pdict.CurrentCustomer.authenticated === true && !isTJCPlusContext && dw.system.Site.current.preferences.custom.showTJCCredits && IsCreditAvailable}">
				<div class="tjccredit form-row form-row-no-indent clearfix">
	                <isset name="CreditAvailable" value="${pdict.CurrentCustomer.profile.custom.storeCredit}" scope="page" />
					<h3>Account Credit</h3>
					<div class="tjc-credit-details">
					    <p class="above-p-tag"><isprint value="${Resource.msgf('credit.availablecredit','checkout', null, CreditAvailable)}" encoding="off" /></p>
					    <div>
					        <div class="credit-inner">
					            <span class="label">${Resource.msg('product.pound', 'product', null)}</span>
					            <input class="input-text credit-input" id="${pdict.CurrentForms.billing.tjcCredit.htmlName}" type="text" name="${pdict.CurrentForms.billing.tjcCredit.htmlName}" 
					            	data-creditavailable="${CreditAvailable}" 
					            	data-creditapplied="${pdict.CurrentForms.billing.tjcCredit.htmlValue}"
					            	data-subtotal="${pdict.Basket.totalGrossPrice.value}" 
					            	data-paynowlabel="${Resource.msg('billing.paynow','checkout',null)}"
					            	data-applylabel="${Resource.msg('credit.applycredit','checkout',null)}"
					            	autocomplete="off" />
								<button class="button credit-apply-button" type="submit" value="${Resource.msg('credit.applycredit','checkout',null)}" name="${pdict.CurrentForms.billing.applycredit.htmlName}">
									${Resource.msg('shipment.applycredit','checkout',null)}
								</button>
					        </div>
					    </div>
					    <p class="below-p-tag" style="display:none;margin: 0 0 0 0;"><span class="credits-exceed-error-msg" style="display: block;color: #c20000;">Applied credit cannot be more than the available account credit.</span></p>
					    <p class="below-p-tag"><isprint value="${Resource.msgf('credit.footer','checkout',null, pdict.Basket.totalGrossPrice.value)}" encoding="off" /></p>
					    <p style="line-height: 18px;font-size: 12px;margin-top: 10px;">${Resource.msgf('credit.disclaimer', 'checkout', null, null)}</p>
					</div>
				</div>
			</isif>
			
			<isif condition="${site.getCustomPreferenceValue('NetbanxApiType').value === "cardpayments"} ">
				<div class="payment-method paysafe form-row form-row-no-indent form-row-radio clearfix">
					<div class="field">
						<input class="input-radio paysafe paysafe-toggle" id="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_paysafe'}" type="radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="Paysafe" title="${Resource.msg('billing.paysafe.title','checkout',null)}"/>
						<label for="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_paysafe'}">
							<span class="label-title">
								<span class="label-title-head">Credit / Debit Card</span>
								<span class="label-title-desc">Use your card to pay</span>
							</span>
							<span class="label-image">
								<img src="${URLUtils.staticURL('/images/payment-creditcard-new.png')}" alt="${Resource.msg('billing.paysafe.title','checkout',null)}">
							</span>
						</label>
					</div>
					<isif condition="${isTJCPlusContext}">
						<div class="tjcplus-savecard">
							${Resource.msg('billing.tjcplus.savecard','checkout',null)}

							<isif condition="${tjcResult.currentTJCPlusPLI.product.custom.subscriptionDuration == 1}"> 
								${Resource.msg('billing.tjcplus.authorization','checkout',null)}
							</isif>
						</div>
					</isif>
				</div>
			</isif>
			
			<isif condition="${!empty(dw.system.Site.current.preferences.custom.enableApplePayOnCheckout) && dw.system.Site.current.preferences.custom.enableApplePayOnCheckout}">
				<div class="payment-method apple-pay form-row form-row-no-indent form-row-radio clearfix">
					<div class="field">
						<input class="input-radio apple-pay" id="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_applePay'}" type="radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="ApplePay" title="${Resource.msg('billing.applepay.title','checkout',null)}"/>
						<label for="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_applePay'}">
							<span class="label-title">
								<span class="label-title-head">Apple Pay</span>
								<span class="label-title-desc">Use your Apple Pay account to pay</span>
							</span>
							<span class="label-image">
								<img src="${URLUtils.staticURL('/images/payment-applepay.png')}" alt="${Resource.msg('billing.applepay.title','checkout',null)}">
							</span>
						</label>
					</div>
				</div>
				<div class="applePayDetails">
					<isinclude template="checkout/singleshipping/opcApplePay"/>
				</div>
			</isif>
			
			<isif condition="${!empty(budgetPay) && budgetPay.available && pdict.CurrentCustomer.authenticated ? true : false}">
				<div class="budgetpayDetails BudgetPay">
					<table class="order-totals-table">
						<isopcbudgetpayordertotals p_budgetpay="${!empty(budgetPay) && budgetPay.available && pdict.CurrentCustomer.authenticated ? budgetPay : null}" />
					</table>
				</div>
			</isif>
			<isinclude template="checkout/billing/opcPaysafecard" />
			<div class="billing checkout-addresses-form" style="display:none;">
				<isinclude template="checkout/singleshipping/billingaddress"/>
				
				<div class="payment-method-description budgetpay">
			        <iscontentasset aid="payment-method-description-budgetpay" />
			    </div>
				<div class="payment-method-description paypal">
			        <iscontentasset aid="payment-method-description-paypal" />
			    </div>
				<div class="payment-method-description paypal-credit">
			        <iscontentasset aid="payment-method-description-paypalcredit" />
				</div>
			</div>
			
			<isif condition="${showBudgetPay}">
				<isif condition="${customer.profile.custom.budgetPayAvailable}">
					<div class="payment-method budgetpay form-row form-row-no-indent form-row-radio clearfix">
						<div class="field">
							<input class="input-radio paysafe paysafe-toggle" id="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_budgetPay'}" type="radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="BudgetPay" title="${Resource.msg('billing.budgetpay.title','checkout',null)}"/>
							<label for="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_budgetPay'}">
								<span class="label-title">
									<span class="label-title-head">Budget Pay</span>
									<span class="label-title-desc">Pay in instalments with your card</span>
								</span>
								<span class="label-image">
									<img src="${URLUtils.staticURL('/images/payment-budgetpay.png')}" alt="${Resource.msg('billing.budgetpay.title','checkout',null)}">
								</span>
							</label>
						</div>
					</div>
				</isif>
			</isif>
			
			<isif condition="${!isTJCPlusContext}">
				<isif condition="${payPalEnabled}"> 
					<div class="payment-method paypal form-row form-row-no-indent form-row-radio clearfix">
						<div class="field">
							<input class="input-radio paypal" id="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_paypal'}" type="radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="PayPal" title="${Resource.msg('billing.paypal.title','checkout',null)}"/>
							<label for="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_paypal'}">
								<span class="label-title">
									<span class="label-title-head">PayPal</span>
									<span class="label-title-desc">Use your PayPal account to pay</span>
								</span>
								<span class="label-image">
									<img src="${URLUtils.staticURL('/images/payment-paypal.png')}" alt="${Resource.msg('billing.paypal.title','checkout',null)}">
								</span>
							</label>
						</div>
					</div>
				</isif>
				<isif condition="${showPayPalCredit && payPalEnabled}">
					<div class="payment-method paypal-credit form-row form-row-no-indent form-row-radio clearfix">
						<div class="field"> 
							<input class="input-radio paypal-credit" id="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_payPalCredit'}" type="radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="PayPalCredit" title="${Resource.msg('billing.paypalcredit.title','checkout',null)}"/>
							<label for="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName + '_payPalCredit'}">
								<span class="label-title">
									<span class="label-title-head">PayPal Credit</span>
									<span class="label-title-desc">Spread the cost with PayPal</span>
								</span>
								<span class="label-image">
									<img src="${URLUtils.staticURL('/images/payment-paypalcredit.png')}" alt="${Resource.msg('billing.paypalcredit.title','checkout',null)}">
								</span>
								<span style="display:none;"><iscontentasset aid="order-summary-paypal-promotional"/></span>
							</label>
						</div>
					</div>
				</isif>
				<isif condition="${pdict.CurrentSession.custom.isMobileApp}">
					<div class="paypal-details hidden">
						<div class="message-content">
							<iscontentasset aid="payment-method-privacypolicy-paypal" />
						</div>
						<isinputfield formfield="${pdict.CurrentForms.billing.checkoutpp}" type="checkbox" rowclass="form-row-auto form-row-no-indent form-row-no-margin-top checkout-pp"  labelparameter="${dw.web.URLUtils.https('Page-Show', 'cid', 'privacy-policy')}"/>
					</div>
				</isif>
			</isif>
			
			<input type="hidden" name="${pdict.CurrentForms.billing.secureKeyHtmlName}" value="${pdict.CurrentForms.billing.secureKeyValue}"/>
		</div>

		<button class="button button-large button-green paynow hidden" id="nonCardPayNow" type="submit" value="${Resource.msg('billing.paynow','checkout',null)}" name="${pdict.CurrentForms.billing.continue.htmlName}">
			${Resource.msg('billing.paynow','checkout',null)}
		</button>
	</fieldset>
	
	<div class="cart-checkout">
		<iscomment>continue shop url is a non-secure but checkout needs a secure and that is why separate forms!</iscomment>
		<isset name="checkoutLoginClass" value="${!pdict.CurrentCustomer.isAuthenticated() ? 'checkout-login-class' : ''}" scope="page" />
		<form class="xlt-continueCheckout" action="${URLUtils.httpsContinue()}" method="post" name="${pdict.CurrentForms.cart.dynamicHtmlName}" id="checkout-form1">
			<fieldset>
				<isif condition="${enableCheckout}">
					<button class="button button-green button-larger button-wider xlt-continueCheckout ${checkoutLoginClass}" type="submit" value="${Resource.msg('global.checkout','locale',null)}" name="${pdict.CurrentForms.cart.checkoutCart.htmlName}">
						${Resource.msg('addresses.singleshipping.orderButton','checkout',null)}
					</button>
				<iselse/>
					<button class="button button-green button-larger button-wider xlt-continueCheckout ${checkoutLoginClass}" disabled="disabled" type="submit" value="${Resource.msg('global.checkout','locale',null)}"   name="${pdict.CurrentForms.cart.checkoutCart.htmlName}">
						${Resource.msg('addresses.singleshipping.orderButton','checkout',null)}
					</button>
				</isif>
			</fieldset>
		</form>
	</div>
	
	<div class="checkout-summary-hint">
		<iscontentasset aid="checkout-summary-hint" />
	</div>
	
	<div class="checkout-redirect-hint">
        ${Resource.msg('billing.redirect','checkout',null)}
    </div>
    
<iselse/>
	<div class="gift-cert-used form-indent">
		<isif condition="${pdict.gcPITotal>0}">${Resource.msg('billing.giftcertnomethod','checkout',null)}<iselse/>${Resource.msg('billing.zerobalance','checkout',null)}</isif>
		<input type="hidden" name="${selectedPaymentMethodID.htmlName}" value="${dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE}" />
		<input type="hidden" id="noPaymentNeeded" name="noPaymentNeeded" value="true" />
	</div>
</isif> 
