<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isif condition="${!empty(pdict.CacheDeleteTime)}">
    <iscache type="daily" hour="${pdict.CacheDeleteTime.getHours()}" minute="${pdict.CacheDeleteTime.getMinutes()}" varyby="price_promotion"/>
<iselse/>
    <iscache type="relative" hour="24" varyby="price_promotion"/>
</isif>
<isinclude template="util/modules"/>
<iscomment>
    This template is best used via a **remote** include (Product-HitTile) and _not_ local include.
    This template renders a product tile using a product. The following parameters
    must be passed into the template module:

    product         : the product to render the tile for
    showswatches    : check, whether to render the color swatches (default is false)
    showpricing     : check, whether to render the pricing (default is false)
    showpromotion   : check, whether to render the promotional messaging (default is false)
    showrating      : check, whether to render the review rating (default is false)
    showfooter      : check, whether to render Quickview & Likelist (not in footer anymore, but we kept variable name) (default is true)
    showbadge       : check, whether to render the badges (default is true)
</iscomment>
<isset name="isPlp" value="true" scope="page"/>
<isset name="Product" value="${pdict.product}" scope="page"/>
<iscomment>set default values</iscomment>
<isset name="showname" value="${(pdict.showname != null) ? pdict.showname : false}" scope="page"/>
<isset name="showswatches" value="${(pdict.showswatches != null) ? pdict.showswatches : false}" scope="page"/>
<isset name="showpricing" value="${(pdict.showpricing != null) ? pdict.showpricing : false}" scope="page"/>
<isset name="showpromotion" value="${(pdict.showpromotion != null) ? pdict.showpromotion : false}" scope="page"/>
<isset name="showrating" value="${(pdict.showrating != null) ? pdict.showrating : false}" scope="page"/>
<isset name="showfooter" value="${(pdict.showfooter != null) ? pdict.showfooter : true}" scope="page"/>
<isset name="showbadge" value="${(pdict.showbadge != null) ? pdict.showbadge : true}" scope="page"/>

<isif condition="${!empty(Product)}">
    <iscomment>
        Get the colors selectable from the current product master or variant range if we
        need to determine them based on a search result.
    </iscomment>
    <isscript>
        var selectableColors = new dw.util.ArrayList();
        var imageSize = 'medium';
        var PVM = Product.variationModel;
        var colorVarAttr, selectedColor, imageSource, image;
        if (PVM) {
            colorVarAttr = PVM.getProductVariationAttribute('color');
            if (colorVarAttr) {
                selectableColors = PVM.getFilteredValues(colorVarAttr);
            }
            if (Product.variationGroup) {
                imageSource = selectedColor = PVM.getSelectedValue(colorVarAttr);
                if (!imageSource) {
                    if (!PVM.variants.isEmpty()) {
                        imageSource = PVM.defaultVariant;
                        if (imageSource) {
                            selectedColor = PVM.getVariationValue(imageSource, colorVarAttr);
                        }
                    }
                }
            } else if (Product.isMaster() && PVM.defaultVariant) {
                if (colorVarAttr) {
                    imageSource = PVM.defaultVariant;
                    selectedColor = imageSource.variationModel.getSelectedValue(colorVarAttr);
                } else {
                    imageSource = PVM.master;
                }
            } else if (Product.isVariant()) {
                if (colorVarAttr) {
                    imageSource = selectedColor = PVM.getSelectedValue(colorVarAttr);
                    if (!imageSource) {
                        if (!PVM.variants.isEmpty()) {
                            imageSource = PVM.variants[0];
                            selectedColor = imageSource.variationModel.getSelectedValue(colorVarAttr);
                        }
                    }
                }
                else {
                    imageSource = Product;
                }
            } else {
                /* standard product, product set or bundle*/
                imageSource = Product;
            }
        } else {
            imageSource = Product;
        }
        image = imageSource.getImage(imageSize, 0);

        /* Generate link to product detail page: by default it's just the product of the product search hit.
           If a color variation is available, the first color is used as link URL.*/
        var productUrl = URLUtils.url('Product-Show', 'pid', Product.ID);
        if (selectedColor) {
            productUrl = Product.variationModel.urlSelectVariationValue('Product-Show', colorVarAttr, selectedColor);
        }

        var hasBadge = !empty(Product.custom.badges);
        
        var blackFridayCategoryId = dw.system.Site.getCurrent().getCustomPreferenceValue('blackFridayCategoryForBadge'); /*"black-friday-doorbusters";*/
		
		var doorBusterId = dw.system.Site.getCurrent().getCustomPreferenceValue('doorBusterCategoryForBadge'); /*"black-friday-doorbusters";*/
        
        var categoriesArray = Product.categories.toArray();
        
        var isInBlackFridayCategory = isContainedInCategory(blackFridayCategoryId, categoriesArray);
		
		var isDoorBusterCategory = isContainedInCategory(doorBusterId, categoriesArray);
        
        function isContainedInCategory(categoryId, array){
        	for (var i = 0; i < array.length; i++){
        		if (array[i].ID == categoryId){
        			return true;
        		}
        	}
        	return false;
        }

		var productStock;
		if(!empty(Product.availabilityModel.inventoryRecord)){
			productStock = Product.availabilityModel.inventoryRecord.ATS.value;
		}

        var stockRequired = dw.system.Site.getCurrent().getCustomPreferenceValue('stockThresholdToShowBadge'); /*3*/
        
        var lastInStock = false;
        
        if (productStock && stockRequired >= productStock){
        	lastInStock = true;
        }
                
        if (hasBadge == false){
        	if (isInBlackFridayCategory == true || lastInStock == true){
        		hasBadge = true;
        		showbadge = true;
        	}
        }

        /*if (pdict.IsLiveAuctionProduct) {
            productUrl = URLUtils.https('LiveTV-Start');
        }*/
        
        var isRA = false;
    </isscript>
    
    <div class="product-tile<isif condition="${showbadge && hasBadge}"> has-badge</isif><isif condition="${!showfooter}"> reduced-tile</isif>" id="${Product.UUID}" data-itemid="${Product.ID}" data-cgid="${pdict.cgid}">
        <div class="product-tile-top">
            <isif condition="${!pdict.IsLiveAuctionProduct && showfooter}">
            	<a href="${productUrl}" class="quickview button not-for-mobile">
                    ${Resource.msg('product.quickview', 'product', null)}
                </a>
            </isif>
			
            <isif condition="${typeof disableLinks == 'undefined' || !disableLinks}">
            	<a class="product-link" href="${productUrl}" title="${Product.name}" <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('openPDPinnewtabPLP') && !pdict.ishomepage}" >target="_blank"</isif>>
            </isif>
            <isif condition="${showbadge && hasBadge && showfooter}">            
                <isscript>
					var badgeID;             	
                	if(isDoorBusterCategory == true)
                	{
                		 badgeID ="doorbuster";
                	}
                	else if(isInBlackFridayCategory == true)
                	{
                		 badgeID ="blackfriday";
                	}                 	
                	else if(lastInStock == true && Product.custom.badges.length==0)
                	{
                				
                				badgeID = "last-in-stock";                				
                			
                	} 
                	                 	              	
                	else 
                	{                		
                		for(var i=0,len=Product.custom.badges.length;i<len;i++) {
                		
                			var badge = Product.custom.badges[i];
                			
                			if( badge =="freeship")
                			{
                					badgeID = "freeship";
                					break;                					
                			}   
                			else  
                			{
                				
                				badgeID =  Product.custom.badges[0];
                				if(badgeID == 'lastinstock')
                				{
                					badgeID = "last-in-stock";
                				}
                				if(badgeID == '48hourdelivery')
                				{
                					badgeID = "48-HOUR-DELIVERY";
                				}
                			}              	
                	}                		
                	}             	
                </isscript>  
                <div class="product-badge ${badgeID}">
                    <img src="${URLUtils.staticURL('/images/badge-' + badgeID + '.png')}" alt="${badgeID}">
                </div>
            </isif>

            <isif condition="${!empty(Product.custom.installments) && Product.custom.installments > 0}">
                <div class="budgetpay">
                    <img src="${URLUtils.staticURL('/images/budgetpay-logo.png')}">
                </div>
            <iselseif condition="${!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('PaypalCreditProductPriceThreshold'))}">
                <isscript>
                    var productPrice = 0;;

                    if (Product.master) {
                        productPrice = Product.variants[0].getPriceModel().getPrice().value;
                    } else {
                        productPrice = Product.getPriceModel().getPrice().value;
                    }
                </isscript>
                <isif condition="${productPrice >= dw.system.Site.getCurrent().getCustomPreferenceValue('PaypalCreditProductPriceThreshold')}">
                    <div class="paypal-credit">
                        <img src="${URLUtils.staticURL('/images/paypal-credit-logo.png')}">
                    </div>
                </isif>
            </isif>

            <isinclude template="product/components/producttile_top">

            <isif condition="${showswatches}">
                <iscomment>Render the color swatch secion for a product. We show color swatches for color variations known to the product master.</iscomment>
                <isif condition="${!empty(selectableColors) && selectableColors.size() > 1 && !empty(colorVarAttr)}">
                    <div class="product-swatches">
                        <iscomment>render a link to the palette and hide the actual palette if there are more than five colors contained</iscomment>
                        <isif condition="${selectableColors.size() > 5}">
                            <a class="product-swatches-all">${Resource.msg('productresultarea.viewallcolors','search',null)} (<isprint value="${selectableColors.size()}"/>)</a>
                        </isif>

                        <iscomment>render the palette, the first swatch is always preselected</iscomment>
                        <ul class="swatch-list<isif condition="${selectableColors.size() > 5}"> swatch-toggle</isif>">
                            <isloop items="${selectableColors}" var="colorValue" status="varloop">
                                <iscomment>Determine the swatch and the thumbnail for this color</iscomment>
                                <isset name="colorSwatch" value="${colorValue.getImage('swatch')}" scope="page"/>
                                <isset name="colorThumbnail" value="${colorValue.getImage('medium')}" scope="page"/>

                                <iscomment>If images couldn't be determined, display a "no image" thumbnail</iscomment>
                                <isif condition="${!empty(colorSwatch)}">
                                    <isset name="swatchUrl" value="${colorSwatch.getURL()}" scope="page"/>
                                    <isset name="swatchAlt" value="${colorSwatch.alt}" scope="page"/>
                                    <isset name="swatchTitle" value="${colorSwatch.title}" scope="page"/>
                                <iselse/>
                                    <isset name="swatchUrl" value="${URLUtils.staticURL('/images/noimagesmall.png')}" scope="page"/>
                                    <isset name="swatchAlt" value="${colorValue.displayValue}" scope="page"/>
                                    <isset name="swatchTitle" value="${colorValue.displayValue}" scope="page"/>
                                </isif>
                                <isif condition="${!empty(colorThumbnail)}">
                                    <isset name="thumbnailUrl" value="${colorThumbnail.getURL()}" scope="page"/>
                                    <isset name="thumbnailAlt" value="${colorThumbnail.alt}" scope="page"/>
                                    <isset name="thumbnailTitle" value="${colorThumbnail.title}" scope="page"/>
                                <iselse/>
                                    <isset name="thumbnailUrl" value="${URLUtils.staticURL('/images/noimagesmall.png')}" scope="page"/>
                                    <isset name="thumbnailAlt" value="${colorValue.displayValue}" scope="page"/>
                                    <isset name="thumbnailTitle" value="${colorValue.displayValue}" scope="page"/>
                                </isif>
                                <isif condition="${!empty(selectedColor)}">
                                    <isset name="preselectCurrentSwatch" value="${colorValue.value == selectedColor.value}" scope="page"/>
                                <iselse/>
                                    <isset name="preselectCurrentSwatch" value="${varloop.first}" scope="page"/>
                                </isif>

                                <iscomment>build the proper URL and append the search query parameters</iscomment>
                                <isset name="swatchproductUrl" value="${Product.variationModel.url('Product-Show', colorVarAttr, colorValue.value)}" scope="page"/>
                                <isif condition="${!empty(pdict.ProductSearchResult)}">
                                    <isset name="swatchproductUrl" value="${pdict.ProductSearchResult.url(swatchproductUrl)}" scope="page"/>
                                </isif>

                                <iscomment>render a single swatch, the url to the proper product detail page is contained in the href of the swatch link</iscomment>
                                <li>
                                    <a href="${swatchproductUrl}" class="swatch ${(preselectCurrentSwatch) ? 'selected' : ''}" title="<isprint value="${colorValue.displayValue}"/>">
                                        <img class="swatch-image" src="${swatchUrl}" alt="${swatchAlt}" title="${swatchTitle}" data-thumb='{"src":"${thumbnailUrl}","alt":"${thumbnailAlt}","title":"${thumbnailTitle}"}'/>
                                    </a>
                                </li>
                            </isloop>
                        </ul>
                    </div>
                </isif>
            </isif>
            <isif condition="${typeof disableLinks == 'undefined' || !disableLinks}">
                </a>
            </isif>
        </div>

        <div class="product-tile-bottom clearfix">
            <isif condition="${showrating && !Product.productSet}">
	            <iscomment>Rating</iscomment>
	            <isscript>
					importScript("product/ProductUtils.ds");
					var productRating = {};
					productRating = ProductUtils.GetProductRating(Product);
				</isscript>
				<div class="product-review" data-pid="${productRating.sku}">
					<div class="rating" data-rating="${productRating.productActualRating}" data-isHalf="${productRating.isHalfExist}">
						<isloop begin="1" end="${productRating.ratingToDisplay}">
							<i class="fa fa-star" style="color:#ffa000;"></i>
						</isloop>
						<isif condition="${productRating.isHalfExist}">
							<i class="fa fa-star-half" style="color:#ffa000;"></i>
						</isif>
						<isloop begin="1" end="${productRating.disRatingToDisplay}">
							<i class="fa fa-star-regular" style="color:#ffa000;"></i>
						</isloop>
					</div>
				</div>
			</isif>

            <iscomment>Price</iscomment> 
            <isinclude url="${URLUtils.url('Product-IncludeTilePrice','pid',pdict.Product.ID,'showpricing', showpricing, 'IsLiveAuctionProduct',pdict.IsLiveAuctionProduct)}"/>
			
			<isset name="showTJCPlusDeliveryMsg" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('tjcPlus_showTJCPlusDeliveryMsg')}" scope="page" />
			<isif condition="${showTJCPlusDeliveryMsg}">
				<isif condition="${!Product.custom.isEngravingItem}" >
					<iscomment>this remote include is to display TJC Plus delivery time and to avoid caching</iscomment>
					<isinclude url="${URLUtils.url('TJCPlus-ShowDeliveryTime','pid', pdict.Product.ID,'source', 'plp' )}"/>
				<iselse/>
					<div class="tjcplus-engraving">
						<isinclude url="${URLUtils.url('TJCPlus-ShowDeliveryTime','pid', pdict.Product.ID,'source', 'plp' )}"/>
					</div>
				</isif>
			</isif>
			<isscript>
				var masterId = pdict.Product.isVariant() ? pdict.Product.masterProduct.ID : pdict.Product.ID;
				var avm = pdict.Product.availabilityModel;
				pdict.available = avm.availability>0;
		
				var availableCount = "0";
				if (pdict.available && !empty(avm.inventoryRecord)) {
					availableCount = avm.inventoryRecord.perpetual ? "999" : avm.inventoryRecord.ATS.value.toFixed();
				} 
			</isscript>
            <isif condition="${showfooter}">
                <div class="product-tile-buttons">
	               <isif condition="${pdict.Product.master || pdict.Product.custom.isEngravingItem || pdict.Product.variant}" >
		                   <a class="button variant-addtocart" data-url="${productUrl}" href="${productUrl}" title="Add to bag">
		                      ${Resource.msg('global.addtocart','locale',null)}
		                   </a> 
	                  <iselse>
	                    <isif condition="${availableCount == 0}">
	                    	<span class="oos-add-to-cart" href="">Out of stock</span>
	                    <iselse>
	                    	<isscript>var className = "addtocart_" +pdict.Product.ID;</isscript>
	                    	<a id="add-to-cart" data-pid="${pdict.Product.ID}" href="${dw.web.URLUtils.url('Cart-AddProduct', 'pid', pdict.Product.ID,'quickview','isQuickView')}" class="xlt-addToCart button ${className}" title="Add to bag" >
								${Resource.msg('global.addtocart','locale',null)}
							</a> 
	                    </isif>
	                </isif> 
	                <iscomment>
	                code added for TW-1455 Likelist signin journey
					</iscomment>                  
					<span class="wishlistplp-pid" style="display:none">${pdict.Product.ID}</span> 
                    <isset name="unregisterClassplp" value="${!pdict.CurrentCustomer.isAuthenticated() ? 'sigin-model-button' : ''}" scope="page" />     
                    	<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('LoginPopUpActive')}" >
                    		<a data-action="wishlist" href="${URLUtils.https('Wishlist-Add', 'pid', pdict.Product.ID, 'source', 'productdetail')}" title="${Resource.msg('global.addtowishlist','locale',null)}" class="button wishlist-button ${unregisterClassplp}">
						<iselse>
							<a data-action="wishlist" href="${URLUtils.https('Wishlist-Add', 'pid', pdict.Product.ID, 'source', 'productdetail')}" title="${Resource.msg('global.addtowishlist','locale',null)}" class="button wishlist-button">
						</isif>
						<span>${Resource.msg('global.wishlist','locale',null)}</span>		
					</a>										
                </div>
            </isif>
        </div>
        <input type="hidden" name="gtm-info" class="gtm-info" value="${JSON.stringify(pdict.info)}"/>
    </div>
</isif>
 