<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isif condition="${!empty(pdict.CacheDeleteTime)}">
    <iscache type="daily" hour="${pdict.CacheDeleteTime.getHours()}" minute="${pdict.CacheDeleteTime.getMinutes()}" varyby="price_promotion"/>
<iselse/>
    <iscache type="relative" hour="24" varyby="price_promotion"/>
</isif>
<isinclude template="util/modules"/>
<iscomment>
    This template is best used via a **remote** include (Product-ProductSlider) and _not_ local include.
    This template renders a product tile using a product. The following parameters
    must be passed into the template module:

    product         : the product to render the tile for
    showpricing     : check, whether to render the pricing (default is false)
    showpromotion   : check, whether to render the promotional messaging (default is false)
    showrating      : check, whether to render the review rating (default is false)
    showbadge       : check, whether to render the badges (default is true)
</iscomment>
<isset name="isPlp" value="true" scope="page"/>
<isset name="Product" value="${pdict.product}" scope="page"/>
<iscomment>set default values</iscomment>
<isset name="showname" value="${(pdict.showname != null) ? pdict.showname : false}" scope="page"/>
<isset name="showquickview" value="${(pdict.showquickview != null) ? pdict.showquickview : false}" scope="page"/>
<isset name="showpricing" value="${(pdict.showpricing != null) ? pdict.showpricing : false}" scope="page"/>
<isset name="showpromotion" value="${(pdict.showpromotion != null) ? pdict.showpromotion : false}" scope="page"/>
<isset name="showrating" value="${(pdict.showrating != null) ? pdict.showrating : false}" scope="page"/>
<iscomment><isset name="showbadge" value="${(pdict.showbadge != null) ? pdict.showbadge : true}" scope="page"/></iscomment>

<isif condition="${!empty(Product)}">
    <iscomment>
        Get the colors selectable from the current product master or variant range if we
        need to determine them based on a search result.
    </iscomment>
    <isscript>
    	importScript("app_tjc:util/libUtils.ds");
        var selectableColors = new dw.util.ArrayList();
        var imageSize = 'medium';
        var PVM = Product.variationModel;
        var colorVarAttr, selectedColor, imageSource, image;
        if (PVM) {
            colorVarAttr = PVM.getProductVariationAttribute('color');
            if (colorVarAttr) {
                selectableColors = PVM.getFilteredValues(colorVarAttr);
            }
            if (Product.variationGroup) {
                imageSource = selectedColor = PVM.getSelectedValue(colorVarAttr);
                if (!imageSource) {
                    if (!PVM.variants.isEmpty()) {
                        imageSource = PVM.defaultVariant;
                        if (imageSource) {
                            selectedColor = PVM.getVariationValue(imageSource, colorVarAttr);
                        }
                    }
                }
            } else if (Product.isMaster() && PVM.defaultVariant) {
                if (colorVarAttr) {
                    imageSource = PVM.defaultVariant;
                    selectedColor = imageSource.variationModel.getSelectedValue(colorVarAttr);
                } else {
                    imageSource = PVM.master;
                }
            } else if (Product.isVariant()) {
                if (colorVarAttr) {
                    imageSource = selectedColor = PVM.getSelectedValue(colorVarAttr);
                    if (!imageSource) {
                        if (!PVM.variants.isEmpty()) {
                            imageSource = PVM.variants[0];
                            selectedColor = imageSource.variationModel.getSelectedValue(colorVarAttr);
                        }
                    }
                }
                else {
                    imageSource = Product;
                }
            } else {
                /* standard product, product set or bundle*/
                imageSource = Product;
            }
        } else {
            imageSource = Product;
        }
        image = imageSource.getImage(imageSize, 0);

        /* Generate link to product detail page: by default it's just the product of the product search hit.*/
        /* If a color variation is available, the first color is used as link URL.*/
        var productUrl = URLUtils.url('Product-Show', 'pid', Product.ID);
        if (selectedColor) {
            productUrl = Product.variationModel.urlSelectVariationValue('Product-Show', colorVarAttr, selectedColor)
        }

        var hasBadge = !empty(Product.custom.badges);
        
        var blackFridayCategoryId = dw.system.Site.getCurrent().getCustomPreferenceValue('blackFridayCategoryForBadge'); /*"black-friday-doorbusters";*/
		
		var doorBusterId = dw.system.Site.getCurrent().getCustomPreferenceValue('doorBusterCategoryForBadge'); /*"black-friday-doorbusters";*/
        
        var categoriesArray = Product.categories.toArray();
        
        var isInBlackFridayCategory = isContainedInCategory(blackFridayCategoryId, categoriesArray);
		
		var isDoorBusterCategory = isContainedInCategory(doorBusterId, categoriesArray);
        
        function isContainedInCategory(categoryId, array){
        	for (var i = 0; i < array.length; i++){
        		if (array[i].ID == categoryId){
        			return true;
        		}
        	}
        	return false;
        }
                
        var productStock = Product.availabilityModel.inventoryRecord.ATS.value;

        var stockRequired = dw.system.Site.getCurrent().getCustomPreferenceValue('stockThresholdToShowBadge'); /*3*/
        
        var lastInStock = false;
        
        if (productStock && stockRequired >= productStock){
        	lastInStock = true;
        }
                
        if (hasBadge == false){
        	if (isInBlackFridayCategory == true || lastInStock == true){
        		hasBadge = true;
        		showbadge = true;
        	}
        }

        /*if (pdict.IsLiveAuctionProduct) {
            productUrl = URLUtils.https('LiveTV-Start');
        }*/
        
        var isRA = false;
        var productObj = new Object();
		productObj.pid = Product.ID;
    </isscript>
    
      <div class="slider-tile product-link">
        <isif condition="${!empty(pdict.showwishlist) && pdict.showwishlist}">
        	<div class="tile-wishlist">
        		<isscript>
					importScript("product/ProductUtils.ds");
					var avm = pdict.Product.availabilityModel;
					pdict.available = avm.availability>0;
				</isscript>
				<isset name="unregisterClass" value="${!pdict.CurrentCustomer.isAuthenticated() ? 'login-wish-class' : ''}" scope="page" />
				<!-- <a data-action="wishlist" href="${URLUtils.https('Wishlist-Add', 'pid', Product.ID, 'source', 'slider')}" title="${Resource.msg('global.addtowishlist','locale',null)}" class="product-slider-wishlist-link ${unregisterClass}">
					<i class="fa fa-heart"></i>
				</a> -->
				<isset name="isProductInWishlist" value="${ProductUtils.isProductInWishList(pdict.Product.ID,pdict.CurrentCustomer)}" scope="page" />
				<isset name="wishlisturl" value="${isProductInWishlist ? URLUtils.https('Wishlist-Remove', 'pid', pdict.Product.ID, 'source', 'likelist') : URLUtils.https('Wishlist-Add', 'pid', pdict.Product.ID, 'source', 'likelist')}" scope="page" />
				<isset name="wishlistclass" value="${isProductInWishlist ? 'product-wishlist-link added-to-wishlist' : 'product-wishlist-link'}" scope="page" />
				<isset name="wishlistsymbolclass" value="${isProductInWishlist ? 'fa fa-heart-solid' : 'fa fa-heart'}" scope="page" />
				<a data-action="wishlist" href="${wishlisturl}" title="${Resource.msg('global.addtowishlist','locale',null)}" class="${wishlistclass} ${unregisterClass}">
					<i class="${wishlistsymbolclass}"><!-- icon --></i>
				</a>
			</div>
        </isif>
         
    	<div class="slider-product-tile<isif condition="${showbadge && hasBadge}"> has-badge</isif>" id="${Product.UUID}" data-itemid="${Product.ID}" data-cgid="${pdict.cgid}">
            <iscomment>
            <isif condition="${showbadge && hasBadge}">            
                <isscript>
					var badgeID;             	
                	if(isDoorBusterCategory == true)
                	{
                		 badgeID ="doorbuster";
                	}
                	else if(isInBlackFridayCategory == true)
                	{
                		 badgeID ="blackfriday";
                	}                 	
                	else if(lastInStock == true && Product.custom.badges.length==0)
                	{
                				
                				badgeID = "last-in-stock";                				
                			
                	} 
                	                 	              	
                	else 
                	{                		
                		for(var i=0,len=Product.custom.badges.length;i<len;i++) {
                		
                			var badge = Product.custom.badges[i];
                			
                			if( badge =="freeship")
                			{
                					badgeID = "freeship";
                					break;                					
                			}   
                			else 
                			{
                				
                				badgeID =  Product.custom.badges[0];
                				if(badgeID == 'lastinstock')
                				{
                					badgeID = "last-in-stock";
                				}
                			}              	
                	}                		
                	}             	
                </isscript>  
                <div class="product-badge ${badgeID}">
                    <img src="${URLUtils.staticURL('/images/badge-' + badgeID + '.png')}" alt="${badgeID}">
                </div>
            </isif>
            </iscomment>
            
            <iscomment>
            <isif condition="${!empty(Product.custom.installments) && Product.custom.installments > 0}">
                <div class="budgetpay">
                    <img src="${URLUtils.staticURL('/images/budgetpay-logo.png')}">
                </div>
            <iselseif condition="${!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('PaypalCreditProductPriceThreshold'))}">
                <isscript>
                    var productPrice = 0;;

                    if (Product.master) {
                        productPrice = Product.variants[0].getPriceModel().getPrice().value;
                    } else {
                        productPrice = Product.getPriceModel().getPrice().value;
                    }
                </isscript>
                <isif condition="${productPrice >= dw.system.Site.getCurrent().getCustomPreferenceValue('PaypalCreditProductPriceThreshold')}">
                    <div class="paypal-credit">
                        <img src="${URLUtils.staticURL('/images/paypal-credit-logo.png')}">
                    </div>
                </isif>
            </isif>
            </iscomment>
            <a href="${productUrl}" title="${Product.name}" class="<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('useNewPDPTemplate') && pdict.showquickview}">quickview newquickview</isif>">
            	<isinclude template="product/components/producttile_top">
            </a>
            
            <isif condition="${showrating && !Product.productSet}">
                <isreviewsmini product="${Product}" variantname="category_page"/>
            </isif>

            <iscomment>Price</iscomment>
            <isinclude url="${URLUtils.url('Product-IncludeTilePrice','pid',pdict.Product.ID,'showpricing', showpricing, 'IsLiveAuctionProduct',pdict.IsLiveAuctionProduct)}"/>
			
			<iscomment>
			<isset name="showTJCPlusDeliveryMsg" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('tjcPlus_showTJCPlusDeliveryMsg')}" scope="page" />
			<isif condition="${showTJCPlusDeliveryMsg}">
				<isinclude url="${URLUtils.url('TJCPlus-ShowDeliveryTime','pid', pdict.Product.ID,'source', 'plp' )}"/>
			</isif>
			</iscomment>
	        <input type="hidden" name="gtm-info" class="gtm-info" value="${JSON.stringify(pdict.info)}"/>
	 	</div>
	   </div>
</isif>
