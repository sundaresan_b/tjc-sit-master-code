<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	Creates a div rendering product availability information. The three booleans allow you to
	control the rendering of the availability message. Note that the 'not available' message
	is always rendered if the product is not available.

	p_productli           : the product line item representing the product to render.
	p_displayinstock      : a boolean that controls if the 'in stock' message should be rendered.
	p_displaypreorder     : a boolean that controls if the 'pre order' message should be rendered.
	p_displaybackorder    : a boolean that controls if the 'back ordered' message should be rendered.
	p_hidedates			  : a boolean that hides the dates of pre-order and back ordered items.
</iscomment>

<iscomment>Create page variable representing the product line item</iscomment>
<isset name="p_productli" value="${pdict.p_productli}" scope="page"/>
<isif condition="${p_productli.product != null}">
	<isset name="availabilityModel" value="${p_productli.product.availabilityModel}" scope="page"/>
	<isset name="availabilityStatus" value="${p_productli.product.availabilityModel.availabilityStatus}" scope="page"/>
	<isset name="inventoryRecord" value="${p_productli.product.availabilityModel.inventoryRecord}" scope="page"/>
	<isset name="isOutOfStock" value="${availabilityStatus != dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK && availabilityStatus != dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER && availabilityStatus != dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER}" scope="page"/>
    <isset name="InventoryAvailability" value="${inventoryRecord.stockLevel.value}" scope="page" />
    <input type="hidden" value="${InventoryAvailability}" class="inventoryRecord" name="inventoryRecord"/>
    <div class="availability-msg">
    	<isif condition="${availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK}">
    		
    		<isif condition="${empty(pdict.p_displayinstock) || pdict.p_displayinstock}">
    		<div class="product-availability">			
    			<isif condition="${inventoryRecord != null && inventoryRecord.stockLevel.available}">
    				<isif condition="${inventoryRecord.stockLevel.value <= 5 && !inventoryRecord.perpetual}">
    					<div class="in-stock-msg">${StringUtils.format(Resource.msg('global.quantityinstock','locale',null),inventoryRecord.stockLevel.value)}</div>
    					<isif condition="${inventoryRecord.ATS.available && inventoryRecord.ATS.value > inventoryRecord.stockLevel.value}">
    						<div class="backorder-msg">${StringUtils.format(Resource.msg('global.allbackorder','locale',null))}</div>
    					</isif>
    				<iselse/>
    					<div class="in-stock-msg">${Resource.msg('global.instock','locale',null)}</div>
    				</isif>
    			<iselse/>
    				<div class="in-stock-msg">${Resource.msg('global.instock','locale',null)}</div>
    			</isif>
    		</div>
    		</isif>
    		
    	<iselseif condition="${availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER && !p_productli.product.custom.isEngravingItem}">
    	
    		<isif condition="${empty(pdict.p_displayinstock) || pdict.p_displaypreorder}">
    		<div class="product-availability">	
    			<div class="preorder-msg">${Resource.msg('global.allpreorder','locale',null)}
        			<isif condition="${!pdict.p_hidedates && p_productli.product.availabilityModel.inventoryRecord != null && p_productli.product.availabilityModel.inventoryRecord.inStockDate != null}">
        				<span class="expected">${Resource.msg('global.instock','locale',null)} <isprint value="${p_productli.product.availabilityModel.inventoryRecord.inStockDate.toDateString()}" />.</span>			
        			</isif>
                </div>  
    		</div>
    		</isif>
    		
    	<iselseif condition="${availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER && !p_productli.product.custom.isEngravingItem}">
    		
    		<isif condition="${empty(pdict.p_displayinstock) || pdict.p_displaybackorder}">
    			<div class="product-availability">
    				<isif condition="${p_productli.product.availabilityModel.inventoryRecord != null && p_productli.product.availabilityModel.inventoryRecord.inStockDate != null}">
    					<div class="backorder-msg">
    						<isif condition="${pdict.p_hidedates}">
    							${Resource.msg('global.allbackorder','locale',null)}
    						<iselse/>
    							<isset name="inStockDate" value="${p_productli.product.availabilityModel.inventoryRecord.inStockDate.toDateString()}" scope="page"/>
    							<span class="expected">${StringUtils.format(Resource.msg('global.inStockDate','locale',null), inStockDate)}</span>
    						</isif>
    					</div>
    				<iselse/>
    					<div class="backorder-msg">${Resource.msg('global.allbackorder','locale',null)}</div>
    				</isif>
    			</div>
    		</isif>
    		
    	<iselseif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && p_productli.custom.fromStoreId != null}">
    		<iscomment>base the display of the instock messaging on the store inventory</iscomment>
    		<isscript>
    			var store : Store = dw.catalog.StoreMgr.getStore(p_productli.custom.fromStoreId);
    			var storeinventory : ProductInventoryList = dw.catalog.ProductInventoryMgr.getInventoryList(store.custom.inventoryListId);
    		</isscript>
    				
    		<isif condition="${(!empty(storeinventory.getRecord(p_productli.productID)) && (storeinventory.getRecord(p_productli.productID).ATS.value >= p_productli.quantityValue))}">
    			<isif condition="${empty(pdict.p_displayinstock) || pdict.p_displayinstock}">
    				<div class="product-availability"><div class="in-stock-msg">${Resource.msg('global.instock','locale',null)}</div></div>
    			</isif>		
    		<iselse/>
    			<div class="product-availability"><div class="not-available-msg">${Resource.msg('global.allnotavailable','locale',null)}</div></div>
    		</isif>
    	<iselseif condition="${!p_productli.product.custom.isEngravingItem}">
    	
    		<div class="product-availability"><div class="not-available-msg">${Resource.msg('global.allnotavailable','locale',null)}</div></div>
    		
    	</isif>
    </div>
</isif>