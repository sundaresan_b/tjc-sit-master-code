<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules"/>
<div class="product-list-item">
<iscomment>
	Creates a div rendering a product and product line item information.
	p_productli : the product line item to render
	p_formli 	: the form line item
	p_editable  : boolean indicating if the pli is editable or not
	p_hideprice : boolean to hide price
	p_hidepromo : boolean to hide promotional message(s)
    p_hidepdplinks : boolean to hide links to the pdp
    p_istjcplusitem : boolean to know if the pli is a TJC Plus subscription item
    page_ns 	: Page namespace
</iscomment>

<iscomment>Create page variable representing the product line item</iscomment>
<isset name="productLineItem" value="${pdict.p_productli}" scope="page"/>
<iscomment>Create page variable representing the form line item</iscomment>
<isset name="formLineItem" value="${pdict.p_formli}" scope="page"/>
<iscomment>See if the items is part of the site catalog, and online prior to rendering link</iscomment>
<iscomment>Check if it is Cart Page</iscomment>
<isset name="isCartPage" value="${pdict.page_ns}" scope="page"/>
<isset name="isOrderPage" value="${pdict.p_isorderpage}" scope="page"/>
<isset name="trackorderpage" value="${pdict.trackorderpage}" scope="page"/>

<isif condition="${!empty(productLineItem.product) && !empty(productLineItem.product.custom.SEOProductName)}">
	<isset name="productDisplayName" value="${productLineItem.product.custom.SEOProductName}" scope="page"/>
<iselse>
	<isset name="productDisplayName" value="${productLineItem.productName}" scope="page"/>
</isif>

<isif condition="${productLineItem.product.custom.isWarrantyProduct}">
    <isscript>
    	if(pdict.Basket instanceof dw.order.Basket){
        	var warrantyParentProducts = pdict.Basket.getProductLineItems(productLineItem.custom.linkedSelectedWarrantyProduct);
        }
        else{
        	var warrantyParentProducts = pdict.Order.getProductLineItems(productLineItem.custom.linkedSelectedWarrantyProduct);
        }
        var warrantyParentProduct = warrantyParentProducts[0];
        var warrantyParentProductName = warrantyParentProduct.product.name;
        var warrantyParentProductPrice = warrantyParentProduct.product.custom.warrantyProductPrice;
        var warrantyParentProductQuantity = warrantyParentProduct.quantityValue;
        var warrantyParentProductDuration = warrantyParentProduct.product.custom.warrantyDuration
    </isscript>
    <isset name="ParentPriceModel" value="${warrantyParentProduct.product.getPriceModel()}" scope="page"/>
	<isset name="ParentSalesPrice" value="${ParentPriceModel.getPrice()}" scope="page"/>
	<isproductimage product="${warrantyParentProduct.product}" imgsize="cart" lazyloading="${false}" dataonly="${true}"/>
</isif>
<isscript>
var warrantyProductProcessed, warrantyProductWarrantyID, warrantyProductWarrantyExpiry, warrantyProductIsWarrantyExpired, warrantyProductWarrantyVoidReason;
</isscript>
<isif condition="${!empty(productLineItem.custom.WarrantyID)}">
    <isscript>
        warrantyProductProcessed = true;
        warrantyProductWarrantyID = productLineItem.custom.WarrantyID;
        warrantyProductWarrantyExpiry = productLineItem.custom.WarrantyExpiry;
        warrantyProductIsWarrantyExpired = productLineItem.custom.IsWarrantyExpired;
        warrantyProductWarrantyVoidReason = productLineItem.custom.WarrantyVoidReason;
    </isscript>
</isif>
<isif condition="${productLineItem.product == null || (productLineItem.product != null && (!productLineItem.product.assignedToSiteCatalog || !productLineItem.product.online))}">
	<span class="not-available">
		<iscomment>${Resource.msg('cart.removeditem','checkout',null)}</iscomment>
	</span>
	<div class='name'><isprint value="${productDisplayName}"/></div>
<iselse/>
	<div class='name'>
		<iscomment>
			Item has a category context, forward this category context
			MUST use URLUtils.http as creates absolute url and URLUtils.url does not work in case of an email
			e.g. order confirmation email, links inside it are wrong i.e. host name is missing because
			URLUtils.url api returns relative url!!
		</iscomment>
		<isif condition="${productLineItem.product.custom.isWarrantyProduct}"> 
			<isscript>
				importScript("checkout/Utils.ds");
				var warrantyDur = getPliWarrantyDetails(warrantyParentProductDuration);
			</isscript>
			<isif condition="${isCartPage}">			
        		<div class='nonTabView-warrantyProdMsg warranty-product-name warranty-dialog' data-cartPage="true" data-warrantyDuration="${warrantyDur}" data-warrantyCost="<isif condition="${'isTJCPlusMember' in pdict.CurrentCustomer.profile.custom && pdict.CurrentCustomer.profile.custom.isTJCPlusMember}">${Resource.msg('product.freewarrantyfortjcpluscustomers.price', 'product', null)}<iselse> ${warrantyParentProductPrice} </isif>" data-warrantyQuantity="${productLineItem.quantityValue}" data-productCost='<isprint value="${ParentSalesPrice}"/>' data-productQuantity='<isprint value="${warrantyParentProductQuantity}"/>' data-productName="${warrantyParentProductName}" data-productImage="${imageData.url.toString()}"><isprint value="${StringUtils.format(Resource.msg('product.warrantyduration','product',null),warrantyDur)}"/><isif condition="${('isTJCPlusMember' in pdict.CurrentCustomer.profile.custom && pdict.CurrentCustomer.profile.custom.isTJCPlusMember)}"> <iselse> + ${warrantyParentProductPrice}</isif><br class="nonTabView-warrantyProdMsg"/><span class="nonTabView-warrantyProdMsg hidden">Protects against manufacturing faults and accidental damage</span></div>
        	<iselse/>
        		<isprint value="${StringUtils.format(Resource.msg('product.warrantyduration','product',null),warrantyDur)}"/>
        	</isif>
        <iselseif condition="${pdict.p_hidepdplinks}">
            <isprint value="${productLineItem.lineItemText}"/>
		<iselseif condition="${productLineItem.product.isVariant() && !(productLineItem.product.getMasterProduct().isSearchable())}">
			<isprint value="${productLineItem.lineItemText}"/>
		<iselseif condition="${!productLineItem.product.isSearchable()}">
			<isprint value="${productLineItem.lineItemText}"/>
		<iselseif condition="${productLineItem.categoryID != null}">
			<a href="${URLUtils.https('Product-Show','pid', productLineItem.productID, 'cgid', productLineItem.categoryID)}" title="${productLineItem.productName}"><isprint value="${productDisplayName}"/></a>
		<iselse/>
			<a href="${URLUtils.https('Product-Show','pid', productLineItem.productID,'buyAllApplicableProductCodes')}" title="${productLineItem.productName}"><isprint value="${productDisplayName}"/></a>
		</isif>
	</div>
</isif>

<isif condition="${productLineItem.product.custom.isWarrantyProduct && !isCartPage && (isOrderPage || trackorderpage)}">
	<div class="warrantyID">
		<isif condition="${warrantyProductProcessed}">
			<div>Warranty ID: &#35;<span class="id">${warrantyProductWarrantyID}</span></div>
			<div>Expiry Date: ${warrantyProductWarrantyExpiry} <isif condition="${warrantyProductIsWarrantyExpired}"><span class="expired">No Longer Valid</span></isif></div>
			<isif condition="${warrantyProductIsWarrantyExpired}"><div class="expired-msg">${warrantyProductWarrantyVoidReason}</div></isif>
			<isscript>warrantyProductProcessed = false;</isscript>
		</isif>
		<a href="${dw.web.URLUtils.https('Page-Show', 'cid', 'warrantyprogram-terms-and-conditions')}" target="_blank">Terms & Conditions</a>
	</div>
</isif>

<isif condition="${empty(pdict.p_hidepromo) || !pdict.p_hidepromo}">
	<iscomment>promotional messaging</iscomment>
	<isloop items="${productLineItem.priceAdjustments}" var="pli" status="loopstate">
		<div class="promo <isif condition="${loopstate.first}"> first <iselseif condition="${loopstate.last}"> last</isif>">- <isprint value="${pli.lineItemText}"/></div>
	</isloop>
</isif>

<iscomment>surcharge message</iscomment>
<isif condition="${productLineItem.shippingLineItem != null}">
	<isif condition="${productLineItem.shippingLineItem.surcharge}">
		<div class="promo"><isprint value="${Resource.msg('product.display.surcharge','product',null)}"/></div>
	</isif>
<iselse/>
	<isscript>
		var shippingMgr = dw.order.ShippingMgr;
		var defaultMethod : dw.order.ShippingMethod = shippingMgr.getDefaultShippingMethod();
		var shippingModel : dw.order.ProductShippingModel = null;

		if(productLineItem != null || productLineItem.getProduct() != null){
			shippingModel = shippingMgr.getProductShippingModel(productLineItem.getProduct());
		}

		var shippingCost = null;
		if (defaultMethod !=null && shippingModel != null) {
			shippingCost = shippingModel.getShippingCost(defaultMethod);
		}
	</isscript>
	<isif condition="${!empty(shippingCost) && shippingCost.surcharge}">
		<div class="promo"><isprint value="${Resource.msg('product.display.surcharge','product',null)}"/></div>
	</isif>
</isif>

<iscomment>
    Show Edit Details link if
    Product is not null and it is either a variant and online or options product and online
</iscomment>

<div class="sku <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif> <isif condition="${!productLineItem.product.custom.isEngravingItem}">d-none</isif>">
	<isinclude template="product/components/engravingdetails"/>
</div>

<div class="productNumber hidden">
	<isif condition="${productLineItem.product.custom.isWarrantyProduct && isCartPage}">
		<div class='tabView-warrantyProdMsg warranty-product-name warranty-dialog' data-cartPage="true" data-warrantyDuration="${warrantyDur}" data-warrantyCost="${warrantyParentProductPrice}" data-warrantyQuantity="${productLineItem.quantityValue}" data-productCost='<isprint value="${ParentSalesPrice}"/>' data-productQuantity='<isprint value="${warrantyParentProductQuantity}"/>' data-productName="${warrantyParentProductName}" data-productImage="${imageData.url.toString()}"><isprint value="${StringUtils.format(Resource.msg('product.warrantyduration','product',null),warrantyDur)}"/><br class="tabView-warrantyProdMsg"/><span class="tabView-warrantyProdMsg">Protects against manufacturing faults and accidental damage</span></div>
		<div class="tjcplussubheading"> <strong>FREE FOR <span style="color:#FF520E;font-weight:600;">PLUS</span> MEMBERS</strong></div>
	</isif>
	<span class="value <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
		<isif condition="${empty(pdict.p_istjcplusitem) && !pdict.p_istjcplusitem}">
			<isprint value="${productLineItem.productID}"/>
		<iselse>
			&nbsp;
		</isif>
	</span>
</div>

<iscomment>product attributes</iscomment>

<iscomment>render pricing only for editable product line items</iscomment>
<isif condition="${pdict.p_editable && !empty(productLineItem.product)}">
	<isif condition="${empty(pdict.p_hideprice) || !pdict.p_hideprice}">
		<div class="attribute <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
			<span class="label">${Resource.msg('global.price','locale',null)}</span>
			<isset name="Product" value="${productLineItem.product}" scope="pdict"/>
			<isinclude template="product/components/pricing"/>
		</div>
	</isif>
</isif>

<isif condition="${empty(pdict.p_istjcplusitem) && !pdict.p_istjcplusitem}">
	<div class="source hidden <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
	    <span class="value">
	        <isif condition="${!empty(productLineItem.custom.itemSource)}">
	            <isif condition="${productLineItem.custom.itemSource == 'RISING'}">
	                <isprint value="${Resource.msg('cart.itemsource.rising','checkout',null)}"/>
	            <iselseif condition="${productLineItem.custom.itemSource == 'WEBC' || productLineItem.custom.itemSource == 'TGWEBC'}">
	                <isprint value="${Resource.msg('cart.itemsource.missed','checkout',null)}"/>
	            <iselseif condition="${productLineItem.custom.itemSource == 'TV' || productLineItem.custom.itemSource == 'TGTV'}">
	                <isprint value="${Resource.msg('cart.itemsource.live','checkout',null)}"/>
	            <iselse>
	                <isprint value="${Resource.msg('cart.itemsource.web','checkout',null)}"/>
	            </isif>
	        <iselse>
	            <isprint value="${Resource.msg('cart.itemsource.web','checkout',null)}"/>
	        </isif>
	    </span>    
	</div>
</isif>

	
 
<isif condition="${!empty(productLineItem.custom.shippingDate)}">
	<div class="attribute dispatchDate <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
		<span class="label">${Resource.msg('order.orderdetails.dispatchDate','order',null)}:</span>
		<span class="value"><isprint value="${productLineItem.custom.shippingDate}"/></span>
	</div>
</isif>

<isif condition="${!empty(productLineItem.custom.trackingLink)}">
	<div class="attribute trackingInfo <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
		<a class="link-tracking" href="${productLineItem.custom.trackingLink}" target="_blank">
	    	${Resource.msg('order.orderdetails.trackingInfo','order',null)}
	    </a>
	</div>
</isif>

<isif condition="${!empty(productLineItem.custom.returnsStatus)}">
	<div class="attribute returnsStatus <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
		<span class="label">${Resource.msg('order.orderdetails.returnsStatus','order',null)}:</span>
		<span class="value"><isprint value="${productLineItem.custom.returnsStatus}"/></span>
	</div>
</isif>

<iscomment>
<isif condition="${productLineItem.custom.isPreorder}">
	<div class="attribute preOrder <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
		<isavailability p_product="${productLineItem.product}"/>
	</div>
</isif>
</iscomment>



<iscomment>product list info</iscomment>
<!---
<isif condition="${productLineItem.productListItem != null}">
	<span class="item-links">
		<isif condition="${productLineItem.productListItem.list.type == dw.customer.ProductList.TYPE_WISH_LIST}">
			<a href="${URLUtils.url('Wishlist-ShowOther','WishListID', productLineItem.productListItem.list.ID)}" title="">
				${Resource.msg('product.display.wishlistitem','product',null)}
			</a>
		<iselseif condition="${productLineItem.productListItem.list.type == dw.customer.ProductList.TYPE_GIFT_REGISTRY}">
			<a href="${URLUtils.url('GiftRegistryCustomer-Show','ID', productLineItem.productListItem.list.ID)}" title="">
				${Resource.msg('product.display.registryitem','product',null)}
			</a>
		</isif>
	</span>
</isif>
--->

<iscomment>product options</iscomment>
<isif condition="${productLineItem.optionProductLineItems.size() > 0}">
	<isloop items="${productLineItem.optionProductLineItems}" var="optionLI" status="loopstate">
		<div class="product-option <isif condition="${loopstate.first}">first<iselseif condition="${loopstate.last}">last</isif> <isif condition="${productLineItem.product.custom.isWarrantyProduct}">visibility-hidden</isif>">
			<isprint value="${optionLI.lineItemText}"/>
		</div>
	</isloop>
</isif>
</div>  

