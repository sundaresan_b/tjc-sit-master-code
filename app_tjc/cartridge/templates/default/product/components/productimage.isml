<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	Creates the image tag for the product.
	product 		: the product to get the image from
	variationvalue  : the variation value
	protocol		: http or https or empty (absolute url)
	imgsize    		: size code
	index   		: the index of the image, default is 0
	displaywidth	: width to use in img element to overwrite actual image size
	displayheight	: height to use in img element to overwrite actual image size
	imageconfig     : a custom image config entry
	dataonly		: if true, the image tag is not rendered but the 'imageData' variable can be used
</iscomment>

<isinclude template="product/components/productimageconfig"/>
<isscript>
	var ProductSO = require('/app_tjc/cartridge/scripts/product/libProduct'),
	    scopeObj = this.dontUsePdict ? this : pdict,
	    product = scopeObj.product,
		protocol = empty(scopeObj.protocol) ? '' : scopeObj.protocol.toLocaleLowerCase(),
		size = empty(scopeObj.imgsize) ? 'catalog' : scopeObj.imgsize,
		index = empty(scopeObj.index) ? 0 : parseInt(scopeObj.index, 10),
		headless = empty(scopeObj.headless) ? false : scopeObj.headless,
		displayheight = empty(scopeObj.displayheight) ? null : scopeObj.displayheight,
		displaywidth = empty(scopeObj.displaywidth) ? null : scopeObj.displaywidth,
		dataonly = empty(pdict.dataonly) ? false : pdict.dataonly,
		isDefaultImage = !empty(scopeObj.isdefaultimage) && (scopeObj.isdefaultimage);
    	
    var imageProductSO = ProductSO(product),
	getImageData = function () {
		return empty(scopeObj.variationmodel) && empty(scopeObj.variationvalue) ? imageProductSO.getProductImage(productImageConfig, size, index, protocol, null, null, scopeObj.noimage, isDefaultImage) : imageProductSO.getProductImage(productImageConfig, size, index, protocol, scopeObj.variationmodel, scopeObj.variationvalue, scopeObj.noimage, isDefaultImage);
	};
	imageData = getImageData(); 
	getImageThumbnailData = function () {
		return empty(scopeObj.variationmodel) && empty(scopeObj.variationvalue) ? imageProductSO.getProductImage(productImageConfig, "thumbnail", index, protocol, null, null, scopeObj.noimage, isDefaultImage) : imageProductSO.getProductImage(productImageConfig, "thumbnail", index, protocol, scopeObj.variationmodel, scopeObj.variationvalue, scopeObj.noimage, isDefaultImage);
	};
	imageThumbnailData = getImageThumbnailData(); 
	zoomImageData = empty(scopeObj.variationmodel) && empty(scopeObj.variationvalue) ? imageProductSO.getProductImage(productImageConfig, 'pdp', index, protocol, null, null, scopeObj.noimage, isDefaultImage) : imageProductSO.getProductImage(productImageConfig, size, index, protocol, scopeObj.variationmodel, scopeObj.variationvalue, scopeObj.noimage, isDefaultImage);

	if (!empty(scopeObj.imageconfig)) {
	    productImageConfig[size] = scopeObj.imageconfig;
	}
	var lazyLoadingFlag = dw.system.Site.current.preferences.custom.lazyLoadingFlag;
	
</isscript>

<isif condition="${headless != true && !dataonly}">
	<isif condition="${lazyLoadingFlag}">
		<isif condition="${pdict.showquickview || (empty(pdict.lazyloading) || pdict.lazyloading==false)}">
			<img src="${imageData.url}" alt="${imageData.alt}" data-zoom="${zoomImageData.url}" data-src="${imageData.url}" class="clickable-image non-lazy pdp-main-img" data-lazyload="${pdict.lazyloading}" <isif condition="${scopeObj.notitle !== true}"> title="${imageData.title}"</isif> onerror="replaceNoImage(this);"/>
		<iselse>
			<img src="${imageThumbnailData.url}" alt="${imageData.alt}" data-zoom="${zoomImageData.url}" data-src="${imageData.url}" class="clickable-image lazy lazy-thumbnail pdp-main-img" data-lazyload="${pdict.lazyloading}" <isif condition="${scopeObj.notitle !== true}"> title="${imageData.title}"</isif> onerror="replaceNoImage(this);"/>
		</isif>
	<iselse>
		<img src="${imageData.url}" alt="${imageData.alt}" data-zoom="${zoomImageData.url}" data-src="${imageData.url}" class="clickable-image lazy-off pdp-main-img" <isif condition="${scopeObj.notitle !== true}"> title="${imageData.title}"</isif> onerror="replaceNoImage(this);"/>
	</isif>
</isif>
