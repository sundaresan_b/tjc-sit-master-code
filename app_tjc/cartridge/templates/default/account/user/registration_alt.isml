<iscontent type="text/html" charset="UTF-8" compact="true" />

<iscomment>
	Builds a registration page for the user. It provides all input fields for names and address.
	If the alternative registration URL is used, 'pdict.AltRegistration' will be 'true'. 
</iscomment>

<isdecorate template="pagetypes/pt_account_register_alt">
	<isinclude template="util/modules" />
    <isibmpageviewtag type="mapping" arguments="'registera-alt'" />

	<div class="registration registration-alt">
		<h1 class="page-title">				
			<span class="page-title-inner">${Resource.msg('account.user.registration.createnew','account',null)}</span>
		</h1>
	       
		<isslot id="header-slot-alt-registration" description="Slot below the Header (alternative Registration)" context="global" />
	
		<form action="${URLUtils.httpsContinue()}" method="post" id="RegistrationForm" class="alternative-registration">
			<input type="hidden" name="${pdict.CurrentForms.profile.secureKeyHtmlName}" value="${pdict.CurrentForms.profile.secureKeyValue}" />
        	<iserrormessage form="${pdict.CurrentForms.profile}" />
        	
        	<fieldset>
        		<legend>
        			<i class="fa fa-lock"></i>
        			<span>${Resource.msg('account.user.registration.section1', 'account', null)}</span>
        		</legend>
        		
        		<isif condition="${!pdict.CurrentSession.customer.externallyAuthenticated}">
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.email}" type="email" p_dynamic="true" xhtmlclass="first-email" placeholder="${Resource.msg('form.placeholder.email','forms',null)}" />
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.emailconfirm}" type="email" p_dynamic="true" placeholder="${Resource.msg('form.placeholder.email.confirm','forms',null)}" xhtmlclass="cross-validation" attribute1="data-crossvalidationclass" value1="first-email" />
				<iselse/>				
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.email}" type="input" placeholder="${Resource.msg('form.placeholder.email','forms',null)}" attribute1="disabled" value1="disabled" />				
				</isif>

				<isinputfield formfield="${pdict.CurrentForms.profile.login.pin}" type="password" autocomplete="false" rowclass="form-row-no-margin-top icon-tooltip" attribute1="data-tooltip-text" value1="${Resource.msg('form.tooltips.pin', 'forms', null)}" xhtmlclass="first-pin" placeholder="${Resource.msg('form.placeholder.pin','forms',null)}" />
				<isinputfield formfield="${pdict.CurrentForms.profile.login.pinconfirm}" type="password" autocomplete="false" placeholder="${Resource.msg('form.placeholder.pinconfirm','forms',null)}" xhtmlclass="cross-validation" attribute1="data-crossvalidationclass" value1="first-pin" />
        	</fieldset>
        	
        	<fieldset>
        		<legend>
        			<i class="fa fa-user"></i>
        			<span>${Resource.msg('account.user.registration.section2', 'account', null)}</span>
        		</legend>
        		
        		<isif condition="${!pdict.CurrentSession.customer.externallyAuthenticated}">
                	<isinputfield formfield="${pdict.CurrentForms.profile.customer.title}" type="select" rowclass="form-row-half form-row-no-margin-top"/>
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.firstname}" xhtmlclass="first-name" type="text" placeholder="${Resource.msg('form.placeholder.firstname','forms',null)}" />
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.lastname}" xhtmlclass="last-name" type="text" placeholder="${Resource.msg('form.placeholder.lastname','forms',null)}" />
				<iselse/>
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.firstname}" type="text" xhtmlclass="first-name" rowclass="form-row-no-margin-top" placeholder="${Resource.msg('form.placeholder.firstname','forms',null)}" attribute1="disabled" value1="disabled" />
					<isinputfield formfield="${pdict.CurrentForms.profile.customer.lastname}" type="text" xhtmlclass="last-name" placeholder="${Resource.msg('form.placeholder.lastname','forms',null)}" attribute1="disabled" value1="disabled" />
				</isif>
				
				<isinputfield formfield="${pdict.CurrentForms.profile.customer.shippingAddress.addressid}" type="hidden" placeholder="${Resource.msg('address.addressid.placeholder','forms',null)}" />
				<isaddressdetailsfields baseform="${pdict.CurrentForms.profile.customer.shippingAddress.address}" />
        	</fieldset>

			<fieldset>
				<legend>
					<i class="fa fa-lightbulb-o"></i>
					<span>${Resource.msg('account.user.registration.section3', 'account', null)}</span>
				</legend>
				
				<isinputfield formfield="${pdict.CurrentForms.profile.customer.phone}" type="text" xhtmlclass="phone" rowclass="form-row-no-margin-top" placeholder="${Resource.msg('form.placeholder.phone', 'forms', null)}" />
				<isinputfield formfield="${pdict.CurrentForms.profile.customer.mobile}" type="text" xhtmlclass="mobile" rowclass="form-row-no-margin-top" placeholder="${Resource.msg('form.placeholder.mobile', 'forms', null)}" />
				
				<div class="form-row form-row-birthdate">
				    <label class="label" for="">
				        <span>${Resource.msg('profile.dob','forms',null)}</span>
				    </label>
				    <div class="field birthdate-fields">
				        <isinputfield formfield="${pdict.CurrentForms.profile.customer.day}"   type="select" label="false" xhtmlclass="day bday" rowclass="first"  attribute1="data-monthclass" value1="bmonth" attribute2="data-yearclass" value2="byear" />    
				        <isinputfield formfield="${pdict.CurrentForms.profile.customer.month}" type="select" label="false" xhtmlclass="month bmonth" attribute1="data-dayclass" value1="bday" attribute2="data-yearclass" value2="byear" />
				        <isinputfield formfield="${pdict.CurrentForms.profile.customer.year}"  type="select" label="false" xhtmlclass="year byear" rowclass="last" attribute1="data-monthclass" value1="bmonth" attribute2="data-dayclass" value2="bday" />
				    </div>
				</div>
				
				<isinputfield formfield="${pdict.CurrentForms.profile.customer.gender}" type="select" />
				
				<iscomment>Since the label is removed from the heardfrom select, the required red star is not shown. This should be done in a better way</iscomment>
               <div class="form-row">
                   <label class="label" for><span>${Resource.msg('profile.heardfrom','forms',null)}</span> <span class="required-indicator">*</span></label>
                   <div class="field">
                       <isinputfield formfield="${pdict.CurrentForms.profile.customer.heardfrom}" type="select" rowclass="form-row-float" label="false" />
                       <isinputfield formfield="${pdict.CurrentForms.profile.customer.heardfromcustom}" type="text" rowclass="form-row-float last" placeholder="${Resource.msg('form.placeholder.specify','forms',null)}" label="false" />
                   </div>
               </div>
				
				<isinputfield formfield="${pdict.CurrentForms.profile.customer.tvchannel}" type="select" />
			</fieldset>

			<fieldset class="empty">

				<isinputfield formfield="${pdict.CurrentForms.profile.customer.addtomaillist}" type="checkbox" rowclass="form-row-auto form-row-no-indent form-row-no-margin-top"/>
				<isinputfield formfield="${pdict.CurrentForms.profile.customer.readtac}" type="checkbox" rowclass="form-row-auto form-row-no-indent" labelparameter="${dw.web.URLUtils.https('Page-Show', 'cid', 'terms-and-conditions')}"/>
			
				<div class="form-row form-row-button form-row-no-indent">
					<div class="field">
						<button class="button button-wide" type="submit" value="${Resource.msg('global.apply','locale',null)}" name="${pdict.CurrentForms.profile.confirm.htmlName}">
							${Resource.msg('global.register', 'locale', null)}
						</button>
					</div>
				</div>
			</fieldset>
		</form>

		<isslot id="footer-slot-alt-registration" description="Slot below the Registration Form (alternative Registration)" context="global" />
	</div>
</isdecorate>
