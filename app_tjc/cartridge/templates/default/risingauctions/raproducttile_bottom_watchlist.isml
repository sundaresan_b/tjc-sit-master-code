<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules"/>
<isinclude template="risingauctions/components/ramodules"/>

<isset name="auctionData" value="${pdict.AuctionData}" scope="page"/>
<isscript>
	var RisingAuctionMgr = require("app_tjc/cartridge/scripts/risingauctions/manager/RisingAuctionMgr"),
	   	mgr : RisingAuctionMgr = new RisingAuctionMgr(),
		customerHighestBidValues = mgr.getHighestBidValuesOfCustomerOnAuction(pdict.CurrentCustomer.profile.customerNo, auctionData.auctionCode),
		customerCurrentValue = !empty(customerHighestBidValues) ? new dw.value.Money(customerHighestBidValues.highestBidValue, session.currency.currencyCode) : null,
		customerHighestValue = !empty(customerHighestBidValues) ? new dw.value.Money(customerHighestBidValues.autoBidMaxValue, session.currency.currencyCode) : null;
</isscript>

<isif condition="${!empty(auctionData)}">

	<!--- Time Counter / Outbid Message --->
	<!--- ----------------------------- --->
	<td class="cell-timer">
		<isratimecounter end_time="${auctionData.endTime}" exclude_timecounter="${pdict.ExcludeTimeCounter}"/>
        
        <isif condition="${!auctionData.highestBidderYou}">
			<div class="outbid-msg">
				${dw.web.Resource.msg('risingauctions.bid.outbid.message', 'risingauctions', null)}		
			</div>
		</isif>
	</td>
	
	<!--- Current Bid (Price) --->
	<!--- ------------------- --->	
	<td class="cell-current-bid">
		
        <!--- Desktop Version --->
        <div class="only-for-desktop">
            <isinclude template="risingauctions/components/racomponent_price"/>                                   
        </div>
        
        <!--- Tablet & Mobile Version --->
        <ul class="not-for-desktop bid-list">
            <li class="bid-item">
                <div class="bid-list-label">${dw.web.Resource.msg('risingauctions.watchlist.label.currentbid', 'risingauctions', null)}</div>
                <isinclude template="risingauctions/components/racomponent_price"/>
            </li>
            
            <isif condition="${!empty(customerCurrentValue)}">
                <li class="bid-item">
                    <div class="bid-list-label">${dw.web.Resource.msg('risingauctions.watchlist.label.yourbid', 'risingauctions', null)}</div>
                    <isprint value="${customerCurrentValue}"/>
                    <isif condition="${auctionData.highestBidderYou}">
                    <div class="activebidhighest activebidhighest-you">
						<span class="activebidlabel">
			                <i class="fa fa-thumbs-up "><!-- icon --></i>
			                <span>
			                ${dw.web.Resource.msg('risingauctions.bidder.youwon', 'risingauctions', null)}
			                </span>
			            </span>
					</div>
            		<iselse/>
            		<div class="activebidhighest">
            		<span class="activebidlabel">				
	                	<i class="fa fa-thumbs-down "><!-- icon --></i>
	                	<span>
	                	  ${dw.web.Resource.msg('risingauctions.bidder.notyou', 'risingauctions', null)}
	                	</span>
	            	</span>
	            	</div>
                </isif>            
                </li>
            </isif>          
            
            <isif condition="${!empty(customerHighestValue) && customerHighestValue.value != 0}">
                <li class="bid-item">        
                    <div class="bid-list-label">${dw.web.Resource.msg('risingauctions.watchlist.label.yourbidmax', 'risingauctions', null)}</div>
                    <div class="bid bid-you bid-you-max">
                        <isprint value="${customerHighestValue}"/>
                    </div>
                </li>
            </isif>
        </ul>
	</td>
	
	
	<!--- Your Bid --->	
	<!--- -------- --->
	<td class="cell-your-bid only-for-desktop">
		<div class="bid bid-you">
			<isif condition="${!empty(customerCurrentValue)}">
				<isprint value="${customerCurrentValue}"/>
				<isif condition="${auctionData.highestBidderYou}">
                    <div class="activebidhighest activebidhighest-you">
						<span class="activebidlabel">
			                <i class="fa fa-thumbs-up "><!-- icon --></i>
			                <span>
			                ${dw.web.Resource.msg('risingauctions.bidder.youwon', 'risingauctions', null)}
			                </span>
			            </span>
					</div>
            		<iselse/>
            		<div class="activebidhighest">
            		<span class="activebidlabel">				
	                	<i class="fa fa-thumbs-down "><!-- icon --></i>
	                	<span>
	                	${dw.web.Resource.msg('risingauctions.bidder.notyou', 'risingauctions', null)}
	                	</span>
	            	</span>
	            	</div>
                </isif>  				
			</isif>
		</div>
	</td> 
	<!--- Actions / Buttons & Highest Bidder / Winner --->
	<!--- ------------------------------------------- --->
	<td class="ra-bid-action-wrapper cell-actions bid-actions">
		<div class="product-actions">
			<isif condition="${pdict.CurrentCustomer.authenticated || pdict.CurrentCustomer.registered}">
		    	<isif condition="${auctionData.auctionClosed}">
		    	<isif condition="${auctionData.bidCount == 0}"> 
		  			<span class="auction-ended-msg nobids">
						${dw.web.Resource.msg('risingauctions.auction.closed', 'risingauctions', null)}
					</span>
				<iselse/>
				<span class="auction-ended-msg nobids">
						${dw.web.Resource.msg('risingauctions.auction.SOLD', 'risingauctions', null)}
				</span>
				</isif>
				<iselse/> 
					<iscomment>code changes done for TW1430 to display the correct RA button</iscomment>
					<isif condition="${!auctionData.auctionClosed && (pdict.CurrentCustomer.authenticated || pdict.CurrentCustomer.registered)}"> 
					<isif condition="${auctionData.customerBiddedAlready && auctionData.bidCount > 0}" >
				<isset name="bidButtonLabel" value="${dw.web.Resource.msg('risingauctions.auction.bidnow.increase', 'risingauctions', null)}" scope="page"/> 
				<isset name="bidButtonClass" value="button-red" scope="page"/> 
				<isinclude template="risingauctions/components/racomponent_bidbuttonpdp"/> 
					</isif> 
					</isif>
				</isif>
			</isif>
		</div>	 
	</td>
	
		
</isif>
