<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules"/>
<isinclude template="util/functions"/>

<iscomment>NOTE: Removes Refinements if NO Products are found</iscomment>
<isif condition="${pdict.ProductSearchResult}">

<iscomment>Added to access unsanitizeOR()</iscomment>
<isscript>
	importScript('product/ProductUtils.ds');
	importScript("app_tjc:util/libUtils.ds");
    importScript('util/ViewHelpers.ds');

	var productSearchResult = pdict.ProductSearchResult,
		refinements = productSearchResult.refinements;
</isscript>
<iscomment>Process refinement options based on configured refinement definitions</iscomment>
<isif condition="${refinements != null && refinements.refinementDefinitions.size() > 0}">

	<h2 class="visually-hidden">${Resource.msg('searchrefinebar.refineresults','search',null)}</h2>

	<iscomment>If we have a category context, we build a path from the root to the category</iscomment>
	<isscript>
		var cat = productSearchResult.category;
		var path = new dw.util.ArrayList();
		while( cat != null && cat.parent != null )
		{
			if( !cat.online )
			{
				cat = cat.parent;
				continue;
			}
			path.addAt( 0, cat );
			cat = cat.parent;
		}
	</isscript>

	<div class="refinements">

		<div class="refinements-header toggle">
			<span class="refinements-header-text">${Resource.msg('global.refine','locale',null)}</span>
			<span class="refinements-header-icon"></span>

			<!--- ugly, but necessary for dropdown (tablet & mobile) --->
			<div class="bar"><!-- empty --></div>
		</div>

		<div class="refinements-content toggle-content">
			<div class="refinements-content-inner">
                <isinclude template="search/components/productsearchrefinementsselection"/>

				<isloop items="${refinements.refinementDefinitions}" var="RefinementDefinition" status="refinementsLoopState">

					<isif condition="${Utils.isInternalRefinementAttribute(RefinementDefinition.attributeID)}">
						<iscontinue/>
					</isif>


					<div class="refinement ${RefinementDefinition.displayName}">

					<iscomment>ATTRIBUTE REFINEMENTS</iscomment>
					<isif condition="${RefinementDefinition.isAttributeRefinement() && (empty(pdict.PermanentAttributeRefinements) || !pdict.PermanentAttributeRefinements.contains(RefinementDefinition.attributeID))}">

						<h3 class="refinement-headline toggle">
							<isprint value="${RefinementDefinition.getDisplayName()}"/>
						</h3>

                        <div class="refinement-content toggle-content">
							<ul class="refinement-list <isif condition="${!empty(RefinementDefinition.cutoffThreshold)}">cutoff-threshold-${RefinementDefinition.cutoffThreshold}</isif>">
								<iscomment>render the refinement values as simple list</iscomment>
								<isloop items="${refinements.getAllRefinementValues(RefinementDefinition)}" var="RefinementValue"><!---
								---><isif condition="${productSearchResult.isRefinedByAttributeValue(RefinementDefinition.attributeID,RefinementValue.value)}"><!---
									---><li class="selected"><!---
                                        ---><isscript>
                                                var relaxUrl = productSearchResult.urlRelaxAttributeValue('Search-Show',RefinementValue.getID(),RefinementValue.getValue());
                                            </isscript><!---
										--->
										<iscomment>I'm adding at the end the raStartPrice parameter since it isn't really a sorting rule and otherwise I wasn't able to keep it in the url</iscomment>
										<a href="${unsanitizeOR(relaxUrl)} <isif condition="${!empty(pdict.CurrentHttpParameterMap.raStartPrice.doubleValue)}">&raStartPrice=${pdict.CurrentHttpParameterMap.raStartPrice.doubleValue}</isif>" title="${Resource.msg('search.productsearchrefinebar.clickrefine','search',null)} ${RefinementValue.getDisplayValue()}">
												<div class="checkbox"><div class="checkbox-inner"><!-- checkbox --></div></div>
												<isprint value="${RefinementValue.getDisplayValue()}"/>
												<span class="count">(<isprint value="${RefinementValue.getHitCount()}"/>)</span>
											</a>
										</li>
									<iselse/><!---
									---><li><!---
                                        ---><isscript>
                                                var refineUrl = productSearchResult.urlRefineAttributeValue('Search-Show',RefinementValue.getID(),RefinementValue.getValue());
                                            </isscript><!---
										--->
										<iscomment>I'm adding at the end the raStartPrice parameter since it isn't really a sorting rule and otherwise I wasn't able to keep it in the url</iscomment>
										<a href="${unsanitizeOR(refineUrl)} <isif condition="${!empty(pdict.CurrentHttpParameterMap.raStartPrice.doubleValue)}">&raStartPrice=${pdict.CurrentHttpParameterMap.raStartPrice.doubleValue}</isif>" title="${Resource.msg('search.productsearchrefinebar.clickrefine','search',null)}${RefinementValue.getDisplayValue()}">
												<div class="checkbox"><div class="checkbox-inner"><!-- checkbox --></div></div>
												<isprint value="${RefinementValue.getDisplayValue()}"/>
												<span class="count">(<isprint value="${RefinementValue.getHitCount()}"/>)</span>
											</a>
										</li>
									</isif><!---
							---></isloop>
                                <li class="notfound">
                                    <isprint value="${Resource.msgf('search.productsearchrefinebar.notfound','search',null,RefinementDefinition.getDisplayName())}"/>
                                </li>
						    </ul>

							<isfreetextsearch showifgreaterthan="${refinements.getAllRefinementValues(RefinementDefinition).length}" placeholder="${RefinementDefinition.getDisplayName()}"/>
						</div>
					</isif>

					<iscomment>PRICE REFINEMENTS</iscomment>
					<isif condition="${RefinementDefinition.isPriceRefinement() && empty(pdict.PermanentPriceMax) && empty(pdict.PermanentPriceMin)}">

						<h3 class="refinement-headline toggle">
							<isprint value="${RefinementDefinition.getDisplayName()}"/>
						</h3>

						<ul class="refinement-list toggle-content">
							<isloop items="${refinements.getAllRefinementValues(RefinementDefinition)}" var="RefinementValue">
								<isif condition="${productSearchResult.isRefinedByPriceRange(RefinementValue.valueFrom,RefinementValue.valueTo)}">
									<li class="selected"><!---
                                    ---><isscript>
                                            var relaxUrl = productSearchResult.urlRelaxPrice('Search-Show');
                                        </isscript><!---
									---><a class="refinement-link" title="${Resource.msg('global.remove','locale',null)} ${RefinementValue.getDisplayValue()}" href="${unsanitizeOR(relaxUrl)}">
											<div class="checkbox"><div class="checkbox-inner"><!-- checkbox --></div></div>
											<isprint value="${RefinementValue.getDisplayValue()}"/>
                                            <span class="count">(<isprint value="${RefinementValue.getHitCount()}"/>)</span>
										</a>
									</li>
								<iselse/>
									<li><!---
                                    ---><isscript>
                                            var refineUrl = productSearchResult.urlRefinePrice('Search-Show',RefinementValue.getValueFrom(),RefinementValue.getValueTo());
                                    	</isscript><!---
									---><a class="refinement-link" title="${RefinementValue.getDisplayValue()}" href="${unsanitizeOR(refineUrl)}">
											<div class="checkbox"><div class="checkbox-inner"><!-- checkbox --></div></div>
											<isprint value="${RefinementValue.getDisplayValue()}"/>
                                            <span class="count">(<isprint value="${RefinementValue.getHitCount()}"/>)</span>
										</a>
									</li>
								</isif>
							</isloop>
						</ul>

					</isif>

					</div>
				</isloop>


				<!--- Special Rising Auction Refinements --->
				<!--- ---------------------------------- --->
				<isif condition="${pdict.IsAuctionShop && empty(pdict.PermanentRemainingTime)}">
					<isscript>
                        var remainingTimeRefinementValues = 'raRemainingTimeRefinement' in dw.system.Site.current.preferences.custom ? dw.system.Site.current.getCustomPreferenceValue('raRemainingTimeRefinement') : [];
                    </isscript>
					<isif condition="${!empty(remainingTimeRefinementValues)}">
						<div class="refinement remaining-time">
							<h3 class="refinement-headline toggle">
								${Resource.msg('risingauctions.refinement.remainingtime.headline', 'risingauctions', null)}
							</h3>

							<ul class="refinement-list toggle-content">
								<isloop items="${remainingTimeRefinementValues}" var="RefinementValue">
									<isscript>
										var valueSelected = pdict.RemainingTime && RefinementValue === pdict.RemainingTime.toString(),
                                            remainingTimeRefinementDisplayValue = Resource.msgf('risingauctions.refinement.remainingtime.value', 'risingauctions', null, RefinementValue);
									</isscript>
                                    <isif condition="${valueSelected}">
                                        <li class="selected"><!---
                                        ---><isscript>
                                                var relaxUrl = productSearchResult.urlRelaxAttributeValue('Search-Show','remainingTime',RefinementValue);
                                            </isscript><!---
                                        --->
                                        <iscomment>I'm adding at the end the raStartPrice parameter since it isn't really a sorting rule and otherwise I wasn't able to keep it in the url </iscomment>
                                        <a class="refinement-link" title="${Resource.msg('search.productsearchrefinebar.clickrefine','search',null)}${remainingTimeRefinementDisplayValue}" href="${unsanitizeOR(relaxUrl)} <isif condition="${!empty(pdict.CurrentHttpParameterMap.raStartPrice.doubleValue)}">&raStartPrice=${pdict.CurrentHttpParameterMap.raStartPrice.doubleValue}</isif>">
                                                <div class="checkbox"><div class="checkbox-inner"><!-- checkbox --></div></div>
                                                <isprint value="${remainingTimeRefinementDisplayValue}"/>
                                            </a>
                                        </li>
                                    <iselse/>
                                        <li><!---
                                        ---><isscript>
                                                var refineUrl = productSearchResult.urlRefineAttribute('Search-Show','remainingTime',RefinementValue);
                                            </isscript><!---
                                        --->
                                        <iscomment>I'm adding at the end the raStartPrice parameter since it isn't really a sorting rule and otherwise I wasn't able to keep it in the url</iscomment>
                                        <a class="refinement-link" title="${Resource.msg('search.productsearchrefinebar.clickrefine','search',null)}${remainingTimeRefinementDisplayValue}" href="${unsanitizeOR(refineUrl)} <isif condition="${!empty(pdict.CurrentHttpParameterMap.raStartPrice.doubleValue)}">&raStartPrice=${pdict.CurrentHttpParameterMap.raStartPrice.doubleValue}</isif>">
                                                <div class="checkbox"><div class="checkbox-inner"><!-- checkbox --></div></div>
                                                <isprint value="${remainingTimeRefinementDisplayValue}"/>
                                            </a>
                                        </li>
                                    </isif>
								</isloop>
							</ul>
						</div>
					</isif>
				</isif>
			</div>
		</div>
	</div>
	<iselseif condition="${pdict.ProductSearchResult.count == 0}">
		<isinclude template="search/components/categoryrefinebar"/>
	</isif>
</isif>