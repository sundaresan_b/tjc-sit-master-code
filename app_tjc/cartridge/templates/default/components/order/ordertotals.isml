<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	This template is used to render the order totals for a basket or an order.

    Parameters:
    p_lineitemctnr     : the line item container to render (this could be either an order or a basket as they are both line item containers)
    p_showshipmentinfo : boolean that controls if individual shipment information is rendered or if aggregate totals are rendered
    p_shipmenteditable : boolean that controls if the shipment should have an edit link
    p_shipmentmethods  : boolean that controls if the shipment is replaced by an shipping method selection
    p_totallabel       : label to use for the total at bottom of summary table
</iscomment>

<iscomment>Create page varibale representing the line item container</iscomment>
<isset name="LineItemCtnr" value="${pdict.p_lineitemctnr}" scope="page"/>

<isif condition="${!empty(LineItemCtnr)}">
	<table class="order-totals-table">
		<tbody>
			<iscomment>
				render order subtotal if there are both contained in the cart, products and gift certificates
				(product line item prices including product level promotions plus gift certificate line items prices)
			</iscomment>
			<tr class="order-subtotal">
				<isif condition="${!pdict.ams}">
					<td>${Resource.msg('order.ordersummary.subtotal','order',null)}</td>
					<td><isprint value="${LineItemCtnr.getAdjustedMerchandizeTotalPrice(false).add(LineItemCtnr.giftCertificateTotalPrice)}"/></td>
				</isif>
			</tr>

			<iscomment>calculate order level & shipping discounts</iscomment>
			<isscript>
				var isTJCPlusSubscriptionOrder = false;
				var plis = LineItemCtnr.productLineItems;
				if(plis.length == 1 && plis[0].product.custom.isTJCPlusProduct){
					isTJCPlusSubscriptionOrder = true;
				}
				
				if(!pdict.ams)
				{
					var merchTotalExclOrderDiscounts : dw.value.Money = LineItemCtnr.getAdjustedMerchandizeTotalPrice(false);
					var merchTotalInclOrderDiscounts : dw.value.Money = LineItemCtnr.getAdjustedMerchandizeTotalPrice(true);
					var orderDiscount : dw.value.Money = merchTotalExclOrderDiscounts.subtract( merchTotalInclOrderDiscounts );
                    var shippingExclDiscounts : dw.value.Money = LineItemCtnr.shippingTotalPrice;
                    var shippingInclDiscounts : dw.value.Money = LineItemCtnr.getAdjustedShippingTotalPrice();
                    var shippingDiscount : dw.value.Money = shippingExclDiscounts.subtract( shippingInclDiscounts );
				}
			</isscript>

			<isif condition="${!empty(orderDiscount) && orderDiscount.value > 0.0}">
				<tr class="order-discount discount">
					<td>${Resource.msg('order.ordersummary.orderdiscount','order',null)}</td>
					<td>- <isprint value="${orderDiscount}"/></td>
				</tr>
			</isif>

            <isif condition="${!pdict.ams}">
	            <isif condition="${pdict.p_shipmentmethods}">
			        <tr class="order-shipping shipping-method-selection">
	                    <td colspan="2">
	                        <isinputfield formfield="${pdict.CurrentForms.cart.selectedShippingMethodID}" type="select" rowClass="form-row-auto form-row-no-margin xlt-shippingMethod validation-success"/>

                            <iscomment>check if the delivery time cut-off message needs to be displayed</iscomment>
                            <isscript>
                                var selectedShippingMethod = LineItemCtnr.defaultShipment.shippingMethod;
                                var deliveryDay = selectedShippingMethod.custom.deliveryDay.value;
                                var cutOffDay = selectedShippingMethod.custom.cutOffDay.value;
                                var cutOffTime = selectedShippingMethod.custom.cutOffTime;
                                var cutOffMessage = selectedShippingMethod.custom.cutOffMessage;
                                var currentCalendar = new dw.util.Calendar();
                                var currentDay = currentCalendar.get(dw.util.Calendar.DAY_OF_WEEK);
                                var currentHour = currentCalendar.get(dw.util.Calendar.HOUR_OF_DAY);
                                var showMessage = false;
                                var hasPreOrderProducts = false;
						        var productLis = LineItemCtnr.allProductLineItems.iterator();
						        var product;
						        var availabilityStatus;

                                if (!empty(deliveryDay) && !empty(cutOffDay) && !empty(cutOffTime) && !empty(cutOffMessage)) {
                                    if (currentDay == cutOffDay && currentHour >= cutOffTime) {
                                        showMessage = true;
                                    } else if (deliveryDay > cutOffDay && currentDay <= deliveryDay && currentDay > cutOffDay) {
                                        showMessage = true;
                                    } else if (deliveryDay < cutOffDay && (currentDay > cutOffDay || currentDay <= deliveryDay)) {
                                        showMessage = true;
                                    }
                                }
                               	while (productLis.hasNext()) {
									product = productLis.next().product;
									availabilityStatus = product.availabilityModel.availabilityStatus;
									if (availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER) {
										hasPreOrderProducts = true;
										showMessage = true;	
									}
								}
                                
                            </isscript>

                            <isif condition="${showMessage}">
                                <div class="shipping-method-message">
                                	<isif condition="${hasPreOrderProducts}">
										${Resource.msg('order.ordersummary.preorderproducts','order',null)}
                                	<iselse>
	                                    ${cutOffMessage}
                                	</isif>
                                </div>
                            </isif>

                            <isset name="reachedPnpMax" value="${pdict.Basket.defaultShipment.getStandardShippingLineItem().custom.reachedPnpMax}" scope="page" />
                            <isset name="standardPnPChargingMaximum" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('standardPnPChargingMaximum')}" scope="page" />
                            <isset name="standardPnPChargingMaximumFormated" value="${dw.util.StringUtils.formatMoney(new dw.value.Money(standardPnPChargingMaximum, session.getCurrency().getCurrencyCode()))}" scope="page" />

                            <isif condition="${shippingInclDiscounts.value == 0}">
		                        <div class="shipping-method-message free-shipping-msg"> 
		                        	${Resource.msgf('order.ordersummary.freeshipping','order',null,standardPnPChargingMaximumFormated)}
		                        </div>
		                    <iselseif condition="${reachedPnpMax}">
		                        <div class="shipping-method-message max-pnp-reached-msg">
		                        	${Resource.msgf('order.ordersummary.maxpnpreached','order',null,standardPnPChargingMaximumFormated)}
		                        </div>
		                    </isif>
		                </td>
	                </tr>
				<iselseif condition="${pdict.p_showshipmentinfo}"/>
					<iscomment>the url to edit shipping depends on the checkout scenario</iscomment>
					<isset name="editUrl" value="${URLUtils.https('COAddresses-Start')}" scope="page"/>
					<isloop items="${LineItemCtnr.shipments}" var="Shipment" status="loopstatus">
						<iscomment>show shipping cost per shipment only if it's a physical shipment containing product line items</iscomment>
						<isif condition="${Shipment.productLineItems.size() > 0}">
						<tr class="order-shipping <isif condition="${loopstatus.first}"> first <iselseif condition="${loopstatus.last}"> last</isif>">
							<td>
								<isif condition="${pdict.p_shipmenteditable}">
									<a href="${editUrl}" title="${Resource.msg('order.ordersummary.ordershipping.edit','order',null)}">${Resource.msg('order.ordersummary.ordershipping.edit','order',null)}</a>
								</isif>
								<!--  SHIPPING NAME -->
								<isif condition="${!empty(Shipment) && !empty(Shipment.shippingMethod)}">
	                                <isprint value="${Shipment.shippingMethod.displayName}"/>
	                            </isif>
	                        </td>
							<td>
								<isif condition="${LineItemCtnr.shippingTotalPrice && LineItemCtnr.shippingTotalPrice.available}">
									<isprint value="${Shipment.shippingTotalPrice}"/>
								<iselse/>
									${Resource.msg('order.ordersummary.nodata','order',null)}
								</isif>
							</td>
						</tr>
						</isif>
					</isloop>
				<iselse/>
					
					<isif condition="${!empty(isTJCPlusSubscriptionOrder) && !isTJCPlusSubscriptionOrder}"> 
						<tr class="order-shipping">
							<td>
								<!-- Display Shipping Method -->
								<isset name="Shipment" value="${LineItemCtnr.shipments.iterator().next()}" scope="page"/>
								<isif condition="${!empty(Shipment) && !empty(Shipment.shippingMethod)}">
									<isprint value="${Shipment.shippingMethod.displayName}"/>
								</isif>
							</td>
							<td>
								<isif condition="${LineItemCtnr.shippingTotalPrice && LineItemCtnr.shippingTotalPrice.available}">
									<isprint value="${LineItemCtnr.shippingTotalPrice}"/>
								<iselse/>
									${Resource.msg('order.ordersummary.nodata','order',null)}
								</isif>
							</td>
						</tr>
					</isif>
				</isif>
				<isif condition="${!empty(shippingDiscount) && shippingDiscount.value > 0.0 && !empty(isTJCPlusSubscriptionOrder) && !isTJCPlusSubscriptionOrder}">
					<isif condition="${pdict.p_showshipmentinfo}">
						<tr class="order-shipping-discount discount">
							<td>${Resource.msg('order.ordersummary.ordershippingdiscount','order',null)}</td>
							<td>- <isprint value="${shippingDiscount}"/></td>
						</tr>
					<iselse/>
						<tr class="order-shipping-discount discount">
							<td>${Resource.msg('order.ordersummary.ordershippingdiscount','order',null)}</td>
							<td>- <isprint value="${shippingDiscount}"/></td>
						</tr>
					</isif>
				</isif>
			</isif>

			<iscomment>order total</iscomment>
			<tr class="order-total">
					<isif condition="${LineItemCtnr.totalGrossPrice.available}">
					 	<isset name="orderTotalValue" value="${LineItemCtnr.totalGrossPrice}" scope="page"/>
					<iselse/>
						<isset name="orderTotalValue" value="${LineItemCtnr.getAdjustedMerchandizeTotalPrice(true).add(LineItemCtnr.giftCertificateTotalPrice)}" scope="page"/>
					</isif>
				<td><span class="order-totals-heading"><isprint value="${pdict.p_totallabel}"/></span><span class="order-totals-budgetpay-heading">${Resource.msg('global.paidwithbp.ordertotal','locale',null)}</td></span></td>
				<td><isprint value="${orderTotalValue}"/></td>
			</tr>
			<iscomment>
				Display BP today you pay total containing orderTotal - (installments * number of installments left)
			</iscomment>
			<isif condition="${!empty(pdict.p_budgetpay) && pdict.p_budgetpay.available}">
				<tr class="today-you-pay-total order-total order-totals-budgetpay">
					<isset name="todayYouPayValue" value="${orderTotalValue.subtract(pdict.p_budgetpay.installment.multiply(pdict.p_budgetpay.installments - 1))}" scope="page"/>
					<td><isprint value="${pdict.p_todayyoupaylabel}"/></td>
					<td><isprint value="${todayYouPayValue}"/></td>
				</tr>
			</isif>
		</tbody>
		<iscomment>
			Display BP order totals here if cart does not contain both BP & Non BP items
		</iscomment>
		<isif condition="${!empty(pdict.p_budgetpay) && !pdict.p_budgetpay.containsBPAndNonBP}">
			<isbudgetpayordertotals p_budgetpay="${pdict.p_budgetpay}" />
		</isif>		
	</table>
</isif>