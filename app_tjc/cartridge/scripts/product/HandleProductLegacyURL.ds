/**
 * The script handles a legacy product URL and parses out the product id.
 * The id is used to redirect to the correct product URL in the new shop.
 * If the product is offline, the script redirects to the primary URL. If the
 * product doesn't exist or the primary category is offline/doesn't exist,
 * the script redirects to the Homepage.
 * 
 * @input LegacyURL : String - the legacy URL
 * @output Location : String - the location to redirect
 */
importPackage( dw.system );
importPackage( dw.catalog );
importPackage( dw.web );

function execute( args : PipelineDictionary ) : Number
{
	var legacyURL : String = args.LegacyURL;
	
	if (legacyURL && legacyURL.indexOf("ProductDetail.aspx?StockCode=") != -1) {
		var productId : String;
		try {
			var parameters = legacyURL.split("?")[1].split("&"),
				parameter : String;
			for each (parameter in parameters) {
				if (parameter.indexOf("StockCode=") != -1) {
					productId = parameter.split("=")[1];
				}
			}
		}
		catch (e) {
			return PIPELET_ERROR;
		}
	
		var product : Product = ProductMgr.getProduct(productId);
		if (product && product.online) {
			args.Location = URLUtils.https('Product-Show', 'pid', productId).toString();
		}
		else {
			// If the product is not online, redirect to the primary category if online and existing
			if (product && product.getPrimaryCategory() && product.getPrimaryCategory().online) {
				args.Location = URLUtils.https('Search-Show', 'cgid', product.getPrimaryCategory().ID).toString();
			}
			// Redirect to the homepage
			else {
				var homepageURL : String = URLUtils.httpHome().toString();
				if (homepageURL.lastIndexOf('/') != homepageURL.length-1) {
					homepageURL += '/';
				}
				args.Location = homepageURL;
			}
		}
	}
	else {
		return PIPELET_ERROR;
	}
	
    return PIPELET_NEXT;
}
