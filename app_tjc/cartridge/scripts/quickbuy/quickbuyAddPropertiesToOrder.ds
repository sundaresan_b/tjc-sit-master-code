/**
*   @input Order : dw.order.Order
*   @input BasketID : String
*   @input IsPaypalCheckout : Boolean
*/
importPackage( dw.system );
importPackage( dw.order );
importPackage( dw.customer );
importPackage( dw.catalog );
importPackage( dw.util );
importPackage( dw.web );

function execute( args : PipelineDictionary ) : Number
{
    var order : Order = args.Order,
        customer : Customer = order.customer;
    
    order.custom.isGuestCheckout = !customer.registered;
    
    var cookies = request.getHttpCookies(); 
    
    //workaround for TJC-826
    if (cookies && cookies['channel'] == null && cookies['morph_sessionId']  && cookies['morph_sessionId'].value) {
    	order.custom.deviceType = 'iOS'; 
    }
    
    if (cookies && cookies['channel'] && cookies['channel'].value) {
    	order.custom.deviceType = cookies['channel'].value; 
    }
    
    //fill in firstname and lastname of the order addresses
    if (!empty(args.IsPaypalCheckout) && args.IsPaypalCheckout == true && customer.registered) {
    	order.custom.amsCustomerId = customer.profile.custom.amsCustomerId;
    } else { 	
	    if (customer.registered) { 
		    order.billingAddress.firstName = customer.profile.firstName;
		    order.billingAddress.lastName = customer.profile.lastName;
		    order.billingAddress.phone = customer.profile.phoneHome;
		    order.defaultShipment.shippingAddress.firstName = customer.profile.firstName;
		    order.defaultShipment.shippingAddress.lastName = customer.profile.lastName;
		    order.defaultShipment.shippingAddress.phone = customer.profile.phoneHome;
	    	order.custom.amsCustomerId = customer.profile.custom.amsCustomerId;
	    } else { 
	        order.defaultShipment.shippingAddress.firstName = order.billingAddress.firstName;
	        order.defaultShipment.shippingAddress.lastName = order.billingAddress.lastName;
	    }
    }
    
    var allProductLineItems : Iterator = order.allProductLineItems.iterator();
   
    while(allProductLineItems.hasNext()){
        var pli : ProductLineItem = allProductLineItems.next();
        var product = pli.product;
    	    
        if (empty(product)) continue;
        
        if (!empty(product.custom.markID)){
            pli.custom.markID = product.custom.markID;
        }
        pli.custom.basketID = args.BasketID + '_' + pli.getPosition();
        
        if(product.getAvailabilityModel() != null && product.getAvailabilityModel().getAvailabilityStatus() == ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER){
        	pli.custom.isPreorder = true;        
        }
        if(pli.isBonusProductLineItem() == true){
        	pli.custom.isFreeItem = true;
        }
        
    }
    
    return PIPELET_NEXT;
}
