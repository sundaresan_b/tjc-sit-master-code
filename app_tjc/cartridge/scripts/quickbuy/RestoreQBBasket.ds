/** Restore old basket once quickbuy journey is completed or aborted in between
*  @input Customer : dw.customer.Customer
*  @output tempQbBasket : dw.util.ArrayList
*/

importPackage( dw.util );
importPackage( dw.order );
importPackage( dw.system );

function execute( args : PipelineDictionary ) : Number
{	
	if (session.custom.qbBasket) {
		try {
				var Transaction = require('dw/system/Transaction');
			    var BasketMgr = require('dw/order/BasketMgr');
				var ProductListIterator : Iterator;
			    var CurrentProductLists : dw.customer.ProductLists;
			    var Customer = customer;
				    CurrentProductLists = dw.customer.ProductListMgr.getProductLists(Customer,dw.customer.ProductList.TYPE_CUSTOM_3,'QuickBuy');
			    var ltvProductList = findQBProductList(CurrentProductLists); 
			    var basket : dw.order.Basket = BasketMgr.getCurrentOrNewBasket();
			    var restoreBasket = false;
			    var qbBasketForSession : ArrayList = new ArrayList();
				if( !empty(ltvProductList) && (ltvProductList.items.length > 0)) {
					var productLineItemsList : ArrayList = new dw.util.ArrayList(ltvProductList.items);
					Transaction.begin();
				    try {
						for each (var productLineItem in productLineItemsList) {
							if ('customAttributesStoredatProductListItem' in productLineItem.custom && !empty(productLineItem.custom.customAttributesStoredatProductListItem) ) {
									var productDetail = JSON.parse(productLineItem.custom.customAttributesStoredatProductListItem);
									if ( 'nonWarrantyEngravingProduct' in productDetail && !empty(productDetail.nonWarrantyEngravingProduct) ) {
											/*restore live tv, Rising auction products and normal products*/
											var qty = new dw.value.Quantity(new Number(productDetail.productQty), '');
											productDetail.productQty = qty;
											qbBasketForSession.add(productDetail);
											restoreBasket = true;
									} else {
										/*restore engraving and warranty items*/
											var basket : dw.order.Basket = BasketMgr.getCurrentOrNewBasket();
						    	            var shipment = basket.shipments.length ? basket.shipments[0] : basket.createShipment(1);
											var pli = basket.createProductLineItem(productLineItem, shipment);
											var customObject = JSON.parse(productLineItem.custom.customAttributesStoredatProductListItem);
											var i = 0;
											for each( var key in customObject){
												var key = Object.keys(customObject)[i];
												var value = customObject[key];
												if (key == 'engravingJSON') {
													var defaultJSON : Object = {
												            engravingDetails : new dw.util.ArrayList(productLineItem.custom.engravingDetails)
												        };
														pli.custom[key] = JSON.stringify(defaultJSON);
														i++;
														continue;
												}
												if (key == "installments") {
													pli.custom[key] = parseInt(value);
													i++;
													continue;
												}
												pli.custom[key] = value;
												i++;
											}
											if ( 'isengravingitem' in  pli.custom && pli.custom.isengravingitem){
												var engravingDetails= [];
												for each(var engravingDetailsArray in productLineItem.custom.engravingDetails){
													engravingDetails.push(engravingDetailsArray);
												}
												pli.custom.engravingDetails = engravingDetails;
											}
											//var customAttributeDefinition = productLineItem.custom.customAttributesStoredatProductListItem 
											pli.setQuantityValue(productLineItem.quantityValue);
											var realPrice : Number = productLineItem.product.getPriceModel().price.value;
											pli.setPriceValue(realPrice);
											restoreBasket = true;
									
									}
							} else {
										var basket : dw.order.Basket = BasketMgr.getCurrentOrNewBasket();
					    	            var shipment = basket.shipments.length ? basket.shipments[0] : basket.createShipment(1);
										var pli = basket.createProductLineItem(productLineItem, shipment);
										pli.setQuantityValue(productLineItem.quantityValue);
										var realPrice : Number = productLineItem.product.getPriceModel().price.value;
										pli.setPriceValue(realPrice);
										restoreBasket = true;
							}
						}
						dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
						if (restoreBasket) {
							dw.customer.ProductListMgr.removeProductList(ltvProductList);
						}
						Transaction.commit();
						session.custom.qbBasket = null;	
				    } catch (e) {
				        Transaction.rollback();
				        throw new Error('Cannot restore the quick buy basket: ' + e.message);
				        return PIPELET_ERROR;
				    }
				}
			} catch(e) {
				var error = Logger.error('RestoreQBBasket : Error occurred  ' + e.stack + '  Error: ' + e.message );
				return PIPELET_ERROR;
			}
			args.tempQbBasket = qbBasketForSession;
			session.custom.qbBasket = null;
	        return PIPELET_NEXT;
	}
}


function findQBProductList (ActiveProductLists : ProductLists) : ProductList {
	
	// Ensure product list has products
	if (ActiveProductLists.length == 0)
		return null;

	// Return product list if it is PAYRUNNER-specific
	if (ActiveProductLists.length > 0) {
		ProductListIterator =  ActiveProductLists.iterator();
		while (ProductListIterator.hasNext()) {
			var productList : ProductList = ProductListIterator.next();
			if (productList.eventType == "QuickBuy" && productList.items.length > 0)  {
				return productList;	
			}  
		}
 	}	
}