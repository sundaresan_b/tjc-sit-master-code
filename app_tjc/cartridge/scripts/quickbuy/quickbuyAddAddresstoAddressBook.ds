/**
 * AddAddressToAddressBook.ds
 *
 * This script attempts to add the given order address to the 
 * address book of the current customer profile.
 * The address attribute "city" is used to generate the address 
 * ID within the address book. 
 *
 * @input Profile : dw.customer.Profile
 * @input OrderAddress : dw.order.OrderAddress
 */
importPackage( dw.customer );
importPackage( dw.order );
importPackage( dw.system );
importPackage( dw.web ); 

importScript( "account/Utils.ds" );

function execute( pdict : PipelineDictionary ) : Number
{
	var addressBook : AddressBook = pdict.Profile.addressBook;
	var usedAddress : OrderAddress = pdict.OrderAddress;
	
	if(usedAddress == null)
	{
		Logger.debug("Cannot add address to address book, without given order address.");
		return PIPELET_ERROR;
	}
	
	if(getEquivalentAddress(addressBook, usedAddress) == null)
	{
		var addressID : String = determineUniqueAddressID(usedAddress.city, addressBook);
		if(empty(addressID))
		{
			Logger.debug("Cannot add address to address book, with empty address ID.");
			return PIPELET_ERROR;
		}
		
		var address : CustomerAddress = addressBook.createAddress(addressID);
		address.countryCode = usedAddress.countryCode.value;
		address.postalCode = usedAddress.postalCode;
		address.city = usedAddress.city;
		address.address1 = usedAddress.address1;
		address.address2 = usedAddress.address2;
		address.custom.address3 = usedAddress.custom.address3;
		address.custom.address4 = usedAddress.custom.address4;
		address.custom.locality = usedAddress.custom.locality;
	}
    return PIPELET_NEXT;
}