	/**
 *
 * This script adds the BudgetPay attributes to the Lineitems OCAPI response
 *
 */

var Logger = require("dw/system").Logger;
var BasketMgr = require("dw/order").BasketMgr; 
var HookMgr = 	require("dw/system").HookMgr;
var Site = require('dw/system/Site');
var ArrayList = require('dw/util/ArrayList');
importScript("cart/BudgetPayUtils.ds");

exports.afterPOST = function( basket, items ) {
	if (!empty(basket) && !empty(items)) {
		for(var i = 0; i < items.length; i++){
			var item = items[i];
			var pid = item.product_id;
			var productLI = basket.getProductLineItems(pid)[0];
			if (!empty(productLI.product.custom.installments) && productLI.product.custom.installments > 0) {
				productLI.custom.installments = productLI.product.custom.installments;
			}
		}
		var quantity;
		for each (var productLineItem in basket.productLineItems) {
			var product = productLineItem.product;
			if (product.isVariant()) {
				product = product.getMasterProduct();
			}  
			var category;
			if ( product.primaryCategory ) {
				category = product.primaryCategory;
			} else if ( !empty(product.categories) ) {
				category = product.categories[0];
			}
			var itemSource;
			if (empty(productLineItem.custom.itemSource)) {
			    if ( !empty(category) ) {
				    itemSource = category.custom.itemSource;
			    } else {
				    itemSource = 'FIXED';
			    }
			    productLineItem.custom.itemSource = itemSource;
			}
			if (category.custom.ibmDigitalAnalyticsCategoryID) {
				productLineItem.custom.ibmDigitalAnalyticsCategoryID = category.custom.ibmDigitalAnalyticsCategoryID;
			}
			productLineItem.custom.basketID = basket.UUID + '_' + productLineItem.getPosition();
			var context = getShopContextByCategory(category);
			if (context === 'auctionshop') {
				var RisingAuctionMgr = require('app_tjc/cartridge/scripts/risingauctions/manager/RisingAuctionMgr');
				var mgr =  new RisingAuctionMgr();
				var HighestBid = mgr.searchHighestBidByAuction(product.custom.raAuctionCode, product.ID);
				productLineItem.custom.itemSource = 'RISING';
				productLineItem.custom.auctionCode = product.custom.raAuctionCode;
				productLineItem.setPriceValue(HighestBid.bid.getHighestBid());
				
				if (!empty(product) && !empty(product.custom.raAuctionCode) && !empty(product.custom.raMainProductCode)){
					productLineItem.custom.raMainProductCode = product.custom.raMainProductCode;
				}
			}
			
			var limit = '',totalWithoutRisingAuctions = 0;
			if (productLineItem.custom.itemSource !== 'RISING') {
			  totalWithoutRisingAuctions += quantity;
			}
			if (empty(productLineItem.custom.auctionCode)) {
				limit = Site.getCurrent().getCustomPreferenceValue('fixedPriceProductLimit');
			} else if (productLineItem.custom.itemSource == 'TV' || productLineItem.custom.itemSource == 'TGTV') {
				limit = Site.getCurrent().getCustomPreferenceValue('liveTVProductLimit');
			} else if (productLineItem.custom.itemSource == 'WEBC' || productLineItem.custom.itemSource == 'TGWEBC') {
				limit = Site.getCurrent().getCustomPreferenceValue('missedAuctionProductLimit');
			}
			
			if (limit && productLineItem.quantityValue > limit) {
				productLineItem.setQuantityValue(limit);
			}
			
			if (!empty(product.custom.pnp)){
				productLineItem.custom.pnpBaseValue = product.custom.pnp;
				productLineItem.custom.pnp = product.custom.pnp * productLineItem.quantityValue;	
			} else {
				productLineItem.custom.pnpBaseValue = Site.getCurrent().getCustomPreferenceValue('fallbackPnp');
				productLineItem.custom.pnp = Site.getCurrent().getCustomPreferenceValue('fallbackPnp') * productLineItem.quantityValue; 
			}
			
			
			if (productLineItem.custom.iswarrantySelected) {
				var qty = productLineItem.getQuantityValue();
				if (!productLineItem.custom.warrantyQty) {
					productLineItem.custom.warrantyQty = qty;
				}
				var warrantyProducts = basket.getProductLineItems(productLineItem.custom.linkedWarrantyProduct);
				for each (var warrantyProduct in warrantyProducts) {
					if (warrantyProduct.custom.linkedSelectedWarrantyProduct == productLineItem.productID) {
						if (!productLineItem.custom.warrantyQty) {
							warrantyProduct.setQuantityValue(qty);
						}
						warrantyProduct.setPriceValue(productLineItem.product.custom.warrantyProductPrice);
					}
				}
				
			}
		}
		checkWarrantyQuantity(basket);
	} else {
		return dw.system.Status(dw.system.Status.ERROR); 
	} 
};


exports.modifyPOSTResponse = function( basket, basketResponse, productItems ) {
	if (!empty(basket) && !empty(basketResponse)) {
		dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
		var budgetPayAvailable = BudgetPayUtils.checkAvailabilityOCAPI(basket);
		basketResponse.c_budgetpayAvailableForCart = budgetPayAvailable;
	} else {
		return dw.system.Status(dw.system.Status.ERROR); 
	} 
};

exports.afterPatch = function( basket, basketInput ) {
	if (!empty(basket) && !empty(basketInput)) {
		var budgetPay = BudgetPayUtils.getInstallments(basket);
		var installmentsString = '';
		if (!empty(budgetPay) && budgetPay.available) {
		
			installmentsString += "'" + budgetPay.firstInstallment.value + "', ";
			for (var i = 1; i < budgetPay.installments; i++) {
				installmentsString += "'" + budgetPay.installment.value + "'";
				if (i < budgetPay.installments - 1) {
					installmentsString += ", ";
				}
			}
			basket.custom.orderInstallments = installmentsString;
		}
	} else {
		return dw.system.Status(dw.system.Status.ERROR); 
	} 
};


function checkWarrantyQuantity(basket) {
	if(!empty(basket)){
		var plis  = basket.getAllProductLineItems();
		var sortedBasket  = new ArrayList();
		for each(var pli : ProductLineItem in plis){
					
			if(!sortedBasket.contains(pli.productID) && !pli.product.custom.isWarrantyProduct)
				sortedBasket.add(pli.productID);
			 
			if(!empty(pli.product.custom.isWarrantyEligible) && pli.product.custom.isWarrantyEligible && !empty(pli.custom.iswarrantySelected) && pli.custom.iswarrantySelected){
				var pliqty = pli.getQuantityValue();
				var warrantyplis = basket.getProductLineItems(pli.custom.linkedWarrantyProduct);
				if(warrantyplis.length > 0){
					var warrantypli : ProductLineItem = warrantyplis[0];
					if(warrantypli.getQuantityValue() > pliqty)
					{
						warrantypli.setQuantityValue(pliqty);
						pli.custom.warrantyQty = pliqty;
					}
					else if(!(warrantypli.getQuantityValue() == pli.custom.warrantyQty)){
						pli.custom.warrantyQty = warrantypli.getQuantityValue();
					}
					
					if(!sortedBasket.contains(pli.custom.linkedWarrantyProduct))
						sortedBasket.add(pli.custom.linkedWarrantyProduct);
				}
				
			}
			if ( !empty(pli.product.custom.isWarrantyEligible) && pli.product.custom.isWarrantyEligible && (basket.customer.authenticated || basket.customer.registered || basket.customer.externallyAuthenticated)&& basket.customer.profile.custom.isTJCPlusMember )
			{
				if(!empty(warrantypli) && warrantypli.getQuantityValue() < pliqty)
				{
					warrantypli.setQuantityValue(pliqty);
					pli.custom.warrantyQty = pliqty;
				}
			}
			if(!empty(pli.product.custom.isWarrantyProduct) && pli.product.custom.isWarrantyProduct && empty(pli.custom.linkedSelectedWarrantyProduct)){
				basket.removeProductLineItem(pli);
				
			}
		}
		
		if(!empty(sortedBasket)){
			for(var i=0; i<sortedBasket.length; i++){
				var pid = sortedBasket[i];
				var plis = basket.getProductLineItems(pid);
				var pli : ProductLineItem = plis[0];
				pli.setPosition(i+1);
			}
		}
			
		
		basket.updateTotals();
	}
}


var getShopContextByCategory = function(category) {
			
if (category && !category.isRoot()) {
	while (!category.getParent().isRoot()) {
			category = category.getParent();
	}
	return category.ID == "auction-navigation" ? "auctionshop" : "webshop";
}
return "webshop";
};
