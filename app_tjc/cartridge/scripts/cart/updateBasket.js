'use strict';

exports.beforePOST = function(basket) {
	for each (var productLineItem in basket.productLineItems) {
		var product = productLineItem.product;
		if (product.isVariant()) {
			product = product.getMasterProduct();
		}  
		var category;
		if ( product.primaryCategory ) {
			category = product.primaryCategory;
		} else if ( !empty(product.categories) ) {
			category = product.categories[0];
		}
		productLineItem.custom.basketID = basket.UUID + '_' + productLineItem.getPosition();
		var context = getShopContextByCategory(category);
		if (context === 'auctionshop') {
			var RisingAuctionMgr = require('app_tjc/cartridge/scripts/risingauctions/manager/RisingAuctionMgr');
			var mgr =  new RisingAuctionMgr();
			var HighestBid = mgr.searchHighestBidByAuction(product.custom.raAuctionCode, product.ID);
			productLineItem.custom.itemSource = 'RISING';
			productLineItem.custom.auctionCode = product.custom.raAuctionCode;
			productLineItem.setPriceValue(HighestBid.bid.getHighestBid());
			
			if (!empty(product) && !empty(product.custom.raAuctionCode) && !empty(product.custom.raMainProductCode)){
				productLineItem.custom.raMainProductCode = product.custom.raMainProductCode;
			}
		}
		var itemSource;
		if (empty(productLineItem.custom.itemSource)) {
		    if ( !empty(category) ) {
			    itemSource = category.custom.itemSource;
		    } else {
			    itemSource = 'FIXED';
		    }
		    productLineItem.custom.itemSource = itemSource;
		}
		if( 'auctionCode' in productLineItem.custom && !empty(productLineItem.custom.auctionCode) && (productLineItem.custom.itemSource == 'WEBC' || productLineItem.custom.itemSource == 'TGWEBC')  ) {
			if ('laAuctionCode' in productLineItem.product.custom && !empty(productLineItem.product.custom.laAuctionCode) && !empty(productLineItem.custom.auctionCode) && !productLineItem.custom.auctionCode.equals(productLineItem.product.custom.laAuctionCode) ) {
				productLineItem.custom.auctionCode = productLineItem.product.custom.laAuctionCode;
			} else if(empty(productLineItem.product.custom.laAuctionCode)) {
				productLineItem.custom.auctionCode = productLineItem.product.custom.laAuctionCode;
				productLineItem.custom.itemSource = 'FPC';
			}
		}
		if (category.custom.ibmDigitalAnalyticsCategoryID) {
			productLineItem.custom.ibmDigitalAnalyticsCategoryID = category.custom.ibmDigitalAnalyticsCategoryID;
		}
	}
}
var getShopContextByCategory = function(category) {
	
if (category && !category.isRoot()) {
	while (!category.getParent().isRoot()) {
			category = category.getParent();
	}
	return category.ID == "auction-navigation" ? "auctionshop" : "webshop";
}
return "webshop";
}