var ProductMgr = require("dw/catalog").ProductMgr;
var PromotionMgr = require("dw/campaign").PromotionMgr;
var HashMap = require("dw/util").HashMap;

exports.modifyGETResponse_v2 = function(customer,productList,customerProductListItemResultResponse) {
	var productListData = customerProductListItemResultResponse.data;
	for (var i=0; i < productListData.length; i++ ) {
		// promotions
		var product = ProductMgr.getProduct(productListData[i].product_id);
		var productPromotions = PromotionMgr.getActiveCustomerPromotions().getProductPromotions(product);
		var promotionMessages = new Array();
		var promotionalPrice = null;
		var promotionalPriceHashMap = new Object();
		for (var j=0; j < productPromotions.length; j++ ) {
			var promotion = productPromotions[j];

			if (!empty(promotion.calloutMsg)) {
				promotionMessages.push(promotion.calloutMsg.source);
			}
			if(promotion.getPromotionalPrice(product).valueOrNull != null){
				promotionalPrice = promotion.getPromotionalPrice(product);
			}
		}

		if(('listPricebookId' in dw.system.Site.current.preferences.custom) && !empty(dw.system.Site.current.preferences.custom.listPricebookId))
			var StandardPrice = product.getPriceModel().getPriceBookPrice(dw.system.Site.current.preferences.custom.listPricebookId);
		else
			var StandardPrice = product.getPriceModel().getPriceBookPrice('list-prices');
		var SalesPrice = product.getPriceModel().getPrice();
		
		if (promotionMessages.length > 0) {
			productListData[i].product.c_promotions = promotionMessages;
		}
		if (!empty(promotionalPrice) && !empty(product.priceModel.price) && promotionalPrice.value < product.priceModel.price.valueOrNull) {
			promotionalPriceHashMap['gb-gbp-list-prices'] =  product.priceModel.price.valueOrNull; 
			promotionalPriceHashMap['gb-gbp-sale-prices'] = promotionalPrice.value;
			productListData[i].product.c_prices = promotionalPriceHashMap;
			productListData[i].product.c_price = promotionalPrice.value;
		}
		else if(StandardPrice.available && SalesPrice.available && StandardPrice.getValue() > SalesPrice.getValue()){
			promotionalPriceHashMap['gb-gbp-list-prices'] = StandardPrice.getValue();
			promotionalPriceHashMap['gb-gbp-sale-prices']=  SalesPrice.getValue();
			productListData[i].product.c_prices = promotionalPriceHashMap;
			productListData[i].product.c_price = SalesPrice.getValue();
		}
		productListData[i].product.c_installments = product.custom.installments ? product.custom.installments : 0;
	}
	
}