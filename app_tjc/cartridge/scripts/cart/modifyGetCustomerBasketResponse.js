/**
 *
 * This script adds the BudgetPay attribute to the OCAPI response
 *
 */

var Logger = require("dw/system").Logger;
var BasketMgr = require("dw/order").BasketMgr; 
var HookMgr = 	require("dw/system").HookMgr;
importScript("cart/BudgetPayUtils.ds");

exports.modifyGETResponse_v2 = function( customer, customerBasketsResultResponse ) {
	var basket = BasketMgr.getCurrentBasket();

	if (!empty(basket)) {
		dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
		var budgetPayAvailable = BudgetPayUtils.checkAvailabilityOCAPI(basket);
		customerBasketsResultResponse.baskets[0].c_budgetpayAvailableForCart = budgetPayAvailable;
	} else {
		return dw.system.Status(dw.system.Status.ERROR);  
	}
};
