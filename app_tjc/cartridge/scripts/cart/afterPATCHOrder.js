var Logger = require("dw/system").Logger;
var OrderMgr = require("dw/order/OrderMgr");

exports.afterPATCH = function( order, orderInput ) {
	
	Logger.error("Order PATCH Call");
	if(!empty(order) && !empty(orderInput)){
		//check if the orderInput has c_orderFailed attribute
		if(orderInput.c_orderFailed){
			var status = OrderMgr.failOrder(order,true);
			Logger.error("Order Fail status : "+status.getStatus());
		}
	}
};
