/**
 * @input  ProductLineItem : dw.order.ProductLineItem
 * @input  CurrentCustomer : dw.customer.Customer 
 */

/*
	This scripts is to prevent the user from getting a "Out of Stock" message on the RA products.
	See Ticket TJC-485 for more information.
	As an improvement we're now checking if the RA product has already been ordered in the last 5 minutes.
	If it was, the stock isn't increased, preventing it from being purchased multiple times. 
*/

importPackage( dw.system );
importPackage( dw.order );
importPackage( dw.catalog );
importPackage( dw.util );
importPackage( dw.customer );

function execute( args : PipelineDictionary ) : Number
{ 
	var currentCustomer = args.CurrentCustomer;
	var productLineItem : ProductLineItem = args.ProductLineItem;
	
	var tempCalendar = Site.current.calendar;

    var todayEnd : Date = tempCalendar.time;  
    var hrs : Number = System.getPreferences().custom.raExpirationTimeAms;
 	//Get orders placed in the last 5 minutes
     tempCalendar.add(Calendar.HOUR, (-1* hrs)); 

    var todayBegin : Date = tempCalendar.time;
	var previousOrders : Iterator = dw.order.OrderMgr.searchOrders('customerNo = {0} AND creationDate > {1} AND creationDate < {2}', 'orderNo asc', currentCustomer.profile.customerNo, todayBegin, todayEnd);
	var restoreInventoryOrder = null;
	var availabilityModel : ProductAvailabilityModel = productLineItem.getProduct().getAvailabilityModel();
	
	var inventoryRecord : ProductInventoryRecord  = availabilityModel.getInventoryRecord();
		
	if (inventoryRecord != null && availabilityModel.getAvailabilityLevels(1).getNotAvailable().value > 0){
			while (previousOrders.hasNext()) { 
			
	    	var order = previousOrders.next();
	    	var status = order.status.value;
	    		dw.system.Logger.info(" EnsureStockForRAProducts.ds : RA order status" + order.status.value); 
			
	    	if ( status == dw.order.Order.ORDER_STATUS_CREATED && productLineItem.custom.itemSource == 'RISING'){
	    		dw.system.Logger.info(" EnsureStockForRAProducts.ds : RA product is out of stock " ); 
	    		var productLineItems = order.getAllProductLineItems().iterator();
	        	while(productLineItems.hasNext()){
	        		var placedProductLineItem : ProductLineItem = productLineItems.next();
	        		if(placedProductLineItem.custom.itemSource == 'RISING' && placedProductLineItem.productID.equals(productLineItem.productID)){ 
	        			dw.system.Logger.info(" EnsureStockForRAProducts.ds : RA product inventory restored from past created order" );
	        			//fail this order.	
	        			dw.order.OrderMgr.failOrder(order,true);
	        			
					}
	        		
	        	}
	    	}
	    	       
	    } 
	}else{
		dw.system.Logger.info(" EnsureStockForRAProducts.ds : RA product is in stock " );
	}
 
/*
	var availabilityModel : ProductAvailabilityModel = productLineItem.getProduct().getAvailabilityModel();
	
	var inventoryRecord : ProductInventoryRecord  = availabilityModel.getInventoryRecord();
	
	if(inventoryRecord != null){ 		
		var isNotAvailable = availabilityModel.getAvailabilityLevels(1).getNotAvailable().value > 0;
		
		if(isNotAvailable){
			inventoryRecord.setAllocation(1);
		}
				
	}
	else{
		return PIPELET_ERROR;
	}	
	*/
	
    return PIPELET_NEXT;
}