/**
 *
 * This script adds the BudgetPay attribute to the OCAPI response
 *
 */

var Logger = require("dw/system").Logger;
var BasketMgr = require("dw/order").BasketMgr; 
var HookMgr = 	require("dw/system").HookMgr;
var HashMap = require("dw/util/HashMap");
importScript("cart/BudgetPayUtils.ds");
importScript("product/libProduct.ds");
exports.modifyGETResponse = function( basket, basketResponse ) {
	if (!empty(basket) && !empty(basketResponse)) {
		dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
		var budgetPayAvailable = BudgetPayUtils.checkAvailabilityOCAPI(basket);
		
		basketResponse.c_budgetpayAvailableForCart = budgetPayAvailable;
		basketResponse.c_paypalCreditThreshold = dw.system.Site.current.preferences.custom.PaypalCreditThreshold;
		
		if(!empty(basket.customer) && basket.customer.authenticated && !empty(basket.customer.profile)){
			basketResponse.c_budgetpayAvailableForCustomer = !empty(basket.customer.profile.custom.budgetPayAvailable) ? basket.customer.profile.custom.budgetPayAvailable : false;
			basketResponse.c_budgetPayCreditAvailable = !empty(basket.customer.profile.custom.budgetPayCreditAmount) ? basket.customer.profile.custom.budgetPayCreditAmount : 0 ;
			basketResponse.c_storeCreditAvailable = !empty(basket.customer.profile.custom.storeCredit) ? basket.customer.profile.custom.storeCredit : 0 ;
			basketResponse.c_isTJCPlusCustomer = !empty(basket.customer.profile.custom.isTJCPlusMember) ? basket.customer.profile.custom.isTJCPlusMember : false ;
			basketResponse.c_firstName = basket.customer.profile.firstName;
			basketResponse.c_lastName = basket.customer.profile.lastName;
		}
		var produtItems = basketResponse.productItems;
		if (produtItems) {
			var productLineItemIterator = produtItems.iterator();
			while (productLineItemIterator.hasNext()) {
				var prodLineItem = productLineItemIterator.next();
				if (prodLineItem.c_itemSource == 'RISING') {
					var RisingAuctionMgr = require("../risingauctions/manager/RisingAuctionMgr"),
					RAStatus = require("app_tjc/cartridge/scripts/risingauctions/objects/RAStatus"),
					risingAuctionMgr =  new RisingAuctionMgr(),
					status =  new RAStatus();
					risingAuctionMgr =  new RisingAuctionMgr();
					var expired = risingAuctionMgr.isAuctionExpired(prodLineItem.productID)
					prodLineItem.c_isExpired = expired;
					var res = risingAuctionMgr.getCurrentCustomerBidHistory();
				    var auctionwonedProducts = res.getBidsListByStatus(status.WON.ID);
				    var remainingTime = '';
				    for each(var entry in auctionwonedProducts) {
						if (prodLineItem.productID == entry.productID) {
							var auctionEndTime = entry.endTime;
							var auctionExpirationTimeDWR = dw.system.System.preferences.custom['raExpirationTimeDwr'],
							endTime = auctionEndTime + (auctionExpirationTimeDWR * 3600000),
							remainingTime = endTime - new Date().getTime();
						}
				    }
				    prodLineItem.c_raRemainingTime = remainingTime;
				}
			}
		}
	   
	} else {
		return dw.system.Status(dw.system.Status.ERROR);  
	}
};


exports.validateBasket = function (basketResponse,duringSubmit) {
	var productItems = basketResponse.product_items;
	var productItemIterator = productItems.iterator();
	var basketQty = 0,basketLimitExceed = false;
	var Site = require('dw/system/Site');
	if (!basketResponse.customer_info.email) {
		basketResponse.addFlash({
			"type":"EmailAddressRequired",
			"message":"Email ID is Missing",
			"path": "$.basket"
		});
	}
	var productImages = new Object(),productAvailability = new Object(),productLimitExceedQtys = [],limit;
	while (productItemIterator.hasNext()) {
		var productItem = productItemIterator.next();
		var productid = productItem.product_id;
		basketQty = basketQty + productItem.quantity;
		if (empty(productItem.c_auctionCode)) {
			limit = Site.getCurrent().getCustomPreferenceValue('fixedPriceProductLimit');
		} else if (productItem.c_itemSource == 'TV' || productItem.c_itemSource == 'TGTV') {
			limit = Site.getCurrent().getCustomPreferenceValue('liveTVProductLimit');
		} else if (productItem.c_itemSource == 'WEBC' || productItem.c_itemSource == 'TGWEBC') {
			limit = Site.getCurrent().getCustomPreferenceValue('missedAuctionProductLimit');
		}
		var dynamicConfigFile = dw.system.Site.current.getCustomPreferenceValue('productImageConfig');
    	var productImageConfig = new Object();
    	if (dynamicConfigFile) {
    		productImageConfig = JSON.parse(dynamicConfigFile);
    	} else {
    		productImageConfig = {
                    producttile: {
                        width: 310,
                        height: 310,
                        scaleMode: 'fit'
                    }
                };
    	}
    	var product = dw.catalog.ProductMgr.getProduct(productid);
    	var imageData = ProductSO(product).getProductImage(productImageConfig, 'cart', 0, '', null, null, null);
         if (!empty(imageData) && !empty(imageData.url)) {
              productImages[productid] = imageData.url.toString();
          }
        
		if (limit && productItem.quantity > limit) {
			productLimitExceedQtys.push(productid);
		}
		if (product && product.availabilityModel && product.availabilityModel.inventoryRecord) {
         	var productInventoryRecord = product.availabilityModel.inventoryRecord;
         	var inventory  = {
         		"ats": productInventoryRecord.ATS.value,
       			"backorderable": productInventoryRecord.backorderable,
       			"id": "inventory",
       			"in_stock_date": productInventoryRecord.inStockDate ? (productInventoryRecord.inStockDate).toISOString() : '',
      			"orderable": product.availabilityModel.orderable,
        		"preorderable": productInventoryRecord.preorderable,
        		"stock_level": productInventoryRecord.stockLevel.value
         	}
         	productAvailability[productid] = inventory;
         } 
	}
	if (productLimitExceedQtys.length > 0) {
		basketResponse.addFlash({
				"type":"ProductItemLimitExceeded",
				"message":"Total Quantity of the product item cannot be more than " +limit + " to the product " +productLimitExceedQtys,
				"path": "$.basket",
				"details": new Number(limit)
			});
	}
	var warrantyProductIDs : String = dw.system.Site.getCurrent().getCustomPreferenceValue('warrantyProductIDs');
	basketResponse.c_warrantyProducts = warrantyProductIDs.split(',');
	basketResponse.c_productAvailability = productAvailability;
	basketResponse.c_productImages = productImages;
	basketResponse.c_productAvailability = productAvailability;
	var totalProductLimit = dw.system.Site.getCurrent().getCustomPreferenceValue('totalProductLimit');
	if (basketQty > totalProductLimit) {
		basketLimitExceed = true;
		basketResponse.addFlash({
			"type":"BasketLimitExceeded",
			"message":"Total Quantity in the cart cannot be more than "+totalProductLimit,
			"path": "$.basket",
			"details": new Number(totalProductLimit)
		});
	}
	basketResponse.c_basketLimitExceed = basketLimitExceed;
};