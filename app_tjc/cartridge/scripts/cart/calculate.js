/**
 * @module calculate.js
 *
 * This javascript file implements methods (via Common.js exports) that are needed by
 * the new (smaller) CalculateCart.ds script file.  This allows OCAPI calls to reference
 * these tools via the OCAPI 'hook' mechanism
 *
 */
var HashMap = require("dw/util").HashMap,
	PromotionMgr = require("dw/campaign").PromotionMgr,
	ShippingMgr = require("dw/order").ShippingMgr,
	ShippingLocation = require("dw/order").ShippingLocation,
	TaxMgr = require("dw/order").TaxMgr,
	Logger = require("dw/system").Logger,
	Site = require("dw/system").Site,
	ArrayList = require("dw/util").ArrayList,
    Calendar = require("dw/util").Calendar,
    Decimal = require("dw/util").Decimal,
	Money = require("dw/value").Money;

/**
 * @function calculate
 *
 * calculate is the arching logic for computing the value of a basket.  It makes
 * calls into cart/calculate.js and enables both SG and OCAPI applications to share
 * the same cart calculation logic.
 *
 * @param {object} basket The basket to be calculated
 */
exports.calculate = function(basket)
{
    // ===================================================
	// =====   CALCULATE PRODUCT LINE ITEM PRICES    =====
    // ===================================================
	calculateProductPrices(basket);

    // ===================================================
	// =====    CALCULATE GIFT CERTIFICATE PRICES    =====
    // ===================================================

	calculateGiftCertificatePrices(basket);

    // ===================================================
	// =====   Note: Promotions must be applied      =====
	// =====   after the tax calculation for         =====
	// =====   storefronts based on GROSS prices     =====
    // ===================================================

    // ===================================================
	// =====   APPLY PROMOTION DISCOUNTS			 =====
	// =====   Apply product and order promotions.   =====
	// =====   Must be done before shipping 		 =====
	// =====   calculation. 					     =====
    // ===================================================

	PromotionMgr.applyDiscounts(basket);

    // ===================================================
	// =====        CALCULATE SHIPPING COSTS         =====
    // ===================================================

	// apply product specific shipping costs
	// and calculate total shipping costs
	ShippingMgr.applyShippingCost(basket);

	// Apply the custom shipping logic for TJC
	applyCustomShippingCostLogic(basket);

    // ===================================================
	// =====   APPLY PROMOTION DISCOUNTS			 =====
	// =====   Apply product and order and 			 =====
	// =====   shipping promotions.                  =====
    // ===================================================

	PromotionMgr.applyDiscounts(basket);

	// since we might have bonus product line items, we need to
	// reset product prices
	calculateProductPrices(basket);

    // adjust the prices VAT specific for certain countries -> reduce
    // the gross prices again
    calculateVATSpecificPrices(basket);

    // ===================================================
	// =====         CALCULATE TAX                   =====
    // ===================================================
    
    var associatedWarrantyProduct : String;
	var plis = basket.getAllProductLineItems();
	for each(var pli in plis){
		if(!empty(pli.custom.warrantyUnitPrice) && !empty(pli.custom.warrantyQty)){
			associatedWarrantyProduct = pli.custom.linkedWarrantyProduct;
			var plis = basket.getProductLineItems(associatedWarrantyProduct);
			if(!empty(plis)){
				var wpli : ProductLineItem = plis[0];
				wpli.setPriceValue(pli.custom.warrantyUnitPrice);
				wpli.updateTax(0);
			}
		}
	}
	PromotionMgr.applyDiscounts(basket); 
	checkAndApplyBuyAllDiscount(basket); 
	calculateTax(basket);

    // ===================================================
	// =====         CALCULATE BASKET TOTALS         =====
    // ===================================================

	basket.updateTotals();
	
    // ===================================================
	// =====            DONE                         =====
    // ===================================================

	return new dw.system.Status(dw.system.Status.OK);
};

/**
 * @function calculateProductPrices
 *
 * Calculates product prices based on line item quantities. Set calculates prices
 * on the product line items.  This updates the basket and returns nothing
 *
 * @param {object} basket The basket containing the elements to be computed
 */
function calculateProductPrices (basket)
{
	// get total quantities for all products contained in the basket
	var productQuantities = basket.getProductQuantities();

	// get product prices for the accumulated product quantities
	var productPrices = new HashMap();

	for each(var product in productQuantities.keySet())
	{
		var quantity = productQuantities.get(product);
		productPrices.put(product, product.priceModel.getPrice(quantity));
	}

	// iterate all product line items of the basket and set prices
	var productLineItems = basket.getAllProductLineItems().iterator();
	while(productLineItems.hasNext())
	{
		var productLineItem = productLineItems.next();

		// handle non-catalog products
		if(!productLineItem.catalogProduct)
		{
			productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
			continue;
		}

		var product = productLineItem.product;

		//handle auction products
	    if (!empty(productLineItem.custom.auctionCode))
	    {
	        if (productLineItem.custom.itemSource == 'RISING') {
	            productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
	            continue;
	        }
	    }
		// handle option line items
		else if(productLineItem.optionProductLineItem)
		{
			// for bonus option line items, we do not update the price
			// the price is set to 0.0 by the promotion engine
			if(!productLineItem.bonusProductLineItem)
			{
				productLineItem.updateOptionPrice();
			}
		}
		// handle bundle line items, but only if they're not a bonus
		else if(productLineItem.bundledProductLineItem)
		{
			// no price is set for bundled product line items
		}
		// handle bonus line items
		// the promotion engine set the price of a bonus product to 0.0
		// we update this price here to the actual product price just to
		// provide the total customer savings in the storefront
		// we have to update the product price as well as the bonus adjustment
		else if(productLineItem.bonusProductLineItem && product != null)
		{
			var price = product.priceModel.price;
			productLineItem.setPriceValue(price.valueOrNull);
			// get the product quantity
			var quantity = productLineItem.quantity;
			// we assume that a bonus line item has only one price adjustment
			var adjustments = productLineItem.priceAdjustments;
			if(!adjustments.isEmpty())
			{
				var adjustment = adjustments.iterator().next();
				var adjustmentPrice = price.multiply(quantity.value).multiply(-1.0);
				adjustment.setPriceValue(adjustmentPrice.valueOrNull);
			}
		}

		// set the product price. Updates the 'basePrice' of the product line item,
		// and either the 'netPrice' or the 'grossPrice' based on the current taxation
		// policy

		// handle product line items unrelated to product
		else if(product == null)
		{
			productLineItem.setPriceValue(null);
		}
		// handle normal product line items
		else
		{
			productLineItem.setPriceValue(productPrices.get(product).valueOrNull);
		}
    }
}

/**
 * @function calculateVATSpecificPrices
 *
 * The function calculates VAT-specific prices and adjusts all prices again
 * when the basic calculation is done.
 *
 * @param {object} basket The basket containing the elements to be computed
 */
function calculateVATSpecificPrices(basket) {
    //  get total quantities for all products contained in the basket
    var productQuantities = basket.getProductQuantities();

    // get product prices for the accumulated product quantities
    var products = productQuantities.keySet().iterator();
    var productPrices = new HashMap();

    var shipment = basket.defaultShipment;

    while (products.hasNext())
    {
        var product = products.next();
        var quantity = productQuantities.get(product);
        productPrices.put(product, product.priceModel.getPrice(quantity));
    }

    // iterate all product line items of the basket and set prices
    var productLineItems = basket.getAllProductLineItems().iterator();
    while (productLineItems.hasNext())
    {
        var productLineItem = productLineItems.next();
        var product = productLineItem.product;

        // handle non-catalog products
        if (productLineItem.catalogProduct && product != null && !productLineItem.optionProductLineItem && !productLineItem.bonusProductLineItem)
        {
            var price = productPrices.get(product);
            if (!empty(productLineItem.custom.auctionCode))
            {
                if (productLineItem.custom.itemSource == 'RISING' || productLineItem.product.ID.indexOf("V") != -1) {
                    if (empty(productLineItem.custom.basePrice)) {
                    	productLineItem.custom.basePrice = productLineItem.basePrice.value;
                    }
                    price = productLineItem.basePrice.newMoney(Decimal(productLineItem.custom.basePrice));
                }
                if(productLineItem.custom.itemSource == 'TV' || productLineItem.custom.itemSource == 'TGTV'){
                	if (!empty(productLineItem.custom.basePrice)){
                		price = productLineItem.basePrice.newMoney(Decimal(productLineItem.custom.basePrice));
                	}
                	else {
                        //keep the current base price if the price from the UpdateLiveTVPrice isn't set
                        price = productLineItem.basePrice;
                	}
                }
            }

            setAdjustedVATPrice(shipment, productLineItem, price);
        }
    }
}

/**
 * @function setAdjustedVATPrice
 *
 * This function calculates the adjusted VAT product price based on a given reference price.
 *
 * @param {object} shipment The shipment to get the country from
 * @param {object} productLineItem The line item the price should be changed for
 * @param {object} price The original price of the line item
 */
function setAdjustedVATPrice(shipment, productLineItem, price) {

    // preference to check the VAT against and adjust the prices
    var adjustTaxCountries = Site.getCurrent().getCustomPreferenceValue('adjustVATCountries'),
        adjustTaxCountriesList = new ArrayList(adjustTaxCountries);

    // Adjust the gross price of the product for configured countries and subtract the
    // tax value gap. This is especially necessary for zero percent tax regions, such as
    // Jersey, and Guernsey
    if (shipment != null && shipment.shippingAddress != null && !empty(shipment.shippingAddress.countryCode)) {
        var countryCode = shipment.shippingAddress.countryCode.value;

        if (adjustTaxCountriesList.contains(countryCode)) {

            var taxJurisdictionID,
                taxClassID = productLineItem.taxClassID;

            // adjust the price only for the default tax class (Standard)
            if (taxClassID == TaxMgr.defaultTaxClassID) {

                var location = new ShippingLocation(shipment.shippingAddress);
                taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location);

                var defaultTaxRate = TaxMgr.getTaxRate(TaxMgr.defaultTaxClassID, TaxMgr.defaultTaxJurisdictionID);
                var taxRate = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID);

                // adjust the price only if the taxRate is lower (so only reduce the price)
                if (taxRate < defaultTaxRate) {

                    // calculate the net price of the gross price
                    var netPrice = price.multiply(100).divide(100 + (defaultTaxRate * 100));

                    // calculate the new gross price using the country-specific tax rate
                    var taxAdjustedPrice = netPrice.addRate(taxRate);

                    // set this new gross price value for the line item
                    productLineItem.setPriceValue(taxAdjustedPrice.valueOrNull);

                    // if the price is adjusted compared to the original price value
                    // the price adjustment also needs to be adjusted
                    var adjustments = productLineItem.priceAdjustments.iterator(),
                        adjustment;
                    if (adjustments.hasNext()) {

                        // iterate through all adjustments
                        while (adjustments.hasNext()) {
                            adjustment = adjustments.next();

                            // this discount is based on the original line item tax rate // calculation must be based on qty 1
                            var discountPrice = adjustment.getPrice().multiply(-1.0).divide(adjustment.getQuantity());

                            // calculate the discount net price for this adjustment
                            var netDiscountPrice = discountPrice.multiply(100).divide(100 + (defaultTaxRate * 100));

                            // calculate the discount price with the correct tax rate (same tax rate as for the product)
                            var realDiscountPrice = netDiscountPrice.addRate(taxRate).multiply(adjustment.getQuantity());

                            // set this as the price of the price adjustment
                            adjustment.setPriceValue(realDiscountPrice.multiply(-1.0).valueOrNull);
                        }

                        // there can be a rounding issue for 1 cent/penny for free gifts - so we need
                        // to check for negative adjusted prices and set them to zero. The last adjustment price
                        // will be recalculated in that case
                        if (productLineItem.getAdjustedPrice().value < 0) {
                            // adjust the last price adjustment again
                            adjustment.setPriceValue(adjustment.getPrice().subtract(productLineItem.getAdjustedPrice()).valueOrNull);
                        }

                    }
                }
            }
        }
        else {
        	productLineItem.setPriceValue(price.valueOrNull);
        }
    }
    else {
        productLineItem.setPriceValue(price.valueOrNull);
    }
}

/**
 * @function calculateGiftCertificates
 *
 * Function sets either the net or gross price attribute of all gift certificate
 * line items of the basket by using the gift certificate base price. It updates the basket in place.
 *
 * @param {object} basket The basket containing the gift certificates
 */
function calculateGiftCertificatePrices (basket)
{
	var giftCertificates = basket.getGiftCertificateLineItems().iterator();
	while(giftCertificates.hasNext())
	{
		var giftCertificate = giftCertificates.next();
		giftCertificate.setPriceValue(giftCertificate.basePrice.valueOrNull);
	}
} 

function checkAndApplyBuyAllDiscount (basket) {
	 var productQTY = 0,
	 	productAuctionCodes,
	 	quantityAdjustmentRequired = false;
	for each(var pli : ProductLineItem in basket.getAllProductLineItems()) {
	    if( (pli.custom.isBuyAllApplicable || pli.custom.islivetvBuyAllApplicable) && !empty(pli.custom.buyAllApplicableProductCodes) ){
			quantityAdjustmentRequired = false;
		 	var sameProductAuctionCodes =  !empty(productAuctionCodes) && productAuctionCodes.equals(pli.custom.buyAllApplicableProductCodes) ? true : false;
		 	if (sameProductAuctionCodes) { continue; }
	 		productAuctionCodes = pli.custom.buyAllApplicableProductCodes;
	 		var productCodesApplicable : dw.util.ArrayList = productAuctionCodes.split(',');
	 		if (!sameProductAuctionCodes) {	
		 		for each( var productID in productCodesApplicable ){
		 			var plis = basket.getProductLineItems(productID);
		 			if ( empty(plis)  ) { break; }
			        if( (plis[0].custom.isBuyAllApplicable || plis[0].custom.islivetvBuyAllApplicable) && (plis[0].custom.buyAllApplicableProductCodes != null) ) {
			        	productQTY = productQTY == 0 ? plis[0].quantityValue : productQTY;
			        	if (productQTY > plis[0].quantityValue ) { 
			        		productQTY =plis[0].quantityValue;  
			        		quantityAdjustmentRequired = true;
			        	} else if (productQTY != plis[0].quantityValue) {
			        		quantityAdjustmentRequired = true;
			        	}
			        }
		 		}
	 		}
		 		if ( quantityAdjustmentRequired ) {
		 		for each( var productID in productCodesApplicable ) {
		 			var plis = basket.getProductLineItems(productID);
		 			if (empty(plis)) { continue; }
			        if( (plis[0].custom.isBuyAllApplicable || plis[0].custom.islivetvBuyAllApplicable) && (plis[0].custom.buyAllApplicableProductCodes != null) ) {
						if (plis[0].quantityValue > productQTY) {
							var priceAdj = plis[0].getPriceAdjustments();
							for each(var pc2 in priceAdj) {                       
								var id = pc2.promotionID;
								if (id.indexOf('Buyalldiscount')> -1){
									var priceAdjustment = plis[0].getPriceAdjustmentByPromotionID(id);
									if (!empty(priceAdjustment)) {
										var newPrice = ( priceAdjustment.getPriceValue() / (plis[0].quantityValue)) * (productQTY);
										priceAdjustment.setPriceValue(newPrice);
									}
								}
							}
						}
			        }
				}
			}
		}
	}	
     
    for each(var pli : ProductLineItem in basket.getAllProductLineItems()) {
        if(pli.custom.isBuyAllApplicable || pli.custom.islivetvBuyAllApplicable){
            var productCodes = pli.custom.buyAllApplicableProductCodes;
            if( 'buyAllApplicableProductCodes' in pli.custom && pli.custom.buyAllApplicableProductCodes != null){
            var productCodesApplicable : dw.util.ArrayList = productCodes.split(',');
                    for each(var productID in productCodesApplicable) {
                        if ( empty(basket.getProductLineItems(productID)) ) {
                            for each( var productID in productCodesApplicable ){
                                  var productLineItem = basket.getProductLineItems(productID);
                                  if (productLineItem.length > 0 ) {
                                        var priceAdj = productLineItem[0].getPriceAdjustments();
                                        for each(var pc2 in priceAdj){                       
                                            var id = pc2.promotionID;
                                            if (id.indexOf('Buyalldiscount')> -1){
                                            	var priceAdjustment = productLineItem[0].getPriceAdjustmentByPromotionID(id);
                                            	productLineItem[0].removePriceAdjustment(priceAdjustment);                           
                                            }
                                        }
                                 }
                             }
                         break;                        
                        }
                    }
        		}
            else if(!('buyAllApplicableProductCodes' in pli.custom) || pli.custom.buyAllApplicableProductCodes == null){
                var promoid = pli.getPriceAdjustments();
                for each(var pc2 in promoid){                       
                    var id = pc2.promotionID;
                    if (id.indexOf('Buyalldiscount')> -1){
                        var priceAdjustment = pli.getPriceAdjustmentByPromotionID(id);
                        pli.removePriceAdjustment(priceAdjustment);                           
                    }
                }       
        }
    }
    else{
    	 var promoid = pli.getPriceAdjustments();
                for each(var pc2 in promoid){                       
                    var id = pc2.promotionID;
                    if (id.indexOf('Buyalldiscount')> -1){
                        var priceAdjustment = pli.getPriceAdjustmentByPromotionID(id);
                        pli.removePriceAdjustment(priceAdjustment);                           
                    }
                } 
    }
}
}
/**
 * @function calculateTax <p>
 *
 * Determines tax rates for all line items of the basket. Uses the shipping addresses
 * associated with the basket shipments to determine the appropriate tax jurisdiction.
 * Uses the tax class assigned to products and shipping methods to lookup tax rates. <p>
 *
 * Sets the tax-related fields of the line items. <p>
 *
 * Handles gift certificates, which aren't taxable. <p>
 *
 * Note that the function implements a fallback to the default tax jurisdiction
 * if no other jurisdiction matches the specified shipping location/shipping address.<p>
 *
 * Note that the function implements a fallback to the default tax class if a
 * product or a shipping method does explicitly define a tax class.
 *
 * @param {object} basket The basket containing the elements for which taxes need to be calculated
 */
function calculateTax (basket)
{
	var shipments = basket.getShipments().iterator();
	while(shipments.hasNext())
	{
		var shipment = shipments.next();

		// first we reset all tax fields of all the line items
		// of the shipment
		var shipmentLineItems = shipment.getAllLineItems().iterator();
		while(shipmentLineItems.hasNext())
		{
			var lineItem = shipmentLineItems.next();
			// do not touch tax rate for fix rate items
			if(lineItem.taxClassID == TaxMgr.customRateTaxClassID)
			{
				lineItem.updateTax(lineItem.taxRate);
			}
			else
			{
				lineItem.updateTax(null);
			}
		}

		// identify the appropriate tax jurisdiction
		var taxJurisdictionID = null;

		// if we have a shipping address, we can determine a tax jurisdiction for it
		if(shipment.shippingAddress != null)
		{
			var location = new ShippingLocation(shipment.shippingAddress);
			taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location);
		}

		if(taxJurisdictionID == null)
		{
			taxJurisdictionID = TaxMgr.defaultTaxJurisdictionID;
		}

		// if we have no tax jurisdiction, we cannot calculate tax
		if(taxJurisdictionID == null)
		{
			continue;
		}

		// shipping address and tax juridisction are available
		var shipmentLineItems = shipment.getAllLineItems().iterator();
		while(shipmentLineItems.hasNext())
		{
			var lineItem = shipmentLineItems.next();
			var taxClassID = lineItem.taxClassID;

			dw.system.Logger.debug("1. Line Item {0} with Tax Class {1} and Tax Rate {2}", lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);

			// do not touch line items with fix tax rate
			if(taxClassID == TaxMgr.customRateTaxClassID)
			{
				continue;
			}

			// line item does not define a valid tax class; let's fall back to default tax class
			if(taxClassID == null)
			{
				taxClassID = TaxMgr.defaultTaxClassID;
			}

			// if we have no tax class, we cannot calculate tax
			if(taxClassID == null)
			{
				dw.system.Logger.debug("Line Item {0} has invalid Tax Class {1}", lineItem.lineItemText, lineItem.taxClassID);
				continue;
			}

			// get the tax rate
			var taxRate = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID);
			// w/o a valid tax rate, we cannot calculate tax for the line item
			if(taxRate == null)
			{
				continue;
			}

			// calculate the tax of the line item
	    lineItem.updateTax(taxRate);
			dw.system.Logger.debug("2. Line Item {0} with Tax Class {1} and Tax Rate {2}", lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);
	}
	}

	// besides shipment line items, we need to calculate tax for possible order-level price adjustments
    // this includes order-level shipping price adjustments
    if(!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty)
    {
	// calculate a mix tax rate from
	var basketPriceAdjustmentsTaxRate = (basket.getMerchandizeTotalGrossPrice().value / basket.getMerchandizeTotalNetPrice().value) - 1;

	    var basketPriceAdjustments = basket.getPriceAdjustments().iterator();
	    while(basketPriceAdjustments.hasNext())
	    {
			var basketPriceAdjustment = basketPriceAdjustments.next();
			basketPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
	    }

	    var basketShippingPriceAdjustments = basket.getShippingPriceAdjustments().iterator();
	    while(basketShippingPriceAdjustments.hasNext())
	    {
			var basketShippingPriceAdjustment = basketShippingPriceAdjustments.next();
			basketShippingPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
	    }
	}
}

function applyCustomShippingCostLogic(basket : Basket)
{
    var shipment = basket.defaultShipment;
    var shippingMethod = shipment.shippingMethod;
    var shippingLineItem = shipment.getStandardShippingLineItem();
    var customerNo = basket.customer.profile && basket.customer.profile.customerNo;
    var productLineItems = basket.getProductLineItems().iterator();
    var newPnpLogic = Site.getCurrent().getCustomPreferenceValue('newPnpLogicEnabled');

    shippingLineItem.custom.reachedPnpMax = false;

    //nothing to do, if no shippingMethod is set
    if (empty(shippingMethod)) {
        return;
    }
    /*if (shippingMethod.ID == 'europe_default') {
        return;
    }*/

    //TJC will create shipping methods starting with "custom" so we don't need to apply custom logic to these.
    var shippingIdSplit = shippingMethod.ID.split('_');

    //Commenting out this line of code as its not required as of now
    /*if (shippingIdSplit[0] == 'custom') {
        return;
    }*/

    //get the preferences values for the current method
    var pnPChargingMaximum, pnPQuantity;
    var acumulatedPnp = 0;

    if (newPnpLogic) {
        while (productLineItems.hasNext()) {
            var productLineItem : ProductLineItem = productLineItems.next();
            acumulatedPnp += productLineItem.custom.pnp;
        }
    }

    if (shippingMethod.ID == 'gb_standard') {  
        pnPChargingMaximum = Site.getCurrent().getCustomPreferenceValue('standardPnPChargingMaximum');
        pnPQuantity = Site.getCurrent().getCustomPreferenceValue('standardPnPQuantity');
    } else {
        pnPChargingMaximum = Site.getCurrent().getCustomPreferenceValue('nextDayPnPChargingMaximum');
        pnPQuantity = Site.getCurrent().getCustomPreferenceValue('nextDayPnPQuantity');
    }

    //guest checkout has no previous orders and should always pay normal price
    if (!empty(customerNo) ) {
        //get previous order of the customer to check if he has purchased something today 
        var tempCalendar = Site.current.calendar;
        tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
        tempCalendar.set(Calendar.MINUTE, 0);
        tempCalendar.set(Calendar.SECOND, 0);
        tempCalendar.set(Calendar.MILLISECOND, 0);
        var todayBegin : Date = tempCalendar.time;
        tempCalendar.add(Calendar.DAY_OF_MONTH, 1);
        var todayEnd : Date = tempCalendar.time;
        var previousOrders : Iterator = dw.order.OrderMgr.searchOrders('customerNo = {0} AND creationDate > {1} AND creationDate < {2}', 'orderNo asc', customerNo, todayBegin, todayEnd);

        //calculate total count of purchased items
        var totalProductsPurchased = 0;
        var totalDailyShippingCosts = new Money(0,'GBP');
        while (previousOrders.hasNext()) {
            var order = previousOrders.next();
            var status = order.status.value;
            if (status != dw.order.Order.ORDER_STATUS_CANCELLED && status != dw.order.Order.ORDER_STATUS_CREATED
            		&& status != dw.order.Order.ORDER_STATUS_FAILED && status != dw.order.Order.ORDER_STATUS_REPLACED) {
            	if (order.defaultShipment.shippingMethodID == basket.defaultShipment.shippingMethodID) {
            		totalDailyShippingCosts = totalDailyShippingCosts.add(order.adjustedShippingTotalGrossPrice);
            	}
        	}
        }

    	//customer has already reached the maximum shipping costs in earlier orders today
    	if (totalDailyShippingCosts.value >= pnPChargingMaximum) {
    		shippingLineItem.setPriceValue(0);
    		shippingLineItem.custom.reachedPnpMax = true;
    		return;
    	}

        //customer has already purchased something today, but didn't payed maximum shipping costs yet
        if (totalDailyShippingCosts.value > 0 && totalDailyShippingCosts.value < pnPChargingMaximum) {
            if (acumulatedPnp > 0 && (totalDailyShippingCosts.value + acumulatedPnp) < pnPChargingMaximum) {
                shippingLineItem.setPriceValue(acumulatedPnp);
            } else {
            	var shippingDifferenceToCharge = pnPChargingMaximum - totalDailyShippingCosts.value;
            	shippingLineItem.custom.reachedPnpMax = true;
                shippingLineItem.setPriceValue(shippingDifferenceToCharge); 
            }
            return;
        }
        //I think I need to change this logic. Checking what the totalDailyShippingCosts are and then summing the pnp from each line item
        //and for each line item, checking whether the pnPChargingMaximum was reached. Then, set the "reachedPnpMax" to true to display the message in the cart.
    }

    if (!newPnpLogic) {
    	 //customer has nothing purchased today, but has enough items to be charged for maximum shipping costs
        if (basket.productQuantityTotal >= pnPQuantity) {
            shippingLineItem.setPriceValue(pnPChargingMaximum);
        }
    } else {
    	if (acumulatedPnp > pnPChargingMaximum) {
    		shippingLineItem.setPriceValue(pnPChargingMaximum);
    		shippingLineItem.custom.reachedPnpMax = true;
    	} else {
    		if (shippingMethod.ID == 'gb_standard') {     			
    			shippingLineItem.setPriceValue(acumulatedPnp);
    		}
    		else {
    			shippingLineItem.setPriceValue(pnPChargingMaximum);
    		}
    	}
    }

    // Additionally, apply the net price for channel islands delivery

    // preference to check the VAT against and adjust the prices
    var adjustTaxCountries = Site.getCurrent().getCustomPreferenceValue('adjustVATCountries'),
        adjustTaxCountriesList = new ArrayList(adjustTaxCountries);

    // Adjust the gross price of the product for configured countries and subtract the
    // tax value gap. This is especially necessary for zero percent tax regions, such as
    // Jersey, and Guernsey
    if (shipment != null && shipment.shippingAddress != null && !empty(shipment.shippingAddress.countryCode)) {
        var countryCode = shipment.shippingAddress.countryCode.value;

        if (adjustTaxCountriesList.contains(countryCode)) {

            var location = new ShippingLocation(shipment.shippingAddress),
            	taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location),
                taxClassID = shippingMethod.taxClassID,
            	location = new ShippingLocation(shipment.shippingAddress),
            	defaultTaxRate = TaxMgr.getTaxRate(TaxMgr.defaultTaxClassID, TaxMgr.defaultTaxJurisdictionID),
            	taxRate = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID),
            	price = shippingLineItem.getPrice();

            // adjust the price only if the taxRate is lower (so only reduce the price)
            if (taxRate < defaultTaxRate) {

                // calculate the net price of the gross price
                var netPrice = price.multiply(100).divide(100 + (defaultTaxRate * 100));

                // calculate the new gross price using the country-specific tax rate
                var taxAdjustedPrice = netPrice.addRate(taxRate);

                // set this new gross price value for the line item
                shippingLineItem.setPriceValue(taxAdjustedPrice.valueOrNull);

                // if the price is adjusted compared to the original price value
                // the price adjustment also needs to be adjusted
                var adjustments = shippingLineItem.shippingPriceAdjustments.iterator(),
                    adjustment;
                if (adjustments.hasNext()) {

                    // iterate through all adjustments
                    while (adjustments.hasNext()) {
                        adjustment = adjustments.next();

                        // this discount is based on the original line item tax rate // calculation must be based on qty 1
                        var discountPrice = adjustment.getPrice().multiply(-1.0).divide(adjustment.getQuantity());

                        // calculate the discount net price for this adjustment
                        var netDiscountPrice = discountPrice.multiply(100).divide(100 + (defaultTaxRate * 100));

                        // calculate the discount price with the correct tax rate (same tax rate as for the product)
                        var realDiscountPrice = netDiscountPrice.addRate(taxRate).multiply(adjustment.getQuantity());

                        // set this as the price of the price adjustment
                        adjustment.setPriceValue(realDiscountPrice.multiply(-1.0).valueOrNull);
                    }

                    // there can be a rounding issue for 1 cent/penny for free gifts - so we need
                    // to check for negative adjusted prices and set them to zero. The last adjustment price
                    // will be recalculated in that case
                    if (shippingLineItem.getAdjustedPrice().value < 0) {
                        // adjust the last price adjustment again
                        adjustment.setPriceValue(adjustment.getPrice().subtract(shippingLineItem.getAdjustedPrice()).valueOrNull);
                    }
                }
            }
        }
    }
}
