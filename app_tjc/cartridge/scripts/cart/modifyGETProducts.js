 /**
 *
 * Add product promotions, badges and installments
 *
 */

var Logger = require("dw/system").Logger;
var PromotionMgr = require("dw/campaign").PromotionMgr;
var HashMap = require("dw/util").HashMap;
importScript("product/ProductUtils.ds");
importScript("product/libProduct.ds");

exports.modifyGETResponse = function( product, productDoc ) {
	// temporal image fix for ecdn
	var prod = productDoc;
	var imgGroups = productDoc.image_groups;
	var dynamicConfigFile = dw.system.Site.current.getCustomPreferenceValue('productImageConfig');
    var productImageConfig = new Object();
    if (!empty(imgGroups)) {
        var currentImgGroup = imgGroups[0];
        var images = currentImgGroup.images;

        if (!empty(images) && images.length > 0) {
            for (var i=0; i < images.length; i++ ) {
                var currentImg = images[i];
                if (dynamicConfigFile) {
                	productImageConfig = JSON.parse(dynamicConfigFile);
                } else {
                	productImageConfig = {
                            producttile: {
                                width: 310,
                                height: 310,
                                scaleMode: 'fit'
                            }
                        };
                }
                 var imageData = ProductSO(product).getProductImage(productImageConfig, 'pdp', i, '', null, null, null);
                currentImg.link = imageData.url.toString()
                currentImg.dis_base_link = currentImg.link;
            }
        }
    }
    // promotions and prices
    var productPromotions = PromotionMgr.getActiveCustomerPromotions().getProductPromotions(product);
    var promotionMessages = new Array();
    var promotionalPrice;
    var promotionalPriceHashMap = new HashMap();
    for (var j=0; j < productPromotions.length; j++ ) {
        var promotion = productPromotions[j];
        if (!empty(promotion.calloutMsg)) {
            promotionMessages.push(promotion.calloutMsg.source);
        }
        if(promotion.getPromotionalPrice(product).valueOrNull != null){
            promotionalPrice = promotion.getPromotionalPrice(product);
        }
    }
    if (promotionMessages.length > 0) {
        productDoc.c_promotions = promotionMessages;
    }
    if (!empty(promotionalPrice) && !empty(productDoc.price) && promotionalPrice.value < productDoc.price) {
        promotionalPriceHashMap.put('gb-gbp-list-prices', productDoc.price);
        promotionalPriceHashMap.put('gb-gbp-sale-prices', promotionalPrice.value);
        productDoc.prices = promotionalPriceHashMap;
    }
    
 // product thumbnail (workaround for missing scaling)
    
    if (dynamicConfigFile) {
    	productImageConfig = JSON.parse(dynamicConfigFile);
    } else {
    	productImageConfig = {
                producttile: {
                    width: 310,
                    height: 310,
                    scaleMode: 'fit'
                }
            };
    }
    var imageData = ProductSO(product).getProductImage(productImageConfig, 'pdp', 0, '', null, null, null);
    if (!empty(imageData) && !empty(imageData.url)) {
    	productDoc.c_thumbnail = imageData.url.toString();
    }
    if (product.custom.raMainProductCode) {
    	var productReevooRating = ProductUtils.GetReevooProductRating(product);
    	productDoc.c_avgRating = productReevooRating.ratingToDisplay;
    } else if (product.isVariant()) {
    	var masterProduct = product.getMasterProduct();
    	var rating = masterProduct.custom.avgRating;
    	if (!rating || rating == 0) {
    		if (masterProduct.variationModel && masterProduct.variationModel.defaultVariant) {
    			var defaultVariant = masterProduct.variationModel.defaultVariant;
    			rating = defaultVariant.custom.avgRating;
    		} else {
    			var defaultVariant = masterProduct.variants[0];
    			rating = defaultVariant.custom.avgRating;
    		}
    		
    	} 
    	productDoc.c_avgRating = rating
    }
    var productRating = ProductUtils.GetProductRating(product);
    productDoc.c_turntoAverageRating = productRating.ratingToDisplay;
    productDoc.c_turntoReviewCount = productRating.reviewCount;
	return new dw.system.Status(dw.system.Status.OK);
};
