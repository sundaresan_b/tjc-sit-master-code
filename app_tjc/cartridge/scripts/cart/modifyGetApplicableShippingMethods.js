 /**
 *
 * Here we adjust the OCAPI response for getApplicableShippingMethods 
 * running the custom logic for
 *
 */

var Logger = require("dw/system").Logger;
var BasketMgr = require("dw/order").BasketMgr; 
var Pipeline = require("dw/system").Pipeline; 


exports.modifyGETResponse_v2 = function(shipment,shippingMethodResult ) {
	var currentBasket, 
		applicableShippingMethods,
		pdict,
		shippingCosts; 
	 
	currentBasket = BasketMgr.getCurrentBasket();
	    
    pdict = Pipeline.execute('COShipping-UpdateShippingMethodList', {
    	Basket : currentBasket
    });
	
    shippingCosts = pdict.ShippingCosts;
    
    applicableShippingMethods = shippingMethodResult.applicableShippingMethods;  
    for(var i=0; i < applicableShippingMethods.length; i++){
    	var shippingMethod = applicableShippingMethods[i];
    	 
    	var shippingCost = shippingCosts.get(shippingMethod.id);
    	if(shippingCost != null){
    		shippingMethod.price = shippingCost.shippingInclDiscounts.getValue();
    		shippingMethod.c_available = 'true';
    	}
    	else
    	{
    		shippingMethod.c_available = 'false';
    	}
    }

	return new dw.system.Status(dw.system.Status.OK);  
}; 
