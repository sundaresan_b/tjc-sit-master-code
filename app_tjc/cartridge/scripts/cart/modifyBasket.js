/**
 *
 * This script adds the BudgetPay attributes to the Lineitems OCAPI response
 *
 */

var Logger = require("dw/system").Logger;
var Transaction = require("dw/system/Transaction");

exports.calculate = function( basket ) {
	
	if(!empty(basket)){
		basket.custom.budgetpayAvailable = false;
		basket.custom.paypalCreditThreshold = dw.system.Site.current.preferences.custom.PaypalCreditThreshold;
		
		var plis = basket.getProductLineItems();
		for each(var pli in plis){
			if (!empty(pli.product.custom.installments) && pli.product.custom.installments > 0) {
				pli.custom.installments = pli.product.custom.installments;
				basket.custom.budgetpayAvailable = true;
			}
		}
	}
	
	
	if(!empty(basket) && !empty(basket.customer) && basket.customer.authenticated && !empty(basket.customer.profile)){
		basket.custom.budgetPayCreditAvailable = !empty(basket.customer.profile.custom.budgetPayCreditAmount) ? basket.customer.profile.custom.budgetPayCreditAmount : 0 ;
		basket.custom.storeCreditAvailable = !empty(basket.customer.profile.custom.storeCredit) ? basket.customer.profile.custom.storeCredit : 0 ;
		basket.custom.isTJCPlusCustomer = !empty(basket.customer.profile.custom.isTJCPlusMember) ? basket.customer.profile.custom.isTJCPlusMember : false ;
		basket.custom.firstName = basket.customer.profile.firstName;
		basket.custom.lastName = basket.customer.profile.lastName;
	}
}