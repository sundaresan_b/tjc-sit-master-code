	/**
 *
 * This script adds the Certificate link attribute to the OCAPI response
 *
 */

var Logger = require("dw/system").Logger;
var ProductMgr = require("dw/catalog").ProductMgr;
var turnToLogger = dw.system.Logger.getLogger("TurnTo");
var HTTPClient= require('dw/net/HTTPClient');

exports.modifyGETResponse = function( order, orderResponse ) {
	if (!empty(order) && !empty(orderResponse)) {
		var orderMail = order.customer.profile ? order.customer.profile.email : '';
		var ratingProducts =  turnTORatings(orderMail);
		var shipment  = order.defaultShipment;
		if( shipment.productLineItems.size() > 0 ) {
			var productLineItems = shipment.productLineItems;
			for (var i = 0; i < productLineItems.length; i++) {
				var isProductCertificate = false;
				var pLineItem = productLineItems[i];
				var productObj = ProductMgr.getProduct(pLineItem.productID);
				var categoryAllAssignments = productObj.getCategoryAssignments();
				var isEngraving = productObj.custom.isEngravingItem;
				var categoryValue;
				var categoryAllId;
				var isCertificate;
				for (var j = 0; j < categoryAllAssignments.length; j++) {
					categoryValue = categoryAllAssignments[j].getCategory();
					categoryAllId = categoryValue.getID();
					isCertificate = categoryAllId.search("jewellery");
					if (isCertificate >= 0)
					{
						break;
					}
				}
				if ((productObj != null || !empty(productLineItem.custom.raMainProductCode) )) {
					if (isCertificate >= 0 || isCertificate == null) {
						if (!isEngraving) {
							isProductCertificate = true;
						}
					}
				}
				orderResponse.product_items[i].c_isProductCertificate = isProductCertificate;
				if (ratingProducts[pLineItem.productID]) {
					orderResponse.product_items[i].c_turntoAverageRating = ratingProducts[pLineItem.productID];
				}
			}
		}
	} else {
		return dw.system.Status(dw.system.Status.ERROR);  
	}
};

exports.modifyPOSTResponse = function(orderSearchResultResponse) {
	var orderHits = orderSearchResultResponse.hits;
	var orderIterator = orderHits.iterator();
	while (orderIterator.hasNext()) {
		var order = orderIterator.next();
		order = order.data;
		var customerInfo = order.customer_info;
		var orderMail = customerInfo.email;
		var ratingProducts =  turnTORatings(orderMail);
		var productItems = order.product_items;
		if (productItems) {
			var productLineItemIterator = productItems.iterator();
			while (productLineItemIterator.hasNext()) {
				var prodLineItem = productLineItemIterator.next();
				if (ratingProducts[prodLineItem.product_id]) {
					prodLineItem.c_turntoAverageRating = ratingProducts[prodLineItem.product_id];
				}
			}
		}
	}
	
}


function turnTORatings( emailID )
{	
	var reviewdProducts = new Object();

	var auth = dw.system.Site.getCurrent().getCustomPreferenceValue('turnTOBearerTkn'),
		bearerToken : String = 'Bearer '+ auth,
		url = dw.system.Site.getCurrent().getCustomPreferenceValue('turnTOFindReviewUrl') + '?'+'email='+emailID;

	var httpSvc : HTTPClient = new HTTPClient();
	httpSvc.setRequestHeader("Content-type", "application/json");
	httpSvc.setRequestHeader("Authorization", bearerToken);

	httpSvc.open("GET", url);
	httpSvc.setTimeout(10000);
	var result = httpSvc.send();

	if (!empty(httpSvc) && (!empty(httpSvc.errorBytes) || !empty(httpSvc.errorText) || httpSvc.statusCode != '200' )) {
			// Some Error has occured
			turnToLogger.error('EmailID:- '+emailID+'Error Occured in findReview response'+httpSvc.errorText+'CODE:- '+httpSvc.statusCode);
		} else if (!empty(httpSvc) && (httpSvc.statusMessage == 'Created' || httpSvc.statusCode == '200')) {
			// Successful created the review
			var response = JSON.parse(httpSvc.text),
				tempReviewArray = [];
			tempReviewArray = response.reviews;
			for (var i = 0; i < tempReviewArray.length; i++) {
				var tempCatlogArray = tempReviewArray[i].catalogItems;
				for (var j = 0; j < tempCatlogArray.length; j++) {
					reviewdProducts[tempCatlogArray[j]['sku']] =  tempReviewArray[i].rating;
				}
			}
		} else {
			// Something else
			turnToLogger.error('EmailID:- '+emailID+'Exception in findReview response'+httpSvc.errorText+'CODE:- '+httpSvc.statusCode);
		}

   return reviewdProducts;
}


