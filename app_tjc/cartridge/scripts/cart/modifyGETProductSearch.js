 /**
 *
 * Add product promotions, badges and installments
 *
 */

var Logger = require("dw/system").Logger;
var ProductMgr = require("dw/catalog").ProductMgr;
var PromotionMgr = require("dw/campaign").PromotionMgr;
var HashMap = require("dw/util").HashMap;
importScript("product/ProductUtils.ds");
importScript("product/libProduct.ds");

exports.modifyGETResponse = function( productSearchResult ) {

	for ( var x=0; x < productSearchResult.refinements.length; x++ ){
		var refinementItem = productSearchResult.refinements[x];
		var attributeType = refinementItem.attribute_id;
		if(attributeType == 'c_raEndTime' ||
			attributeType == 'c_raStartTime' ||
			attributeType == 'c_caExported' ||
			attributeType == 'c_caExportedFlag' ||
			attributeType == 'c_raMainProductCode' ||
			attributeType == 'c_raHighestBidPriceAuto' ||
			attributeType == 'c_raHighestBidPriceManual' ||
			attributeType == 'c_raStartPrice' ||
			attributeType == 'c_raAuctionCode' ||
			attributeType == 'c_raBidIncrement'
		){

			refinementItem.values = [];
		}
	}
	var productSearchHits = productSearchResult.hits;
	if (productSearchResult.count == 1) {
		if (productSearchHits[0].product_id == productSearchResult.query) {
			var product = ProductMgr.getProduct(productSearchHits[0].product_id);
			if (product.custom.isEngravingItem) {
				productSearchHits[0].c_engravingFeature = "Looks like you’re searching for an engraving product which our app currently doesn’t support, but it's a feature that's coming soon! Please visit <a> http://tjc.co.uk </a> to complete your purchase."
			}
		}
	}
	for (var i=0; i < productSearchHits.length; i++ ) {
		var productSearchHit = productSearchHits[i];
		var product = ProductMgr.getProduct(productSearchHit.product_id);
		
		if (product.custom.isEngravingItem) {
			productSearchHits.removeAt(i);
			i--;
			continue;
		}
		
		//sending Average revoo ratings
		productSearchHit.c_avgRating = product.custom.avgRating ? product.custom.avgRating : 0;
		var productRating = ProductUtils.GetProductRating(product);
    	 productSearchHit.c_turntoAverageRating = productRating.ratingToDisplay;
   		 productSearchHit.c_turntoReviewCount = productRating.reviewCount;
		
		if (product.master) {
		    for each(var variant in product.variants){
				if(variant.getAvailabilityModel().isOrderable() ){	
					product = variant;
					break;
				}
		    }
		}
		
		
		//check if warranty product or not
		productSearchHit.c_isWarrantyProduct = (!empty(product.custom.isWarrantyProduct) && product.custom.isWarrantyProduct) ? true : false;
		
		// promotions
		var productPromotions = PromotionMgr.getActiveCustomerPromotions().getProductPromotions(product);
		var promotionMessages = new Array();
		var promotionalPrice = null;
		var promotionalPriceHashMap = new HashMap();
		for (var j=0; j < productPromotions.length; j++ ) {
			var promotion = productPromotions[j];

			if (!empty(promotion.calloutMsg)) {
				promotionMessages.push(promotion.calloutMsg.source);
			}
			if(promotion.getPromotionalPrice(product).valueOrNull != null){
				promotionalPrice = promotion.getPromotionalPrice(product);
			}
		}

		if(('listPricebookId' in dw.system.Site.current.preferences.custom) && !empty(dw.system.Site.current.preferences.custom.listPricebookId))
			var StandardPrice = product.getPriceModel().getPriceBookPrice(dw.system.Site.current.preferences.custom.listPricebookId);
		else
			var StandardPrice = product.getPriceModel().getPriceBookPrice('list-prices');
		var SalesPrice = product.getPriceModel().getPrice();
		
		if (promotionMessages.length > 0) {
			productSearchHit.c_promotions = promotionMessages;
		}
		if (!empty(promotionalPrice) && !empty(productSearchHit.price) && promotionalPrice.value < productSearchHit.price) {
			promotionalPriceHashMap.put('gb-gbp-list-prices', productSearchHit.price);
			promotionalPriceHashMap.put('gb-gbp-sale-prices', promotionalPrice.value);
			productSearchHit.prices = promotionalPriceHashMap;
			productSearchHit.price = promotionalPrice.value;
		}
		else if(StandardPrice.available && SalesPrice.available && StandardPrice.getValue() > SalesPrice.getValue()){
			promotionalPriceHashMap.put('gb-gbp-list-prices', StandardPrice.getValue());
			promotionalPriceHashMap.put('gb-gbp-sale-prices', SalesPrice.getValue());
			productSearchHit.prices = promotionalPriceHashMap;
			productSearchHit.price = SalesPrice.getValue();
		}

		// badges
		var auctionSettings = ProductUtils.GetLiveAuctionSettings(product.custom.laStartTime)
		var badgeID;
        if (auctionSettings.isLiveAuctionProduct) {
            badgeID = 'live';
        } else if (auctionSettings.isMissedAuctionProduct) {
            badgeID = 'missed';
        } else if (!empty(product.custom.badges)) {
            badgeID = product.custom.badges[0];
        }
        if (!empty(badgeID)) {
        	productSearchHit.c_badges = badgeID;
        }

        // installments
        if (!empty(product.custom.installments) && product.custom.installments > 0) {
        	productSearchHit.c_installments = product.custom.installments;
        }

        // fix for images in plp
        if (productSearchHit.image != null) {
            productSearchHit.image.dis_base_link = productSearchHit.image.link;
        }

        // product thumbnail (workaround for missing scaling)
        var dynamicConfigFile = dw.system.Site.current.getCustomPreferenceValue('productImageConfig');
        var productImageConfig = new Object();
        if (dynamicConfigFile) {
        	productImageConfig = JSON.parse(dynamicConfigFile);
        } else {
        	productImageConfig = {
                    producttile: {
                        width: 310,
                        height: 310,
                        scaleMode: 'fit'
                    }
                };
        }
        
        var imageData = ProductSO(product).getProductImage(productImageConfig, 'producttile', 0, '', null, null, null);
        if (!empty(imageData) && !empty(imageData.url)) {
            productSearchHit.c_thumbnail = imageData.url.toString();
        }
	}

	//sorting product search hits based on promo pricing
	var productHits = productSearchHits; 
	if(productSearchResult.selectedSortingOption == 'priceDesc'){
		productHits.sort(function(a,b){return b.price - a.price});
	}else if(productSearchResult.selectedSortingOption == 'priceAsc'){
		productSearchResult.hits.sort(function(a,b){return a.price - b.price });
	}
	productSearchResult.hits = productHits;
	productSearchResult.count = productHits.length;

	return new dw.system.Status(dw.system.Status.OK);
};
