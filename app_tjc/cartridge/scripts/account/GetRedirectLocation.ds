/**
 * The script finds the last click and returns to it after login
 *
 * @input  Customer : dw.customer.Customer
 * @output TargetPipeline : String
 * @output TargetPipelineParams : String
 * @output Protocol : String
 * @input PageContext : String
 * @input CurrentHttpParameterMap : dw.web.HttpParameterMap
 *
 */
importPackage( dw.system );
importPackage( dw.web );
importPackage( dw.util );

function execute( pdict : PipelineDictionary ) : Number
{
	var newSign = pdict.CurrentHttpParameterMap.getParameterMap('newSignin').stringValue;
	var redirectold = true;
	if (!empty(newSign)) {
		redirectold = false;
	}
	if (redirectold && (empty(pdict.Customer) || !pdict.Customer.authenticated)) {
		var list : List = session.clickStream.clicks;
		for( var i = list.size()-1; i >= 0; i-- )
		{
			var click : ClickStreamEntry = list[i];
			switch( click.pipelineName )
			{
				case "Product-Show":
				case "Product-ShowInCategory":
					var params : Array;
					if (click.queryString) {
						params = click.queryString.split('&').filter(function (value : String) {
		                    return empty(value.match(/uuid=/)) && empty(value.match(/source=/)) && empty(value.match(/format=/));
						});
						pdict.TargetPipelineParams = params.join('&');
					}
					pdict.TargetPipeline = click.pipelineName;
					return PIPELET_NEXT;
				case "Search-Show":
					var params : Array;
	                if (click.queryString) {
	                    params = click.queryString.split('&').filter(function (value : String) {
	                        return empty(value.match(/source=/)) && empty(value.match(/format=/));
	                    });
	                    pdict.TargetPipelineParams = params.join('&');
	                }
					pdict.TargetPipeline = click.pipelineName;
					return PIPELET_NEXT;
				case "Page-Show":
					var params : Array;
	                if (click.queryString) {
	                    params = click.queryString.split('&').filter(function (value : String) {
	                        return empty(value.match(/source=/)) && empty(value.match(/format=/));
	                    });
	                    pdict.TargetPipelineParams = params.join('&');
	                }
					pdict.TargetPipeline = click.pipelineName;
					return PIPELET_NEXT;
				case "Cart-Show":
				    pdict.TargetPipeline = click.pipelineName;
	                pdict.Protocol = 'https';
	                return PIPELET_NEXT;
	            case "LiveTV-Start":
	            case "LiveTV-WatchTJC":
	            var params : Array;
	                if (click.queryString) {
	                    params = click.queryString.split('&').filter(function (value : String) {
	                        return empty(value.match(/source=/)) && empty(value.match(/format=/));
	                    });
	                    pdict.TargetPipelineParams = params.join('&');
	                }
	                pdict.TargetPipeline = click.pipelineName;
	                return PIPELET_NEXT;
	            case "LiveTV-WatchTJCBeauty":
	            	var params : Array;
	                if (click.queryString) {
	                    params = click.queryString.split('&').filter(function (value : String) {
	                        return empty(value.match(/source=/)) && empty(value.match(/format=/));
	                    });
	                    pdict.TargetPipelineParams = params.join('&');
	                }
	                pdict.TargetPipeline = click.pipelineName;
	                return PIPELET_NEXT;
	            case "LiveTV-MissedAuctions":
	                var params : Array;
	                if (click.queryString) {
	                    params = click.queryString.split('&').filter(function (value : String) {
	                        return empty(value.match(/source=/)) && empty(value.match(/format=/));
	                    });
	                    pdict.TargetPipelineParams = params.join('&');
	                }
	                pdict.TargetPipeline = click.pipelineName;
	                return PIPELET_NEXT;
	           case "Home-Show":
				    pdict.TargetPipeline = click.pipelineName;
	                return PIPELET_NEXT;
	             case "LiveTV-HomeGetRecentlyLiveProducts" :
	              // we need to redirect the home page after doing registration 
	              pdict.TargetPipeline = 'Home-Show';
	                return PIPELET_NEXT;   
	                
			}
		}
    } else if(!redirectold){
    	var httpParameterMap = pdict.CurrentHttpParameterMap;
    	var pageContext = httpParameterMap.pagecontext.stringValue;
    	switch (pageContext) {
    		case 'cart' : 
    				var targetparams= '';
	                if(httpParameterMap.redirecttjcplusactivation.booleanValue && httpParameterMap.tjcpluspid.stringValue){
	             		targetparams = targetparams + 'redirecttjcplusactivation=true';
	             		targetparams = targetparams + '&tjcpluspid='+httpParameterMap.tjcpluspid.stringValue;
	                }
    				pdict.TargetPipelineParams = targetparams;
    				
    				pdict.TargetPipeline = 'Cart-Show';
	                pdict.Protocol = 'https';
	                return PIPELET_NEXT;
	        case 'storefront' :
	             var targetparams= '';
	             if(httpParameterMap.redirectQuickBuyActivation.booleanValue){
	             	targetparams = targetparams + 'quickBuyClick=true';
	             }
	             if(!empty(httpParameterMap.variationValue.stringValue)){
	             	targetparams = targetparams + '&variationValue='+httpParameterMap.variationValue.stringValue;
	             }
	             if(!empty(httpParameterMap.quantity.stringValue)){
	             	targetparams = targetparams + '&quantity='+httpParameterMap.quantity.stringValue;
	             }
	             if(!empty(httpParameterMap.secondVariation.stringValue)){
                     targetparams = targetparams + '&secondVariation='+httpParameterMap.secondVariation.stringValue;
                 } 
	             pdict.TargetPipelineParams = targetparams;
	             pdict.TargetPipeline = 'Home-Show';
	                pdict.Protocol = 'https';
	                return PIPELET_NEXT;
	        case 'search' :
	              pdict.TargetPipeline = 'Search-Show';
	              var params = {};
	              if ( httpParameterMap.cgid.stringValue) {
						pdict.TargetPipelineParams = 'cgid='+httpParameterMap.cgid.stringValue;
	              }
	                return PIPELET_NEXT;
	        case 'livetv': 
	        	if ( httpParameterMap.pipeline.stringValue) {
	        		var targetPipeLine = '',targetparams='';
	        		var pipeline = httpParameterMap.pipeline.stringValue;
	        		if(pipeline == 'watch-tjc-beauty'){
	        			targetPipeLine = 'LiveTV-WatchTJCBeauty';
	        		} else if(pipeline == 'beauty-missed-auctions'){
	        			targetPipeLine = 'LiveTV-BeautyMissedAuctions';
	        		} else if (pipeline == 'live-tv') {
	        			targetPipeLine = 'LiveTV-Start';
	        		} else if(pipeline == 'missed-auctions'){
	        			targetPipeLine = 'LiveTV-MissedAuctions';
	        		}else if(pipeline == 'watch-tjc') {
	        			targetPipeLine = 'LiveTV-WatchTJC';
	        		} 
	        		
	              	pdict.TargetPipeline = targetPipeLine;
	              } else {
	        		if(httpParameterMap.channel.stringValue && httpParameterMap.channel.stringValue == 'tjcchoice'){
	              		pdict.TargetPipeline = 'LiveTV-WatchTJCBeauty';
	              	}else{
	              		pdict.TargetPipeline = 'LiveTV-WatchTJC';
	              	}
	        	  }
	              if(httpParameterMap.redirectQuickBuyActivation.booleanValue){
	             	targetparams = 'quickBuyClick=true';
	             }
	             if(!empty(httpParameterMap.variationValue.stringValue)){
	             	targetparams = targetparams + '&variationValue='+httpParameterMap.variationValue.stringValue;
	             }
	             if(!empty(httpParameterMap.quantity.stringValue)){
	             	targetparams = targetparams + '&quantity='+httpParameterMap.quantity.stringValue;
	             }if(!empty(httpParameterMap.secondVariation.stringValue)){
                     targetparams = targetparams + '&secondVariation='+httpParameterMap.secondVariation.stringValue;
                 } 
	             
	             pdict.TargetPipelineParams = targetparams;
	        	
	                return PIPELET_NEXT;
			case 'product' :	        	
    			pdict.TargetPipeline = 'Product-Show';
					var targetparams = '';
					if ( httpParameterMap.pid.stringValue) {
						targetparams = targetparams + 'pid='+httpParameterMap.pid.stringValue;
					}else if ( httpParameterMap.pids.stringValue) {
						targetparams = targetparams + 'pids='+httpParameterMap.pids.stringValue;
						targetparams = targetparams + '&pid='+httpParameterMap.pids.stringValue;
					}
					if ( httpParameterMap.redirectQuickBuyActivation.stringValue) {
						targetparams = targetparams + '&redirectQuickBuyActivation='+httpParameterMap.redirectQuickBuyActivation.stringValue;
					}
					if ( httpParameterMap.quantity.stringValue) {
						targetparams = targetparams + '&quantity='+httpParameterMap.quantity.stringValue;
					}
					if ( httpParameterMap.warrantyFlag.stringValue) {
						targetparams = targetparams + '&warrantyFlag='+httpParameterMap.warrantyFlag.stringValue;
						if ( httpParameterMap.warrantyQuantity.stringValue) {
							targetparams = targetparams + '&warrantyQuantity='+httpParameterMap.warrantyQuantity.stringValue;
						}
					}
					if ( httpParameterMap.engravingFlag.stringValue) {
						targetparams = targetparams + '&engravingFlag='+httpParameterMap.engravingFlag.stringValue;
						if ( httpParameterMap.engravingCount.stringValue) {
							targetparams = targetparams + '&engravingCount='+httpParameterMap.engravingCount.stringValue;
							var engravingCount = parseInt(httpParameterMap.engravingCount.stringValue),
							    engravingParameterId;
							for(var i=0; i<engravingCount; i++){
								engravingParameterId = 'engravingField'+(i+1);
								if(httpParameterMap[engravingParameterId].stringValue){
									targetparams = targetparams + '&'+engravingParameterId+'='+httpParameterMap[engravingParameterId].stringValue;
								}
							}
						}
					}
	              
	              if(httpParameterMap.redirecttjcplusactivation.booleanValue && httpParameterMap.tjcpluspid.stringValue){
	             		targetparams = targetparams + '&redirecttjcplusactivation=true';
	             		targetparams = targetparams + '&tjcpluspid='+httpParameterMap.tjcpluspid.stringValue;
	                }
	              
	              pdict.TargetPipelineParams = targetparams;
	                return PIPELET_NEXT;
	         case 'account' :
	       		  if ( httpParameterMap.pipeline.stringValue) {
	              	pdict.TargetPipeline = httpParameterMap.pipeline.stringValue;
	              }
	                return PIPELET_NEXT;
	         case 'content' :
	         	  if (httpParameterMap.cgid.stringValue) {
		         	  	pdict.TargetPipeline = 'Search-Show';
						pdict.TargetPipelineParams = 'cgid='+httpParameterMap.cgid.stringValue;
		                return PIPELET_NEXT;
	         	  } else if ( httpParameterMap.cid.stringValue) {
						pdict.TargetPipeline = 'Page-Show';
	              		pdict.TargetPipelineParams = 'cid='+httpParameterMap.cid.stringValue;
	              		return PIPELET_NEXT;
	         	  }
    	}
    	
	}
	
	// nothing found, go to the account page
	pdict.TargetPipeline = "Account-Show";
    return PIPELET_NEXT;
}
  