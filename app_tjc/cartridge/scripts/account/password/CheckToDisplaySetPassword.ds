/**
* Script file for use in the Script pipelet node.
* To define input and output parameters, create entries of the form:
*
* @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
*
* where
*   <paramUsageType> can be either 'input' or 'output'
*   <paramName> can be any valid parameter name
*   <paramDataType> identifies the type of the parameter
*   <paramComment> is an optional comment
*
* For example:
*
*   @input Customer : dw.customer.Customer
*   @output IsDisplaySetPassword : Boolean
*
*/


function execute( args : PipelineDictionary ) : Number
{
	if (args.Customer == null || args.Customer.profile.custom.hasSetPassword === true) {
		args.IsDisplaySetPassword = false;
		return PIPELET_NEXT;	
	}
	if (empty(args.Customer.profile.custom.lastShownSetPassword)) {
		args.IsDisplaySetPassword = true;
		return PIPELET_NEXT;
	}
	
	var currentDate = new Date(),
		daysToShowNext = dw.system.Site.getCurrent().getCustomPreferenceValue('daysToRedisplaySetPassword'),
		dateLastShown = args.Customer.profile.custom.lastShownSetPassword,
		nextDateToShow = new Date(dateLastShown.setDate(dateLastShown.getDate() + daysToShowNext));
	if (nextDateToShow < currentDate) {
		args.IsDisplaySetPassword = true;
	} else {
		args.IsDisplaySetPassword = false;	
	}
   	return PIPELET_NEXT;
}