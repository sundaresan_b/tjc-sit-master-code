/**
 * GetApplicableShippingMethods.ds
 *
 * This script retrieves the list of applicable shipping methods
 * for a given shipment.
 * A shipping method is applicable if it does not exclude any of 
 * the products in the shipment
 * 
 * @input Basket: dw.order.Basket
 * @input IgnoreCountry : Boolean
 * @input Customer : dw.customer.Customer
 * @output ShippingMethods : Object The shipping methods.
 */

importPackage( dw.customer );
importPackage( dw.order );
importPackage( dw.campaign );
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.web ); 

var TJCPlusHelper = require("~/cartridge/scripts/modules/tjcplus/TJCPlusHelper.ds");

function execute( pdict : PipelineDictionary ) : Number
{
    var basket : Basket = pdict.Basket,
        shipment : Shipment = basket.defaultShipment,
        productLis = basket.allProductLineItems.iterator(),
        shippingModel : ShipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment),
        shippingMethods : Collection = new ArrayList(),
        commonshippingMethods : Collection = new ArrayList(),
        nonPreOrderShippingMethods = new ArrayList(),
        nextDay : Calendar = Site.current.calendar,
        shippingMethod,
        availabilityStatus,
        product,
        hasPreOrderProducts = false;
	nextDay.add(Calendar.DATE, 1);
	
	//custom code: restricting shipping methods
	// only TJC Plus in cart - show only gb_express method
	// tjc plus clubbed with other products - show gb_express & gb_standard
	var plis = basket.getAllProductLineItems();
	var warrantyResult = require("~/cartridge/scripts/modules/WarrantyHelper").containsWarrantyProduct(basket);
	var isTJCPlusSubscribedCustomer = pdict.Customer && pdict.Customer.isAuthenticated() && pdict.Customer.profile.custom.isTJCPlusMember;
	
	if(isTJCPlusSubscribedCustomer && !warrantyResult.containsWarrantyProduct){
		var shippingResult = TJCPlusHelper.getSubscriptionDeliveryMethods(shippingModel,false);
		shippingMethods = shippingResult;
		
		pdict.ShippingMethods = shippingMethods;
    	return PIPELET_NEXT;
	}
	else if(isTJCPlusSubscribedCustomer && warrantyResult.containsWarrantyProduct){
		var shippingResult = TJCPlusHelper.getSubscriptionDeliveryMethods(shippingModel,false);
		var warrantyshippingmethods = getWarrantyDeliveryMethods();
		shippingMethods = shippingResult;
		
		for each(var method in shippingMethods){
			for each(var warrantymethod in warrantyshippingmethods.warrantyMethods){
				if(method.ID.equals(warrantymethod.ID))
					commonshippingMethods.add(method);
			}
		}
		
		pdict.ShippingMethods = commonshippingMethods;
    	return PIPELET_NEXT;
	}
	
	var tjcResult = TJCPlusHelper.containsTJCPlusProduct(basket);
	
	if(tjcResult.containsTJCPlusProduct && !warrantyResult.containsWarrantyProduct){
		var tjcplusshippingmethods = TJCPlusHelper.getTJCPlusDeliveryMethods();
		
    	if(plis.length == 1){
	    	shippingMethods = tjcplusshippingmethods.tjcplusexclusive;
    	}else {
    		shippingMethods = tjcplusshippingmethods.tjcplusclubbed;
    	}
	}
	else if(warrantyResult.containsWarrantyProduct){
		var warrantyshippingmethods = getWarrantyDeliveryMethods();
		
	    	shippingMethods = warrantyshippingmethods.warrantyMethods;
	}else{
		shippingMethods = shippingModel.getApplicableShippingMethods();
		
		//find the next day delivery method
	    var nextDayDeliveryMethod : ShippingMethod,
	        iterator : Iterator = shippingMethods.iterator();
	    while (iterator.hasNext()) {
	    	shippingMethod = iterator.next();
	    	if (shippingMethod.ID == 'gb_express') {
	    		nextDayDeliveryMethod = shippingMethod;
	    		continue;
	    	}
	    	if ('nonPreOrder' in shippingMethod.custom && shippingMethod.custom.nonPreOrder) {
				nonPreOrderShippingMethods.add(shippingMethod);
	    	}
	    }
		
		while (productLis.hasNext()) {
			product = productLis.next().product;
			availabilityStatus = product.availabilityModel.availabilityStatus;
			if (availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER) {
				hasPreOrderProducts = true;	
			}
		}
		
		//get the list of holidays
		var countryCode : String = !empty(shipment.shippingAddress) && (empty(pdict.IgnoreCountry) || !pdict.IgnoreCountry) ? shipment.shippingAddress.countryCode.value : null,
	        holidayTable : Object = JSON.parse(Site.getCurrent().getCustomPreferenceValue('nextDayDeliveryHolidayTable')),
	        holidays : HashSet = new HashSet();
		if (!empty(countryCode)) {
			var holidayList = holidayTable[countryCode];
			if (!empty(holidayList)) {
	    	   holidays.addAll(new ArrayList(holidayList));
			}
		}
		holidays.addAll(new ArrayList(holidayTable['global']));
		
	    //check if tomorrow is an holiday
	    var nextDayString =  StringUtils.formatCalendar(nextDay, 'dd/MM');
		if (holidays.contains(nextDayString)) {
			shippingMethods.remove(nextDayDeliveryMethod);
		}
		
		//check if tomorrow is on the weekend
		var excludeWeekend : Boolean = Site.getCurrent().getCustomPreferenceValue('nextDayDeliveryExcludeWeekend'),
	        weekDay : String = nextDay.get(Calendar.DAY_OF_WEEK);
		if (excludeWeekend && ( weekDay == Calendar.SATURDAY || weekDay == Calendar.SUNDAY) ) {
			shippingMethods.remove(nextDayDeliveryMethod);
		}
		
		// if pre order products are in basket, remove selected shipping methods
		if (hasPreOrderProducts) {
			for (var i=0; i < nonPreOrderShippingMethods.length; i++) {
				shippingMethods.remove(nonPreOrderShippingMethods[i]);
			}
		}
	}
	
	pdict.ShippingMethods = shippingMethods;
    return PIPELET_NEXT;
}


function getTJCPlusDeliveryMethods(){
	var tjcplusexclusiveShippingMethods = new ArrayList();
	var tjcplusclubbedShippingMethods = new ArrayList();
	
	var iterator = ShippingMgr.getAllShippingMethods().iterator(); 
	while (iterator.hasNext()) {
    	var shippingMethod = iterator.next();
    	
		if (!empty(shippingMethod) && shippingMethod.isOnline()) {
    		if(!empty(shippingMethod.custom.tjcplusExclusiveMethod) && shippingMethod.custom.tjcplusExclusiveMethod){
    			tjcplusexclusiveShippingMethods.add(shippingMethod);
    		}
    		if(!empty(shippingMethod.custom.tjcplusClubbedMethod) && shippingMethod.custom.tjcplusClubbedMethod){
    			tjcplusclubbedShippingMethods.add(shippingMethod);
    		}
    	}
    }
    
	return {
		"tjcplusexclusive" : tjcplusexclusiveShippingMethods,
		"tjcplusclubbed" : tjcplusclubbedShippingMethods
	};
}

function getWarrantyDeliveryMethods(){
	var warrantyMethods = new ArrayList();
	
	var iterator = ShippingMgr.getAllShippingMethods().iterator(); 
	while (iterator.hasNext()) {
    	var shippingMethod = iterator.next();
    	
		if (!empty(shippingMethod) && shippingMethod.isOnline()) {
    		if(!empty(shippingMethod.custom.warrantyEligibleShippingMethod) && shippingMethod.custom.warrantyEligibleShippingMethod){
    			warrantyMethods.add(shippingMethod);
    		}
    	}
    }
    
	return {
		"warrantyMethods" : warrantyMethods
	};
}