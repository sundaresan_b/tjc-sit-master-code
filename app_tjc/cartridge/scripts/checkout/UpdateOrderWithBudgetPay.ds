/**
 * Script to construct the String, that will be saved in
 * the 'orderInstallments' attribute of the order/basket.
 * 
 * @input Basket: dw.order.Basket
 * @output PaymentAmount: dw.value.Money
 *
 */
importPackage( dw.order );
importPackage( dw.system );

importScript("cart/BudgetPayUtils.ds");

function execute( pdict : PipelineDictionary ) : Number
{
	var basket : Basket = pdict.Basket;
	var budgetPay = BudgetPayUtils.getInstallments(basket);
	var installmentsString : String = '';
	var orderTotalValue : Money;
	if (!empty(budgetPay) && budgetPay.available) {
		installmentsString += "'" + budgetPay.firstInstallment.value + "', ";
		
		for (var i = 1; i < budgetPay.installments; i++) {
			installmentsString += "'" + budgetPay.installment.value + "'";
			if (i < budgetPay.installments - 1) {
				installmentsString += ", ";
			}
		}

		basket.custom.orderInstallments = installmentsString;

		//pdict.PaymentAmount = budgetPay.firstInstallment;
		if (basket.totalGrossPrice.available) {
			orderTotalValue = basket.totalGrossPrice;
		} else {
			orderTotalValue = basket.getAdjustedMerchandizeTotalPrice(true).add(basket.giftCertificateTotalPrice);
		}
		
		// Payment amount if Basket value - the remaining installments which is the same as
		// total of non-BP items + shipping + first installment
		pdict.PaymentAmount = orderTotalValue.subtract(budgetPay.installment.multiply(budgetPay.installments - 1));

		return PIPELET_NEXT;
	}

	return PIPELET_ERROR;
}