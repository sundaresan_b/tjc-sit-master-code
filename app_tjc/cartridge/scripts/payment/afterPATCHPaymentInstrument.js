/**
 *
 * This script updates the payment instrument after authorization call
 *
 */

var Logger = require("dw/system").Logger;
var OrderMgr = require('dw/order/OrderMgr');
var ArrayList = require('dw/util/ArrayList');
var ProductLineItem = require('dw/order/ProductLineItem');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var RAStatus = require("app_tjc/cartridge/scripts/risingauctions/objects/RAStatus");

exports.afterPatchPaymentInstrument = function( order, paymentInstrument, newPaymentInstrument, successfullyAuthorized) {
	var Status = require('dw/system/Status');
    var PaymentMgr = require("dw/order/PaymentMgr");
    var OrderMgr = require("dw/order/OrderMgr");
    var Order = require("dw/order/Order");
	var Pipeline = require("dw/system").Pipeline;
	var Transaction = require("dw/system/Transaction");
	
	var PaymentInstrument = paymentInstrument;
	var paymentProcessorID = PaymentInstrument.paymentMethod;
	var pdict; 
	
	if(!empty(paymentProcessorID) && paymentProcessorID == 'TJCCredit'){
		try{
				Transaction.wrap(function(){
					var placeOrderStatus = OrderMgr.placeOrder(order);
					if (!placeOrderStatus.error) {
						order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
					    order.setExportStatus(Order.EXPORT_STATUS_READY);
					    
					    pdict = Pipeline.execute('PAYSAFE_DIRECT_PAYMENT-TJCCredit', {
							Order : order
						 });
						 CreateAuctionStatusUpdate(order);
					    
				    }else{
				    	Logger.error('Order placing has failed for '+ order.orderNo);
				    }
				});
			}
			catch(e){
				Logger.error('Error while placing the order '+ order.orderNo);
				return new Status(Status.ERROR);
			}
	}
	else if(!empty(paymentProcessorID) && paymentProcessorID == 'PAYPAL' && successfullyAuthorized){
		//Call Paypal authorization flow
		pdict = Pipeline.execute('PaypalService-DoAuthorization', {
							Order : order
						 });
		
		if(pdict != undefined && pdict.EndNodeName == 'SUCCESS'){
			try{
				Transaction.wrap(function(){
					var placeOrderStatus = OrderMgr.placeOrder(order);
					if (!placeOrderStatus.error) {
					    order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
					    order.setExportStatus(Order.EXPORT_STATUS_READY);
				    }else{
				    	Logger.error('Order placing has failed for '+ order.orderNo);
				    }
				});
			}
			catch(e){
				Logger.error('Error while placing the order '+ order.orderNo);
				return new Status(Status.ERROR);
			}
		}
		else{
			Transaction.wrap(function(){
				OrderMgr.failOrder(order, true);
			});
			return new Status(Status.ERROR);
		}
	}
	else if(!empty(paymentProcessorID) && (paymentProcessorID == 'Paysafe' || paymentProcessorID == 'BudgetPay')){
		var cardDetails = JSON.parse(paymentInstrument.custom.cardDetails);
		var existingCards = JSON.parse(paymentInstrument.custom.existingCards);	
		
		var existingCards = new Object();
		existingCards.value = String(paymentInstrument.custom.existingCards);
		Transaction.wrap(function () {
        	pdict = Pipeline.execute('PAYSAFE_DIRECT_PAYMENT-Authorize', {
	    		Order : order,
	    		PaymentInstrument : PaymentInstrument,
	    		isMobileOrder : true,
	    		orderID : order.orderNo ,
	    		CVV : newPaymentInstrument.paymentCard.securityCode,
	    		existingCards : existingCards,
	    		existingCardsForget : String(cardDetails.existingCardsForget),
	    		newCardsRemember : String(cardDetails.newCardsRemember)
   			});
   			importScript("cart/BudgetPayUtils.ds");
   			var budgetPay = BudgetPayUtils.getInstallments(order);
   			if (!empty(paymentProcessorID) && paymentProcessorID == 'BudgetPay' && !empty(budgetPay) && budgetPay.available) {
        		PaymentInstrument.paymentTransaction.amount = new dw.value.Money((order.totalGrossPrice.subtract(budgetPay.installment.multiply(budgetPay.installments - 1))).value,order.currencyCode);
        	}
        });
        
        if(pdict != undefined && pdict.EndNodeName == 'SUCCESS'){
			try{
				Transaction.wrap(function(){
					var placeOrderStatus = OrderMgr.placeOrder(order);
					if (!placeOrderStatus.error) {
						paymentInstrument.custom.existingCards = '';
						paymentInstrument.custom.cardDetails = '';
					    order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
					    order.setExportStatus(Order.EXPORT_STATUS_READY);
					    
				    }else{
				    	Logger.error('Order placing has failed for '+ order.orderNo);
				    }
				    CreateAuctionStatusUpdate(order);
				});
			}
			catch(e){
				Logger.error('Error while placing the order '+ order.orderNo);
				return new Status(Status.ERROR);
			}
		}
		else{
			Transaction.wrap(function(){
				OrderMgr.failOrder(order, true);
			});
			
			var errorMsg = !empty(pdict.NetbanxPlaceOrderError) ? pdict.NetbanxPlaceOrderError : '';
			var errorCode = !empty(pdict.PaysafeResponse) && !empty(pdict.PaysafeResponse.content) && !empty(pdict.PaysafeResponse.content.error) && !empty(pdict.PaysafeResponse.content.error.code) ? pdict.PaysafeResponse.content.error.code : '';
			
			return new Status(Status.ERROR,errorCode,errorMsg,'');
		}
	
	}
	
	return new Status(Status.OK);
};


function CreateAuctionStatusUpdate(order) {
	var order = order,
    	auction,
    	status,
    	statusObj,
    	statusList = [],
    	pli,
    	raStatus = new RAStatus();
	try {
	    var lineItems : ArrayList = order.getProductLineItems();    	
	    //loop over all the product order lines
	    for each (var pli : ProductLineItem in lineItems) {
	    	//if the product is part of a Rising Auction
	    	if (pli.custom.itemSource === "RISING") {
	    		//Get the auction
	    		auction = CustomObjectMgr.getCustomObject("Auction", pli.custom.auctionCode);
				//If the auction object exist
				if (auction) {
					// add status in order to set the item to PAID
					statusObj = {
						auctionID : auction.custom.auctionID,
						customerID : auction.custom.highestBidderID,
						status : raStatus.PAID.ID,
						timestamp : new Date().getTime()
					}
					
					// add current statusObj to the List
					statusList.push(JSON.stringify(statusObj));
	    		} else {
	    			//The auction object do not exist anymore: the payment time is expired or there is an unespected data issue
	    			//Log the condition as an error
	    			Logger.error("CHECKOUT - CreateAuctionStatus - Auction CO not found. Order no: {0}, auction no: {1}, product id: {2}", order.getOrderNo(), pli.custom.auctionCode, pli.getProductID());
	    		}
	    	}
		} 
		
		//Create the status update CO and set the list
		if (!empty(statusList)) {						
			status = CustomObjectMgr.createCustomObject("AuctionStatusUpdates", dw.util.UUIDUtils.createUUID());
			status.custom.statusList = statusList;
		}
		
	} catch (e) {
		Logger.error("CHECKOUT - CreateAuctionStatus - Impossible to create the Status Update for Order no: {0}. Please check the order", order.getOrderNo(), e);
	 	return PIPELET_ERROR;
	}
}