/**
 *
 * This script updates the payment instrument after authorization call
 *
 */

"use strict";
 
/** Package Include **/

var Status = require("dw/system/Status");
importScript("cart/BudgetPayUtils.ds");
importScript("checkout/Utils.ds");

exports.beforePostPaymentInstrument = function( basket, paymentInstrument) {
	var status = new Status(Status.OK);
	var Transaction = require("dw/system/Transaction");
	var OrderMgr = require('dw/order/OrderMgr');
	var Logger = require("dw/system").Logger;
	var installmentsStringObj = {};
	installmentsStringObj.installmentsStringArray = [];
	var paymentProcessorID = paymentInstrument.paymentMethodId;
	
	try {
		var budgetPay = BudgetPayUtils.getInstallments(basket);
			Transaction.wrap(function(){
				if (!empty(budgetPay) && budgetPay.available) {
					installmentsStringObj.installmentsStringArray.push(budgetPay.firstInstallment.value);
					
					for (var i = 1; i < budgetPay.installments; i++) {
						installmentsStringObj.installmentsStringArray.push(budgetPay.installment.value);
					}
					basket.custom.mobileorderInstallments = JSON.stringify(installmentsStringObj);
				}
				
				if (!empty(paymentProcessorID) && paymentProcessorID == 'BudgetPay' && !empty(budgetPay) && budgetPay.available) {
					paymentInstrument.amount = (basket.totalGrossPrice.subtract(budgetPay.installment.multiply(budgetPay.installments - 1))).value;
					
					var installmentsString = '';
					installmentsString += "'" + budgetPay.firstInstallment.value + "', ";
					for (var i = 1; i < budgetPay.installments; i++) {
						installmentsString += "'" + budgetPay.installment.value + "'";
						if (i < budgetPay.installments - 1) {
							installmentsString += ", ";
						}
					}
					basket.custom.orderInstallments = installmentsString;
				} else {
					paymentInstrument.amount = calculateNonGiftCertificateAmount(basket).value;
				}	
			});
		} catch (e) {
	        Logger.error("Error occur in beforePOST PaymentInstrument " + e.message);
	    }
		return status;
		
	};
	
