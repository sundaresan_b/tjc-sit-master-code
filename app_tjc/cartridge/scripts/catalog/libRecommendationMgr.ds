importPackage(dw.system);
importPackage(dw.util);
importPackage(dw.web);
importPackage(dw.catalog);

importScript("app_tjc:util/libUtils.ds");

const MAX_COUNT = Number(Utils.getSetting( 'recommendations.maxcount' )) || 10;

/* PUBLIC ***********************************************************************************************************/
function RecommendationMgr () {
}

/**
 * Returns the recommendations as defined in the Demandware Standard (for the product).
 */
RecommendationMgr.getProductRecommendations = function (product : Product) {
	// get all orderable cross sell recommendations (1 = cross sell)
	var recommendations : Iterator = product.getOrderableRecommendations(1).iterator(),
		recProducts : Collection = new ArrayList(),
		recommendation : Recommendation,
		maxAPIRecommendationsCount = dw.system.Site.getCurrent().getPreferences().getCustom()['maxAmountOfProductsToShowFromRecommendation'] || 0,
		count = 0;
	while( recommendations.hasNext() && count < maxAPIRecommendationsCount) {
		recommendation = recommendations.next();
		recProducts.add( recommendation.getRecommendedItem() );
		count++;
	}
	
	return recProducts;
}

/**
 * Returns the Array List of products necessary for a product include - in this case the recommendation
 * include. The configuration of the recommendation rule is made at the primary category of the given
 * product. This will be used as a fallback or default recommendation rule on the PDP.
 */
RecommendationMgr.getConfiguredRecommendations = function (product : Product, orderable : Boolean) {
	var recommendations : ArrayList = new ArrayList(),
    	cnt = 0,
    	masterProduct : Product = product.variant ? product.variationModel.master : product;
    
   		       
	var category : Category = null;
    if (!empty(masterProduct.primaryCategory)) {
		category = masterProduct.primaryCategory;
    }
    // fallback - randomly pick a category
    if (empty(category)) {
    	category = masterProduct.categories && masterProduct.categories.size() > 0 ? masterProduct.categories[0] : null;
    }
    
	if (!empty(category)) {
   
		var onlineProducts : Collection = RecommendationMgr.getConfiguredRecommendationsByCategory(category);
		
		// randomly select the products
		var pidList : Set = new HashSet();
		for (var i = 0; i < onlineProducts.length; i++) {
			var idx = Math.floor((Math.random()*onlineProducts.length));
			var rec : Product = onlineProducts[idx];
			if (!empty(rec) && masterProduct.ID != rec.ID && !pidList.contains(rec.ID) && (orderable ? rec.availabilityModel.orderable : true)) {
				recommendations.add(rec);
				pidList.add(rec.ID);
				cnt++;
			}
			if (cnt == MAX_COUNT) {
				break;
			}
		}
	}
	
	return recommendations;
}

/**
 * Gets the recommendations of the given product, which are the manually maintained recommendations
 * plus random recommendations using the category configuration.
 */
RecommendationMgr.getRecommendations = function (product : Product, orderable : Boolean) {
	var recommendations : Collection = RecommendationMgr.getProductRecommendations(product);
	recommendations.addAll(RecommendationMgr.getConfiguredRecommendations(product, orderable));
	return recommendations;
}

/**
 * Returns the recommendations for the empty cart page.
 */
RecommendationMgr.getEmptyCartRecommendations = function () {
	var recommendations : Collection = new ArrayList(),
    	cnt = 0;
   		       
	var category : Category = CatalogMgr.getCategory('shop-navigation');
    
	if (!empty(category)) {
   
		var onlineProducts : Collection = RecommendationMgr.getConfiguredRecommendationsByCategory(category);
		
		// randomly select the products
		var pidList : Set = new HashSet();
		for (var i = 0; i < onlineProducts.length; i++) {
			var idx = Math.floor((Math.random()*onlineProducts.length));
			var rec : Product = onlineProducts[idx];
			if (!empty(rec) && !pidList.contains(rec.ID) && rec.availabilityModel.orderable) {
				recommendations.add(rec);
				pidList.add(rec.ID);
				cnt++;
			}
			if (cnt == MAX_COUNT) {
				break;
			}
		}
	}
	
	return recommendations;
}

/**
 * Gets the recommendations for the category.
 */
RecommendationMgr.getConfiguredRecommendationsByCategory = function(category : Category) {
	var recommendationConfigJSON = category.custom.recommendationConfiguration,
		recommendationConfig,
		targetCategoryId = category.ID,
		sortingRuleId,
		sortingRule,
		searchModel : ProductSearchModel = new ProductSearchModel(),
		elementCount = Number(Utils.getSetting( 'recommendations.pickcount' )) || 20;

   	// Get the configuration values from the category
	if (!empty(recommendationConfigJSON)) {
		try {
			recommendationConfig = JSON.parse(recommendationConfigJSON);
			targetCategoryId = recommendationConfig.targetCategoryId;
			sortingRuleId = recommendationConfig.sortingRuleId;
			sortingRule = CatalogMgr.getSortingRule(sortingRuleId);
		}
		catch (e) {/* ignore the JSON parse error and use the fallback */}
	}
   
	// do a search using the configuration values
	searchModel.setCategoryID(targetCategoryId);
	if (sortingRule) {
		searchModel.setSortingRule(sortingRule);
	}
	else {
		searchModel.setSortingRule(category.getDefaultSortingRule());
	}
	searchModel.search();
			
	// add the first products in a list
	var onlineProducts : ArrayList = new ArrayList(),
		pagingModel : PagingModel = new PagingModel(searchModel.products, searchModel.count),
		it : Iterator;
	pagingModel.setPageSize(elementCount);
	pagingModel.setStart(0);
	it = pagingModel.getPageElements();
	while (it.hasNext()) {
		onlineProducts.add(it.next());
	}
	
	return onlineProducts;
}

/**
 * Returns the recommendations for the category page.
 */
RecommendationMgr.getCategoryRecommendations = function (category : Category) {
	var recommendations : Collection = new ArrayList(),
    	cnt = 0;


	if (!empty(category)) {
   
		var onlineProducts : Collection = RecommendationMgr.getConfiguredRecommendationsByCategory(category);
		
		// randomly select the products
		var pidList : Set = new HashSet();
		for (var i = 0; i < onlineProducts.length; i++) {
			var idx = Math.floor((Math.random()*onlineProducts.length));
			var rec : Product = onlineProducts[idx];
			if (!empty(rec) && !pidList.contains(rec.ID) && rec.availabilityModel.orderable) {
				recommendations.add(rec);
				pidList.add(rec.ID);
				cnt++;
			}
			if (cnt == MAX_COUNT) {
				break;
			}
		}
	}
	
	return recommendations;
}

/**
 * Returns the recommendations for the order confirmation page.
 */
RecommendationMgr.getOrderConfirmationRecommendations = function () {
	var recommendations : Collection = new ArrayList(),
    	cnt = 0;
   		       
	var category : Category = CatalogMgr.getCategory('shop-navigation');
    
	if (!empty(category)) {
   
		var onlineProducts : Collection = RecommendationMgr.getConfiguredRecommendationsByCategory(category);

		// randomly select the products
		var pidList : Set = new HashSet();
		for (var i = 0; i < onlineProducts.length; i++) {
			var idx = Math.floor((Math.random()*onlineProducts.length));
			var rec : Product = onlineProducts[idx];
			if (!empty(rec) && !pidList.contains(rec.ID) && rec.availabilityModel.orderable) {
				recommendations.add(rec);
				pidList.add(rec.ID);
				cnt++;
			}
			if (cnt == MAX_COUNT) {
				break;
			}
		}
	}
	
	return recommendations;
}

RecommendationMgr.getPDPRecommendations = function (product : Product) {
	// get all recommendations
	var recommendations : Iterator = product.getRecommendations().iterator(),
		recProducts : Collection = new ArrayList(),
		recommendation : Recommendation;
	while( recommendations.hasNext()) {
		recommendation = recommendations.next();
		recProducts.add( recommendation.getRecommendedItem() );
	}
	
	return recProducts;
}
