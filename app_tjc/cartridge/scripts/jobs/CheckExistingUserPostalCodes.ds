/**
* Script file for use in the Script pipelet node.
* To define input and output parameters, create entries of the form:
*
* @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
*
* where
*   <paramUsageType> can be either 'input' or 'output'
*   <paramName> can be any valid parameter name
*   <paramDataType> identifies the type of the parameter
*   <paramComment> is an optional comment
*
* For example:
*
*-   @input ExampleIn : String This is a sample comment.
*-   @output ExampleOut : Number
*
*/

importPackage( dw.io );
importPackage( dw.util );
importPackage( dw.customer );
var Logger = require('dw/system/Logger');

function execute( args : PipelineDictionary ) : Number
{
	try {
		Logger.info("Start of CheckExistingUserPostalCodes");
		var queryString = "customerNo  != null";
		var csvWriter : CSVStreamWriter;
		var file : File = new File(  File.IMPEX + File.SEPARATOR + "src" + File.SEPARATOR + "postcode.csv");
		var fileWriter = new FileWriter(file);
		csvWriter = new CSVStreamWriter(fileWriter);
	    var profiles : dw.util.SeekableIterator = CustomerMgr.searchProfiles( queryString, null );
	    csvWriter.writeNext(["Customer ID", "Postal code"]);
	    if (!empty(profiles)) {
	    	for each (var profile in profiles) {
	    		if (profile.addressBook.addresses.size() > 0) {
			    	var addresses = profile.addressBook.getAddresses();
			    	for each ( var address in addresses ){
			    		var postalCode = address.postalCode;
			    		var regex : RegExp = new RegExp('[^A-Za-z0-9 ]');
			    		var isValid = regex.test(postalCode);
			    		var responseMap = new HashMap();
			    		if (isValid) {
			    			responseMap.put(address.ID, postalCode);
			    			csvWriter.writeNext([ profile.customerNo, postalCode ]);
			    		}
			    	}
		    	}
	    	}
	    }
	} catch(e){
		Logger.error ("Check existing User Postal codes : {0} ",e.message);
		return PIPELET_ERROR;
	}   
	csvWriter.close();
	fileWriter.close();
	return PIPELET_NEXT;
}

module.exports = {
	execute: function(){
		return execute();
	}
}