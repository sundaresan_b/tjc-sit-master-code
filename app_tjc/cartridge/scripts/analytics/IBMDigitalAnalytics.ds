importPackage( dw.system );
importPackage( dw.catalog );
importPackage( dw.util );
importPackage( dw.web );
importPackage( dw.order );
importPackage( dw.customer );

var clientID = Site.getCurrent().getCustomPreferenceValue('ibmDigitalAnalyticsClientID'),
    searchIbmCategoryID = 'SE1000';

function createAttributeString(attributes){
	var attributeString;
	if (empty(attributes)) {
		return;
	}
    for(var i=0;i < attributes.length; i++){
        var attribute = attributes[i] || '';
        if (i > 0) {
            attributeString += '-_-' + attribute;
        } else {
            attributeString = attribute;
        }
    }
    return attributeString;
}

function createArgumentsString(){
    var takeAll = false,
        argumentsArray : Array = [];	   

    for(var i=arguments.length - 1;i >= 0; i--){
    	var argument = arguments[i];
    	if (!takeAll && empty(argument)){
    	   continue;
    	}
    	
    	takeAll = true;
    	if(!empty(argument) && typeof argument === "string"){
    		argument = argument.replace("'","\\'");
    	}
    	argumentsArray.unshift(argument);  
    	 
    }	    
	return argumentsArray.join('\', \'');
}

    
var wrapExtern = function (tag){
    return '\n    window.ibmCallbacks = window.ibmCallbacks || [];\n    ' +
       'window.ibmCallbacks.push(function() {\n        ' + tag + '\n    });\n'; 
}

module.exports.wrapTags = wrapExtern; 

module.exports.isEnabled = function(){
	return !empty(clientID);
}

module.exports.generateliveTVBidTag = function(product : Product, customer : Customer, bid : Number, startPrice : Number) {
    if (empty(product) || empty(customer)) return;
    
    var customerCode = customer.profile && customer.profile.custom.amsCustomerCode;
    
    var attributes = [product.custom.laAuctionCode, product.name, product.ID, customerCode, bid, bid, startPrice];
	return 'cmCreateElementTag(\'' + createArgumentsString('LIVETV-CONFIRMBID', 'BID', createAttributeString(attributes)) + '\');';
}

module.exports.generatePlaceRaBidTag = function(product : Product, customer : Customer, bid : Number, previousBid : Number, isAutoBid : Boolean) {
	if (empty(product) || empty(customer)) return;
	
	var productID = product.ID;
	if (!empty(product.custom.raAuctionCode)) {
	   	productID = product.custom.raMainProductCode;
	}
	
	var customerCode = customer.profile && customer.profile.custom.amsCustomerCode;
	
	var attributes = [product.custom.raAuctionCode, product.name, productID, customerCode, bid, previousBid || product.custom.raStartPrice, product.custom.raStartPrice];
	var elementID = isAutoBid ? 'RAMAXBID' : 'RABIDNOW';
	return wrapExtern('cmCreateElementTag(\'' + createArgumentsString(elementID, 'RA Bid Now', createAttributeString(attributes)) + '\');');
}

module.exports.generateRegistrationTag = function(customer : Customer, order : Order, iswebcustomer : Boolean) {
    var registrationID, email, address, webCustomer; 
    
    if (empty(customer) || (empty(customer.profile) && empty(order))) return;
    
    if (!empty(customer.profile)) {
        registrationID = customer.profile.custom.amsCustomerCode;
        email = customer.profile.email;
        address = customer.addressBook.preferredAddress || (customer.addressBook.addresses.length > 0 && customer.addressBook.addresses[0]) || (order && order.billingAddress) || {};
	} else {
		registrationID = order.customerEmail;
		email = order.customerEmail;
		address = order.billingAddress;
	}
	
	if (!empty(iswebcustomer) && iswebcustomer != false) {
        webCustomer = 'NEW';
    } else {
        	webCustomer = '';
    }
	
	return wrapExtern('cmCreateRegistrationTag(\'' + createArgumentsString(registrationID, email, address.city, null, address.postalCode, address && address.countryCode && address.countryCode.displayValue, webCustomer) + '\');');
}

module.exports.generateOrderTag = function (order : Order){
    if (empty(order)) return;

    var attributes : Array = new Array(7),
        productLineItems : Iterator = order.productLineItems.iterator();
    
    while (productLineItems.hasNext()) {
        var productLineItem : ProductLineItem = productLineItems.next();
        switch (productLineItem.custom.itemSource) {
            case 'RISING':
	            attributes[1] = 'RA';
	            break;   
	        case 'TV':
	            attributes[2] = 'Live TV';
	            break;   
            case 'WEBC':
	            attributes[3] = 'Missed Auctions';
	            break;   
	        case 'TGTV':
	            attributes[5] = 'Live TV TJC Choice';
	            break;
	        case 'TGWEBC':
	            attributes[6] = 'Missed Auctions TJC Choice';
	            break;   
            default: 
                attributes[0] = 'FPC';
	    }
	    if (productLineItem.custom.thirdPartyName == 'PoQ') {
	        attributes[4] = 'PoQ';
	    } else {
	        attributes[4] = 'Website';
	    }
    }
    
    var registrationID = !empty(order.customer.profile) ? order.customer.profile.custom.amsCustomerCode : order.customerEmail;
    return wrapExtern('cmCreateOrderTag(\'' + createArgumentsString(order.orderNo, order.getAdjustedMerchandizeTotalPrice(false), order.shippingTotalGrossPrice, registrationID, order.billingAddress.city, null, order.billingAddress.postalCode, createAttributeString(attributes)) + '\');');
}
 
module.exports.generateShop9Tag = function (productLineItem : ProductLineItem, order : Order){
    var salesChannel = 'FPC';
    
    if (empty(productLineItem) || empty(order)) return;
    
    var ibmDigitalAnalyticsCategoryID = productLineItem.custom.ibmDigitalAnalyticsCategoryID;
    
    switch (productLineItem.custom.itemSource) { 
       case 'RISING':
           salesChannel = 'RA';
           break;   
       case 'WEBC':
           salesChannel = 'Missed Auction';
           break;   
       case 'TV':
           salesChannel = 'Live TV';
           break;   
       case 'TGWEBC':
           salesChannel = 'Missed Auction: TJC Choice';
           break;   
       case 'TGTV':
           salesChannel = 'Live TV: TJC Choice';
           break;
    }
    var registrationID = !empty(order.customer.profile) ? order.customer.profile.custom.amsCustomerCode : order.customerEmail,
        platform;
    if (productLineItem.custom.thirdPartyName == 'PoQ') {
        platform = 'PoQ';
    } else {
        platform = 'Website';
    }
    var product : Product = productLineItem.product;
    var attributes = [salesChannel, product.custom.articleType, product.custom.mainStoneName, product.brand, product.custom.metalName, platform]; 
    return 'cmCreateShopAction9Tag(\'' + createArgumentsString(productLineItem.productID, productLineItem.productName, productLineItem.quantityValue, productLineItem.basePrice.decimalValue, registrationID, order.orderNo, order.getAdjustedMerchandizeTotalPrice(false), ibmDigitalAnalyticsCategoryID, createAttributeString(attributes)) + '\');';
}

module.exports.generateShop5Tag = function (productLineItem : ProductLineItem, quantity : Number){
    var salesChannel = 'FPC';
    
    if (empty(productLineItem)) return;
    var ibmDigitalAnalyticsCategoryID = productLineItem.custom.ibmDigitalAnalyticsCategoryID;
    
    if (empty(quantity)) return;
    
	switch (productLineItem.custom.itemSource) {
	   case 'RISING':
	       salesChannel = 'RA';
	       break; 	
	   case 'WEBC':
	       salesChannel = 'Missed Auction';
	       break; 	
	   case 'TV':
	       salesChannel = 'Live TV';
	       break; 	
	   case 'TGWEBC':
	       salesChannel = 'Missed Auction: TJC Choice';
	       break; 	
	   case 'TGTV':
	       salesChannel = 'Live TV: TJC Choice';
	       break;
	}
	var platform;
    if (productLineItem.custom.thirdPartyName == 'PoQ') {
        platform = 'PoQ';
    } else {
        platform = 'Website';
    }
	var product : Product = productLineItem.product;
	if (empty(product)) return;
    var attributes = [salesChannel, product.custom.articleType, product.custom.mainStoneName, product.brand, product.custom.metalName, platform]; 
    return 'cmCreateShopAction5Tag(\'' + createArgumentsString(productLineItem.productID, productLineItem.productName, quantity, productLineItem.basePrice.decimalValue, ibmDigitalAnalyticsCategoryID, createAttributeString(attributes)) + '\');';
}

module.exports.generateProductViewTag = function (product : Product, categoryID : String, isSearch : Boolean){
	var category = CatalogMgr.getCategory(categoryID),
        salesChannel = 'FPC';
    
	if (empty(product) || (empty(category) && !isSearch)) return;
	
	var productID = product.ID,
        ibmDigitalAnalyticsCategoryID = isSearch ? searchIbmCategoryID : category.custom.ibmDigitalAnalyticsCategoryID;
	//we only need to distingiush between RA or FPC, because the other sections don't have product view tags for the moment
    if (!empty(product.custom.raAuctionCode)) {
    	salesChannel = 'RA';
        productID = product.custom.raMainProductCode;
    }
	var attributes = [salesChannel, product.custom.articleType, product.custom.mainStoneName, product.brand, product.custom.metalName]; 
    return wrapExtern('cmCreateProductviewTag(\'' + createArgumentsString(productID, product.name, ibmDigitalAnalyticsCategoryID, createAttributeString(attributes)) + '\');');
}

//takes the following attributes Air Date and Time , Article Type , Brand , Mainstone, Metal, Price, Total Gem Weight, Sort by, Item Per Page
module.exports.generatePageViewTag = function (type, arguments, attributes){
	var pageID,
        categoryID,
        searchTerm,
        searchResultCount;
    
    switch(type) {
        case 'category':
        case 'content':  
            pageID = arguments.custom.ibmDigitalAnalyticsPageID;
            categoryID = arguments.custom.ibmDigitalAnalyticsCategoryID;
            break;
        case 'mapping':
            importScript("app_tjc:util/libUtils.ds"); 
            var pageViewMapping = JSON.parse(dw.system.Site.getCurrent().getCustomPreferenceValue('ibmDigitalAnalyticsPageViewMapping')),
                ibmIDs = pageViewMapping[arguments] || {};
            pageID = ibmIDs.pageID;
            categoryID = ibmIDs.categoryID;
            break;
        case 'product':
            var category = CatalogMgr.getCategory(arguments.categoryID),
                isSearch = arguments.isSearch;
            if(empty(category) && !isSearch) break;
            pageID = "PRODUCT: " + arguments.productName;
            categoryID = isSearch ? searchIbmCategoryID : category.custom.ibmDigitalAnalyticsCategoryID;
            break;
        case 'search':
            var searchResultCount = arguments.count,
                page = arguments.currentPage + 1 || 1,
                searchTerm = arguments.keyword;
            if (searchResultCount > 0) {
                pageID = "Search Successful: Page " + page;
                categoryID = searchIbmCategoryID;
            } else {
                pageID = "Search Unsuccessful";
                categoryID = "SEARCH";
            }
            break;
    }
    
    if (empty(pageID) || empty(categoryID)) return;
    
    return wrapExtern('cmCreatePageviewTag(\'' + createArgumentsString(pageID, categoryID, searchTerm, searchResultCount, createAttributeString(attributes)) + '\');');
}

module.exports.getAttributesFromRefinements = function (productSearchResult : ProductSearchModel, pagingModel : PagingModel, permanentAttributeRefinements : Array, permanentRefinedByPrice : Boolean) {
    var refinements : ProductSearchRefinements = productSearchResult.refinements,
        refinementDefintions : Iterator = refinements.refinementDefinitions.iterator(),
        pageViewAttributes : Array = new Array(9),
        attributeSortingOrder : Array = ['airAndDate', 'articleType', 'brand', 'mainStoneName', 'materialName', 'price', 'tgwt', 'sortBy', 'pageSize'];
    
    while (refinementDefintions.hasNext()) {
        var refinementDefinition : SearchRefinementDefinition = refinementDefintions.next(),
            refinementValues : Iterator = refinements.getAllRefinementValues(refinementDefinition).iterator();
        while (refinementValues.hasNext()) {
            var refinementValue : ProductSearchRefinementValue = refinementValues.next();
            if (productSearchResult.isRefinedByAttributeValue(refinementDefinition.attributeID, refinementValue.value) && (empty(permanentAttributeRefinements) || !permanentAttributeRefinements.contains(refinementDefinition.attributeID))) {
            	var index = attributeSortingOrder.indexOf(refinementDefinition.attributeID);
            	if (empty(pageViewAttributes[index])) {
                    pageViewAttributes[index] = refinementValue.value;
            	} else {
                	pageViewAttributes[index] += "|" + refinementValue.value;
                }
            }
            if (productSearchResult.isRefinedByPriceRange(refinementValue.valueFrom, refinementValue.valueTo) && !permanentRefinedByPrice) {
                pageViewAttributes[attributeSortingOrder.indexOf('price')] = refinementValue.valueFrom + '-' + refinementValue.valueTo;
            }
        }
    }
    
    if (!empty(productSearchResult.sortingRule)) {
        pageViewAttributes[attributeSortingOrder.indexOf('sortBy')] = productSearchResult.sortingRule.ID;
    }
    if (!empty(pagingModel)) {
        pageViewAttributes[attributeSortingOrder.indexOf('pageSize')] = pagingModel.pageSize;
    }
    return pageViewAttributes;
}