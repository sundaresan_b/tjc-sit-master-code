/**
 *
 * This script adds the BudgetPay attributes to the Lineitems OCAPI response
 *
 */

var Logger = require("dw/system").Logger;
var BasketMgr = require("dw/order").BasketMgr; 
var HookMgr = 	require("dw/system").HookMgr;
importScript("cart/BudgetPayUtils.ds");

exports.transformProduct = function (product, facebookProduct) {
	
    
	var productCategory;
	var masterProduct;
	if(!empty(product) && product.master) {
		var salesPrice = getPrice(product);
		if(!empty(salesPrice)) {
			facebookProduct.setPrice(salesPrice);
		}
	}
	
    if(!empty(product.longDescription)) {
    	facebookProduct.setDescription(product.longDescription);    	
    }
    else {
    	
    	facebookProduct.setDescription(product.name);
    }
    
    masterProduct = product.variant ? product.variationModel.master : product;
    
    productCategory = masterProduct.primaryCategory ? masterProduct.primaryCategory : (!empty(masterProduct.categories) ? masterProduct.categories[0] : null);
    if(!empty(productCategory)){    	
    	facebookProduct.setGoogleProductCategory(productCategory.custom.googleProductCategory); 
    }
    
};
/**
 * FUNCTION: getPrice
 *
 * Gets the price of a product.
 */
function getPrice(product){
	var priceProduct = product,
		priceModel,
		standardPrice,
		result = "";

	if (product.master && product.variationModel.defaultVariant) {
		priceProduct = product.variationModel.defaultVariant;
	} 
	else if (product.master) {
		if (product.variationModel.variants.length > 0) {
			priceProduct = product.variationModel.variants[1];
		}
		else { 
			return "";
		}
	}
	else if (product.productSet) {
		return "";
	}

	priceModel = priceProduct.priceModel;
	standardPrice = priceModel.getPrice();
	if (('listPricebookId' in Site.current.preferences.custom) && !empty(Site.current.preferences.custom.listPricebookId)) {
		standardPrice = priceModel.getPriceBookPrice(Site.current.preferences.custom.listPricebookId);
	}

	if (standardPrice.available && standardPrice.compareTo(priceModel.minPrice) == 1) {
		result = standardPrice + Site.getCurrent().defaultCurrency;
	}
	else {
		result = product.priceModel.minPrice + " " + Site.getCurrent().defaultCurrency;
	}

	return result;
}