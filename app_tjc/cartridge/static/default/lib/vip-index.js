'use strict';

var player;
var videoWithControls = document.getElementById("player0");
var videoElement = videoWithControls.getElementsByTagName('video')[0];
var playerContainer = document.getElementsByClassName('veygo-player-window')[0];
var stream = {};
if (videoWithControls.classList.contains('pdp-player-container')) {
	stream = {
			type: 'hls',
			uri: playerContainer.getAttribute('data-hls-uri')
	};
} else {	
	
	//This is for the rest of the devices.
	stream = {
			type: 'dash',
			uri: playerContainer.getAttribute('data-dash-uri')
	};
	
	var mac = /(Mac|iPhone|iPod|iPad)/i.test(navigator.platform);
	
	/*
	 * This is for IOS devices. Otherwise it doesn't work properly.
	 */
	if (mac) {
		stream = {
				type: 'hls',
				uri: playerContainer.getAttribute('data-hls-uri')
		};
	}
}

function init () {
	vip.setup({
		app_key: vipConfig.CONFIG_VIP_ANALYTICS_API_KEY,
		app: {
			name: 'VeygoWebSimpleDemoPlayer',
			version: vip.CONFIG_VIP_BUILD_ID,
		},
	}, 'default', // or an hash of your application userId
	videoElement).then(function(result) {
		player = result.player;

        //Uncomment if controls are needed instead of native video
        if (videoElement.classList.contains('veygo-controls-enabled')) {
            VeygoControls.init(videoWithControls, player);
        }
        player.addEventListener('status', onPlayerStatus, false);

        // starting the player
        player.start();
	}).catch(function(err) {
		console.log('error on vip.setup', err);
	});
}

function onPlayerStatus(info)
{
	console.log("vip-player status " + info.status);
	if (info.status === "initialized") {
		player.setup().then(function() {
			var options = {};
			if(!(videoWithControls.classList.contains('pdp-player-container'))){
				options = { autoplay: true };			
			} else {
				options = { autoplay: false }; 
			}
			
			player.setSource(stream, options); 
		}).catch(function(err) {
			console.log('error on vip.Player.setup', err);
		});;
	} else if (info.status === "ready") {
		if(!(videoWithControls.classList.contains('pdp-player-container'))){
			player.play().catch(function(ex) {
				player.setVolume({muted: true});
				player.play();
			}.bind(this));
		}
	}
}

init();

var VeygoIndex = {
		init: function () {
			init();
		}
};
