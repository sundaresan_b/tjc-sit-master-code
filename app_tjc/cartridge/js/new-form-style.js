'use strict';


$(document).ready(function(){
jQuery.extend(jQuery.validator.messages, {
  required: 'Cannot be blank'
});
});



function formstylestyle() {

//check length of input on focus out the field
$('.field input').on('blur', function() {
  $(".focus-helper").hide();
  $(this).siblings(".label").removeClass('focus-active');
      if( $(this).val().length > 0 ) {
        $(this).siblings(".label").addClass('focus-active');
        
    } 

});


//Lable appear on top on focus
$('.field input').on('focus', function() {
   $(this).siblings(".focus-helper").show();
   $(".login-box-content .error").hide();   
   $(this).siblings(".label").addClass('focus-active'); 
});

// Hide error msg on click on focus on login and registration
$(".checkemail-button .button").click(function(){ 
 $('.checkemail-field .error-msg').css('display','none');
});

$(".new-registration").click(function(){ 
 $('.error-msg').css('display','none');
});

$(".input-password").on('focus', function() {
  $(this).closest('.field').find('.error-msg').css('display','none');
});

//Email helper text 
$('.new-form-style input[type=password]').on('focus', function() {
     $(this).siblings(".focus-helper").text("8 to 12 characters, mix of letters and numbers");
});


// Hide the error msg on focus
$('.post-code-field input').on('focus', function() {
    $('.post-code-field  .error-msg').hide();

});



//Password Focus validation

$('.input-password').on('blur', function() {        
          var passwordval = $(this).val()
       if (RegExp("^[a-zA-Z0-9]*$").test(passwordval)) {         
         $(this).closest(".form-row").addClass('validation-success'); 
          $(this).closest(".form-row").removeClass('validation-error'); 
    
     if (/\d/.test(passwordval) && /[a-zA-Z]/.test(passwordval)) {
            $(this).closest(".form-row").addClass('validation-success'); 
            $(this).closest(".form-row").removeClass('validation-error'); 
         }
         
         else{
            $(this).closest(".form-row").addClass('validation-error'); 
            $(this).closest(".form-row").removeClass('validation-success');
             $(this).siblings(".focus-helper").show();
             $(this).siblings(".focus-helper").text("8 to 12 characters, mix of letters and numbers");
            
           } 
         }
         else{
            $(this).closest(".form-row").addClass('validation-error'); 
            $(this).closest(".form-row").removeClass('validation-success');
            $(this).siblings(".focus-helper").show();
            $(this).siblings(".focus-helper").text("8 to 12 characters, mix of letters and numbers"); 
         } 
         
         
       if( $(this).val().length == 0 ) {
         $(this).siblings(".focus-helper").hide();
       } 
       
       if( $(this).val().length > 0 ) {
         $(this).closest(".checkpw-field").removeClass('validation-error'); 
         $(this).closest(".checkpw-field").addClass('validation-success');
      } 
     
});


//Edit email remove validations
$(".emailEditButton").click(function(){ 
$('.email-field').removeClass('validation-error'); 
$('.email-field').addClass('validation-success'); 
});


//Password Validation for TV Customers
 $('.tvcustomerpassword .input-password').on('blur', function() {
        if (/^\d+$/.test($(this).val())) {
        if( $(this).val().length == 4 )  {
         $(this).closest(".form-row").removeClass('validation-error'); 
         $(this).closest(".form-row").addClass('validation-success');
          } 
      } 
})


//Type my address btn click -> focused function for inside the text box
$(".addresses-select-group .showmore").click(function(){ 
 $(this).closest('.addresses-select').find('.styledSelect').css('pointer-events', 'none');
 
  $(".additional-address-fields .input-text").each(function () {  
  if( $(this).val().length > 0 ) {
        $(this).siblings(".label").addClass('focus-active');
        $(this).closest('.form-row').addClass('validation-success');
    } 
                    
 })  
  
});


//trigger the optinal select box field have a value

if($('.birthdate-fields select.input-select').val()) {
        $('.birthdate-fields select.input-select').trigger('blur');
    }


if($('.genderselect select.input-select').val()) {
        $('.genderselect select.input-select').trigger('blur');
    }

$(".country").trigger('blur');

//Select box close for OPC
  $('.opcdelivery-addresses').click(function () {
$('.styledSelect').removeClass('active');
$('.optiongroup').hide();
$('body').removeClass('selectopen');
 $('.overlaybg').remove();
 $('.select-close').remove();  
 
});
 
 //Select box close outer click
 $('.ui-widget-overlay, .AddnewNameFormModal').click(function () {
$('.styledSelect').removeClass('active');
$('.optiongroup').hide();
$('body').removeClass('selectopen');
 $('.overlaybg').remove();
 $('.select-close').remove();  
 
});
 
//Click type address button 
 $('.billingform .type-address-btn').off('click').on('click', function(){
	$('.additional-address-fields input[id^="dwfrm_address"]').each(function(){
		validateAdditionalAddressFiedinPopup(this);
	});
});
 
 	
$('.type-address-btn-new .button-new').click(function () {
$('.addresses-select-group').css('display' , 'block');
$('.additional-address-fields').removeClass('visually-hidden');
$('.additional-address-fields').addClass('visually-un-hidden');
$('.addOPCdeliverylocation').removeClass('visually-hidden');

 $('.type-address-btn-new').css('display' , 'none');
 $('.addresses-select-group').css('display' , 'none');
 $('.address-check-btn').css('display' , 'none');
 $('.post-code-field').addClass('full-width-postcode'); 
    optionalRemove();  
	$('.additional-address-fields input[id^="dwfrm_address"]').each(function(){
		validateAdditionalAddressFiedinPopup(this);
	});
	   if($('#opcDeliveryAddresses').length > 0){
		 $('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').each(function(){
			 validateAdditionalAddressFiedinPopup(this);  
		 });
    }
    
    	$('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').on('keyup keydown change', function(){
        			validateAdditionalAddressFiedinPopup(this);
        		
        		});	

             });



  function optionalRemove() { 
   $(".address-content .optinal-field input").each(function () {
      
    if( $(this).val().length > 0 ) {

	$(this).closest('.optinal-field').addClass('remove-optional');
	 }	
	else{
	  $(this).closest('.optinal-field').removeClass('remove-optional');
	   }
     });
   }

 
 $('.address-check-btn').click(function () {
$('.type-address-btn-new').hide();
});
 
$('.enable-find-my-address a').click(function () {
	var $form = $(this).closest('.xlt-addressForm'),
	$addressSelect = $form.find('.addresses-select');
	$addressSelect.removeClass("visually-hidden");
	$form.find('.additional-address-fields input').val('');
	$form.find('.selected-address-summary').addClass('visually-hidden');
	$form.find('.addresses-select-group h5').addClass('visually-hidden');
	$form.find('.selected-address-summary span').empty();
	$addressSelect.removeClass('loqate-select');
   $('.address-check-btn').css('display' , 'block');
   $('.additional-address-fields').addClass('visually-hidden');
   $('.additional-address-fields').removeClass('visually-un-hidden');
   $('.type-address-btn-new').show();
   $('.post-code-field').removeClass('full-width-postcode');
   $('.type-own-address').remove();
   $('.guestusershipping .additional-address-fields').removeClass('skip-hide');
}); 
 
  
$('body').on('click','.type-address-btn', function ()  {
 $(".additional-address-fields .input-text").each(function () {  
  if( $(this).val().length > 0 ) {
        $(this).siblings(".label").addClass('focus-active');
        $(this).closest('.form-row').addClass('validation-success');
    } 
                    
    })    
    
    if($('#opcDeliveryAddresses').length > 0){
		 $('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').each(function(){
			 validateAdditionalAddressFiedinPopup(this);  
		 });
    }
    
     /* $('.address-check-btn').css('display' , 'none');
     $('.post-code-field').addClass('full-width-postcode'); 
     $('.addresses-select-group').css('display' , 'none'); */
     optionalRemove();
    
  });


function validateAdditionalAddressFiedinPopup($this){
	var countvalue = $($this).attr('maxlength') - $($this).val().length;
	$($this).closest('.address-content').find('.addresscharacterlength .count').html(countvalue);
    if (countvalue < 1 ) {
    	$($this).closest('.address-content').find('.addresscharacterlength').addClass('alert-danger');
    }
    else {
    	$($this).closest('.address-content').find('.addresscharacterlength').removeClass('alert-danger');
    }
}




//Tooptip position
$(".QB-address-heading i").hover(function() {
    $('.ui-tooltip').addClass('Toltip-top');
  });



$('.optinal-field input').on('keyup keydown change', function(){
	
 if( $(this).val().length >= 1 ) {

	$(this).closest('.optinal-field').addClass('remove-optional');
	 }	
	else{
	  $(this).closest('.optinal-field').removeClass('remove-optional');

	  }

 });



 //Shipping Address section

$(".shippingform .address-check-btn .button").on("click", function(){

 if( $('.shippingform .address-alias').hasClass('validation-success')) {
  $('.shippingform .addresses-select-group').css('display', 'none');
 }
    
setTimeout(function(){ 	
    if( $('.shippingform .address-alias').hasClass('validation-error') ) {
     } 
   else{
   $('.shippingform  .addresses-select .form-row ').removeClass('validation-success');
     $('.shippingform  .addresses-select .form-row ').removeClass('validation-error');
     $('.shippingform  .addresses-select .styledSelect').remove();
     $('.shippingform  .addresses-select .optiongroup').remove();		 
     addressselectBoxCustom(); 
    
    if ($('.shippingform .addresses-select li').length == 1) {
       if ($('.shippingform .additional-address-fields').hasClass('visually-hidden')) {
             $(".shippingform .address-check-btn .button").trigger('click');
         }
     }
     
     $('.shippingform .addresses-select-group').css('display', 'block');
   }

  },900);

 });
 
 
 
 //Billing Address section
 
$(".billingform .address-check-btn .button").on("click", function(){

 
 if( $('.billingform .address-alias').hasClass('validation-success')) {
$('.billingform .addresses-select-group').css('display', 'none');
}

setTimeout(function(){ 	
    if( $('.billingform .address-alias').hasClass('validation-error') ) {
    } 
   else{
     $('.billingform  .addresses-select .form-row ').removeClass('validation-success');
     $('.billingform  .addresses-select .form-row ').removeClass('validation-error');
    $('.billingform .addresses-select-group').css('display', 'block');
     $('.billingform  .addresses-select .styledSelect').remove();
     $('.billingform  .addresses-select .optiongroup').remove();			 
     
     billingformselectBox();  
     
    if ($('.billingform .addresses-select li').length == 1) {
       if ($('.billingform .additional-address-fields').hasClass('visually-hidden')) {
             
             $(".billingform .address-check-btn .button").trigger('click');
         }
     }
     
     if( $('.optiongroup > .optiongroup')) {   
      $('.optiongroup').closest('.optiongroup').find('.optiongroup').removeClass('optiongroup');

      }

    }

  },900);

 });
 
 
 
  //New User Billing Address section
 
$(".opcnewusershipping .address-check-btn .button").on("click", function(){ 

setTimeout(function(){ 	
     $('.opcnewusershipping  .addresses-select .form-row ').removeClass('validation-success');
     $('.opcnewusershipping  .addresses-select .form-row ').removeClass('validation-error');
    $('.opcnewusershipping   .addresses-select-group').css('display', 'block');
     $('.opcnewusershipping  .addresses-select .styledSelect').remove();
     $('.opcnewusershipping  .addresses-select .optiongroup').remove();	 
      addressselectBoxCustom();  
        
    if ($('.opcnewusershipping .addresses-select li').length == 1) {
              
       if ($('.opcnewusershipping .additional-address-fields').hasClass('visually-hidden')) {
             
             $(".opcnewusershipping .address-check-btn .button").trigger('click');
             
         }
     }
     
  },900);

    });
 
 
 
 //Guestuser Billing Address section 
 
$(".guestuserbilling .address-check-btn .button").on("click", function(){
 
 if( $('.guestuserbilling .address-alias').hasClass('validation-success')) {
$('.guestuser-checkout .addresses-select-group').css('display', 'none');
}

setTimeout(function(){ 	
     $('.guestuserbilling  .addresses-select .form-row ').removeClass('validation-success');
     $('.guestuserbilling  .addresses-select .form-row ').removeClass('validation-error');
    $('.guestuserbillingt .addresses-select-group').css('display', 'block');
     $('.guestuserbilling  .addresses-select .styledSelect').remove();
     $('.guestuserbilling  .addresses-select .optiongroup').remove();	 
      billingformselectBox(); 
        
    if ($('.guestuserbilling .addresses-select li').length == 1) {
              
       if ($('.guestuserbilling .additional-address-fields').hasClass('visually-hidden')) {
             
             $(".guestuserbilling .address-check-btn .button").trigger('click');
             
         }
     }
    
  },900);

    });



 //Guestuser Shipping Address section

$(".guestusershipping .address-check-btn .button").on("click", function(){

 if( $('.guestusershipping .address-alias').hasClass('validation-success')) {
$('.guestusershipping .addresses-select-group').css('display', 'none');
}

setTimeout(function(){ 	
     $('.guestusershipping  .addresses-select .form-row ').removeClass('validation-success');
     $('.guestusershipping  .addresses-select .form-row ').removeClass('validation-error');
    $('.guestusershipping .addresses-select-group').css('display', 'block');
     $('.guestusershipping  .addresses-select .styledSelect').remove();
     $('.guestusershipping  .addresses-select .optiongroup').remove();	 
      addressselectBoxCustom(); 
    
    if ($('.guestusershipping .addresses-select li').length == 1) {
       if ($('.guestusershipping .additional-address-fields').hasClass('visually-hidden')) {
             
             $(".guestusershipping .address-check-btn .button").trigger('click');

         }
     }
    
    
  },900);

    });


 
  }



//Selectbox new design

function selectBoxCustom() {
  $('select:not(.non-custom-select)').each(function () {
    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;
    
     var a = $(this).find('option:selected').text();

    // Hides the select element
    $this.addClass('s-hidden');

    // Wrap the select element in a div
   $this.wrap('<div class="select"></div>');


    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect"></div>');
    

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
       $styledSelect.text($(this).find('option:selected').text());
    
  
    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);

     
    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
    $styledSelect.click(function (e) {
        e.stopPropagation();            
        $('body').addClass('selectopen');
        $this.after('<div class="overlaybg"></div>'); 
        $('.options li:first-child').before('<div class="select-close"></div>');       
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('.optiongroup').hide(); 
        });
          $(this).toggleClass('active').next('.optiongroup').toggle();        
          
    });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
    $listItems.click(function (e) {
        e.stopPropagation();
          $('body').removeClass('selectopen');
          $('.overlaybg').remove();
          $('.select-close').remove();
         $styledSelect.text($(this).text()).removeClass('active');
         $this.val($(this).attr('rel'));
          $list.hide();
          $('.optiongroup').hide();
         $(this).closest('.select').find('select').trigger("change");

         
    });

    // Hides the unordered list when clicking outside of it
    $('#wrapper').click(function () {
        $('.styledSelect').removeClass('active');
        $list.hide();
         $('.optiongroup').hide();
        $('body').removeClass('selectopen');
         $('.overlaybg').remove();
         $('.select-close').remove();
    
       
    });
    
    $('#qb-activation-page').click(function () {
    $('.styledSelect').removeClass('active');
    $list.hide();
    $('.optiongroup').hide();
    $('body').removeClass('selectopen');
     $('.overlaybg').remove();
     $('.select-close').remove();  
     
    });
    

    $(".styledSelect").on("click", function(){
      $(this).siblings('select').trigger('blur');
   });

  $(".options li").on("click", function(){
   $(this).parents().siblings('select').trigger('blur');
  }); 
 
 });
 
$('ul.options').wrap('<div class="optiongroup"></div>');
 
  // Restrict more select append
   if( $('.select > .select')) {     
     $('.select').closest('.select').siblings('.styledSelect').remove();
      $('.select').closest('.select').siblings('.optiongroup').remove();
      
    }

// Auto fucts when text box having a value 
$(".input-text").each(function () {  
  if( $(this).val().length > 0 ) {
        $(this).siblings(".label").addClass('focus-active');
        $(this).closest('.form-row').addClass('validation-success');
    } 
                    
 })  


// Remove overlapping with new style select in Missed actions page
 $('.missed-auctions-main .select').removeClass('select');
 $('.missed-auctions-main .styledSelect').remove();
 $('.missed-auctions-main .optiongroup').remove();
    
}



// Select box new design for shipping address selectbox

function addressselectBoxCustom()  {

 $('.shippingform .addresses-select select').each(function () {
    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;
     var a = $(this).find('option:selected').text();
    // Hides the select element
    $this.addClass('s-hidden');
    
    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
       $styledSelect.text($(this).find('option:selected').text());
    
  
    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);
     
    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    $styledSelect.click(function (e) {
        e.stopPropagation();
            
        $('body').addClass('selectopen');
        $this.after('<div class="overlaybg"></div>'); 
        $('.options li:first-child').before('<div class="select-close"></div>');       
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('.optiongroup').hide(); 
        });
          $(this).toggleClass('active').next('.optiongroup').toggle();
          
    });

    $listItems.click(function (e) {
        e.stopPropagation();
          $('body').removeClass('selectopen');
          $('.overlaybg').remove();
          $('.select-close').remove();
          $styledSelect.text($(this).text()).removeClass('active');
          $this.val($(this).attr('rel'));
          $list.hide();
          $('.optiongroup').hide();
         $(this).closest('.select').find('select').trigger("change");
        
    });
    
    
  $(".options li").on("click", function(){
   $(this).parents().siblings('select').trigger('blur');
  }); 
  
      $(".styledSelect").on("click", function(){
      $(this).siblings('select').trigger('blur');

   });
  
 
 $('.shippingform .addresses-select ul.options').wrap('<div class="optiongroup"></div>');
   
  });
 
 }




// Select box new design for billing address selectbox

function billingformselectBox()  {

 $('.billingform .addresses-select select').each(function () {
    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;
     var a = $(this).find('option:selected').text();
    // Hides the select element
    $this.addClass('s-hidden');
    
    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
       $styledSelect.text($(this).find('option:selected').text());
    
  
    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);
     
    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    $styledSelect.click(function (e) {
        e.stopPropagation();
            
        $('body').addClass('selectopen');
        $this.after('<div class="overlaybg"></div>'); 
        $('.options li:first-child').before('<div class="select-close"></div>');       
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('.optiongroup').hide(); 
        });
          $(this).toggleClass('active').next('.optiongroup').toggle();
          
    });

    $listItems.click(function (e) {
        e.stopPropagation();
          $('body').removeClass('selectopen');
          $('.overlaybg').remove();
          $('.select-close').remove();
          $styledSelect.text($(this).text()).removeClass('active');
          $this.val($(this).attr('rel'));
          $list.hide();
          $('.optiongroup').hide();
         $(this).closest('.select').find('select').trigger("change");
        
    });
    
    
  $(".options li").on("click", function(){
   $(this).parents().siblings('select').trigger('blur');
  }); 
  
   $(".styledSelect").on("click", function(){
    $(this).siblings('select').trigger('blur');
 
   });
 
 $('.billingform .addresses-select ul.options').wrap('<div class="optiongroup"></div>');
   
  });
 
 }



exports.init = function () {
    formstylestyle();
   selectBoxCustom();
};
