'use strict';

var dialog = require('./dialog'),
			 util = require('./util'),
			 ajax = require('./ajax'),
			 validator = require('./validator'),
			 page = require('./page'),
			 progress = require('./progress'),
			 flyinheader = require('./flyinheader'),
			 addressjs = require('./pages/checkout/address'),
			 accountjs = require('./pages/account'),
			 newformstyle = require('./new-form-style'),
			 orderconfirmation = require('./pages/orderconfirmation');

function ViewHide(pre, show){
	var view,hide;
    var arr = ["title", "content", "field", "button"];

    $.each( arr, function( index, value ){
    	hide = "."+pre+"-"+value;
    	$(hide).addClass("d-none");
        hide = "";
    });
    
    $.each( arr, function( index, value ){
    	view = "."+show+"-"+value;
        $(view).removeClass("d-none");
        view = "";
    });
}

function enableQuickBuy() {
    $('.quickbuypref-check').addClass('enabled');
    $('.change-mode').html('enabled');
    $('.quickbuy-detail').css('opacity', '1');
    $(".quickbuy-detail :input[type=text], .quickbuy-detail :input[type=submit], .quickbuy-detail :input[type=checkbox], .quickbuy-detail select").prop("disabled", false);
    $('#qbpref-enablecontent').removeClass('d-none');
    $('#isquikbuydisabled').addClass('d-none');
    $('.add-new-address').css('pointer-events','auto');
	$('.add-new-address').css('cursor','pointer');
	$(".quickbuy-detail  .styledSelect").css('pointer-events', 'auto');
    $(".quickbuy-detail  .styledSelect").css('cursor', 'pointer');
}

function disableQuickBuy() {
    $('.quickbuypref-check').removeClass('enabled');
    $('#qbpref-disablecontent').addClass('d-none');
    $('#qbpref-enablecontent').addClass('d-none');
    $('#isquikbuydisabled').removeClass('d-none');
    $('.change-mode').html('disabled');
    $('.add-new-address').css('pointer-events','none');
    $('.add-new-address').css('cursor','default');
    $('.quickbuy-detail').css('opacity', '0.5');
    $('.quickbuytac').css('display', 'none');
    $(".quickbuy-detail :input[type=text], .quickbuy-detail :input[type=submit], .quickbuy-detail :input[type=checkbox], .quickbuy-detail select").prop("disabled", true);
    $(".quickbuy-detail  .styledSelect").css('pointer-events', 'none');
    $(".quickbuy-detail  .styledSelect").css('cursor', 'auto');
}

var pids= '',itemPnP='',installments='',auctioncode='',channel='',quantity='',variationValue='',secondVariation='',secondvariationValue='';

var auctionCodeApplicable = $("input[name=auctionCodeApplicable]").val(),
	discountAvailable = $("input[name=discountAvailable]").val(),
	buyAllDiscount = $("input[name=buyAllDiscount]").val(),
	discountType = $("input[name=discountType]").val();

function initializeEvents() {    	
	quickBuyAction();
    showActivateQBModel();
    flyinheader.init();
    showActivateModalPopup();
    checkFieldsIntiator();
    checkFieldsType();
    whatiscvv();
    
    //$("#dwfrm_quickbuyaddress_shippingAddress_address_address2").attr("required", true);
    //$("#dwfrm_quickbuyaddress_billingAddress_address_address2").attr("required", true);
    
    if(!$('#use-billing-address').is(':checked')) {
    	//$('.customerAddressBilling').find('.add-billing-address').removeClass('hidden');
    	$('.customerAddressBilling').find('.saved-address').removeClass('hidden');
	}
            
    $("#dwfrm_quickbuyaddress_shippingAddress_addressid").focus(function(){
        $('#dwfrm_quickbuyaddress_shippingAddress_addressid-error').remove();
        $('#dwfrm_quickbuyaddress_shippingAddress_addressid').css('border-color','#b9addb');
        $('#dwfrm_quickbuyaddress_shippingAddress_addressid').css('color','#2c163d');
    });
    
    if($('.quickbuypref-check input').is(":checked") || ($('#isQbActivated').val() != "true") && ($('#isQbEnabled').val() != "true")) {
    	enableQuickBuy();
    }
    else{
        disableQuickBuy();
    }
    
	$('body').off('click').on('click', '.live-tv-add-to-cart-form a.quick-buy-login-class' , function (e) {
	    e.preventDefault();
	    var url = Urls.quickbuysignpage;
	    pids = $(this).data('pid');
	    auctioncode = $(this).data('auctioncode');
	    installments = $(this).data('installments');
	    channel = $(this).data('channel');
	    itemPnP = $(this).data('itempnp');
	    auctionCodeApplicable = $("input[name='auctionCodeApplicable']").val();
	    quantity= $('#live-tv-product-quantity').val();
	    if ($('#live-tv-product-options').hasClass('border')){
			$('#live-tv-product-options').removeClass('border');
		}
	    
	    if($('#live-tv-detail-main .groupVariations').length > 0){
	    	if ($('#live-tv-product-options').length > 0 && $('#live-tv-product-options').val() != null){
		    	variationValue = $('#live-tv-product-options').val();
	    	}
	    	else{
	    		variationValue = $('.auction-product-data .product-id').data('value');
	    	}
	    }
	    
	    if($('#live-tv-detail-main .groupVariations').length > 0){
	    	if ($('#live-tv-product-options').length > 0 && ($('#live-tv-product-options').val() == "null" || $('#live-tv-product-options').val() =='' )) {
		    	$('#live-tv-product-options').addClass('border')
		    	return false;
		    }
		    else{
		    	$('#live-tv-product-options').removeClass('border')
		    }
	    }
	    
	    
	    if($('.live-tv-detail-main .MLAVariations').length > 0){
	    	secondVariation = $('.second-variation .input-select').val();
	    	
	    	if ($('#live-tv-product-options').length > 0 && $('#live-tv-product-options').val() != null){
		    	variationValue = $('#live-tv-product-options').val();
	    	}
	    	else{
	    		variationValue = $('.auction-product-data .product-id').data('value');
	    	}
	    	
	    	var firstVariation = $('.product-selection .input-select').val();
	    	if (firstVariation == null || firstVariation == "null") {
	    		$('#live-tv-product-options').addClass('border');
	    		return false;
	    	}
	    	else{
	    		$('#live-tv-product-options').removeClass('border');
	    	}
	    	
	    	if (secondVariation == null || secondVariation == "null") {
	    		$('#select-second-variation').addClass('border');
	    		return false;
	    	}
	    	else{
	    		$('#select-second-variation').removeClass('border');
	    	}
	    	    	
	    }		    
	    
	    /*var $productContainer = $('#live-tv-detail-main'),
		$tile = $productContainer.find('.auction-product-data'),
        $selectField = $productContainer.find('.product-selection .input-select'),
        $selectedOption = $selectField.find('option:selected'),
        pids = $selectField.length !== 0 ? $selectField.val() : $tile.find('.product-id').data('value'),
        auctionCodes = $selectField.length !== 0 ? $selectedOption.data('auctioncode') :
                $tile.find('.tile-inner').data('auctioncode');*/
	    var $productContainer = $('#live-tv-detail-main'),
        $tile = $productContainer.find('.auction-product-data'),
        $mlaVariations = $productContainer.find('.MLAVariations'),
    	$groupVariations = $productContainer.find('.groupVariations'),
        $selectField = $productContainer.find('.product-selection .input-select'),
        $selectedOption = $selectField.find('option:selected'),
        $selectedFirstOptionValue = $selectedOption.val(),
        $selectedFirstAuctionCode = $selectedOption.attr('data-auctioncode'),
        $selectedfirstPnP = $selectedOption.attr('data-pnp'),
        $selectsecondField = $productContainer.find('.second-variation .input-select'),
        $selectedSecondOption = $selectsecondField.find('option[selected=selected]'),
		$selectedSecondOptionValue = $selectedSecondOption.val(),
		$selectedSecondAuctionCode = $selectedSecondOption.attr('data-auctioncode'),
    	$selectedSecondPnP = $selectedSecondOption.attr('data-pnp');
		
		if($mlaVariations.length > 0){
    		var stockCode = $selectedSecondOptionValue,
    			auctionCode = $selectedSecondAuctionCode,
    			pnPValue = $selectedSecondPnP;
    		}
		
		if($groupVariations.length > 0){
    		var stockCode = $selectField.length !== 0 ? $selectField.val() : $tile.find('.product-id').data('value'),
    			auctionCode = $selectField.length !== 0 ? $selectedOption.data('auctioncode') : $tile.find('.tile-inner').data('auctioncode'),
    			pnPValue = $selectedSecondPnP;
    		}
        
        var pids = stockCode,
        auctionCodes = auctionCode,
        quantity = $productContainer.find('.quantity-selection .input-select').val();
        
        var secondVarient = $('.second-variation .input-select option:selected').val();
        sessionStorage.setItem('secondField', secondVarient);              
                
        if ( auctionCodes != ''){
        	auctioncode = auctionCodes;
        }	    
        
        var Login_PopUp_Active = window.SitePreferences.Login_PopUp_Active;
	    if(Login_PopUp_Active){
	        url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    dialog.open({ 
		        url: url,
		        options:{
		        	dialogClass:'new-sign-registration quick-buy-dialog',
		        	open: function(event,ui){
		        		validator.init();
		        		newformstyle.init();	        		
		        		
		        		$('.oAuthIcon').bind('click', function () {
		        	    	$('.OAuthProvider').val(this.id);
		        	    	$('.quickBuyClick').val(true);
		        	    });
		        		
		        		$('body').css('overflow','hidden');
		        		$('.checkemail-button button').off('click').on('click submit', function(event){
		        			event.preventDefault(); 
		        			var url = Urls.checkMailAvailabilty;
		        			var $form = $(this).closest('form');
		        			var emailid = $form.find('.email-field input').val();
		        			if ($form.find('.checkemail-field .error').length > 0){
		        				$form.find('.checkemail-field .error').remove();
		        			}
		        			var regex = validator.regex;
		        			if (emailid != '' && !regex.newEmail.test(emailid)){
		        				var newDiv ='<div class="error" style="color:#c20000;">The email address is invalid.<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			if (emailid == ''){
		        				var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			url = util.appendParamToURL(url,'emailid',emailid);
		        			 $.ajax({
		        			        url: url,
		        			        type: 'POST',
		        			        success: function (response) { 
		        			        	var responseJson = JSON.parse(response);
		        			        	if (responseJson.availableEmail){
		        			        		var $form1 = $('.returning-customers form');
		        			        		if ($form1.find('.email-value').hasClass('d-none')){
		        			        			$form1.find('.email-value').text(responseJson.email).removeClass('d-none');
		        			        		} else {
		        			        			$form1.find('.email-value').text(responseJson.email);
		        			        		}
		        			        		if ($form1.find('.checkpw-content').hasClass('d-none')){
		        			        			$form1.find('.checkpw-content').removeClass('d-none');
		        			        		}
		        			        		if (!$form1.find('.checkemail-field').hasClass('d-none')){
		        			        			$form1.find('.checkemail-field').addClass('d-none');
		        			        		}
		        			        		if ($form1.find('.form-row-password').hasClass('d-none')){
		        			        			$form1.find('.form-row-password').removeClass('d-none');
		        			        		}
		        			        		if (!$form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
		        			        			$form1.find('.form-row-button .checkemail-button').addClass('d-none');
		        			        		}
		        			        		if ($form1.find('.checkpw-button').hasClass('d-none')){
		        			        			$form1.find('.checkpw-button').removeClass('d-none');
		        			        		}
		        			        		if ($('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkpw-title').removeClass('d-none');
		        			        		}
		        			        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        			        		}
		        			        		if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
		        			        		}
		        			        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
		        			        		}
		        			        		
		        			        		$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        			        		qbsignInForCustoer();
		        			        		qbforgotPwdClick();
		        			        		qbeditbuttonclick();
		        			        		qbenteredWrongEmail();
		        			        		qbdisguisePassword();
		        			        	} else {
		        			        		
		        			        		var url = util.appendParamsToUrl(Urls.oldRegPage,{
		        			        			'emailid':emailid,
		        			        			'fromPopUpRegister' : 'yes',
		        			        			'quickbuy':true,
		        			        			'pagecontext':pageContext.ns,
		        			        			'pids':pids,
		        			        			'itemPnP':itemPnP,
		        			        			'installments':installments,
		        			        			'auctioncode':auctioncode,
		        			        			'auctionCodeApplicable':auctionCodeApplicable,
		        			        			'channel':channel,
		        			        			'quantity':quantity,
		        			        			'variationValue':variationValue,
		        			        			'secondVariation':secondVariation,
		        			        			'redirectQuickBuyActivation':true
		        			        			});
		        			        		window.location.href = url;
		        			        	}
		        			        	
		        			        	$(".emailEditButton").on("click", function(){
		        			        		$('.newsignin-box-inner .checkpw-title').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-content').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-field').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-button').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-title').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-content').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-field').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-button').removeClass('d-none');
		        			        	});
		        			        	
		        			        }, 
		        			        error: function (jqXHR, textStatus, errorThrown) {console.log(jqXHR + textStatus + errorThrown); }
		        			    });
		        		});
		        	},
		        	close:function() {
		        		$('body').removeAttr("style");
		        		$('html').removeAttr("style");	
		        	}
		        }
		    });
	    }else{
	    	if(discountAvailable == "true" && $('#live-tv-product-options option:selected').attr("data-isbuyall") == "true"){
	    		var url = util.appendParamsToUrl(Urls.oldRegPage,{
	    			'quickbuy':true,
	    			'pagecontext':pageContext.ns,
	    			'pids':pids,
	    			'itemPnP':itemPnP,
	    			'installments':installments,
	    			'auctioncode':auctioncode,
	    			'auctionCodeApplicable':auctionCodeApplicable,
	    			'channel':channel,
	    			'quantity':quantity,
	    			'variationValue':variationValue,
	    			'secondVariation':secondVariation,
	    			'redirectQuickBuyActivation':true,
	    			'emailid':'',
	    			'fromPopUpRegister' : 'no',
	    			'buyAllDiscount' : buyAllDiscount,
					'discountAvailable' : discountAvailable,
					'discountType' : discountType
    			});
	    	}else{
	    		var url = util.appendParamsToUrl(Urls.oldRegPage,{
	    			'quickbuy':true,
	    			'pagecontext':pageContext.ns,
	    			'pids':pids,
	    			'itemPnP':itemPnP,
	    			'installments':installments,
	    			'auctioncode':auctioncode,
	    			'auctionCodeApplicable':auctionCodeApplicable,
	    			'channel':channel,
	    			'quantity':quantity,
	    			'variationValue':variationValue,
	    			'secondVariation':secondVariation,
	    			'redirectQuickBuyActivation':true,
	    			'emailid':'',
	    			'fromPopUpRegister' : 'no'
    			});
	    	}
    		window.location.href = url;
	    }
	});
	
	$('body').on('change', '#dwfrm_quickbuyaddress_customer_quickbuytac', function() {
        if($('#dwfrm_quickbuyaddress_customer_quickbuytac').is(':checked')){
        	$('#dwfrm_quickbuyaddress_customer_quickbuytac').attr('value', 'true');
          }
          else{
        	  $('#dwfrm_quickbuyaddress_customer_quickbuytac').attr('value', 'false');
          }
    });
	
	/** Enable/ Disable functionality on preference page **/
	$('.quickbuypref-check input').on('change', function(){
		if($('.quickbuypref-check input').is(":checked")) {
			var url = Urls.toggleEnableQuickbuy;
        	$.ajax({
    	        url: url,
    	        type: 'POST',
    	        success: function (response) {  }
        	});
        	enableQuickBuy();
		}
		else {
			$("#QBdisableconfirm").dialog({
		        dialogClass:'QBdisableconfirm',
		        open: function (event, ui) {
		            if ($('#QBdisableconfirm').hasClass('d-none')){
		                $('#QBdisableconfirm').removeClass('d-none');
		            }
		            $('.cancel-disable').on('click', function(){
		                $('#QBdisableconfirm').dialog('close');
		            });
		            $('.confirm-disable').on('click', function(){
		            	var url = Urls.toggleEnableQuickbuy;
		            	$.ajax({
		        	        url: url,
		        	        type: 'POST',
		        	        success: function (response) {  }
		            	});
		            	disableQuickBuy();
		    			$('#QBdisableconfirm').dialog('close');
		            });
		        },
		        modal: true
		    });
		}
	});
}

function redirectQuickBuyPrefPage(){
	var url = Urls.quickbuyprefpage;
	url = util.appendParamsToUrl(url,{
		'pids':pids,
		'itemPnP':itemPnP,
		'installments':installments,
		'auctioncode':auctioncode,
		'auctionCodeApplicable':auctionCodeApplicable,
		'channel':channel,
		'quantity':quantity,
		'pagecontext':pageContext.ns,
		'redirectQuickBuyActivation':true
	});
	window.location.href = url
}

function showActivateQBModel() {
	$('#showActivateQBModel').off('click').on('click', function (e) {
	    e.preventDefault();
	    pids = $(this).data('pids');
	    auctioncode = $(this).data('auctioncode');
	    installments = $(this).data('installments');
	    channel = $(this).data('channel');
	    itemPnP = $(this).data('itempnp');
	    auctionCodeApplicable = $("input[name='auctionCodeApplicable']").val();
	    quantity= $('#live-tv-product-quantity').val();	    	   	    
	    
	    if ($('#live-tv-product-options').hasClass('border')){
			$('#live-tv-product-options').removeClass('border');
		}
	    
	    if($('#live-tv-detail-main .groupVariations').length > 0){
	    	if ($('#live-tv-product-options').length > 0 && $('#live-tv-product-options').val() != null){
		    	variationValue = $('#live-tv-product-options').val();
	    	}
	    	else{
	    		variationValue = $('.auction-product-data .product-id').data('value');
	    	}
	    }	    
	    
	    if(sessionStorage.getItem("secondField")  != null) {
			$('.second-variation .input-select').val(sessionStorage.getItem("secondField")).change();
			sessionStorage.removeItem("secondField");
		}
	    
	    if($('#live-tv-detail-main .groupVariations').length > 0){
	    	if ($('#live-tv-product-options').length > 0 && ($('#live-tv-product-options').val() == "null" || $('#live-tv-product-options').val() =='' )) {
		    	$('#live-tv-product-options').addClass('border')
		    	return false;
		    }
		    else{
		    	$('#live-tv-product-options').removeClass('border')
		    }
	    }
	    
	    
	    if($('.live-tv-detail-main .MLAVariations').length > 0){
	    	secondVariation = $('.second-variation .input-select').val();
	    	
	    	if ($('#live-tv-product-options').length > 0 && ($('#live-tv-product-options').val() == "null" || $('#live-tv-product-options').val() =='' )) {
		    	$('#live-tv-product-options').addClass('border')
		    	return;
		    }
		    else{
		    	$('#live-tv-product-options').removeClass('border')
		    }
	    	
	    	if (secondVariation == null || secondVariation == "null") {
	    		$('#select-second-variation').addClass('border');
	    		return false;
	    	}
	    	else{
	    		$('#select-second-variation').removeClass('border');
	    	}
	    }
		
		var $form = $(this).closest('form'),
		$productContainer = $('#live-tv-detail-main'),
		$mlaVariations = $productContainer.find('.MLAVariations'),
    	$groupVariations = $productContainer.find('.groupVariations'),		
		$tile = $productContainer.find('.auction-product-data'),
        $selectField = $productContainer.find('.product-selection .input-select'),
        $selectedOption = $selectField.find('option:selected'),
        $selectedFirstOptionValue = $selectedOption.val(),
        $selectedFirstAuctionCode = $selectedOption.attr('data-auctioncode'),
        $selectedfirstPnP = $selectedOption.attr('data-pnp'),
        $selectsecondField = $productContainer.find('.second-variation .input-select'),
        $selectedSecondOption = $selectsecondField.find('option[selected=selected]'),
		$selectedSecondOptionValue = secondVariation ? secondVariation : $selectedSecondOption.val(),
		$selectedSecondAuctionCode = $selectedSecondOption.attr('data-auctioncode'),
    	$selectedSecondPnP = $selectedSecondOption.attr('data-pnp'),
    	budgetPayContainer = $productContainer.find('.budgetpay'),
		installments = budgetPayContainer.attr('data-installments');
		
		if ($selectedSecondOption.val() == 'null' || $selectedSecondOption.val() == '' || $selectedSecondOption.val() == null){
	        var options = $selectsecondField.find('option');
	        $.each(options, function(index,value) {
	        	
	            //console.log(r.find(".second-variation .input-select option")[index])
	            if ($(this).val() == secondvariationValue){
	            	$selectedSecondOptionValue = $(this).val();
                    $selectedSecondAuctionCode = $(this).data('auctioncode');
                    $selectedSecondPnP = $(this).data('pnp');
                }
            });
        }
		
		if($mlaVariations.length > 0){
    		var stockCode = $selectedSecondOptionValue,
    			auctionCode = $selectedSecondAuctionCode,
    			pnPValue = $selectedSecondPnP;
    	}
    	
    	if($groupVariations.length > 0){
    		var stockCode = $selectField.length !== 0 ? $selectedFirstOptionValue : $tile.find('.product-id').data('value'),
				auctionCode = $selectField.length !== 0 ? $selectedFirstAuctionCode : $tile.find('.tile-inner').data('auctioncode'),
    			pnPValue = $selectField.length !== 0 ? $selectedfirstPnP : $tile.find('.tile-inner').data('data-pnp');
    	}
    	
    	var pids =	stockCode,
    	auctionCodes = auctionCode,
    	quantity = $productContainer.find('.quantity-selection .input-select').val();
	    
                
        if ( auctionCodes != '') {
        	auctioncode = auctionCodes;
        }	   
        
	    /*if (variationValue != ''){
	    	pids = variationValue;
	    }*/         
	    
	   if ($(this).hasClass('quick-buy-pref-Class')){
		   redirectQuickBuyPrefPage();
		   return;
	   } else {
		    var url = Urls.quickbuyactivatepage;
		    //url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    
		    if(discountAvailable == "true" && $('#live-tv-product-options option:selected').attr("data-isbuyall") == "true"){
		    	url = util.appendParamsToUrl(url,{
					'pids':pids,
					'itemPnP':itemPnP,
					'installments':installments,
					'auctioncode':auctioncode,
					'channel':channel,
					'quantity':quantity,
					'variationValue':variationValue,
					'secondVariation':secondVariation,
					'redirectQuickBuyActivation':true,
					'auctionCodeApplicable' : auctionCodeApplicable,
	    			'buyAllDiscount' : buyAllDiscount,
					'discountAvailable' : discountAvailable,
					'discountType' : discountType
				});
		    }else{
		    	url = util.appendParamsToUrl(url,{
					'pids':pids,
					'itemPnP':itemPnP,
					'auctionCodeApplicable' : auctionCodeApplicable,
					'installments':installments,
					'auctioncode':auctioncode,
					'channel':channel,
					'quantity':quantity,
					'variationValue':variationValue,
					'secondVariation':secondVariation,
					'redirectQuickBuyActivation':true
				});
		    }
		    
		    //url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    progress.show();
		    dialog.open({ 
		        url: url,
		        options:{
		        	dialogClass:'showActivateQBModel',
		        	closeOnEscape: false,
		        	open: function(event,ui){ 
		        		progress.hide();
		        		var paysafe = require('./paysafe');
		        		$('body').css('overflow','hidden');
		        		$('.credit-card-box').css('max-width','unset');
		        		require('./uniform')();
		        		whatiscvv();
		        		addressjs.init();
		        		validator.init();
		        		accountjs.init();
		        		newformstyle.init();
		        		window.paysafeForQB = true;
		        		paysafe.init();
		        		
		        		/*Hide warranty details in QB popup in home page and live Tv page*/
		        		if (pageContext.ns == 'storefront' || pageContext.ns == 'livetv') {
		        			$('.showActivateQBModel .qbwarrantydetails').hide()
		        		}
		        		/*$( "body" ).on('click', '#addnewCardQb' , function() {
			        		$.ajax({
			    		        url: Urls.addCard,
			    		        type: 'POST',
			    		        data: data,
			    		        success: function (response) { 
			    		        	console.log("addcard");
			    		        	$('#addnewCardQb').append(response);
			    		        	
			    		        }
			        		});
		        		});     
		        		
		        		$('#addnewCardQb').on('click', function (e) {
		        		    if($( "#qbaddnewcard" ).hasClass('d-none')){
		        		    	$( "#qbaddnewcard" ).removeClass('d-none'); 
		        		    }
		        		    else{
		        		    	$( "#qbaddnewcard" ).addClass('d-none'); 
		        		    }
		        		});  */  
		        		
		        	},
		        	close:function() {		        		
		        		var url = Urls.restoreqbbasketfromsession; 
		        		$('body').removeAttr("style");
		        		$('html').removeAttr("style");
		        		$('body').css('overflow','x-hidden');
		        		$.ajax({
		    		        url: url,
		    		        type: 'POST',
		    		        success: function (response) { 
		    		        	//do something
		    		        }
		        		});
		        		
		        		url = window.location.href.split('#')[0];
    	        		window.location.href = url.split('?')[0];
		        	}
		        }
		    });
	    }
	});
}

function whatiscvv(){
	$('.whatiscvv').click(function (e) {
	    if($( "#whatiscvv-modelbox" ).hasClass('d-none')){
	    	$( "#whatiscvv-modelbox" ).removeClass('d-none'); 
	    }
	    else{
	    	$( "#whatiscvv-modelbox" ).addClass('d-none'); 
	    }
	});
}

function tooltipInitialize(){
	if (window.matchMedia("(max-width: 767px)").matches){
		$('.tooltip').each(function(index){
	        $(this).tooltip({
	            position: {
	                my: "bottom-15",
	                at: "bottom-15"
	            },
	            content: function () { return $(this).prop('title'); },
	            //show: "slideDown",  
	            open: function(event, ui){
	            	var _elemId = ui.tooltip.attr('id');
	            	$('.ui-tooltip').each(function(index){
	            		if(this.id!=_elemId){
	            			$(this).remove();
	            		}
	            	});
	            	var width = $(window).width(); 
	                $('.ui-tooltip').attr('max-width',width+'px');
	                if(width<350){
	                	$('#'+_elemId).css('left','0px');
	                }else{
	                	$('#'+_elemId).css('left','5px');
	                }
	                setTimeout(function(){ if($('#'+_elemId).is(":visible")){$('#'+_elemId).remove();} }, 5000);
	            },
	            close: function(event, ui) {
	            	ui.tooltip.hover(function()  {
	                    $(this).remove();
	                    //$(this).stop(true).fadeTo(500, 1);
	                },
	                function() {
	                    $(this).remove();
	                });
	            }
	        });
	    });
	}else{
		$('.tooltip').each(function(index){
	        $(this).tooltip({
	            position: {
	                my: "left+45 top-5",
	                at: "left+45 top-5"
	            },
	            content: function () { return $(this).prop('title'); },
	            //show: "slideDown",  
	            open: function(event, ui){
	            	var _elemId = ui.tooltip.attr('id');
	            	$('.ui-tooltip').each(function(index){
	            		if(this.id!=_elemId){
	            			$(this).remove();
	            		}
	            	});
	                $('#'+_elemId).hover(function(){
	                    $(this).remove();
	                });
	            },
	            close: function(event, ui) {
	                ui.tooltip.hover(function()  {
	                    $(this).remove();
	                    //$(this).stop(true).fadeTo(500, 1);
	                },
	                function() {
	                    $(this).remove();
	                });
	            }
	        });
	    });
	}
}

/*function activateQuickbyclick() {
	$(document).off('click').on('click','.activate-quick-buy', function(e){
		e.preventDefault();
		
		if ($('.product-selection .error').length > 0){
            $('.product-selection .error').remove();

            $('.product-selection .input-select').removeClass('error')
        }
        if ($('.product-selection .input-select').length > 0 && ($('.product-selection .input-select').val() == 'null' || $('#live-tv-product-options').val() =='' )) {
            var newDiv = '<span class="error" style="color:#c20000;"> Please select size</span>';
            $('.product-selection .input-select').addClass('error');
            $('.product-selection').append(newDiv);
            return false
        }
		
		var url = Urls.activatequickbuyforcustomer;//$(this).attr('href');
		pids = $(this).data('pids');
		auctioncode = $(this).data('auctioncode');
		installments = $(this).data('installments');
		channel = $(this).data('channel');
		itemPnP = $(this).data('itempnp');
		auctionCodeApplicable = $("input[name='auctionCodeApplicable']").val();
		quantity= $('#live-tv-product-quantity').val();
		dialog.open({    
			url: url,
			options:{
				dialogClass:'new-sign-registration quick-buy-dialog test',
				open: function(event,ui){
					
					$('body').off('click').on('click submit','.activateNow-button', function(e){
						e.preventDefault();
						var activateqburl = Urls.quickbuyactivatepage;
						if ($('#live-tv-product-options .error').length > 0){
							$('#live-tv-product-options .error').remove();
						}
						if ($('#live-tv-product-options').length > 0 && ($('#live-tv-product-options').val() == null || $('#live-tv-product-options').val() =='' )) {
							var newDiv = '<span class="error" style="color:#c20000;"> Please select size</span>';
							$('#live-tv-product-options').append(newDiv);
							return false;
						}
						url = util.appendParamsToUrl(activateqburl,{
							'pids':pids,
							'itemPnP':itemPnP,
							'installments':installments,
							'auctioncode':auctioncode,
							'auctionCodeApplicable':auctionCodeApplicable,
							'channel':channel,
							'quantity':quantity,
							'redirectQuickBuyActivation':true,
							'pagecontext':pageContext.ns
							});
						window.location.href = url;
					});
				}
			}
		});
	});
	
}*/

function qbsignInForCustoer() {
	$('.checkpw-button .singn-button').off('click').on('click', function(e){
		flyinheader.init();
		e.preventDefault();
		var $this = $(this);
		var url = Urls.newLogin;
		var $form = $(this).closest('form');
		var emailid = $form.find('.email-value').text();
		var pwd = $form.find('.form-row-password input').val();
		if ($form.find('.form-row-password .error').length > 0){
			$form.find('.form-row-password .error').remove();
		}
		if ($form.find('.form-row-password .input-password').hasClass('error-message')){
			$form.find('.form-row-password .input-password').removeClass('error-message')
		}
		if (pwd == '') {
			var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank <div>' 
			$form.find('.form-row-password').append(newDiv);
			return;
		}
		var data = $form.serialize();
		var cgid = '',pid='',pipeline='',cid='';
		if (pageContext.ns == 'search') {
			cgid = $('.new-register-direction').val();
		} else if (pageContext.ns == 'product'){
			pid = $('.product-code-pdp .product-id').text().trim();
		} else if (pageContext.ns == 'livetv' || pageContext.ns == 'account'){
			var pathName = window.location.pathname;
			pipeline = pathName.split('/')[pathName.split('/').length-1]
		} else if (pageContext.ns == 'content'){
			var cgidvalue = $('.new-register-direction').val();
			var cidvalue = $('.new-cid-register-direction').val();
			if (cgidvalue != "null") {
				cgid = cgidvalue;
			} else if (cidvalue != "null"){
				cid = cidvalue;		 
			} else {
				var pathName = window.location.pathname;
    			pipeline = pathName.split('/')[pathName.split('/').length-1];
				if (pipeline == 'Search-Show'){
    				cgid = window.location.search.split('=')[1];
    			} else {
    				cid = window.location.search.split('=')[1];
    			}
			}
		}
		
		var $productContainer = $('#live-tv-detail-main'),
        $tile = $productContainer.find('.auction-product-data'),
        $mlaVariations = $productContainer.find('.MLAVariations'),
    	$groupVariations = $productContainer.find('.groupVariations'),
        $selectField = $productContainer.find('.product-selection .input-select'),
        $selectedOption = $selectField.find('option:selected'),
        $selectedFirstOptionValue = $selectedOption.val(),
        $selectedFirstAuctionCode = $selectedOption.attr('data-auctioncode'),
        $selectedfirstPnP = $selectedOption.attr('data-pnp'),
        $selectsecondField = $productContainer.find('.second-variation .input-select'),
        $selectedSecondOption = $selectsecondField.find('option[selected=selected]'),
		$selectedSecondOptionValue = $selectedSecondOption.val(),
		$selectedSecondAuctionCode = $selectedSecondOption.attr('data-auctioncode'),
    	$selectedSecondPnP = $selectedSecondOption.attr('data-pnp');
		
		if($mlaVariations.length > 0){
    		var stockCode = $selectedSecondOptionValue,
    			auctionCode = $selectedSecondAuctionCode,
    			pnPValue = $selectedSecondPnP;
    		}
		
		if($groupVariations.length > 0){
    		var stockCode = $selectField.length !== 0 ? $selectField.val() : $tile.find('.product-id').data('value'),
    			auctionCode = $selectField.length !== 0 ? $selectedOption.data('auctioncode') : $tile.find('.tile-inner').data('auctioncode'),
    			pnPValue = $selectedSecondPnP;
    		}
        
        var pids = stockCode,
        auctionCodes = auctionCode,
        quantity = $productContainer.find('.quantity-selection .input-select').val();
        
        if ( auctionCodes != '') {
            auctioncode = auctionCodes;
        }      
       
        if (variationValue != ''){
            pids = variationValue;
        }
		
		$this.attr('disabled',true);
		progress.show();
		
		url = util.appendParamsToUrl(url,{'emailid':emailid , 'password':pwd,'quickbuy':true,'cgid':cgid,'pid':pid,'pipeline':pipeline,'cid':cid,'pagecontext':pageContext.ns});
		 $.ajax({
		        url: url,
		        type: 'POST',
		        data: data,
		        success: function (response) { 
		        	var parseResponse = JSON.parse(response);
		        	if (parseResponse.loginstatus && !parseResponse.tvcustomer){
		        		flyinheader.init();
		        		progress.hide();
		        		if (parseResponse.openPlaceOrderDialog) {
		        			$( ".header .header-inner" ).load(" .header-inner > *" );
		        			$( ".search-result-content #search-result-items" ).load(" #search-result-items > *" );
		        			$( "#ra-details-wrapper .ra-details-bottom" ).load(" .ra-details-bottom > *" );
		        			$( ".main-wrapper #ajaxRefresh" ).load(" #ajaxRefresh > *" );	
		        			$( ".main-wrapper .cart" ).load(" .cart > *" );	
		        			setTimeout(function(){ dialog.close(); quickBuyAction(); $( ".quick-buy-signin" ).trigger('click');}, 5000);
		        		} else {
		        			if (parseResponse.redirectQuickbuyPref) {
		        				url = util.appendParamsToUrl(parseResponse.RedirectURL,{
			        				'pids':pids,
			        				'itemPnP':itemPnP,
				        			'installments':installments,
				        			'auctioncode':auctioncode,
				        			 'auctionCodeApplicable':auctionCodeApplicable,
				        			'channel':channel,
				        			'quantity':quantity,
				        			'pagecontext':pageContext.ns,
				        			'redirectQuickBuyActivation':true
				        			});
		        				window.location.href = url;
		        			} else {
		        				$( ".header .header-inner" ).load(" .header-inner > *" );
			        			$( ".search-result-content #search-result-items" ).load(" #search-result-items > *" );
			        			$( "#ra-details-wrapper .ra-details-bottom" ).load(" .ra-details-bottom > *" );
			        			$( ".main-wrapper #ajaxRefresh" ).load(" #ajaxRefresh > *" );	
			        			$( ".main-wrapper .cart" ).load(" .cart > *" );	
			        			setTimeout(function(){ dialog.close(); showActivateQBModel();  $( "#showActivateQBModel" ).trigger('click');}, 5000);
		        			}
		        			/*url = util.appendParamsToUrl(parseResponse.RedirectURL,{
		        				'pids':pids,
		        				'itemPnP':itemPnP,
			        			'installments':installments,
			        			'auctioncode':auctioncode,
			        			 'auctionCodeApplicable':auctionCodeApplicable,
			        			'channel':channel,
			        			'quantity':quantity,
			        			'pagecontext':pageContext.ns,
			        			'redirectQuickBuyActivation':true
			        			});
		        			window.location.href = url;*/
		        		}
		        	} 
		        	else if(parseResponse.loginstatus && parseResponse.tvcustomer){	
						var prevUrl = 	window.location.href; 
		        		window.location.href = util.appendParamsToUrl(Urls.setnewPassword,{
		        			'emailid':emailid , 
		        			'PasswordResetEmail': parseResponse.PasswordResetEmail,
							'prevUrl' : prevUrl,
		        			'quickbuyredirection':parseResponse.RedirectURL,
		        			'pids':pids,
		        			'itemPnP':itemPnP,
		        			'installments':installments,
		        			'pagecontext':pageContext.ns,
		        			'auctioncode':auctioncode,
		        			 'auctionCodeApplicable':auctionCodeApplicable,
		        			'channel':channel,
		        			'quantity':quantity,
		        			'redirectQuickBuyActivation':true,
		        			'variationValue':variationValue,
		        			'openPlaceOrderDialog' : parseResponse.openPlaceOrderDialog
		        			}); 
		        	}
		        	else {
		        		$this.attr('disabled',false);
		        		progress.hide();
        				if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
		        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
		        		}
        				if (!$('.newsignin-box-inner .checkpw-content').hasClass('d-none')){
		        			$('.newsignin-box-inner .checkpw-content').addClass('d-none')
		        		}
        				if ($form.find('.checkemail-field').hasClass('d-none')){
        					$form.find('.checkemail-field').removeClass('d-none');
        				}
        				if ($form.find('.checkemail-field .email-placeholder').hasClass('d-none')){
        					$form.find('.checkemail-field .email-placeholder').removeClass('d-none');
        				}
        				if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
		        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
		        		}
        				if (!$form.find('.form-row-password .input-password').hasClass('error-message')){
        					$form.find('.form-row-password .input-password').addClass('error-message')
        				}
        				if ($form.find('.form-row-password .pwd-placeholder').hasClass('d-none')){
        					$form.find('.form-row-password .pwd-placeholder').removeClass('d-none')
        				}
        				if ($form.find('.form-row-password .error').length > 0){
        					$form.find('.form-row-password .error').remove();
        				}
    					var newDiv ='<div class="error" style="color:#c20000;">The password is incorrect for this account<div>' 
            			$form.find('.form-row-password').append(newDiv);

		        	}
		        }		      
		 });	
	});
}

function qbforgotPwdClick() {
	$('.forgot-pin a, .resend-button .button,forgotpw-button a').off('click').on('click',function(e){
		e.preventDefault();
		var url = Urls.forgotPassword;
		var $form = $(this).closest('form');
		var emailid = $form.find('.email-value').text();
		url = util.appendParamToURL(url,'emailid',emailid);
		 $.ajax({
		        url: url,
		        type: 'POST',
		        success: function (response) { 
		        	var parseResponse = JSON.parse(response);
		        	if (parseResponse.mailHasSend){
		        		if ($form.find('.forgotpw-title').hasClass('d-none')){
		        			$form.find('.forgotpw-title').removeClass('d-none');
		        		}
		        		if ($form.find('.forgotpw-content').hasClass('d-none')) {
		        			$form.find('.forgotpw-content').removeClass('d-none');
		        		}
		        		if (!$form.find('.checkpw-content .edit-button').hasClass('d-none')){
		        			$form.find('.checkpw-content .edit-button').addClass('d-none');
		        		}
		        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
		        			$('.newsignin-box-inner .checkemail-content').addClass('d-none');
		        		}
		        		if (!$form.find('.checkpw-content').hasClass('d-none')){
		        			$form.find('.checkpw-content').addClass('d-none');
		        		}
		        		if (!$form.find('.checkpw-button').hasClass('d-none')){
		        			$form.find('.checkpw-button').addClass('d-none');
		        		}
		        		if ($form.find('.forgotpw-button').hasClass('d-none')){
		        			$form.find('.forgotpw-button').removeClass('d-none');
		        		}
		        		if (!$('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
		        			$('.newsignin-box-inner .checkpw-title').addClass('d-none');
		        		}
		        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
		        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        		}
		        		if (!$form.find('.form-row-password').hasClass('d-none')){
		        			$form.find('.form-row-password').addClass('d-none');
		        		}
		        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
		        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
		        		}
		        		if ($('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
		        			$('.newsignin-box-inner .guest-checkout-button').removeClass('d-none')
		        		}
		        	} else {
		        		$form.find('.login-error-message').removeClass('d-none');
		        	}
		        	
		        	$('.pwemail').html(emailid);
		        }
		 });
	});
}

/**Start - Script to display the modal popup in registration page when you click "Shopped on our TV channel but not on our website?" **/
$( "#tvconnect-modelbox" ).dialog({      
	autoOpen: false,      
	dialogClass: "tvconnectdialog",
    modal: true,   
    height: 'auto',            
    resizable: true,              
});     

$( "#tvconnect" ).click(function() {  
	$(this).closest('form');
	$( "#tvconnect-modelbox" ).dialog( "open" );      
});  

$( ".quickbuy-preference-page .fa-question-circle" ).on('click', function() {
    var container = $(this).closest('h5').find('.help-dialog');
    $(container).clone().dialog({
	    dialogClass:'help-dialog-modal',
	    modal: true
	});
});

function qbeditbuttonclick() {
	$('body').off('click').on('click','.returning-customers form .checkpw-content .edit-button, .wrong-email-link', function(e){
		var $form1 = $(this).closest('form');
		if (!$form1.find('.checkpw-content').hasClass('d-none')){
			$form1.find('.checkpw-content').addClass('d-none');
		}
		if ($form1.find('.checkemail-field').hasClass('d-none')){
			$form1.find('.checkemail-field').removeClass('d-none');
		}
		if (!$form1.find('.form-row-password').hasClass('d-none')){
			$form1.find('.form-row-password').addClass('d-none');
		}
		if ($form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
			$form1.find('.form-row-button .checkemail-button').removeClass('d-none');
		}
		if (!$form1.find('.checkpw-button').hasClass('d-none')){
			$form1.find('.checkpw-button').addClass('d-none');
		}
		if (!$('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
			$('.newsignin-box-inner .checkpw-title').addClass('d-none');
		}
		if ($('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
			$('.newsignin-box-inner .checkemail-title').removeClass('d-none')
		} 
		if (!$form1.find('.forgotpw-title').hasClass('d-none')){
			$form1.find('.forgotpw-title').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-content').hasClass('d-none')) {
			$form1.find('.forgotpw-content').addClass('d-none');
		}
		if (!$form1.find('.guest-checkout-button').hasClass('d-none')) {
			$form1.find('.guest-checkout-button').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-button').hasClass('d-none')){
			$form1.find('.forgotpw-button').addClass('d-none');
		}
		$.each($form1.find('.error'),function(){$(this).remove()});
	});
}
/**End**/    

function qbenteredWrongEmail() {
	 $('.guest-checkout-button a').off('click').on('click', function(e){

		var $form1 = $(this).closest('form');
		if ($('.checkemail-title').hasClass('d-none')){
			$('.checkemail-title').removeClass('d-none');
		}
		if ($('.checkemail-content').hasClass('d-none')){
			$('.checkemail-content').removeClass('d-none');
		}
		if ($('.edit-button').hasClass('d-none')){
			$('.edit-button').removeClass('d-none');
		}
		if ($form1.find('.checkemail-button').hasClass('d-none')){
			$form1.find('.checkemail-button').removeClass('d-none');
		}
		if ($form1.find('.checkemail-field').hasClass('d-none')){
			$form1.find('.checkemail-field').removeClass('d-none');
		}
		if (!$form1.find('.forgotpw-title').hasClass('d-none')){
			$form1.find('.forgotpw-title').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-content').hasClass('d-none')) {
			$form1.find('.forgotpw-content').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-button').hasClass('d-none')){
			$form1.find('.forgotpw-button').addClass('d-none');
		}
	});
}

function qbdisguisePassword(){
	$('.icon-password').off('click').on('click', function () {
        var $this = $(this), $passwordField = $this.closest('.form-row-password').find('.input-text');

        if (!$this.hasClass('show')) {
            $passwordField.removeAttr('type');
            $passwordField.prop('type', 'password');
        } else {
            $passwordField.removeAttr('type');
            $passwordField.prop('type', 'text');
        }
        $this.toggleClass('show');
    });
}

function quickBuyAction() {
	$( document ).off('click').on('click', '#quick-buy.quick-buy-signin', function(e) {
		e.preventDefault();
		
		if ((($('#live-tv-product-options').val() == "null" || $('#live-tv-product-options').val() == null)) && $('#live-tv-product-options').length > 0) {
        	$('.product-selection .input-select').css('border','3px solid #c20000');
            $('.product-selection .input-select').addClass('error');
            return;
        }
		
		if(sessionStorage.getItem("secondField")  != null) {
			$('.second-variation .input-select').val(sessionStorage.getItem("secondField")).change();
			sessionStorage.removeItem("secondField");
		}
		
		if($('.live-tv-detail-main .MLAVariations').length > 0){
	    	var secondVariation = $('.second-variation .input-select').val();
	    	if (secondVariation == null || secondVariation == "null") {
	    		$('#select-second-variation').addClass('border')
	    		return false;
	    	}
	    	else{
	    		$('#select-second-variation').removeClass('border')
	    	}
	    }
		
		$('body').css('overflow','x-hidden');				
		
		/*var $form = $(this).closest('form'),
		$productContainer = $('#live-tv-detail-main'),
		$tile = $productContainer.find('.auction-product-data'),
        $selectField = $productContainer.find('.product-selection .input-select'),
        $selectedOption = $selectField.find('option:selected'),
        pids = $selectField.length !== 0 ? $selectField.val() : $tile.find('.product-id').data('value'),
        auctionCodes = $selectField.length !== 0 ? $selectedOption.data('auctioncode') :
                $tile.find('.tile-inner').data('auctioncode'),
        quantity = $productContainer.find('.quantity-selection .input-select').val();*/
		
		var $form = $(this).closest('form'),
		$productContainer = $('#live-tv-detail-main'),
		$mlaVariations = $productContainer.find('.MLAVariations'),
    	$groupVariations = $productContainer.find('.groupVariations'),		
		$tile = $productContainer.find('.auction-product-data'),
        $selectField = $productContainer.find('.product-selection .input-select'),
        $selectedOption = $selectField.find('option:selected'),
        $selectedFirstOptionValue = $selectedOption.val(),
        $selectedFirstAuctionCode = $selectedOption.attr('data-auctioncode'),
        $selectedfirstPnP = $selectedOption.attr('data-pnp'),
        $selectsecondField = $productContainer.find('.second-variation .input-select'),
        $selectedSecondOption = $selectsecondField.find('option[selected=selected]'),
		$selectedSecondOptionValue = $selectedSecondOption.val(),
		$selectedSecondAuctionCode = $selectedSecondOption.attr('data-auctioncode'),
    	$selectedSecondPnP = $selectedSecondOption.attr('data-pnp'),
    	budgetPayContainer = $productContainer.find('.budgetpay'),
		installments = budgetPayContainer.attr('data-installments');
		
		if($mlaVariations.length > 0){
    		var stockCode = $selectedSecondOptionValue,
    			auctionCode = $selectedSecondAuctionCode,
    			pnPValue = $selectedSecondPnP;
    	}
    	
    	if($groupVariations.length > 0){
    		var stockCode = $selectField.length !== 0 ? $selectedFirstOptionValue : $tile.find('.product-id').data('value'),
				auctionCode = $selectField.length !== 0 ? $selectedFirstAuctionCode : $tile.find('.tile-inner').data('auctioncode'),
    			pnPValue = $selectField.length !== 0 ? $selectedfirstPnP : $tile.find('.tile-inner').data('data-pnp');
    	}
    	
    	var pids =	stockCode,
    	auctionCodes = auctionCode,
    	auctionCodeApplicable = $("input[name='auctionCodeApplicable']").val(),
    	//auctionCodeApplicable =$('.auctionCodeApplicable').text(),     	
    	quantity = $productContainer.find('.quantity-selection .input-select').val();

    

    /*$form.append('<input type="hidden" name="pids" value="' + pids + '"/>');
    $form.append('<input type="hidden" name="quantity" value="' + quantity + '"/>');
    $form.append('<input type="hidden" name="auctionCodes" value="' + auctionCodes + '"/>');*/
        
    
		/*var installments = $("input[name='installments']").val();
		var itemPnP = $("input[name='itemPnP']").val();
		 var auctionCodeApplicable = $("input[name='auctionCodeApplicable']").val();
		var channel = $("input[name='channel']").val();
        var url = Urls.liveTvOrder + "?installments=" + installments + "&itemPnP= "+ itemPnP + "&channel= "+ channel;*/
    
    if ($('#uniform-live-tv-product-options span').html() != "Select") {
    	progress.show();
    }

        
    if ($('#live-tv-product-options').val() != "null") {
    	progress.show();
    }       
    

    var url = Urls.liveTvOrder;
    url = util.appendParamsToUrl(url,{
		'pids':pids, 
		'auctionCodes':auctionCodes,
		'auctionCodeApplicable':auctionCodeApplicable,
		'quantity':quantity,
	});
    
    var checkBidStatus = Urls.checkBidStatus;
    checkBidStatus = util.appendParamsToUrl(checkBidStatus,{
		'pids':pids,
		'auctionCodes':auctionCodes,
		'auctionCodeApplicable':auctionCodeApplicable,
		'quantity':quantity,
		'checkbidstatus' : true,
	});
    $.ajax({
        url: checkBidStatus,
        type: 'POST',
        success: function (response, status, xhr) { 
        	var ct = xhr.getResponseHeader("content-type") || "";
            if (ct.indexOf('html') > -1) {
            	dialog.open({
                    url: url,
                    data: $form.serialize(),
                    options:{
                        dialogClass:'qb-livetv-dialog',
                        open: function(event,ui){
                        	progress.hide();
                        	$('body').css('overflow','hidden');
                        	require('./uniform')();
                        	$('#existingCardsCVV').addClass('checkType');
                        	$('#existingCardsCVV').attr('pattern','[0-9]{1,4}');
                        	$('.qb-livetv-dialog .footer .content-heading').css('line-height','2.5rem');
                        	$('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_Paysafe').on('click', function(){
                        		$('.qb-livetv-dialog .footer .content-heading').css('line-height','2.5rem');
                        		var ordertotal = $('#paysafe_ordertotal').text();
                        		$('.updateqbtotal').html(ordertotal);
                        	});
                        	$('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_budgetPay').on('click', function(){
                        		$('.qb-livetv-dialog .footer .content-heading').css('line-height','1.5rem');
                        		var ordertotal = $('#budgetpay_ordertotal').text();
                        		$('.updateqbtotal').html(ordertotal);
                        		
                        	});
        	            	$('.qb-livetv-dialog').off('click').on('click', '.confirm-qborder', function(event){
        	            		event.preventDefault();
        	            		var $form = $(this).closest('form');
        	                	//var data = $form.serialize();
        	            		var token = $('#dwfrm_billing_paymentToken').val();
        	            		//var cvv = $('#existingCardsCVV').val();
        	            		var existingCards = $('#existing-paysafe-card').val();
        	            		if($('.safe-budget').length){
        	            			var radioValue = $("input[name='dwfrm_billing_paymentMethods_selectedPaymentMethodID']:checked").val();
        	            		}
        	            		else{
        	            			var radioValue = $("input[name='dwfrm_billing_paymentMethods_selectedPaymentMethodID']").val();
        	            		}
        	            		/*if (cvv == ''){
        	        				var newDiv ='<div class="error" style="color:#c20000;">CVV cannot be blank<div>';
        	        				if($('.cvv-field').find('.error').length == 0) {
        	        					$form.find('.cvv-field').append(newDiv);
        	        				}
        	        				return;
        	        			}*/
        	            		var placeOrderURL = Urls.quickbuyPlaceOrder;
        	            		$.ajax({
        	        		        url: placeOrderURL + "?token=" + token + "&paymentMethod=" + radioValue + "&existingCards=" + existingCards,
        	        		        type: 'POST',
        	        		        beforeSend: function() { progress.show(); },
        	        		        success: function (response) {
        	        		        	progress.hide();
        	        		        	if(response.success && response.Orderno!=""){
        		        		        	if ($('#quickBuyConfirmation').hasClass('d-none')){
        		        		        		$('#quickBuyForm').addClass('d-none');
        		        		        		$('#quickBuyConfirmation').removeClass('d-none');
        		        		        		$("#qborder-number").html(response.Orderno);
        		        		        		var gtmPurchase = JSON.stringify(response.gtmPurchase),
        		        		        		gtmProducts = JSON.stringify(response.gtmProducts);
        		        		        		$("#quickBuyConfirmation #gtmDataHolder").attr('data-gtmPurchase', gtmPurchase);
        		        		        		$("#quickBuyConfirmation #gtmDataHolder").attr('data-gtmProducts', gtmProducts);
												/*window.dataLayer.push({
													'event':'purchase',
													'ecommerce': {
														'purchase': {
															'actionField': JSON.parse(gtmPurchase),
															'products': JSON.parse(gtmProducts)
														}
													}
												});*/
												orderconfirmation.init();
												dataLayer.push({'event': 'QBOrderConfirmation'});
        		        		        	}
        	        		        	}
        	        		        	else{
        	        		        		if(response.errorCode >= 0 && response.errorCode <= 11 ){
        	        		        			if(response.errorCode == 11){
        	        		        				if ($('.budgetPaySpecificError').hasClass('d-none')){
            			        		        		$('.budgetPaySpecificError').removeClass('d-none');
            			        		        	}     	        		        				
        	        		        			}
        	        		        			else{
        	        		        			if ($('.budgetPaySomethingError').hasClass('d-none')){
        			        		        		$('.budgetPaySomethingError').removeClass('d-none');        			        		        		
        			        		        	}
        	        		        			}
        	        		        			if ($('#activation-error').hasClass('d-none')){
        	        		                		$('#activation-error').removeClass('d-none');
        	        		                		$("#activation-error").html("Something didn't work. Please contact customer care on 0344 375 2525.");	     
        	        			        		}	
        	        		        		}	        		        		
        	        		        	}
        	        		        	
        	        		        	$("#qbConfirm-close").on('click', function(){
        	        		        		dialog.close();
        	        		        	});
        	        		        }
        	            		});
        	            	});
        	            	
        	            	$('.qb-livetv-dialog').on('click', '.settings', function(event){
        	                    var url = Urls.restoreqbbasketfromsession;
        	                    $.ajax({
        	                        url: url,
        	                        type: 'POST',
        	                        success: function (response) {
        	                            //do something
        	                        }
        	                    });
        	                });
        	            	
                        },
                        close:function() {
                        	$('body').removeAttr("style");
        	        		$('html').removeAttr("style");	             	        		
        	        		var url = Urls.restoreqbbasketfromsession; 
        	        		$.ajax({
        	    		        url: url,
        	    		        type: 'POST',
        	    		        success: function (response) { 
        	    		        	//do something
        	    		        }
        	        		});
        	        		url = window.location.href.split('#')[0];
        	        		window.location.href = url.split('?')[0];
        	        	}               	
                      }
                });
            }
            if (ct.indexOf('json') > -1) {
            	if(response.errorCode == 12.0){
            		$("#bidException").dialog({
            			dialogClass:'bidException',
            			open: function (event, ui) {
        				progress.hide();
            			if ($('#bidException').hasClass('d-none')){
            			$('#bidException').removeClass('d-none');
            			}
            			$('#bidException').css('min-height','unset');
            			},
            			modal: true
            		});
            		
            	}
            } 
        	
        }
	});	
        	
    //var url = Urls.quickbuyOrder;
        
    });
}

var quickBuyClick = false,variationValue = '',qbClicked = false,quickBuyOrderClick=false;
function showActivateModalPopup(){
	if($('.quickBuyClick').length > 0){
		quickBuyClick = $('.quickBuyClick').val();
		if(quickBuyClick == 'true'){
			quickBuyClick = true;
		}		
	} else if($('.quickBuyOrderClick').length > 0){
		quickBuyOrderClick = true;
	}
	
	if ($('.quantity').length > 0){
		var qty = $('.quantity').val();
		$('#live-tv-detail-main').find('.quantity-selection .input-select').val(qty);
	}
	
	if($( document ).find('#showActivateQBModel').length > 0 && quickBuyClick && !qbClicked && !quickBuyOrderClick){
		$('.quickBuyClick').val('false');
		qbClicked = true;
		variationValue = $('.variationValue').val();
		secondvariationValue = $('.secondVariation').val();
		sessionStorage.setItem('secondField', secondvariationValue); 
		$('#live-tv-product-options').val(variationValue).trigger('change');
		$('.second-variation .input-select').val(secondvariationValue).trigger('change');
		$( document ).find('#showActivateQBModel').trigger('click');
	} else if ($(document).find('#quick-buy.quick-buy-signin').length > 0 && !quickBuyClick && !qbClicked && quickBuyOrderClick){
		qbClicked = true;
		variationValue = $('.variationValue').val();
		secondvariationValue = $('.secondVariation').val();
		sessionStorage.setItem('secondField', secondvariationValue); 
		$('#live-tv-product-options').val(variationValue).trigger('change');
		$('.second-variation .input-select').val(secondvariationValue).trigger('change');
		$( document ).find('#quick-buy.quick-buy-signin').trigger('click');
	}	
}

function checkFieldsIntiator(){
	$('#dwfrm_quickbuyaddress_customer_firstname').addClass('checkType');
	$('#dwfrm_quickbuyaddress_customer_firstname').attr('pattern','[A-Za-z\s]{1,50}');
	$('#dwfrm_quickbuyaddress_customer_lastname').addClass('checkType');
	$('#dwfrm_quickbuyaddress_customer_lastname').attr('pattern','[A-Za-z\s]{1,50}');
	$('#dwfrm_quickbuyaddress_customer_mobile').addClass('checkType');
	$('#dwfrm_quickbuyaddress_customer_mobile').attr('pattern','[0-9]{1,11}');
	$('#existing-paysafe-cvv').addClass('checkType');
	$('#existing-paysafe-cvv').attr('pattern','[0-9]{1,4}');
	$('#dwfrm_quickbuyaddress_billingAddress_address_postCode').addClass('checkType');
	$('#dwfrm_quickbuyaddress_billingAddress_address_postCode').attr('pattern','[A-Za-z0-9 ]{1,8}');
	$('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').addClass('checkType');
	$('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').attr('pattern','[A-Za-z0-9 ]{1,8}');
	$('.mobile-no').find('.required-indicator').addClass('tooltip');
	$('.mobile-no').find('.required-indicator').attr('title','Share your phone number for a seamless shopping experience through our Website and TV.');
	$('.address-alias').find('.required-indicator').addClass('tooltip');
	$('.address-alias').find('.required-indicator').attr('title','example - home, office, etc.');
	tooltipInitialize();
}

function checkFieldsType(){
	$('form').find('input').on('keypress', function (e) {
		var keyCode = e.keyCode || e.which;
		if ($(this).is(':visible')) {
			var classes = ($(this).attr('class') != null) ? $(this).attr('class').search('checkType') : -1;
			if (classes >= 0) {
				var regexLength = this.pattern.split('{');
				var regex = new RegExp(regexLength[0]);
				var lengthVal = regexLength[1].replace('}', '');
				var lengthValues = lengthVal.split(',');
				var minLength = lengthValues[0];
				var maxLength = lengthValues[1];
				var isValid = regex.test(String.fromCharCode(keyCode));
				var isValid1 = $(this).val().length+1;
				if (!isValid) {
					//$(this).css("border-bottom", "3px solid #FF0000");
					return isValid;
				}else{
					//$(this).css("border-bottom", "3px solid #00FF00");
				}
				if (isValid1 < minLength) {
					//$(this).css("border-top", "3px solid #FF0000");
				}else{
					//$(this).css("border-top", "3px solid #00FF00");
				}
				if(isValid1 > maxLength){
					//$(this).css("border-top", "3px solid #FF0000");
					return false;
				}
			}
		}
	});
}

$(".activate-quickbuy").on("click", function(){
$('.dwfrm_quickbuyaddress_shippingAddress_addressid-error').hide();
});

$("#dwfrm_quickbuyaddress_billingAddress_addressid").focus(function(){
    $('#dwfrm_quickbuyaddress_shippingAddress_addressid-error').remove();
 });
  
 $(".add-billing-address .address-check-btn").click(function(){ 
		 $('.billingform #dwfrm_quickbuyaddress_billingAddress_addressid-error').remove();
  });

function qbbuttondisable(){
	if ($('.shippingaddressgroup .post-code-field').hasClass('validation-error') || $('.billingaddressgroup .post-code-field').hasClass('validation-error')){		
		
		$('.activate-quickbuy').attr('disabled','disabled');	
	}
	else {
		$('.activate-quickbuy').removeAttr('disabled','disabled');
	}
}
setInterval(qbbuttondisable, 1000);
exports.init = function () {
    initializeEvents();
    if (!$('.ui-dialog.showActivateQBModel').is(':visible')) {
        addressjs.init();
    }
};
