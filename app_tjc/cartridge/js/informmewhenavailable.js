'use strict';
var newformstyle = require('./new-form-style');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
    $('#cart-table')
        .on('click', '.inform-me-when-available button', function () {        	
        	informmewhenavailable();            
    $( document ).ajaxComplete(function( event, xhr, settings ) {
    	newformstyle.init();        	
        	informmewhenavailable();  	
    });
   });            
}
function informmewhenavailable() {
	
	$('#cart-table')
    .on('click', '.inform-me-when-available button', function () {
        var $this = $(this),
            $informBox = $this.parents('.inform-me-when-available'),
            $formRow = $informBox.find('.field'),
            $formField = $formRow.find('input.input-text');
        
        var email = $informBox.find('input').val();
        if(email == "") {
        	$informBox.addClass('validation-error');
        	if($formRow.find('.error-msg.null-validation').length == 0) {
        		$formRow.append('<div  class="error-msg null-validation" style="display:block;">Cannot be blank</div>');
        		
        	}
        	return false;
        }else{
        	 $informBox.find('.error-msg.null-validation').remove();                   
        }
        if($formRow.find('.error-msg').length>0){
        	return false;
        }                    
        $informBox.find('.error-msg').remove();
        $formRow.removeClass('validation-error');  
        $.ajax({
            type: 'post',
            url: $this.data('url'),
            data: {
                email: $informBox.find('input').val(),
                pid: $this.data('pid') 
            }
        }).done(function (response) {
            if (response.success) {
                $informBox.html('<div class="message success-form"><div class="message-inner">' +
                        '<div class="message-content">' + response.message + '</div></div></div>');
            } else {
                $formRow.find('.field').append('<div class="error-msg">' + response.message + '</div>');
                $formRow.addClass('validation-error');
            }
            var url = Urls.InformProductRemove; 
            $.ajax({
                type: 'post',
                url: url,
                data: {                       
                    pid: $this.data('pid')                       
                },
            success: function (response) {         	    	  
				var res = response; 
				if(res.removed == "true" ){          	        	    	
					$( "#primary .cart" ).load(" .cart > *" );
					$( ".xlt-header .header-bag" ).load(" .header-bag > *" );
					
				}  				
			},        
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR + textStatus + errorThrown);
			} 
            });                  
        });
    });
}

exports.init = function () {
    initializeEvents();
    informmewhenavailable();
};
