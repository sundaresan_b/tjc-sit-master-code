'use strict';

/* Required Modules */

var util = require('./util'),
    ajax = require('./ajax'),
    progress = require('./progress'),
    newsignin = require('./newsignin'),
    dialog = require('./dialog'),
    quickbuy = require('./quickbuy');


/* Local Variables / Configuration */
/* ******************************* */
var LiveTV_RefreshTime = parseInt((window.SitePreferences.LiveTV_ContentRefresh_Time != null)?window.SitePreferences.LiveTV_ContentRefresh_Time:"3000"),  //ms
    RecentlyOnAir_RefreshTime = parseInt((window.SitePreferences.RecentlyOnAir_ContentRefresh_Time != null)?window.SitePreferences.RecentlyOnAir_ContentRefresh_Time:"15000"),  //ms
    updateIntervalLiveTVLandingPage = LiveTV_RefreshTime,
    updateIntervalLiveTVDetailPage = LiveTV_RefreshTime,
    liveTVLandingPageUpdateURL,
    liveTVDetailPageUpdateURL,
    lastResponse,
    currentSelecedOption,
    secondVariation,
    secondVariationAuctionCode,
    carouselClickTimeout,
    lastAuctionObjectString,
    nonUpdateOptionChangeFlag = false,
	browsername = util.browsername();


/* Local Functions */
/* *************** */
function updateLiveTVLandingPageTile($tile) {
    // get channel id from product tile
    var channel = $tile.data('channel');
    if (channel === undefined || !channel) {
        return;
    }

    // load new product infos from server
    $.ajax(
        liveTVLandingPageUpdateURL,
        {
            dataType: 'html',
            data: {channel: channel, format: 'ajax', context: 'landing'},
            method: 'GET',
            timeout: updateIntervalLiveTVLandingPage - 500 < 2500 ? 2500 : updateIntervalLiveTVLandingPage - 500,
            success: function (response) {
                $tile.html(response);
                util.imageAdjust();
            }
        }
    );
}

function initLiveTVLandingPage() {
	
	var $channelSelector = $('#live-tv-channel-selector:not(.initialized)'),
        $tiles;

    //check if we need to initialize the channel selector
    if (!$channelSelector.length) {
        return;
    }

    // get product tiles and update URL
    $tiles = $channelSelector.find('.auction-product-data');
    liveTVLandingPageUpdateURL = $channelSelector.data('update-url');

    // init automatic update
    setInterval(function () {
        $tiles.each(function () {
            updateLiveTVLandingPageTile($(this));
        });
    }, updateIntervalLiveTVLandingPage);

    // mark channel selector as initialized
    $channelSelector.addClass('initialized');
}

function homeRecentlyOnAir(){
	/*
	$(".recently-on-aired-slider").slick({
	    mobileFirst: true,
	    responsive: [{
	        breakpoint: 300,
	        settings: {
	            slidesToShow: 2,
	            slidesToScroll: 2,
	            centerMode: true,
	            centerPadding: '15%'
	        }
	    }, {
	        breakpoint: 767,
	        settings: {
	        	rows:1,
	        	slidesToShow: 5,
	            slidesToScroll: 5,
	            centerMode: true,
	            centerPadding: '10%'
	        }
	    }, {
	        breakpoint: 1023,
	        settings: {
	            rows:2,
	        	slidesToShow: 3,
	            slidesToScroll: 3,
	            centerMode: false,
	            infinite: false
	        }
	    }, {
	        breakpoint: 1479,
	        settings: {
	        	rows:2,
	        	slidesToShow: 4,
	            slidesToScroll: 4,
	            centerMode: false,
	            infinite: false
	        }
	    }]
	});
	*/
	$('.missedauction-price').each(function(index,value){
		var price = $(this).val();
		var $parent = $(this).parents('.custom-slider-inner');
		$parent.find('.slider-tile .slider-product-tile').append('<div class="product-pricing"><div class="price "><span class="price-sales" title="Sale Price">'+price+'</span></div></div>');
	});
	util.imageAdjust();
}


function updateMLAImage(){
	if($('.second-variation .input-select').length > 0){
		var mlaVarientID = $('.second-variation .input-select option:selected').val(),
		url = Urls.mlaproductimage,
		data = {mlaVarientID : mlaVarientID };
		
		if(mlaVarientID == undefined || mlaVarientID == "undefined" || mlaVarientID == "null" || mlaVarientID == null){
			mlaVarientID = $('.second-variation .input-select option:nth-child(2)').val();
		}
		
		$.ajax({
	        url: url + "?mlaProductID=" + mlaVarientID,
	        data : data,
	        type: 'GET',
	        success: function (response) {
	        	$('.updateMLAImage').html(response);
	        	util.imageAdjust();
	        }
		});
	}
}

function updateLiveTVDetailPageTile($tile) {
	
    var channel = $tile.data('channel'),
        optionSelector,
        data,
        selectedOption;

    if (channel === undefined || !channel) {
        return;
    }

    // load new product infos from server
    optionSelector = $tile.find('#live-tv-product-options');
    if (optionSelector.find('option:selected').data('isbuyall')) {
        selectedOption = 'BUYALL';
    } else {
        selectedOption = optionSelector.length ?
                (currentSelecedOption !== undefined ? currentSelecedOption : optionSelector.val()) :
                null;
    }
    data = {
        format: 'ajax',
        channel: channel,
        context: 'detail',
        selectedOption: selectedOption,
        secondVariation: secondVariation,
        quantity: $tile.find('.quantity-selection .input-select').val()
    };
    
    var isproductvisible = $('#v-si').is(":visible");
    var isvideovisible = $('#v-tv').is(":visible");
    
    $.ajax(
        liveTVDetailPageUpdateURL,
        {
            dataType: 'html',
            data: data,
            method: 'GET',
            timeout: Math.max(updateIntervalLiveTVDetailPage - 500, 2500),
            success: function (response) {           	          	 
                //fetch response details to check UI needs to be updated
                var parseHTML = $.parseHTML(response),
                	$parentDIV = $(parseHTML).filter('div.tile-inner'),
                	auctionCode = $parentDIV.attr('data-auctioncode'),
                	auctionPrice = $parentDIV.attr('data-price'),
                	auctionPNP = $parentDIV.attr('data-pnp'),
                	productID = $parentDIV.find('.home-recently-live-content .product-section.product-id').attr('data-value'),
                	tempAuctionCode = '',
                	auctionObject = new Object(),
                	auctionObjectString = '',
                	tempObject,
                	tempObject1;
                auctionObject.auctionCode = auctionCode;
                auctionObject.auctionPrice = auctionPrice;
                auctionObject.auctionPNP = auctionPNP;
                auctionObject.auctionDefaultProduct = productID;
                if($parentDIV.find('select#live-tv-product-options').length > 0){
	                auctionObject.auctionGroup = [];
	                $parentDIV.find('select#live-tv-product-options option:not(:first)').each(function(index, value){
	                	tempObject = new Object();
	                	tempAuctionCode = $(this).attr('data-auctioncode');
	                	tempObject.auctionCode = tempAuctionCode;
	                	tempObject.auctionValue = $(this).attr('value');
	                	tempObject.auctionPrice = $(this).attr('data-price');
	                	tempObject.auctionPNP = $(this).attr('data-pnp');
	                	tempObject.auctionInStock = $(this).attr('data-stock');
	                	
	                	if($parentDIV.find('select#select-second-variation').length > 0){
		                	tempObject.auctionVariants = [];
		                	$parentDIV.find('select#select-second-variation option:not(:first)[data-group*="'+tempAuctionCode+'"]').each(function(index, value){
		                		tempObject1 = new Object();
		                		tempObject1.auctionCode = $(this).attr('data-auctioncode');
		                		tempObject1.auctionValue = $(this).attr('value');
		                		tempObject1.auctionPNP = $(this).attr('data-pnp');
		                		tempObject1.auctionInStock = $(this).attr('data-stock');
		                		tempObject.auctionVariants.push(tempObject1);
							});
	                	}
	                	
	                	auctionObject.auctionGroup.push(tempObject);
	                });
                }
                auctionObjectString = JSON.stringify(auctionObject);
                
                //check if UI needs to be updated
                if ((lastAuctionObjectString === auctionObjectString) && !nonUpdateOptionChangeFlag) {
                	$('select#live-tv-product-options, select#select-second-variation').off('click');
                	$('select#live-tv-product-options, select#select-second-variation').on('change',function(){ nonUpdateOptionChangeFlag = true; });
                    return;
                }
                //lastResponse = response;                         
                lastAuctionObjectString = auctionObjectString;
                nonUpdateOptionChangeFlag = false;
                console.log('data-updating');
                if($('.second-variation .input-select').length > 0){
                	secondVariation = $tile.find('#select-second-variation').val();
                }
                
                //update ui
                $tile.html(response);
                if(isproductvisible){
                	$('#v-si').show();
                	$('#v-tv').hide();
                }
                if(isvideovisible){
                	$('#v-si').hide();
                	$('#v-tv').show();
                }
                require('./uniform')(); 
                increaseDecreaseQuantity();
                MLAvariantCheck();
                selectVariations();
				quickbuy.init();
				newsignin.init();
				util.imageAdjust();
				if($('.second-variation .input-select').length > 0){
					$tile.find('#select-second-variation').val(secondVariation);
					$tile.find('#select-second-variation option:selected').prop("selected",false);
					$tile.find('#select-second-variation option[selected=selected]').removeAttr('selected');
					$tile.find('#select-second-variation option[value="'+secondVariation+'"]').prop("selected",true);
					$tile.find('#select-second-variation option[value="'+secondVariation+'"]').attr("selected","selected");
					//updateMLAImage();
				}
				var RecentlyAirTimeout = setTimeout(updateRecentlyOnAirURL, RecentlyOnAir_RefreshTime);
				$(".sigin-model-button, .live-tv-add-to-cart-form a.home-page-login-bid-now, .live-tv-add-to-cart-form a.quick-buy-login-class").on("click", function(){
					clearTimeout(RecentlyAirTimeout);
				});
				$(".new-sign-registration .ui-dialog-titlebar-close").on("click", function(){
					setTimeout(RecentlyAirTimeout);
				});
            }
        }
    );
}

var $homePageLiveTVStreamVideoInitialized = false;
function homePageLiveTVStreamVideo(){
	var urlChannel = $('#v-tv').attr('data-LoadLiveTVContentURLChannel'),
		url = Urls.homeLiveStreamVideo;
    url = util.appendParamToURL(url,'channel',urlChannel);
	console.log("TV Initializing");
	if (window.innerWidth >= 1024) {
		$('#tv-progress-loader').css({'top':'10rem','left':'50%'});
	}else if (window.innerWidth >= 768 && window.innerWidth <= 1023) {
		$('#tv-progress-loader').css({'top':'9rem','left':'11rem'});
	}else {
		$('#tv-progress-loader').css({'top':'9rem','left':'50%'});
	}
	$('#tv-progress-loader').show();
	$("#v-tv-Content").load(url, function(responseTxt, statusTxt, jqXHR){
		if(statusTxt == "success"){
            console.log("TV Initialized");
            $homePageLiveTVStreamVideoInitialized = true;
            $("#v-tv-Content").css('background-color','transparent');
            $("#v-tv").css('height','auto');
            $("video").prop('muted', false);
            $('#tv-progress-loader').hide();
        }
        if(statusTxt == "error"){
        	console.log("TV Initializing Error");
        	//$('#w-si').trigger('click');
        }
	});
}

function updateRecentlyOnAirURL(){
	var $homeRecentlyOnAir = $('#recently-on-aired-slider:not(.initialized)');
    if ($homeRecentlyOnAir.length) {
    	var homeRecentlyOnAirURL = $("#recently-on-aired-slider").attr("data-update-url");
    	$.get(homeRecentlyOnAirURL, {},
  		      function (returnedHtml) {
  		      $("#recently-on-aired-slider").html(returnedHtml);  
  		      homeRecentlyOnAir();
  		}); 
    }
}

function initLiveTVDetailPage() {
    var $productContainer = $('#live-tv-detail-main:not(.initialized)'),
        $tile;

    //check if we need to initialize the liveTV product tile
    if (!$productContainer.length) {
        return;
    }

    // get product tile and update URL
    $tile = $productContainer.find('.auction-product-data');
    liveTVDetailPageUpdateURL = $productContainer.data('update-url');

    // update ui on page load to get current information and set lastResponse
    updateLiveTVDetailPageTile($tile);

    // init automatic update
    setInterval(function () {
        updateLiveTVDetailPageTile($tile);
    }, updateIntervalLiveTVDetailPage);

    // init additional updates (when user changes option)
    $productContainer.on('change', '.product-selection .input-select', function () {
    
           var $this = $(this),
            selectedValue = $(this).find('option:selected').val(),
  	        selectedFirstAuctionCode = $(this).find('option:selected').attr('data-auctioncode'),      	
        	selectedFirstPNP = $(this).find('option:selected').attr('data-pnp');        	
        	      	           	
        if(selectedValue !== 'null'){        	
        	if((selectedFirstAuctionCode == null) || (selectedFirstAuctionCode == "") || (selectedFirstAuctionCode == 'undefined' || typeof selectedFirstAuctionCode === 'undefined' )  ||  (selectedFirstPNP == null) || (selectedFirstPNP == "") || (selectedFirstPNP == 'undefined' || typeof selectedFirstPNP === 'undefined' )){
	         $('.live-tv-detail-main').addClass('code-not-available');         
            }else{            
             $('.live-tv-detail-main').removeClass('code-not-available');
            }
           } 
    
        currentSelecedOption = $(this).val();
        
        nonUpdateOptionChangeFlag = true;
        if($('.live-tv-detail-main .MLAVariations').length > 0){        	
        	secondVariation = "null";
        }        
        updateLiveTVDetailPageTile($tile);
        if((currentSelecedOption != "null") || (currentSelecedOption != null)){
        	$('#live-tv-product-options').removeClass('border');
		}
        
        if($('.live-tv-detail-main .MLAVariations').length > 0){        	
        	selectVariations();
        }        
        
    });
    
    // Validation for first varient to check status of stock and aother validation
    $productContainer.on('change', '.variations .input-select', function () {
        secondVariation = $(this).val();
        var $selectField1 = $productContainer.find('.variations .input-select'),
        $selectedOption1 = $selectField1.find('option:selected'),
        secondVariationAuctionCode = $selectedOption1.attr('data-auctioncode'),
    	$selectedSecondPnP = $selectedOption1.attr('data-pnp'),
    	$stock = $selectedOption1.attr('data-stock');             
        
        if($('.live-tv-detail-main .MLAVariations').length > 0){
        	 if($stock == "true" || $stock == true){
             	$('#enablebutton').prop( "disabled", false );
             	$('.live-tv-add-to-cart-form #showActivateQBModel').prop( "disabled", false );
             	$('.live-tv-add-to-cart-form .enablebutton').prop( "disabled", false );
             	$('.live-tv-add-to-cart-form .quick-buy-signin').prop( "disabled", false );             	             	
             	$('.live-tv-add-to-cart-form .enablebutton').removeClass("disabled");
             	$('.live-tv-add-to-cart-form .quick-buy-signin').removeClass("disabled");
             	$('.MLAQuantityBox').show();
             }
        	 else if($('#select-second-variation').val() == null || $('#select-second-variation').val() == "null"){
        		 $('.MLAQuantityBox').show();
        	 }
             else{
            	$('.live-tv-add-to-cart-form #showActivateQBModel').prop( "disabled", false ); 
             	$('.live-tv-add-to-cart-form .enablebutton').prop( "disabled", true );
             	$('.live-tv-add-to-cart-form .enablebutton').addClass("disabled");
             	$('.live-tv-add-to-cart-form .quick-buy-signin').prop( "disabled", true );
             	$('.live-tv-add-to-cart-form .quick-buy-signin').addClass("disabled");
             	$('.MLAQuantityBox').hide();
             }
        	 
        }
        
        if((secondVariation != "null") || (secondVariation != null) ){
        	$('#select-second-variation').removeClass('border');
        	//updateMLAImage();
        	nonUpdateOptionChangeFlag = true;
        	updateLiveTVDetailPageTile($tile);
		}
        
        // Script to add "selected" attribute for selected option in the dropdown 
        $("option[value=" + secondVariation + "]", this)
		  .attr("selected", true).siblings()
		  .removeAttr("selected")
		  
	  	if($('.live-tv-add-to-cart-form a.quick-buy-login-class').length > 0){
	  		$('.live-tv-add-to-cart-form a.quick-buy-login-class').attr('data-pid',secondVariation);
	  		$('.live-tv-add-to-cart-form a.quick-buy-login-class').attr('data-auctioncode',secondVariationAuctionCode);
	  		$('.live-tv-add-to-cart-form a.quick-buy-login-class').attr('data-itempnp',$selectedSecondPnP);
	  	}
		  
        
    });
    
    //$productContainer.find('.product-selection .input-select').trigger('change');

    // init add to cart / bid button
    $productContainer.on('submit', '.live-tv-add-to-cart-form', function (e) {
    	
    	var $selectSecondvariation = $productContainer.find('.second-variation .input-select'),
        $selectedSecondOption = $selectSecondvariation.find('option:selected').val(),
    	$selectedSecondAuctionCode = $selectSecondvariation.find('option:selected').attr('data-auctioncode'),
    	$selectedSecondPnP = $selectSecondvariation.find('option:selected').attr('data-pnp');
    	
    	if($('.live-tv-detail-main .MLAVariations').length > 0){
	    	if((currentSelecedOption != "null" && currentSelecedOption != null) && (secondVariation != "null" && secondVariation != null)){
	    		$('#live-tv-product-options').removeClass('border');
	    		$('#select-second-variation').removeClass('border');
	    	}
	    	else{
	    		if((currentSelecedOption == "null") || (currentSelecedOption == null)){
	    			$('#live-tv-product-options').addClass('border');
	    			return false;
	    		}
	    		if((secondVariation == "null") || (secondVariation == null) ){
	    			$('#select-second-variation').addClass('border');
	    			return false;
	    		}
	    	}
    	}
    	    	
    	if($('.live-tv-detail-main .groupVariations').length > 0){
    		if($('.product-selection .input-select').length > 0){
	    		if((currentSelecedOption == "null") || (currentSelecedOption == null)){
	    			$('#live-tv-product-options').addClass('border');
	    			return false;
	    		}
	    		else{
	    			$('#live-tv-product-options').removeClass('border');
	    		}
    		}
    	}
    	
    	if($('.live-tv-detail-main .MLAVariations').length > 0){
    		var StockCode = secondVariation,
    			AuctionCode = $selectedSecondAuctionCode;
    	}
    	else if($('.live-tv-detail-main .groupVariations').length > 0){
    		var $selectField = $productContainer.find('.product-selection .input-select'),
             $selectedOption = $selectField.find('option:selected'),
             StockCode = $selectField.length !== 0 ? $selectField.val() : $tile.find('.product-id').data('value'),
    		 AuctionCode = $selectField.length !== 0 ? $selectedOption.data('auctioncode') : $tile.find('.tile-inner').data('auctioncode');
    	}
    	else{
    		var StockCode = $tile.find('.product-id').data('value'),
    			AuctionCode = $tile.find('.tile-inner').data('auctioncode');
    	}

    	var $form = $(this),            
            pids = StockCode,
    		auctionCodes = AuctionCode,
            quantity = $productContainer.find('.quantity-selection .input-select').val();

        $form.append('<input type="hidden" name="pids" value="' + pids + '"/>');
        $form.append('<input type="hidden" name="quantity" value="' + quantity + '"/>');
        $form.append('<input type="hidden" name="auctionCodes" value="' + auctionCodes + '"/>');
    });

    // mark product container as initialized
    $productContainer.addClass('initialized');
}

function updateMissedAuctionProducts(url, channel, timeslot, tvshowname, showDate, tvshowpresenter) {
    progress.show();
    ajax.load({
        url: url,
        target: '.missed-auctions-main .missed-auction-items',
        data: {
            channel: channel,
            timeslot: timeslot,
            tvshowname: tvshowname,
            showdate: showDate,
            tvshowpresenter: tvshowpresenter
        },
        callback: function () {
            progress.hide();
            util.imageAdjust();
        }       
    });
}

function initMissedAuctionsPage() {
    var $productContainer = $('.missed-auctions-main:not(.initialized)'),
        $channelFilterSelect,
        $channelFilterButtonTJC,
        $channelFilterButtonTJCChoice,
        $tvShows,
        selectedChannel,
        url;

    if (!$productContainer.length) {
        return;
    }

    $channelFilterSelect = $productContainer.find('.wrapper-links');
    $channelFilterButtonTJC = $channelFilterSelect.find('.wrapper-links-btn.tjc');
    $channelFilterButtonTJCChoice = $channelFilterSelect.find('.wrapper-links-btn.tjc-choice');
    $tvShows = $productContainer.find('#carousel-recommendations');
    url = $productContainer.data('url');
    selectedChannel = $('.wrapper-links .active').data('channel');

    $channelFilterButtonTJC.on('click', function (event) {
        event.preventDefault();
        selectedChannel = 'tjc';
        window.location.href = util.appendParamToURL($channelFilterSelect.data('url'), 'channel',
            selectedChannel);
    });

    $channelFilterButtonTJCChoice.on('click', function (event) {
        event.preventDefault();
        selectedChannel = 'tjc-choice';
        window.location.href = util.appendParamToURL($channelFilterSelect.data('url'), 'channel',
            selectedChannel);
    });

    $tvShows.on('click', 'li', function (event) {
        event.preventDefault();
        var $timeSlot = $(this),
            currentTimeSlot = $timeSlot.data('timeslot'),
            tvShowName = $timeSlot.data('name'),
            channel = selectedChannel,
            showDate = $timeSlot.data('showdate'),
            tvshowpresenter = $timeSlot.data('tvshowpresenter');

        $tvShows.find('li.active').removeClass('active');
        $timeSlot.addClass('active');
        carouselClickTimeout = setTimeout(function () {
            updateMissedAuctionProducts(url, channel, currentTimeSlot, tvShowName, showDate, tvshowpresenter);
        }, 1500);
    });

    $productContainer.on('click', '.missed-auctions-add-to-cart-btn', function (event) {
        event.preventDefault();
        var currenttile = $(this),
        	$maTile = currenttile.closest('.ma-product-tile'),
        	$mlaVariations = $maTile.find('.MLAVariations'),
        	$groupVariations = $maTile.find('.groupVariations'),
	        $selectFirstvariation = $maTile.find('.product-selection .input-select'),
	        $selectedFirstOption = $selectFirstvariation.find('option:selected').val(),
	        $selectedFirstAuctionCode = $selectFirstvariation.find('option:selected').attr('data-auctioncode'),
	        $selectedfirstPnP = $selectFirstvariation.find('option:selected').attr('data-pnp'),
	        $selectSecondvariation = $maTile.find('.second-variation .input-select'),
	        $selectedSecondOption = $selectSecondvariation.find('option:selected').val(),
        	$selectedSecondAuctionCode = $selectSecondvariation.find('option:selected').attr('data-auctioncode'),
        	$selectedSecondPnP = $selectSecondvariation.find('option:selected').attr('data-pnp'),
        	budgetPayContainer = $maTile.find('.budgetpay');
        
        if($mlaVariations.length > 0){
	    	if(($selectedFirstOption != "null" && $selectedFirstOption != null) && ($selectedSecondOption != "null" && $selectedSecondOption != null)){
	    		$('.product-selection .input-select').removeClass('border');
	    		$('.second-variation .input-select').removeClass('border');
	    	}
	    	else{
	    		if(($selectedFirstOption == "null") || ($selectedFirstOption == null)){
	    			$maTile.find('.product-selection .input-select').addClass('border');
	    			return false;
	    		}
	    		if(($selectedSecondOption == "null") || ($selectedSecondOption == null) ){
	    			$maTile.find('.second-variation .input-select').addClass('border');
	    			return false;
	    		}	    		
	    	}
    	}
        
        if($groupVariations.length > 0){
        	if($selectFirstvariation.length > 0){
        		if(($selectedFirstOption == "null") || ($selectedFirstOption == null) ){
        			$maTile.find('.product-selection .input-select').addClass('border'); 			
        		}
        		else{
        			$maTile.find('.product-selection .input-select').removeClass('border');       			
        		}
        	}
    	}
        
        	
    	if($mlaVariations.length > 0){
    		var stockCode = $selectedSecondOption,
    			auctionCode = $selectedSecondAuctionCode,
    			pnPValue = $selectedSecondPnP;
    	}
    	
    	if($groupVariations.length > 0){
    		var stockCode = $selectFirstvariation.length !== 0 ? $selectedFirstOption : $maTile.data('productid'),
				auctionCode = $selectFirstvariation.length !== 0 ? $selectedFirstAuctionCode : $maTile.data('auctioncode'),
    			pnPValue = $selectFirstvariation.length !== 0 ? $selectedfirstPnP : $maTile.data('pnp');		
    	}
    	
        var pids = stockCode,
            auctionCodes = auctionCode,
            budgetPayContainer = budgetPayContainer,
        	itemPnP = pnPValue,
            addToCartURL;

        if (budgetPayContainer.length > 0) {
            addToCartURL = util.appendParamsToUrl(currenttile.attr('href'), {
                pids: pids,
                channel: $('.wrapper-links .active').data('channel'),
                auctionCodes: auctionCodes,
                installments: budgetPayContainer.data('installments'),
                itemPnP : itemPnP

            });
        } else {
            addToCartURL = util.appendParamsToUrl(currenttile.attr('href'), {
                pids: pids,
                channel: $('.wrapper-links .active').data('channel'),
                auctionCodes: auctionCodes,
                itemPnP : itemPnP
            });
        }

        if (!pids || !auctionCodes) {
            return;
        }
        // redirect to the add to cart functionality
        window.location.href = addToCartURL;
    });
    
    $productContainer.on('change', '.product-selection .input-select', function () {
        var $this = $(this),
            $tile = $this.parents('.ma-product-tile'),
            $selectField = $tile.find('.product-selection .input-select'),
            $selectedOption = $selectField.find('option:selected'),
            $selectedFirstOption = $selectedOption.find('option:selected').val(),
	        $selectedFirstAuctionCode = $selectedOption.find('option:selected').attr('data-auctioncode'),
	        $selectSecondvariation = $tile.find('.second-variation .input-select'),
	        $selectedSecondOption = $selectSecondvariation.find('option:selected').val(),
        	$selectedSecondAuctionCode = $selectSecondvariation.find('option:selected').attr('data-auctioncode');
        	

        $tile.find('.product-image img:not(.placeholder-image)')
            .attr('src', $selectedOption.data('image'))
            .attr('title', $selectedOption.data('name'))
            .attr('title', $selectedOption.data('name'));
        $tile.find('.product-name').text($selectedOption.data('name'));

        //$tile.find('.missed-auctions-add-to-cart-btn').toggleClass('disabled', !$selectedOption.data('auctioncode') || !$selectedOption.val());
        
        if(( $selectedFirstOption != "null") || ($selectedFirstOption != null)){
        	$('.product-selection .input-select').removeClass('border');
    		$('.second-variation .input-select').removeClass('border');
		}
        
        if($tile.find('.groupVariations').length > 0){
        	if($selectField.length > 0){
	        	if($selectedOption.data('stock') == false || $selectedOption.data('stock') == "false"){
	        		$tile.find('.missed-auctions-add-to-cart-btn').css( "pointer-events", "none" );
	        		$tile.find('.missed-auctions-add-to-cart-btn').addClass("disabled");
	        	}
	        	else{
	        		$tile.find('.missed-auctions-add-to-cart-btn').css( "pointer-events", "auto" );
	        		$tile.find('.missed-auctions-add-to-cart-btn').removeClass("disabled");
	        	}
        	}
        }
        
        if($('.ma-product-tile .MLAVariations').length > 0){
        	var selectValue = $tile.find('.product-selection .input-select option:selected').val();
        	var mlaAuctionCode = $tile.find('.product-selection .input-select option:selected').attr('data-auctioncode');
            var mlaSecondVariationID = $tile.find('#select-second-variation');
                                    
            if(selectValue == null || selectValue == "null") {
            	//$tile.find('#select-second-variation').find('option').hide();
            	$tile.find( "#select-second-variation" ).css( "pointer-events", "none" );
            }
            else {
            	$tile.find( "#select-second-variation" ).css( "pointer-events", "auto" );
            	$tile.find("#select-second-variation").children().each(function(){
                	if($(this).attr('data-group') == mlaAuctionCode){
           			 	$(this).prop('disabled',false);
           			 	$(this).css('display','block');
	           	    }
	           	    else{
	           	    	$(this).prop('disabled',true);
	           	    	$(this).css('display','none');
	           	    }
                });
            }
            //mlaSecondVariationID.val("null");
            $tile.find('#select-second-variation').prepend('<option data-group="0" value="null" selected="selected" disabled>' + "Select" + '</option>');
        }

    });
    
    $productContainer.on('change', '.second-variation .input-select', function () {
        secondVariation = $(this).val();
        var $this = $(this),
        $tile = $this.parents('.ma-product-tile'),
        $selectField = $tile.find('.second-variation .input-select'),
        $stock = $selectField.find('option:selected'),
        $selectField1 = $productContainer.find('.second-variation .input-select'),
        $selectedOption1 = $selectField1.find('option:selected'),
        secondVariationAuctionCode = $selectedOption1.attr('data-auctioncode');
        
        if(( $stock.val() != "null") || ($stock.val() != null)){
        	$('.product-selection .input-select').removeClass('border');
    		$('.second-variation .input-select').removeClass('border');
    		$tile.find('.second-variation .input-select').removeClass('border');
    		
    		var mlaVarientID = $stock.val(),
    		url = Urls.mlaproductimage,
    		data = {mlaVarientID : mlaVarientID };

			if(mlaVarientID == undefined || mlaVarientID == "undefined" || mlaVarientID == null || mlaVarientID == "null"){
    			mlaVarientID = $('.second-variation .input-select option:nth-child(2)').val();
    		}			
    		
    		$.ajax({
    	        url: url + "?mlaProductID=" + mlaVarientID,
    	        data : data,
    	        type: 'GET',
    	        success: function (response) {
    	        	$tile.find('.updateMLAImage').html(response);
    	        	util.imageAdjust();
    	        }
    		});
		}
        else{
        	$tile.find('.second-variation .input-select').addClass('border');
        }
                
        if($tile.find('.MLAVariations').length > 0){
        	if($stock.attr('data-stock') == false || $stock.attr('data-stock') == "false"){
        		$tile.find('.missed-auctions-add-to-cart-btn').css( "pointer-events", "none" );
        		$tile.find('.missed-auctions-add-to-cart-btn').addClass("disabled");
        	}
        	else{
        		$tile.find('.missed-auctions-add-to-cart-btn').css( "pointer-events", "auto" );
        		$tile.find('.missed-auctions-add-to-cart-btn').removeClass("disabled");
        	}
        }
        
    });

    $productContainer.addClass('initialized').find('.product-selection .input-select').trigger('change');
}

function initMissedAuctionsCarouselButtons() {
    $('.missed-auctions-carousel-innerwraper .product-tile-nav-arrows').on('click', '.nav-prev.button', function () {
        clearTimeout(carouselClickTimeout);
        var $selectedShow = $('.missed-auctions-carousel-innerwraper .carousel-container-inner')
            .find('.grid-tile.active'),
            previousShow = $selectedShow.prev();

        previousShow.click();

        $('.missed-auctions-carousel-innerwraper .carousel-container-inner .nav-prev').click();

    });
    $('.missed-auctions-carousel-innerwraper .product-tile-nav-arrows').on('click', '.nav-next.button', function () {
        clearTimeout(carouselClickTimeout);
        var $selectedShow = $('.missed-auctions-carousel-innerwraper .carousel-container-inner')
            .find('.grid-tile.active'),
            nextShow = $selectedShow.next();

        nextShow.click();

        $('.missed-auctions-carousel-innerwraper .carousel-container-inner .nav-next').click();
    });
}

function selectVariations(){	
	if($('#live-tv-detail-main').length > 0){
		var selectValue = $('#live-tv-product-options').val();
		var mlaAuctionCode = $('#live-tv-product-options option:selected').attr('data-auctioncode');
	    var mlaSecondVariationID = $('#select-second-variation');
	    
	    if(selectValue == null || selectValue == "null") {
	    	//$('#select-second-variation').find('option').hide();
	    	$( "#select-second-variation" ).css( "pointer-events", "none" );
	    }
	    else {
	    	$( "#select-second-variation" ).css( "pointer-events", "auto" );
	        $("#select-second-variation").children().each(function(){
	    		if($(this).attr('data-group') == mlaAuctionCode){
	    			if(browsername != "Safari"){
	    				$(this).show();
	    			}else{
	    				$(this).append();
	    			}
	    	    }
	    	    else{
	    	        if(browsername != "Safari"){
	    	    		$(this).hide();
	    			}else{
	    				$(this).detach();
	    			}
	    	    }
	    	});
	    }   
	    //mlaSecondVariationID.val(null);
	    $('#select-second-variation').prepend('<option data-group="0" value="null" selected="selected">' + "Select" + '</option>');
	}	
}

function increaseDecreaseQuantity(){
	
	var totalCount = $('.variations #totalCount').val()
	
	$('#home-add').off('click').on('click', function(e){
	  var homeqty=$('#live-tv-product-quantity').val();
	  homeqty++;
	  if(homeqty<=totalCount){
		  document.getElementById('live-tv-product-quantity').value=homeqty;
	  }
	});
	$('#home-sub').off('click').on('click', function(e){
	  var homeqty=$('#live-tv-product-quantity').val();
	  homeqty--;
	  if(homeqty>0){
		  document.getElementById('live-tv-product-quantity').value=homeqty;
	  }
	});	
	
	 $("#w-tv").off('click').on('click', function(e){
		 	if($homePageLiveTVStreamVideoInitialized == false){
		 		homePageLiveTVStreamVideo();
		 	}else{
		 		$("#v-tv-Content").css('background-color','black');
				$("#v-tv").css('height','170px');
				if (window.innerWidth >= 1024) {
					$('#tv-progress-loader').css({'top':'10rem','left':'50%'});
				}else if (window.innerWidth >= 768 && window.innerWidth <= 1023) {
					$('#tv-progress-loader').css({'top':'9rem','left':'11rem'});
				}else {
					$('#tv-progress-loader').css({'top':'9rem','left':'50%'});
				}
				$('#tv-progress-loader').show();
				VeygoIndex.init();
			 	setTimeout(function(){
					$("#v-tv-Content").css('background-color','transparent');
					$("#v-tv").css('height','auto');
					$("video").prop('muted', false);
					$('#tv-progress-loader').hide();
			 	}, 2000);
		 	}
	        $("#w-tv").hide();
	        $("#w-si").show();
	        $("#v-tv").show();
	        $("#v-si").hide();
	        $("video").prop('muted', false);
	        $("video").trigger('play');
	        $(".currently-on-air").addClass('video');
	    });
	    $("#w-si").off('click').on('click', function(e){
	        $("video").prop('muted', true);
	        $("video").trigger('pause');
	        $("#w-si").hide();
	        $("#w-tv").show();
	        $("#v-si").show();
	        $("#v-tv").hide();
	        //$('#v-tv-Content').empty();
	        //$homePageLiveTVStreamVideoInitialized=false;
	        //$("#v-tv-Content").css('background-color','black');
            //$("#v-tv").css('height','170px');
            var stream1 = {type: 'hls',uri: playerContainer.getAttribute('')},options = { autoplay: true };
            player.setSource(stream1);
	        $(".currently-on-air").removeClass('video');
	    });
	    
		$( document ).ajaxComplete(function() {
			if($("#v-si").is(":visible")){
			    $('#v-tv').hide();
			}
			});
		
	    if($('.live-tv-detail-main .groupVariations').length > 0){
	    	if($('.product-selection .input-select').length > 0){
	    		if($('.product-selection .input-select option:selected').attr('data-stock') == "false" || $('.product-selection .input-select option:selected').attr('data-stock') == false ){
	    			$('.live-tv-add-to-cart-form #showActivateQBModel').prop( "disabled", false ); 
	             	$('.live-tv-add-to-cart-form .enablebutton').prop( "disabled", true );
	             	$('.live-tv-add-to-cart-form .enablebutton').addClass("disabled");
	             	$('.live-tv-add-to-cart-form .quick-buy-signin').prop( "disabled", true );
	             	$('.live-tv-add-to-cart-form .quick-buy-signin').addClass("disabled");
	    		}
	    		else{
	    			$('.live-tv-add-to-cart-form #showActivateQBModel').prop( "disabled", false );
	             	$('.live-tv-add-to-cart-form .enablebutton').prop( "disabled", false );
	             	$('.live-tv-add-to-cart-form .quick-buy-signin').prop( "disabled", false );             	             	
	             	$('.live-tv-add-to-cart-form .enablebutton').removeClass("disabled");
	             	$('.live-tv-add-to-cart-form .quick-buy-signin').removeClass("disabled");
	    		}
	    	}
	    }
}

function MLAvariantCheck(){
	
     function productContainerCheck($parentClass , $firstOptionValue){
		if($firstOptionValue == null || $firstOptionValue == "null"){
			$parentClass.find('.product-selection .input-select').css('border','3px solid #c20000');
		}else{
			$parentClass.find('.product-selection .input-select').css('border','1px solid #bbb');
		 }
     }
    
	$('.second-variation').on('click',function(){
		var $this = $(this),
			$tile = $this.parents('.MLAVariations'),
	        $selectField = $tile.find('.product-selection .input-select'),
	        $stock = $selectField.find('option:selected');
			productContainerCheck($tile , $stock.val());
	});   
	        
   $('#select-second-variation').on('change',function(){
	   var $this = $(this),
	   	   $tile = $this.parents('.MLAVariations'),
           $selectField = $tile.find('.product-selection .input-select'),
           $stock = $selectField.find('option:selected');
	   	   productContainerCheck($tile , $stock.val());
	   	   
	   	  var selectedSecondValue = $(this).find('option:selected').val(),
  	        selectedSecondAuctionCode = $(this).find('option:selected').attr('data-auctioncode'),      	
        	selectedSecondPNP = $(this).find('option:selected').attr('data-pnp');
        	
          if(selectedSecondValue !== 'null'){        	
        	if((selectedSecondAuctionCode == null) || (selectedSecondAuctionCode == "") || (selectedSecondAuctionCode == 'undefined' || typeof selectedSecondAuctionCode === 'undefined' )  ||  (selectedSecondPNP == null) || (selectedSecondPNP == "") || (selectedSecondPNP == 'undefined' || typeof selectedSecondPNP === 'undefined' )){
	         $('.live-tv-detail-main').addClass('code-not-available');      
            }else{            
             $('.live-tv-detail-main').removeClass('code-not-available');
            }
           }       
	   	   
	}); 

}
	

/* Module Exports */
/* ************** */
exports.init = function () {
    initLiveTVLandingPage();
    initLiveTVDetailPage();
    initMissedAuctionsPage();
    initMissedAuctionsCarouselButtons();
    homeRecentlyOnAir();
    selectVariations();
    increaseDecreaseQuantity();
    MLAvariantCheck();
};
