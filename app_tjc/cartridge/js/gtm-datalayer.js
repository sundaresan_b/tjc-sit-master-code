'use strict';

var util = require('./util');

function init() {
    /**
     * Event listener for GTM dataLayer when an item is added to the cart. The event listener is listening for event triggered from '#add-to-cart'
     */
    $(document).on('click', '#add-to-cart', function () {
    	try {
	        var $parents = $(this).parents('.gtmListener');
	        var gtmData = $parents.data("gtm");
	        var quantity = $parents.find('#Quantity').val() || '';
	        /*eslint-disable no-undef */
	        window.dataLayer.push({
	            'event': 'addToCart',
	            'ecommerce': {
	                'currencyCode': '' + gtmData.currency,
	                'add': { // 'add' actionFieldObject measures.
	                    'products': [{ //  adding a product to a shopping cart.
	                        'name': '' + gtmData.name,
	                        'id': '' + gtmData.id,
	                        'price': '' + gtmData.price,
	                        'brand': '' + gtmData.brand,
	                        'category': '' + gtmData.category,
	                        'variant': '' + gtmData.variant,
	                        'quantity': parseInt(quantity),
	                        'metric1': parseFloat(gtmData.price * parseInt(quantity)).toFixed(2)
	                    }]
	                }
	            }
	        });
	        /*eslint-enable no-undef */
    	} catch (b) {
            console.log(b);
        }
    });

    /**
     * Pushing given data from gtmData object to dataLayer when a product is removed from the cart.
     * @param {Object} gtmData - Collected data from the product that has been removed.
     */
    function pushInfoToDataLayer(gtmData) {
    	try {
	    	/*eslint-disable no-undef */
	        window.dataLayer.push({
	            'event': 'removeFromCart',
	            'ecommerce': {
	                'remove': { // 'remove' actionFieldObject measures.
	                    'products': [{ //  removing a product to a shopping cart.
	                        'name': '' + gtmData.name,
	                        'id': '' + gtmData.id,
	                        'price': '' + gtmData.price,
	                        'brand': '' + gtmData.brand,
	                        'category': '' + gtmData.category,
	                        'variant': '' + gtmData.variant,
	                        'quantity': parseInt(gtmData.quantity)
	                    }]
	                }
	            }
	        });
	        /*eslint-enable no-undef */
    	} catch (b) {
            console.log(b);
        }
    }

    /**
     * Event for GTM dataLayer when an item is removed from the cart. The event listener is listening for event triggered from '.button-remove-item'.
     */
    $(document).on('click', '.button-remove-item', function () {
        try {
	        var $parents = $(this).parents('.cart-row');
	        var gtmData = $parents.data("gtm");
	        /*eslint-disable no-undef */
	        window.dataLayer.push({
	        	'event': 'removeFromCart',
	            'ecommerce': {
	                'remove': { // 'remove' actionFieldObject measures.
	                    'products': [{ //  removing a product to a shopping cart.
	                        'name': '' + gtmData.name,
	                        'id': '' + gtmData.id,
	                        'price': '' + gtmData.price,
	                        'brand': '' + gtmData.brand,
	                        'category': '' + gtmData.category,
	                        'variant': '' + gtmData.variant,
	                        'quantity': parseInt(gtmData.quantity),
	                        'metric1': '-'+parseFloat(gtmData.price * parseInt(gtmData.quantity)).toFixed(2)
	                    }]
	                }
	            }
	        });
	        /*eslint-enable no-undef */
    	} catch (b) {
            console.log(b);
        }
    });

    $(document).on('click', '.mini-cart-item-remove-button', function () {
        var gtmData = $(this).parents('.mini-cart-product').data("gtm");
        pushInfoToDataLayer(gtmData); // it is already in it. Check
    });

    /**
     * Event for GTM dataLayer when an item has been clicked. The event listener is listening when a 'Quickview' button is clicked or the grid of the product.
     */
    $(document).on('click', '.product-link, .product-details-button, .quickview ', function () {
    	try {
	        var gtmData = $(this).parents('.grid-tile').data("gtm");
	        /*eslint-disable no-undef */
	        window.dataLayer.push({
	            'event': 'productClick',
	            'ecommerce': {
	                'click': {
	                    'actionField': {
	                        'list': 'Search Results'
	                    }, // Optional list property.
	                    'products': [{
	                        'name': gtmData.name, // Name or ID is required.
	                        'id': gtmData.id,
	                        'price': gtmData.price,
	                        'brand': gtmData.brand,
	                        'category': gtmData.category,
	                        'variant': gtmData.variant,
	                        'position': gtmData.position
	                    }]
	                }
	            },
	            'eventCallback': function () {
	                //document.location = gtmData.attr('href');
	            	console.log('callback triggered: '+gtmData);
	            }
	        });
	        /*eslint-enable no-undef */
    	} catch (b) {
            console.log(b);
        }
    });

    $(document).on('pdp:recommendationsLoaded', function () {
    	try {
	        var recommendedProductsInfo = Array.from($('.gtm-info')).map(function (el) {
	            return JSON.parse(el.value);
	        });
	
	        var con = console;
	        con.log(recommendedProductsInfo);
	        for (var i = 0; i < recommendedProductsInfo.length; i++) {
	            /*eslint-disable no-undef */
	            if (window.dataLayer && window.dataLayer.length > 1) {
	                window.dataLayer[window.dataLayer.length - 1].ecommerce.impressions.push({
	                    'id': recommendedProductsInfo[i].id,
	                    'name': recommendedProductsInfo[i].name,
	                    'price': recommendedProductsInfo[i].price,
	                    'brand': recommendedProductsInfo[i].brand,
	                    'category': recommendedProductsInfo[i].category,
	                    'position': recommendedProductsInfo[i].position
	                });
	            }
	        }
	        /*eslint-enable no-undef */
    	} catch (b) {
            console.log(b);
        }
    });
}

/**
 * Function for pushing data into dataLayer. The function is called in index.js which is used in PDP decorator.
 */
function pushDataToDataLayerForPDP() {
	try {
		var gtmData = $('.secondary-col').data('gtm');
	    /*eslint-disable no-undef */
	    window.dataLayer.push({
	        'ecommerce': {
	            'impressions' : [],
	            'detail': {
	                'actionField': {
	                    'list': gtmData.category
	                }, // 'detail' actions have an optional list property.
	                'products': [{
	                    'name': gtmData.name, // Name or ID is required.
	                    'id': gtmData.id,
	                    'price': gtmData.price,
	                    'brand': gtmData.brand,
	                    'category': gtmData.category,
	                    'variant': gtmData.variant
	                }]
	            }
	        }
	        /*eslint-enable no-undef */
	    });
	} catch (b) {
        console.log(b);
    }
}

/**
 * Function for pushing data into dataLayer. The function is called in index.js which is used in PLP decorator.
 */
function splitToChunks(array, parts) {
    var result = [];
    for (var i = parts; i > 0; i--) {
        result.push(array.splice(0, Math.ceil(array.length / i)));
    }
    return result;
}
var dataListMst = [];
function pushDataToDataLayerForPLP() {
	var googleTagManagerID = document.getElementById('gtm_id').dataset.gtm;
    
    var tiles = $('.grid-tile');
    var currency = '';
    if (tiles.length > 0) {
        var infoObject = JSON.parse(tiles[0].getAttribute('data-gtm'));
        if (infoObject) {
            currency = infoObject.currency;
        }
    }
    
    var dataList = [];
    var splitLimit = 40;
    if(tiles.length>=splitLimit){
	    var splitLength = parseInt(tiles.length/splitLimit);
	    var splitTiles = splitToChunks(tiles, splitLength);
	    $.each(splitTiles, function(index, value){
	    	//delete window.dataLayer.ecommerce;
	    	var products = [];
	    	for (var i = 0; i < value.length; i++) {
	            var filteredInfo = JSON.parse(value[i].getAttribute('data-gtm'));
	            delete filteredInfo.currency;
	            products.push(filteredInfo);
	        }
	        /*eslint-disable no-undef */
	    	dataList.push({
	    		'ecommerce': {
	                'currencyCode': currency, // Local currency is optional.
	                'impressions': products
	            }
	        });
	        /*eslint-enable no-undef */
	    	//sendDataToGTM(window,document,'script','dataLayer',googleTagManagerID);
	    }); 
	    $.each(dataList,function(index,value){
	    	window.dataLayer.push(value);
	    	//console.log(dataLayer);
	    });
    }else{
    	var products = [];
    	for (var i = 0; i < tiles.length; i++) {
            var filteredInfo = JSON.parse(tiles[i].getAttribute('data-gtm'));
            delete filteredInfo.currency;
            products.push(filteredInfo);
        }
        /*eslint-disable no-undef */
    	dataList.push({
    		'ecommerce': {
                'currencyCode': currency, // Local currency is optional.
                'impressions': products
            }
        });
        /*eslint-enable no-undef */
        $.each(dataList,function(index,value){
        	window.dataLayer.push(value);
        	//console.log(dataLayer);
        });
    }
}

//sends data to the gtm after each step is pushed to the datalayer
function sendDataToGTM(w,d,s,l,i) {
 w[l] = w[l] || [];
 w[l].push({
     'gtm.start': new Date().getTime(),
     event:'gtm.js'
 });
 var f = d.getElementsByTagName(s)[0],
 j = d.createElement(s),
 dl = l !== 'dataLayer' ? '&l=' + l : '';
 j.async = true;
 j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
 f.parentNode.insertBefore(j,f);
}

function pushDataFromPurchase() {
	try {
		var $gtmDataHolder = $('#gtmDataHolder');
	    var purchase = $gtmDataHolder.data('gtmpurchase');
	    var products = $gtmDataHolder.data('gtmproducts');
	    var orderCategory = $gtmDataHolder.data('gtmordercategory');
	    var googleTagManagerID = document.getElementById('gtm_id').dataset.gtm;
	
	    /*eslint-disable no-undef */
	    window.dataLayer.push({
	    	'event':'purchase',
	    	'ecommerce': {
	            'purchase': {
	                'actionField': purchase,
	                'products': products
	            }
	        }
	    });
	    sendDataToGTM(window,document,'script','dataLayer',googleTagManagerID);
	    /*eslint-enable no-undef */
	    
	    /*AWIN*/
	    pushToAWINDataLayer(googleTagManagerID, purchase, products, orderCategory);
	    /*AWIN*/
	    
	    /*TurnTo Feed*/
	    pushToTurnToLayer(googleTagManagerID, purchase, products, orderCategory);
	    /*TurnTo Feed*/
	} catch (b) {
        console.log(b);
    }
}

function pushToAWINDataLayer(googleTagManagerID, purchase, products, orderCategory){
	try {
		var productCoupons = (purchase.coupon)?purchase.coupon+",":"";
		$(products).each(function(index, value){
			if(value.coupon){
				productCoupons += value.coupon;
				if(index+1 < products.length && productCoupons != ""){
					productCoupons += ",";
				}
			}
		});
		window.dataLayer.push({
	    	'transactionTotal':''+purchase.revenue,
	    	'transactionCurrency':'GBP',
	    	'transactionID':''+purchase.id,
	    	'transactionParts':orderCategory+':'+purchase.revenue,
	    	'transactionPromoCode':''+productCoupons,
	    	'transactionProducts': products,
	    	'transactionCustomerGroup': orderCategory
	    });
		window.dataLayer[0].transactionProducts = products;
	    sendDataToGTM(window,document,'script','dataLayer',googleTagManagerID);
	} catch (b) {
		console.log(b);
	}
}

function pushToTurnToLayer(googleTagManagerID, purchase, products, orderCategory){
	try {
		var orderCustomer = $('#gtmDataHolder').data("gtmcustomer");
		var temp = {};
		window.TurnToOrderFeed = {
			orderId:''+purchase.id,
			email:''+orderCustomer.email,
			postalCode:''+orderCustomer.shipping.postalCode,
			firstName:''+orderCustomer.shipping.firstName,
			lastName:''+orderCustomer.shipping.lastName,
			items:[]
		};
		$(products).each(function(index,value){
			temp = {
				/*title:''+value.name,*/
				sku:''+value.ratingid
			};
			window.TurnToOrderFeed.items.push(temp);
		});
		TurnToCmd('feed.send', TurnToOrderFeed);
		console.log('TurnTo order feed send', TurnToOrderFeed);
	} catch (b) {
		console.error(b);
	}
}

// used primarily in the checkout to send each stage of checkout to gtm
// JSON is rendered in gtm_chekcout.isml
function pushCheckoutStepToDataLayer (JSON) {
    var googleTagManagerID = document.getElementById('gtm_id').dataset.gtm;
    /*eslint-disable no-undef */
    window.dataLayer.push(JSON);
    sendDataToGTM(window,document,'script','dataLayer',googleTagManagerID);
}

// grabs and parses the json data to pass into the gtm
function getGTMData () {
    var gtmJSON = document.getElementById('gtm_checkout').dataset.gtm;
    var eventCallback = document.getElementById('gtm_eventCallback').dataset.eventcallback;
    // we can't pass double quotes into a data variable
    gtmJSON = gtmJSON.replace(/'/g,'"');
    gtmJSON = JSON.parse(gtmJSON);
    // we cannot store functions in json, so convert the callback function accordingly
    if (eventCallback !== '') {
        gtmJSON.ecommerce.eventCallback = function () {
            document.location = eventCallback;
        };
    }
    return gtmJSON;
}

// only available on the final step of the checkout process
function getPayNowGTMData () {
    var gtmJSON = document.getElementById('gtm_checkout-paynow').dataset.gtm;
    // we can't pass double quotes into a data variable
    gtmJSON = gtmJSON.replace(/'/g,'"');
    gtmJSON = JSON.parse(gtmJSON);
    return gtmJSON;
}

module.exports = {
    init: init,
    pushDataToDataLayerForPDP: pushDataToDataLayerForPDP,
    pushDataToDataLayerForPLP: pushDataToDataLayerForPLP,
    pushDataFromPurchase: pushDataFromPurchase,
    pushCheckoutStepToDataLayer: pushCheckoutStepToDataLayer,
    getGTMData: getGTMData,
    getPayNowGTMData: getPayNowGTMData
};