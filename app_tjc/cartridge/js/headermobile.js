'use strict';

var util = require('./util');

function updateMinicartSize() {
    var screenWidth = $(window).width(),
        breakpointScreen = 767;

    if (screenWidth <= breakpointScreen) {
        $('.coremenu-mobile.only-for-mobile .mini-cart-content').width(screenWidth);
    }
}

function headermobile() {
    updateMinicartSize();

    $('.coremenu-mobile.only-for-mobile').on('click', '.js-menuflyout-trigger', function (e) {
        e.preventDefault();
        var $content = $('.coremenu-mobile.only-for-mobile').
            find('.mini-cart-content.js-menuflyout-content');

        if (!$content.length) {
            return {};
        }

        $(this).toggleClass('hover');
        $content.toggle();

    });

    $('.topbar-mobile-bagwhite-link').on('click', function (e) {
        e.preventDefault();
        util.scrollBrowser(0);
        var $content = $('.coremenu-mobile.only-for-mobile')
            .find('.mini-cart-content.js-menuflyout-content');

        if (!$content.length) {
            return {};
        }
        $('.coremenu-mobile.only-for-mobile').find('.js-menuflyout-trigger').toggleClass('hover');
        $content.show();
    });


    $(window).resize(function () {
        updateMinicartSize();
    });

}

//mini-cart-content js-menuflyout-content

function bagmobileicon() {
    var scrollTimeout;
    $(window).on('scroll', function () {
        if (scrollTimeout) {
            return;
        }
        scrollTimeout = setTimeout(function () {
            var pageTop = $(window).scrollTop(),
                breakpointScreen = 40;

            if (pageTop <= breakpointScreen) {
                $('.topbar-mobile-bagwhite').removeClass('visible');
            } else {
                $('.topbar-mobile-bagwhite').addClass('visible');
            }
            scrollTimeout = null;
        }, 100);
    });
}

// Module public functions
module.exports = {
    init: function () {
        headermobile();
        bagmobileicon();
    },
    updateMinicartSize: function () {
        updateMinicartSize();
    }
};
