'use strict';

var dialog = require('./dialog'),
	util = require('./util'),
	ajax = require('./ajax'),
	validator = require('./validator'),
	page = require('./page'),
	progress = require('./progress'),
	flyinheader = require('./flyinheader'),
	newformstyle = require('./new-form-style');

function initializeEvents(){ 
	var addressjs = require('./pages/checkout/address'),
	    accountjs = require('./pages/account'),
	    paysafe = require('./paysafe');
	
$('.tjcplus-activation').on('click',function(e){
		var url = Urls.TJCcheckmember;       
        $.ajax({            	
      	        url: url,
      	        type: 'GET',
      	      success: function (response) {         	    	  
      	        	var res = response; 	        	
      	        	        $('.ismember').text(res.ismember);
      	        	        $('.ismember').css("display","none"); 
       	        	      if(res.ismember == "true")     
      	        		   {   
      	        		   	window.location.href = window.location.pathname;
      	        		  $('.showActivateQBModel').addClass("membercheck");
      	        	     
                            
      	        		   }
             },        
              error: function (jqXHR, textStatus, errorThrown) {console.log(jqXHR + textStatus + errorThrown); }
                });
});


 	
 	$('.tjcplus-activation').on('click',function(e){
		var pid; 
		var tjcpluscontext = $(this).data('tjcpluscontext');
		
		if(tjcpluscontext == 'account'){
			pid = $(this).data('pid');
		}else if(tjcpluscontext == 'pdp' || tjcpluscontext == 'cart'){
			pid = $('input[name="subscription-product"]:checked').val();
		}
		
		if($('.tjcplus-newuser-redirection').length > 0){
			pid = $(this).data('selectedtjcpluspid');
			$('.tjcplus-newuser-redirection').remove();
		}
		
		var url = Urls.tjcplusactivate;
		
	    url = util.appendParamsToUrl(url,{
				'pid':pid
			});

	    progress.show();
	    dialog.open({ 
	        url: url,
	        options:{
	        	dialogClass:'showActivateQBModel tjc-sub-papin',
	        	closeOnEscape: false,
	        	open: function(event,ui){
	        		progress.hide();
	        		$('body').css('overflow','hidden');
	        		$('.credit-card-box').css('max-width','unset');
	        		require('./uniform')(); 
	        		whatiscvv();
	        		accountjs.init();
	        		addressjs.init();
	        		newformstyle.init();
	        		validator.init();
	        		window.paysafeForQB = true;
	        		paysafe.init();
	        		function tjcdeliveryissue(){	  		        		
  		        		var town =  $('#dwfrm_quickbuyaddress_shippingAddress_address_city').val();
  		        		if(town !=""){
  		        		var t1 = town.toUpperCase();
  		        		var t2=t1.replace(/\s+/g, '');
  		        		var towncurrent=t2.replace(/[^a-zA-Z ]/g, ""); 
  		        		}
  		        		if (towncurrent === 'JERSEY' || towncurrent === 'GUERNSEY' || towncurrent === 'ISLEOFMAN')
                          {  	
  		        			if($('.addresses-select .showmore').hasClass('deliverycheck')){
  		        				$('.cityerror').css("display","none");
  		        				$('.cityerror2').css("display","block");
  		        				}
  		        				else{
  		        				$('.cityerror').css("display","block");
  		        				$('.cityerror2').css("display","none");
  		        				}
  		        			$('#dwfrm_quickbuyaddress_shippingAddress_address_city').closest('.form-row').addClass('validation-error');		  
  		        			}  
  		        		else{  
  		        			$('.cityerror').css("display","none");
  		        			$('.cityerror2').css("display","none");
  		        			$('#dwfrm_quickbuyaddress_shippingAddress_address_city').closest('.form-row').removeClass('validation-error');

  		        		}

  		        	}
	        		$('.addresses-select .showmore').on('click',function(){	        			
       			
	        			$('.addresses-select .showmore').addClass('deliverycheck');
	        			
	        		});
	        		$('.address-boxes .address-check-btn').on('click',function(){	        			
	           			
	        			$('.addresses-select .showmore').removeClass('deliverycheck');
	        			
	        		});
  		        	setInterval(tjcdeliveryissue,1000);
	        	},
	        	close:function() {		        		
	        		var url = Urls.restoreqbbasketfromsession; 
	        		$('body').removeAttr("style");
	        		$('html').removeAttr("style");
	        		$('body').css('overflow','x-hidden');
	        		$.ajax({
	    		        url: url,
	    		        type: 'POST',
	    		        success: function (response) { 
	    		        	//do something
	    		        }
	        		});
	        		
	        		var context = $('.tjcplus-activation').data('tjcpluscontext');
	        		if(context == 'pdp'){
	        			var redirect = Urls.getProductUrl;
	        			redirect = util.appendParamsToUrl(redirect,{
	        				'pid': $('.product-code-pdp .product-id').text().trim()
	        			});
	        			window.location.href = redirect;
	        		}else if(context == 'cart'){
	        			var redirect = Urls.cartShow;
	        			window.location.href = redirect;
	        		}else{
	        			window.location.href = window.location.href.split('#')[0];
	        		}
	        	}
	        }
	    });
 		
         	        	    

	});

 


	
	$('body').on('click', '.tjcplus-activation-login' , function (e) {
	    e.preventDefault();
	    var Login_PopUp_Active = window.SitePreferences.Login_PopUp_Active;
	    if(Login_PopUp_Active){
		    var url = Urls.newRegistration;
		    url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    url = util.appendParamToURL(url,'tjcpluscontext',true);
		    dialog.open({ 
		        url: url,
		        options:{
		        	dialogClass:'new-sign-registration',
		        	open: function(event,ui){
		        		validator.init();
		        		newformstyle.init();
		        		
		        		$('.oAuthIcon').bind('click', function () {
		        	    	$('.OAuthProvider').val(this.id);
		        	    	
		        	    	var tjcplusproductId = $('input[name="subscription-product"]:checked').val();
		        	    	$('.tjcpluspid').val(tjcplusproductId);
		        	    	$('.redirecttjcplusactivation').val(true);
		        	    });
		        	    
		        		$('body').css('overflow','hidden');
		        		$('.checkemail-button button').on('click', function(event){
		        			event.preventDefault(); 
		        			var url = Urls.checkMailAvailabilty;
		        			var $form = $(this).closest('form');
		        			var emailid = $form.find('.email-field input').val();
		        			if ($form.find('.checkemail-field .error').length > 0){
		        				$form.find('.checkemail-field .error').remove();
		        			}
		        			var regex = validator.regex;
		        			if (emailid != '' && !regex.newEmail.test(emailid)){
		        				var newDiv ='<div class="error" style="color:#c20000;">The email address is invalid.<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			if (emailid == ''){
		        				var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			url = util.appendParamToURL(url,'emailid',emailid);
		        			 $.ajax({
		        			        url: url,
		        			        type: 'POST',
		        			        success: function (response) { 
		        			        	var responseJson = JSON.parse(response);
		        			        	if (responseJson.availableEmail){
		        			        		var $form1 = $('.returning-customers form');
		        			        		if ($form1.find('.email-value').hasClass('d-none')){
		        			        			$form1.find('.email-value').text(responseJson.email).removeClass('d-none');
		        			        		} else {
		        			        			$form1.find('.email-value').text(responseJson.email);
		        			        		}
		        			        		if ($form1.find('.checkpw-content').hasClass('d-none')){
		        			        			$form1.find('.checkpw-content').removeClass('d-none');
		        			        		}
		        			        		if (!$form1.find('.checkemail-field').hasClass('d-none')){
		        			        			$form1.find('.checkemail-field').addClass('d-none');
		        			        		}
		        			        		if ($form1.find('.form-row-password').hasClass('d-none')){
		        			        			$form1.find('.form-row-password').removeClass('d-none');
		        			        		}
		        			        		if (!$form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
		        			        			$form1.find('.form-row-button .checkemail-button').addClass('d-none');
		        			        		}
		        			        		if ($form1.find('.checkpw-button').hasClass('d-none')){
		        			        			$form1.find('.checkpw-button').removeClass('d-none');
		        			        		}
		        			        		if ($('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkpw-title').removeClass('d-none');
		        			        		}
		        			        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        			        		}
		        			        		if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
		        			        		}
		        			        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
		        			        		}
		        			        		
		        			        		$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        			        		signInForCustoer();
		        			        		forgotPwdClick();
		        			        		editbuttonclick();
		        			        		enteredWrongEmail();
		        			        		disguisePassword();
		        			        	} else {
		        			        		var cgid = '',pid='',pipeline='',cid='';
		        			        		var tjcpluspid = $('input[name="subscription-product"]:checked').val();
		        			        		
		        			        		if (pageContext.ns == 'search') {
		        			        			cgid = $('.new-register-direction').val();
		        			        		} else if (pageContext.ns == 'product'){
		        			        			pid = $('.product-code-pdp .product-id').text().trim();
		        			        		} else if (pageContext.ns == 'livetv' || pageContext.ns == 'account'){
		        			        			var pathName = window.location.pathname;
		        			        			pipeline = pathName.split('/')[pathName.split('/').length-1]
		        			        		} else if (pageContext.ns == 'content'){
		        			        			var cgidvalue = $('.new-register-direction').val();
		        			        			var cidvalue = $('.new-cid-register-direction').val();
		        			        			if (cgidvalue != "null") {
		        			        				cgid = cgidvalue;
		        			        			} else if (cidvalue != "null"){
		        			        				cid = cidvalue;		 
		        			        			} else {
		        			        				var pathName = window.location.pathname;
			        			        			pipeline = pathName.split('/')[pathName.split('/').length-1];
		        			        				if (pipeline == 'Search-Show'){
			        			        				cgid = window.location.search.split('=')[1];
			        			        			} else {
			        			        				cid = window.location.search.split('=')[1];
			        			        			}
		        			        			}
		        			        		}
		        			        		var url = util.appendParamsToUrl(Urls.oldRegPage,{
		        			        			'emailid':emailid, 
												'fromPopUpRegister' : 'yes',
		        			        			'newSignin':'newSignin',
		        		 	        			'pagecontext':pageContext.ns,
		        			        			'cgid':cgid,
		        			        			'pid':pid,
		        			        			'pipeline':pipeline,
		        			        			'cid':cid,
		        			        			'tjcpluspid':tjcpluspid,
		        			        			'redirecttjcplusactivation':true
		        			        			});
		        			        		
		        			        		window.location.href = url;
		        			        	}
		        			        	
		        			        	$(".emailEditButton").on("click", function(){
		        			        		$('.newsignin-box-inner .checkpw-title').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-content').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-field').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-button').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-title').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-content').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-field').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-button').removeClass('d-none');
		        			        	});
		        			        	
		        			        }, 
		        			        error: function (jqXHR, textStatus, errorThrown) {console.log(jqXHR + textStatus + errorThrown); }
		        			    });
		        		});
		        	}, 
		        	close:function() {
		        		$('body').removeAttr("style");
		        		$('html').removeAttr("style");	
		        	}
		        }
		    });
	    }else{
	    	var cgid = '',pid='',pipeline='',cid='';
    		var tjcpluspid = $('input[name="subscription-product"]:checked').val();
    		
    		if (pageContext.ns == 'search') {
    			cgid = $('.new-register-direction').val();
    		} else if (pageContext.ns == 'product'){
    			pid = $('.product-code-pdp .product-id').text().trim();
    		} else if (pageContext.ns == 'livetv' || pageContext.ns == 'account'){
    			var pathName = window.location.pathname;
    			pipeline = pathName.split('/')[pathName.split('/').length-1]
    		} else if (pageContext.ns == 'content'){
    			var cgidvalue = $('.new-register-direction').val();
    			var cidvalue = $('.new-cid-register-direction').val();
    			if (cgidvalue != "null") {
    				cgid = cgidvalue;
    			} else if (cidvalue != "null"){
    				cid = cidvalue;		 
    			} else {
    				var pathName = window.location.pathname;
        			pipeline = pathName.split('/')[pathName.split('/').length-1];
    				if (pipeline == 'Search-Show'){
        				cgid = window.location.search.split('=')[1];
        			} else {
        				cid = window.location.search.split('=')[1];
        			}
    			}
    		}
    		var url = util.appendParamsToUrl(Urls.oldRegPage,{
    			'pagecontext':pageContext.ns,
    			'cgid':cgid,
    			'pid':pid,
    			'pipeline':pipeline,
    			'cid':cid,
    			'tjcpluspid':tjcpluspid,
    			'redirecttjcplusactivation':true,
    			'emailid':'', 
				'fromPopUpRegister' : 'no',
    			'newSignin':'newSignin'
     			});
    		
    		window.location.href = url;
	    }
	});
	
	 
	if($('.tjcplus-newuser-redirection').length > 0){
		$(".sticky-desktop-check .tjcplus-activation").trigger('click');
	}
	
}

function whatiscvv(){
	$('.whatiscvv').click(function (e) {
	    if($( "#whatiscvv-modelbox" ).hasClass('d-none')){
	    	$( "#whatiscvv-modelbox" ).removeClass('d-none'); 
	    }
	    else{
	    	$( "#whatiscvv-modelbox" ).addClass('d-none'); 
	    }
	});
}

function signInForCustoer() {
$('.checkpw-button .singn-button').off('click').on('click', function(e){
	flyinheader.init();
	e.preventDefault();
	var url = Urls.newLogin;
	var $form = $(this).closest('form');
	var emailid = $form.find('.email-value').text();
	var pwd = $form.find('.form-row-password input').val();
	if ($form.find('.form-row-password .error').length > 0){
		$form.find('.form-row-password .error').remove();
	}
	if (pwd == '') {
		var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank <div>' 
		$form.find('.form-row-password').append(newDiv);
		return;
	}
	
	var data = $form.serialize();
	url = util.appendParamsToUrl(url,{'emailid':emailid , 'password':pwd, 'redirecttjcplusactivation':true});
	 $.ajax({
	        url: url,
	        type: 'POST',
	        data: data,
	        success: function (response) { 
	        	var parseResponse = JSON.parse(response);
	        	if (parseResponse.loginstatus && !parseResponse.tvcustomer){
        			if(!parseResponse.isTJCPlusMember){
        				if($('a.tjcplus-activation-login').length > 0){
	        				var plusanchor = $('a.tjcplus-activation-login');
	        				plusanchor.addClass("tjcplus-activation");
	        				plusanchor.removeClass("tjcplus-activation-login");
	        				initializeEvents();
	        				$(".sticky-desktop-check .tjcplus-activation").trigger('click');
	        			}
        			}else{
        				$('.plus-subscription-class').remove();
        			}
	        		
        			if(pageContext.ns == 'product'){
          				window.location.reload();
    			}
        			else{
        				$( ".header .header-inner" ).load(" .header-inner > *" );
            			$( ".search-result-content #search-result-items" ).load(" #search-result-items > *" );
            			$( "#ra-details-wrapper .ra-details-bottom" ).load(" .ra-details-bottom > *" );
            			$( ".main-wrapper #ajaxRefresh" ).load(" #ajaxRefresh > *" );	
            			$( ".main-wrapper .cart" ).load(" .cart > *" );
            			$( ".main-wrapper .plus-delivery-message" ).load(" .plus-delivery-message > *" );
            			
            			$('.product-wishlist-link').removeClass('login-class');
            			$('.wishlist-button').removeClass('sigin-model-button');       			
            			$( ".main-wrapper #product-content-wrapper" ).load(" #product-content-wrapper > *" );
            			dialog.close();
        			}        			      			        			
	        	}
	        	else if(parseResponse.loginstatus && parseResponse.tvcustomer){	
					var prevUrl = 	window.location.href;
					prevUrl = util.appendParamsToUrl(prevUrl,{'redirecttjcplusactivation':true, 'tjcpluspid':$('input[name="subscription-product"]:checked').val()});

	        		window.location.href = util.appendParamsToUrl(Urls.setnewPassword,{'emailid':emailid , 'PasswordResetEmail': parseResponse.PasswordResetEmail, 'prevUrl' : prevUrl}); 
	        	}
	        	else {
	        		if ($form.find('.login-error-message').hasClass('d-none')){
	        			$form.find('.login-error-message').removeClass('d-none');
	        		}
    				if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
	        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
	        		}
    				if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
	        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
	        		}
    				if ($form.find('.form-row-password .error').length > 0){
    					$form.find('.form-row-password .error').remove();
    				}
					var newDiv ='<div class="error" style="color:#c20000;">The password is incorrect<div>' 
        			$form.find('.form-row-password').append(newDiv);

	        	}
	        }		      
	 });
});
}

function forgotPwdClick() {
$('.forgot-pin a, .resend-button .button,forgotpw-button a').on('click',function(e){
	e.preventDefault();
	var url = Urls.forgotPassword;
	var $form = $(this).closest('form');
	var emailid = $form.find('.email-value').text();
	url = util.appendParamToURL(url,'emailid',emailid);
	 $.ajax({
	        url: url,
	        type: 'POST',
	        success: function (response) { 
	        	var parseResponse = JSON.parse(response);
	        	if (parseResponse.mailHasSend){
	        		enteredWrongEmail();
	        		if ($form.find('.forgotpw-title').hasClass('d-none')){
	        			$form.find('.forgotpw-title').removeClass('d-none');
	        		}
	        		if ($form.find('.forgotpw-content').hasClass('d-none')) {
	        			$form.find('.forgotpw-content').removeClass('d-none');
	        		}
	        		if (!$form.find('.checkpw-content .edit-button').hasClass('d-none')){
	        			$form.find('.checkpw-content .edit-button').addClass('d-none');
	        		}
	        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
	        			$('.newsignin-box-inner .checkemail-content').addClass('d-none');
	        		}
	        		if (!$form.find('.checkpw-content').hasClass('d-none')){
	        			$form.find('.checkpw-content').addClass('d-none');
	        		}
	        		if (!$form.find('.checkpw-button').hasClass('d-none')){
	        			$form.find('.checkpw-button').addClass('d-none');
	        		}
	        		if ($form.find('.forgotpw-button').hasClass('d-none')){
	        			$form.find('.forgotpw-button').removeClass('d-none');
	        		}
	        		if (!$('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
	        			$('.newsignin-box-inner .checkpw-title').addClass('d-none');
	        		}
	        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
	        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
	        		}
	        		if (!$form.find('.form-row-password').hasClass('d-none')){
	        			$form.find('.form-row-password').addClass('d-none');
	        		}
	        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
	        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
	        		}
	        		if ($('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
	        			$('.newsignin-box-inner .guest-checkout-button').removeClass('d-none')
	        		}
	        	} else {
	        		$form.find('.login-error-message').removeClass('d-none');
	        	}
	        	
	        	$('.pwemail').html(emailid);
	        }
	 });
});
}

/**Start - Script to display the modal popup in registration page when you click "Shopped on our TV channel but not on our website?" **/
$( "#tvconnect-modelbox" ).dialog({      
autoOpen: false,      
dialogClass: "tvconnectdialog",
modal: true,   
height: 'auto',            
resizable: true,              
});     

$( "#tvconnect" ).click(function() {  
$(this).closest('form');
$( "#tvconnect-modelbox" ).dialog( "open" );      
});  

function editbuttonclick() {
$('body').on('click','.returning-customers form .checkpw-content .edit-button, .wrong-email-link', function(e){
	var $form1 = $(this).closest('form');
	if (!$form1.find('.checkpw-content').hasClass('d-none')){
		$form1.find('.checkpw-content').addClass('d-none');
	}
	if ($form1.find('.checkemail-field').hasClass('d-none')){
		$form1.find('.checkemail-field').removeClass('d-none');
	}
	if (!$form1.find('.form-row-password').hasClass('d-none')){
		$form1.find('.form-row-password').addClass('d-none');
	}
	if ($form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
		$form1.find('.form-row-button .checkemail-button').removeClass('d-none');
	}
	if (!$form1.find('.checkpw-button').hasClass('d-none')){
		$form1.find('.checkpw-button').addClass('d-none');
	}
	if (!$('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
		$('.newsignin-box-inner .checkpw-title').addClass('d-none');
	}
	if ($('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
		$('.newsignin-box-inner .checkemail-title').removeClass('d-none')
	}
	if (!$form1.find('.forgotpw-title').hasClass('d-none')){
		$form1.find('.forgotpw-title').addClass('d-none');
	}
	if (!$form1.find('.forgotpw-content').hasClass('d-none')) {
		$form1.find('.forgotpw-content').addClass('d-none');
	}
	if (!$form1.find('.guest-checkout-button').hasClass('d-none')) {
		$form1.find('.guest-checkout-button').addClass('d-none');
	}
	if (!$form1.find('.forgotpw-button').hasClass('d-none')){
		$form1.find('.forgotpw-button').addClass('d-none');
	}
	$.each($form1.find('.error'),function(){$(this).remove()});
});
}
/**End**/    

function enteredWrongEmail() {
$('body').on('click','.guest-checkout-button a', function(e){
	var $form1 = $(this).closest('form');
	if ($('.checkemail-title').hasClass('d-none')){
		$('.checkemail-title').removeClass('d-none');
	}
	if ($('.checkemail-content').hasClass('d-none')){
		$('.checkemail-content').removeClass('d-none');
	}
	if ($('.edit-button').hasClass('d-none')){
		$('.edit-button').removeClass('d-none');
	}
	if ($form1.find('.checkemail-button').hasClass('d-none')){
		$form1.find('.checkemail-button').removeClass('d-none');
	}
	if ($form1.find('.checkemail-field').hasClass('d-none')){
		$form1.find('.checkemail-field').removeClass('d-none');
	}
	if (!$form1.find('.forgotpw-title').hasClass('d-none')){
		$form1.find('.forgotpw-title').addClass('d-none');
	}
	if (!$form1.find('.forgotpw-content').hasClass('d-none')) {
		$form1.find('.forgotpw-content').addClass('d-none');
	}
	if (!$form1.find('.forgotpw-button').hasClass('d-none')){
		$form1.find('.forgotpw-button').addClass('d-none');
	}
});
}

function disguisePassword(){
$('.icon-password').off('click').on('click', function () {
    var $this = $(this), $passwordField = $this.closest('.form-row-password').find('.input-text');

    if (!$this.hasClass('show')) {
        $passwordField.removeAttr('type');
        $passwordField.prop('type', 'password');
    } else {
        $passwordField.removeAttr('type');
        $passwordField.prop('type', 'text');
    }
    $this.toggleClass('show');
});
}


function menuclick(){
$(document).ready(function(){
$('.menu-category li .menu-item-toggle').unbind().click(function (e) {
   e.preventDefault();
    var $parentLi = $(e.target).closest('li');
    $parentLi.siblings('li.active').removeClass('active').find('.menu-item-toggle.active').click()
    .removeClass('fa-minus active').addClass('fa-plus');
    $parentLi.toggleClass('active');
    $(e.target).toggleClass('active');
    if ($(e.target).hasClass('fa-minus') || $(e.target).hasClass('fa-plus')){
        $(e.target).toggleClass('fa-plus fa-minus');
    }
});

});
}


exports.init = function () {
    initializeEvents();
};