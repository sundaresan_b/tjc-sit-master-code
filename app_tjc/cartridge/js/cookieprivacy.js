'use strict';

var dialog = require('./dialog'),
    util = require('./util');

module.exports = function () {
    var $cookieWarning = $('.cookiewarning'),
        $gdpr = $('.GDPR'),

        getCookie = function (cname) {
            var name = cname + '=',
                decodedCookie = decodeURIComponent(document.cookie),
                ca = decodedCookie.split(';'),
                i,
                c;
            for (i = 0; i < ca.length; i++) {
                c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return '';
        },

        isGDPRaccepted = function () {
            var accepted = getCookie('gdpr_cookies_accepted');
            return accepted === 'true';
        },

        createExpiryString = function () {
            var d = new Date();
            // a year from now
            d.setTime(d.getTime() + 31536000000);
            return 'expires=' + d.toUTCString();
        },
        expires = null;
    if ($cookieWarning.length && document.cookie.indexOf('dw_cookies_accepted') < 0) {
        if (document.cookie.indexOf('dw_cookies_accepted') < 0) {
            $cookieWarning.addClass('visible');
            $cookieWarning.find('.cookiewarning-close').on('click', function () {
                document.cookie = 'dw_cookies_accepted=1; path=/';
                $cookieWarning.removeClass('visible');
            });
        }
    }

    if (document.cookie.indexOf('dw=1') < 0) {
        document.cookie = 'dw=1; path=/';
    }

    if (!isGDPRaccepted()) {
        $gdpr.css('display', 'flex');
        $gdpr.find('.gdpr-accept').on('click', function () {
            expires = createExpiryString();
            document.cookie = 'gdpr_cookies_accepted=true;' + expires + ';path=/';
            document.cookie = 'CookiesStats=true;' + expires + ';path=/';
            document.cookie = 'CookiesPers=true;' + expires + ';path=/';
            document.cookie = 'CookiesMark=true;' + expires + ';path=/';
            $gdpr.css('display', 'none');
        });
    } else {
        $gdpr.css('display', 'none');
    }

    // load values from cookies
    if (getCookie('CookiesStats') === 'true') {
        $('input[name=statistical_cookie]').prop('checked', true);
    }
    if (getCookie('CookiesPers') === 'true') {
        $('input[name=personalisation_cookie]').prop('checked', true);
    }
    if (getCookie('CookiesMark') === 'true') {
        $('input[name=marketing_cookie]').prop('checked', true);
    }

    $('.message-sticky').find('a.button').on('click', function () {
        $(this).parents('.message-sticky').css('display', 'none');
    });

    $('.gdpr-checkboxes #save').on('click', function () {
        expires = createExpiryString();
        document.cookie = 'CookiesStats=' +
            $('input[name=statistical_cookie]').prop('checked') + ';' + expires + ';path=/';
        document.cookie = 'CookiesPers=' +
            $('input[name=personalisation_cookie]').prop('checked') + ';' + expires + ';path=/';
        document.cookie = 'CookiesMark=' +
            $('input[name=marketing_cookie]').prop('checked') + ';' + expires + ';path=/';
        document.cookie = 'dw_cookies_accepted=1; path=/';
        document.cookie = 'gdpr_cookies_accepted=true;' + expires + ';path=/';
        $('.message-sticky').css('display', 'block');
        $gdpr.css('display', 'none');
    });

    $('.gdpr-checkboxes .info').on('click', function () {
        var cookieOverlay = $(this).siblings('.overlay-container');
        cookieOverlay.removeClass('overlay-container-off');
        cookieOverlay.addClass('overlay-container-on');

    });

    // if we are on the order confirmation page
    if ($('.pt_order-confirmation').length > 0) {
        
    }
};
