/*jslint nomen: true */
'use strict';

var _ = require('lodash'),
    inventory = require('./');

var newLine = '\n';
var pdpStoreTemplate = function (store) {
    return [
        '<li class="store-list-item ' + (store.storeId === User.storeId ? ' selected' : '') + '">',
        '   <div class="store-address">' + store.address1 + ', ' + store.city + ' ' + store.stateCode +
            ' ' + store.postalCode + '</div>',
        '   <div class="store-status ' + store.statusclass + '">' + store.status + '</div>',
        '</li>'
    ].join(newLine);
};
var pdpStoresListingTemplate = function (stores) {
    if (stores && stores.length) {
        return [
            '<div class="store-list-pdp-container">',
            (stores.length > 1 ? '  <a class="stores-toggle collapsed" href="#">' + Resources.SEE_MORE + '</a>' : ''),
            '   <ul class="store-list-pdp">',
            _.map(stores, pdpStoreTemplate).join(newLine),
            '   </ul>',
            '</div>'
        ].join(newLine);
    }
};

var storesListing = function (stores) {
    // list all stores on PDP page
    if ($('.store-list-pdp-container').length) {
        $('.store-list-pdp-container').remove();
    }
    $('.availability-results').append(pdpStoresListingTemplate(stores));
};

var productInventory = {
    setPreferredStore: function (storeId) {
        User.storeId = storeId;
        $.ajax({
            url: Urls.setPreferredStore,
            type: 'POST',
            data: {storeId: storeId}
        });
    },
    productSelectStore: function () {
        var self = this;
        inventory.getStoresInventory(this.pid).then(function (stores) {
            inventory.selectStoreDialog({
                stores: stores,
                selectedStoreId: User.storeId,
                selectedStoreText: Resources.PREFERRED_STORE,
                continueCallback: storesListing,
                selectStoreCallback: self.setPreferredStore
            });
        }).done();
    },
    init: function () {
        var $availabilityContainer = $('.availability-results'),
            self = this;
        this.pid = $('input[name="pid"]').val();

        $('#product-content .set-preferred-store').on('click', function (e) {
            e.preventDefault();
            if (!User.zip) {
                inventory.zipPrompt(function () {
                    self.productSelectStore();
                });
            } else {
                self.productSelectStore();
            }
        });

        if ($availabilityContainer.length) {
            if (User.storeId) {
                inventory.getStoresInventory(this.pid).then(storesListing);
            }

            // See more or less stores in the listing
            $availabilityContainer.on('click', '.stores-toggle', function (e) {
                e.preventDefault();
                $('.store-list-pdp .store-list-item').toggleClass('visible');
                if ($(this).hasClass('collapsed')) {
                    $(this).text(Resources.SEE_LESS);
                } else {
                    $(this).text(Resources.SEE_MORE);
                }
                $(this).toggleClass('collapsed');
            });
        }
    }
};

/***** Start Show more / Show Less Toggle in PDP page *****/
if($('.wrapper.pdp_redesign').length == 0){
if($('.stones-wrapper').length == 0){	
	
	if ($('.section-info-sub .attribute-item').size() > 20 ) {
		var listcount = $('.section-info-sub .attribute-item').size();
		var allowlist = 20 - parseInt(listcount);
		$('.attribute-item').slice(allowlist).remove();
		$('.show-more').removeClass('hidden');
		$('.att-list-pdp').css('column-count','2');
		$('.att-list-pdp').css('display', 'block');
	}
	else if($('.section-info-sub .attribute-item').size() > 10){
		if ($(window).width() <= 767){
			$('.show-more').removeClass('hidden');
   			$('.att-list-pdp').css('column-count','1');
   			$('.att-list-pdp').css('display', 'table');
       }
       else{
    	   	$('.show-more').css('visibility','hidden');
   			$('.att-list-pdp').css('column-count','2');
   			$(".section-info-row.hide-section").css("height","auto");
       }
	}
	else{
		$('.att-list-pdp').css('column-count','1');
		$('.show-more').addClass('hidden');
		$(".section-info-row.hide-section").css("height","auto");
	}
}
else{
	if( $('.section-info-sub .attribute-item').size() > 20 ) {
		var listcount = $('.section-info-sub .attribute-item').size();
		var allowlist = 20 - listcount;
		$('.attribute-item').slice(allowlist).remove();
		$('.show-more').removeClass('hidden');
	}
	else if( $('.section-info-sub ul li').size() > 10 ) {
		$('.show-more').removeClass('hidden');
	}
	else{
		$('.show-more').addClass('hidden');
		$(".section-info-row.hide-section").css("height","auto");
	}
}
}

$(document).on('click','.show', function(){
    $(this).closest('.section-info-row').removeClass('hide-section');      
    $(this).css('display','none');
    $(this).closest('.section-info-row').find('.hideshowmore').css('display','inline-block');
    $(this).closest('.show-more').addClass('show-less');
    
    setTimeout(function(){ 
        if($('.header-fixed.scrolled.hide').length == 1){
        $('.pdp-sticky-on').addClass('top-stick');
         }
    else {
        $('.pdp-sticky-on').removeClass('top-stick');
         }	    	
    }, 10);
});

$(document).on('click','.hideshowmore', function(){
	$(this).closest('.section-info-row').addClass('hide-section');    
	$(this).closest('.section-info-row').find('.show').css('display','inline-block');
    $(this).css('display','none');
    $(this).closest('.show-more').removeClass('show-less');
    $(this).closest('.section-info-row.hide-section').css('position','relative');
    $(this).closest('.section-info-row.hide-section').css('overflow','hidden');
    $('html, body').animate({
        scrollTop: $(this).closest('.section-info-row').offset().top - 100
    }, 'slow');
    setTimeout(function(){ 
        if($('.header-fixed.scrolled.hide').length == 1){
        $('.pdp-sticky-on').addClass('top-stick');
         }
    else {
        $('.pdp-sticky-on').removeClass('top-stick');
         }	    	
    }, 10);
});
/***** End Show more / Show Less Toggle in PDP page *****/

module.exports = productInventory;
