'use strict';

var gtmDataLayer = require('./gtm-datalayer'),
	enableCheckoutButton = require('./pages/checkout/index'),
	progress = require('./progress');

var initPaysafe = function () {
	
	if(window.paysafeScriptInsert != true && !window.paysafe){
		var paysafe_script = document.createElement('script');
		paysafe_script.setAttribute('src','https://hosted.paysafe.com/js/v1/latest/paysafe.min.js');
		document.head.appendChild(paysafe_script);
		console.log('paysafe script inserted');
		window.paysafeScriptInsert = true;
	}else{
		console.log('paysafe script exist');
	}
	if(window.SitePreferences.ApplePay_OnCheckout_Flag && window.applePayScriptInsert != true){
		var applePay_script = document.createElement('script');
		applePay_script.setAttribute('src','https://hosted.paysafe.com/request/v1/latest/paysafe.request.min.js');
		document.head.appendChild(applePay_script);
		console.log('ApplePay-paysafe script inserted');
		window.applePayScriptInsert = true;
	}

	if (!$('.paysafe-card-details').length) {
        return;
    }

    var btoa = window.btoa,
        apiKey = btoa(window.SitePreferences.PAYSAFE_SINGLE_USE_TOKEN),
        paysafe = window.paysafe,
        $form = $('.paysafe-card-details'),
        $credit = $('.tjc-credit-details'),
        $formSubmitOrder = $('.checkout-billing'),
        $paymentMethod = $('input[name=dwfrm_billing_paymentMethods_selectedPaymentMethodID]'),
        $paymentMethodsWarning = $('.payment-methods-warning-inner'),
        payBtn = $form.find('#payNow'),
        $secondayPayNowBtn = $('.paynow'), // This is the Big PAY NOW BUTTON.
        $selectCard = $form.find('#existing-paysafe-card'),
        $selectCardCVV = $form.find('#existing-paysafe-cvv'),
        //$selectCVVForm = $form.find('.cvv-input'),
        $existingCardForm = $form.find('.existing-card-options'),
        $newCardForm = $form.find('.new-card-form'),
        newFormClass = '',
        cardBrandClass = {
            AmericanExpress: 'fa-cc-amex',
            MasterCard: 'fa-cc-mastercard',
            Visa: 'fa-cc-visa',
            Diners: 'fa-cc-diners-club',
            JCB: 'fa-cc-jcb',
            Maestro: 'fa-cc-discover'
        },
        $tjcCredit = $('input[name=dwfrm_billing_tjcCredit]'),
        tjcCreditBalance = $tjcCredit.data('creditavailable'),
        netPrice = $tjcCredit.data('subtotal'),
        paysafeConfig = { // PAYSAFE FIELD CONFIGURATION
            environment: window.SitePreferences.PAYSAFE_ENVIRONMENT,

            // set the CSS selectors to identify the payment field divs above
            // set the placeholder text to display in these fields
            fields: {
                cardNumber: {
                    selector: '#cardNumber',
                    placeholder: $('#cardNumber').data('placeholder')
                },
                expiryDate: {
                    selector: '#cardExpiry',
                    placeholder: $('#cardExpiry').data('placeholder')
                },
                cvv: {
                    selector: '#cardCVC',
                    placeholder: $('#cardCVC').data('placeholder')
                }
            },
            
            initializationTimeout: window.SitePreferences.Netbanx_Timeout
        },

        // work around for submitting the form in case of new card, which is asynchronus.
        submitPaymentForm = function (form) {
            var input = $('<input>')
                .attr('type', 'hidden')
                .attr('name', $('#payNow').attr('name')).val('X');
            $(form).append(input);
            form.submit();
        },

        paymentFormReady = function () {
            var isPaymentFormReady = false,
                tjcCreditValue = $tjcCredit.val();
            if ($form.find('#cardNumber').hasClass('success') &&
                    $form.find('#cardExpiry').hasClass('success') &&
                    $form.find('#cardCVC').hasClass('success') &&
                    $selectCard.val() === 'newCard') {
                isPaymentFormReady = true;
            } else if ($selectCard.val() !== 'newCard' && ($selectCard.val() !== null && $selectCard.val() !== '') &&
                    $selectCardCVV.val() !== '') {
                isPaymentFormReady = true;
            } else if (parseInt(tjcCreditValue) >= tjcCreditBalance) {
                return true;
            }
            return isPaymentFormReady;
        };


    $(document).on('click', '.credit-apply-button', function () {
        $('.credit-apply-button').data('credit-clicked', true);

    });

    /**
     * Handle case for the paysafe form
     * if the paysafe radio is selected, show form and hide Pay now button
     * else hide the paysafe form and show pay now button.
     * For TJC Credit, we disregard this logic as it can be coupled with
     * any payment method.
     */
    $(document).on('click', '.payment-method-wrapper .input-radio', function () {
    	if ($(this).val().toLowerCase() === 'tjccredit') {
            $credit.slideToggle();
            return;
        }
        // TJC Credit could also be active at this time
        var selectedPaymentMethods = $paymentMethod.filter(':checked'),
            selectedPaymentMethod = {};
        for (var i = 0; i < selectedPaymentMethods.length; i++) {
            if (selectedPaymentMethods[i].value !== "TJCCredit") {
                selectedPaymentMethod = selectedPaymentMethods[i].value;
                break;
            }
        }
        if ($(this).hasClass('paysafe')) {
            if (selectedPaymentMethod === 'Paysafe') {
                $('.buget-pay-remember-card-text').css('display', 'none');
                $('#newcard-paysafe-remember').parent().addClass('checked');
                $('#newcard-paysafe-remember').closest('.checkbox-input-wrapper').removeAttr('style');
                $('#existing-paysafe-forget').closest('.checkbox-input-wrapper').removeAttr('style');
            } else {
                $('#newcard-paysafe-remember').prop('checked', true);
                $('#existing-paysafe-forget').prop('checked', false);
                $('.buget-pay-remember-card-text').css('display', 'block');
                $('#existing-paysafe-forget').parent().removeClass('checked');
                $('#newcard-paysafe-remember').parent().addClass('checked');
                $('#newcard-paysafe-remember').closest('.checkbox-input-wrapper').css('display', 'none');
                $('#existing-paysafe-forget').closest('.checkbox-input-wrapper').css('display', 'none');
            }
            newFormClass = 'paysafe-card-details ' + selectedPaymentMethod;
            $secondayPayNowBtn.hide(400);
            $form.attr('class', newFormClass).slideDown(400);
        } else {
            $form.slideUp().removeClass(selectedPaymentMethod);
            $secondayPayNowBtn.show(400);
        }
        $('.payment-method-wrapper .paypal-details').addClass('hidden');
        var $privacyPolicyRequiredButton = $('.privacy-policy-required');
        var payPalCheckFlag = ($('.paypal-details .checkout-pp input[type="checkbox"]').length > 0)?true:false;
        if ($(this).hasClass('paypal')) {
        	if(payPalCheckFlag){
	        	var displayOrder = ($('#isOPCPage').val()=="true")?'7':'5';
	        	$('.payment-method-wrapper .paypal-details').removeClass('hidden').css({'order':displayOrder,'margin-bottom':'20px'});
	        	$('.paypal-details .checkout-pp input[type="checkbox"]').prop('checked',false);
	        	$('.paypal-details .checkout-pp input[type="checkbox"]').parent().removeClass('checked');
	        	$privacyPolicyRequiredButton.addClass('disabled');
	        	enableCheckoutButton.enableCheckoutButton(false);
	        	$('.paypal-details .checkout-pp input[type="checkbox"]').on('change',function(e){
	        		if($(this).is(':checked')){
	        			$privacyPolicyRequiredButton.removeClass('disabled');
	        			enableCheckoutButton.enableCheckoutButton(true);
	        		}else{
	        			$privacyPolicyRequiredButton.addClass('disabled');
	        			enableCheckoutButton.enableCheckoutButton(false);
	        		}
	        	});
        	}
        }
        if ($(this).hasClass('paypal-credit')) {
        	if(payPalCheckFlag){
        		var displayOrder = ($('#isOPCPage').val()=="true")?'9':'6';
	        	$('.payment-method-wrapper .paypal-details').removeClass('hidden').css({'order':displayOrder,'margin-bottom':'20px'});
	        	$('.paypal-details .checkout-pp input[type="checkbox"]').prop('checked',false);
	        	$('.paypal-details .checkout-pp input[type="checkbox"]').parent().removeClass('checked');
	        	$privacyPolicyRequiredButton.addClass('disabled');
	        	enableCheckoutButton.enableCheckoutButton(false);
	        	$('.paypal-details .checkout-pp input[type="checkbox"]').on('change',function(e){
	        		if($(this).is(':checked')){
	        			$privacyPolicyRequiredButton.removeClass('disabled');
	        			enableCheckoutButton.enableCheckoutButton(true);
	        		}else{
	        			$privacyPolicyRequiredButton.addClass('disabled');
	        			enableCheckoutButton.enableCheckoutButton(false);
	        		}
	        	});
        	}
        }
    });

    $selectCard.on('change', function () {
        $selectCardCVV.val('');

        payBtn.prop('disabled', true);
        if ($selectCard.val() === 'newCard') {
            $newCardForm.slideDown(400);
            $existingCardForm.slideUp(400);
        } else {
            $existingCardForm.slideDown(400);
            $newCardForm.slideUp(400);
        }
        if (paymentFormReady()) {
            payBtn.prop('disabled', false);
        }
    });

    $selectCardCVV.on('keyup', function () {
        if (paymentFormReady()) {
            payBtn.prop('disabled', false);
        } else {
            payBtn.prop('disabled', true);
        }
    });

    if(window.paysafeForQB != true){
    // initalize the hosted iframes using the SDK setup function
    paysafe.fields.setup(apiKey, paysafeConfig, function (instance, error) {

    	if (error) {
            // Do the Error logging here.
        	enableCheckoutButton.enableCheckoutButton(false);
            return;
        }

        /*jslint unparam: true */
        instance.fields('cvv cardNumber expiryDate').valid(function (eventInstance, event) {
            $(event.target.containerElement).closest('.card-input').removeClass('error').addClass('success');
            enableCheckoutButton.enableCheckoutButton(true);
            if (paymentFormReady()) {
                payBtn.prop('disabled', false);
            }
        });
        /*jslint unparam: false */

        // instance.fields('cardNumber').value('alabala');

        /*jslint unparam: true */
        instance.fields('cvv cardNumber expiryDate').invalid(function (eventInstance, event) {
            $(event.target.containerElement).closest('.card-input').removeClass('success').addClass('error');
            enableCheckoutButton.enableCheckoutButton(false);
            if (!paymentFormReady()) {
                payBtn.prop('disabled', true);
            }
        });
        /*jslint unparam: false */

        instance.fields.cardNumber.on('FieldValueChange', function (eventInstance) {
            if (!instance.fields.cardNumber.isEmpty()) {

                var cardBrand,
                    cardClass;
                if (eventInstance.getCardBrand()) {
                    cardBrand = eventInstance.getCardBrand().replace(/\s+/g, '');
                }
                cardClass = cardBrandClass[cardBrand];
                $form.find($('.fa')).removeClass('fa-credit-card').addClass(cardClass);
            } else {
                $form.find($('.fa')).removeClass().addClass('fa fa-credit-card');
            }
        });

        // When the customer clicks Pay Now,
        // call the SDK tokenize function to create
        // a single-use payment token corresponding to the card details entered
        $formSubmitOrder.on('submit', function (e) {
            payBtn.prop('disabled', true);
            var selectedPaymentMethod;
            var isMultiplePaymentMethodsSelected = $paymentMethod.filter(':checked').length > 1;
            if (isMultiplePaymentMethodsSelected) {
                var tjcCreditisClicked = $('.credit-apply-button').data('credit-clicked') ? true : false;
                var checkboxes = $('.input-radio.paysafe');

                if (tjcCreditisClicked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        checkboxes[i].checked = false;
                    }
                    checkboxes.parent().removeClass('checked');
                    $('.paysafe-card-details').slideUp(400);
                    selectedPaymentMethod = $('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_TJCCredit').val();
                } else {
                    selectedPaymentMethod = $paymentMethod.filter(':checked').not('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_TJCCredit').val();
                    $('input[id=dwfrm_billing_paymentMethods_selectedPaymentMethodID_TJCCredit]').click();
                }

            } else {
                selectedPaymentMethod = $paymentMethod.filter(':checked').val();
            }

            var that = this,
                selectedCardValue = $selectCard.val(),
                threeDS = $form.data('3dsecure'),
                gtmJSON;

            if (selectedPaymentMethod !== 'Paysafe' && selectedPaymentMethod !== 'BudgetPay') {
                payBtn.prop('disabled', false);
                return;
            }
            e.preventDefault();

            try {
                gtmJSON = gtmDataLayer.getPayNowGTMData();
                gtmJSON.ecommerce.checkout_option.actionField.option = selectedPaymentMethod;
                gtmDataLayer.pushCheckoutStepToDataLayer(gtmJSON);
            } catch (er) {
                // eslint-disable-next-line no-console
                console.log(er);
            }

            if (selectedCardValue === 'newCard' || $newCardForm.is(':visible')) {
                /*jslint unparam: true */
                instance.tokenize(threeDS, function (eventInstance, tokenizationError, result) {
                    if (tokenizationError) {
                        if (tokenizationError.code === '9003') {
                            //show tokenizationError if it's card data validation related.
                            $paymentMethodsWarning.text(tokenizationError.message).parent().show();
                            payBtn.prop('disabled', false);
                        // challenge was not accepted
                        } else if (tokenizationError.code === '9040') {
                            $paymentMethodsWarning.text(tokenizationError.detailedMessage).parent().show();
                        // card was rejected
                        } else if (tokenizationError.code === '9039') {
                            $paymentMethodsWarning.text(tokenizationError.detailedMessage).parent().show();
                        // paysafe error
                        } else if (tokenizationError.code === '1007') {
                            $paymentMethodsWarning.text(tokenizationError.displayMessage).parent().show();
                        }
                        $('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').removeClass('freezeSection');
                        $('.progress-loader').hide();
                        console.error('3D secured card error: ',tokenizationError);
                        window.dataLayer.push({
                			'event': 'checkoutError',
                			'ecommerce': {
                				'errorText': '3D Card Error',
                				'errorMessage': tokenizationError.code+': '+tokenizationError.displayMessage
                			}
                		});
                    } else {
                        //console.log(JSON.stringify(result));
                        $('.paysafe-token').val(JSON.stringify(result));
                        submitPaymentForm(that);
                    }
                });
            } else if ($tjcCredit.val() >= netPrice) {
                submitPaymentForm(that);
            } else {
                $('.paysafe-token').val(selectedCardValue);
                submitPaymentForm(that);
            }
        });
    });
	}else{
	setTimeout(function(){
    	var options = { // PAYSAFE FIELD CONFIGURATION
			environment: window.SitePreferences.PAYSAFE_ENVIRONMENT,
			// set the CSS selectors to identify the payment field divs above
			// set the placeholder text to display in these fields
			fields: {
				cardNumber: {
					selector: '#cardNumber',
					placeholder: $('#cardNumber').data('placeholder'),
					separator: " "
				},
				expiryDate: {
					selector: '#expiryDate',
					placeholder: $('#expiryDate').data('placeholder')
				},
				cvv: {
					selector: '#cvv',
					placeholder: $('#cvv').data('placeholder')
				}
			},
			initializationTimeout: window.SitePreferences.Netbanx_Timeout
		};
    	
    	paysafe = window.paysafe;
		/* initalize the hosted iframes using the SDK setup function*/
    	paysafe.fields.setup(apiKey, options, function(instance, error) {
		
			/* When the customer clicks Pay Now,
			   call the SDK tokenize function to create
			   a single-use payment token corresponding to the card details entered*/
			document.getElementById("payNow").addEventListener("click", function(event) {
		
				if ($('#qberror .error').length > 0) {
					$('#qberror .error').remove();
				}
		
				instance.tokenize(function(instance, tokenizationError, result) {
					if (tokenizationError) {
						console.log(tokenizationError);
						/*alert("tokenizationError");*/
						var newDiv = '<div class="error">Unable to add card. Please check your card details again. <div>';
						$("#qberror").append(newDiv);
					} else {
						/*console.log(JSON.stringify(result));*/
						$('.paysafe-token').val(JSON.stringify(result));
						var billingaddress = $('#dwfrm_quickbuyaddress_billingAddress_address_address1').val() + ' ' + $('#dwfrm_quickbuyaddress_billingAddress_address_address2').val() + '$' + $('#dwfrm_quickbuyaddress_billingAddress_address_city').val() + '$' + $('#dwfrm_quickbuyaddress_billingAddress_address_postCode').val() + '$' + $('#dwfrm_quickbuyaddress_billingAddress_address_country option:selected').val();
						var url = Urls.authorizeCard;
						url = url + '?token=' + result.token + '&address=' + billingaddress;
						if ($('#fromUpdate').val() == 'yes') {
							url = url + '&fromUpdate=yes';
						}
						$.ajax({
							url: url,
							type: 'POST',
							success: function(response) {
								if (response.success) {
									if ($('#addnewcard-sucessmg').hasClass('d-none')) {
										$('#addnewcard-sucessmg').removeClass('d-none');
										$('.credit-card-box .error').addClass('d-none');
									}
									$('#dialog-container').css('min-height', 'unset');
									$('.qbheading').addClass('d-none');
									$('#cardNumber').addClass('d-none');
									$('.security').addClass('d-none');
									$('#payNow').addClass('d-none');
									/*$("#qbaddnewcard").load(location.href + " #qbaddnewcard");*/
									/*$( ".paysafe-card-details #existing-paysafe-card" ).load(" #existing-paysafe-card > *" );*/
									var qbtoken = JSON.parse($('.paysafe-token').val()).token;
									var paymentToken = response.PaymentToken;
									var refreshurl = Urls.refreshCardSection;
									$.ajax({
										url: refreshurl + '?newCardToken=' + paymentToken,
										type: 'POST',
										success: function(data) {
											$("#qb-paysafecard").html(data);
											$("select#existing-paysafe-card").change(function() {
												var selectedCountry = JSON.parse($(this).children("option:selected").val());
												var selectedExpiryMonth = selectedCountry.cardExpiry.month;
												var selectedExpiryYear = selectedCountry.cardExpiry.year;
												var expiry = "Expiry: " + selectedExpiryMonth + "/" + selectedExpiryYear;
												$('#cardExpiry').val(expiry);
											});
											if ($('#addnewCardQb').hasClass('new-card-empty')) {
												$('#addnewCardQb').removeClass('new-card-empty');
											}
											initPaysafe();
											/*alert("ok");*/
										}
									});
								} else {
									if ($('#addnewcard-notvaliderror').length > 0) {
										$('#addnewcard-notvaliderror').remove();
									}
									/*console.log("Add a different card");*/
									var newDiv = '<div id="addnewcard-notvaliderror">Add a Valid Card<div>';
									$("#qb-paysafecard").append(newDiv);
									initPaysafe();
								}
							}
						});
					}
				});
			}, false);
		});
	}, 3000);
	}
    
	if(window.SitePreferences.ApplePay_OnCheckout_Flag){
		var ApplePayRequestOptions = {
			country: $('#countryCode').val(),
			currency: $('#currencyCode').val(),
			amount: parseInt(parseFloat($('#orderTotalValue').val()).toFixed(2).replace('.','')),
			requestBillingAddress: true,
			label: " ",
			environment: window.SitePreferences.ApplePay_Environment
		};
		
		setTimeout(function(){
			if(window.applePayInit != true){
				paysafe.request.init(apiKey, ApplePayRequestOptions);
				window.applePayInit = true;
			}else{
				paysafe.request.updateOptions(ApplePayRequestOptions);
			}
			//console.debug(ApplePayRequestOptions);
			paysafe.request.canMakePayment(function (methods, error) {
				if (methods) {
					if (methods.indexOf("APPLEPAY") < 0) {
						console.log("Apple Pay not supported");
						$('.payment-method.apple-pay').hide();
					}else{
						paysafe.request.onTokenization(function (event, error) {
							progress.show();
							if (event) {
								try {
									// Process the token - event.result.token
									// Acknowledge - event.showSuccess() or reject event.showFailure()
									var result = {"token":event.result.token};
									$('.applepay-token').val(JSON.stringify(result));
									//console.log(event.result.token);
									window.applePayAddress = new Array();
									window.existingAddress = new Array(); 
									$.each(['address1', 'address2', 'address3', 'address4', 'locality', 'city', 'postCode', 'country'], function (i, attribute) {
										var value = $('.checkout-billing-address').find('[name$=' + attribute + ']').val();
										existingAddress.push(value);
									});
									var billingContact = event.result.billingContact;
									if(typeof(billingContact)!=='undefined'){
										var itr=1;
										$.each(billingContact.addressLines, function(i, item) {
											if(itr <= 4){
												applePayAddress.push(item);
												itr++;
											}
										});
										while(itr<=4){
											applePayAddress.push("");
											itr++;
										}
										applePayAddress.push(billingContact.locality);
										applePayAddress.push(billingContact.locality);
										applePayAddress.push(billingContact.postalCode);
										applePayAddress.push(billingContact.countryCode);
										$.each(['address1', 'address2', 'address3', 'address4', 'locality', 'city', 'postCode', 'country'], function (i, attribute) {
											$('.checkout-billing-address').find('[name$=' + attribute + ']').val(applePayAddress[i]);
										});
									}
									event.showSuccess();
									document.getElementById("nonCardPayNow").click();
								} catch (b) {
									console.error('Apple Pay error - Payment:',b);
									progress.hide();
								}
							} else {
								// handle error
								$paymentMethodsWarning.text(error.detailedMessage).parent().show();
								console.error('Apple Pay error - Tokenization:',error);
								event.showFailure();
								progress.hide();
								//$('.payment-method.apple-pay').hide();
							}
						});
						paysafe.request.showButtons(function (displayedPaymentMethods, error) {
							if (error) {
								// handle error
								//$paymentMethodsWarning.text(error.detailedMessage).parent().show();
								console.error('Apple Pay error - Show Button:',error);
								$('.payment-method.apple-pay').hide();
							}
						});
					}
				}else{
					//console.error(error);
					//$paymentMethodsWarning.text(error.detailedMessage).parent().show();
					//console.log('Apple Pay error - Initialize:',error);
					$('.payment-method.apple-pay').hide();
				}
			});
		}, 1000);
	}
};

exports.init = initPaysafe;
