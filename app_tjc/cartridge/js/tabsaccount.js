'use strict';

function initTabs() {
    var handlePasswordFields = function ($el) {
        $el.find('.icon-password-show')
            .removeClass('icon-password-show')
            .addClass('icon-password show');
    };
    $('.account-tab-login').on('click', function () {
        $('.account-tab-login').addClass('active');
        $('.account-tab-register').removeClass('active');
        $('.login-container').removeClass('hidden');
        $('.reduced-register-container').addClass('hidden');
        $('.input-password').removeAttr('type');
        $('.input-password').prop('type', 'password');
        handlePasswordFields($('.reduced-register-container'));

    });
    $('.account-tab-register').on('click', function () {
        $('.account-tab-register').addClass('active');
        $('.account-tab-login').removeClass('active');
        $('.login-container').addClass('hidden');
        $('.reduced-register-container').removeClass('hidden');
        $('.input-password').removeAttr('type');
        $('.input-password').prop('type', 'password');
        handlePasswordFields($('.login-container'));
        $('#RegistrationForm .form-row-select').find('.styledSelect, .options li').attr('tabindex', '0');
        $('.styledSelect').bind('keypress', function(e) {
		    if(e.keyCode==13){
		        $(this).trigger('click');
		        $(this).siblings('.optiongroup').find('.options li:eq(0)').focus();
		    }
		});
		$('.options li').bind('keypress', function(e) {
		    if(e.keyCode==13){
		        $(this).trigger('click');
		        $('input.first-name').focus();
		    }
		});
		$("input.first-email, input.first-name").focus(function() {
		  $(this).trigger('click');
		});
		$("input.first-name").focus(function() {
		  $('select#dwfrm_profile_customer_title').trigger('focusout');
		});
		$('#onbeforeunload-button').on('click', function(){
			setTimeout(function(){ 
				if($('.form-row-select.validation-error').length > 0) {
		    		$(window).scrollTop($('.form-row-select.validation-error').offset().top - 50);
	    		}
    		}, 100);
		});
		
    });
    
    $('.oAuthIcon').bind('click', function () {
    	$('.OAuthProvider').val(this.id);
    });
}

function toggleLogin() {
    $('.center-wrapper').on('click', function () {
        $('.logintv-container').slideToggle('slow');
        $('.tv-customer-box').toggleClass('toggleopen');        
        //$('#chevron').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
    });

}

function guestLogin() {
    $('.goto-guestlink').on('click', function (event) {
        $(event.target).closest('form').submit();
    });
}

// Module public functions
module.exports = {
    init: function () {
        initTabs();
        toggleLogin();
        guestLogin();
    }
};

