/*global cmCreateElementTag: false */

/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var jqueryui = require('../static/default/lib/jquery/ui/jquery-ui.min.js'),
	jcarousel = require('../static/default/lib/jquery/jquery.jcarousel.min.js'),
	validate = require('../static/default/lib/jquery/jquery.validate.min.js'),
	zoom = require('../static/default/lib/jquery/jquery.zoom.min.js'),
	zoomsl = require('../static/default/lib/jquery/zoomsl.js'),
	uniform = require('../static/default/lib/jquery/uniform/jquery.uniform.min.js'),
	picturefill = require('../static/default/lib/picturefill/picturefill.min.js'),
	fastclick = require('../static/default/lib/fastclick.min.js'),
	slickslider = require('../static/default/lib/slick.min.js'),
	dialog = require('./dialog'),
    minicart = require('./minicart'),
    page = require('./page'),
    simplesearch = require('./simplesearch'),
    searchsuggest = require('./searchsuggest'),
    tooltip = require('./tooltip'),
    util = require('./util'),
    validator = require('./validator'),
    liveTV = require('./livetv'),
    risingAuctions = require('./risingauctions'),
    carousel = require('./carousel'),
    menuflyout = require('./menuflyout'),
    tabspdp = require('./tabspdp'),
    newsletter = require('./newsletter'),
    pdpoverlay = require('./pdpoverlay'),
    pdpprint = require('./pdpprint'),
    freetextsearch = require('./freetextsearch'),
    flyIn = require('./flyin'),
    flyinheader = require('./flyinheader'),
    // stickyheadermain = require('./stickyheadermain'),
    headermobile = require('./headermobile'),
    paysafe = require('./paysafe'),
    gtm = require('./gtm-datalayer'),
    newformstyle = require('./new-form-style'),
    newsignin = require('./newsignin');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	console.warn('Inserting JQuery cdn');
    var s = document.createElement('script');
    s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
}
//if LazyLoad is not loaded, load from static file
if(window.lazyLoadingScriptInsert != true){
	console.warn('Inserting Lazy Load library -init');
	var s = document.createElement('script');
    s.setAttribute('src', location.origin+'/on/demandware.static/Sites-TJC-GB-Site/-/en/lib/lazy.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
    window.lazyLoadingScriptInsert = true;
}

require('./jquery-ext')();
require('./cookieprivacy')();
require('./uniform')();

function initializeEvents() {

    var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'],
        $header = $('.header'),
        $searchContainer = $header.find('.header-search'),
        $subscribeEmail,
        toggleMobileMenu,
        hideMobileAccountDropdown,
        hideMobileSearch,
        hideMobileMenu,
        hideMobileMiniCart,
        hideMiniCart,
        hideAccountDropdown,
        hideSearchSuggestions;
    
    if (window.matchMedia("(max-width: 1023.5px)").matches){
    	$('a[target^="_blank"]').attr('target','_self');
    }

    $('body')
        .on('keydown', 'textarea[data-character-limit]', function (e) {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length;

            if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
                e.preventDefault();
            }
        })
        .on('change keyup mouseup', 'textarea[data-character-limit]', function () {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length,
                charsRemain = charsLimit - charsUsed;

            if (charsRemain < 0) {
                $(this).val(text.slice(0, charsRemain));
                charsRemain = 0;
            }

            $(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
        });

    searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);

    //check menu flyouts exceeding pagewidth
    $header.find('.navigation .menu-category.not-for-mobile .flyout').each(function () {
        var $flyout = $(this),
            left;
        $flyout.css('visibility', 'hidden').show(); //magic so we can properly retrieve the offset
        if ($flyout.width() + $flyout.offset().left > $header.width() + $header.offset().left) {
            if ($flyout.width() > $flyout.offset().left) {
                // this overwrites the 'left' from 'moved-left' if the flyout is too big
                left = $flyout.offset().left - (($header.width() - $flyout.width()) / 2);
                $flyout.css('left', '-' + left + 'px');
                $flyout.css('right', 'auto');
            }
            $flyout.addClass('moved-left');
        }
        $flyout.css({visibility: '', display: ''});
    });

    // print handler
    $('.print-page').on('click', function () {
        window.print();
        return false;
    });

    // add show/hide navigation elements
    /* $('.secondary-navigation .toggle').click(function () {
        $(this).toggleClass('expanded').next('ul').toggle();
    }); */

    util.addToggleEvents();

    (function () {
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 200,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offsetOpacity = 1200,
        //duration of the top scrolling animation (in ms)
            scrollTopDuration = 700,
        //grab the "back to top" link
            $backToTop = $('.backtotop');

        //hide or show the "back to top" link
        $(window).scroll(function () {
            if ($(this).scrollTop() > offset) {
                $backToTop.addClass('btt-is-visible');
            } else {
                $backToTop.removeClass('btt-is-visible btt-fade-out');
            }
            if ($(this).scrollTop() > offsetOpacity) {
                $backToTop.addClass('btt-fade-out');
            }
        });

        //smooth scroll to top
        $backToTop.on('click', function (event) {
            event.preventDefault();
            $('body,html').animate({
                scrollTop: 0
            }, scrollTopDuration);
        });
    }());
    
    // subscribe email box
    $subscribeEmail = $('.subscribe-email');
    if ($subscribeEmail.length > 0) {
        $subscribeEmail.focus(function () {
            var val = $(this.val());
            if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
                return; // do not animate when contains non-default value
            }

            $(this).animate({color: '#999999'}, 500, 'linear', function () {
                $(this).val('').css('color', '#333333');
            });
        }).blur(function () {
            var val = $.trim($(this.val()));
            if (val.length > 0) {
                return; // do not animate when contains value
            }
            $(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
                .css('color', '#999999')
                .animate({color: '#333333'}, 500, 'linear');
        });
    }

    $('.privacy-policy').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                height: 600
            }
        });
    });

    // main menu toggle
    toggleMobileMenu = function (callback) {
        $('.wrapper').toggleClass('menu-active');
        $('.mobile-menu').slideToggle(0, function () {
            if ($.isFunction(callback)) {
                callback();
            }
        });
    };
    
    hideMobileAccountDropdown = function () {
        $('.mobile-account-menu').hide();
        $('.topbar-mobile-account').find('.user-menu').removeClass('opened');
    };

    hideMobileSearch = function () {
        $('.mobile-search-wrapper').hide();
        $('.header-mobile-navitem-icon-trigger').removeClass('active');
    };

    hideMobileMenu = function () {
        $('.mobile-menu').hide();
        $('.menu-toggle').removeClass('active');
        $('#wrapper').removeClass('menu-active');
    };

    hideMobileMiniCart = function () {
        $('.coremenu-mobile').find('.mini-cart-link').removeClass('hover');
        $('.coremenu-mobile').find('.mini-cart-content').hide();
    };

    hideSearchSuggestions = function () {
        searchsuggest.clearResults();
        $('input[name="q"]').blur();
    };

    hideMiniCart = function () {
        $('.header-fullwidth').find('.mini-cart-link').removeClass('hover');
        $('.header-fullwidth').find('.mini-cart-content').hide();
    };

    hideAccountDropdown = function () {
        $('.header-fullwidth').find('.user-account-trigger').removeClass('hover');
        $('.header-fullwidth').find('.header-account-dropdown-content').hide();
    };
    
    $(document).on('ready', function () {
        $('body').addClass('cta-loaded');
    });
    
    $(window).on('load', function () {
    	$('body').addClass('window-loaded');
    });

    $(document)
        .on('mouseover', '#navigation .a-level-1', function () {
            $(this).closest('.li-level-1').delay(50).queue(function (next) {
                $(this).addClass('hover');
                next();
            });
        })
        .on('mouseleave', '#navigation .li-level-1', function () {
            $(this).queue(function (next) {
                $(this).removeClass('hover');
                next();
            });
        })
        .on('touchstart', function () {
            $('#navigation .li-level-1.hover').removeClass('hover');
        })
        .on('touchstart', '#navigation', function (e) {
            var $target = $(e.target);

            e.stopPropagation();
            if ($target.closest('.li-level-1').hasClass('hover')) {
                return;
            }
            if (!$target.closest('.li-level-1').find('.flyout').length) {
                return;
            }
            e.preventDefault();
            $('#navigation .li-level-1').removeClass('hover');
            $target.closest('.li-level-1').toggleClass('hover');
        });

    $('input[name="q"]').on('focus', function () {
        hideAccountDropdown();
        hideMiniCart();
    });

    $('.menu-toggle').on('click', function () {
        toggleMobileMenu();
        window.scrollTo(0, 0);
        $(this).toggleClass('active');
        hideMobileAccountDropdown();
        hideMobileSearch();
        hideMobileMiniCart();
    });
    $('.header-mobile-navitem-icon-trigger').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.mobile-search-wrapper').slideToggle(0);
        hideMobileAccountDropdown();
        hideMobileMenu();
        hideMobileMiniCart();
    });

    $('.user-account-mobile-trigger').on('click', function () {
        hideMobileMenu();
        hideMobileSearch();
        hideMobileMiniCart();
        $('.mobile-account-menu').slideToggle(0);
        $('.user-menu-mobile').toggleClass('opened');
    });

    $('.user-account-trigger').on('touchstart', function (e) {
        e.preventDefault();
        $(this).toggleClass('hover');
        $('.header-account-dropdown-content').toggle();
        hideMiniCart();
        hideSearchSuggestions();
    }).on('mouseenter', function (e) {
        e.preventDefault();
        $(this).addClass('hover');
        $('.header-account-dropdown-content').show();
        hideMiniCart();
        hideSearchSuggestions();
    });

    $('.header-account-dropdown').on('mouseleave', function () {
        $('.user-account-trigger').removeClass('hover');
        $('.header-account-dropdown-content').hide();
        hideMiniCart();
        hideSearchSuggestions();
    });

    $('.mini-cart-link-withitems').on('touchstart', function () {
        hideAccountDropdown();
        hideSearchSuggestions();
    });

    $('.mini-cart').on('mouseenter', function () {
        if ($('.mini-cart-link-withitems').length) {
            hideAccountDropdown();
            hideSearchSuggestions();
        }
    });

    $('.js-mobile-toolbar-account').on('click', function (e) {
        var showAccountMenu = function () {
            $('.mobile-menu').find('.li-level-1.account > .menu-item-toggle:not(.active)').click();
        };
        e.preventDefault();
        if (!$('.wrapper').hasClass('menu-active')) {
            toggleMobileMenu(showAccountMenu);
        } else {
            showAccountMenu();
        }
    });

    $('.menu-category li .menu-item-toggle').on('click', function (e) {
        e.preventDefault();
        var $parentLi = $(e.target).closest('li');
        $parentLi.siblings('li.active').removeClass('active').find('.menu-item-toggle.active').click()
            .removeClass('fa-minus active').addClass('fa-plus');
        $parentLi.toggleClass('active');

        $(e.target).toggleClass('active');
        if ($(e.target).hasClass('fa-minus') || $(e.target).hasClass('fa-plus')){
            $(e.target).toggleClass('fa-plus fa-minus');
        }
    });

    //closeable error messages
    $('.main').on('click', '.message-content a', function () {
        $(this).parents('.message').remove();
    });
    $('.main').on('click', '.form-row', function () {
        $(this).find('.error-msg').remove();
    });

    //element tags for app download
    $('.footer-apps').on('click', 'a', function () {
        var elementID = $(this).hasClass('store-link-google') ? 'Android' : 'IPAD';
        if (cmCreateElementTag !== undefined) {
            cmCreateElementTag(elementID, 'App Download');
        }
    });

    //element tags for social buttons
    $('.footer-social').on('click', 'a', function () {
        var elementID = $(this).data('elementid');
        if (elementID && cmCreateElementTag !== undefined) {
            cmCreateElementTag(elementID, 'SOCIAL-BUTTON');
        }
    });
    
    $(".try-tjc").click(function() {
    	$(".try-tjc-plus").toggle();
	});
    
    $(".footer-newsletter .newsletter-form-row .newsletter-form-cell-right button").insertAfter(".footer-newsletter .newsletter-form-row .newsletter-form-cell-left .field label");
    
    /** Functionality for Email Subscription Pop-Up **/
    var emailSubcriptionPopUp = util.getCookie("tjcEmailSubscription");
    if(emailSubcriptionPopUp == ""){
    	util.setCookie("tjcEmailSubscription", "no-action", 365);
    }
    emailSubcriptionPopUp = util.getCookie("tjcEmailSubscription");
    if(emailSubcriptionPopUp != "" && (emailSubcriptionPopUp == "no-action" && customer.loginStatusCode == "0") && window.SitePreferences.emailSubscriptionFlag){
    	var url = Urls.EmailSubscriptionPopUp;
    	setTimeout(function(){
	    	dialog.open({ 
				url: url,
				options:{
					dialogClass:'email-subscription-popUp',
					open: function(event,ui){
						newformstyle.init();
						validator.init();
						if(window.innerHeight < window.innerWidth){
							$('.ui-dialog.email-subscription-popUp').css('height','-webkit-fill-available !important');
							$('.ui-dialog.email-subscription-popUp .xlt-emailSubscriptionPopUp').css({'max-height':window.innerHeight+'px','overflow-y':'auto','overflow-x':'hidden'});
						}
						$('.email-subscription-popUp label[for^="dwfrm_newsletter_email"] span').html('Enter your email');
						$('.email-subscription-popUp input[id^="dwfrm_newsletter_email"]').attr('placeholder','Enter your email');
						$('.email-subscription-popUp button.footer-newsletter-submit').html('Join the mailing list');
						$('.email-subscription-popUp input[id^="dwfrm_newsletter_email"]').trigger('focus');
						newsletter.init();
						$('.ui-widget-overlay').off('click.close-popUp').on('click.close-popUp', function(e){
							$('.email-subscription-popUp .ui-dialog-titlebar-close').trigger('click');
						});
					},
					close:function() {
						emailSubcriptionPopUp = util.getCookie("tjcEmailSubscription");
						if(emailSubcriptionPopUp == "no-action"){
							util.setCookie("tjcEmailSubscription", "canceled", 365);
						}
					}
				}
			});
    	}, 45001);
    }
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
    // add class to html for css targeting
    $('html').addClass('js');
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $('html').addClass('infinite-scroll');
    }
    // load js specific styles
    util.limitCharacters();
    util.imageAdjust();
    var tempLoop1 = 10;
    var myVar1 = setInterval(function(){
    	if(tempLoop1 <= 0){
    		clearInterval(myVar1);
    	}else{
    		util.imageAdjust();
    		tempLoop1--;
    	}
    }, 5000);
	document.addEventListener("DOMContentLoaded", lazyLoadingInit);
	lazyLoadListeners(true);
	$('.lazy-load-button').removeClass('lazy-load-button');
}

function lazyLoadingInit(){
	if(!window.LazyLoad){
		console.warn('Inserting Lazy Load library');
		var s = document.createElement('script');
	    s.setAttribute('src', location.origin+'/on/demandware.static/Sites-TJC-GB-Site/-/en/lib/lazy.js');
	    s.setAttribute('type', 'text/javascript');
	    document.getElementsByTagName('head')[0].appendChild(s);
	}
	var lazyLoadInstance=new LazyLoad({elements_selector:".lazy",threshold:window.SitePreferences.Lazy_Loading_Threshold||300});
}

function lazyLoadListeners(callType){
	if(callType){
		document.addEventListener("scroll", lazyLoadingInit);
		window.addEventListener("resize", lazyLoadingInit);
		window.addEventListener("orientationChange", lazyLoadingInit);
	}else{
		document.removeEventListener("scroll", lazyLoadingInit);
		window.removeEventListener("resize", lazyLoadingInit);
		window.removeEventListener("orientationChange", lazyLoadingInit);
	}
}

var pages = {
    account: require('./pages/account'),
    cart: require('./pages/cart'),
    content: require('./pages/content'),
    checkout: require('./pages/checkout'),
    product: require('./pages/product'),
    search: require('./pages/search'),
    storefront: require('./pages/storefront'),
    wishlist: require('./pages/wishlist'),
    orderconfirmation: require('./pages/orderconfirmation'),
    tjcplus: require('./pages/tjcplus')
};

var app = {
    init: function () {
        if (document.cookie.length === 0) {
            $('<div/>').addClass('message error-form browser-compatibility-alert').append($('<div/>')
                .addClass('message-inner').append($('<div/>').addClass('message-content')
                    .html(Resources.COOKIES_DISABLED))).appendTo('#browser-check');
        }
        initializeDom();
        initializeEvents();

        // init specific global components
        liveTV.init();
        risingAuctions.init();
        tooltip.init();
        minicart.init();
        validator.init();
        simplesearch.init();
        carousel.init();
        newsletter.init();
        pdpoverlay.init();
        tabspdp.init();
        freetextsearch.init();
        pdpprint.init();
        menuflyout.init($('.header-account-dropdown'));
        util.executeJqueryReadyCallbacks();
        flyIn.init();
        flyinheader.init();
        // stickyheadermain.init();
        headermobile.init();
        util.disguisePassword();
        util.closeHandlerForErrorMessages();
        //paysafe.init();
        gtm.init();
        newformstyle.init();
        newsignin.init();
        // execute page specific initializations
        
        if($('.quickbuy-preference-page').length > 0 || $('.tjc-plus-account').length > 0){
        	window.paysafeForQB = true;
        	paysafe.init();
        }

        $.extend(page, window.pageContext);
        var ns = page.ns;
        if (ns && pages[ns] && pages[ns].init) {
            pages[ns].init();
        }
        util.imageAdjust();
        var tempLoop = 10;
        var myVar = setInterval(function(){
        	if(tempLoop <= 0){
        		clearInterval(myVar);
        		lazyLoadListeners(false);
        	}else{
        		lazyLoadingInit();
        		util.imageAdjust();
        		tempLoop--;
        	}
        }, 5000);
    }
};

// general extension functions
(function () {
    String.format = function () {
        var first = 0,
            str = arguments[first],
            i,
            len = arguments.length - 1,
            reg;
        for (i = 0; i < len; i++) {
            reg = new RegExp('\\{' + i + '\\}', 'gm');
            str = str.replace(reg, arguments[i + 1]);
        }
        return str;
    };
}());

// initialize app
$(document).ready(function () {
    app.init();
});
