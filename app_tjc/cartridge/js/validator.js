'use strict';

/*jslint regexp: true */
var naPhone = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/,
    regex = {
        phone: {
            us: naPhone,
            ca: naPhone
        },
        mobile: /([0-9]*)$/,
        postal: {
            us: /^\d{5}(-\d{4})?$/,
            ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
            gb: new RegExp('^GIR?0AA|[A-PR-UWYZ]([0-9]{1,2}|' +
                '([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])?[0-9][ABD-HJLNP-UW-Z]{2}$')
        },
        email: /^\w+([\.\-]?\w+)*@\w+([\.\-]?\w+)*(\.\w{2,3})+$/,
        newEmail: /^\w+([\.\-]?\w+)*@\w+([\.\-]?\w+)*(\.\w{2,3})+$/,
        notCC: /^(?!(([0-9 \-]){13,19})).*$/
    },
    overrideErrorClass = 'validation-error',
    overrideValidClass = 'validation-success',
    settings = {
        // global form validator settings
        errorElement: 'div',
        errorClass: 'error-msg',
        highlight: function (element) {
            $(element).closest('.form-row')
                .addClass(overrideErrorClass);
                
             $(element).closest('.form-row')
                .removeClass(overrideValidClass);
        },
        unhighlight: function (element) {
            $(element).closest('.form-row')
                .removeClass(overrideErrorClass);
              
              $(element).closest('.form-row')
                .addClass(overrideValidClass);
         
                
        },
        errorPlacement: function (error, element) {
            if (element.parents('.form-row-birthdate').length > 0) {
                element.parents('.form-row-birthdate > .field').append(error);
            } else {
                element.parents('.form-row:not(.form-row-float) > .field').append(error);
            }

            if ($('.registration-alt').length > 0) {
                var top = (element.outerHeight() / 2) - (error.outerHeight() / 2),
                    right = -(error.outerWidth() + 25),
                    position = 'absolute',
                    $fieldset = error.closest('fieldset'),
                    errorSiblings = error.siblings('.form-row-float'),
                    aClass = 'a' + (Math.random() + 1).toString(36).substring(7),
                    arrowRight;

                if (element.parents('.form-row-with-button').length > 0) {
                    right -= element.parents('.form-row-with-button').find('.button-container').outerWidth() + 10;
                }

                error.css('position', position);
                error.css('top', top);
                error.css('right', right);
                error.addClass(aClass);

                setTimeout(function () {
                    if (($fieldset.position().left + $fieldset.outerWidth() - 10) <
                            (error.position().left + error.outerWidth())) {
                        error.css('position', 'relative');
                        error.css('right', 0);
                        error.addClass('error-msg-mobile');

                        if (element.parents('.form-row-with-button').length > 0) {
                            error.css('margin-top', -10);
                        }

                        if (element.parents('.form-row-with-button').length > 0) {
                            element.parents('.form-row-with-button').parent().append(error);
                        }

                        if (errorSiblings.length > 0) {
                            error.parent().parent().append(error);
                        }

                        if (element.parents('.form-row-birthdate').length > 0) {
                            error.parent().parent().append(error);
                        }

                        arrowRight = ((error.outerWidth() / 2) - 5);
                    } else {
                        arrowRight = (error.outerWidth() - 10);
                    }

                    $('<style>.' + aClass + ':before {right: ' +
                            arrowRight + 'px !important;}</style>').appendTo('head');
                }, 1);
            }
        },
        showErrors: function () {
            this.defaultShowErrors();

            //$('.error-msg').each(function () {
                //var $this = $(this);

                //if ($this.find('.close-icon').length === 0) {
                    //$this.append('<span class="close-icon fa fa-times"></span>');
                //}
            //});
        },
        onkeyup: false,
        onfocusout: function (element) {
            if (!this.checkable(element)) {
                this.element(element);
            }
        }
    };
/*jslint regexp: false */

/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value) {
    return value.trim() === '' || ($.isNumeric(value) && value.trim().length === 11);
};

/**
 * @function
 * @description Validates a given mobile number against the UK mobile regex
 * @param {String} value The mobile number which will be validated
 * @param {String} el The input field
 */
var validateMobile = function (value) {
    return value.trim() === '' || (regex.mobile.test(value.trim()) && value.trim().length === 11);
};

/**
 * @function
 * @description Validates a given email
 * @param {String} value The email which will be validated
 * @param {String} el The input field
 */
var validateEmail = function (value, el) {
    var isOptional = this.optional(el),
        isValid = regex.email.test($.trim(value));
    return isOptional || isValid;
};

/**
 * @function
 * @description Validates a confirmation field against another field
 * @param {String} value The value which will be validated
 * @param {String} el The input field
 */
var validateTwoFields = function (value, el) {
    var $otherField = $('.' + $(el).data('crossvalidationclass'));
    return value === $otherField.val();
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
    var isValid = regex.notCC.test($.trim(value));
    return isValid;
};

/**
 * @function
 * @description Validates that a day is valid against the month and the year
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateDay = function (value, el) {
    var daysInMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        $day = $(el),
        $month = $('.' + $day.data('monthclass')),
        $year = $('.' + $day.data('yearclass')),
        isValid = false;
    if (!value && !$month.val() && !$year.val()) {
        isValid = true;
    } else {
        if (!$year.val() || Number($year.val()) % 4 === 0) {
            daysInMonth[2] = 29;
        }
        if (!$month.val()) {
            isValid = true;
        } else if (Number(value) <= daysInMonth[Number($month.val())]) {
            isValid = true;
        }
    }
    return isValid;
};

/**
 * @function
 * @description Validates month against the day and the year
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateMonth = function (value, el) {
    var $month = $(el),
        $day = $('.' + $month.data('dayclass')),
        $year = $('.' + $month.data('yearclass')),
        isValid = false;
    if ($day.val() || (!$day.val() && !value && !$year.val())) {
        isValid = true;
        this.check($day);
    }
    return isValid;
};

/**
 * @function
 * @description Validates the year against the day and the month
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateYear = function (value, el) {
    var $year = $(el),
        $day = $('.' + $year.data('dayclass')),
        $month = $('.' + $year.data('monthclass')),
        isValid = false;
    if ($month.val() || (!$day.val() && !$month.val() && !value)) {
        isValid = true;
        this.check($month);
    }
    return isValid;
};


/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

/**
 * Add mobile validation method to jQuery validation plugin.
 * Text fields must have 'mobile' css class to be validated as mobile
 */
$.validator.addMethod('mobile', validateMobile, Resources.INVALID_MOBILE);

/**
 * Add email validation method to jQuery validation plugin.
 * Text fields must have 'email' css class to be validated as email
 */
$.validator.addMethod('email', validateEmail, Resources.INVALID_EMAIL);

/**
 * Add cross field validation method to jQuery validation plugin.
 * Text fields must have 'email' css class to be validated as email
 */
$.validator.addMethod('cross-validation', validateTwoFields, Resources.INVALID_MATCH);

/**
 * Add date validation method to jQuery validation plugin.
 * Text fields must have 'day' css class to be validated as day
 */
$.validator.addMethod('day', validateDay, Resources.INVALID_DAY);

/**
 * Add date validation method to jQuery validation plugin.
 * Text fields must have 'month' css class to be validated as month
 */
$.validator.addMethod('month', validateMonth, Resources.INVALID_MONTH);

/**
 * Add date validation method to jQuery validation plugin.
 * Text fields must have 'year' css class to be validated as year
 */
$.validator.addMethod('year', validateYear, Resources.INVALID_YEAR);

/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
    var isOptional = this.optional(el),
        isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
    return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
    if ($.trim(value).length === 0) {
        return true;
    }
    return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

var validator = {
    regex: regex,
    settings: settings,
    init: function () {
        var self = this;
        $('form:not(.suppress)').each(function () {
            $(this).validate(self.settings);
        });
    },
    initForm: function (f) {
        $(f).validate(this.settings);
    }
};

module.exports = validator;
