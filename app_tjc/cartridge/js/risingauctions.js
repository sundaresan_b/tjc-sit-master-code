'use strict';

/* Required Modules */
/* **************** */
var addToCart = require('./pages/product/addToCart'),
    util = require('./util'),
    pubnub = require('./pubnub'),
    newsignin = require('./newsignin'),
	ajax = require('./ajax'),
	progress = require('./progress'); 


/* Local Variables / Configuration */
/* ******************************* */
var pubnubChannelName = 'RisingAuctions',

    timeCounterIntervals = [],
    bidHistorySize = 10,
    pubNubInitialized  = false,

    $risingAuctionContainer,
    risingAuctionTileSelector,
    risingAuctionTileUpdateMode,
    risingAuctionTileUpdateSuccess;

function getServerTime(){
	if($('.ra-timer-plp-pdp').length > 0){
		setInterval(function() {
			try {
				var serverURL = $('#getRAserverTime').attr("data-update-url");
				$('#getRAserverTime').load(serverURL);
			} catch (b) {
                console.log(b);
            }
		}, 1000);
	}
}



/* Functions */
/* ********* */

/**
* Handler for auction details reload (ajax)
*/
function updateAuctionProduct($tile, layout, successType) {
    if (!$tile.length) {
        return;
    }

    var updateURL = $tile.data('update-url'),
        $valueInput = $tile.find('input.bid-value'),
        //$bidButton = $tile.find('.bid-button'),
        data = {};

    if (!updateURL) {
        return;
    }

    if ($valueInput.length) {
        data.currentInputValue = $valueInput.val();
    }

    if (layout.length) {
        data.layout = layout;
    }

    data.excludeTimeCounter = 'true';

    $.ajax({
        url: updateURL,
        dataType: 'html',
        data: data,
        type: 'GET',
        success: function (response) {
            // check if the value input field is currently focussed (user enters a value at the moment) -
            // if so, we do not replace the bottom part of the auction tile

            //check if this has implications on some other places where this is reused!
            var skipValueInput = false,
                $response,
                $bidder,
                replaceTimeCounter = function () {
                    var $timeCounterPlaceholder = $response.find('.time-counter-placeholder'),
                        $timeCounter = $tile.find('.time-counter');

                    if ($timeCounterPlaceholder.length) {
                        $timeCounterPlaceholder.before($timeCounter).remove();
                    }
                };

            // watchlist / favourites
            if (successType && successType === 'table') {
                $response = $('<tr/>').append(response);
                replaceTimeCounter();

                if (skipValueInput) {
                    $response.find('.bid-actions').remove();
                    $tile.find('td:not(.product-row-part-left):not(.bid-actions)').remove();
                } else {
                    $tile.find('td:not(.product-row-part-left)').remove();
                }

                $tile.find('td.product-row-part-left').after($response.children());

            // listing / pdp
            } else {
                $response = $('<div/>').html(response);
                replaceTimeCounter();

                if (skipValueInput) {
                    if ($response.find('input.bid-value').length) {
                        $bidder = $response.find('.bidder');
                        $response = $response.find('.product-tile-top');
                        $tile.find('.product-tile-top').before($response).remove();
                        if (layout === 'pdp') {
                            $tile.find('.bidder').before($bidder).remove();
                        }
                    } else {
                        $tile.html(response);
                    }

                // otherwise replace the whole dynamic part of the auction tile
                } else {
                    $tile.html($response);
                }
            }
        }
    });
}



/**
 *Find RecordCount
 */
function recordCount()
{
	try {
		var url = Urls.rarecordcount;       
		$.ajax({            	
			url: url,
			type: 'GET',
			success: function (response) {         	    	  
				var res = response; 
				if(res.activebidcount != "null" && res.paynowcount != "null" ){          	        	    	
					$('.activebidcount').text(res.activebidcount);
					$('.paynowcount').text(res.paynowcount);
				}
			},        
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR + textStatus + errorThrown);
			}
		});
	} catch (b) {
        console.log(b);
    }
}

/**
 *Initilize Bid History UI
 */
function initBidHistory() {
    var $container = $('#ra-detail-bid-history'),
        $bidRows,
        $showMoreButton;
    if (!$container.length) {
        return;
    }

    // find rows in table
    $bidRows = $container.find('tr.bid');

    // show rows up to the current limit
    $container.find('tr.bid').each(function (index) {
        var $element = $(this);
        if (index < bidHistorySize) {
            $element.show();
        }
    });

    // hide show more button if necessary
    $showMoreButton = $container.find('.ra-detail-bid-history-show-more');
    if ($bidRows.length <= bidHistorySize) {
        $showMoreButton.hide();
    } else {
        $showMoreButton.show();
    }
}




/**
* Handler for updating the Bid History
*/
function updateBidHistory(pids) {
    var $container = $('#ra-detail-bid-history'),
        productID = $('#ra-details-wrapper').data('itemid'),
        updateNecessary = false,
        updateURL,
        i;
    if (!$container.length) {
        return;
    }


    // check if update is necessary (requires provided list of pids)
    if (pids !== undefined) {
        for (i = 0; i < pids.length; i++) {
            if (pids[i] === productID) {
                updateNecessary = true;
            }
        }
    }
    if (!updateNecessary) {
        return;
    }


    // update the bid history via AJAX
    updateURL = $container.data('update-url');
    if (!updateURL) {
        return;
    }

    $.ajax({
        url: updateURL,
        dataType: 'html',
        type: 'GET',
        success: function (response) {
            $container.html(response);
            initBidHistory();
        }
    });
}




/**
 *     PubNub Message Handler
 */
function handleNotification(message) {
    var messageObj,
        $tile,
        i,
        $quickviewContainer;

    // parse message
    if (typeof message === 'string') {
        try {
            messageObj = JSON.parse(message);
        } catch (e) {
            return;
        }
    } else {
        messageObj = message;
    }
    console.group('PubNub');
    if(typeof messageObj === 'object'){
	    console.log('Update Type: '+messageObj.action);
	    console.log('Update Products: '+JSON.stringify(messageObj.pids));
    }else{
    	console.warn(messageObj);
    }
    console.groupEnd('PubNub');


    // find and update tile
    for (i = 0; i < messageObj.pids.length; i++) {
        if (risingAuctionTileSelector.length) {
            $tile = $risingAuctionContainer.find(risingAuctionTileSelector +
                    '[data-itemid="' + messageObj.pids[i] + '"]');
        } else {
            if ($risingAuctionContainer.data('itemid') === messageObj.pids[i]) {
                $tile = $risingAuctionContainer;
            }
        }
        if ($tile !== undefined && $tile.length) {
            updateAuctionProduct($tile, risingAuctionTileUpdateMode, risingAuctionTileUpdateSuccess);
        }
    }


    // update bid history on PDP
    if (risingAuctionTileUpdateMode === 'pdp') {
        updateBidHistory(messageObj.pids);
    }


    // update quickview if necessary
    $quickviewContainer = $('#QuickViewDialog #ra-details-wrapper');
    if ($quickviewContainer.length) {
        for (i = 0; i < messageObj.pids.length; i++) {
            if ($quickviewContainer.data('itemid') === messageObj.pids[i]) {
                updateAuctionProduct($quickviewContainer, 'pdp');
                break;
            }
        }
    }
}




/**
*  IBM Tracking Tag handler
*/
function placeBidElementTag($element) {
    var $container = $element.closest('.ra-product-tile'),
        url,
        bid,
        previousBid;

    if ($container.length === 0) {
        $container = $element.closest('.ra-details-wrapper');
    }

    url = $container.data('ibmtag-url');
    bid = $container.find('input.bid-value').val();
    previousBid = $container.find('.price-bids-amount').text().match(/[0-9]*(\.|,)[0-9]{2}/);
    $.getScript(util.appendParamsToUrl(url, {
        bid: bid,
        previousBid: previousBid && previousBid[0],
        isAutoBid: $element.hasClass('bid-auto')
    }));
}




/**
*  Handler for Place Bid action - success callback
*/
function placeBidSuccessHandler(response, $element) {
    var $container = $element.closest('.ra-product-tile');
    var $bidProductParent = (pageContext.type == "product")?$($element).parents('.ra-details-wrapper'):$($element).parents('.product-tile-part-bottom');
		$bidProductParent = ($bidProductParent.length > 0)?$bidProductParent:$($element).parents('.ra-details-wrapper');
    if (response.success) {
        placeBidElementTag($element);
        return undefined;
    }
    if ($container.length) {
		$bidProductParent.find('.progress-loader').css("display","none");
		$bidProductParent.find('.ra-overlay').css("display","none");
    }
    var $errorMessage = $('<div class="error-message">' + response.errorMsg + '</div>');
    $('body').append($errorMessage);
    $errorMessage.dialog({
        draggable: false,
        resizable: false,
        modal: true,
        close: function () {
            $errorMessage.dialog('destroy');
            $errorMessage.remove();
        }
    });
}




/**
*  Handler for Place Bid action
*/
function placeBid($element) {

    var targetURL = $element.attr('href'),
        bidValue = $element.closest('.ra-bid-action-wrapper').find('input.bid-value').val(),
        $bidProductParent = (pageContext.type == "product")?$($element).parents('.ra-details-wrapper'):$($element).parents('.product-tile-part-bottom'),
        bidProduct = util.getParamFromUrl('productid',targetURL),
        $tile,
        i,
        $quickviewContainer;
    $bidProductParent = ($bidProductParent.length > 0)?$bidProductParent:$($element).parents('.ra-details-wrapper');
	console.group('Bid-'+bidProduct);
	console.log('Bid Product: '+bidProduct);
	console.log('Bid Value: '+bidValue);

    $.ajax({
        url: targetURL,
        dataType: 'json',
        data: {bid: bidValue},
        type: 'POST',
        success: function (response) {
        	console.log('Response: Success');
        	if(response.success == 'true' || response.success == true){
        		console.log('Bid Success');
        		/** Updating tile **/
        		if (risingAuctionTileSelector.length) {
                    $tile = $risingAuctionContainer.find(risingAuctionTileSelector +
                            '[data-itemid="' + bidProduct + '"]');
                } else {
                    if ($risingAuctionContainer.data('itemid') === bidProduct) {
                        $tile = $risingAuctionContainer;
                    }
                }
                if ($tile !== undefined && $tile.length) {
                	console.log('Updating: '+'[data-itemid="' + bidProduct + '"]');
                    updateAuctionProduct($tile, risingAuctionTileUpdateMode, risingAuctionTileUpdateSuccess);
                }
                
                // update bid history on PDP
                if (risingAuctionTileUpdateMode === 'pdp') {
                	console.log('Updating: Bid History for '+bidProduct);
                    updateBidHistory(bidProduct);
                }
                
                // update quickview if necessary
                $quickviewContainer = $('#QuickViewDialog #ra-details-wrapper');
                if ($quickviewContainer.length) {
                    if ($quickviewContainer.data('itemid') === bidProduct) {
                    	console.log('Updating: Quick View for '+bidProduct);
                    	updateAuctionProduct($quickviewContainer, 'pdp');
                    }
                }
                /** Updating tile **/
        	}else{
        		console.warn('Bid Failed');
        	}
        	//console.log('Bid Response: '+JSON.stringify(response));
            placeBidSuccessHandler(response, $element);
            $bidProductParent.find('.progress-loader').css("display","none");	
        	$bidProductParent.find('.ra-overlay').css("display","none");
        	$bidProductParent.removeClass('processing');
        	console.groupEnd('Bid-'+bidProduct);
        },
        error: function(jqXHR, exception){
        	console.warn('Response: Error');
        	console.warn(jqXHR.status+': '+jqXHR.statusText);
        	$bidProductParent.find('.progress-loader').css("display","none");	
        	$bidProductParent.find('.ra-overlay').css("display","none");
        	$bidProductParent.removeClass('processing');
        	console.groupEnd('Bid-'+bidProduct);
        }
    });
}




/**
*  Handler for Remaining Time Refinement (Dropdown)
*/
function remainingTimeRefinmentHandlerDropdown($element) {
    var url = $element.val();
    window.location.href = url;
    return;
}




/**
 *  Initialize a single Time Counter
 */
function initTimeCounter($timeCounter) {

    var remainingTime =  parseFloat($timeCounter.data('endTime') - new Date().getTime(), 10),
    // var remainingTime = parseFloat($timeCounter.data('remaining-time'), 10),
        expiratonTreshold = parseFloat($timeCounter.data('expiration-treshold')) * 60000,
        timeCounterInterval;

    // start/stop time counter
    timeCounterInterval = setInterval(function () {
        remainingTime -= 1000; 	
        
     // calculate remaining time values
        var remainingDays,
            remainingHours,
            remainingMinutes,
            remainingSeconds,
            millisRemaining = remainingTime,
            millisPerDay = 86400000,
            millisPerHour = 3600000,
            millisPerMinute = 60000,
            millisPerSecond = 1000,
            renderExpirationFlag = remainingTime <= expiratonTreshold,
            reachedLastSeconds = remainingTime <= 10 * 1000;


        if (millisRemaining <= 0) {
            remainingDays = 0;
            remainingHours = 0;
            remainingMinutes = 0;
            remainingSeconds = 0;
        } else {
            remainingDays = Math.floor(millisRemaining / millisPerDay);
            millisRemaining = millisRemaining % millisPerDay;
            remainingHours = Math.floor(millisRemaining / millisPerHour);
            millisRemaining = millisRemaining % millisPerHour;
            remainingMinutes = Math.floor(millisRemaining / millisPerMinute);
            millisRemaining = millisRemaining % millisPerMinute;
            remainingSeconds = Math.floor(millisRemaining / millisPerSecond);
        }

        // update HTML
        if (remainingDays <= 0) {
            $timeCounter.find('.row-values .days').remove();
            $timeCounter.find('.row-values .separator.days-separator').remove();
        } else {
            $timeCounter.find('.row-values .days').html(remainingDays);
            $timeCounter.find('.row-values .days').removeClass('hidden');
            $timeCounter.find('.row-values .separator.days-separator').removeClass('hidden');
        }

        if (remainingDays <= 0 && remainingHours <= 0) {
            $timeCounter.find('.row-values .hours').remove();
            $timeCounter.find('.row-values .separator.hours-separator').remove();
        } else {
            $timeCounter.find('.row-values .hours').html(remainingHours);
            $timeCounter.find('.row-values .hours').removeClass('hidden');
            $timeCounter.find('.row-values .separator.hours-separator').removeClass('hidden');
        }

        if (remainingDays <= 0 && remainingHours <= 0 && remainingMinutes <= 0) {
            $timeCounter.find('.row-values .minutes').remove();
            $timeCounter.find('.row-values .separator.minutes-separator').remove();
        } else {
            $timeCounter.find('.row-values .minutes').html(remainingMinutes);
            $timeCounter.find('.row-values .minutes').removeClass('hidden');
            $timeCounter.find('.row-values .separator.minutes-separator').removeClass('hidden');

        }

        if (remainingDays <= 0 && remainingHours <= 0 && remainingMinutes <= 0 && remainingSeconds <= 0) {
            $timeCounter.find('.row-values .seconds').remove();
            $timeCounter.find('.row-values .separator.seconds-separator').remove();
        } else if (remainingDays <= 0 && remainingHours <= 0 && remainingMinutes <= 0 && remainingSeconds <= 59) {
            $timeCounter.find('.row-values .separator.seconds-separator').remove();
            $timeCounter.find('.row-values .seconds').html(remainingSeconds + ' <span class="seconds-left">seconds left<span>');
            $timeCounter.find('.row-values .seconds').removeClass('hidden');
        } else {
                $timeCounter.find('.row-values .seconds').html(remainingSeconds);
                $timeCounter.find('.row-values .separator.seconds-separator').removeClass('hidden');
                $timeCounter.find('.row-values .seconds').removeClass('hidden');
        }

        if (renderExpirationFlag) {
            $timeCounter.addClass('red');
            if (reachedLastSeconds) {
                $timeCounter.addClass('pulse-slow');
            }
        }
        
        
        if (remainingTime <= 0) {
            clearInterval(timeCounterInterval);
			$( "#primary #ra-auctionswon-main" ).load(" #ra-auctionswon-main > *" );
        }
    }, 1000);
    timeCounterIntervals.push(timeCounterInterval);    
}




/**
 * Reinitializes all time counters
 */
function reInitTimeCounters() {
    
    $('.ra-timer-plp-pdp .time-counter').each(function(){		
		raTimer($(this));
	});
}




/**
 * Initialize common components of rising auction pages
 *
 * - Value Input Fields
 * - Bid Now Buttons
 * - Add-to-Cart Buttons
 * - Login Buttons
 * - PubNub Notifications
 * - Time Counters
 *
 */
function initCommonEventHandlers($container) {
    if (!$container.length) {
        return;
    }


    // Init input fields event handlers
    $container
        .on('focusin', 'input.bid-value', function () {
            $(this).addClass('focussed');
            $(this).attr('oldValue', $(this).val());
        })
        .on('focusout', 'input.bid-value', function () {
            $(this).removeClass('focussed');
            var newValue = parseFloat($(this).val()),
                oldValue = $(this).attr('oldValue');

            if (!newValue) {
                $(this).val(oldValue);
            }
            $(this).removeAttr('oldValue');
        });


    // Init "Bid Now" button event handlers
    $container
        .on('mouseenter', '.bid-button', function () {
            $(this).addClass('focussed');
        })
        .on('mouseleave', '.bid-button', function () {
            $(this).removeClass('focussed');
        })
        .on('click', '.bid-button', function (event) {

            event.preventDefault();            
            // add progress icon
            if (!$(event.target).hasClass("ra-add-to-cart")) {
                var $con = $(this).closest('.ra-product-tile'),
                	$bidProductParent = (pageContext.type == "product")?$(this).parents('.ra-details-wrapper'):$(this).parents('.product-tile-part-bottom');
                $bidProductParent = ($bidProductParent.length > 0)?$bidProductParent:$(this).parents('.ra-details-wrapper');
                if ($bidProductParent.length) {
                    var $loader = $('<div/>').addClass('progress-loader').append($('<i/>').addClass('fa fa-spinner fa-pulse'));
                    $bidProductParent.addClass('processing');
                    $loader.appendTo($bidProductParent);
                    $bidProductParent.append('<div class="ra-overlay"></div>');
                }
            }
            
            placeBid($(this));         
            /*setTimeout(function(){  	
                $('.progress-loader').css("display","none");	
                $('.ra-overlay').css("display","none"); 	
            },3000);*/
        });  
    
    
    // init add to cart buttons
    addToCart($container);


    // init login buttons (used for redirecting after logging in)
   /* $container.on('click', '.ra-login-btn', function (event) {
        event.preventDefault();
        //window.location.href = $('#ra-login-link').attr('href');
    });*/


    // init update of product tiles on pubnub notification
    if (!pubNubInitialized) {
        pubnub.init(pubnubChannelName, handleNotification, function () {
            pubNubInitialized = true;
        });
    }
    
    $('.ra-timer-plp-pdp .time-counter').each(function(){		
		raTimer($(this));
	});

    $container.find('.ra-bid-action-wrapper').find('input.bid-value').keypress( function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    });

    // init bid history
    initBidHistory();
    $('#ra-detail-bid-history').on('click', '.ra-detail-bid-history-show-more', function (event) {
        event.preventDefault();
        bidHistorySize += 10;
        initBidHistory();
    });
}
/**
* Initialize Listing Page
*
* - General Components
* - Special Refinements (By Auction Duration)
* - Automatic Update of Auction Product Tiles
*
*/
function initRisingAuctionsListingPage() {
	var $container = $('.pt_product-search-result.context-auctionshop');
    if (!$container.length) {
        return;
    }

    // save container and tile selector (for later use in handleNotification)
    $risingAuctionContainer = $container;
    risingAuctionTileSelector = '.ra-product-tile .product-tile-part-bottom';
    risingAuctionTileUpdateMode = '';

    // init general components
    initCommonEventHandlers($container);

    // ??
    $container.find('.ra-product-tile').on('click', '.thumb-link, .name-link', function () {
        var $productLink = $(this),
            $tile = $productLink.parents('.product-tile'),
            cgid = $tile.data('cgid'),
            isSearch = $('.search-result-content').data('issearchwithquery');
        if (cgid) {
            $productLink[0].hash = 'cgid=' + encodeURIComponent(cgid);
        }
        if (isSearch) {
            if (cgid) {
                $productLink[0].hash += '&issearch=true';
            } else {
                $productLink[0].hash = 'issearch=true';
            }
        }
    });

    if (!$container.length) {
        return;
    }
    

    // init add to cart buttons
    addToCart($container, function ($button) {
        var $theContainer = $button.closest('.add-to-cart-btn-container'),
            updateURL = $theContainer.data('update-url');

        $.ajax({
            url: updateURL,
            dataType: 'html',
            type: 'GET',
            success: function (response) {
                $theContainer.html(response);
            }
        });
    });
    
    setInterval(recordCount, 3000);     
}

/**
* Initialize Detail Page
*
* - General Components
* - Automatic Update of Auction Product Details
*
*/
function initRisingAuctionsDetailPage() {
    var $container = $('.ra-details-wrapper:visible');
    if (!$container.length) {
        return;
    }

    // save container and tile selector (for later use in handleNotification)
    $risingAuctionContainer = $container;
    risingAuctionTileSelector = '';
    risingAuctionTileUpdateMode = 'pdp';

    // init general components
    initCommonEventHandlers($container);


    // init "show more" button for bid history
    $('.section-ra-bid-history').on('click', '.ra-detail-bid-history-show-more', function () {
        $(this).data('visible-rows', $(this).data('visible-rows') + 10);
    });
    
 // init add to cart buttons
    addToCart($container, function ($button) {
        var $theContainer = $button.closest('.add-to-cart-btn-container'),
            updateURL = $theContainer.data('update-url');

        $.ajax({
            url: updateURL,
            dataType: 'html',
            type: 'GET',
            success: function (response) {
                $theContainer.html(response);
            }
        });
    });
}

/**
* Initialize Watchlist / Favourites Page
*
*  - General Components
*  - Automatic Update of Auction Product Details
*
*/
function initRisingAuctionsWatchlistFavourites() {	
    var $container = $('#ra-watchlist-favourites-main');
    if (!$container.length) {
        return;
    }

    // save container and tile selector (for later use in handleNotification)
    $risingAuctionContainer = $container;
    risingAuctionTileSelector = '.ra-product-tile-table-row';
    risingAuctionTileUpdateMode = '';
    risingAuctionTileUpdateSuccess = 'table';


    // init general components
    initCommonEventHandlers($container);


    // init special refinment dropdown (remaining time)
    $container.on('change', '.refinement.remaining-time select', function (event) {
        event.preventDefault();
        return remainingTimeRefinmentHandlerDropdown($(this));
    });
    setInterval(recordCount, 3000); 
}

/**
* Initialize Bid History Page
*
* - Add to Cart Buttons
* - Update Time Counter
*
*/
function initRisingAuctionsBidHistory() {
    var $container = $('#ra-bidhistory-main');

    if (!$container.length) {
        return;
    }

    // init add to cart buttons
    addToCart($container, function ($button) {
        var $theContainer = $button.closest('.add-to-cart-btn-container'),
            updateURL = $theContainer.data('update-url');

        $.ajax({
            url: updateURL,
            dataType: 'html',
            type: 'GET',
            success: function (response) {
                $theContainer.html(response);
            }
        });
    });
    
    $('.ra-timer-plp-pdp .time-counter').each(function(){		
		raTimer($(this));
	});
    
    setInterval(recordCount, 3000);
}

/**
* Init Auctions Won Page
*
* - Add to Cart Buttons
* - Update Time Counter
*
*/
function initRisingAuctionsAuctionsWon() {
    // check presence of container
    var $container = $('#ra-auctionswon-main');
    if (!$container.length) {
        return;
    }


    // init add to cart buttons
    addToCart($container, function ($button) {
        var $theContainer = $button.closest('.add-to-cart-btn-container'),
            updateURL = $theContainer.data('update-url');

        $.ajax({
            url: updateURL,
            dataType: 'html',
            type: 'GET',
            success: function (response) {
                $theContainer.html(response);
            }
        });
    });
    
    $('.ra-timer-plp-pdp .time-counter').each(function(){		
		raTimer($(this));
	});
    
    setInterval(recordCount, 3000);
}

/**
* Init Quickview as soon as quickview container is opened
*/
function openRisingAuctionsQuickview() { 
    var $container = $('#QuickViewDialog #ra-details-wrapper');
    if (!$container.length) {
        return;
    }


    // init general components
    initCommonEventHandlers($container);   
    $('#QuickViewDialog #ra-details-wrapper').on('click', '.ra-add-to-cart', function () { 
    	 var a = $('#ra-details-wrapper').find('#pid').val();
    	 $("#pid").closest('.search-result-items').find(".add-to-cart-btn-container").each(function() {
    	var b=$(this).find('#pid').val();
    	if(a === b){
    	$(this).find( ".ra-add-to-cart" ).trigger("click");
    	}

    	});
    	 });
   
} 

/**
* Remove Quickview update when overlay is closed
*/
function closeRisingAuctionsQuickview() {
    reInitTimeCounters();
}

$(window).on('scroll', function () {
var nav = $('#ajaxRefresh');
if (nav.length) {
  var contentNav = nav.offset().top;
  var pageTop = $(window).scrollTop();
  if (pageTop >= contentNav) {
      $('.xgrid').addClass('RA-sticky-on');      
     
     
  } else {
      $('.xgrid').removeClass('RA-sticky-on');
  }
}
});

/*RA Action button slider in mobile*/
    $('.slider-nav').not('.slick-initialized').slick({
        autoplay: false,
        arrows: true,
       responsive: [
        {
            breakpoint: 767,
             settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                }
        }
    ] 
        
        
    }); 
   
$(window).on('scroll', function () {
var filtermove = $('.listing-slot-container-bottom');
var bottomcontent = $('.listing-slot-container-bottom').outerHeight();
if (filtermove.length) {
  var filtercontent = filtermove.offset().top - 800;
  var pageTop = $(window).scrollTop();
  if (pageTop >= filtercontent) {
	   $('.context-auctionshop .secondary-col').css({position: "absolute", bottom: bottomcontent});
 } 
}
});    


function raTimer($raTimerClass){
    	// Set the date we're counting down to
    	var countDownDate = $raTimerClass.attr("data-end-time");
    	var raExpiratonTreshold = parseFloat($raTimerClass.attr("data-expiration-treshold")) * 60000;

    	// Update the count down every 1 second
    	var x = setInterval(function() {

    	  // Get today's date and time
    	  //var now = new Date().getTime();
    	  var now = $('#getRAserverTime').text();
    	    
    	  // Find the distance between now and the count down date
    	  var distance = countDownDate - now;
    	    
    	  // Time calculations for days, hours, minutes and seconds
    	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    	  var raRenderExpirationFlag = distance <= raExpiratonTreshold;
    	  var raReachedLastSeconds = distance <= 10 * 1000;
    	    
    	  // Output the result in an element with class=ra-timer"
    	  if(days > 0 && hours >= 0 && minutes >= 0 && seconds >= 0){
    		  $raTimerClass.find(".ra-timer").html(days + "<span>d</span> " + hours + "<span>h</span> "+ minutes + "<span>m</span> " + seconds + "<span>s</span> ");
    	  }
    	  if(days <= 0 && hours > 0 && minutes >= 0 && seconds >= 0){
    		  $raTimerClass.find(".ra-timer").html( hours + "<span>h</span> "+ minutes + "<span>m</span> " + seconds + "<span>s</span> ");
    	  }
    	  if(days <= 0 && hours <= 0 && minutes > 0 && seconds >= 0){
    		  $raTimerClass.find(".ra-timer").html( minutes + "<span>m</span> " + seconds + "<span>s</span> ");
    		  if(minutes < 5 ){
    			  $raTimerClass.find(".ra-timer").addClass('red');
    		  }
    	  }
    	  if(days <= 0 && hours <= 0 && minutes <= 0 && seconds >0){
    		  $raTimerClass.find(".ra-timer").html( seconds + "<span> seconds left</span> ");
    	  }
    	  
    	  if (raRenderExpirationFlag) {
    		  $raTimerClass.find(".ra-timer").addClass('red');
              if (raReachedLastSeconds) {
                  $raTimerClass.find(".ra-timer").addClass('pulse-slow');
              }
    	  }
    	    
    	  // If the count down is over, write some text 
    	  if (distance <= 0) {
    	    clearInterval(x);
    	    $( "#primary #ra-auctionswon-main" ).load(" #ra-auctionswon-main > *" );
    	    $raTimerClass.find(".ra-timer").text("");
    	  }
    	}, 1000);
    }

	$('.ra-add-to-cart').one('click',function(){
		$(this).closest('form').closest('.add-to-cart-btn-container').find('.ra-add-to-cart').trigger('click');  
	}); 
/* Module Exports */
/* ************** */
exports.init = function () {
    initRisingAuctionsListingPage();
    initRisingAuctionsDetailPage();
    initRisingAuctionsWatchlistFavourites();
    initRisingAuctionsBidHistory();
    initRisingAuctionsAuctionsWon();
    newsignin.init();
    getServerTime();
};
exports.openQuickview = openRisingAuctionsQuickview;
exports.closeQuickview = closeRisingAuctionsQuickview;
exports.reInitTimeCounters = reInitTimeCounters;