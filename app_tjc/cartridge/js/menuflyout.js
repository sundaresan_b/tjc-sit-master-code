/*global FastClick: false */
'use strict';

var util = require('./util');

var menuflyout = {
    init: function ($el) {
        var $trigger = $el.find('.js-menuflyout-trigger'),
            $content = $el.find('.js-menuflyout-content'),
            timer = util.timer(),
            slide,
            close;

        if (!$trigger.length || !$content.length) {
            return {};
        }


        FastClick.attach($content[0]);

        slide = function () {
            timer.clear();
            $trigger.addClass('hover');
            $content.stop(true, true).show();
            timer.start(6000, close);
        };

        close = function () {
            timer.clear();
            $content.hide();
            $trigger.removeClass('hover');
        };

        $trigger
            .on('touchstart', function (ev) {
                if (!$content.is(':visible')) {
                    slide();
                    ev.preventDefault();
                }
            })
            .on('mouseenter', function () {
                if ($content.is(':visible')) {
                    timer.clear();
                } else {
                    slide();
                }
            })
            .on('mouseleave', timer.start.bind(this, 30, close));

        $content
            .on('mouseenter', timer.clear)
            .on('mouseleave', timer.start.bind(this, 30, close));

        return {
            slide: slide,
            close: close
        };
    }
};

module.exports = menuflyout;
