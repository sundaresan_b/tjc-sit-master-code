/*jslint nomen: true */
'use strict';

var _ = require('lodash'),
    Modernizr = require('modernizr');
require('jquery-touchswipe');

var carousel,
    $window = $(window),
    hasTransitions = Modernizr.csstransitions,
    supportsTransform3D = Modernizr.csstransforms3d,
    prevNext = function (dir, e) {
        var that = this;
        if (e) {
            e.preventDefault();
            if ($(e.currentTarget).hasClass('disabled')) {
                return;
            }
        }

        if (!that.scrolling) {
            that.scrolling = true;
            that.goTo(that.currentIndex + dir).done(function () {
                that.scrolling = false;
            });
        }
    };

function CarouselView() {
    return {
        backboneViewFakerInit: function ($el) {
            var that = this;
            that.$el = $el;
            that.$ = function (selector) {
                that.$el.find(selector);
            };
            that.trigger = function (event, data) {
                that.$el.trigger(event, data);
            };
            that.on = function (event) {
                that.$el.on(event);
            };
            _.forOwn(that.events, function (method, event) {
                var match;
                if (!_.isFunction(method)) {
                    method = that[method];
                }
                if (method) {
                    /*jslint regexp: true*/
                    match = event.match(/^(\S+)\s*(.*)$/);
                    /*jslint regexp: false*/
                    that.$el.on(match[1], match[2], _.bind(method, that));
                }
            });
        },

        events: {
            'click .nav-next': 'next',
            'click .nav-prev': 'prev',
            'click .bubble': 'bubbleClick'
        },

        excludedElements: _.without($.fn.swipe.defaults.excludedElements.split(', '), 'a').join(', '),
        itemWidth: null,
        currentIndex: 0,
        posAdd: 0,
        maxItems: 0,
        maxItemsAll: 0,
        visibleItems: 1,
        $carousel: null,
        $shadowEl: null,
        $imageItems: null,
        startSwipeScrollPos: null,
        scrolling: false,
        lastWindowWidth: null,
        automaticChangeTimer: null,
        bubblesInitiated: false,
        bubblesActiveIcon: 'fa-circle',
        bubblesInactiveIcon: 'fa-circle-thin',
        isInfinite: false,
        options: {
            infinite: false,
            automaticChangeTime: false,
            showBubbles: false,
            speed: 500
        },

        initialize: function (config) {
            var that = this,
                $shadowEl = config.$shadowEl,
                options = config.options;
            if (options) {
                $.extend(that.options, config.options);
            }
            if ($shadowEl) {
                this.$shadowEl = $shadowEl;
            }
            that.isInfinite = that.options.infinite;
            that.backboneViewFakerInit(config.$el);
            that.reinitComplete();
        },
        getImageItems: function () {
            return $('li', this.$carousel);
        },
        preReinit: function () {
            var that = this;
            if (that.isInfinite) {
                that.infiniteItems = that.infiniteItems || 1;
                that.infiniteMin = that.infiniteMin || 2;
                if (that.$carousel) {
                    that.getImageItems().filter('.inf').remove();
                    that.$imageItems = that.getImageItems();
                }
            }
        },
        reinit: function () {
            var that = this,
                $el = that.$el,
                $carousel = $('.carousel', $el),
                $imageItems;
            that.preReinit();
            that.$carousel = $carousel;
            $imageItems = that.getImageItems();
            that.$imageItems = $imageItems;
            that.currentIndex = 0;
            that.maxItems = that.maxItemsAll = $imageItems.length;
            that.reinitBeforeResponsive();
            that.initResponsive();
        },
        reinitBeforeResponsive: function () {
            if (this.isInfinite) {
                var that = this,
                    $imageItems = that.$imageItems,
                    infiniteItems = that.infiniteItems,
                    $prependItems,
                    $appendItems;
                that.isInfinite = false;

                // maybe this needs to get more complex in the future
                // Other values can be configured by the view. Save and restore these values first
                that.posAdd = 0;
                that.noNavDisable = false;

                if ($imageItems.length > that.infiniteMin - 1) {
                    that.isInfinite = true;
                    $prependItems = $imageItems.slice(-infiniteItems).clone();
                    $appendItems = $imageItems.slice(0, infiniteItems).clone();
                    $prependItems.add($appendItems).addClass('inf');
                    that.$carousel.prepend($prependItems).append($appendItems);
                    that.$imageItems = that.getImageItems();
                    that.posAdd = infiniteItems;
                    that.maxItemsAll = that.$imageItems.length;
                    that.noNavDisable = true;
                }
            }
        },
        postReinit: function () {
            var that = this;
            that.goToInt(0, true);
            that.initCarousel();
            that.scrollImages((that.currentIndex + that.posAdd) * that.itemWidth, 0, true);
        },
        reinitComplete: function () {
            this.reinit();
            this.postReinit();
        },
        adjustSizeInt: function () {
            var that = this,
                $el = that.$el,
                $carouselContainerInner = $('.carousel-container-inner', $el),
                $imageItems = that.$imageItems,
                $carousel = that.$carousel,
                $shadowEl = that.$shadowEl,
                itemWidth;
            if ($shadowEl) {
                itemWidth = $('li', $shadowEl).width();
            } else {
                $imageItems.add($carousel).css('width', '');
                $carouselContainerInner.css({
                    overflow: '',
                    'transition-duration': 0
                });
                itemWidth = $imageItems.width();
            }
            $imageItems.width(itemWidth);
            $carousel.width(itemWidth * that.maxItemsAll);
            $carouselContainerInner.css({
                overflow: 'hidden'
            });
            that.itemWidth = itemWidth;
            that.visibleItems = $carouselContainerInner.length ?
                    Math.round($carouselContainerInner.width() / itemWidth) : 1;
        },
        adjustSize: function (triggeredByResizeEvent) {
            var that = this,
                windowWidth = $window.width();
            if (!triggeredByResizeEvent || (windowWidth !== that.lastWindowWidth && that.$el.is(':visible'))) {
                that.adjustSizeInt();
                that.bubblesInitiated = false;
                that.goToInt(that.currentIndex, true);
                that.lastWindowWidth = windowWidth;
            }
        },
        initResponsive: function () {
            var adjustSize = _.bind(this.adjustSize, this);
            $window.on('resize', _.debounce(_.partial(adjustSize, true), 10));
            adjustSize();
        },
        initCarousel: function () {
            var that = this;
            if (hasTransitions && that.maxItems > 1) {
                /*jslint unparam: true */
                that.$carousel.swipe({
                    triggerOnTouchEnd: true,
                    preventDefaultEvents: false,
                    swipeStatus: function (event, phase, direction, distance) {
                        var duration = 0,
                            currentIndex = that.currentIndex,
                            itemWidth = that.itemWidth,
                            posAdd = that.posAdd,
                            currentIndexPos = itemWidth * (currentIndex + posAdd),
                            scrollImages = _.bind(that.scrollImages, that),
                            isALink,
                            leftOrRight = direction === 'left' || direction === 'right',
                            posToGo,
                            getNewIndexPos = function () {
                                var newIndexPos = currentIndexPos;
                                if (direction === 'left') {
                                    newIndexPos += distance;
                                } else if (direction === 'right') {
                                    newIndexPos -= distance;
                                }
                                return newIndexPos;
                            },
                            theNewIndexPos = getNewIndexPos(),
                            calculateGoToPos = function () {
                                var tileToShow = theNewIndexPos / itemWidth,
                                    posInTile = (tileToShow - Math.floor(tileToShow)) * 100,
                                    newPos = currentIndexPos;
                                if (direction === 'left') {
                                    newPos = posInTile > 25 ? Math.ceil(tileToShow) : Math.floor(tileToShow);
                                } else if (direction === 'right') {
                                    newPos = posInTile < 75 ? Math.floor(tileToShow) : Math.ceil(tileToShow);
                                }
                                return newPos - posAdd;
                            };
                        if (phase === 'start') {
                            that.startSwipeScrollPos = $window.scrollTop();
                        }
                        //If we are moving before swipe, and we are going L or R in X mode, or U or D in Y mode then
                        //drag.
                        if (phase === 'move' && leftOrRight) {
                            scrollImages(theNewIndexPos, duration);
                        } else if (phase === 'cancel') {
                            if (currentIndexPos === getNewIndexPos() &&
                                    $window.scrollTop() === that.startSwipeScrollPos) {
                                that.$carousel.trigger('carousel:tap');
                            }
                            that.startSwipeScrollPos = null;
                            scrollImages(currentIndexPos, that.options.speed);
                        } else if (phase === 'end') {
                            if (leftOrRight) {
                                posToGo = calculateGoToPos();
                                if (that.isInfinite) {
                                    that.goTo(posToGo);
                                } else {
                                    that.goTo(posToGo < 0 ? 0 :
                                            (posToGo > that.maxItems - 1 ? that.maxItems - 1 : posToGo));
                                }
                            }
                            isALink = $(event.target).parentsUntil(that.$el, 'a');
                            if (isALink.length) {
                                isALink.one('click', function (e) {
                                    e.preventDefault();
                                });
                            }
                            that.startSwipeScrollPos = null;
                        }
                    },
                    allowPageScroll: 'vertical',
                    threshold: 75,
                    excludedElements: that.excludedElements
                });
                /*jslint unparam: false */
            }
            that.setNavComponents();
        },
        scrollValues: function (cssVals, vals, distance, duration, dontScroll) {
            //inverse the number we set in the css
            var value = (distance < 0 ? '' : '-') + Math.abs(distance).toString(),
                dur;
            cssVals.transform = supportsTransform3D ?
                    'translate3d(' + value + 'px,0,0)' : 'translate(' + value + 'px,0)';
            vals.transform = value;
            if (!dontScroll) {
                dur = duration / 1000;
                cssVals['transition-duration'] = dur.toFixed(1) + 's';
                vals.duration = dur;
            } else {
                vals.duration = 0;
            }
        },
        scrollImages: function (distance, duration, dontScroll) {
            var cssVals = {},
                vals = {},
                $carousel = this.$carousel,
                deferred = new $.Deferred(),
                transitionFinished = function () {
                    $carousel.css('transition-duration', '0s');
                    deferred.resolve();
                };
            this.scrollValues(cssVals, vals, distance, duration, dontScroll);
            if (hasTransitions) {
                $carousel.css(cssVals);
                if (dontScroll) {
                    transitionFinished();
                }
                $carousel.one('transitionend', transitionFinished);
            } else {
                $carousel.animate({
                    myTransform: vals.transform
                }, {
                    step: function (now) {
                        $carousel.css({
                            transform: 'translate(' + now + 'px,0)'
                        });
                    },
                    complete: function () {
                        deferred.resolve();
                    }
                }, vals.duration);
            }
            return deferred.promise();
        },
        prev: _.partial(prevNext, -1),
        next: _.partial(prevNext, 1),
        bubbleClick: function (e) {
            var that = this,
                $target = $(e.target),
                targetPage = $target.index();
            that.goTo(targetPage * that.visibleItems);
        },
        stopAutomaticChange: function () {
            if (this.automaticChangeTimer) {
                window.clearTimeout(this.automaticChangeTimer);
            }
        },
        startAutomaticChange: function () {
            var that = this;
            that.stopAutomaticChange();
            that.automaticChangeTimer = window.setTimeout(function () {
                that.next();
            }, that.options.automaticChangeTime * 1000);
        },
        goToInt: function (index, dontScroll) {
            var that = this,
                currentIndex = that.enforceBoundaries(index),
                realIndex = that.getRealGoToPos(index),
                deferred = new $.Deferred();

            that.stopAutomaticChange();
            that.currentIndex = realIndex;
            that.scrollImages(that.itemWidth * (currentIndex + that.posAdd),
                                dontScroll ? 0 : that.options.speed, dontScroll).done(
                function () {
                    if (currentIndex !== realIndex) {
                        that.goToInt(realIndex, true).done(function () {
                            deferred.resolve();
                        });
                    } else {
                        deferred.resolve();
                    }
                }
            );
            that.setNavComponents();
            return deferred.promise();
        },
        goTo: function (index, dontScroll) {
            var that = this;
            return that.goToInt(index, dontScroll).done(function () {
                that.trigger('carousel:selected', that.currentIndex);
            });
        },
        getRealGoToPos: function (index) {
            if (this.isInfinite) {
                var realIndex = index,
                    maxItems = this.maxItems;
                if (index < 0) {
                    realIndex = maxItems + index;
                } else if (index >= maxItems) {
                    realIndex = index - maxItems;
                }
                return realIndex;
            }
            return this.enforceBoundaries(index);
        },
        getBoundaryMin: function () {
            if (this.isInfinite) {
                return -this.posAdd;
            }
            return 0;
        },
        getBoundaryMax: function () {
            var that = this;
            if (that.isInfinite) {
                return that.maxItems + that.posAdd - that.visibleItems;
            }
            return that.maxItems - that.visibleItems;
        },
        enforceBoundaries: function (index) {
            var that = this;
            return Math.max(Math.min(index, this.getBoundaryMax()), that.getBoundaryMin());
        },
        calcUnlimitedIndex: function (index) {
            var that = this;
            if (index < that.getBoundaryMin()) {
                return that.getBoundaryMax();
            }
            if (index > that.getBoundaryMax()) {
                return that.getBoundaryMin();
            }
            return index;
        },
        getCurrentItem: function () {
            return this.$imageItems.eq(this.currentIndex + this.posAdd);
        },
        setNavComponents: function () {
            var that = this;
            that.setNavigation();
            if (that.options.automaticChangeTime) {
                that.startAutomaticChange();
            }
            if (that.options.showBubbles) {
                if (!that.bubblesInitiated) {
                    that.createBubbles();
                    that.bubblesInitiated = true;
                }
                that.$el.find('.bubbles .bubble')
                    .addClass(that.bubblesInactiveIcon)
                    .removeClass(that.bubblesActiveIcon)
                    .eq(Math.floor(that.currentIndex / that.visibleItems))
                        .addClass(that.bubblesActiveIcon)
                        .removeClass(that.bubblesInactiveIcon);
            }
        },
        createBubbles: function () {
            var that = this,
                $bubbleContainer = that.$el.find('.bubble-ctr'),
                $bubbleList = $bubbleContainer.find('.bubbles').empty(),
                $bubble = $('<li/>', {'class': 'bubble fa'}),
                visibleItems = that.visibleItems <= 0 ? that.maxItems : that.visibleItems,
                bubbleCount = Math.ceil(that.maxItems / visibleItems),
                i;

            if ($bubbleContainer.length === 0) {
                $bubbleContainer = $('<div/>', {'class': 'bubble-ctr'});
                $bubbleList = $('<ul/>', {'class': 'bubbles'}).appendTo($bubbleContainer);
            }
            for (i = 0; i < bubbleCount; i++) {
                $bubbleList.append($bubble.clone());
            }
            that.$el.find('.carousel-container-inner').before($bubbleContainer);
        },
        setNavigation: function () {
            var that = this,
                classDisabled = 'disabled',
                $navPrev = $('.nav-prev', that.$el),
                $navNext = $('.nav-next', that.$el);

            if (that.maxItems <= 1 || that.maxItems <= that.visibleItems) {
                $navPrev.add($navNext).hide();
            } else {
                $navPrev.add($navNext).show();
            }
            $navPrev.add($navNext).removeClass(classDisabled);

            if (!that.options.infinite) {
                if (that.currentIndex === that.getBoundaryMin()) {
                    $navPrev.addClass(classDisabled);
                } else if (that.currentIndex === that.getBoundaryMax()) {
                    $navNext.addClass(classDisabled);
                }
            }
        }
    };
}

function CustomCarousel() {
	//Custom Slider
    if($('.custom-slider').length > 0) {
    
		$('.slider-tile').each( function(){
			if ($(this).find('.product-promo').text().trim().length > 0) {
				var el = $(this).find('.product-promo').detach();
				$(this).find('.price-standard').append(el);
			}
		});
		
	    var customSlider = function() {
	    	$('.custom-slider').each( function(){ 
				var sum = 0;
				$(this).find('.custom-slider-inner > a, .custom-slider-inner > img, .custom-slider-inner > div').each( function(){ 
					sum += $(this).width(); 
				});
				if($(this).width() > sum) {
					$(this).addClass('no-scroll');
				} else {
					$(this).removeClass('no-scroll');
				}
			});
			  
			var liWidth = 0;
			var maxliWidth = 0;
			var $customSlider = $('.custom-slider:eq(0)');
			$customSlider.find('.slider-tile, .custom-slider-inner > img').each(function() {
			    if (liWidth < $customSlider.width()) {
			        maxliWidth = liWidth;
			    }
			    liWidth +=  $(this).outerWidth(true);
			});
			
			$('.slideleft').off('click').on('click',function() {
		    	var $slidercontainer = $(this).closest('.custom-slider').find('.custom-slider-inner');
		        var scrLeft = $slidercontainer.scrollLeft() - maxliWidth;
		        $slidercontainer.animate( { scrollLeft: scrLeft }, 300);
		    });
		   
		    $('.slideright').off('click').on('click',function() {
		    	var $slidercontainer = $(this).closest('.custom-slider').find('.custom-slider-inner');
		        var scrRight = $slidercontainer.scrollLeft() + maxliWidth;
		        $slidercontainer.animate( { scrollLeft: scrRight }, 300);
		    });
	    };
	    
	    customSlider();
    	
    	$(window).resize(function(){
		  customSlider();
		});
    }
}

carousel = {
    init: function () {
        $('.carousel-container').each(function () {
            var $el = $(this),
                options = $el.data();
            new CarouselView().initialize({$el: $el, options: options});
            $el.addClass('loaded');
        });
        
        CustomCarousel();
    }
};

window.initRecommendationsCarousel = carousel.init;

module.exports = carousel;
