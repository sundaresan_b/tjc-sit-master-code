'use strict';

var $ = require('jquery');

var initialized = false;
var $document = $(document);

function flyin() {
    $document.on('click', '.flyin-trigger', function (e) {
        $('.flyin,.fade-overlay').toggleClass('flyin-active');
        $('.header-mobile-search-trigger,.header-mobile-search').removeClass('active');
        $('#wrapper .header-wrapper').css('z-index', '2px');
        $('.header').toggleClass('flyin-hide-header');
        $('.pdp-sticky').toggleClass('flyin-hide-pdp-sticky');
        $('.backtotop').toggleClass('flyin-hide-pdp-content');
        //$('.main-wrapper').toggleClass('flyin-hide-pdp-content');
         $('body').css('overflow', 'hidden');
        e.preventDefault();

    }).on('click', '.flyin-close', function (e) {
        $('.flyin,.fade-overlay').removeClass('flyin-active');
        $('#wrapper .header-wrapper').css('z-index', '3px');
        $('.header').toggleClass('flyin-hide-header');
        $('.pdp-sticky').toggleClass('flyin-hide-pdp-sticky');
        $('.backtotop').toggleClass('flyin-hide-pdp-content');
       // $('.main-wrapper').toggleClass('flyin-hide-pdp-content');
        $('body').css('overflow', 'visible');
        e.preventDefault();

    });

    initialized = !initialized;
}

module.exports = {
    init: function () {
        if (!initialized) {
            flyin();
        }
    }
};
