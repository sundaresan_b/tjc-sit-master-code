/*global PUBNUB: false */
'use strict';

/* Required Modules */



/* Local Variables / Configuration */
/* ******************************* */
var pubnub,
    subscribeKey,
    publishKey;


/* Functions */
/* ********* */
function init(channel, onMessageCallback, onConnectCallback) {

    // get site preferences
    subscribeKey = window.SitePreferences.PUBNUB_SKEY;
    publishKey = window.SitePreferences.PUBNUB_PKEY;
    if (!subscribeKey || !publishKey) {
        return null;
    }


    // initialize pubnub object
    /*eslint-disable */
    pubnub = PUBNUB({
        publish_key   : publishKey,
        subscribe_key : subscribeKey,
        ssl: true
    });
    /*eslint-enable */


    // subscribe to listen on channel
    pubnub.subscribe({
        channel : channel,
        message : onMessageCallback,
        connect: onConnectCallback
    });

    return pubnub;
}

exports.init = init;
