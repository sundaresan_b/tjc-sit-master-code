'use strict';

var dialog = require('./dialog'),
			 util = require('./util'),
			 ajax = require('./ajax'),
			 validator = require('./validator'),
			 page = require('./page'),
			 progress = require('./progress'),
			 flyinheader = require('./flyinheader'),
			 ratimer = require('./risingauctions'),
			 tjcplusactivation = require('./tjcplusactivation'),
			 newformstyle = require('./new-form-style');

function ViewHide(pre, show){
	var view,hide;
    var arr = ["title", "content", "field", "button"];

    $.each( arr, function( index, value ){
    	hide = "."+pre+"-"+value;
    	$(hide).addClass("d-none");
        hide = "";
    });
    
    $.each( arr, function( index, value ){
    	view = "."+show+"-"+value;
        $(view).removeClass("d-none");
        view = "";
    });
}

function initializeEvents() {
	
	if($( ".guest-class" ).length){
		$( ".guest-class" ).find('.guestbox').addClass('sigin-model-button');
	}
	
	$('#dwfrm_profile_customer_readtac').on('change',function(){
		var $tacCheckedProp = $(this).prop('checked');
		$('#dwfrm_profile_customer_addtomaillist').prop('checked', $tacCheckedProp);
		if($tacCheckedProp){
			$('#dwfrm_profile_customer_addtomaillist').parents('.checker').find('span').addClass('checked');
		}else{
			$('#dwfrm_profile_customer_addtomaillist').parents('.checker').find('span').removeClass('checked');
		}
	});
	
	/* auto focus on submit */
    if($('.personal-fileds select.input-select').val()) {
        $('.personal-fileds select.input-select').trigger('blur');
    }
	/* code added for TW-1455 Likelist signin journey*/
	 $('.pdp-main').on('click', '.product-wishlist-link', function () { 

    	$('.product-wishlist').addClass('wishlist');  	
    	 });
    $('.xlt-searchresults').on('click', '.wishlist-button', function () { 

    	$('.product-tile-buttons').addClass('wishlist');
    	
    });
	$('body').on('click', '.sigin-model-button, .live-tv-add-to-cart-form a.home-page-login-bid-now, .live-tv-add-to-cart-form a.login-live-tv, #checkout-form button.checkout-login-class, a.ra-login-btn, a.login-class' , function (e) {
	    e.preventDefault();
	    var isHeaderSignIn = $(this).hasClass('header-addnamelink');
	    /* ProductIDPLP added for TW-1455 Likelist signin journey*/
	    if (!isHeaderSignIn) {
	    	var ProductIDPLP = $(this).closest('.wishlist').find('.wishlistplp-pid').text();
			if (ProductIDPLP == "") {
				ProductIDPLP = $('.wishlist-pid').text();
			}
			if ( (pageContext.ns == 'product' || pageContext.ns == 'search' ) && (ProductIDPLP != "") ) {
			   if ( window.location.href.indexOf('?source=likelist') < 0) {
				   if ( window.location.href.indexOf('#') > -1 || pageContext.ns == 'product' ) {
					   var oldurl = window.location.href.split('#');
					   var url = oldurl[0] + "?source=likelist&pid="+ ProductIDPLP;
					   window.history.pushState({},'',url);
				   }
				   else{
						var url = window.location.href + "?source=likelist&pid="+ ProductIDPLP;
						window.history.pushState({},'',url);
				   }
				} else {
					var oldurl = window.location.href.split('?source=likelist&pid=');
					var url = oldurl[0] + "?source=likelist&pid="+ ProductIDPLP;
					window.history.pushState({},'',url);
				}
			} else if (pageContext.ns == 'cart') {
				if ( window.location.href.indexOf('?source') < 0) {
					var url = window.location.href + "?source=signin";
					window.history.pushState({},'',url);
				} else {
					var oldurl = window.location.href.split('?source=signin');
					var url = oldurl[0] + "?source=signin";
					window.history.pushState({},'',url);
				}
			}
	    } else {
	    	if (pageContext.ns == 'cart' && window.location.href.indexOf('?source=signin') > -1) {
				var oldurl = window.location.href.split('?source=signin');
				var url = oldurl[0];
		    	window.history.pushState({},'',url);
	    	}
	    }
	    var Login_PopUp_Active = window.SitePreferences.Login_PopUp_Active;
	    if(Login_PopUp_Active){
		    var url = Urls.newRegistration;
		    url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    dialog.open({ 
		        url: url,
		        options:{
		        	dialogClass:'new-sign-registration',
		        	open: function(event,ui){
		        		validator.init();
		        		newformstyle.init();
		        		$('.oAuthIcon').bind('click', function () {
		        	    	$('.OAuthProvider').val(this.id);
		        	    });
		        	    
		        	     $('.rising-action-btn-group').addClass('hidebutton');
		        		
		        		$('body').css('overflow','hidden');
		        		$('.checkemail-button button').on('click', function(event){
		        			event.preventDefault(); 
		        			var url = Urls.checkMailAvailabilty;
		        			var $form = $(this).closest('form');
		        			var emailid = $form.find('.email-field input').val();
		        			if ($form.find('.checkemail-field .error').length > 0){
		        				$form.find('.checkemail-field .error').remove();
		        			}
		        			var regex = validator.regex;
		        			if (emailid != '' && !regex.newEmail.test(emailid)){
		        				var newDiv ='<div class="error" style="color:#c20000;">Must be valid email address.<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			if (emailid == ''){
		        				var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			url = util.appendParamToURL(url,'emailid',emailid);
		        			 $.ajax({
		        			        url: url,
		        			        type: 'POST',
		        			        success: function (response) { 
		        			        	try {
			        			        	var responseJson = JSON.parse(response);
			        			        	if (responseJson.availableEmail){
			        			        		var $form1 = $('.returning-customers form');
			        			        		if ($form1.find('.email-value').hasClass('d-none')){
			        			        			$form1.find('.email-value').text(responseJson.email).removeClass('d-none');
			        			        		} else {
			        			        			$form1.find('.email-value').text(responseJson.email);
			        			        		}
			        			        		if ($form1.find('.checkpw-content').hasClass('d-none')){
			        			        			$form1.find('.checkpw-content').removeClass('d-none');
			        			        		}
			        			        		if (!$form1.find('.checkemail-field').hasClass('d-none')){
			        			        			$form1.find('.checkemail-field').addClass('d-none');
			        			        		}
			        			        		if ($form1.find('.form-row-password').hasClass('d-none')){
			        			        			$form1.find('.form-row-password').removeClass('d-none');
			        			        		}
			        			        		if (!$form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
			        			        			$form1.find('.form-row-button .checkemail-button').addClass('d-none');
			        			        		}
			        			        		if ($form1.find('.checkpw-button').hasClass('d-none')){
			        			        			$form1.find('.checkpw-button').removeClass('d-none');
			        			        		}
			        			        		if ($('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
			        			        			$('.newsignin-box-inner .checkpw-title').removeClass('d-none');
			        			        		}
			        			        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
			        			        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
			        			        		}
			        			        		if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
			        			        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
			        			        		}
			        			        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
			        			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
			        			        		}
			        			        		
			        			        		$('.newsignin-box-inner .checkemail-title').addClass('d-none')
			        			        		/* Passed ProductIDPLP for TW-1455 Likelist signin journey*/
			        			        		signInForCustoer(ProductIDPLP, isHeaderSignIn);
			        			        		forgotPwdClick();
			        			        		editbuttonclick();
			        			        		enteredWrongEmail();
			        			        		disguisePassword();
			        			        	} else {
			        			        		var cgid = '',pid='',pipeline='',cid='';
			        			        		if (pageContext.ns == 'search') {
			        			        			cgid = $('.new-register-direction').val();
			        			        		} else if (pageContext.ns == 'product'){
			        			        			pid = $('.product-code-pdp .product-id').text().trim();
			        			        		} else if (pageContext.ns == 'livetv' || pageContext.ns == 'account'){
			        			        			var pathName = window.location.pathname;
			        			        			pipeline = pathName.split('/')[pathName.split('/').length-1]
			        			        		} else if (pageContext.ns == 'content'){
			        			        			var cgidvalue = $('.new-register-direction').val();
			        			        			var cidvalue = $('.new-cid-register-direction').val();
			        			        			if (cgidvalue != "null") {
			        			        				cgid = cgidvalue;
			        			        			} else if (cidvalue != "null"){
			        			        				cid = cidvalue;		 
			        			        			} else {
			        			        				var pathName = window.location.pathname;
				        			        			pipeline = pathName.split('/')[pathName.split('/').length-1];
			        			        				if (pipeline == 'Search-Show'){
				        			        				cgid = window.location.search.split('=')[1];
				        			        			} else {
				        			        				cid = window.location.search.split('=')[1];
				        			        			}
			        			        			}
			        			        		}
			        			        		var url = util.appendParamsToUrl(Urls.oldRegPage,{
			        			        			'emailid':emailid, 
													'fromPopUpRegister' : 'yes',
			        			        			'newSignin':'newSignin',
			        			        			'pagecontext':pageContext.ns,
			        			        			'cgid':cgid,
			        			        			'pid':pid,
			        			        			'pipeline':pipeline,
			        			        			'cid':cid});
			        			        		window.location.href = url;
			        			        	}
			        			        	
			        			        	$(".emailEditButton").on("click", function(){
			        			        		$('.newsignin-box-inner .checkpw-title').addClass('d-none');
			        			        		$('.newsignin-box-inner .checkpw-content').addClass('d-none');
			        			        		$('.newsignin-box-inner .checkpw-field').addClass('d-none');
			        			        		$('.newsignin-box-inner .checkpw-button').addClass('d-none');
			        			        		$('.newsignin-box-inner .checkemail-title').removeClass('d-none');
			        			        		$('.newsignin-box-inner .checkemail-content').removeClass('d-none');
			        			        		$('.newsignin-box-inner .checkemail-field').removeClass('d-none');
			        			        		$('.newsignin-box-inner .checkemail-button').removeClass('d-none');
			        			        	});
		        			        	} catch (b) {
		        		                    console.log(b);
		        		                }
		        			        	
		        			        }, 
		        			        error: function (jqXHR, textStatus, errorThrown) {console.log(jqXHR + textStatus + errorThrown); }
		        			    });
		        		});
		        	},
		        	close:function() {
		        		$('body').removeAttr("style");
		        		$('html').removeAttr("style");
		        		$('.rising-action-btn-group').removeClass('hidebutton');	
		        	}
		        }
		    });
	    }else{
	    	var cgid = '',pid='',pipeline='',cid='';
    		if (pageContext.ns == 'search') {
    			cgid = $('.new-register-direction').val();
    		} else if (pageContext.ns == 'product'){
    			pid = $('.product-code-pdp .product-id').text().trim();
    		} else if (pageContext.ns == 'livetv' || pageContext.ns == 'account'){
    			var pathName = window.location.pathname;
    			pipeline = pathName.split('/')[pathName.split('/').length-1]
    		} else if (pageContext.ns == 'content'){
    			var cgidvalue = $('.new-register-direction').val();
    			var cidvalue = $('.new-cid-register-direction').val();
    			if (cgidvalue != "null") {
    				cgid = cgidvalue;
    			} else if (cidvalue != "null"){
    				cid = cidvalue;		 
    			} else {
    				var pathName = window.location.pathname;
        			pipeline = pathName.split('/')[pathName.split('/').length-1];
    				if (pipeline == 'Search-Show'){
        				cgid = window.location.search.split('=')[1];
        			} else {
        				cid = window.location.search.split('=')[1];
        			}
    			}
    		}
    		var url = util.appendParamsToUrl(Urls.oldRegPage,{
    			'pagecontext':pageContext.ns,
    			'cgid':cgid,
    			'pid':pid,
    			'pipeline':pipeline,
    			'cid':cid,
    			'emailid':'', 
				'fromPopUpRegister' : 'no',
    			'newSignin':'newSignin'});
    		window.location.href = url;
	    }
	});
	if($('.pt_account_login, .pt_checkout_login').length > 0 && $(window).width() <= 767) {
		$(window).load(function() {
	    	setTimeout(function(){ 
	    		$(window).scrollTop($('.account-container').offset().top - 20);
    		}, 100);
		});
    }
    $('form#dwfrm_login .button.button-wider').on('click', function() {
		setTimeout(function () {
			if($('form#dwfrm_login .validation-success').length == 2) {
		        $('form#dwfrm_login').addClass('btn-loader');
		    }
		}, 150);
	});
	$('form#RegistrationForm #onbeforeunload-button').on('click', function() {
		setTimeout(function () {
			if($('form#RegistrationForm .validation-success').length == 6) {
		        $('form#RegistrationForm').addClass('btn-loader');
		    }
		}, 150);
	});
	
	if($('.pt_account_login, .pt_checkout_login').length > 0){
		if($('.error-form.message .message-content:visible').length > 0 && window.loginErrorPunch != true){
			var errorMessage = $('.error-form.message .message-content').text();
			var errorSection = $('.account-tab-login').hasClass("active")?'Sign In':($('.account-tab-register').hasClass("active")?'Sign Up':'');
			window.dataLayer.push({
				'event': 'loginError',
				'ecommerce': {
					'errorText': errorSection+' Error',
					'errorMessage': ''+errorMessage.trim()
				}
			});
			window.loginErrorPunch = true;
		}
	}
}
/* Passed ProductIDPLP for TW-1455 Likelist signin journey*/
function signInForCustoer(ProductIDPLP, isHeaderSignIn) {
	$('.checkpw-button .singn-button').off('click').on('click', function(e){
		flyinheader.init();
		e.preventDefault();
		$('.focus-helper').css('display','none');
		$('.error-msg').css('display','none');


		var url = Urls.newLogin;
		var $form = $(this).closest('form');
		var emailid = $form.find('.email-value').text();
		var pwd = $form.find('.form-row-password input').val();
		if ($form.find('.form-row-password .error').length > 0){
			$form.find('.form-row-password .error').remove();
		}
		if (pwd == '') {
			var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank <div>' 
			$form.find('.form-row-password').append(newDiv);
			return;
		}
		
		progress.show();
		var data = $form.serialize();
		url = util.appendParamsToUrl(url,{'emailid':emailid , 'password':pwd});
		 $.ajax({
		        url: url,
		        type: 'POST',
		        data: data,
		        success: function (response) {
		        	try {
			        	var parseResponse = JSON.parse(response);
			        	if (parseResponse.loginstatus && !parseResponse.tvcustomer){   		
			        		if(parseResponse.isTJCPlusMember){
		        				$('.plus-subscription-class').remove();
		        			}
			        		var prevUrl = 	window.location.href;

			        		$.ajax({
			    		        url: prevUrl,
			    		        type: 'GET',
			    		        success: function (loginresponse) {
			    		        	try {
				    		        	if(pageContext.ns == "cart" && !isHeaderSignIn){
				    		        		window.location.href = Urls.opcStartNode;
				    		        	}else if(pageContext.ns == 'product' && $('.wishlist').length <=0 ){
				    		        		window.location.reload();
				    		        	} else if (isHeaderSignIn){
				    		        		window.location.reload();
				    		        	}
				    		        	/* code added for TW-1455 Likelist signin journey*/	
				    		        	else if(pageContext.ns == 'product' && $('.wishlist').length > 0){ 
				    		        		var ProductID = $('.pdp-main .wishlist-pid').text();
			        			 			url =  Urls.wishlistadd;			        	
			        			 			url = util.appendParamsToUrl(url,{pid : ProductID,source : 'productdetail'}); 
			        			 			window.location.href = url;   
			        					}else if(pageContext.ns == 'search' && $('.wishlist').length > 0){ 
			        			 			url =  Urls.wishlistadd;			        	
			        			 			url = util.appendParamsToUrl(url,{pid : ProductIDPLP,source : 'productdetail'}); 
			        			 			window.location.href = url;     
			        		 
			        					}else{
				    		        		//updating header user name
					    		        	var headerSelector = $(".header-wrapper:not(.header-wrapper-menu) .header-inner", loginresponse);
					    		        	var headerDOM = headerSelector.html();
					    		        	$(".header-wrapper:not(.header-wrapper-menu) .header-inner").html(headerDOM);
					    		        	
					    		        	//updating product tiles 
					    		        	var tileSelector = $("#ajaxRefresh", loginresponse);
					    		        	var tileDOM = tileSelector.html();
					    		        	$("#ajaxRefresh").html(tileDOM);
					    		        	
					    		        	//$( ".header .header-inner" ).load(" .header-inner > *" );
						        			//$( ".search-result-content #search-result-items" ).load(" #search-result-items > *" );
						        			$( "#ra-details-wrapper .ra-details-bottom" ).load(" .ra-details-bottom > *" );
						        			//$( ".main-wrapper #ajaxRefresh" ).load(" #ajaxRefresh > *" );	
						        			$( ".main-wrapper .cart" ).load(" .cart > *" );
						        			$('.product-wishlist-link').removeClass('login-class');
						        			$('.wishlist-button').removeClass('sigin-model-button');
						        			dialog.close();
						        			progress.hide();
				    		        	}
			    		        	} catch (b) {
			    	                    console.log(b);
			    	                }
			    		        }
			        		});
			        	}
			  
			        	else if(parseResponse.loginstatus && parseResponse.tvcustomer){	
							var prevUrl = 	Urls.opcStartNode;     		
			        		window.location.href = util.appendParamsToUrl(Urls.setnewPassword,{'emailid':emailid , 'PasswordResetEmail': parseResponse.PasswordResetEmail, 'prevUrl' : prevUrl});
			        		progress.hide();
			        	}
			        	else {
			        		if ($form.find('.login-error-message').hasClass('d-none')){
			        			$form.find('.login-error-message').removeClass('d-none');
			        		}
	        				if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
			        		}
	        				if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
			        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
			        		}
	        				if ($form.find('.form-row-password .error').length > 0){
	        					$form.find('.form-row-password .error').remove();
	        				}
	    					var newDiv ='<div class="error" style="color:#c20000;">The password is incorrect<div>' 
	            			$form.find('.form-row-password').append(newDiv);
	            			$('.form-row-new').removeClass('validation-success'); 
	             			$('.form-row-new').addClass('validation-error'); 
	    					progress.hide();
			        	}
		        	} catch (b) {
	                    console.log(b);
	                }
		        }		      
		 });
		 
		
		$( document ).ajaxComplete(function( event, xhr, settings ) {	
				//ratimer.reInitTimeCountersPDP();
				ratimer.reInitTimeCounters();
				menuclick();
				tjcplusactivation.init();
				newformstyle.init();
				$('.rising-action-btn-group').removeClass('hidebutton');
				 $('.slider-nav').not('.slick-initialized').slick({
				        autoplay: false,
				        arrows: true,
				       responsive: [
				        {
				            breakpoint: 767,
				             settings: {
				                    slidesToShow: 2,
				                    slidesToScroll: 2,
				                    infinite: true,
				                }
				        }
				    ] 			        
				        
				    }); 
					
		});
		 
	});
}
 
function forgotPwdClick() {
	$('.forgot-pin a, .resend-button .button,forgotpw-button a').on('click',function(e){
		e.preventDefault();
		var url = Urls.forgotPassword;
		var $form = $(this).closest('form');
		var emailid = $form.find('.email-value').text();
		url = util.appendParamToURL(url,'emailid',emailid);
		 $.ajax({
		        url: url,
		        type: 'POST',
		        success: function (response) { 
		        	try {
			        	var parseResponse = JSON.parse(response);
			        	if (parseResponse.mailHasSend){
			        		enteredWrongEmail();
			        		if ($form.find('.forgotpw-title').hasClass('d-none')){
			        			$form.find('.forgotpw-title').removeClass('d-none');
			        		}
			        		if ($form.find('.forgotpw-content').hasClass('d-none')) {
			        			$form.find('.forgotpw-content').removeClass('d-none');
			        		}
			        		if (!$form.find('.checkpw-content .edit-button').hasClass('d-none')){
			        			$form.find('.checkpw-content .edit-button').addClass('d-none');
			        		}
			        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none');
			        		}
			        		if (!$form.find('.checkpw-content').hasClass('d-none')){
			        			$form.find('.checkpw-content').addClass('d-none');
			        		}
			        		if (!$form.find('.checkpw-button').hasClass('d-none')){
			        			$form.find('.checkpw-button').addClass('d-none');
			        		}
			        		if ($form.find('.forgotpw-button').hasClass('d-none')){
			        			$form.find('.forgotpw-button').removeClass('d-none');
			        		}
			        		if (!$('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
			        			$('.newsignin-box-inner .checkpw-title').addClass('d-none');
			        		}
			        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
			        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
			        		}
			        		if (!$form.find('.form-row-password').hasClass('d-none')){
			        			$form.find('.form-row-password').addClass('d-none');
			        		}
			        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
			        		}
			        		if ($('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
			        			$('.newsignin-box-inner .guest-checkout-button').removeClass('d-none')
			        		}
			        	} else {
			        		$form.find('.login-error-message').removeClass('d-none');
			        	}
			        	
			        	$('.pwemail').html(emailid);
		        	} catch (b) {
	                    console.log(b);
	                }
		        }
		 });
	});
}

/**Start - Script to display the modal popup in registration page when you click "Shopped on our TV channel but not on our website?" **/
$( "#tvconnect-modelbox" ).dialog({      
	autoOpen: false,      
	dialogClass: "tvconnectdialog",
    modal: true,   
    height: 'auto',            
    resizable: true,              
});     

$( "#tvconnect" ).click(function() {  
	$(this).closest('form');
	$( "#tvconnect-modelbox" ).dialog( "open" );      
});  

function editbuttonclick() {
	$('body').on('click','.returning-customers form .checkpw-content .edit-button, .wrong-email-link', function(e){
		var $form1 = $(this).closest('form');
		if (!$form1.find('.checkpw-content').hasClass('d-none')){
			$form1.find('.checkpw-content').addClass('d-none');
		}
		if ($form1.find('.checkemail-field').hasClass('d-none')){
			$form1.find('.checkemail-field').removeClass('d-none');
		}
		if (!$form1.find('.form-row-password').hasClass('d-none')){
			$form1.find('.form-row-password').addClass('d-none');
		}
		if ($form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
			$form1.find('.form-row-button .checkemail-button').removeClass('d-none');
		}
		if (!$form1.find('.checkpw-button').hasClass('d-none')){
			$form1.find('.checkpw-button').addClass('d-none');
		}
		if (!$('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
			$('.newsignin-box-inner .checkpw-title').addClass('d-none');
		}
		if ($('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
			$('.newsignin-box-inner .checkemail-title').removeClass('d-none')
		}
		if (!$form1.find('.forgotpw-title').hasClass('d-none')){
			$form1.find('.forgotpw-title').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-content').hasClass('d-none')) {
			$form1.find('.forgotpw-content').addClass('d-none');
		}
		if (!$form1.find('.guest-checkout-button').hasClass('d-none')) {
			$form1.find('.guest-checkout-button').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-button').hasClass('d-none')){
			$form1.find('.forgotpw-button').addClass('d-none');
		}
		$.each($form1.find('.error'),function(){$(this).remove()});
	});
}
/**End**/    

function enteredWrongEmail() {
	$('body').on('click','.guest-checkout-button a', function(e){
		var $form1 = $(this).closest('form');
		if ($('.checkemail-title').hasClass('d-none')){
			$('.checkemail-title').removeClass('d-none');
		}
		if ($('.checkemail-content').hasClass('d-none')){
			$('.checkemail-content').removeClass('d-none');
		}
		if ($('.edit-button').hasClass('d-none')){
			$('.edit-button').removeClass('d-none');
		}
		if ($form1.find('.checkemail-button').hasClass('d-none')){
			$form1.find('.checkemail-button').removeClass('d-none');
		}
		if ($form1.find('.checkemail-field').hasClass('d-none')){
			$form1.find('.checkemail-field').removeClass('d-none');
		}
		if (!$form1.find('.forgotpw-title').hasClass('d-none')){
			$form1.find('.forgotpw-title').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-content').hasClass('d-none')) {
			$form1.find('.forgotpw-content').addClass('d-none');
		}
		if (!$form1.find('.forgotpw-button').hasClass('d-none')){
			$form1.find('.forgotpw-button').addClass('d-none');
		}
	});
}

function disguisePassword(){
	$('.icon-password').off('click').on('click', function () {
        var $this = $(this), $passwordField = $this.closest('.form-row-password').find('.input-text');

        if (!$this.hasClass('show')) {
            $passwordField.removeAttr('type');
            $passwordField.prop('type', 'password');
        } else {
            $passwordField.removeAttr('type');
            $passwordField.prop('type', 'text');
        }
        $this.toggleClass('show');
    });
}


function menuclick(){
$(document).ready(function(){
    $('.menu-category li .menu-item-toggle').unbind().click(function (e) {
       e.preventDefault();
        var $parentLi = $(e.target).closest('li');
        $parentLi.siblings('li.active').removeClass('active').find('.menu-item-toggle.active').click()
        .removeClass('fa-minus active').addClass('fa-plus');
        $parentLi.toggleClass('active');
        $(e.target).toggleClass('active');
        if ($(e.target).hasClass('fa-minus') || $(e.target).hasClass('fa-plus')){
            $(e.target).toggleClass('fa-plus fa-minus');
        }
    });
    
    });
}

/* Popup for adding the name in header*/

function addNamePopup() {
	 $('#opener').off('click').on('click', function(e){
 		var url = Urls.addNamePopup;
 		dialog.open({ 
 	        url: url,      
 	        options:{
 	        	dialogClass:'AddnewNameFormModal',
 	        	open: function(event,ui){
 	        		var addselectflag = false;
 	        		var addfnameflag = false;
 	        		var addlnameflag = false;
                    newformstyle.init();
                    validator.init();
 	        		function addnamevalidation(){
 	        			var addselect = $(".AddnewNameForm #dwfrm_profile_customer_title").val();
 	        			var addfname = $(".AddnewNameForm #dwfrm_profile_customer_firstname").val();
 	        			var addlname = $(".AddnewNameForm #dwfrm_profile_customer_lastname").val();
 	        			
 	        			 if(addselect == ''){
 	        					$("#AddnewNameForm .addselect").show();
 	        					addselectflag = false;
 	        			    }
 	        			    else{
 	        			    	$("#AddnewNameForm .addselect").hide();
 	        			    	addselectflag = true;
 	        			    }
 	        				if(addfname == ''){
 	        					$("#AddnewNameForm .addfname").show();
 	        					addfnameflag = false;
 	        			    }
 	        				 else{
 	        					 $("#AddnewNameForm .addfname").hide();	
 	        					 addfnameflag = true;
 	        				    }
 	        				if(addlname == ''){
 	        					$("#AddnewNameForm .addlname").show();
 	        					addlnameflag = false;
 	        			    }
 	        				 else{
 	        					 $("#AddnewNameForm .addlname").hide();	
 	        					 addlnameflag = true;
 	        				    }
 	        		}
 	        		$(".AddnewNameForm #dwfrm_profile_customer_title"). change(function(){
 	        			addnamevalidation();
 	        		});
 	        		$(".AddnewNameForm #dwfrm_profile_customer_firstname" ).keyup(function() {
 	        				addnamevalidation();
 	        		});
 	        		$(".AddnewNameForm #dwfrm_profile_customer_lastname" ).keyup(function() {
 	        			addnamevalidation();
 	        		});

 	        		$("#AddnewNameForm").submit(function(event){
 	        		    event.preventDefault(); 
 	        		    var url = $("#AddnewNameForm").attr('action');
 	        		    var data = $("#AddnewNameForm").serialize();  
 	        			
 	        		    addnamevalidation();
 	        		    //var res = null;
 	        			if(addselectflag == true && addfnameflag == true && addlnameflag == true){
 	        				$.ajax({
 	        		  	        url: url,
 	        		  	        type: 'POST',
 	        		  	        data: data,
 	        		  	        success: function (response) { 
 	        		  	        	var res = JSON.parse(response);
 	        		  	        	if (res.success == true) {
 	        		  	        		$('.customer-name').text(res.name);
 	        		  	        		dialog.close();
 	        		  	        	} 
 	        		  	        	$( "#dialog" ).remove();
 	        		  	        	$(".header-account .customer-name").css("color","#2c163d"); 
 	        		  	        	$(".header-account .customer-name").css("text-transform","capitalize");
 	        		  	        },        
 	        		  	        error: function (jqXHR, textStatus, errorThrown) {console.log(jqXHR + textStatus + errorThrown); }
 	        		  	    });
 	        			}

 	        		  });
 	        				
 	        		
 	        	},
 	        	close:function() {
 	        		
 	        	}
 	        }
 	    });
 	});
}

exports.init = function () {
    initializeEvents();
    addNamePopup();
};