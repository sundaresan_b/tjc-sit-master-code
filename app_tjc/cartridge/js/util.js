/* global Countries */
/*jslint nomen: true */

'use strict';

var _ = require('lodash');

var util = {
    /**
     * @function
     * @description adds generic toggle functionality to all .toggle in a container
     * @param {jQuery Object} $target The target container for this, defaults to document
     */
    addToggleEvents: function ($target) {
        if ($target === null || $target === undefined) {
            $target = $(document);
        }
        var refinementFlag = false;
        $target.find('.toggle').on('click', function () {
            var $toggleTrigger = $(this),
                $toggleContent = $toggleTrigger.next('.toggle-content');
            $toggleTrigger.add($toggleContent).toggleClass('expanded').toggleClass('collapsed');
            $toggleContent.slideToggle();
        }).each(function (length, value) {
            var $this = $(this),
                $toggleContent = $this.next('.toggle-content');
            if($this.hasClass('refinement-headline') && $this.is(":visible")){
            	if(refinementFlag){
	            	//$this.removeClass('expanded').addClass('collapsed');
	            	$this.add($toggleContent).removeClass('expanded').addClass('collapsed');
	            	$toggleContent.hide();
            	}else{
            		refinementFlag = true;
            	}
            }
            if (!$this.hasClass('expanded') && !$this.hasClass('collapsed')) {
                if ($this.parents('.not-for-desktop').length > 0) {
                    $this.add($toggleContent).addClass('collapsed');
                } else {
                    $this.add($toggleContent).addClass('expanded');
                }
            }
        });
    },
    /**
     * @function
     * @description appends the parameter with the given name and value to the given url and returns the changed url
     * @param {String} url the url to which the parameter will be added
     * @param {String} name the name of the parameter
     * @param {String} value the value of the parameter
     */
    appendParamToURL: function (url, name, value) {
        // quit if the param already exists
        if (url.indexOf(name + '=') !== -1) {
            return url;
        }
        var separator = url.indexOf('?') !== -1 ? '&' : '?';
        return url + separator + name + '=' + encodeURIComponent(value);
    },
    /**
     * @function
     * @description appends the parameters to the given url and returns the changed url
     * @param {String} url the url to which the parameters will be added
     * @param {Object} params
     */
    appendParamsToUrl: function (url, params) {
        var theUrl = url;
        _.each(params, function (value, name) {
            theUrl = this.appendParamToURL(theUrl, name, value);
        }.bind(this));
        return theUrl;
    },
    /**
     * @function
     * @description extract the query string from URL
     * @param {String} url the url to extra query string from
     **/
    getQueryString: function (url) {
        var qs,
            a;
        if (!_.isString(url)) {
            return undefined;
        }
        a = document.createElement('a');
        a.href = url;
        if (a.search) {
            qs = a.search.substr(1); // remove the leading ?
        }
        return qs;
    },
    /**
     * @function
     * @description
     * @param {String}
     * @param {String}
     */
    elementInViewport: function (el, offsetToTop) {
        var top = el.offsetTop,
            left = el.offsetLeft,
            width = el.offsetWidth,
            height = el.offsetHeight;

        while (el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        if (offsetToTop !== undefined) {
            top -= offsetToTop;
        }

        if (window.pageXOffset !== null) {
            return (
                top < (window.pageYOffset + window.innerHeight) &&
                left < (window.pageXOffset + window.innerWidth) &&
                (top + height) > window.pageYOffset &&
                (left + width) > window.pageXOffset
            );
        }

        if (document.compatMode === 'CSS1Compat') {
            return (
                top < (window.document.documentElement.scrollTop + window.document.documentElement.clientHeight) &&
                left < (window.document.documentElement.scrollLeft + window.document.documentElement.clientWidth) &&
                (top + height) > window.document.documentElement.scrollTop &&
                (left + width) > window.document.documentElement.scrollLeft
            );
        }
    },

    /**
     * @function
     * @description Appends the parameter 'format=ajax' to a given path
     * @param {String} path the relative path
     */
    ajaxUrl: function (path) {
        return this.appendParamToURL(path, 'format', 'ajax');
    },

    /**
     * @function
     * @description
     * @param {String} url
     */
    toAbsoluteUrl: function (url) {
        if (url.indexOf('http') !== 0 && url.charAt(0) !== '/') {
            url = '/' + url;
        }
        return url;
    },
    /**
     * @function
     * @description Loads css dynamically from given urls
     * @param {Array} urls Array of urls from which css will be dynamically loaded.
     */
    loadDynamicCss: function (urls) {
        var i, len = urls.length;
        for (i = 0; i < len; i++) {
            this.loadedCssFiles.push(this.loadCssFile(urls[i]));
        }
    },

    /**
     * @function
     * @description Loads css file dynamically from given url
     * @param {String} url The url from which css file will be dynamically loaded.
     */
    loadCssFile: function (url) {
        return $('<link/>').appendTo($('head')).attr({
            type: 'text/css',
            rel: 'stylesheet'
        }).attr('href', url); // for i.e. <9, href must be added after link has been appended to head
    },
    // array to keep track of the dynamically loaded CSS files
    loadedCssFiles: [],

    /**
     * @function
     * @description Removes all css files which were dynamically loaded
     */
    clearDynamicCss: function () {
        var i = this.loadedCssFiles.length;
        while (i-- < 0) {
            $(this.loadedCssFiles[i]).remove();
        }
        this.loadedCssFiles = [];
    },
    /**
     * @function
     * @description Extracts all parameters from a given query string into an object
     * @param {String} qs The query string from which the parameters will be extracted
     */
    getQueryStringParams: function (qs) {
        if (!qs || qs.length === 0) {
            return {};
        }
        var params = {},
            unescapedQS = decodeURIComponent(qs);
        // Use the String::replace method to iterate over each
        // name-value pair in the string.
        /*jslint unparam: true */
        unescapedQS.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
            function ($0, $1, $2, $3) {
                params[$1] = $3;
            });
        /*jslint unparam: false */
        return params;
    },

    fillAddressFields: function (address, $form) {
        var field;
        for (field in address) {
            if (address.hasOwnProperty(field)) {
                /*jslint continue: true */
                if (field === 'ID' || field === 'UUID' || field === 'key') {
                    continue;
                }
                /*jslint continue: false */
                // if the key in address object ends with 'Code', remove that suffix
                // keys that ends with 'Code' are postalCode, stateCode and countryCode
                $form.find('[name$="' + field.replace('Code', '') + '"]').val(address[field]);
                // update the state fields
                if (field === 'countryCode') {
                    $form.find('[name$="country"]').trigger('change');
                    // retrigger state selection after country has changed
                    // this results in duplication of the state code, but is a necessary evil
                    // for now because sometimes countryCode comes after stateCode
                    $form.find('[name$="state"]').val(address.stateCode);
                }
            }
        }
    },
    /**
    * @function
    * @description Adds a error message to a form
    * @param {jQueryObject} $form The form
    * @param {String} message The error message
    */
    addGeneralFormError: function ($form, message) {
        var $closeIcon = $('<i/>').addClass('fa fa-times'),
            $closeButton = $('<a/>', {title: 'Close'}).addClass('button button-icon button-transparent'),
            $messageContent = $('<div/>').addClass('message-content').text(message),
            $messageInner = $('<div/>').addClass('message-inner'),
            $messageErrorForm = $('<div/>').addClass('message error-form');

        $closeButton.append($closeIcon);
        $messageContent.append($closeButton);
        $messageInner.append($messageContent);
        $messageErrorForm.append($messageInner).hide();
        $form.find('.message.error-form').remove();
        $messageErrorForm.prependTo($form).fadeIn();
    },
    /**
     * @function
     * @description Removes the error message to a form
     * @param {jQueryObject} $form The form
     */
    removeGeneralFormError: function ($form) {
        $form.find('.message.error-form').remove();
    },
    /**
     * @function
     * @description Updates the states options to a given country
     * @param {String} countrySelect The selected country
     */
    updateStateOptions: function (form) {
        var $form = $(form),
            $country = $form.find('select[id$="_country"]'),
            country = Countries[$country.val()],
            arrHtml = [],
            $stateField,
            $postalField,
            $stateLabel,
            $postalLabel,
            prevStateValue,
            s,
            o1;
        if ($country.length === 0 || !country) {
            return;
        }
        $stateField = $country.data('stateField') || $form.find('select[name$="_state"]');
        $postalField = $country.data('postalField') || $form.find('input[name$="_postal"]');
        $stateLabel = ($stateField.length > 0) ? $form.find('label[for="' + $stateField[0].id + '"] span')
            .not('.required-indicator') : undefined;
        $postalLabel = ($postalField.length > 0) ? $form.find('label[for="' + $postalField[0].id + '"] span')
            .not('.required-indicator') : undefined;
        prevStateValue = $stateField.val();
        // set the label text
        if ($postalLabel) {
            $postalLabel.html(country.postalLabel);
        }
        if ($stateLabel) {
            $stateLabel.html(country.regionLabel);
        } else {
            return;
        }
        for (s in country.regions) {
            if (country.regions.hasOwnProperty(s)) {
                arrHtml.push('<option value="' + s + '">' + country.regions[s] + '</option>');
            }
        }
        // clone the empty option item and add to stateSelect
        o1 = $stateField.children().first().clone();
        $stateField.html(arrHtml.join('')).removeAttr('disabled').children().first().before(o1);
        // if a state was selected previously, save that selection
        if (prevStateValue && $.inArray(prevStateValue, country.regions)) {
            $stateField.val(prevStateValue);
        } else {
            $stateField[0].selectedIndex = 0;
        }
    },
    /**
     * @function
     * @description Updates the number of the remaining character
     * based on the character limit in a text area
     */
    limitCharacters: function () {
        $('form').find('textarea[data-character-limit]').each(function () {
            var characterLimit = $(this).data('character-limit'),
                charCountHtml = String.format(Resources.CHAR_LIMIT_MSG,
                    '<span class="char-remain-count">' + characterLimit + '</span>',
                    '<span class="char-allowed-count">' + characterLimit + '</span>'),
                charCountContainer = $(this).next('div.char-count');
            if (charCountContainer.length === 0) {
                charCountContainer = $('<div class="char-count"/>').insertAfter($(this));
            }
            charCountContainer.html(charCountHtml);
            // trigger the keydown event so that any existing character data is calculated
            $(this).change();
        });
    },
    /**
     * @function
     * @description Binds the onclick-event to a delete button on a given container,
     * which opens a confirmation box with a given message
     * @param {String} container The name of element to which the function will be bind
     * @param {String} message The message the will be shown upon a click
     */
    setDeleteConfirmation: function (container, message) {
        /*eslint-disable no-alert */
        $(container).on('click', '.delete', function () {
            return window.confirm(message);
        });
        /*eslint-enable no-alert */
    },
    /**
     * @function
     * @description Scrolls a browser window to a given x point
     * @param {String} The x coordinate
     */
    scrollBrowser: function (xLocation) {
        $('html, body').animate({scrollTop: xLocation}, 500);
    },

    isMobile: function () {
        if ($(window).width() < 767) {
            return true;
        }
        var mobileAgentHash = ['mobile', 'tablet', 'phone', 'ipad', 'ipod', 'android', 'blackberry', 'windows ce',
                'opera mini', 'palm'],
            idx = 0,
            isMobile = false,
            userAgent = (navigator.userAgent).toLowerCase();

        while (mobileAgentHash[idx] && !isMobile) {
            isMobile = (userAgent.indexOf(mobileAgentHash[idx]) >= 0);
            idx++;
        }
        return isMobile;
    },

    timer: function () {
        var id,
            clear;
        clear = function () {
            if (id) {
                window.clearTimeout(id);
                id = null;
            }
        };
        return {
            clear: clear,
            start: function (duration, callback) {
                clear();
                id = setTimeout(callback, duration);
            }
        };
    },

    getCountrySettings : function (countryCode) {
        var i;
        if (Countries === undefined || Countries === null || typeof Countries !== 'object') {
            return null;
        }

        for (i in Countries) {
            if (Countries.hasOwnProperty(i)) {
                if (Countries[i].code === countryCode) {
                    return Countries[i];
                }
            }
        }

        return null;
    },
    disguisePassword : function () {
        $('.icon-password').on('click', function () {
            var $this = $(this), $passwordField = $this.closest('.form-row-password').find('.input-text');

            if (!$this.hasClass('show')) {
                $passwordField.removeAttr('type');
                $passwordField.prop('type', 'password');
            } else {
                $passwordField.removeAttr('type');
                $passwordField.prop('type', 'text');
            }
            $this.toggleClass('show');
        });
    },
    closeHandlerForErrorMessages : function () {
        $('.message.error-form').on('click', function () {
            $(this).remove();
        });
    },
    /**
     * @function
     * @description Get parameter value from URL
     * @param {String} "name" parameter for which the value need to get
     * @param {String} "url" parameter from where the value must be fetched
     * @param {String} "results" parameter returns the value of the respective name
     */
    getParamFromUrl: function (name, url) {
    	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
    	if(results != null){
	    	if(results.length > 0){
	    		return results[1];
	    	}
    	}
    	return 0;
    },
    /**
     * @function
     * @description adjust the images with irregular dimensions
     */
    imageAdjust: function () {
    	$('img').each(function(index, value){ /* Global tag */
			var tempSRC = ($(value).attr('data-src'))?$(value).attr('data-src'):$(value).attr('src'), tempSW = 0, tempSH = 0, tempSH_ = 0; 
			if(typeof(tempSRC) !== "undefined"){
				if(tempSRC.indexOf("?") >= 0){
					tempSW = util.getParamFromUrl('sw',tempSRC);
					tempSH = util.getParamFromUrl('sh',tempSRC); 
					tempSH_ = parseInt(tempSH) + 50;
					if(typeof($(this).attr('class')) == "undefined" || $(this).parent().hasClass('thumbnail-link') || $(this).parent().hasClass('magnifier')){
					
					}else if($(this).parent().hasClass('single-view') || $(this).parents().hasClass('product-tile-top')){
						if(tempSH > 1){
							$(value).css({'max-height':tempSH+'px','object-fit':'scale-down','object-position':'50% 50%'});
						}
					}else{/*'max-width':tempSW+'px',*/
						if(tempSH > 1){
							$(value).css({'max-height':tempSH+'px','object-fit':'scale-down','object-position':'50% 50%'});
						}
						//$(this).parent().css({'display':'flex','justify-content':'center'});
					}
				}
			}
		});
    },
    setCookie: function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	},
	getCookie: function(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	},
	/**
     * @function
     * @description get the name of the browser
     */
	browsername:function () { 
	    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
	        return 'Opera';
	    } else if(navigator.userAgent.indexOf("Chrome") != -1 ) {
	        return 'Chrome';
	    } else if(navigator.userAgent.indexOf("Safari") != -1) {
	        return 'Safari';
	    } else if(navigator.userAgent.indexOf("Firefox") != -1 ){
	        return 'Firefox';
	    } else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {
	        return 'IE';//crap
	    } else {
	        return 'Unknown';
	    }
	}

};

util.executeJqueryReadyCallbacks = function () {
    if (window.jqueryReadyCallbacks && $.isArray(window.jqueryReadyCallbacks)) {
        /*jslint unparam: true */
        $.each(window.jqueryReadyCallbacks, function (i, callback) {
            if ($.isFunction(callback)) {
                callback(util);
            }
        });
        /*jslint unparam: false */
        window.jqueryReadyCallbacks = [];
    }
};

module.exports = util;
