'use strict';

var address = require('./address'),
    billing = require('./billing'),
    tabs = require('../../tabsaccount'),
	util = require('../../util'),
	ajax = require('../../ajax'),
	paysafe = require('../../paysafe'),
	progress = require('../../progress'),
	dialog = require('../../dialog'),
	validator = require('../../validator'),
	newformstyle = require('../../new-form-style'),
	continueCheckout = "",
	paymentTriggeredClass = "",
	paymentErrorMessage = "",
	paymentErrorType = "",
	pageAJAXRefreshFlag = false,
	carousel = require('../../carousel');

function initialFunction(){
	var tjcCreditsFlag = false;
	carousel.init();
	var couponstatus = $('.couponstatus').text();
	 var already=couponstatus.search("COUPON_CODE_ALREADY_IN_BASKET");
	 var correct=couponstatus.search("OK");
	 if(already >=0){
		 $('.already').css("display","block"); 
		 $('.active').css("display","none");
	 }
	 else if(correct >= 0){
		 $('.already').css("display","none");
		 $('.active').css("display","none");
		 $('.cart-coupon-code .optinal-field').removeClass('validation-error'); 
	 }
	 else {
		 $('.already').css("display","none");
		 $('.active').css("display","block");
	 }
	function couponButton(){
		if($('.cart-coupon-code-button .symbol-down').length){
			$('.cart-coupon-code-button .symbol-down').addClass('symbol-up').removeClass('symbol-down');
			$('.cart-coupon-code-button').css('border-bottom','1px solid transparent');
			$('.cart-coupon-code').show();
		}else if($('.cart-coupon-code-button .symbol-up').length){
			$('.cart-coupon-code-button .symbol-up').addClass('symbol-down').removeClass('symbol-up');
			$('.cart-coupon-code-button').css('border-bottom','1px solid #b9addb');
			$('.cart-coupon-code').hide();
		}
	}
	$('.cart-coupon-code-button').off('click').on('click',function(){
		couponButton();		
	});
	
	$('.gift-receipt .gift-content .field textarea').attr('maxlength','160');
	$('.gift-receipt .gift-content .field textarea').on('keyup keydown',function(){
		$('.gift-receipt .gift-content .message-count .count').html(160 - $(this).val().length);
	});
	
	validateFieldsCount();	
	
	/* Made hidden on UI
	 * $(window).scroll(function(){
		var scroll = $(window).scrollTop();
		if (scroll >= 100){ $('.fixed-content').css('display','unset'); }
		else { $('.fixed-content').css('display','none'); }
	});*/
	
	if($('#useThisOPCDeliverylocation .customerAddresses').length > 0){
		var addressFormValue = $('.checkout-shipping-address .customerAddresses .input-radio:checked').attr('data-address');
		if(typeof addressFormValue  === 'undefined')
			addressFormValue = $('.checkout-shipping-address .customerAddresses .input-radio:first').attr('data-address');
		fillAddressFields(addressFormValue, '.checkout-shipping-address');
	}
	
	if($('#useThisOPCBillinglocation .customerAddresses').length > 0){
		var addressFormValue = $('.checkout-billing-address .customerAddresses .input-radio:checked').attr('data-address');
		if(typeof addressFormValue  === 'undefined')
			addressFormValue = $('.checkout-billing-address .customerAddresses .input-radio:first').attr('data-address');
		fillAddressFields(addressFormValue, '.checkout-billing-address');
	}else{
		fillAddressFields(null, null,'.checkout-shipping-address', '.checkout-billing-address');
	}
	
	$('.payment-method-wrapper .payment-method input[type$=radio]').on('change',function(){
		var thisClass = $('.payment-method-wrapper .payment-method input[type$=radio]:checked').closest('.payment-method').attr('class'),
			$billingClass = $('.payment-method-wrapper .billing'),
			$orderTotalsBudgetpay = $('.cartSummary .cart-order-totals .order-totals-table .order-totals-budgetpay'),
			$orderTotalsForBudgetPay = $('.order-totals-table .order-full-total td'),
			$budgetPayDescription = $('.tac .msg-budgetpay'),
			$budgetPayDetails = $('.payment-method-wrapper .budgetpayDetails'),
			$applePayDetails = $('.payment-method-wrapper .applePayDetails'),
			$checkoutPlaceOrderButton = $('.payment-methods .cart-checkout .xlt-continueCheckout'),
			$applePayLabelImage = $('.payment-method-wrapper .payment-method.apple-pay .label-image img');
		paymentTriggeredClass = thisClass;
		if(tjcCreditsFlag == true){
			$billingClass.addClass('tjccredit').removeClass('Paysafe').removeClass('apple-pay').removeClass('BudgetPay').removeClass('paypal').removeClass('paypal-credit');
			$billingClass.css({'display':'block'});
			$orderTotalsForBudgetPay.css({'font-size':'20px'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').hide();
			$orderTotalsForBudgetPay.find('.order-totals-heading').show();
			$budgetPayDescription.hide();
			$budgetPayDetails.hide();
			$orderTotalsBudgetpay.hide();
			$checkoutPlaceOrderButton.show();
			$applePayDetails.hide();
			$applePayLabelImage.show();
		}else if(typeof thisClass === 'undefined' && tjcCreditsFlag == false){
			$billingClass.removeClass('tjccredit').removeClass('Paysafe').removeClass('apple-pay').removeClass('BudgetPay').removeClass('paypal').removeClass('paypal-credit');
			$billingClass.css({'display':'none'});
			$orderTotalsForBudgetPay.css({'font-size':'20px'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').hide();
			$orderTotalsForBudgetPay.find('.order-totals-heading').show();
			$budgetPayDescription.hide();
			$budgetPayDetails.hide();
			$orderTotalsBudgetpay.hide();
			$checkoutPlaceOrderButton.show();
			$applePayDetails.hide();
			$applePayLabelImage.show();
		}else if(thisClass.indexOf("paysafe") != -1){
			$billingClass.removeClass('tjccredit').addClass('Paysafe').removeClass('apple-pay').removeClass('BudgetPay').removeClass('paypal').removeClass('paypal-credit');
			$billingClass.css({'display':'block'});
			$orderTotalsForBudgetPay.css({'font-size':'20px'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').hide();
			$orderTotalsForBudgetPay.find('.order-totals-heading').show();
			$budgetPayDescription.hide();
			$budgetPayDetails.hide();
			$orderTotalsBudgetpay.hide();
			setTimeout(function(){ $('#existing-paysafe-cvv:visible').trigger('focus'); $('#existing-paysafe-cvv:visible').trigger('click'); }, 500);
			$checkoutPlaceOrderButton.show();
			$applePayDetails.hide();
			$applePayLabelImage.show();
		}else if(thisClass.indexOf("apple-pay") != -1){
			$billingClass.removeClass('tjccredit').removeClass('Paysafe').addClass('apple-pay').removeClass('BudgetPay').removeClass('paypal').removeClass('paypal-credit');
			$billingClass.css({'display':'none'});
			$orderTotalsForBudgetPay.css({'font-size':'20px'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').hide();
			$orderTotalsForBudgetPay.find('.order-totals-heading').show();
			$budgetPayDescription.hide();
			$budgetPayDetails.hide();
			$orderTotalsBudgetpay.hide();
			$checkoutPlaceOrderButton.hide();
			$applePayDetails.show();
			$applePayLabelImage.hide();
		}else if(thisClass.indexOf("budgetpay") != -1){
			$billingClass.removeClass('tjccredit').removeClass('Paysafe').removeClass('apple-pay').addClass('BudgetPay').removeClass('paypal').removeClass('paypal-credit');
			$billingClass.css({'display':'block'});
			$orderTotalsForBudgetPay.css({'font-size':'unset'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').show().css({'font-weight':'500'});
			$orderTotalsForBudgetPay.find('.order-totals-heading').hide();
			$budgetPayDescription.show();
			$budgetPayDetails.show();
			$orderTotalsBudgetpay.show();
			setTimeout(function(){ $('#existing-paysafe-cvv:visible').trigger('focus'); $('#existing-paysafe-cvv:visible').trigger('click'); }, 500);
			$checkoutPlaceOrderButton.show();
			$applePayDetails.hide();
			$applePayLabelImage.show();
		}else if(thisClass.indexOf("paypal-credit") != -1){
			$billingClass.removeClass('tjccredit').removeClass('Paysafe').removeClass('apple-pay').removeClass('BudgetPay').removeClass('paypal').addClass('paypal-credit');
			$billingClass.css({'display':'block'});
			$orderTotalsForBudgetPay.css({'font-size':'20px'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').hide();
			$orderTotalsForBudgetPay.find('.order-totals-heading').show();
			$budgetPayDescription.hide();
			$budgetPayDetails.hide();
			$orderTotalsBudgetpay.hide();
			$checkoutPlaceOrderButton.show();
			$applePayDetails.hide();
			$applePayLabelImage.show();
		}else if(thisClass.indexOf("paypal") != -1){
			$billingClass.removeClass('tjccredit').removeClass('Paysafe').removeClass('apple-pay').removeClass('BudgetPay').addClass('paypal').removeClass('paypal-credit');
			$billingClass.css({'display':'block'});
			$orderTotalsForBudgetPay.css({'font-size':'20px'});
			$orderTotalsForBudgetPay.find('.order-totals-budgetpay-heading').hide();
			$orderTotalsForBudgetPay.find('.order-totals-heading').show();
			$budgetPayDescription.hide();
			$budgetPayDetails.hide();
			$orderTotalsBudgetpay.hide();
			$checkoutPlaceOrderButton.show();
			$applePayDetails.hide();
			$applePayLabelImage.show();
		}
	});
	
	$('.billing .checkout-billing-address .checker input').on('change',function(){
		billingClassFields($(this).prop('checked'));
	});
	billingClassFields($('.billing .checkout-billing-address .checker input').prop('checked'));
	
	$('.paysafe-card-details').find('#existing-paysafe-card').val($('.paysafe-card-details .paysafemodelbox').attr('data-paysafeSelectedCard'));
	var selectLable = $('#existing-paysafe-card').find('option:selected').text();
	$('.paysafe-card-details').find('.selector span:first').html(selectLable);
	
	var $tjcCredit = $('.credit-input'),
		$tjcCreditAvailable = $('.credit-available'),
		$tjcCreditApplyButton = $('.credit-apply-button'),
		$tjcCreditExceedError = $('.credits-exceed-error-msg');
	$tjcCredit.on('keyup', function () {
		if (isNaN($(this).val()) === false || isDecimal($(this).val()) === true) {
			$tjcCreditApplyButton.show();
			var subtotal = $tjcCredit.data('subtotal'),
				newCreditBalance = subtotal - $(this).val(),
				userCreditAvailable = $tjcCredit.data('creditavailable'),
				userCreditBalance = userCreditAvailable - $(this).val();
			/*if ($(this).val() > userCreditAvailable) {
				$tjcCreditExceedError.parent().show();
				$tjcCreditAvailable.parent().hide();
				return;
			}else{
				$tjcCreditExceedError.parent().hide();
			}*/
			if ($(this).val() < 0) {
				$('.credit-input').css({'border':'2px solid #c20000'});
				tjcCreditsFlag = false;
				return;
			}else{
				$('.credit-input').css({'border':'1px solid #b9addb'});
			}
			if (newCreditBalance <= 0) {
				$tjcCreditAvailable.parent().hide();
				$(this).val(subtotal);
				tjcCreditsFlag = true;
				$('.payment-method-wrapper .payment-method input[type$=radio]').trigger('change')
				$tjcCreditApplyButton.html($tjcCredit.data('paynowlabel'));
			} else {
				$tjcCreditAvailable.parent().show();
				$tjcCreditAvailable.html('£ ' + newCreditBalance.toFixed(2));
				tjcCreditsFlag = false;
				$('.payment-method-wrapper .payment-method input[type$=radio]').trigger('change')
				$tjcCreditApplyButton.html($tjcCredit.data('applylabel'));
			}
		} else {
			$('.credit-input').css({'border':'2px solid #c20000'});
			$tjcCreditApplyButton.hide();
			tjcCreditsFlag = false;
		}
	});
	
	$('.xlt-cart-coupon-form input').blur(function(){
		if(!$(this).val()){
			$(this).parents('form-row').addClass("validation-error");
		} else{
			$(this).parents('form-row').removeClass("validation-error");
		}
	});
	
	var couponFormClass = $('.xlt-cart-coupon-form .form-row').attr('class');
	if(couponFormClass.indexOf("validation-error") != -1){
		couponButton();
	}
	
	$(".gift-receipt .gift-description").insertAfter($(".gift-receipt .is-gift .field label .tooltip"));
	
	if($('#currentUserAddressExist').val() == "true" || $('#showGuestChangeButton').val() == 'true'){
		$(".gift-receipt .gift-content .field-inner-text").insertAfter($(".gift-receipt .gift-content .input-text"));
		$(".gift-save-button").off('click').on('click', function(e){
			$('.delivery-address .xlt-continueShopping').trigger('click');
		});
	}
	
	if($('#showGuestChangeButton').val() == 'true'){
		var guestDetailTitle = $('.guestDetails .detail-title select').val(),
			guestDetailFirst = $('.guestDetails .detail-first-name input').val(),
			guestDetailSecond = $('.guestDetails .detail-last-name input').val(),
			guestDetailEmail = $('.guestDetails .detail-email input').val(),
			guestDetailMobile = $('.guestDetails .detail-mobile input').val(),
			guestAddressCountry = $('.checkout-shipping-address .selector span:first').text(),
			guestAddressPostCode = $('.checkout-shipping-address input[name$=postCode]').val(),
			guestAddressAddress1 = $('.checkout-shipping-address input[name$=address1]').val(),
			guestAddressAddress2 = $('.checkout-shipping-address input[name$=address2]').val(),
			guestAddressCity = $('.checkout-shipping-address input[name$=city]').val();
		
		$('#guestDetails .guestShowDetails .custName span').html(guestDetailTitle+' '+guestDetailFirst+' '+guestDetailSecond);
		$('#guestDetails .guestShowDetails .custEmail span').html(guestDetailEmail);
		$('#guestDetails .guestShowDetails .custMobile span').html(guestDetailMobile);
		
		$('#guestDetailsAddress .preferredAddress .custName span').html(guestDetailFirst+' '+guestDetailSecond);
		var guestLabel = guestAddressAddress1;
		guestLabel += guestAddressAddress2 ? ', ' + guestAddressAddress2 : '';
		guestLabel += ', ' + guestAddressCity;
		guestLabel += guestAddressCountry ? ', ' + guestAddressCountry : '';
		guestLabel += guestAddressPostCode ? ', ' + guestAddressPostCode : '';
		$('#guestDetailsAddress .preferredAddress .custAddress span').html(guestLabel);
		
		$('.gift .gift-receipt .gift-content .gift-recipient-name .gift-save-button').removeClass('visually-hidden');
		
		$('.guestaddressmodelbox').off('click').on('click',function(){
			$('#guestDetailsAddress').addClass('hidden');
			$('#guestDetails').addClass('hidden');
			$('.guestDetails fieldset').removeClass('hidden');
			$('.checkout-shipping-address .opcAddressFields').removeClass('hidden');
			$('.guestaddressmodelbox').addClass('hidden');
			$('.xlt-continueShopping').removeClass('hidden');
			
			$('.gift .gift-receipt .gift-content .gift-recipient-name .gift-save-button').addClass('visually-hidden');
			
			$('.checkout-billing-methods #cart-shippingmethod-form').addClass('freezeSection');
			$('.payment .paymentMethod').addClass('freezeSection');
			$('form.xlt-continueCheckout').addClass('freezeSection');
			//$('.cart-coupon-code').addClass('freezeSection');
			$('.couponsList').addClass('freezeSection');
			freezeSectionEvent();
		});
	}
	
	var giftCheckedFlag = false;
	//if($('#currentUserAddressExist').val() == "true"){
		if($('.gift-receipt .is-gift .checker input.input-checkbox').prop('checked')){
			$('.gift-content').addClass('visually-hidden');
			var giftMessageText = $('.gift-receipt .gift-content .gift-message-text .input-textarea').val();
			var giftRecipientName = $('.gift-receipt .gift-content .gift-recipient-name .input-text').val();
			var joinText = (giftMessageText.length > 22)?'...':'';
			$('.gift-receipt .gift-content-with-message .gift-to-name .gift-to-name-content').html(giftRecipientName);
			$('.gift-receipt .gift-content-with-message .gift-to-message .gift-to-message-content').html(giftMessageText.slice(0, 22)+joinText);
			$('.gift-content-with-message').removeClass('visually-hidden');
			giftCheckedFlag = true;
		}else{
			$('.gift-content').removeClass('visually-hidden');
			$('.gift-receipt .gift-content .gift-message-text .input-textarea').val("");
			$('.gift-receipt .gift-content .gift-recipient-name .input-text').val("");
			giftCheckedFlag = false;
		}
		$(".gift-edit-button").off('click').on('click', function(e){
			$('.gift-content').removeClass('visually-hidden');
			$('.gift-content-with-message').addClass('visually-hidden');
		});
		$(".gift-receipt div[id$='_shippingAddress_isGift']").on('click' , function(){
			if(!$('.gift-receipt .is-gift .checker input.input-checkbox').prop('checked') && giftCheckedFlag){
				$('.delivery-address .xlt-continueShopping').trigger('click');
			}
		});
	//}
	
	$("div[id$='_shippingAddress_isGift']").on('click' , function(){
		var addressSubmitVisible = $('.xlt-addressForm button.xlt-continueShopping').is(":visible") && !$('.xlt-addressForm button.xlt-continueShopping').hasClass("visually-hidden");
		if(addressSubmitVisible == false || addressSubmitVisible == "false"){
			if ($("div[id$='_shippingAddress_isGift'] span").hasClass('checked')){
				$('.checkout-billing-methods #cart-shippingmethod-form').addClass('freezeSection');
				$('.payment .paymentMethod').addClass('freezeSection');
				$('form.xlt-continueCheckout').addClass('freezeSection');
				//$('.cart-coupon-code').addClass('freezeSection');
				$('.couponsList').addClass('freezeSection');
				freezeSectionEvent();
			}else{
				$('.checkout-billing-methods #cart-shippingmethod-form').removeClass('freezeSection');
				$('.payment .paymentMethod').removeClass('freezeSection');
				//$('.cart-coupon-code').removeClass('freezeSection');
				$('.couponsList').removeClass('freezeSection');
				$('.customFreezeMessageDeliveryAddress').hide();
				$('.gift-content .gift-recipient-name').removeClass('validation-error');
				//freezeSectionEventAction();
			}
		}
	});
	
	$('.checkout-addresses-form').off('submit').on('submit', function(e){
		/*e.preventDefault();*/
		var $formData = $(this).closest('form'),
			$formActionURL = $formData.attr('action'),
			isValid = $formData.valid(),
			url = Urls.opcAddressResponse,
			urlRedirect = Urls.opcredirect;
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			urlRedirect = util.appendParamToURL(urlRedirect,'isGuestUser',true);
		}
		
		//if Session timeout redirect to login page
		$.ajax({
            type: "GET",
            url: urlRedirect,
            success: function(data) {
                try {
                	if(!data.status){
                		window.location.href = Urls.opcStartNode;
                		return false;
                	}              	                
                } catch (b) {
                    console.log(b);
                }
            },
            error: function(f) {
                console.log(f.statusText + " " + f.status);
            }
        });
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			url = util.appendParamToURL(url,'isGuestUser',true);
		}
		
		if(isValid == "true" || isValid == true){
			if($('.guestDetails .validation-error-custom').length > 0){
				$('html, body').animate({scrollTop: $(".delivery .validation-error-custom").offset().top-60}, 500);
				return false;
			}
			var $addressFields = $('.checkout-shipping-address .opcAddressFields .additional-address-fields');
			if($addressFields.find('.city input').val().length < 1 && $addressFields.find('.address1 input').val().length < 1){
				//$('.checkout-shipping-address .opcAddressFields .post-code-field .address-check-btn button').trigger('click');
				$('html, body').animate({scrollTop: $(".delivery .opcAddressFields").offset().top-60}, 500);
				return false;
			}
			progress.show();
			$.ajax({
	            type: "POST",
	            url: url,
	            data: $formData.serialize(),
	            success: function(data) {
	                try {
	                    $('.xlt-opcPageContent').html(data);
	                    reInitialFunction();
	                } catch (b) {
	                    console.log(b);
	                }
	                progress.hide();
	            },
	            error: function(f) {
	                console.log(f.statusText + " " + f.status)
	            }
	        });
		}else{
			var $addressFields = $('.checkout-shipping-address .opcAddressFields .additional-address-fields');
			if($addressFields.find('.city input').val().length < 1 && $addressFields.find('.address1 input').val().length < 1 && $addressFields.find('.postCode input').val().length > 0){
				//$('.checkout-shipping-address .opcAddressFields .post-code-field .address-check-btn button').trigger('click');
				$('html, body').animate({scrollTop: $(".delivery .opcAddressFields").offset().top-60}, 500);
			}
			opcFormValidation();
			/*$.each(['.guestDetails', '.delivery-address'], function (i, attribute) {
				if($(attribute).find('.validation-error').length > 0){
					$(attribute).find('.required-indicator').css('display','unset');
				}else{
					$(attribute).find('.required-indicator').css('display','none');
				}
			});*/
			if($('.sections.delivery .additional-address-fields').hasClass('visually-hidden')){
				if (window.matchMedia("(max-width: 480px)").matches) {
					$('html, body').animate({scrollTop: $(".sections.delivery").offset().top-60}, 500);
				}else{
					if($('.guestDetails').length > 0){
						$('html, body').animate({scrollTop: $(".delivery .validation-error").offset().top-60}, 500);
					}else{
						$('html, body').animate({scrollTop: $("html, body").offset().top-60}, 500);
					}					
				}
			}else{
				if(window.matchMedia("(max-width: 480px)").matches && $(".gift-content").is(":visible")){
					$('html, body').animate({scrollTop: $(".sections.delivery").offset().top-60}, 500);
				}else{
					$('html, body').animate({scrollTop: $(".delivery .validation-error").offset().top-60}, 500);
				}
			}
			progress.hide();
		}
		return false;
	});
	
	$('.checkout-billing-methods input[type$=radio]').on('change',function(){
		/*e.preventDefault();*/
		var $formData = $(this).closest('form'),
			$formActionURL = $formData.attr('action'),
			isValid = $formData.valid(),
			url = Urls.opcUpdateShippingMethod;
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			url = util.appendParamToURL(url,'isGuestUser',true);
		}
		giftCheck();
		
		progress.show();
		$.ajax({
            type: "POST",
            url: url,
            data: $formData.serialize(),
            success: function(data) {
                try {
                	if(!data.status){
                		//if Session timeout redirect to login page
                		window.location.href = Urls.opcStartNode;
                	}else{
                		pageAJAXRefreshFlag = true;
                		$('.delivery-address .xlt-continueShopping').trigger('click');
                	}
                    
                } catch (b) {
                    console.log(b);
                }
            },
            error: function(f) {
                console.log(f.statusText + " " + f.status)
            }
        });
	});
	
	$('.xlt-cart-coupon-form').on('submit',function(){
		/*e.preventDefault();*/
		var $formData = $(this).closest('form'),
			$formActionURL = $formData.attr('action'),
			isValid = $formData.valid(),
			url = Urls.opcApplyCouponCode;
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			url = util.appendParamToURL(url,'isGuestUser',true);
		}
		giftCheck();
		
		if(isValid == "true" || isValid == true){
			progress.show();
			$.ajax({
	            type: "POST",
	            url: url,
	            data: $formData.serialize(),
	            success: function(data, textStatus , xhr) {
	                try {
	                	var ct = xhr.getResponseHeader("content-type") || "";
	                	if((ct.indexOf("text/json")>-1) && !data.status){
	                		//if Session timeout redirect to login page
	                		window.location.href = Urls.opcStartNode;
	                	}else{
	                		$('.xlt-opcPageContent').html(data);
	                		pageAJAXRefreshFlag = true;
		                	reInitialFunction();
		                    /*$('.delivery-address .xlt-continueShopping').trigger('click');*/
	                	}	                	
	                } catch (b) {
	                    console.log(b);
	                }
	                progress.hide();
	            },
	            error: function(f) {
	                console.log(f.statusText + " " + f.status)
	            }
	        });
		}else{
			progress.hide();
		}
		return false;
	});
	
	$('.xlt-coupon-items-list .couponsList .couponAction').on('click',function(){
		/*e.preventDefault();*/
		var $formData = $(this).closest('form'),
			$formActionURL = $formData.attr('action'),
			isValid = $formData.valid(),
			url = Urls.opcRemoveCouponCode;
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			url = util.appendParamToURL(url,'isGuestUser',true);
		}
		giftCheck();
		
		if(isValid == "true" || isValid == true){
			progress.show();
			$.ajax({
	            type: "POST",
	            url: url,
	            data: $formData.serialize(),
	            success: function(data, textStatus , xhr) {
	                try {
	                	var ct = xhr.getResponseHeader("content-type") || "";
	                	if((ct.indexOf("text/json")>-1) && !data.status){
	                		//if Session timeout redirect to login page
	                		window.location.href = Urls.opcStartNode;
	                	}else{
	                		$('.xlt-opcPageContent').html(data);
	                		pageAJAXRefreshFlag = true;
		                	reInitialFunction();
		                	progress.hide();
		                    //$('.delivery-address .xlt-continueShopping').trigger('click');
	                	}	                    
	                } catch (b) {
	                    console.log(b);
	                }
	            },
	            error: function(f) {
	                console.log(f.statusText + " " + f.status)
	            }
	        });
		}else{
			progress.hide();
		}
		return false;
	});
	
	$('.tjc-credit-details .credit-apply-button').on('click', function(e){
		e.preventDefault();
		var $creditInputField = $('.tjc-credit-details .credit-input'),
			tjcCredit = $creditInputField.val(),
			url = Urls.opcApplyCredit,
			creditValue = tjcCredit,
			creditTotal = $creditInputField.attr('data-subtotal'),
			creditAvalilable = $creditInputField.attr('data-creditavailable'),
			newCreditBalance = creditTotal - creditValue,
			userCreditBalance = creditAvalilable - creditValue;
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			url = util.appendParamToURL(url,'isGuestUser',true);
		}
		giftCheck();

		//if Session timeout redirect to login page
		$.ajax({
            type: "GET",
            url: Urls.opcredirect,
            success: function(data) {
                try {
                	if(!data.status){
                		window.location.href = Urls.opcStartNode;
                		return false;
                	}              	                
                } catch (b) {
                    console.log(b);
                }
            },
            error: function(f) {
                console.log(f.statusText + " " + f.status);
            }
        });
		
		$('.payment-method-wrapper .payment-method .field').find('input[name$=paymentMethods_selectedPaymentMethodID]:checked').val("TJCCredit");
		if(tjcCreditsFlag == true){
			if(newCreditBalance <= 0 && userCreditBalance >= 0){
				var $formData = $('form.checkout-billing');
				$('#tjcCreditsFlag').val(tjcCreditsFlag);
				$('.tjc-credit-details .credit-apply-button').css('pointer-events','none');
				$('.tjc-credit-details .credit-apply-button').attr('disabled',true);
				$('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').addClass('freezeSection');
				progress.show();
				$formData.trigger('submit');
			}else{
				tjcCreditsFlag == false;
				$('.credit-balance').css({'color':'#C20000'});
				$('.credit-input').css({'border':'2px solid #c20000'});
			}
		}else if(tjcCredit > 0 && userCreditBalance >= 0 && tjcCreditsFlag == false){
			progress.show();
			$.ajax({
	            type: "GET",
	            url: url,
	            data: {'tjcCredit':tjcCredit, 'tjcCreditsFlag':tjcCreditsFlag},
	            success: function(data) {
	                try {
	                    //$('.delivery-address .xlt-continueShopping').trigger('click');
	                    $('.xlt-opcPageContent').html(data);
	                    pageAJAXRefreshFlag = true;
	                	reInitialFunction();
	                	progress.hide();
	                } catch (b) {
	                    console.log(b);
	                }
	            },
	            error: function(f) {
	                console.log(f.statusText + " " + f.status)
	            }
	        });
		}else{
			//$('.credit-available').css({'color':'#C20000'});
			$('.credit-input').css({'border':'2px solid #c20000'});
		}
		return false;
	});
	
	/*##### Start OPC delivery address model popup box #####*/
	$('.addressmodelbox').off('click').on('click', function(e){
		var url = Urls.opcAddressList,
			addressType = $(this).attr('data-addressType');
		
		giftCheck();
		
		//if Session timeout redirect to login page
		$.ajax({
            type: "GET",
            url: Urls.opcredirect,
            success: function(data) {
                try {
                	if(!data.status){
                		window.location.href = Urls.opcStartNode;
                		return false;
                	}              	                
                } catch (b) {
                    console.log(b);
                }
            },
            error: function(f) {
                console.log(f.statusText + " " + f.status)
            }
        });
		
		
		progress.show();
		dialog.open({ 
	        url: url,
	        options:{
	        	dialogClass:'opcAddressListModelPopup',
	        	open: function(event,ui){
	        		
	        		var sUniformStyled = 'uniformstyled';
                    $(['input:checkbox', 'input:radio', '.opc .existing-card-section #existing-paysafe-card']).each(function () {
                        $(this + ':not(.' + sUniformStyled + ')').not('.no-uniform').addClass(sUniformStyled).uniform();
                    });                   
                    
                    $('body').css('overflow-y','hidden');
                    newformstyle.init();
                    
                    address.init();
                    validator.init();
	        		
	        		$(".opcAddressListModelPopup .ui-dialog-titlebar-close").hide(); /*Hide the close icon*/
	        		progress.hide();
	        		
	        		/*##### Start - Enable/Disable the links when you click the radio button #####*/
	        		var $checkedstatus = $("#opcDeliveryAddresses input:radio[name=opcdeliveryaddresses]").filter(':checked');
	        		
	        		if($checkedstatus.prop("checked")){
	        			$checkedstatus.parents('.form-row-radio').find(".buttonlinks").removeClass("hidden");
	        		}
	        		
	        		var isBillingAddress = false;
	        		$('.address-make-default').attr('data-addressType',addressType);
	        		$('.opcAddressListModelPopup .editOPCaddress .addAddressOpc .editOPCdeliverylocation').attr('data-addressType',addressType);
	        		if(addressType == 'billing'){
	        			$('.opcAddressListModelPopup .addressbooklist .buttonlinks').addClass('billing-address');
	        			isBillingAddress = true;
	        		}else{
	        			$('.opcAddressListModelPopup .addressbooklist .buttonlinks').addClass('delivery-address');
	        			isBillingAddress = false;
	        		}
	        		
	        		$("#opcDeliveryAddresses input[name$=opcdeliveryaddresses]").click(function() {
	        			 var $this = $(this),
	       	         	 $tile = $this.parents('.form-row-radio');
	        			 $('#opcDeliveryAddresses .form-row-radio').find(".buttonlinks").addClass("hidden");
	        			 $tile.find(".buttonlinks").removeClass("hidden");	        		    
        		    });
	        		/*##### End - Enable/Disable the links when you click the radio button #####*/
	        		
	        		/*##### Start - close the dialog/section when you click cancel button in the popup #####*/
	        		$('#opcDeliveryAddresses .closedeliverypopup').on('click', function(e){
	        			dialog.close();
	        		});
	        		
	        		$('#setDefaultaddress-section .address-create').on('click', function(e){
	        			e.preventDefault();
	        			$('#setDefaultaddress-section').addClass('visually-hidden');
	        			
	        			if($('#addOPCAddress-section').hasClass('visually-hidden')){
	        				$('#addOPCAddress-section').removeClass('visually-hidden')
	        			}	
	        			$(".country").trigger('blur');        			
	        		});
	        		
	        		editOPCaddress(); // Edit the OPC address in billing and delivery popup
	        		
	        		$('.addOPCdeliverylocation').off('click').on('click', function(e){
	        			//e.preventDefault();	        			
	        			var $formData = $(this).closest('form'),
							url = Urls.opcAddAddress,
							isValid = $formData.valid();
	        			
	        			if($formData.find('.selected-address-summary').hasClass('visually-hidden')){
	        				$formData.find('.post-code-field-opc .form-row').removeClass('validation-success')
	        				$formData.find('.post-code-field-opc .form-row').addClass('validation-error')
	        				$formData.find('.post-code-field-opc .form-row .field').append('<div class="error-msg">Please select valid address</div>');
	        			}
	        			
		        		if(isValid == "false" || isValid == false){
		        			return false;
		        		}
		        		
		        		var addressid=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_addressid').val(),
							country=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_country').val(),
							postCode=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_postCode').val(),
							address1=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_address1').val(),
							address2=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_address2').val(),
							address3=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_address3').val(),
							address4=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_address4').val(),
							city=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_city').val(),
							addresses=$('#addOPCAddress-section #dwfrm_checkoutaddresses_shippingAddress_address_addresses').val(),
							isGift=($('.gift-receipt .is-gift .checker input.input-checkbox').prop('checked') && $('.gift-receipt .gift-content-with-message .gift-to-name .gift-to-name-content').html().length > 1)?true:false;
						
						progress.show();
						$.ajax({
				            type: "GET",
				            url: url,
				            data: {'country':country,'addressid':addressid,'postCode':postCode,'address1':address1,'address2':address2,'address3':address3,'address4':address4,'city':city,'addresses':addresses,'isBillingAddress':isBillingAddress,'isGift':isGift},
				            success: function(data) {
				                try {				                	
				                	$formData.find('.address-alias .input-text').on('focusin', function() {
				                		$formData.find('.address-alias .field .error-msg').remove();
				            		});
				                	if(data.success == false){
				                		$formData.find('.address-alias .field').append('<div class="error-msg" style="display:block">' + data.data + '</div>');
				                		$formData.find('.address-alias').removeClass('validation-success');
				                		$formData.find('.address-alias .field .input-text').css('border','2px solid #c20000');
				                		progress.hide();
				                	}else{
				                		var AddressJSONStringValue, address; 
					                	if(isBillingAddress == true || isBillingAddress == "true"){
											$('#useThisOPCBillinglocation').html(data);
											if($('#useThisOPCBillinglocation .customerAddresses').length > 0){
												AddressJSONStringValue = $('.checkout-billing-address .customerAddresses .input-radio:checked').attr('data-address');
												var addressFormValue = AddressJSONStringValue;
												fillAddressFields(addressFormValue, '.checkout-billing-address');
												var address = JSON.parse(AddressJSONStringValue),
													addressCountry = "";
												$('.checkout-billing-address select.country').val(address.countryCode);
												addressCountry = $('.checkout-billing-address select.country option:selected').attr('label');
												$('.checkout-billing-address .styledSelect').text(addressCountry);
												//var label = address.ID;
												var label = address.address1;
												label += address.address2 ? ', ' + address.address2 : '';
												label += ', ' + address.city;
												label += addressCountry ? ', ' + addressCountry : '';
												label += address.postCode ? ', ' + address.postCode : '';
												$('.checkout-billing-address #useThisOPCBillinglocation .preferredAddress .custAddress span').html(label);
											}
											progress.hide();
					                	}else{
					                		$('.xlt-opcPageContent').html(data);
					                		$('#currentCheckoutStep').val('0.0');
					                		reInitialFunction();
					                		if($('#currentUserAddressExist').val() == "false"){
					                			$('.delivery-address .xlt-continueShopping').click();
					                		}
					                	}
										dialog.close();
				                	}				                					                	
				                } catch (b) {
				                    console.log(b);
				                }
				            },
				            error: function(f) {
				                console.log(f.statusText + " " + f.status)
				            }
				        });
	        		});
	        		
	        		$('.hide-createaddress').on('click', function(e){
	        			e.preventDefault();
	        			$('.type-own-address').remove();
	        			$('#addOPCAddress-section').addClass('visually-hidden');
	        			$('#editOPCAddress-section').addClass('visually-hidden');
	        			
	        			if($('#setDefaultaddress-section').hasClass('visually-hidden')){
	        				$('#setDefaultaddress-section').removeClass('visually-hidden')
	        			}
						
						$('.addAddressOpc .form-row').removeClass('validation-error');
	        			$('.addAddressOpc .error-msg').hide();
						
	        				/*$('.additional-address-fields').addClass('visually-hidden');
	        				$('.additional-address-fields').removeClass('visually-un-hidden');
	                        $('.type-address-btn-new').css('display' , 'block');
	                        $('.addresses-select-group').css('display' , 'none');
	                        $('.address-check-btn').css('display' , 'block');
                            $('.post-code-field').removeClass('full-width-postcode'); */
	        			
	        			
	        		});
	        		/*##### End - close the dialog/section when you click cancel button in the popup #####*/
	        		        		
	        		usethisOPCDeliveryLocation();	/*##### Set the default delivery address #####*/	   		
	        		
	        	},
	        	close:function() {
	        		
	        		$('body').css('overflow-y','auto');
	        	}
	        }
	    });
	});
	/*##### End OPC delivery address model popup box #####*/
	
	/*##### Start PaySafe Card model popup box #####*/
	$('.paysafe-card-details .paysafemodelbox').off('click').on('click', function(e){
		var url = Urls.opcPaySafeCard_PopUp,
			paysafeCardsList = $(this).attr('data-paysafeCardsList'),
			paysafeCardsJSONList = JSON.parse(paysafeCardsList),
			paysafeSelectedCard = $(this).attr('data-paysafeSelectedCard'),
			thisSelectedMethod = $('.payment-method-wrapper .payment-method input[type$=radio]:checked').closest('.payment-method').attr('class'),
			isForgetCardEnabled = $('#isForgetCardEnabled'),
			forgetCardFlag = "false";
		forgetCardFlag = (isForgetCardEnabled.val()=="true")?(thisSelectedMethod.indexOf("paysafe") != -1)?"Paysafe":(thisSelectedMethod.indexOf("budgetpay") != -1)?"Budgetpay":"false":"false";
		
		giftCheck();
		
		progress.show();
		dialog.open({ 
	        url: url,
	        options:{
	        	dialogClass:'opcAddressListModelPopup',
	        	open: function(event,ui){
                    $(".opcAddressListModelPopup .ui-dialog-titlebar-close").hide(); /*Hide the close icon*/
	        		 newformstyle.init();
	        		
	        		
	        		paysafeCardsJSONList.forEach(function(itemValues){
	        			var tempLabel = itemValues.cardType+': **** **** **** '+itemValues.lastDigits+', Expiry: '+itemValues.cardExpiry.month+'/'+itemValues.cardExpiry.year;
	        			var checkedFlag = "";
	        			if(JSON.stringify(itemValues) == paysafeSelectedCard){checkedFlag="checked"}
	        			var temp = "<div class='form-row form-row-no-indent form-row-radio clearfix'><div class='field'>";
	        			temp += "<input class='input-radio' id='"+itemValues.id+"' type='radio' name='opcpaysafeCard' value='"+JSON.stringify(itemValues)+"' title='PaySafe cards' "+checkedFlag+"/><label style='text-transform: uppercase;' for='"+itemValues.id+"'>"+tempLabel+"</label>";
	        			temp += "</div><div class='buttonlinks hidden'><a href='javascript:void(0)' class='address-make-default'>Use this card</a>";
	        			if(forgetCardFlag=="Paysafe"){
	        				temp += "<a href='javascript:void(0)' data-forgetfrom='paysafe' class='address-edit paysafe'>Use and forget</a></div>";
	        			}else if(forgetCardFlag=="Budgetpay"){
	        				temp += "<a href='javascript:void(0)' data-forgetfrom='budgetpay' data-messageClass='BP"+itemValues.id+"' style='opacity: 0.5;' class='address-edit paysafe'>Use and forget</a>";
	        				temp += "<p class='BP"+itemValues.id+"' style='color:#c20000;display:none;'>You cannot delete this card as it is associated with Budgetpay order.</p></div>";
	        			}
	        			temp += "</div>";
	        			$("#opcPaySafeCards .cardList").append(temp);
	        		});
	        		
	        		var sUniformStyled = 'uniformstyled';
                    $(['input:checkbox', 'input:radio', '.opc .existing-card-section #existing-paysafe-card']).each(function () {
                        $(this + ':not(.' + sUniformStyled + ')').not('.no-uniform').addClass(sUniformStyled).uniform();
                    });
	        		
	        		progress.hide();
	        		
	        		/*##### Start - Enable/Disable the links when you click the radio button #####*/
	        		var $checkedstatus = $("#opcPaySafeCards input:radio[name=opcpaysafeCard]").filter(':checked');
	        		if($checkedstatus.prop("checked")){
	        			$checkedstatus.parents('.form-row-radio').find(".buttonlinks").removeClass("hidden");
	        		}
	        		
	        		$("#opcPaySafeCards input[name$=opcpaysafeCard]").click(function() {
	        			 var $this = $(this),
	       	         	 $tile = $this.parents('.form-row-radio');
	        			 $('#opcPaySafeCards .form-row-radio').find(".buttonlinks").addClass("hidden");
	        			 $tile.find(".buttonlinks").removeClass("hidden");	        		    
        		    });
	        		/*##### End - Enable/Disable the links when you click the radio button #####*/
	        		
	        		/*##### Start - close the dialog/section when you click cancel button in the popup #####*/
	        		$('#opcPaySafeCards .closedeliverypopup').on('click', function(e){
	        			$(".opcAddressListModelPopup .ui-dialog-titlebar-close").trigger('click');
	        		});
	        		/*##### End - close the dialog/section when you click cancel button in the popup #####*/
	        		
	        		$('#opcPaySafeCards .address-make-default').off('click').on('click', function(e){
	        			e.preventDefault();
	        			progress.show();
		        		var opcPaySafeSelectedCards = $('#opcPaySafeCards input[name$=opcpaysafeCard]:checked').val();
	        			
		        		var itemValues = JSON.parse(opcPaySafeSelectedCards);
		        		var tempLabel = itemValues.cardType+': **** **** **** '+itemValues.lastDigits+', Expiry: '+itemValues.cardExpiry.month+'/'+itemValues.cardExpiry.year;
	        			$('.paysafe-card-details .paysafemodelbox').attr('data-paysafeSelectedCard',opcPaySafeSelectedCards);
		        		$('.paysafe-card-details .credit-card-box .firstCard').html(tempLabel);
		        		
		        		$('.paysafe-card-details').find('.selector #existing-paysafe-card').val(opcPaySafeSelectedCards);
		        		var selectLable = $('#existing-paysafe-card').find('option:selected').text();
		        		$('.paysafe-card-details').find('.selector span:first').html(selectLable);
		        		
		        		$('.paysafe-card-details .existing-card-section').css({'display':'block'});
	        			$('.paysafe-card-details .new-card-form').addClass('input-wrapper');
	        			$('.paysafe-card-details .opcPaySafeHeader .title').text('Default Card');
	        			$(".opcAddressListModelPopup .ui-dialog-titlebar-close").trigger('click');
	        			
	        			$('#uniform-existing-paysafe-forget span').removeClass('checked');
	        			$('#existing-paysafe-forget').prop('checked',false);
	        			
	        			enableCheckoutButton(true);
	        			progress.hide();
	        		});
	        		
	        		$('#opcPaySafeCards .address-create-container').off('click').on('click', function(e){
	        			e.preventDefault();
	        			progress.show();
	        			$('.paysafe-card-details').find('#existing-paysafe-card').val("newCard");
						var selectLable = $('#existing-paysafe-card').find('option:selected').text();
						$('.paysafe-card-details').find('.selector span:first').html(selectLable);
		        		
	        			$('.paysafe-card-details .existing-card-section').css({'display':'none'});
	        			$('.paysafe-card-details .new-card-form').removeClass('input-wrapper');
	        			$('.paysafe-card-details .opcPaySafeHeader .title').text('New Card');
	        			$(".opcAddressListModelPopup .ui-dialog-titlebar-close").trigger('click');
	        			enableCheckoutButton(true);
	        			progress.hide();
	        		});
	        		
	        		$('#opcPaySafeCards .address-edit').off('click').on('click', function(e){
						e.preventDefault();
						if($(this).attr('data-forgetfrom')=="paysafe"){
							progress.show();
			        		var opcPaySafeSelectedCards = $('#opcPaySafeCards input[name$=opcpaysafeCard]:checked').val();
		        			
			        		var itemValues = JSON.parse(opcPaySafeSelectedCards);
			        		var tempLabel = itemValues.cardType+': **** **** **** '+itemValues.lastDigits+', Expiry: '+itemValues.cardExpiry.month+'/'+itemValues.cardExpiry.year;
		        			$('.paysafe-card-details .paysafemodelbox').attr('data-paysafeSelectedCard',opcPaySafeSelectedCards);
			        		$('.paysafe-card-details .credit-card-box .firstCard').html(tempLabel);
			        		
			        		$('.paysafe-card-details').find('#existing-paysafe-card').val(opcPaySafeSelectedCards);
			        		var selectLable = $('#existing-paysafe-card').find('option:selected').text();
			        		$('.paysafe-card-details').find('.selector span:first').html(selectLable);
			        		
			        		$('.paysafe-card-details .existing-card-section').css({'display':'block'});
		        			$('.paysafe-card-details .new-card-form').addClass('input-wrapper');
		        			$('.paysafe-card-details .opcPaySafeHeader .title').text('Default Card');
		        			$(".opcAddressListModelPopup .ui-dialog-titlebar-close").trigger('click');
		        			
		        			$('#uniform-existing-paysafe-forget span').addClass('checked');
		        			$('#existing-paysafe-forget').prop('checked',true);
		        			
		        			enableCheckoutButton(true);
						}else if($(this).attr('data-forgetfrom')=="budgetpay"){
							$("."+$(this).attr('data-messageClass')).show();
						}
	        			progress.hide();
					});
	        	},
	        	close:function() {
	        		
	        	}
	        }
	    });
	});
	/*##### End PaySafe Card model popup box #####*/
	
	$('.payment-method-wrapper .input-radio').on('click', function(){
		var classList = $(this).closest('.payment-method').attr('class');
		$('fieldset .xlt-continueCheckout').prop('disabled',true);
		$('form.xlt-continueCheckout').addClass('freezeSection');
		freezeSectionEvent();
		
		if(classList.indexOf("budgetpay") != -1 || classList.indexOf("paysafe") != -1){
			continueCheckout = "card";
			
			enableCheckoutButton(true);
			$('#existing-paysafe-cvv').on('keyup keydown',function(){				
				if($(this).val().length >= 3 && continueCheckout=="card"){
					enableCheckoutButton(true);
				}else{
					enableCheckoutButton(false);
				}
			});
		}else{
			continueCheckout = "non-card";
			enableCheckoutButton(true);
		}
	});
	
	$("#existing-paysafe-cvv").keypress(function (e) {
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	       return false;
	    }
	});
	
	$('form.xlt-continueCheckout').off('submit').on('submit',function(){
		var url = Urls.opcredirect;
		$('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').addClass('freezeSection');
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			url = util.appendParamToURL(url,'isGuestUser',true);
		}
		
		//if Session timeout redirect to login page
		$.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                try {
                	if(!data.status){
                		window.location.href = Urls.opcStartNode;
                		return false;
                	}              	                
                } catch (b) {
                    console.log(b);
                    $('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').removeClass('freezeSection');
                }
            },
            error: function(f) {
                console.log(f.statusText + " " + f.status);
                $('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').removeClass('freezeSection');
            }
        });
		
		var addressFormValid = $('.checkout-addresses-form').valid();
		if(addressFormValid == false || addressFormValid == "false"){
			$('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').removeClass('freezeSection');
			return false;
		}
		
		var $checkoutBilling = '.checkout-billing-address',
			$checkoutShipping = '.checkout-shipping-address',
			$checkoutBillingCountry = $($checkoutBilling).find('.opcAddressFields .selector .input-select.country'),
			$checkoutBillingPostal = $($checkoutBilling).find('.opcAddressFields .additional-address-fields .postCode .input-text'),
			$checkoutBillingAddress1 = $($checkoutBilling).find('.opcAddressFields .additional-address-fields .address1 .input-text'),
			$checkoutBillingCity = $($checkoutBilling).find('.opcAddressFields .additional-address-fields .city .input-text'),
			$checkoutBillingasShipping = $('.billing .checkout-billing-address .checker input').prop('checked');
		
		if(($checkoutBillingCountry.val() == "" || $checkoutBillingPostal.val() == "" || $checkoutBillingAddress1.val() == "" || $checkoutBillingCity.val() == "")){
			billingClassFields($checkoutBillingasShipping);
			
			if(($checkoutBillingCountry.val() == "" || $checkoutBillingPostal.val() == "" || $checkoutBillingAddress1.val() == "" || $checkoutBillingCity.val() == "")){
				billingClassFields(false);
				$('.billing .checkout-billing-address .checker input').prop('checked',false);
				$('.billing .checkout-billing-address .checker span').removeClass('checked');
				$('.checkout-billing-address').parents('form').valid();
				$('.sections.delivery .section, .sections.payment .section, .sections.order-summary .section').removeClass('freezeSection');
				$('html, body').animate({scrollTop: $(".checkout-billing-address").offset().top-60}, 500);
				return false;
			}
		}
		
		var newCard = $('.paysafe-card-details').find('#existing-paysafe-card'),
			newCardVisible = $('.paysafe-card-details').find('.new-card-form'),
			paysafeToken = $('.paysafe-card-details').find('.paysafe-token'),
			selectedCard = $('.paysafe-card-details').find('#existing-paysafe-card');
		if((newCard.val() !== 'newCard' && !newCardVisible.is(':visible'))){
			if(paysafeToken.val() == ""){
				paysafeToken.val(selectedCard.val());console.warn("token is populated");
			}
		}
		
		progress.show();
		
		var $formData = $('form.checkout-billing'),
			url = $formData.attr('action');
		
		if(document.URL.indexOf('isGuestUser=true') > 0){
			$formData.attr('action',$formData.attr('action')+'?isGuestUser=true')
		}
		
		$('#tjcCreditsFlag').val('false');
		$formData.trigger('submit');
		
		return false;
	});
}

function validateFieldsCount(){
	$('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').on('keyup keydown change', function(){
		validateAdditionalAddressFied(this);
	});
	
	$('.checkout-shipping-address .address-check-btn button').off('click').on('click', function(){
		$('.checkout-shipping-address .type-address-btn').off('click').on('click', function(){
			validateAllAddressFied();
		});
		$('.checkout-shipping-address .addresses-select select').on('change', function(){
			validateAllAddressFied();
		});
	});
}

function validateAllAddressFied(){
	$('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').each(function(){		
		validateAdditionalAddressFied(this);
	});
}

function validateAdditionalAddressFied($this){	
		var countvalue = $($this).attr('maxlength') - $($this).val().length;
		$($this).closest('.address-content').find('.addresscharacterlength .count').html(countvalue);
	    if (countvalue < 1 ) {
	    	$($this).closest('.address-content').find('.addresscharacterlength').addClass('alert-danger');
	    }
	    else {
	    	$($this).closest('.address-content').find('.addresscharacterlength').removeClass('alert-danger');
	    }
}

function giftCheck(){
	var giftRecipient = ($('.gift-receipt .is-gift .checker input.input-checkbox').prop('checked') && $('.gift-receipt .gift-content .gift-recipient-name .input-text').val().length > 1)?false:true;
	if(giftRecipient){
		$('.gift-receipt .is-gift .checker input.input-checkbox').prop('checked',false);
		$('.gift-receipt .is-gift .checker input.input-checkbox').val(false);
		$('.gift-receipt .is-gift .checker span').removeClass('checked');
		$("div[id$='_shippingAddress_isGift']").trigger('click');
	}
}

function usethisOPCDeliveryLocation(){
	$('.buttonlinks .address-make-default').off('click').on('click', function(e){
		e.preventDefault();
		var url = Urls.usethisOPCDeliveryLocation,
			$addressType = $(this).attr('data-addressType'),
			addressFormValue = $(this).attr('data-address');
		var addressID = $('#opcDeliveryAddresses input[name$=opcdeliveryaddresses]:checked').val();
		url = util.appendParamToURL(url,'AddressID',addressID);
		
		if($addressType == 'delivery'){
			progress.show();
			$.ajax({
	            type: "POST",
	            url: url,	        	            
	            success: function(data) {
	                try {
	                	$('#useThisOPCDeliverylocation').html(data);
	                	$('#currentCheckoutStep').val('0.0');
						reInitialFunction();
	                	if($('#currentUserAddressExist').val() == "false"){
	                		$('.delivery-address .xlt-continueShopping').trigger('click');
	                	}
	                	dialog.close();
	                } catch (b) {
	                    console.log("setDefaultAddressOPC" + b);
	                }
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                console.log("setDefaultAddressOPC" + jqXHR + textStatus + errorThrown)
	            }
	        });
		}else if($addressType == 'billing'){
			fillAddressFields(addressFormValue, '.checkout-billing-address');
			var address = JSON.parse(addressFormValue),
				addressCountry = "";
			//var label = address.ID;
			$('.checkout-billing-address select.country').val(address.countryCode);
			addressCountry = $('.checkout-billing-address select.country option:selected').attr('label');
			$('.checkout-billing-address .styledSelect').text(addressCountry);
			var label = address.address1;
            label += address.address2 ? ', ' + address.address2 : '';
            label += ', ' + address.city;
            label += addressCountry ? ', ' + addressCountry : '';
            label += address.postCode ? ', ' + address.postCode : '';
	        $('.checkout-billing-address #useThisOPCBillinglocation .preferredAddress .custAddress span').html(label);
			$('.opcAddressListModelPopup .closedeliverypopup').trigger('click');
		}
	});
}

function editOPCaddress(){
	$('#setDefaultaddress-section .address-edit').on('click', function(e){
		e.preventDefault();
		$('#setDefaultaddress-section').addClass('visually-hidden');
		
		if($('#editOPCAddress-section').hasClass('visually-hidden')){
			$('#editOPCAddress-section').removeClass('visually-hidden')
		}
		
		var	url = Urls.opcEditaddress,
			addressID = $('#opcDeliveryAddresses input[name$=opcdeliveryaddresses]:checked').val(),
			url = util.appendParamToURL(url,'AddressID',addressID);	        			
		progress.show();
		$.ajax({
	        type: "POST",
	        url: url,
	        success: function(data) {
	            try {
	               $('#editOPCAddress').html(data);
	               /*$('<div class="form-row form-row-button type-address-btn-new"> <a class="button-new showmore">Enter Address Manually </a> </div>').insertAfter(".editOPCaddress .post-code-field");*/
	               $('<div class="form-row form-row-button enable-find-my-address"><a class="showmore">Use Address Finder </a></div>').insertAfter(".editOPCaddress .address-content-last");
	               	progress.hide();
	                //reInitialFunction();
	                address.init()
	                validator.init();
	                newformstyle.init();
	                $('.countrycheck select.country').trigger('blur');
	                $('.editOPCaddress .editOPCdeliverylocation').off('click').on('click', function(e){
	            		//e.preventDefault();	        			
	            		var $formData = $(this).closest('form'),
	            			url = Urls.opcSaveEditaddress,
	            			isValid = $formData.valid();
	            			addressID = $('#opcDeliveryAddresses input[name$=opcdeliveryaddresses]:checked').val(),
	            			url = util.appendParamToURL(url,'AddressID',addressID);
	            			
            			if($formData.find('.selected-address-summary').hasClass('visually-hidden')){
            				e.preventDefault();
	        				$formData.find('.post-code-field .form-row').removeClass('validation-success')
	        				$formData.find('.post-code-field .form-row').addClass('validation-error')
	        				$formData.find('.post-code-field .form-row .field').append('<div class="error-msg">Please select valid address</div>');
	        				return false;
	        			}
	            		
	            		if(isValid == "false" || isValid == false){
	            			return false;
	            		}	            		
	            		
	            		$.ajax({
	        	            type: "POST",
	        	            data: $formData.serialize(),
	        	            url: url,	        	            
	        	            success: function(data) {
	        	                try {
	        	                	dialog.close();	
	        	                	$('.checkout-shipping-address .addressmodelbox').trigger('click');        	                	
	        	                } catch (b) {
	        	                    console.log("EditOPCAddress" + b);
	        	                }
	        	            },
	        	            error: function(jqXHR, textStatus, errorThrown) {
	        	                console.log("EditOPCAddress" + jqXHR + textStatus + errorThrown)
	        	            }
	        	        });
	            	});
	            } catch (b) {
	                console.log(b);
	            }
	        },
	        error: function(f) {
	            console.log(f.statusText + " " + f.status)
	        }
	    });
	});
}

function billingClassFields(checkedFlag){
	var $billingClass = $('.billing .checkout-billing-address');
	if(checkedFlag == true || checkedFlag == "true"){
		$billingClass.find('.opcDeliveryAddress').css({'display':'none'});
		$billingClass.find('#useThisOPCBillinglocation').css({'display':'none'});
		$billingClass.find('.opcAddressFields').css({'display':'none'});
		if($('#useThisOPCDeliverylocation .customerAddresses').length > 0){
			var addressFormValue = $('.checkout-shipping-address .customerAddresses .input-radio:checked').attr('data-address');
			if(typeof addressFormValue  === 'undefined')
				addressFormValue = $('.checkout-shipping-address .customerAddresses .input-radio:first').attr('data-address');
			fillAddressFields(addressFormValue, '.checkout-billing-address');
			var address = JSON.parse(addressFormValue),
				addressCountry = "";
			$('.checkout-billing-address select.country').val(address.countryCode);
			addressCountry = $('.checkout-billing-address select.country option:selected').attr('label');
			$('.checkout-billing-address .styledSelect').text(addressCountry);
			//var label = address.ID;
			var label = address.address1;
			label += address.address2 ? ', ' + address.address2 : '';
			label += ', ' + address.city;
			label += addressCountry ? ', ' + addressCountry : '';
			label += address.postCode ? ', ' + address.postCode : '';
			$('.checkout-billing-address #useThisOPCBillinglocation .preferredAddress .custAddress span').html(label);
		}else{
			fillAddressFields(null, null,'.checkout-shipping-address', '.checkout-billing-address');
		}
	}else{
		$billingClass.find('.opcDeliveryAddress').css({'display':'block'});
		$billingClass.find('#useThisOPCBillinglocation').css({'display':'block'});
		$billingClass.find('.opcAddressFields').css({'display':'block'});
	}
}

function fillAddressFields(addressFormValue, parentClass, fromForm, toForm){
	if(parentClass!=null){
		var JSONaddressFormValue = JSON.parse(addressFormValue);
		$(parentClass).find('.opcAddressFields .input-select.country').val(JSONaddressFormValue.countryCode);
		var selectLable = $(parentClass).find('.opcAddressFields .input-select.country option:selected').attr('label');
		$(parentClass).find('.opcAddressFields .selector span').html(selectLable);
		//$(parentClass).find('.opcAddressFields .post-code-field .input-text').val(JSONaddressFormValue.postCode);
		$(parentClass).find('.opcAddressFields .additional-address-fields .address1 .input-text').val(JSONaddressFormValue.address1);
		$(parentClass).find('.opcAddressFields .additional-address-fields .address2 .input-text').val(JSONaddressFormValue.address2);
		$(parentClass).find('.opcAddressFields .additional-address-fields .address3 .input-text').val(JSONaddressFormValue.address3);
		$(parentClass).find('.opcAddressFields .additional-address-fields .address4 .input-text').val(JSONaddressFormValue.address4);
		$(parentClass).find('.opcAddressFields .additional-address-fields .city .input-text').val(JSONaddressFormValue.city);
		$(parentClass).find('.opcAddressFields .additional-address-fields .postCode .input-text').val(JSONaddressFormValue.postCode);
	}else{
		var countryCode = $(fromForm).find('.opcAddressFields .input-select.country').val(),
			selectLable = $(fromForm).find('.opcAddressFields .input-select.country option:selected').attr('label'),
			postCode = $(fromForm).find('.opcAddressFields .post-code-field .input-text').val();
		
		$(toForm).find('.opcAddressFields .input-select.country').val(countryCode);
		$(toForm).find('.opcAddressFields .selector span').html(selectLable);
		$(toForm).find('.opcAddressFields .post-code-field .input-text').val(postCode);
		
		$.each(['address1', 'address2', 'address3', 'address4', 'locality', 'city', 'postCode'], function (i, attribute) {
			var value = $(fromForm).find('.additional-address-fields input[name$=' + attribute + ']').val();
			$(toForm).find('.additional-address-fields input[name$=' + attribute + ']').val(value);
		});
	}
}

function isDecimal(number) {
	return new RegExp(/^[-+]?[0-9]+\.[0-9]+$/).test(number) === true;
}

function checkoutStepFunction() {
	$('.checkout-billing-methods #cart-shippingmethod-form').addClass('freezeSection');
	$('.payment .paymentMethod').addClass('freezeSection');
	$('form.xlt-continueCheckout').addClass('freezeSection');
	//$('.cart-coupon-code').addClass('freezeSection');
	$('.couponsList').addClass('freezeSection');
	
	switch($('#currentCheckoutStep').val()){
		case "0.0":
			$('.couponsList').removeClass('freezeSection');
			if($('#continueButtonShow').val() == "true" && $('#checkoutError').val() == "false"){
				paymentErrorMessage = "";
				paymentErrorType = "";
				$('.delivery-address .xlt-continueShopping').trigger('click');
			}else if($('#currentUserAddressExist').val() == "true" && $('#checkoutError').val() == "true"){
				if($('.checkout-summary form.checkout-billing .billing-error').length >= 1){
					paymentErrorMessage = $('.checkout-summary form.checkout-billing .billing-error').text();
					paymentErrorType = "billing-error";
				}else if($('.checkout-summary form.checkout-billing .error-form').length >= 1){
					paymentErrorMessage = $('.checkout-summary form.checkout-billing .error-form').text();
					paymentErrorType = "error-form";
				}
				$('.delivery-address .xlt-continueShopping').trigger('click');
			}
			break;
		case "1.0":
			$('.checkout-billing-methods #cart-shippingmethod-form').removeClass('freezeSection');
			$('.payment .paymentMethod').removeClass('freezeSection');
            //$('.cart-coupon-code').removeClass('freezeSection');
            $('.couponsList').removeClass('freezeSection');
            $('.opcAddressFields .additional-address-fields').removeClass('visually-hidden');
            if(typeof paymentErrorMessage !== 'undefined' && typeof paymentErrorType !== 'undefined' && paymentErrorType != "" && paymentErrorMessage != ""){
            	if($('.checkout-summary form.checkout-billing .'+paymentErrorType).length < 1){
            		$('.checkout-summary form.checkout-billing').prepend("<div class='"+paymentErrorType+"'>"+paymentErrorMessage+"</div>");
            		window.dataLayer.push({
            			'event': 'checkoutError',
            			'ecommerce': {
            				'errorText': 'Payment Error',
            				'errorMessage': paymentErrorType+': '+paymentErrorMessage.trim()
            			}
            		});
            	}
            }
            var couponFormClass = $('.xlt-cart-coupon-form .form-row').attr('class');
        	if(couponFormClass.indexOf("validation-error") == -1 && typeof pageAJAXRefreshFlag !== 'undefined' && pageAJAXRefreshFlag == true){
        		$('html, body').animate({scrollTop: $("#checkout-billing-methods").offset().top-60}, 500);
        		pageAJAXRefreshFlag = false;
        	}else{
        		$("html").stop(true, false).animate({ scrollTop: 0 }, "fast");
        		floatingOrderSummary();
        		var couponErrorActive = $('.xlt-cart-coupon-form .form-row .error-msg .active:visible').length,
        		    couponErrorAlready = $('.xlt-cart-coupon-form .form-row .error-msg .already:visible').length,
        		    couponErrorMessage = '';
        		if(couponErrorActive > 0){
        			couponErrorMessage = $('.xlt-cart-coupon-form:visible .form-row .error-msg .active').text();
        		}else if(couponErrorAlready > 0){
        			couponErrorMessage = $('.xlt-cart-coupon-form:visible .form-row .error-msg .already').text();
        		}else{
        			couponErrorMessage = $('.xlt-cart-coupon-form:visible .form-row .error-msg .couponstatus').text();
        		}
        		if(couponErrorMessage != '' && couponErrorMessage != 'null'){
        			window.dataLayer.push({
            			'event': 'checkoutError',
            			'ecommerce': {
            				'errorText': 'Coupon Error',
            				'errorMessage': ''+couponErrorMessage.trim()
            			}
            		});
        		}
        	}
			break;
		default:
			break;
	}
}

function enableCheckoutButton(flagValue){
	if($('#isOPCPage').val() == "true" && flagValue == true && typeof continueCheckout !== 'undefined'){
		if(continueCheckout == "card"){
			if($('#existing-paysafe-card').val() == "newCard" || $('#existing-paysafe-card').length == 0){
				var cardNumberValid = $('.new-card-form #cardNumber').hasClass('success'),
					cardExpiryValid = $('.new-card-form #cardExpiry').hasClass('success'),
					cardCVVValid = $('.new-card-form #cardCVC').hasClass('success');
				if(cardNumberValid && cardExpiryValid && cardCVVValid){
					$('fieldset .xlt-continueCheckout').prop('disabled',false);
					$('form.xlt-continueCheckout').removeClass('freezeSection');
					freezeSectionEventAction();
				}else{
					$('fieldset .xlt-continueCheckout').prop('disabled',true);
					$('form.xlt-continueCheckout').addClass('freezeSection');
					freezeSectionEvent();
				}
			}else{
				if($('#existing-paysafe-cvv').val().length > 0){
					$('fieldset .xlt-continueCheckout').prop('disabled',false);
					$('form.xlt-continueCheckout').removeClass('freezeSection');
					freezeSectionEventAction();
				}else{
					$('fieldset .xlt-continueCheckout').prop('disabled',true);
					$('form.xlt-continueCheckout').addClass('freezeSection');
					freezeSectionEvent();
				}
			}
		}else if(continueCheckout == "non-card"){
			$('fieldset .xlt-continueCheckout').prop('disabled',false);
			$('form.xlt-continueCheckout').removeClass('freezeSection');
			freezeSectionEventAction();
		}else{
			$('fieldset .xlt-continueCheckout').prop('disabled',true);
			$('form.xlt-continueCheckout').addClass('freezeSection');
			freezeSectionEvent();
		}
	}else if($('#isOPCPage').val() == "true" && flagValue == false){
		$('fieldset .xlt-continueCheckout').prop('disabled',true);
		$('form.xlt-continueCheckout').addClass('freezeSection');
		freezeSectionEvent();
	}
}

function freezeSectionEvent(){
	$('.freezeSection').parent('div, fieldset').off('click').on('click',function(e){
		freezeSectionEventAction();
	});
}
function freezeSectionEventAction(){
	var addressFormValid = $('.checkout-addresses-form').valid(),
		addressSubmitVisible = $('.xlt-addressForm button.xlt-continueShopping').is(":visible") && !$('.xlt-addressForm button.xlt-continueShopping').hasClass("visually-hidden");
	if(addressFormValid == false || addressFormValid == "false"){
		opcFormValidation();
		$('html').stop(true, false).animate({scrollTop: $(".delivery .required-indicator:visible").offset().top-70}, 500);
		return false;
	}else{
		var formFields = "";
		$.each(['.guestDetails', '.delivery-address'], function (i, attribute) {
			if($(attribute).find('.validation-error').length > 0){
			}else{
				$(attribute).find('.required-indicator').css('display','none');
			}
		});
		if(formFields.indexOf(".guestDetails") > -1){
		}else{
			$('.customFreezeMessageDeliveryContact').hide();
		}
		if(formFields.indexOf(".delivery-address") > -1){
		}else{
			$('.customFreezeMessageDeliveryAddress').hide();
		}
	}
	if(addressSubmitVisible == true || addressSubmitVisible == "true"){
		$(".xlt-addressForm button.xlt-continueShopping").css({'box-shadow':'0 0 10px #492565','font-weight':'900'});
		$('html').stop(true, false).animate({scrollTop: $(".xlt-addressForm button.xlt-continueShopping").offset().top-10}, 500);
		return false;
	}
	if(typeof continueCheckout !== 'undefined'){
		if(continueCheckout == "card"){
			if($('#existing-paysafe-card').val() == "newCard" || $('#existing-paysafe-card').length == 0){
				var cardNumberValid = $('.new-card-form #cardNumber').hasClass('success'),
					cardExpiryValid = $('.new-card-form #cardExpiry').hasClass('success'),
					cardCVVValid = $('.new-card-form #cardCVC').hasClass('success');
				if(cardNumberValid && cardExpiryValid && cardCVVValid){
					$('.paymentMethod').find('.required-indicator').css('display','none');
					$('.customFreezeMessagePayment').hide();
				}else{
					$('.paymentMethod').find('.required-indicator').css('display','unset');
					if($('.customFreezeMessagePayment:visible').length < 1){
						$("<div style='margin-bottom:10px;' class='customErrorMessage customFreezeMessagePayment'>Please complete payment details</div>").insertBefore( ".payment .paymentMethod legend:first");
					}
					$('html').stop(true, false).animate({scrollTop: $(".required-indicator:visible").offset().top-70}, 500);
				}
			}else{
				if($('#existing-paysafe-cvv').val().length > 0){
					$('.paymentMethod').find('.required-indicator').css('display','none');
					$('.customFreezeMessagePayment').hide();
					$('.existing-card-options #existing-paysafe-cvv').removeClass('validation-error');
					$('.existing-card-options .cvv-input-error').hide();
				}else{
					$('.paymentMethod').find('.required-indicator').css('display','unset');
					$('.existing-card-options #existing-paysafe-cvv').addClass('validation-error');
					$('.existing-card-options .cvv-input-error').show();				
					if($('.customFreezeMessagePayment:visible').length < 1){
						$("<div style='margin-bottom:10px;' class='customErrorMessage customFreezeMessagePayment'>Please complete payment details</div>").insertBefore( ".payment .paymentMethod legend:first");
					}
					$('html').stop(true, false).animate({scrollTop: $(".required-indicator:visible").offset().top-70}, 500);
				}
			}
		}else if(continueCheckout == "non-card"){
			$('.paymentMethod').find('.required-indicator').css('display','none');
			$('.customFreezeMessagePayment').hide();
		}
		return false;
	}
}
function opcFormValidation(){
	var formFields = "",
		customFreezeFlag = false;
	$.each(['.guestDetails', '.delivery-address'], function (i, attribute) {
		if($(attribute).find('.validation-error').length > 0){
			$(attribute).find('.required-indicator').css('display','unset');
			formFields += attribute;
		}else{
			$(attribute).find('.required-indicator').css('display','none');
		}
	});
	if(formFields.indexOf(".guestDetails") > -1 && !customFreezeFlag){
		if($('.customFreezeMessageDeliveryContact:visible').length < 1){
			$("<div style='margin-bottom:10px;' class='customErrorMessage customFreezeMessageDeliveryContact'>Please complete contact details</div>").insertBefore( ".delivery .guestDetails legend:first");
		}
		customFreezeFlag = true;
	}else{
		$('.customFreezeMessageDeliveryContact').hide();
	}
	if(formFields.indexOf(".delivery-address") > -1 && !customFreezeFlag){
		if($('.customFreezeMessageDeliveryAddress:visible').length > 0){$('.customFreezeMessageDeliveryAddress').remove();}
		if($('.customFreezeMessageDeliveryAddress:visible').length < 1 && $(".gift-content .gift-save-button").is(":visible") == false && !$('.addresses-select-group .addresses-select').hasClass('loqate-select')){
			$("<div style='margin-bottom:10px;' class='customErrorMessage customFreezeMessageDeliveryAddress'>Please complete delivery address details</div>").insertBefore( ".delivery .delivery-address legend:first");
		}
		customFreezeFlag = true;
	}else{
		$('.customFreezeMessageDeliveryAddress').hide();
	}
}

function floatingOrderSummary(){
	var stickyHeight = $('.xlt-opcPageContent .sections-col-div-40 .sections-col-div-div').height() - 320;
	$(window).scroll(function(){
		var sticky = $('.xlt-opcPageContent .sections-col-div-40 .sections-col-div-div'),
			scroll = $(window).scrollTop();
		if (scroll >= 56){ sticky.addClass('floatingHeader'); }
		else { sticky.removeClass('floatingHeader'); }
	});
}

function reInitialFunction(){
	if($('#isOPCPage').val()=="true"){
		if(typeof paymentTriggeredClass !== 'undefined' && paymentTriggeredClass != ""){
			if(paymentTriggeredClass.indexOf("paysafe") != -1){
				$('.payment-method-wrapper .payment-method.paysafe .field').find('input.input-radio').prop('checked', true);
			}else if(paymentTriggeredClass.indexOf("budgetpay") != -1){
				$('.payment-method-wrapper .payment-method.budgetpay .field').find('input.input-radio').prop('checked', true);
			}else if(paymentTriggeredClass.indexOf("paypal-credit") != -1){
				$('.payment-method-wrapper .payment-method.paypal-credit .field').find('input.input-radio').prop('checked', true);
			}else if(paymentTriggeredClass.indexOf("paypal") != -1){
				$('.payment-method-wrapper .payment-method.paypal .field').find('input.input-radio').prop('checked', true);
			}
		}
		
		if($('.paymentMethod .checkout-summary .payment-methods').length > 0 && $('#payment-form .new-card-form iframe').length < 3){
			paysafe.init();
		}
		
		if($('#currentCheckoutStep').val() == '1.0'){
			$('.payment-method-wrapper .payment-method.paysafe .field').find('input.input-radio').prop('checked', true);
		}
		
		var sUniformStyled = 'uniformstyled';
		$(['input:checkbox', 'input:radio', '.opc .existing-card-section #existing-paysafe-card']).each(function () {
			$(this + ':not(.' + sUniformStyled + ')').not('.no-uniform').addClass(sUniformStyled).uniform();
		});
		
		var shippingMethodsDescriptionJSON = JSON.parse(Resources.ShippingMethods_CheckoutDescription), method_id, method_labelText;
		$.each(shippingMethodsDescriptionJSON, function(key, data){
			method_id = $('input[value='+key+']').attr('id');
			method_labelText = $('label[for='+method_id+']').text();
			$('label[for='+method_id+']').html(method_labelText + '<span class="delivery-method-description">('+data+')</span>');
		});
		
		initialFunction();
		
		validator.init();
		newformstyle.init();
	}else{
    	paysafe.init();
    }
	
	if ($('.checkout-summary')) {
        billing.init();
    }
	
    if ($('.checkout-addresses-form')) {
        address.init();
    }
    
    //if on the order review page and there are products that are not available diable the submit order button
    if ($('.order-summary-footer').length > 0) {
        if ($('.notavailable').length > 0) {
            $('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
        }
    }
    
    tabs.init();
    newformstyle.init();
    
    if($('#isOPCPage').length == 0){console.log("form submit initialized");
    	$('form.checkout-billing').on('submit',function(){
	    	var newCard = $('.paysafe-card-details').find('#existing-paysafe-card'),
				newCardVisible = $('.paysafe-card-details').find('.new-card-form'),
				paysafeToken = $('.paysafe-card-details').find('.paysafe-token'),
				selectedCard = $('.paysafe-card-details').find('#existing-paysafe-card');
			if((newCard.val() !== 'newCard' && !newCardVisible.is(':visible'))){
				if(paysafeToken.val() == ""){
					paysafeToken.val(selectedCard.val());console.warn("token is populated");
				}
			}
    	});
    }
    
    if($('#isOPCPage').val()=="true"){
	    if(typeof paymentTriggeredClass !== 'undefined' && paymentTriggeredClass != ""){
	    	$('.payment-method-wrapper .payment-method input[type$=radio]').trigger('change');
	    	setTimeout(function(){
	    		if(paymentTriggeredClass.indexOf("paysafe") != -1){
	    			$('.payment-method-wrapper .payment-method.paysafe input').trigger('click');
		    		continueCheckout = "card";
				}else if(paymentTriggeredClass.indexOf("budgetpay") != -1){
					$('.payment-method-wrapper .payment-method.budgetpay input').trigger('click');
					continueCheckout = "card";
				}else if(paymentTriggeredClass.indexOf("paypal-credit") != -1){
					$('.payment-method-wrapper .payment-method.paypal-credit input').trigger('click');
					continueCheckout = "non-card";
					enableCheckoutButton(true);
				}else if(paymentTriggeredClass.indexOf("paypal") != -1){
					$('.payment-method-wrapper .payment-method.paypal input').trigger('click');
					continueCheckout = "non-card";
					enableCheckoutButton(true);
				}
	    	}, 500);
		}
	    
	    if($('#currentCheckoutStep').val() == '1.0'){
	    	$('.payment-method-wrapper .payment-method input[type$=radio]').trigger('change');
	    	setTimeout(function(){
	    		$('.payment-method-wrapper .payment-method.paysafe input').trigger('click');
	    		continueCheckout = "card";
	    		setTimeout(function(){ $('#existing-paysafe-cvv:visible').trigger('focus'); $('#existing-paysafe-cvv:visible').trigger('click'); }, 500);
	    	}, 500);
	    }
	    
		checkoutStepFunction();
		
		freezeSectionEvent();
		floatingOrderSummary();
		
		$('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').each(function(){
			var countvalue = $(this).attr('maxlength') - $(this).val().length;
			$(this).closest('.address-content').find('.addresscharacterlength .count').html(countvalue);
			if (countvalue < 1 ) {
				$(this).closest('.address-content').find('.addresscharacterlength').addClass('alert-danger');
			} else {
				$(this).closest('.address-content').find('.addresscharacterlength').removeClass('alert-danger');
			}
		});
    }

	$('.existing-cards').on('blur', function() {
		if( $(this).val().length > 0 ) {
			$(this).removeClass('validation-error');
			$('.cvv-input-error').hide();
		}else{
			$(this).addClass('validation-error');
			$('.cvv-input-error').show();
		}
	});

}

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
module.exports = {
    init: reInitialFunction,
    enableCheckoutButton: enableCheckoutButton,
};
