'use strict';

var addressFields = require('../../address-fields'),
	account = require('../account'),
	ajax = require('../../ajax'),
	progress = require('../../progress');

var address; 
var giftReceiptSelected = false;
function addShowAddressFieldsEvent($radioGroup) {
    $radioGroup.on('change', function () {
        var toggleFlag = $radioGroup.filter(':checked').hasClass('alternative-address');
        $radioGroup.parents('fieldset').find('.alternative-address-fields').toggleClass('visually-hidden', !toggleFlag);
    });
}

function initializeEvents() {
    var $checkoutAddressesForm = $('.checkout-addresses-form'),
    	$isOPCPage = $('.checkout-addresses-form').find('input[name$=isOPCPage]'),
        $submitButton = $checkoutAddressesForm.find('.submit-button'),
        $billingAddressRadioGroup = $checkoutAddressesForm.find('input:radio[name=billing-address]'),
        $shippingAddressRadioGroup = $checkoutAddressesForm.find('input:radio[name=shipping-address]');
    addShowAddressFieldsEvent($billingAddressRadioGroup);
    addShowAddressFieldsEvent($shippingAddressRadioGroup);
    addressFields.init($checkoutAddressesForm);

    if($isOPCPage.val() != "true"){
    $checkoutAddressesForm.on('submit', function (e) {
        var $selectedBillingOption = $billingAddressRadioGroup.filter(':checked'),
            $billingAddressFields = $billingAddressRadioGroup.parents('fieldset').find('.alternative-address-fields'),
            $additionalBillingAddressFields = $billingAddressFields.find('.additional-address-fields'),
            $selectedShippingOption = $shippingAddressRadioGroup.filter(':checked'),
            $shippingAddressFields = $shippingAddressRadioGroup.parents('fieldset').find('.alternative-address-fields'),
            $additionalShippingAddressFields = $shippingAddressFields.find('.additional-address-fields'),
            isValid = $checkoutAddressesForm.valid(),
            $billingAddToAddressBookCheckBox = $billingAddressFields.find('input[name$=addToAddressBook]'),
            $shippingAddToAddressBookCheckBox = $shippingAddressFields.find('input[name$=addToAddressBook]'),
            $billingFindButton = $billingAddressFields.find('.post-code-field button'),
            $shippingFindButton = $shippingAddressFields.find('.post-code-field button'),
            $billingPostCodeInput = $billingAddressFields.find('.post-code-field input'),
            $shippingPostCodeInput = $shippingAddressFields.find('.post-code-field input'),
            selectedBillingOptionData = $selectedBillingOption.data('address'),
            selectedShippingOptionData = $selectedShippingOption.data('address');

        if (!$selectedBillingOption.hasClass('alternative-address')) {
            $billingAddressFields.find('input[name$=addressid]').val(selectedBillingOptionData.ID);
            $billingAddressFields.find('select[name$=country]')
                .val(selectedBillingOptionData.countryCode).click();
            $billingPostCodeInput.val(selectedBillingOptionData.postCode);
            addressFields.fillAddressFields($additionalBillingAddressFields, selectedBillingOptionData);
            if ($billingAddToAddressBookCheckBox.prop('checked')) {
                $billingAddToAddressBookCheckBox.click();
            }
        } else {
            $billingAddressFields.find('input[name$=addressid]').val('dwfrm_alternative-address');
        }
        if ($selectedShippingOption.hasClass('use-billing-address')) {
            $shippingAddressFields.find('input[name$=addressid]').val('dwfrm_use-billing-address');
            $shippingAddressFields.find('select[name$=country]')
                .val($billingAddressFields.find('select[name$=country]').val()).click();
            $shippingPostCodeInput.val($billingAddressFields.find('.post-code-field input').val());
            /*jslint unparam: true */
            $.each(['address1', 'address2', 'address3', 'address4', 'locality', 'city', 'postCode'], function (i, attribute) {
                var value = $additionalBillingAddressFields.find('input[name$=' + attribute + ']').val();
                $additionalShippingAddressFields.find('input[name$=' + attribute + ']').val(value);
            });
            /*jslint unparam: false */
            if ($shippingAddToAddressBookCheckBox.prop('checked')) {
                $shippingAddToAddressBookCheckBox.click();
            }
        } else if (!$selectedShippingOption.hasClass('alternative-address')) {
            $shippingAddressFields.find('input[name$=addressid]').val(selectedShippingOptionData.ID);
            $shippingAddressFields.find('select[name$=country]')
                .val(selectedShippingOptionData.countryCode).click();
            $shippingPostCodeInput.val(selectedShippingOptionData.postCode);
            addressFields.fillAddressFields($additionalShippingAddressFields, selectedShippingOptionData);
            if ($shippingAddToAddressBookCheckBox.prop('checked')) {
                $shippingAddToAddressBookCheckBox.click();
            }
        } else {
            $shippingAddressFields.find('input[name$=addressid]').val('dwfrm_alternative-address');
        }

        if (isValid !== $checkoutAddressesForm.valid()) {
            e.preventDefault();
            $checkoutAddressesForm
                .append($('<input/>', {type: 'hidden', name: $submitButton.attr('name'), value: 'save'}))
                .submit();
        }

        if (!$billingAddressFields.hasClass('visually-hidden') && $billingFindButton.is(':visible') &&
                $billingPostCodeInput.val() !== '' && !$billingFindButton.data('searchstarted')) {
            $billingFindButton.click();
            e.preventDefault();
        }

        if (!$shippingAddressFields.hasClass('visually-hidden') && $shippingFindButton.is(':visible') &&
                $shippingPostCodeInput.val() !== '' && !$shippingFindButton.data('searchstarted')) {
            $shippingFindButton.click();
            e.preventDefault();
        }
    });
    }
    $('select.country').on('click',function(){
    	if($("#isWarrantyOnCart").length > 0 && $("#isWarrantyOnCart").val() == "true"){
    		if($(this).closest('.xlt-addressForm').hasClass('checkout-shipping-address') && $('.checkout-shipping-address .warrantyAddressMessage').length == 0){
    			$('.checkout-shipping-address select.country').parents('.form-row').after('<span class="warrantyAddressMessage" style="display: block;font-weight: 900;font-size: 13px;color: #cc0000;text-align: justify;line-height: 14px;">Warranty is only for UK customers. Please remove warranty from your order, for EU delivery.</span>');
    		}else if($(this).closest('.xlt-addressForm').hasClass('checkout-billing-address') && $('.checkout-billing-address .warrantyAddressMessage').length == 0){
    			$('.checkout-billing-address select.country').parents('.form-row').after('<span class="warrantyAddressMessage" style="display: block;font-weight: 900;font-size: 13px;color: #cc0000;text-align: justify;line-height: 14px;">Warranty is only for UK customers. Please remove warranty from your order, for EU delivery.</span>');
    		}
    	}
    });

    /*Start Loqate Email Validation*/
    $('.email-fileds .first-email, .detail-email .first-email').on('focusout', function() {
    	if($('#isEmailValidationEnabled').val() == "true"){
		var $this = $(this),
			$emailID = $this.val(),
			errormsg;
				setTimeout(function(){ 
					if($('.email-fileds .form-row').hasClass('validation-success') == true){
						progress.show();
						$.ajax({
							  url: Urls.emailAddressVerify + "?emailId=" + $emailID,
							  type: 'POST',
							  success: function (response) {
								var jsonResponse = response.dataObj,
									responseCode = jsonResponse.responseCode,
									responseMessage = jsonResponse.responseMessag,
									isValidCatchAllowed = response.isValidCatchAllowed;
								if(responseCode == "Valid"){
									$('.email-fileds').addClass('valid-email');
									$('.email-fileds').removeClass('invalid-email');
									$('.invalid-email-Message').remove();
									/*if($(".opc").length > 0){
							    		$('.delivery-address .xlt-continueShopping').css( "pointer-events", "auto" );
							    	}else{
							    		$('.reduced-register-container #onbeforeunload-button').css( "pointer-events", "auto" );
							    		$('.reduced-register-container #onbeforeunload-button').trigger('click');
							    	}*/		
									//responseMessage = jsonResponse.responseMessage;
									//errormsg = '<div class="error-msg valid-mail-Message" style="display:block;color:#13770D;">' + responseMessage + '</div>';
									//$(errormsg).insertAfter('.valid-email .field .focus-helper');
								}else if(responseCode == "Valid_CatchAll"){
									if (isValidCatchAllowed) {
										$('.email-fileds').addClass('valid-email');
										$('.email-fileds').removeClass('invalid-email');
										$('.invalid-email-Message').remove();
										if($(".opc").length > 0){
								    		$('.delivery-address .xlt-continueShopping').css( "pointer-events", "auto" );
								    	}else{
								    		$('.reduced-register-container #onbeforeunload-button').css( "pointer-events", "auto" );
								    		$('.reduced-register-container #onbeforeunload-button').trigger('click');
								    	}
									} else {
										responseMessage = jsonResponse.responseMessage;
										$('.email-fileds').addClass('invalid-email');
										$('.email-fileds').removeClass('valid-email');
										//$('.valid-mail-Message').remove();
										errormsg = '<div class="error-msg invalid-email-Message" style="display:block;">Please enter a valid Email Address.</div>';
										if($('.invalid-email-Message').length == 0){
											$(errormsg).insertAfter('.invalid-email .field .focus-helper');
										}
									}
								}
								else if(responseCode == "Invalid"){
									responseMessage = jsonResponse.responseMessage;
									$('.email-fileds').addClass('invalid-email');
									$('.email-fileds').removeClass('valid-email');
									//$('.valid-mail-Message').remove();
									errormsg = '<div class="error-msg invalid-email-Message" style="display:block;">Looks like you have entered an Invalid Email Address. Please <span> enter a valid Email Address or contact <a href="https://www.tjc.co.uk/contactus">Customer Care</a></div>';
									if($('.invalid-email-Message').length == 0){
										$(errormsg).insertAfter('.invalid-email .field .focus-helper');
									}
								}
								else{
									$('.email-fileds').addClass('invalid-email');
									$('.email-fileds').removeClass('valid-email');
									//$('.valid-mail-Message').remove();
									errormsg = '<div class="error-msg invalid-email-Message" style="display:block;">Oops, system is unable to validate your Email Address<span>Please contact <a href="https://www.tjc.co.uk/contactus">Customer Care.</a></div>';
									if($('.invalid-email-Message').length == 0){
										$(errormsg).insertAfter('.invalid-email .field .focus-helper');
									}
									progress.hide();
								}
								progress.hide();
				              },
				              error: function(f) {
				                  console.log(f.statusText + " " + f.status);
				                  $('.email-fileds').addClass('invalid-email');
				                  $('.email-fileds').removeClass('valid-email');
									//$('.valid-mail-Message').remove();
									errormsg = '<div class="error-msg invalid-email-Message" style="display:block;">Oops, system is unable to validate your Email Address<span>Please contact <a href="https://www.tjc.co.uk/contactus">Customer Care.</a></div>';
									if($('.invalid-email-Message').length == 0){
										$(errormsg).insertAfter('.invalid-email .field .focus-helper');
									}
									progress.hide();
				              }
						});
					}else{
						invalidEmail();
					}
				}, 30);								
    	}
    });
    
    $('.email-fileds .first-email, .detail-email .first-email').on('focusin', function() {
    	if($('#isEmailValidationEnabled').val() == "true"){
	    	invalidEmail();
	    	progress.hide();
	    	/*if($(".opc").length > 0){
	    		$('.delivery-address .xlt-continueShopping').css( "pointer-events", "none" );
	    	}else{
	    		$('.reduced-register-container #onbeforeunload-button').css( "pointer-events", "none" );
	    	}*/
    	}
    });
    
function invalidEmail(){
	$('.email-fileds').removeClass('valid-email');
	$('.email-fileds').removeClass('invalid-email');
	$('.invalid-email-Message').remove();
	//$('.valid-mail-Message').remove();
}

$('.reduced-register-container #onbeforeunload-button, .delivery-address .xlt-continueShopping').off('click').on('click', function(e){
	if($('.email-fileds').hasClass('invalid-email')){
		return false;
	}else{
		return;
	}
});
/*End Loqate Email Validation*/

$('.post-code-field .postcode-loqate').on('keyup', function () {
	var $thisForm = $(this).closest('.xlt-addressForm');
	if ($(this).val().length >= 3) {
		fetchAddress($(this), $(this).val());
		if($('.opcAddressListModelPopup').length > 0 && $thisForm.find('.additional-address-fields').hasClass('visually-hidden')) {
			$thisForm.addClass('not-scroll');
		}
	}else{
		$(this).siblings('.loqate-autocomplete-list').empty();
		if($('.opcAddressListModelPopup').length > 0) {
			$thisForm.removeClass('not-scroll');
		}
	}
	
	if ($(this).val().length > 0) {
		if ($(this).siblings('.postal-clear').length == 0) {
			$(this).closest('.field').append('<span class="postal-clear">CLEAR</span>');
		}
		$('.postal-clear').on('click', function() {
			$(this).closest('.field').find('input').val('').focus();
			$(this).closest('.field').find('ul.loqate-autocomplete-list').empty();
			$(this).remove();
		});
	} else {
		$(this).closest('.field').find('.postal-clear').remove();
	}
});    
    
function fetchAddress($el, $postCode){
	var $form = $el.closest('.xlt-addressForm'),
		$additionalAddresses = $form.find('.additional-address-fields');
	try {
		$.ajax({
			  url: Urls.loqateAddressFinder + "?userText=" + $postCode,
			  type: 'POST',
	          success: function (response) {
				var i,addresslist, jsonObj = JSON.parse(response.dataObj);
				if(response.serviceStatus){
					$form.find('.type-own-address').remove();
					var $ulList = $el.siblings('.loqate-autocomplete-list');
					$ulList.empty();
					$ulList.prepend('<li class="firsttext">Keep typing your address to display more results</li>');
			        for (i = 0; i < jsonObj.length; i++) {
			        	addresslist ='<li class="loqate-autocomplete-item getthe'+ jsonObj[i].Type +'" data-loqateid='+ jsonObj[i].Id +' data-loqateType='+ jsonObj[i].Type +'><div><span id="firstline">' + jsonObj[i].Text + "," + "</span>" + "<span id='secondline'>" + jsonObj[i].Description + '</span></div></li>'; 			        	
			            $ulList.append(addresslist);
					}
				}else{
					if($form.find('.type-own-address').length == 0){
						$('<div class="type-own-address"> We could not find your address, please check your postcode or enter address below: </div>').insertBefore($additionalAddresses);
					}					
					$additionalAddresses.removeClass('visually-hidden');					
				}
	          }
		});
			
	} catch (b) {
		console.log("fetchAddress__" + b);
    }
}

$( document ).ajaxComplete(function( event, request, settings ) {
	
	$('#loqate-autocomplete-list li.getthePostcode, #loqate-autocomplete-list li.gettheStreet, #loqate-autocomplete-list li.gettheCommercial, #loqate-autocomplete-list li.gettheLocality, #loqate-autocomplete-list li.gettheBuildingName, #loqate-autocomplete-list li.gettheBuildingNumber').off('click').on('click', function(e){
		var addressId = $(this).attr("data-loqateid"), addresslist;
		var $thisList = $(this);
		var $ulList = $(this).closest('.loqate-autocomplete-list');
		try {
			$.ajax({
				url: Urls.accurateAddress + "?addressId=" + addressId,
				type: 'POST',
				success: function (response) {
					var i, jsonObj = JSON.parse(response.dataObj);
					$ulList.find('li.accurate-address').remove();
					$ulList.find('li.loqate-autocomplete-item').hide();
					$ulList.find('li.firsttext').remove();
					$ulList.find('.selected-postal').remove();
					$ulList.prepend('<li class="selected-postal"> <span>' + $thisList.find('#firstline').text().replace(/,/g, '') + '</span> </li>');
					for (i = 0; i < jsonObj.length; i++) {
						addresslist ='<li class="accurate-address" data-loqateid='+ jsonObj[i].Id +' data-loqateType='+ jsonObj[i].Type +'><span id="firstline">' + jsonObj[i].Text + "," + "</span>" + "<span id='secondline'>" + jsonObj[i].Description + '</span></li>'; 			        	
					    $ulList.append(addresslist);
					}
					
				    $('#loqate-autocomplete-list li.selected-postal').on('click', function() {
				    	$ulList.find('li.loqate-autocomplete-item').show();
				    	$ulList.find('li.accurate-address').remove();
				    	$ulList.find('li.firsttext').remove(); 
				    	$ulList.prepend('<li class="firsttext">Keep typing your address to display more results</li>');
				    	$(this).remove();
				    });	        
				}
			});
				
		} catch (b) {
			console.log("Accurate Address__" + b);
	    }
	});
	
	$('#loqate-autocomplete-list li.accurate-address, #loqate-autocomplete-list li.gettheAddress').off('click').on('click', function(e){
		var $form = $(this).closest('.xlt-addressForm'),
			$addressSelect = $form.find('.addresses-select'),
			selectedAddressId = $(this).attr("data-loqateid"),
			jsonObj, address2;
		
		try {
			$.ajax({
				  url: Urls.addressVerify + "?selectedAddressId=" + selectedAddressId,
				  type: 'POST',
		          success: function (response) {
					jsonObj = JSON.parse(response.dataObj);
					/*address2 = [jsonObj[0].Line1,jsonObj[0].Line2,jsonObj[0].Line3,jsonObj[0].Line4,jsonObj[0].City];
					address2 = address2.filter(function (el) { return el != ''; });
					$addressSelect.find("input[id$='address_addresses']").val(address2.toString());*/
					$form.find('.postcode-loqate').val('');
					$form.find('.postal-clear').remove();
					$form.find('.post-code-field label').removeClass('focus-active');
					$form.find("input[id$='address_postCode']").val(jsonObj[0].PostalCode);
					$addressSelect.find("[id$='summary_address_line1']").text(jsonObj[0].Line1);
					$addressSelect.find("[id$='summary_address_line2']").text(jsonObj[0].Line2);
					$addressSelect.find("[id$='summary_address_line3']").text(jsonObj[0].Line3);
					$addressSelect.find("[id$='summary_address_line4']").text(jsonObj[0].Line4);
					$addressSelect.find("[id$='summary_address_city']").text(jsonObj[0].City);
					$addressSelect.find("[id$='summary_address_postalcode']").text(jsonObj[0].PostalCode);
					$addressSelect.find('.new-label').addClass("focus-active");
					$form.find("input[id$='address_address1']").val(jsonObj[0].Line1);
					$form.find("input[id$='address_address2']").val(jsonObj[0].Line2);
					$form.find("input[id$='address_address3']").val(jsonObj[0].Line3);
					$form.find("input[id$='address_address4']").val(jsonObj[0].Line4);
					$form.find("input[id$='address_city']").val(jsonObj[0].City);
					$form.find('#loqate-autocomplete-list').empty();
					$form.find('.addresses-select').addClass('loqate-select');
					$form.find('.addresses-select-group h5').removeClass('visually-hidden');
					$form.find('.addresses-select-group .selected-address-summary').removeClass('visually-hidden');
					$addressSelect.removeClass('visually-hidden');
					$addressSelect.show();        
					$form.find('.addOPCdeliverylocation').prop( "disabled", false );
					$form.find('.editOPCdeliverylocation').prop( "disabled", false );
					if($('.opcAddressListModelPopup').length > 0) {
						$form.removeClass('not-scroll');
					}
					$form.find('.additional-address-fields').removeClass('visually-un-hidden');
					$form.find('.additional-address-fields').addClass('visually-hidden');
					addressFields.updateBillingAddress();
		          }
			});
		} catch (b) {
			console.log("Verify Location" + b);
	    }
	    
    });
    
});

}

function accountAddressSelect() {
    var $buttonBillingAddress = $('.checkout-billing-address .address-check-btn > button'),
        $buttonShippingAddress = $('.checkout-shipping-address .address-check-btn > button'),
        $buttonBillingAddressZipInput = $buttonBillingAddress.parent().siblings('.field').children('input'),
        $buttonShippingAddressZipInput = $buttonShippingAddress.parent().siblings('.field').children('input'),
        $addressSelect     = $('.addresses-select'),
        $addressSelectRow  = $addressSelect.children().first(),
        $typeAddressButton = $addressSelect.children().last().find('button'),
        $additionaFields = $('.additional-address-fields'),
        $isOPCPage = $('.checkout-addresses-form').find('input[name$=isOPCPage]');

    if ($buttonBillingAddressZipInput.val() === '') {
        $buttonBillingAddress.prop('disabled', true);
    }
    if ($buttonShippingAddressZipInput.val() === '') {
        $buttonShippingAddress.prop('disabled', true);
    }

    $buttonBillingAddressZipInput.on('change keydown keyup', function () {
        if ($buttonBillingAddressZipInput.val() === '') {
            $buttonBillingAddress.prop('disabled', true);
        } else {
            $buttonBillingAddress.prop('disabled', false);
        }
    });

    $('body').on('change keydown keyup', $buttonShippingAddressZipInput, function () {
        if ($buttonShippingAddressZipInput.val() === '') {
            $buttonShippingAddress.prop('disabled', true);
        } else {
            $buttonShippingAddress.prop('disabled', false);
        }
    });

    $buttonBillingAddressZipInput.on('click', function () {
        $addressSelect.removeClass('visually-hidden');
        if($('.checkout-billing-address .addresses-select .button-container.type-address-btn button').length < 1){
        	$typeAddressButton.appendTo('.type-address-btn');
        }
    });
    $buttonShippingAddressZipInput.on('click', function () {
        $addressSelect.removeClass('visually-hidden');
        if($('.checkout-shipping-address .addresses-select .button-container.type-address-btn button').length < 1){
        	$typeAddressButton.appendTo('.type-address-btn');
        }
        if($('.checkout-shipping-address .addresses-select .button-container button').length > 0){
        	$('.checkout-shipping-address .addresses-select .form-row-button.type-address-btn').css({'display':'none'});
        }
    });

    $typeAddressButton.on('click', function () {
        //$addressSelect.addClass('visually-hidden');
        if(!$('#wrapper').hasClass('pt_checkout')) {
        	$additionaFields.removeClass('visually-hidden');
        }
    });

    $addressSelectRow.addClass('form-row-with-button');
    $addressSelectRow.append('<div class="button-container type-address-btn"></div>');
    
    //Gift Receipt Functionality
    $("div[id$='_shippingAddress_isGift']").on('click' , function(){
    	if ($("div[id$='_shippingAddress_isGift'] span").hasClass('checked')){
    		if($isOPCPage.val() != "true"){
	    		$('.gift-recipient-name').show();
	    		$('.gift-message-text').show();
    		}
    		$('.gift-content').show();
    		$('.gift-content .gift-recipient-name').removeClass('validation-error');
    		$('.gift-recipient-name').find('input').addClass('required');
    		$("div[id$='_shippingAddress_addToAddressBook']").closest('.form-row-checkbox').hide();
    		if ($("div[id$='_shippingAddress_addToAddressBook'] span").hasClass('checked')){
    			giftReceiptSelected = true;
    			$("div[id$='_shippingAddress_addToAddressBook'] span").removeClass('checked');
    			$("div[id$='_shippingAddress_addToAddressBook'] input").val(false);
    		} 
		} else {
			if($isOPCPage.val() != "true"){
				$('.gift-recipient-name').hide();
				$('.gift-message-text').hide();
			}
			$('.gift-content').hide();
			$('.gift-content .gift-recipient-name').removeClass('validation-error');
			$('.gift-recipient-name').find('input').removeClass('required');
			$("div[id$='_shippingAddress_addToAddressBook']").closest('.form-row-checkbox').show();
			if (giftReceiptSelected){
				$("div[id$='_shippingAddress_addToAddressBook'] span").addClass('checked')
			}
			$("div[id$='_shippingAddress_addToAddressBook'] input").val(true);
		}
    });
    
    updateGiftReceiptForOrder();

}

function updateGiftReceiptForOrder(){
	var $isOPCPage = $('.checkout-addresses-form').find('input[name$=isOPCPage]');
	if ($("div[id$='_shippingAddress_isGift'] span").hasClass('checked')){
		if($isOPCPage.val() != "true"){
			$('.gift-recipient-name').show();
			$('.gift-message-text').show();
		}
		$('.gift-content').show();
		$('.gift-recipient-name').find('input').addClass('required');
		$("div[id$='_shippingAddress_addToAddressBook']").closest('.form-row-checkbox').hide();
		if ($("div[id$='_shippingAddress_addToAddressBook'] span").hasClass('checked')){
			giftReceiptSelected = true;
			$("div[id$='_shippingAddress_addToAddressBook'] span").removeClass('checked');
			$("div[id$='_shippingAddress_addToAddressBook'] input").val(false);
		} 
	} else {
		if($isOPCPage.val() != "true"){
			$('.gift-recipient-name').hide();
			$('.gift-message-text').hide();
		}
		$('.gift-content').hide();
		$('.gift-recipient-name').find('input').removeClass('required');
		$("div[id$='_shippingAddress_addToAddressBook']").closest('.form-row-checkbox').show();
		if (giftReceiptSelected){
			$("div[id$='_shippingAddress_addToAddressBook'] span").addClass('checked');
			$("div[id$='_shippingAddress_addToAddressBook'] input").val(true);
		}
	}
}

address = {
    init: function () {
        initializeEvents();
        accountAddressSelect();
        account.initSelectElements();
    }
};

module.exports = address;