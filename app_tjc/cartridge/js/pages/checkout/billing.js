'use strict';

var informMeWhenAvailable = require('../../informmewhenavailable'),
    gtmDataLayer = require('../../gtm-datalayer');

var $paymentMethodsWarning = $('.payment-methods-warning'),
    $paymentMethod = $('input[name=dwfrm_billing_paymentMethods_selectedPaymentMethodID]'),
    $lastSelectedPaymentMethod,
    $formSubmitOrder = $('.checkout-billing'),
    $buttonSubmitOrder = $('.paynow'),
    $descriptionContainers = $('.payment-method-description'),
    $description,
    $tjcCredit = $('.credit-input'),
    $tjcCreditAvailable = $('.credit-available'),
    $tjcCreditApplyButton = $('.credit-apply-button');

function showPaymentMethodDescription(selectedPaymentMethod) {
    $description = $descriptionContainers.filter('.' + selectedPaymentMethod.val().toLowerCase());
    $description.addClass('active');

    if (selectedPaymentMethod.val().toLowerCase() === 'budgetpay') {
        $('.order-totals-budgetpay').show();
        $('.order-totals-budgetpay-heading').show();
        $('.order-totals-heading').hide();
    } else {
        $('.order-totals-budgetpay').hide();
        $('.order-totals-budgetpay-heading').hide();
        $('.order-totals-heading').show();
    }

    if ($description && $description.length > 0 && $.trim($description.html()).length > 0) {
        $description.slideDown(0);
        $description = null;
    }
    $lastSelectedPaymentMethod = selectedPaymentMethod;
}

function isDecimal(number) {
    return new RegExp(/^[-+]?[0-9]+\.[0-9]+$/).test(number) === true;
}

function initializeEvents() {
    $formSubmitOrder.on('submit', function () {
        if ($paymentMethod.filter(':checked').length === 1) {
            $buttonSubmitOrder.addClass('disabled');
            return true;
        } else if ($paymentMethod.filter(':checked').length > 1) {
            $paymentMethod = $paymentMethod.not('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_TJCCredit');
            $buttonSubmitOrder.addClass('disabled');
            return true;
        }
        $paymentMethodsWarning.slideDown(0);
        return false;
    });
    $paymentMethod.on('change', function () {
        $paymentMethodsWarning.slideUp(0);
        if ($lastSelectedPaymentMethod && $lastSelectedPaymentMethod.val() !== $(this).val()) {
            if ($descriptionContainers.filter('.active').length > 0) {
                $descriptionContainers.filter('.active').removeClass('active').slideUp(0, function () {
                    showPaymentMethodDescription($(this));
                }.bind(this));
            } else {
                showPaymentMethodDescription($(this));
            }


        } else if (!$lastSelectedPaymentMethod) {
            showPaymentMethodDescription($(this));
        }
    });
    $tjcCredit.on('keyup', function () {
        if (isNaN($(this).val()) === false || isDecimal($(this).val()) === true) {
            $tjcCreditApplyButton.show();
            var subtotal = $tjcCredit.data('subtotal'),
                newCreditBalance = subtotal - $(this).val();
            if ($(this).val() < 0) {
                return;
            }
            if (newCreditBalance <= 0) {
                $tjcCreditAvailable.parent().hide();
                $(this).val(subtotal);
                $tjcCreditApplyButton.html($tjcCredit.data('paynowlabel'));
            } else {
                $tjcCreditAvailable.parent().show();
                $tjcCreditAvailable.html('£ ' + newCreditBalance.toFixed(2));
                $tjcCreditApplyButton.html($tjcCredit.data('applylabel'));
            }
        } else {
            $tjcCreditApplyButton.hide();
        }
    });

    informMeWhenAvailable.init();
}

// for each checkout step, we need to send gtm data
function sendGTMCheckoutData() {
    try {
        // we store it in a placeholder div in gtm_checkout.isml
        var gtmJSON = gtmDataLayer.getGTMData();
        gtmDataLayer.pushCheckoutStepToDataLayer(gtmJSON);
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
    }
}

/**
 * @function
 * @description loads Payment methods
 */
exports.init = function () {
    initializeEvents();
    sendGTMCheckoutData();
};
