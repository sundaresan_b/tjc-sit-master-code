'use strict';
var dialog = require('../dialog');
var ajax = require('../ajax');

exports.init = function () {  
    $('.end-membership').on('click', function(e){
    	e.preventDefault();
		$('.tjc-ended-completed').dialog({ 
			modal: true,
			dialogClass: "tjc-ended-completed-dialog"
		}); 
               
    });
    
    $('.end-membership-confirm').on('click', function(e){
    	e.preventDefault();
    	$.ajax({
            url: Urls.confirmCancellation,
            type: 'GET', 
            success: function (response) {
            	$('.cancelconfirm').html(response.message);
            	$('.cancelconfirm').removeClass('d-none');
            	 
            	$('.tjc-ended-completed-dialog .ui-icon-closethick').trigger('click');
        		$(".cancelconfirm").dialog({
        			modal: true,
        			closeOnEscape: false,
        			dialogClass: "cancelconfirm",
        			buttons: [
        			    {
          			      text: "OK",
          			      click: function() {
          			    	window.location.href = Urls.myTJCPlus;
          			      }
          			    }
        			 ]
        		});
            },
            error: function(xhr, error){
            	alert('in error');
            	console.log(error);
            }
    	});
    });
    
};
function tjcplusbuttondisable(){
	if ($('.plusshippingaddressgroup .post-code-field').hasClass('validation-error') || $('.plusbillingaddressgroup .post-code-field').hasClass('validation-error') || $('#dwfrm_quickbuyaddress_shippingAddress_address_city').closest('.form-row').hasClass('validation-error')){		
		$('.activate-tjcplus-membership').attr('disabled','disabled');	
	} 
	else {
		$('.activate-tjcplus-membership').removeAttr('disabled','disabled');
	}
}
setInterval(tjcplusbuttondisable, 1000);

$(document).on("click",".logo-container",function() {
	if($('.pt_product-details').hasClass('pdp_redesign')){
		return;
	}
	$(".plus-subscription-class").toggleClass("active");
});

$("input:radio[name=subscription-product]").on('click', function(){
	$('.radio-sub').removeClass('selected');
	$(this).closest(".radio-sub").addClass('selected');
});


