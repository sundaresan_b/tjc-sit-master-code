'use strict';

function calculateLIsInRow() {
    var lisInRow = 0,
        $presenters = $('.presenters-list .presenter');
    $presenters.each(function () {
        var current = $(this),
            currentIndex = current.index('.presenter');
        if (currentIndex > 0) {
            if (current.position().top !== $presenters.eq(currentIndex - 1).position().top) {
                return false;
            }
            lisInRow++;
        } else {
            lisInRow++;
        }
    });
    return lisInRow;
}

exports.init = function () {
    var $presentersList = $('.presenters-list'),
        presenterPerLine,
        $activeBeforeResize,
        resizedFinished;

    if ($presentersList) {
        presenterPerLine = calculateLIsInRow();
        $presentersList.on('click', '.presenter-image', function () {
            var $currentPresenter = $presentersList.find('.presenter.active'),
                $currentPresenterDesc = $presentersList.find('.presenter-desc.active').insertAfter($currentPresenter),
                $clickedPresenter = $(this).parents('.presenter'),
                $clickedPresenterDesc = $clickedPresenter.next('.presenter-desc'),
                insertAfterPos,
                $insertAfterElement;
            if (!$clickedPresenter.hasClass('active')) {
                insertAfterPos = Math.ceil(($clickedPresenter.index('.presenter') + 1) / presenterPerLine) *
                    presenterPerLine - 1;
                $insertAfterElement = $presentersList.find('.presenter:eq(' + insertAfterPos + ')');
                $clickedPresenterDesc.insertAfter($insertAfterElement.length !== 0 ? $insertAfterElement :
                        $presentersList.find('.presenter').last());
                $clickedPresenterDesc.slideDown({
                    complete: function () {
                        $clickedPresenter.add($clickedPresenterDesc).addClass('active');
                    }
                });
            }
            $currentPresenterDesc.hide();
            $currentPresenter.add($currentPresenterDesc).removeClass('active');
        });
        $(window).resize(function () {
            var $currentPresenter = $presentersList.find('.presenter.active'),
                $currentPresenterDesc = $presentersList.find('.presenter-desc.active').insertAfter($currentPresenter);
            clearTimeout(resizedFinished);
            if ($currentPresenter.length > 0) {
                $activeBeforeResize = $currentPresenter;
                $currentPresenter.add($currentPresenterDesc).removeClass('active');
            }
            resizedFinished = setTimeout(function () {
                presenterPerLine = calculateLIsInRow();
                if ($activeBeforeResize) {
                    $activeBeforeResize.find('.presenter-image').click();
                    $activeBeforeResize = null;
                }
            }, 250);
        });
    }
};
