'use strict';

var productTile = require('../product-tile'),
    progress = require('../progress'),
    util = require('../util'),
    risingAuctions = require('../risingauctions'),
    newsignin = require('../newsignin'),
    gtmDatalayer = require('../gtm-datalayer'),
    variant = require('./product/variant');

function infiniteScroll() {
    // getting the hidden div, which is the placeholder for the next page
    var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]'),
        // get url hidden in DOM
        gridUrl = loadingPlaceHolder.attr('data-grid-url'),
        useSessionStorage = false,
        fillEndlessScrollChunk;

    if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
        // switch state to 'loading'
        // - switches state, so the above selector is only matching once
        // - shows loading indicator
        loadingPlaceHolder.attr('data-loading-state', 'loading');
        loadingPlaceHolder.addClass('infinite-scroll-loading');

        /**
         * named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
         */
        fillEndlessScrollChunk = function (html) {
            loadingPlaceHolder.removeClass('infinite-scroll-loading');
            loadingPlaceHolder.attr('data-loading-state', 'loaded');
            $('div.search-result-content').append(html);
        };

        // old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
        // it was removed to temporarily address RAP-2649
        if (useSessionStorage) {
            // if we hit the cache
            fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
        } else {
            // else do query via ajax
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: gridUrl,
                success: function (response) {
                    // put response into cache
                    try {
                        sessionStorage['scroll-cache_' + gridUrl] = response;
                    } catch (ignore) {
                        // nothing to catch in case of out of memory of session storage
                        // it will fall back to load via ajax
                    }
                    // update UI
                    fillEndlessScrollChunk(response);
                    productTile.init();
                }
            });
        }
    }
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the
 *              product listing
 */
function updateProductListing() {
    var hash = location.href.split('#')[1],
        refineUrl;
    if (hash === 'results-content' || hash === 'results-products') {
        return;
    }

    if (hash.length > 0) {
        refineUrl = window.location.pathname + '?' + hash;
    } else {
        return;
    }
    progress.show($('.search-result-content'));
    $('#main').load(util.appendParamToURL(refineUrl, 'format', 'ajax'), function () {
        productTile.init();
        progress.hide();
        require('../uniform')();
        util.addToggleEvents($('#main'));
        risingAuctions.reInitTimeCounters();
        util.imageAdjust();
        setTimeout(function(){
        	util.imageAdjust();
		},5000);
    });
}
/** Sticky Secondary Section(Refinement)**/
function fixMe(id) {
    var e = $(id);
    var lastScrollTop = 0;
    var firstOffset = 0;
	var elTop = e.offset().top;
    var lastA = e.offset().top;
    $(window).scroll(function(event){
        var a = e.offset().top;
        var b = e.height();
        var c = $(window).height();
        var d = $(window).scrollTop();
		var f = $('#primary').height();
		var g = $('#primary').offset().top;
		var h = e.position().top;
		var k = $(window).width();
		var l = $('#primary').position().top;
		var m = $('#primary').outerHeight();
		var n = $('.main-wrapper').outerHeight();
		var o = e.outerHeight();
		if (d > (elTop - firstOffset) && f > b &&  k > 1279) {
			$('.main-wrapper').css('position', 'relative');
			if (d > lastScrollTop) { // scroll down
				if (a + o + 1 >= g + m) { // when secondary touches primary bottom
					e.css({position: "absolute", bottom: n - (l + m), top:"auto"});
				} else {
					if (b > c) { //filter bigger than window
						if (e.css("position") != "fixed" && c + d >= a + b) {
							e.css({position: "fixed", bottom: 0, top: "auto"});
						}
						if (e.css("position") == "fixed" && h == firstOffset) {
							e.css({padding: "10px 0 0 0", position: "absolute", bottom: "auto", top: lastA});
						}
					}
					if (b < c) { //filter smaller than window
						if (e.css("position") != "fixed" && c + d >= a + b) {
							e.css({padding: "10px 0 0 0", position: "fixed", bottom: "auto", top: firstOffset});
						}
					}
				}
			} else { // scroll up
				if (a - d >= firstOffset) {
					if (e.css("position") != "fixed") {
						e.css({padding: "10px 0 0 0", position: "fixed", bottom: "auto", top: firstOffset});
					}
				} else {
					if (e.css("position") != "absolute") {
						e.css({padding: "10px 0 0 0", position: "absolute", bottom: "auto", top: lastA});
					}               
				}
			}
		} else if(d < (elTop - firstOffset)) {
			e.removeAttr('style');
		}
		lastScrollTop = d;
		lastA = a;
		
		$(document).on('click', '#secondary .toggle', function() {
			if ($(window).scrollTop() > ($('#primary').position().top + $('#primary').height() - $(window).height())) {
				e.css({position: "absolute", bottom: n - (l + m), top:"auto"});
			}
		});

    });
}
/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
    var $main = $('#main');

    // handle events for updating grid
    $main.on('touchend', '.refinements a, .pagination a, .breadcrumb-refinement-value a', function (event) {
        $(this).click();
        event.preventDefault();
    });

    $main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a', function (event) {
        event.preventDefault();

        if ($(this).parent().hasClass('unselectable') || $(this).hasClass('no-default-handler')) {
            return undefined;
        }
        var catparent = $(this).parents('.category-refinement'),
            folderparent = $(this).parents('.folder-refinement');
        //if the anchor tag is underneath a div with the class names & , prevent the double encoding of the url
        //else handle the encoding for the url
        if (catparent.length > 0 || folderparent.length > 0) {
            return true;
        }

        window.location.href = this.href;

        // We don't use this approach at the moment, as the PLP is not reloaded via ajax.
        /* if (window.location.href.indexOf('?') >= 0) {
            locationURL = window.location.href.split('?')[0];
        } else {
            locationURL = window.location.href.split('#')[0];
        }
        linkURL = $(this).attr('href').split('?')[0];

        query = util.getQueryString(this.href);
        if (locationURL !== linkURL || query === undefined || query.length <= 1) {
            window.location.href = this.href;
        } else {
            window.location.hash = query;
        }

        return false; */
    });

    // handle events item click. append params.
    $main.on('click', '.product-tile a:not(".quickview"):not(".wishlist")', function () {
        var a = $(this),
            // get current page refinement values
            wl = window.location,

            qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {},
            hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {},

            // merge hash params with querystring params
            params = $.extend(hashParams, qsParams),
            tile,
            idx;
        if (!params.start) {
            params.start = 0;
        }
        // get the index of the selected item and save as start parameter
        tile = a.closest('.product-tile');
        idx = tile.data('idx') ? +tile.data('idx') : 0;

        // convert params.start to integer and add index
        params.start = (+params.start) + (idx + 1);
        //dont override previous other hash params
        params = $.extend(util.getQueryStringParams(a[0].hash.substr(1)), params);
        // set the hash and allow normal action to continue
        a[0].hash = $.param(params);
    });

    // handle sorting change
    $main.on('change', '.sort-by select', function () {
        var refineUrl = $(this).find('option:selected').val();

        window.location.href = refineUrl;

        // We don't use this approach at the moment, as the PLP is not reloaded via ajax.
        /* for (i = 0; i < searchParam.length; i++) {
            paramName = searchParam[i].split('=')[0];
            if (queryString.indexOf(paramName) < 0) {
                queryString += '&' + searchParam[i];
            }
        }
        window.location.hash = queryString;
        return false;*/
    }).on('change', '.items-per-page select', function () {
        var refineUrl = $(this).find('option:selected').val();

        window.location.href = refineUrl;

        /* if (refineUrl === 'INFINITE_SCROLL') {
            $('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
        } else {
            $('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
            window.location.hash = queryString;
        }
        return false; */
    });

        /* .on('click', '.pagination.bottom a', function () {
            util.scrollBrowser($('.breadcrumb').offset().top);
        }).on('change', '#grid-sort-footer, #grid-paging-footer', function () {
            util.scrollBrowser($('.breadcrumb').offset().top);
        }); */

    // handle hash change
    window.onhashchange = updateProductListing;

    // trigger hash change event after page load if necessary
    if (window.location.hash) {
        $(window).trigger('hashchange');
    }
        
    // Sticky Secondary
	fixMe("#secondary");
}


exports.init = function () {
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $(window).on('scroll', infiniteScroll);
    }
    productTile.init();
    initializeEvents();
    gtmDatalayer.pushDataToDataLayerForPLP();
    // Removed the script to remove the persistent login in PLP page by adding/removing the class for wishlist icon
    /*if(customer.loggedIn == true && customer.loginStatusCode == "1"){
    	$('.tile-wishlist .product-wishlist-link').removeClass('login-wish-class login-class');
    } else {
    	$('.tile-wishlist .product-wishlist-link').addClass('login-wish-class login-class');
    }*/
    newsignin.init();
    variant.wishListAJAXEvent();
};
