'use strict';

var address = require('./address'),
    billing = require('./billing'),
    tabs = require('../../tabsaccount');

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
exports.init = function () {
    if ($('.checkout-summary')) {
        billing.init();
    }

    if ($('.checkout-addresses-form')) {
        address.init();
    }

    //if on the order review page and there are products that are not available diable the submit order button
    if ($('.order-summary-footer').length > 0) {
        if ($('.notavailable').length > 0) {
            $('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
        }
    }

    tabs.init();

};
