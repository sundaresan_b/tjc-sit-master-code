'use strict';

var addressFields = require('../../address-fields');
var account = require('../account');

var address;

function addShowAddressFieldsEvent($radioGroup) {
    $radioGroup.on('change', function () {
        var toggleFlag = $radioGroup.filter(':checked').hasClass('alternative-address');
        $radioGroup.parents('fieldset').find('.alternative-address-fields').toggleClass('visually-hidden', !toggleFlag);
    });
}

function initializeEvents() {
    var $quickbuyAddressesForm = $('.checkout-addresses-form'),
        $submitButton = $checkoutAddressesForm.find('.submit-button'),
        $billingAddressRadioGroup = $quickbuyAddressesForm.find('input:radio[name=billing-address]'),
        $shippingAddressRadioGroup = $quickbuyAddressesForm.find('input:radio[name=shipping-address]');
    addShowAddressFieldsEvent($billingAddressRadioGroup);
    addShowAddressFieldsEvent($shippingAddressRadioGroup);
    addressFields.init($quickbuyAddressesForm);


    $quickbuyAddressesForm.on('submit', function (e) {
        var $selectedBillingOption = $billingAddressRadioGroup.filter(':checked'),
            $billingAddressFields = $billingAddressRadioGroup.parents('fieldset').find('.alternative-address-fields'),
            $additionalBillingAddressFields = $billingAddressFields.find('.additional-address-fields'),
            $selectedShippingOption = $shippingAddressRadioGroup.filter(':checked'),
            $shippingAddressFields = $shippingAddressRadioGroup.parents('fieldset').find('.alternative-address-fields'),
            $additionalShippingAddressFields = $shippingAddressFields.find('.additional-address-fields'),
            isValid = $checkoutAddressesForm.valid(),
            $billingAddToAddressBookCheckBox = $billingAddressFields.find('input[name$=addToAddressBook]'),
            $shippingAddToAddressBookCheckBox = $shippingAddressFields.find('input[name$=addToAddressBook]'),
            $billingFindButton = $billingAddressFields.find('.post-code-field button'),
            $shippingFindButton = $shippingAddressFields.find('.post-code-field button'),
            $billingPostCodeInput = $billingAddressFields.find('.post-code-field input'),
            $shippingPostCodeInput = $shippingAddressFields.find('.post-code-field input'),
            selectedBillingOptionData = $selectedBillingOption.data('address'),
            selectedShippingOptionData = $selectedShippingOption.data('address');

        if (!$selectedBillingOption.hasClass('alternative-address')) {
            $billingAddressFields.find('input[name$=addressid]').val(selectedBillingOptionData.ID);
            $billingAddressFields.find('select[name$=country]')
                .val(selectedBillingOptionData.countryCode).click();
            $billingPostCodeInput.val(selectedBillingOptionData.postCode);
            addressFields.fillAddressFields($additionalBillingAddressFields, selectedBillingOptionData);
            if ($billingAddToAddressBookCheckBox.prop('checked')) {
                $billingAddToAddressBookCheckBox.click();
            }
        } else {
            $billingAddressFields.find('input[name$=addressid]').val('dwfrm_alternative-address');
        }
        if ($selectedShippingOption.hasClass('use-billing-address')) {
            $shippingAddressFields.find('input[name$=addressid]').val('dwfrm_use-billing-address');
            $shippingAddressFields.find('select[name$=country]')
                .val($billingAddressFields.find('select[name$=country]').val()).click();
            $shippingPostCodeInput.val($billingAddressFields.find('.post-code-field input').val());
            /*jslint unparam: true */
            $.each(['address1', 'address2', 'address3', 'address4', 'locality', 'city'], function (i, attribute) {
                var value = $additionalBillingAddressFields.find('input[name$=' + attribute + ']').val();
                $additionalShippingAddressFields.find('input[name$=' + attribute + ']').val(value);
            });
            /*jslint unparam: false */
            if ($shippingAddToAddressBookCheckBox.prop('checked')) {
                $shippingAddToAddressBookCheckBox.click();
            }
        } else if (!$selectedShippingOption.hasClass('alternative-address')) {
            $shippingAddressFields.find('input[name$=addressid]').val(selectedShippingOptionData.ID);
            $shippingAddressFields.find('select[name$=country]')
                .val(selectedShippingOptionData.countryCode).click();
            $shippingPostCodeInput.val(selectedShippingOptionData.postCode);
            addressFields.fillAddressFields($additionalShippingAddressFields, selectedShippingOptionData);
            if ($shippingAddToAddressBookCheckBox.prop('checked')) {
                $shippingAddToAddressBookCheckBox.click();
            }
        } else {
            $shippingAddressFields.find('input[name$=addressid]').val('dwfrm_alternative-address');
        }

        if (isValid !== $checkoutAddressesForm.valid()) {
            e.preventDefault();
            $checkoutAddressesForm
                .append($('<input/>', {type: 'hidden', name: $submitButton.attr('name'), value: 'save'}))
                .submit();
        }

        if (!$billingAddressFields.hasClass('visually-hidden') && $billingFindButton.is(':visible') &&
                $billingPostCodeInput.val() !== '' && !$billingFindButton.data('searchstarted')) {
            $billingFindButton.click();
            e.preventDefault();
        }

        if (!$shippingAddressFields.hasClass('visually-hidden') && $shippingFindButton.is(':visible') &&
                $shippingPostCodeInput.val() !== '' && !$shippingFindButton.data('searchstarted')) {
            $shippingFindButton.click();
            e.preventDefault();
        }
    });
}

function accountAddressSelect() {
    var $buttonBillingAddress = $('.checkout-billing-address .address-check-btn > button'),
        $buttonShippingAddress = $('.checkout-shipping-address .address-check-btn > button'),
        $buttonBillingAddressZipInput = $buttonBillingAddress.parent().siblings('.field').children('input'),
        $buttonShippingAddressZipInput = $buttonShippingAddress.parent().siblings('.field').children('input'),
        $addressSelect     = $('.addresses-select'),
        $addressSelectRow  = $addressSelect.children().first(),
        $typeAddressButton = $addressSelect.children().last().find('button').detach(),
        $additionaFields = $('.additional-address-fields');

    $addressSelect.addClass('visually-hidden');
    if ($buttonBillingAddressZipInput.val() === '') {
        $buttonBillingAddress.prop('disabled', true);
    }
    if ($buttonShippingAddressZipInput.val() === '') {
        $buttonShippingAddress.prop('disabled', true);
    }

    $buttonBillingAddressZipInput.on('change keydown keyup', function () {
        if ($buttonBillingAddressZipInput.val() === '') {
            $buttonBillingAddress.prop('disabled', true);
        } else {
            $buttonBillingAddress.prop('disabled', false);
        }
    });

    $buttonShippingAddressZipInput.on('change keydown keyup', function () {
        if ($buttonShippingAddressZipInput.val() === '') {
            $buttonShippingAddress.prop('disabled', true);
        } else {
            $buttonShippingAddress.prop('disabled', false);
        }
    });

    $buttonBillingAddressZipInput.on('click', function () {
        $addressSelect.removeClass('visually-hidden');
        $typeAddressButton.appendTo('.type-address-btn');
    });
    $buttonShippingAddressZipInput.on('click', function () {
        $addressSelect.removeClass('visually-hidden');
        $typeAddressButton.appendTo('.type-address-btn');
    });

    $typeAddressButton.on('click', function () {
        $addressSelect.addClass('visually-hidden');
        $additionaFields.removeClass('visually-hidden');
    });

    $addressSelectRow.addClass('form-row-with-button');
    $addressSelectRow.append('<div class="button-container type-address-btn"></div>');

}


address = {
    init: function () {
        initializeEvents();
        accountAddressSelect();
        account.initSelectElements();
    }
};

module.exports = address;