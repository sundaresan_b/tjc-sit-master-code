'use strict';

var giftcert = require('../giftcert'),
    util = require('../util'),
    page = require('../page'),
    ajax = require('../ajax'),
    progress = require('../progress'),
    dialog = require('../dialog'),
    addressFields = require('../address-fields'),
    tabs = require('../tabsaccount'),
    resetPassword = require('../resetpassword'),
    quickbuy = require('../quickbuy'),
    qbfornonlivetv = require('../qbfornonlivetv'),
    newsignin = require('../newsignin'),
	tjcplusactivation = require('../tjcplusactivation'),
	orderconfirmation = require('./orderconfirmation');
var account;

/**
 * @private
 * @function
 * @description Binds the events of the order history
 */
function initOrderHistory() {
    var $table = $('.order-history-items'),
        $overviewRows = $table.find('.order-overview');
    $table.on('click', 'tr.order-overview', function () {
        var $orderOverviewRow = $(this),
            $orderDetailsContainer = $orderOverviewRow.next('tr').find('.slide-container'),
            rowToggle = function () {
                $orderOverviewRow.toggleClass('active')
                    .find('button i').toggleClass('fa-plus fa-minus');
                $orderOverviewRow.add($orderDetailsContainer).toggleClass('collapsed expanded');
                $orderDetailsContainer.slideToggle();
            };

        if ($orderOverviewRow.hasClass('collapsed')) {
            $overviewRows.filter('.expanded').click();
            if ($orderDetailsContainer.find('.order-summary-table').length === 0) {
                progress.show();
                ajax.load({
                    url: $orderDetailsContainer.data('url'),
                    target: $orderDetailsContainer,
                    callback: function () {
                        progress.hide();
                        rowToggle();
                        starRatingHandlers((typeof(window.TurnTo)!=='undefined' && typeof(window.TurnTo.historyFetch)!=='undefined')?window.TurnTo.historyFetch:false);
                    }
                });
            } else {
                rowToggle();
            }
        } else {
            rowToggle();
        }
    });
    
    starRatingInitializer();
}

function starRatingInitializer(){
	var url = Urls.TurnToGetReviewList;
	url = util.appendParamsToUrl(url, {
		'emailID' : ''+customer.emailID, 
		'pagecontext' : pageContext.type});
	$.ajax({
		url: url,
		type: 'POST',
		success: function (response) {
			if(typeof(response) == 'object'){
				window.TurnTo = response;
				window.TurnTo.historyFetch = true;
				window.TurnTo.reviewedProduct = [];
				
				if($('.cart-row .item-details .customer-give-rating-section:visible').length > 0){
					starRatingHandlers((typeof(window.TurnTo)!=='undefined' && typeof(window.TurnTo.historyFetch)!=='undefined')?window.TurnTo.historyFetch:false);
				}
			}else{
				console.error("TurnTo Review Response Error", url);
				window.TurnTo = {};
				window.TurnTo.historyFetch = false;
				window.TurnTo.reviewedProduct = [];
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.error("TurnTo Review AJAX Error", url, jqXHR, textStatus, errorThrown);
		}
	});
}
function starRatingHandlers(initializeFlag){
	if(!initializeFlag || typeof(initializeFlag) === 'undefined'){
		$('.customer-give-rating-section:visible').remove();
		console.error("TurnTo Review stars are remove due to API respone error");
	}
	var $this, $thisRatingParent, $thisRatingParentTop, thisProductID, splitThisVal, splitedProductsRating, allStars, $selectedElement, loopBreaker = false, itr;
	$('.customer-give-rating-section:visible .customer-give-rating:not(.older)').each(function(index,value){
		$thisRatingParent = $(this);
		$thisRatingParentTop = $thisRatingParent.parents('.customer-give-rating-section');
		allStars = $thisRatingParent.find('.fa').length;
		thisProductID = $thisRatingParent.data("productid");
		loopBreaker = false;
		$(TurnTo.ReviewedProductIDs).each(function(index,value){
			if(value.indexOf(';')){
				splitThisVal = value.split(';');
				if(splitThisVal[0] == thisProductID){
					window.TurnTo.reviewedProduct.push(thisProductID);
					splitedProductsRating = parseInt(splitThisVal[1]);
					$thisRatingParentTop.find('.rating-section-title').hide();
					$thisRatingParentTop.css({'border-color':'#fff','padding':'0'});
					for(itr=0; itr<splitedProductsRating; itr++){
						$selectedElement = $thisRatingParent.find('.fa:eq('+itr+')');
						if($selectedElement.hasClass('fa-star-regular')){
							$selectedElement.removeClass('fa-star-regular').addClass('fa-star');
							$thisRatingParent.addClass('older').css('cursor', 'default');
							loopBreaker = true;
						}
					}
				}
			}
			if(loopBreaker == true){
				return;
			}
		});
	});
	
	$('.customer-give-rating:not(.older) .fa').off('mouseenter.ratinghandler').on('mouseenter.ratinghandler', function(){
		var $this = $(this),
		    $thisRatingParent = $this.parents('.customer-give-rating'),
		    starSelected = parseInt($this.index()),
		    allStars = $thisRatingParent.find('.fa').length,
		    $selectedElement, itr;
		
		for(itr=0; itr<allStars; itr++){
			$selectedElement = $thisRatingParent.find('.fa:eq('+itr+')');
			if(itr <= starSelected){
				if($selectedElement.hasClass('fa-star-regular')){
					$selectedElement.removeClass('fa-star-regular').addClass('fa-star');
				}
			}else{
				if($selectedElement.hasClass('fa-star')){
					$selectedElement.removeClass('fa-star').addClass('fa-star-regular');
				}
			}
		}
	}).off('mouseleave.ratinghandler').on('mouseleave.ratinghandler', function(){
		var $this = $(this),
		    $thisRatingParent = $this.parents('.customer-give-rating'),
		    $thisTDElement = $this.parents('td.item-details'),
		    parentHoverFlag = ($thisTDElement.find('.customer-give-rating:hover').length <= 0);
		
		if(parentHoverFlag){
			$thisRatingParent.find('.fa.fa-star').removeClass('fa-star').addClass('fa-star-regular');
		}
	}).off('click.ratinghandler').on('click.ratinghandler', function(){
		var $this = $(this),
		    $thisRatingParent = $this.parents('.customer-give-rating'),
		    $thisRatingParentTop = $this.parents('.customer-give-rating-section'),
		    starSelected = parseInt($this.index()+1),
		    allStars = $thisRatingParent.find('.fa').length,
		    orderID = $thisRatingParent.data("orderid"),
		    productID = $thisRatingParent.data("productid"),
		    url = Urls.TurnToCreateReview;
		
		$thisRatingParent.css('cursor','default');
		$thisRatingParent.find('.fa').off('mouseenter.ratinghandler').off('mouseleave.ratinghandler').off('click.ratinghandler');
		url = util.appendParamsToUrl(url, {
			'orderID' : ''+orderID, 
			'productID' : ''+productID, 
			'rating' : ''+starSelected.toFixed(1), 
			'pagecontext' : pageContext.type});
		$.ajax({
			url: url,
			type: 'POST',
			success: function (response) {
				if(typeof(response) == 'object'){
					$thisRatingParentTop.find('.rating-section-title').hide();
					$thisRatingParentTop.css({'border-color':'#fff','padding':'0'});
					if(!response.isReviewCreated){
						$thisRatingParent.css('cursor','default');
						$thisRatingParent.html('<span class="api-message" style="font-size: 12px;font-weight: 900;color: #25c117;background-color: #fff;">'+Resources.TurnTo_ExistingReview+'</span>');
					}else{
						$thisRatingParent.find('.fa').hide();
						$thisRatingParent.append('<span class="api-message" style="font-size: 12px;font-weight: 900;color: #25c117;background-color: #fff;">'+Resources.TurnTo_NewReview+'</span>');
						setTimeout(function(){
							$thisRatingParent.find('.api-message').hide();
							$thisRatingParent.find('.fa').show();
						}, 5000);
					}
					//$thisRatingParent.delay(5000).fadeOut(400);
				}else{
					console.error("TurnTo Review Response Error", url);
					$thisRatingParentTop.hide();
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.error("TurnTo Review AJAX Error", url, jqXHR, textStatus, errorThrown);
			}
		});
	});
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
    $('#addresses').on('click', '.delete', function (ev) {
        ev.preventDefault();
        /*eslint-disable no-alert */
        if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
            $.ajax({
                url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
                dataType: 'json'
            }).done(function (data) {
                if (data.status.toLowerCase() === 'ok') {
                    page.redirect(Urls.addressesList);
                } else if (data.message.length > 0) {
                    //window.alert(data.message);
                    $("#deleteaddressmodel").dialog({
            			dialogClass:'deleteaddressmodel',
            			open: function (event, ui) {     
            				$('body').css('overflow','hidden');
	            			if ($('#deleteaddressmodel').hasClass('d-none')){
		            			$('#deleteaddressmodel').removeClass('d-none');
		            			$('#deleteaddress-confirmationtext').html(data.message);
            				}
	            			$('#deleteaddressmodel').css('min-height','unset');
            			},
            			close:function() {
        	        		$('body').removeAttr("style");
        	        		$('html').removeAttr("style");	
        	        	},
            			modal: true
            		});
                } else {
                    page.refresh();
                }
            });
        }
        /*eslint-enable no-alert */
    });
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {
    $('.add-card').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });

    var paymentList = $('.payment-list');
    if (paymentList.length === 0) {
        return;
    }

    util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

    $('form[name="payment-remove"]').on('submit', function (e) {
        e.preventDefault();
        // override form submission in order to prevent refresh issues
        var button = $(this).find('.delete'),
            data;
        $('<input/>').attr({
            type: 'hidden',
            name: button.attr('name'),
            value: button.attr('value') || 'delete card'
        }).appendTo($(this));
        data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: data
        })
            .done(function () {
                page.redirect(Urls.paymentsList);
            });
    });
}
/**
 * @private
 * @function
 * @description init events for the loginPage
 */
function initLoginPage() {
    //o-auth binding for which icon is clicked
    $('.oAuthIcon').bind('click', function () {
    	$('.OAuthProvider').val(this.id);
    });
}
/**
 * @private
 * @function
 * @description Set the "empty" select value color in the new registration (TJC-613)
 */
function initSelectElements() {
    var greyPlaceholder = $('<div class="text-grey-placeholder"></div>').hide().appendTo('body').css('color'),
        greyDark        = $('<div class="text-grey-dark"></div>').hide().appendTo('body').css('color'),
        handleSelect;


    handleSelect = function ($element) {
        $element.css('color',  ['', '0'].indexOf($element.siblings().first().val()) > -1 ? greyPlaceholder : greyDark);
    };

    $('.selector > span').each(function () {
        handleSelect($(this));
    });

    $('.selector > span').bind('DOMSubtreeModified', function () {
        handleSelect($(this));
    });

    $('.selector').append('<span class="caret"><i class="fa fa-caret-down" aria-hidden="true"></i></span>');
}
/**
 * @private
 * @function
 * @description Fix the layout and behavior of the address select for the new registration (TJC-613)
 */
function accountAddressSelect() {
    var $buttonBillingAddress = $('.checkout-billing-address .address-check-btn > button'),
        $buttonShippingAddress = $('.checkout-shipping-address .address-check-btn > button'),
        $buttonBillingAddressZipInput = $buttonBillingAddress.parent().siblings('.field').children('input'),
        $buttonShippingAddressZipInput = $buttonShippingAddress.parent().siblings('.field').children('input'),
        $addressSelect     = $('.addresses-select'),
        $addressSelectRow  = $addressSelect.children().first(),
        $typeAddressButton = $addressSelect.children().last().find('button'),
        $additionaFields = $('.additional-address-fields');

    if ($buttonBillingAddressZipInput.val() === '') {
        $buttonBillingAddress.prop('disabled', true);
    }
    if ($buttonShippingAddressZipInput.val() === '') {
        $buttonShippingAddress.prop('disabled', true);
    }

    $buttonBillingAddressZipInput.on('change keydown keyup', function () {
        if ($buttonBillingAddressZipInput.val() === '') {
            $buttonBillingAddress.prop('disabled', true);
        } else {
            $buttonBillingAddress.prop('disabled', false);
        }
    });

    $buttonShippingAddressZipInput.on('change keydown keyup', function () {
        if ($buttonShippingAddressZipInput.val() === '') {
            $buttonShippingAddress.prop('disabled', true);
        } else {
            $buttonShippingAddress.prop('disabled', false);
        }
    });

    $buttonBillingAddressZipInput.on('click', function () {
        $addressSelect.removeClass('visually-hidden');
        $typeAddressButton.appendTo('.type-address-btn');
    });
    $buttonShippingAddressZipInput.on('click', function () {
        $addressSelect.removeClass('visually-hidden');
        $typeAddressButton.appendTo('.type-address-btn');
    });

    $typeAddressButton.on('click', function () {
        $addressSelect.addClass('visually-hidden');
        $additionaFields.removeClass('visually-hidden');
    });

    $addressSelectRow.addClass('form-row-with-button');
    $addressSelectRow.append('<div class="button-container type-address-btn"></div>');
    
    $( "#addnewCardQb" ).click(function() {
        var add1 = $('#dwfrm_quickbuyaddress_billingAddress_address_address1').val(),
        	add2 = $('#dwfrm_quickbuyaddress_billingAddress_address_address2').val(), 
        	city = $('#dwfrm_quickbuyaddress_billingAddress_address_city').val(), 
        	postcode = $('#dwfrm_quickbuyaddress_billingAddress_address_postCode').val(), 
        	country = $('#dwfrm_quickbuyaddress_billingAddress_address_country option:selected').val();
        
        if(!((add1.length > 0 || add2.length > 0) && city.length > 0 && postcode.length > 0 && country.length > 0)){
        	
        	if($( "#addnewcard-billingerror" ).hasClass('d-none')){
		    	$( "#addnewcard-billingerror" ).removeClass('d-none'); 
		    	$('#addnewcard-billingerror').html("Please enter a valid billing address.");
		    }
        }
        else{
        	if(!$( "#addnewcard-billingerror" ).hasClass('d-none')){
		    	$( "#addnewcard-billingerror" ).addClass('d-none'); 
		    }
        	if($( "#qbaddnewcard" ).hasClass('d-none')){
		    	$( "#qbaddnewcard" ).removeClass('d-none'); 
		    }
		    else{
		    	$( "#qbaddnewcard" ).addClass('d-none'); 
		    }
        	//unhide the add a new card div fields
        	/*dialog.open({
                url: Urls.addCard,
                options:{
    	        	dialogClass:'add-new-card-dialog'
                }
            });*/
        }
    	
    });
    
    $('body').on('change', '.activate-quickbuy-box .input-checkbox', function() {
        if($('.activate-quickbuy-box .input-checkbox').is(':checked')){
            $('button.activate-quickbuy').html('Activate & Checkout');
          }
          else{
              $('button.activate-quickbuy').html('Activate');
          }
    });
    
    $('.activate-quickbuy, .activate-tjcplus-membership').off('click').on('click', function(e) {
    	e.preventDefault();
    	
    	var isTJCPlusContext = $(this).data('istjcpluscontext') != undefined ?  $(this).data('istjcpluscontext') : false;
    	
    	if ($('#delivery-box .error').length > 0){
			$('#delivery-box .error').remove();
		}
    	if ($('#payment-form .error').length > 0){
			$('#payment-form .error').remove();
		}
    	if ($('#qbpaysafecard .error').length > 0){
			$('#qbpaysafecard .error').remove();
		}   	
    	if ($('.address-selector .error').length > 0){
			$('.address-selector .error').remove();
		}
    	
    	var isValid = true;
    	
    	if(!isTJCPlusContext && ($('.deliveryList').val() == "" || $('.deliveryList').val() == null)){
    		var newDiv ='<div class="error" style="color: #FFF;background: #c20000;border-radius: 3px;width: 17.8rem;padding: 6px 8px;margin-top: 5px;">Required.<div>';
		    $('#delivery-box').append(newDiv);
		    isValid = false;
    	}
    	
    	if($('.mobile-no input + .error-msg').length) {
            isValid = false;
        }

        if($('#dwfrm_quickbuyaddress_shippingAddress_addressid + .error-msg').length) {
            isValid = false;
        }
        
        $( ".deliveryList" ).change(function() {
			  if( !$('.deliveryList').val() == "" || !$('.deliveryList').val() == null){
					    $('#delivery-box .error').remove();
			    	}
		});    	

    	if ($('.quickbuytac').length > 0 && $('.quickbuytac').val == "true") {
    		$('.QB-tc label').css('color','red'); 
    		$('.QB-tc div.checker').css('border','1px solid #c20000'); 
    		isValid = false;
    	}
    	else{
    		$('.QB-tc label').css('color','#2c163d'); 
    		$('.QB-tc div.checker').css('border','1px solid #b9addb');
    	}

    	if($('#qb-activation-page #dwfrm_quickbuyaddress_customer_quickbuytac').length && !$('#qb-activation-page #dwfrm_quickbuyaddress_customer_quickbuytac').is(':checked')) {
    		$('.QB-tc label').css('color','red').css('font-weight','bold'); 
    		$('.QB-tc div.checker').css('border','3px solid #c20000'); 
    		isValid = false;
    	}
    	else{
    		$('.QB-tc label').css('color','#2c163d'); 
    		$('.QB-tc div.checker').css('border','1px solid #b9addb');
    	}
    	

    	/*if( $('#existing-paysafe-cvv').val() == "" || $('#existing-paysafe-cvv').val() == null){
    		var newDiv ='<div class="error" style="color: #FFF;background: #c20000;border-radius: 3px;width: 6rem;padding: 6px 8px;margin-top: 5px;">Required.<div>';
		    $('.qbpaysafecard').append(newDiv);
		    isValid = false;
    	}*/
    	
    	/*if( $('#customerAddress').val() == "" || $('#customerAddress').val() == null){
    		var newDiv ='<div class="error" style="color: #FFF;background: #c20000;border-radius: 3px;width: 17.8rem;padding: 6px 8px;margin-top: 5px;">Required.<div>'; 
		    $('.address-selector').append(newDiv);
		    isValid = false;
    	}*/
    	
    	if($('#addnewCardQb').hasClass('new-card-empty')){
    		var newDiv ='<div class="error" style="color: #c20000;width: 17.8rem;">Add new Card<div>';
		    $('#payment-form').append(newDiv);
		    isValid = false;
    	}

    	var $form = $(this).closest('form');
    	$form.validate();
    	if(!$form.valid() || !isValid){
    		progress.hide();
    		$('.activate-quickbuy').css('pointer-events','auto');
        	$('.activate-quickbuy').css('cursor','pointer');
        	$('.activate-quickbuy').css('opacity','1');
        	$('html, body, .checkout-addresses-form').animate({
        	    scrollTop: $('.validation-error').offset().top - 200
        	}, 400);
        	
    		return;
    	}
    	//var quickbuytac = $('#dwfrm_quickbuyaddress_customer_quickbuytac').val();
    	//var quickbuytac = $('#dwfrm_quickbuyaddress_customer_quickbuytac').val();
    	if($('#use-billing-address').is(":checked")) {
    		var samebillingaddress = true;
    	}
        else {
        	var samebillingaddress = false;
        }
    	var includeorder = $('.activate-quickbuy-box .input-checkbox').is(':checked');
    	var existingCards = $( "#existing-paysafe-card option:selected" ).val();
    	var deliveryoption = $('#dwfrm_quickbuyaddress_deliveryoption').val();
    	var data = $form.serialize();
    	
    	if(isTJCPlusContext){
    		var url =  Urls.tjcpluscheckout;
    	}else{
    		var url =  Urls.activateQuickbuy;
    	}
    	/*if ((window.location.href).split('?').length > 0) {
    		var urlSplitValues = (window.location.href).split('?')[1].split('&');
    		$.each(urlSplitValues,function(key,value){ 
    			//console.log(asdf(vrl,y[key].split('=')[0],y[key].split('=')[1]))  
    			url = util.appendParamToURL(url, urlSplitValues[key].split('=')[0], urlSplitValues[key].split('=')[1])
    		});
    	}
		util.appendParamsToUrl(url, {
			quickbuytac : quickbuytac,
			includeorder: includeorder
		})*/ 
    	
    	$('.activate-quickbuy').css('pointer-events','none');
    	$('.activate-quickbuy').css('cursor','default');
    	$('.activate-quickbuy').css('opacity','0.5');
    	
    	
    	var clickOnChangeButton = false;
    	if ($(this).hasClass('change-quickbuy')){
    		clickOnChangeButton = true;
    	}

    	$.ajax({
	        url: url + "?samebillingaddress=" + samebillingaddress + "&includeorder=" + includeorder + "&existingCards=" + existingCards + "&deliveryoption=" + deliveryoption,
	        data : data,
	        dataType : 'json',
	        type: 'POST',
	        beforeSend: function() { progress.show(); },
	        success: function (response) {
	        	progress.hide();
	        	if(!response.success){
	        		if ($('#activation-error').hasClass('d-none')){
                		$('#activation-error').removeClass('d-none');
                		$("#activation-error").html(response.data);	
	        		}
	        		if(response.errorCode) {
	        			$(".quantity-error").html(response.message).addClass('active');
	        			$('.testingTJCPLUS #dwfrm_quickbuyaddress').scrollTop(0);
	        			
	        			if(response.errorCode == "5"){
	        				$('.activate-tjcplus-membership').prop("disabled", true);
	        			}
	        		}
	        	}
	        	else if(response.success){
	        		if( (response.data != "") && response.data == "ordersuccessfull"){
	        			$('.showActivateQBModel').hide();
	        			$('.showActivateQBModel + .ui-widget-overlay').hide();
	        			$("#QBactivateconfirm").dialog({
			                dialogClass:'QBactivateconfirm',
			                resizable: true,
			                open: function (event, ui) {
			                	if ($('#QBactivateconfirm').hasClass('d-none')){
			                		$('#QBactivateconfirm').removeClass('d-none');
			                		$('#quickBuyActivateConfirmation').removeClass('d-none');
			                		$('#activation-success').addClass('d-none');
			                		$('body').css('overflow','hidden');
			                		$('#qborder-number').html(response.Orderno);
				        		}
			                    $('#QBactivateconfirm').css('min-height','unset');
			                    $("#qbConfirm-close").on('click', function(){
					        		$( ".ui-dialog-titlebar-close" ).trigger('click');
					        	});
			                    if(response.gtmPurchase){
				                    var gtmPurchase = JSON.stringify(response.gtmPurchase),
	        		        		gtmProducts = JSON.stringify(response.gtmProducts);
	        		        		$("#quickBuyConfirmation #gtmDataHolder").attr('data-gtmPurchase', gtmPurchase);
	        		        		$("#quickBuyConfirmation #gtmDataHolder").attr('data-gtmProducts', gtmProducts);
									/*window.dataLayer.push({
										'event':'purchase',
										'ecommerce': {
											'purchase': {
												'actionField': JSON.parse(gtmPurchase),
												'products': JSON.parse(gtmProducts)
											}
										}
									});*/
									orderconfirmation.init();
									dataLayer.push({'event': 'QBOrderConfirmation'});
			                    }
			                },
				        	close:function() {
				        		$('.ui-tooltip').each(function(index){
				                    $(this).remove();
				                });
				        		
				        		var url = Urls.restoreqbbasketfromsession; 
				        		$.ajax({
				    		        url: url,
				    		        type: 'POST',
				    		        success: function (response) { 
				    		        	//do something
				    		        }
				        		});
				        		$('body').css('overflow','x-hidden');
				        		url = window.location.href.split('#')[0];
	        	        		window.location.href = url.split('?')[0];
				        	},
			                modal: true
			            });
	        		}/* else if (clickOnChangeButton && (response.data != "") && (response.data == "Quick Buy Activated" || response.data == "Saved")){
    						redirectLocation();
    				}*/
	        		else{
	        			$("#QBactivateconfirm").dialog({
			                dialogClass:'QBactivateconfirm',
			                open: function (event, ui) {
			                	if ($('#QBactivateconfirm').hasClass('d-none')){
			                		$('#QBactivateconfirm').removeClass('d-none');
			                		$('#activation-success').html(response.data);
				        		}	                	
			                    //$('.ui-dialog-titlebar-close').hide();
			                    $('#QBactivateconfirm').css('min-height','unset');
			                    setTimeout(function(){ dialog.close(); redirectLocation();}, 2000);
			                },
			                modal: true
			            });
	        		}
	        		
	        		$("#qbConfirm-close").on('click', function(){
		        		dialog.close();
		        	});
	        	}	        		
	        	else{
	        		if ($('#activation-error').hasClass('d-none')){
                		$('#activation-error').removeClass('d-none');
                		$("#activation-error").html("Something didn't work. Please contact customer care on 0344 375 2525.");	     
	        		}	
	        }
	        	$('.activate-quickbuy').css('pointer-events','auto');
	        	$('.activate-quickbuy').css('cursor','pointer');
	        	$('.activate-quickbuy').css('opacity','1');	        	
	        	//var parseRes = JSON.parse(response);
	        	//window.location.href = parseRes.redirectURL
	        	$('body').removeAttr("style");
        		$('html').removeAttr("style");
	        }
		});
    });
}

function redirectLocation() {
	var pageURL = window.location.href,
	    pageContext = util.getParamFromUrl('pagecontext', pageURL);
	if (pageContext == '') {
		pageContext = $("input[name='pagecontext']").val();
	}
	if (pageContext == 'storefront') {
		window.location.href  = Urls.homePage;
	} else if (pageContext == 'livetv'){
		window.location.href  = Urls.watchtvpage;
	} else if (pageContext == 'wishlist') {
		window.location.href  = Urls.wishlist;
	} else if (pageContext == 'product') {
		var redirect = Urls.getProductUrl;
		var pid = util.getParamFromUrl('pid', pageURL);
		redirect = util.appendParamsToUrl(redirect,{
			'pid': pid
		});
		window.location.href  = redirect;
	} else{
		location.reload(true);
	}
}

/**
 * @private
 * @function
 * @description Binds the events of registration input tooltips
 */
function initIconTooltips() {

    // only in the registration page!
    $('.registration .icon-tooltip > .field').bind('click', function (event) {
        var $this    = $(this),
            offset   = $this.offset(),
            width    = $this.width(),
            start    = (offset.left + width) - 29,
            maxWidth = $(document).width() <= 767 ? 'width: ' + $this.first().width() - 40 + 'px;' : '',
            aClass = 'a' + (Math.random() + 1).toString(36).substring(7),
            tooltips,
            tooltipText,
            $fieldset,
            $element,
            $closeIcon,
            arrowRight;

        if (event.pageX >= start) {
            event.preventDefault();

            tooltips = $this.children('.tooltip-msg');
            tooltipText = $this.children('input').data('tooltip-text');

            if (tooltips.length > 0) {
                tooltips.remove();
            } else {
                $this.append('<div class="tooltip-msg" style="' + maxWidth + '">' +
                             tooltipText +
                             '<span class="close-icon fa fa-times"></span></div>');

                $element = $('.tooltip-msg');
                $fieldset = $this.closest('fieldset');

                $element.css('top', ($this.outerHeight() / 2) - ($element.outerHeight() / 2));
                $element.css('right', -($element.outerWidth() + 14));
                $element.addClass(aClass);

                arrowRight = ($element.outerWidth() - 10);

                if (($fieldset.position().left + $fieldset.outerWidth() - 10) <
                        ($element.position().left + $element.outerWidth())) {
                    $element.css('position', 'relative');
                    $element.css('right', 0);
                    $element.css('white-space', 'normal');
                    $element.css('padding-right', 20);
                    $element.addClass('tooltip-msg-mobile');

                    $closeIcon = $element.find('.close-icon');
                    $closeIcon.css('position', 'absolute');
                    $closeIcon.css('top', 4);
                    $closeIcon.css('right', -($element.outerWidth() - 25));

                    arrowRight = (($element.outerWidth() / 2) - 5);
                }

                $('<style>.' + aClass + ':before {right: ' +
                        arrowRight + 'px !important;}</style>').appendTo('head');
            }
        }
    });

    $('.registration .icon-tooltip > .field').bind('mousemove', function (event) {
        var $this    = $(this),
            offset   = $this.offset(),
            start    = (offset.left + $this.width()) - 29;

        if (event.pageX >= start && event.pageX <= start + 25) {
            $this.addClass('tooltip-hover');
        } else {
            $this.removeClass('tooltip-hover');
        }
    });

    $('.registration .icon-tooltip > .field').bind('mouseout', function () {
        $(this).removeClass('tooltip-hover');
    });
}

/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
    initOrderHistory();
    initAddressEvents();
    initPaymentEvents();
    initLoginPage();
    initSelectElements();

    if ($('.registration-alt').length > 0) {
        initIconTooltips();
        accountAddressSelect();
    }
	if ($('.quickbuy-preference-page').length > 0) {
        initIconTooltips();
        accountAddressSelect();
        
    }
    addressFields.init($('.address-form'));
    addressFields.init($('#RegistrationForm'));
	
	addressFields.init($('.country-postal'));
	
    tabs.init();
    resetPassword.init();
    
    // Omit HTML tags from name input fields
    $('.first-name').focusout(function(){
        $('#dwfrm_profile_customer_firstname').val($('#dwfrm_profile_customer_firstname').val().replaceAll(/<(“[^”]*”|'[^’]*’|[^'”>])*>/ig,''));
    });

    $('.last-name').focusout(function(){
        $('#dwfrm_profile_customer_lastname').val($('#dwfrm_profile_customer_lastname').val().replaceAll(/<(“[^”]*”|'[^’]*’|[^'”>])*>/ig,''));
    });
    
    
    $('.sameBillingCheckbox input[type="checkbox"]').click(function(){
        if($(this).prop("checked") == true){
            $( ".checkout-billing-address .saved-address" ).addClass( "hidden" );
        }
        else if($(this).prop("checked") == false){
        	$( ".checkout-billing-address .saved-address" ).removeClass( "hidden" );
        }
    });
      
    
    
    $('.add-new-address').click(function(){ 
    	
		// Auto fucts when text box having a value 
		setTimeout(function(){ 
			$(".input-text").each(function () {  
			  if( $(this).val().length >= 1 ) {
			        $(this).siblings(".label").addClass('focus-active');
			        $(this).closest('.form-row').addClass('validation-success');
			         $('.postcodeinput input').focus();
			         $('.postcodeinput input').blur();
			    } 
			                    
			 }) 
			},500);
			 	 			 
    	
    	if($(this).closest('.address-boxes').find('.shippingform').length) {
            if($('.shippingform').is(':visible')){
                $('.shipping-address-selector #customerAddress').trigger('change');
                //$('.customerAddressBilling #customerAddress').trigger('change');
            }
            else {
                if($(this).hasClass('shipping-new-address')){
                    $('#dwfrm_quickbuyaddress_shippingAddress_addressid').val('');
                }
               
                if($(this).hasClass('billing-new-address')){
                    $('#dwfrm_quickbuyaddress_billingAddress_addressid').val('');
                }
            }
        }
       
        if($(this).closest('.address-boxes').find('.billingform').length) {
            if($('.billingform').is(':visible')){
                $('.customerAddressBilling #customerAddress').trigger('change');
            }
            else {
                if($(this).hasClass('shipping-new-address')){
                    $('#dwfrm_quickbuyaddress_shippingAddress_addressid').val('');
                }
               
                if($(this).hasClass('billing-new-address')){
                    $('#dwfrm_quickbuyaddress_billingAddress_addressid').val('');
                }
            }
        }
    	
    	//Remove disabled attribute if there is a value in postCode
        if($("#dwfrm_quickbuyaddress_shippingAddress_address_postCode").val() !="" ){ $(".address-check-btn button").removeAttr("disabled"); }
    	
    	var $addbillingaddress = $(this).closest('.address-boxes').find('.add-billing-address') ;
    	if($addbillingaddress.hasClass("hidden"))
    		$addbillingaddress.removeClass("hidden");
    	else
    		$addbillingaddress.addClass("hidden");
    });
    
    $('body').on('change','#dwfrm_quickbuyaddress_deliveryoption', function(e) { 
    	if($('#includeOrderBasket').length > 0){
	    	var shippingMethodID = $(this).val(); 
	    	var url = Urls.updateqbshippingmethod;
	    	url = util.appendParamToURL(url,'shippingMethodID',shippingMethodID);
	    	$.ajax({
	            url: url,
	            type: 'POST',
	            success: function (response) { 
	            	$('#includeOrderBasket').html(response);
	            	require('./../uniform')();
	            	qbfornonlivetv.addWarrantyQB();
	            	/*Hide warranty details in QB popup in home page and live Tv page*/
	        		if (pageContext.ns == 'storefront' || pageContext.ns == 'livetv') {
	        			$('.showActivateQBModel .qbwarrantydetails').hide()
	        		}
	            }
	    	});
    	}
    });
    
    $("select#existing-paysafe-card").change(function(){
    	var selectedCountry = JSON.parse($(this).children("option:selected").val());
    	var selectedExpiryMonth = selectedCountry.cardExpiry.month;
    	var selectedExpiryYear = selectedCountry.cardExpiry.year;
    	var expiry = "Expiry: " + selectedExpiryMonth + "/" + selectedExpiryYear;
    	$('#cardExpiry').val(expiry);
    });
    
    $('.change-method').on('click', function(){
    	if ($(this).attr("class") == 'change-method') {
    		var selectDialog = $('.dialog-change-service');
    	}
    	
    	if ($(this).attr("class") == 'end-membership-method') {
    		var selectDialog = $('.dialog-end-service');
    	}
    	
    	selectDialog.dialog({ 
			modal: true,
			dialogClass: "tjc-customer-support",
			open: function(event,ui){
        		$('.ui-widget-overlay').on('click', function(){
        			$('.tjc-customer-support').find('.ui-dialog-content').dialog('close');
        		});
			},
			  buttons: [
			    {
			      text: "OK",
			      click: function() {
			        $( this ).dialog( "close" );
			      }
			    }
			  ]
		});
	});
    
    $('.manage-membership-button').on('click', function(){
    	$('.membership-change').show();
    }); 
    
    $(window).on('click scroll', function (event) {
    	if (!$(event.target).closest('.manage-membership-button, .membership-change').length) {
    		$('.membership-change').hide();
    	}
    });
    
    $('.subscriptions-button.add-to-cart').on('click', function(e) {
    	e.preventDefault();
    	var redirectURL = $(this).attr('href');
    	var pid = $(this).data('pid');
    	$.ajax({
            url: Urls.tjcplusbasketdetails,
            type: 'GET',
            success: function (response) {
            	var responseJson = JSON.parse(response);
            	if(responseJson.containsTJCPlusProduct){
            		if(responseJson.subscriptionDuration == "1") {
            			var existingProductDialog = $('.existing-product.monthly');
            		}
            		if(responseJson.subscriptionDuration == '12') {
            			var existingProductDialog = $('.existing-product.yearly');
            		}
            		existingProductDialog.dialog({ 
            			modal: true,
            			dialogClass: "existing-tjcplus-product",
            			  buttons: [
            			    {
              			      text: "Go to cart",
              			      click: function() {
              			    	window.location.href = Urls.cartShow;
              			      }
              			    },
              			    {
            			      text: "OK",
            			      click: function() {
            			        $( this ).dialog( "close" );
            			      }
            			    }
            			  ]
            		});            		
            	}
            	else {
            		window.location.href = redirectURL;
            	}
            }
    	});
    });
}






account = {
    init: function () {
        initializeEvents();
        giftcert.init();
        quickbuy.init();
        qbfornonlivetv.init();
        newsignin.init();
        tjcplusactivation.init();
    },
    initCartLogin: function () {
        initLoginPage();
    },
    initSelectElements: function () {
        initSelectElements();
    }

};

module.exports = account;
