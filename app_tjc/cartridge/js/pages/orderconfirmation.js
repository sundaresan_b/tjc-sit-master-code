'use strict';

var gtmDatalayer = require('../gtm-datalayer'),
    util = require('../util');

function init(){
	gtmDatalayer.pushDataFromPurchase();
	
	var $gtmDataHolder = $('#gtmDataHolder');
	var purchase = $gtmDataHolder.data('gtmpurchase');
	var products = $gtmDataHolder.data('gtmproducts');
	var orderCategory = $gtmDataHolder.data('gtmordercategory');
	var productCoupons = (purchase.coupon)?purchase.coupon+",":"",
	    awinCookieValue = util.getCookie("awc"),
	    awinMerchantID = window.SitePreferences.awinMerchantID,
	    awinCommissionGroup = orderCategory,
	    awinChannel = window.SitePreferences.awinChannel;
	
	if(awinMerchantID != "" && awinCommissionGroup != "" && awinChannel != ""){
		$(products).each(function(index, value){
			if(value.coupon){
				productCoupons += value.coupon;
				if(index+1 < products.length && productCoupons != ""){
					productCoupons += ",";
				}
			}
		});
		var url = Urls.awinTracking;
		var url = util.appendParamsToUrl(url, {
		    merchant : ''+awinMerchantID,
			ref : ''+purchase.id,
		    amount : ''+purchase.revenue,
		    parts : awinCommissionGroup+':'+purchase.revenue,
		    cr : 'GBP',
		    vc : productCoupons,
		    ch : awinChannel,
		    cks : ''+awinCookieValue
		});
		
		$.ajax({
			type: "GET",
			url: url,
			success: function(data) {
				console.log('AWIN triggered');
			},
			error: function(f) {
				console.log(f.statusText + " " + f.status);
			}
		});
	}
}

module.exports = {
	init: init
};