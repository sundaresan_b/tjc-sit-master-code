'use strict';

var account = require('./account'),
    bonusProductsView = require('../bonus-products-view'),
    informMeWhenAvailable = require('../informmewhenavailable'),
    gtmDataLayer = require('../gtm-datalayer'),
    util = require('../util'),
	ajax = require('../ajax'),
	progress = require('../progress'),
    newsignin = require('../newsignin'),
    tjcplusactivation = require('../tjcplusactivation'),
    pdpoverlay = require('../pdpoverlay');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
    var $shippingMethodForm = $('#cart-shippingmethod-form');

    $('.cart')
        .on('click', 'a.select-bonus', function (e) {
            e.preventDefault();
            bonusProductsView.show(this.href);
        });
    informMeWhenAvailable.init();

    // override enter key for coupon code entry
    $('form input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) {
            return false;
        }
    });

    $shippingMethodForm.find('select').on('change', function () {
        $shippingMethodForm.submit();
    });

    $('.selector').append('<span class="caret"><i class="fa fa-caret-down" aria-hidden="true"></i></span>');

    // we need to pass the gtm data along with the selected shipping option
    $('#checkout-form a.xlt-continueCheckout').on('click',function (e) {
    	console.log(sendGTMCartData());
        return true;
    });
    
    //sendGTMCartData();
    
    $('.item-remove-details').on('click', function(){
    	progress.show();
    	var engraving = $(this).data("engraving"),
    		pliId = $(this).data("pliid");
    	var url = Urls.removeEngravingFromPli;
    	$.ajax({
	        url: url + '?engraving=' + engraving + '&pliId=' + pliId,
	        type: 'POST',
	        success: function (response) { 
	        	progress.hide();
	        	location.reload(true);
	        }
    	});
    	
    });
	
	//Show alert when we enter the product quantity above maximum allowed quantity
	var thresholdquantity = parseInt($('.thresholdquantity').val());
    
    $('.quantity-input-container input').on('keyup', function(){
	    if($(this).val() > thresholdquantity) {
	    	$(this).closest('.quantity-input-container').find('.product-exceed-qty-msg').show();
	    } else {
	    	$(this).closest('.quantity-input-container').find('.product-exceed-qty-msg').hide();
	    }
    });
	
	 $('.item-remove .button-icon').on('click', function () {
    	$('.product-availability .input-text').removeClass('required');    	 
    });
    
	 var couponstatus = $('.couponstatus').text();
	 var already=couponstatus.search("COUPON_CODE_ALREADY_IN_BASKET");
	 var correct=couponstatus.search("OK");
	 if(already >=0){
		 $('.already').css("display","block");
		 $('.active').css("display","none");
	 }
	 else if(correct >= 0){
		 $('.already').css("display","none");
		 $('.active').css("display","none");
		 $('.cart-coupon-code .optinal-field').removeClass('validation-error'); 
	 }
	 else {
		 if($('.cart-coupon-code .form-row').hasClass('validation-error')){
			 $('.cart-coupon-code').css("display","block");
			 $('.cart-coupon-code .error-msg').css("display","block");
		 }		 
		 $('.already').css("display","none");
		 $('.active').css("display","block");
	 }
	 if($('.inform-me-when-available').length >0 || $('.producterror').length >0 ){
		 $('.cart-checkout fieldset .xlt-continueCheckout').addClass('unproceed');
		 }
		 if (pageContext.ns == 'cart'){
		 $('.producterror').css("display","block");
	 }		 
	$('.cart-coupon-code-button').off('click').on('click',function(){
		couponButton();		
	});
	
	if($('.pt_cart').length > 0){
		captureErrorsForGA();
	}
	
	/** Cart page Sticky Checkout button **/
	if (window.matchMedia("(max-width: 767px)").matches) {
		var elementHeight = $('.cart-checkout.sticky-cart-checkout').height();
		var windowHeight = $(window).innerHeight();
		var scroll, elementPos, flagCount = 0, flagButton = false, initFlag = true;
		elementPos = $('.cart-checkout.sticky-cart-checkout').offset().top;
		if((elementPos+elementHeight) < windowHeight){
			$('.cart-checkout.sticky-cart-checkout').css('position','inherit');
			$('.cart-checkout.sticky-cart-checkout').find('a.xlt-continueCheckout').removeClass('sticky-button');
			initFlag = false;
		}else{
			$('.cart-checkout.sticky-cart-checkout').css('position','fixed');
			$('.cart-checkout.sticky-cart-checkout').find('a.xlt-continueCheckout').addClass('sticky-button');
		}
		console.log('Checkout button sticky init:',initFlag);
		if(initFlag){
			$(window).scroll(function(){
				scroll = $(window).scrollTop();
				//elementPos = $('.cart-checkout.sticky-cart-checkout').offset().top;
				if((elementPos+elementHeight)>(scroll+windowHeight)){
					flagCount = 0;
					flagButton = true;
				}else{
					flagCount++;
					flagButton = false;
				}
				
				if(!flagButton){
					$('.cart-checkout.sticky-cart-checkout').css('position','inherit');
					$('.cart-checkout.sticky-cart-checkout').find('a.xlt-continueCheckout').removeClass('sticky-button');
				}else{
					$('.cart-checkout.sticky-cart-checkout').css('position','fixed');
					$('.cart-checkout.sticky-cart-checkout').find('a.xlt-continueCheckout').addClass('sticky-button');
				}
			});
		}
	}
	/** Cart page Sticky Checkout button **/
}

function captureErrorsForGA(){
	var couponErrorActive = $('#cart-coupon-form .error-msg .active:visible').length,
	    couponErrorAlready = $('#cart-coupon-form .error-msg .already:visible').length,
	    couponErrorMessage = '';
	if(couponErrorActive > 0){
		couponErrorMessage = $('#cart-coupon-form:visible .error-msg .active').text();
	}else if(couponErrorAlready > 0){
		couponErrorMessage = $('#cart-coupon-form:visible .error-msg .already').text();
	}else{
		couponErrorMessage = $('#cart-coupon-form:visible .error-msg .couponstatus').text();
	}
	if(couponErrorMessage != '' && couponErrorMessage != 'null'){
		window.dataLayer.push({
			'event': 'cartError',
			'ecommerce': {
				'errorText': 'Coupon Error',
				'errorMessage': ''+couponErrorMessage
			}
		});
	}
	
	if($('.error-form.message .message-content:visible').length > 0){
		var errorMessage = $('.error-form.message .message-content').text();
		window.dataLayer.push({
			'event': 'cartError',
			'ecommerce': {
				'errorText': 'Overall Error',
				'errorMessage': ''+errorMessage.trim()
			}
		});
	}
	
	var productErrorCount = $('.producterror:visible').length,
	    $this, $parent, gtmData, productID, errorMessage;
	if(productErrorCount > 0){
		$('.producterror:visible').each(function(index,value){
			$this = $(this);
			$parent = $this.parents('.cart-row');
			gtmData = $parent.data("gtm");
			productID = gtmData.id;
			errorMessage = $this.text();
			
			window.dataLayer.push({
				'event': 'cartError',
				'ecommerce': {
					'errorText': 'Product Error',
					'errorMessage': productID+': '+errorMessage.trim()
				}
			});
		});
	}
}

function couponButton(){
	if($('.cart-coupon-code-button .symbol-down').length){
		$('.cart-coupon-code-button .symbol-down').addClass('symbol-up').removeClass('symbol-down');
		$('.cart-coupon-code-button').css('border-bottom','1px solid transparent');
		$('.cart-coupon-code').show();
	}else if($('.cart-coupon-code-button .symbol-up').length){
		$('.cart-coupon-code-button .symbol-up').addClass('symbol-down').removeClass('symbol-up');
		$('.cart-coupon-code-button').css('border-bottom','1px solid #ddd');
		$('.cart-coupon-code').hide();
	}
}

// Omit HTML tags from  name input fields
$('.first-name').focusout(function(){
    $('#dwfrm_profile_customer_firstname').val($('#dwfrm_profile_customer_firstname').val().replaceAll(/<(“[^”]*”|'[^’]*’|[^'”>])*>/ig,''));
});

$('.last-name').focusout(function(){
    $('#dwfrm_profile_customer_lastname').val($('#dwfrm_profile_customer_lastname').val().replaceAll(/<(“[^”]*”|'[^’]*’|[^'”>])*>/ig,''));
});


/*Remove unwanted error message */

$('.quantity-input-container input').on('blur', function() {
  var carterror = $('.quantity-input-container .error-msg').text ();     
 
        if (carterror == 'Cannot be blank') {
         $('.error-msg').show();
      } 
      
     else {
         $('.error-msg').hide();
        
      }     
});

$('.quantity-input-container input').on('focus', function() {
  var carterror = $('.quantity-input-container .error-msg').text ();     
 
        if (carterror == 'Cannot be blank') {
         $('.error-msg').show();
      } 
      
     else {
         $('.error-msg').hide();
        
      }     
});


/*Trigger a click once you change the dropdown */
$(".quantity-input-container .cartquantity select").change(function() {
  	$(this).closest('td').find('button.button-update').trigger("click");
});

$(".hiddenActionButtonTrigger").on('click', function(){
	$('.hiddenActionButton').click();
})

$('.quantity-input-container').each(function() {
	var $attribute = $(this).find('.attribute');
	if($attribute.length > 1){
		$attribute.parent().addClass('twovariations');
	}
});


function sendGTMCartData(){
	try {
		// data is held in placeholder div which is generated in gtm_checkout.isml
		var gtmJSON = gtmDataLayer.getGTMData(),
		shippingMethod = document.getElementById('dwfrm_cart_selectedShippingMethodID');
		// for this step, we also need to send the shippingmethod
		gtmJSON.ecommerce.checkout.actionField.option = null;/*shippingMethod.value*/
		gtmDataLayer.pushCheckoutStepToDataLayer(gtmJSON);
		return gtmJSON;
	} catch (e) {
		// eslint-disable-next-line no-console
		return e;
	}
}



exports.init = function () {
    initializeEvents();
    account.initCartLogin();
    newsignin.init();
    tjcplusactivation.init();
};
