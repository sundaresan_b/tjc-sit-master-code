/*jslint nomen: true */
'use strict';

var dialog = require('../../dialog'),
    minicart = require('../../minicart'),
    util = require('../../util'),
    TPromise = require('promise'),
    _ = require('lodash'),
    addedModal = require('../../addedmodal'),
    stickyheader = require('../../stickyheader'),
    variant = require('./variant');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
    var $form = $(form),
        $qty = $form.find('input[name="Quantity"]'),
        cgid = util.getQueryStringParams(window.location.hash.substr(1)).cgid ||
            $('#quickViewCgid').val() || $('#primarycategoryid').val(),
        isSearch = util.getQueryStringParams(window.location.hash.substr(1)).issearch ||
            $('#quickViewIsSearch').val() || false;
    if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
        $qty.val('1');
    }
    
    var isEngravingItem = $('#isEngravingItem').val(),
    	newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign');
    if(isEngravingItem == "true"){
    	var errorFields = "";
        var engravingValidateFields = ["mandatory_color", "mandatory_size", "mandatory_birthstone", "mandatory_birthflower", "mandatory_chakra", "mandatory_zodiac", "mandatory_engraving1", "mandatory_engraving2", "mandatory_engraving3", "mandatory_engraving4"];
        var $engravingForm = $('form.engraving-form:visible');
        jQuery.each(engravingValidateFields, function(i,val){
        	if($engravingForm.find('#'+val).length >= 1){
        		if($engravingForm.find('#'+val).val() == "true"){
        			var fieldToChk = $engravingForm.find('#'+val).attr('data-validateClass');
        			if($engravingForm.find('#'+fieldToChk).val() == ""){
        				if($engravingForm.find('#'+fieldToChk).prop('tagName') == "SELECT"){
        					var $selectParent = $engravingForm.find('#'+fieldToChk).parents('.form-row-select');
        					$selectParent.find('.styledSelect').css('border-color','#C20000').css('border-width','1.5px');
        					$engravingForm.find('#'+fieldToChk+'-error').css('display','block');
        				}else{
        					$engravingForm.find('#'+fieldToChk).css('border-color','#C20000').css('border-width','1.5px');
        					$engravingForm.find('#'+fieldToChk+'-error').css('display','block');
        				}
        				errorFields+=fieldToChk;
        			}else{
        				if($engravingForm.find('#'+fieldToChk).prop('tagName') == "SELECT"){
        					var $selectParent = $engravingForm.find('#'+fieldToChk).parents('.form-row-select');
        					$selectParent.find('.styledSelect').css('border-color','#b9addb').css('border-width','1px');
        					$engravingForm.find('#'+fieldToChk+'-error').css('display','none');
        				}else{
        					$engravingForm.find('#'+fieldToChk).css('border-color','#b9addb').css('border-width','1px');
        					$engravingForm.find('#'+fieldToChk+'-error').css('display','none');
        				}
        			}
        		}
        	}
        });
        if(!$engravingForm.find('#dwfrm_engraving_engravingtac').is(":checked")){
        	$engravingForm.find('div.checker').css('border-color','#C20000').css('border-width','1.5px');
        	$engravingForm.find("label[for='dwfrm_engraving_engravingtac']").css('color','#C20000');
        	var fieldToChk = "form.engraving-form:visible #dwfrm_engraving_engravingtac";
        	errorFields+=fieldToChk;
        }else{
        	$engravingForm.find('div.checker').css('border-color','#b9addb').css('border-width','1px');
        	$engravingForm.find("label[for='dwfrm_engraving_engravingtac']").css('color','#2c163d');
        }
        var engravingForm = $engravingForm;
        
        if(errorFields!=""){
        	$('input, select').on('keyup keydown change', function(){
        		var id = $(this).attr('id'),
        			parentClass = "form.engraving-form:visible ",
        			mandVal = $('input[data-validateClass='+id+']').val();
        		if(mandVal=="true"){
        			if($(this).val() == ""){
        				if($(this).prop('tagName') == "SELECT"){
        					$(parentClass+'#uniform-'+id).css('border-color','#C20000').css('border-width','1.5px');
        					$(parentClass+'.'+id+'-error').css('display','block');
        				}else{
        					$(this).css('border-color','#C20000').css('border-width','1.5px');
        					$(parentClass+'.'+id+'-error').css('display','block');
        				}
        			}else{
        				if($(this).prop('tagName') == "SELECT"){
        					$(parentClass+'#uniform-'+id).css('border-color','#b9addb').css('border-width','1px');
        					$(parentClass+'.'+id+'-error').css('display','none');
        				}else{
        					$(this).css('border-color','#b9addb').css('border-width','1px');
        					$(parentClass+'.'+id+'-error').css('display','none');
        				}
        			}
        		}
        	});
        	return false;
        }
    }
    
    var isWarrantyEligible = $('#isWarrantyEligible').val();
    if(isWarrantyEligible == "true"){
    	var isWarrantySelected = ($(".pdp-mobile-only").is(":visible"))?$(".pdp-mobile-only input[name='warrantyEnabled']").prop("checked"):$(".sticky-desktop-check input[name='warrantyEnabled']").prop("checked");
    	if(isWarrantySelected){
    		var warrantyQty = $('#warranty-total-quantity').val();
    	}
    }
    
    $('#quickViewCgid').remove();
    $('#quickViewIsSearch').remove();
    if (cgid) {
        $form.append($('<input id="quickViewCgid" type="hidden" name="cgid" value="' + cgid + '"/>'));
    }
    $form.append($('<input id="quickViewIsSearch" type="hidden" name="issearch" value="' + isSearch + '"/>'));
    
    if(isWarrantyEligible == "true"){
    	return TPromise.resolve($.ajax({
            type: 'POST',
            url: util.ajaxUrl(Urls.addProduct),
            data: $form.serialize() + '&isWarrantySelected=' + isWarrantySelected + '&warrantyQty=' + warrantyQty + '&quickview=isQuickView'
        }));
    }
    else if(isEngravingItem == "true"){
    	return TPromise.resolve($.ajax({
            type: 'POST',
            url: util.ajaxUrl(Urls.addProduct),
            data: $form.serialize() + '&' + engravingForm.serialize()
        }));
    }else{
    	return TPromise.resolve($.ajax({
            type: 'POST',
            url: util.ajaxUrl(Urls.addProduct),
            data: $form.serialize()
        }));
    }
    
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function (e) {
    e.preventDefault();
    var $form = $(this).closest('form'),
        $button = $(e.target),
        paymentPreselectID = $button.data('payment-preselect'),
        callback = e.data;

    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('cart-variants-group');
    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('crossed');
    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection a.swatchanchor:visible').removeClass('sticky-variant-anchor');
    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection').removeClass('sticky-selected');
    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size li:not(.unselectable):visible').removeClass('sticky-variant-selection');
    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart:visible').css('margin-top','10px');
    $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart .pdp-sticky:visible').removeClass('remove-box-shadow');

    if (paymentPreselectID) {
        $form.append($('<input type="hidden" name="paymentpreselect" value="' + paymentPreselectID + '"/>'));
    }

    addItemToCart($form).then(function (response) {
        var $uuid = $form.find('input[name="uuid"]');
        if($('#isAbTest').length){
    		window.location.href = Urls.cartShow;
    	}
    	else{
	        if ($uuid.length > 0 && $uuid.val().length > 0) {
	            window.location.href = $button.data('carturl');
	        } else {
	            // do not close quickview if adding individual item that is part of product set
	            // @TODO should notify the user some other way that the add action has completed successfully
	            if (!$(this).hasClass('sub-product-item')) {
	                dialog.close();
	            }
	
	            addedModal.init();
	            minicart.show(response);
	        }
	   }
    }.bind(this));

    if (typeof callback === 'function') {
        callback($button);
    }
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
    e.preventDefault();
    var $productForms = $('#product-set-list').find('form').toArray();
    TPromise.all(_.map($productForms, addItemToCart))
        .then(function (responses) {
            dialog.close();
            // show the final response only, which would include all the other items
            minicart.show(responses[responses.length - 1]);
        });
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 * @param {Element} target The target on which an add to cart event-handler will be set
 */
module.exports = function (target, callback) {
    $('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());

    if (target) {
        target.on('click', '.add-to-cart', callback, addToCart);
    } else {
        $('.add-to-cart').on('click', callback, addToCart);
    }

    $('#add-all-to-cart').on('click', addAllToCart);

    $('.add-to-cart-sticky, .quick-buy-sticky').off('click.stickybuttonspdp').on('click.stickybuttonspdp', function (e) {
    	e.preventDefault();
    	var isQuickBuySticky = $(this).hasClass('quick-buy-sticky');
        if($('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper.variant-product:visible').length >= 1 && $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations:visible').length >= 1){
            if($('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').hasClass('cart-variants-group')){
                if($('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection.sticky-selected').length < 1){
                    return;
                }
                var $anchor = $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection.sticky-selected .sticky-variant-anchor');
                var href = $anchor.attr('href');
                var anchorParentIsSelected = $anchor.parents('.sticky-variant-selection').hasClass('selected');
                if(anchorParentIsSelected){
                	if(isQuickBuySticky){
                		console.warn('quick buy triggering....');
                		$('#showActivateQBModel').trigger('click');
                	}else{
                		console.warn('add to bag triggering....');
                		$('.pdpForm .add-to-cart:visible').trigger('click');
                	}
                }else{
                    var response = variant.updateContent(href);
                    var i = 0;
                    $(document).ajaxComplete(function(){
                        if(i < 1){
                            if(isQuickBuySticky){
                            	console.warn('quick buy triggering....');
                                $('#showActivateQBModel').trigger('click');
                        	}else{
                        		console.warn('add to bag triggering....');
                                $('.pdpForm .add-to-cart:visible').trigger('click');
                        	}
                            i++;
                        };
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('cart-variants-group');
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection a.swatchanchor:visible').removeClass('sticky-variant-anchor');
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection').removeClass('sticky-selected');
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size li:not(.unselectable):visible').removeClass('sticky-variant-selection');
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('crossed');
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart:visible').css('margin-top','10px');
                        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart .pdp-sticky:visible').removeClass('remove-box-shadow');
                    });
                }
            }else{
                var height = $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').outerHeight();
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').addClass('cart-variants-group');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart .pdp-sticky:visible').addClass('remove-box-shadow');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart:visible').css('margin-top',(height+15)+'px');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size li:not(.unselectable):visible').addClass('sticky-variant-selection');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection a.swatchanchor:visible').addClass('sticky-variant-anchor');
                stickyheader.stickyVariantChanges();
            }
        }else{
        	if(isQuickBuySticky){
        		console.warn('quick buy triggering....');
        		$('#showActivateQBModel').trigger('click');
        	}else{
        		console.warn('add to bag triggering....');
        		$('.pdpForm .add-to-cart:visible').trigger('click');
        	}
        }
    });
};
