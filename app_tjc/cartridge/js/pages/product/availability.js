'use strict';

var ajax = require('../../ajax'),
    util = require('../../util');

var updateContainer = function (data) {
    var $availabilityMsg = $('.pdp-main .availability .availability-msg'),
        message; // this should be lexically scoped, when `let` is supported (ES6)
    if (!data) {
        $availabilityMsg.html(Resources.ITEM_STATUS_NOTAVAILABLE);
        return;
    }
    $availabilityMsg.empty();
    // Look through levels ... if msg is not empty, then create span el
    if (data.levels.IN_STOCK > 0) {
        if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            // Just in stock
            message = Resources.IN_STOCK;
        } else {
            // In stock with conditions ...
            message = data.inStockMsg;
        }
        $availabilityMsg.append('<div class="in-stock-msg">' + message + '</div>');
    }
    if (data.levels.PREORDER > 0) {
        if (data.levels.IN_STOCK === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            message = Resources.PREORDER;
        } else {
            message = data.preOrderMsg;
        }
        $availabilityMsg.append('<div class="preorder-msg msg">' + message + '</div>');
    }
    if (data.levels.BACKORDER > 0) {
        if (data.levels.IN_STOCK === 0 && data.levels.PREORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            message = Resources.BACKORDER;
        } else {
            message = data.backOrderMsg;
        }
        $availabilityMsg.append('<div class="backorder-msg msg">' + message + '</div>');
    }
    if (data.inStockDate !== '') {
        $availabilityMsg.find('.msg').append(' <span class="expected">' +
            String.format(Resources.IN_STOCK_DATE, data.inStockDate) + '</span>');
    }
    if (data.levels.NOT_AVAILABLE > 0) {
        if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.IN_STOCK === 0) {
            message = Resources.NOT_AVAILABLE;
        } else {
            message = Resources.REMAIN_NOT_AVAILABLE;
        }
        $availabilityMsg.append('<div class="not-available-msg">' + message + '</div>');
    }
};

var getAvailability = function () {
    ajax.getJson({
        url: util.appendParamsToUrl(Urls.getAvailability, {
            pid: $('#pid').val(),
            Quantity: $(this).val()
        }),
        callback: updateContainer
    });
};

module.exports = function () {
    $('#pdpMain').on('change', '.pdpForm input[name="Quantity"]', getAvailability);
};
