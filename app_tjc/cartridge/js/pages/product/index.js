'use strict';

var dialog = require('../../dialog'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    addToCart = require('./addToCart'),
    availability = require('./availability'),
    image = require('./image'),
    productNav = require('./productNav'),
    productSet = require('./productSet'),
    stickyheader = require('../../stickyheader'),
    variant = require('./variant'),
    gtmDatalayer = require('../../gtm-datalayer'),
    informMeWhenAvailablePDP = require('../../informmewhenavailablepdp.js'),
    tjcplusactivation = require('../../tjcplusactivation'),
    qbfornonlivetv = require('../../qbfornonlivetv');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
    $('#pdpMain .product-detail .product-tabs').tabs();
    productNav();
    tooltip.init();
    qbfornonlivetv.init();
    
    var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign');
    if(newPDPFlag == true || newPDPFlag == "true"){
    	var i=0;
		setTimeout(function(){
			var pixleeAdjust = setInterval(function(){
				i++;
				if(i > 10){
					clearInterval(pixleeAdjust);
				}
				pixleeAdjuster();
			}, 1000);
		}, 1000);
		
		$(document).scroll(function(){
			pixleeAdjuster();
		});
    }
}

function pixleeAdjuster(){
	$('.pixlee_container').each(function(){
		var IFrameWidth = $(this).find('iframe').css('width');
		var ParentWidth = $(this).css('width');
		if(IFrameWidth != ParentWidth){
			$(this).find('iframe').css('width',$(this).css('width'));
		}
	});
}

/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
    var $pdpMain = $('#pdpMain');

    addToCart();
    availability();
    variant.init();
    image();
    productSet();
    informMeWhenAvailablePDP.init();
    tjcplusactivation.init();
    if (SitePreferences.STORE_PICKUP) {
        productStoreInventory.init();
    }

    // Add to Wishlist and Add to Gift Registry links behaviors
    $pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
        var data = util.getQueryStringParams($('.pdpForm').serialize()),
            url;
        if (data.cartAction) {
            delete data.cartAction;
        }
        url = util.appendParamsToUrl(this.href, data);
        this.setAttribute('href', url);
    });

    // product options
    $pdpMain.on('change', '.product-options select', function () {
        var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales'),
            selectedItem = $(this).children().filter(':selected').first();
        salesPrice.text(selectedItem.data('combined'));
    });

    // prevent default behavior of thumbnail link and add this Button
    $pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
        e.preventDefault();
    });

    //add cgid on clicking of the full details button in quickview
    $pdpMain.on('click', 'a.link-details', function () {
        var cgid = $('#quickViewCgid').val(),
            isSearch = $('#quickViewIsSearch').val();
        if (cgid) {
            $(this)[0].hash = 'cgid=' + encodeURIComponent(cgid);
        }
        if (isSearch) {
            if (cgid) {
                $(this)[0].hash += '&issearch=true';
            } else {
                $(this)[0].hash = 'issearch=true';
            }
        }
    });

    $('.size-chart-link a').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });

    // Scrolls to #scroll_to_product_details, but before it calculates the height of sticky header on PDP,
    // so it is not hiding the information.
    $pdpMain.on('click', '#product_details_scroll', function (e) {
        e.preventDefault();
        var bodyRect = document.body.getBoundingClientRect(),
            elemRect = document.getElementById('scroll_to_product_details').getBoundingClientRect(),
            headerHeight = document.getElementById('pdpsticky').getBoundingClientRect().height,
            offset = elemRect.top - bodyRect.top - headerHeight;

        window.scrollTo(0, offset);

    });
    $pdpMain.on('click', '#video-accordion .video-title', function () {
        var $moreless = $('#video-accordion .video-title .more-less');
        $('#video-accordion .product-video-wrapper').slideToggle();

        if ($moreless.hasClass('fa-minus')) {
            $moreless.removeClass('fa-minus');
            $moreless.addClass('fa-plus');
        } else {
            $moreless.removeClass('fa-plus');
            $moreless.addClass('fa-minus');
        }
    });
    var quickview = require('../../quickview');
	quickview.init();
}

var product = {
    initializeEvents: initializeEvents,
    init: function () {
        initializeDom();
        initializeEvents();
        stickyheader.init();
        gtmDatalayer.pushDataToDataLayerForPDP();
    }
};

module.exports = product;
