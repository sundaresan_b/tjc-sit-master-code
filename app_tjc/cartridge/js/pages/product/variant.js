'use strict';

var ajax = require('../../ajax'),
    image = require('./image'),
    tabs = require('../../tabspdp'),
    pdpoverlay = require('../../pdpoverlay'),
    pdpprint = require('../../pdpprint'),
    progress = require('../../progress'),
    tooltip = require('../../tooltip'),
    carousel = require('../../carousel'),
    util = require('../../util'),
    tjcplusactivation = require('../../tjcplusactivation'),
    swipe = require('./swipe'),
    newsignin = require('../../newsignin'),
    uniform = require('../../uniform'),
    newformstyle = require('../../new-form-style'),
    qbfornonlivetv = require('../../qbfornonlivetv');


var initAddthisComponentAfterAjax = function () {
    if (window.addthis) {
        window.addthis = null;
    }
    $.getScript('https://s7.addthis.com/js/300/addthis_widget.js');
};


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href) {

    var $pdpMain = $('#pdpMain'),
    	$pdpForm = $('.pdpForm'),
        $pdpFlyIn = $('#pdpMain').find('.flyin .flyin-inner');
    
    if($('#QuickViewDialog').is(':visible')){
		$pdpMain = $('#QuickViewDialog .pdp-main');
		$pdpForm = $('#QuickViewDialog .pdpForm');
		$pdpFlyIn = $('#QuickViewDialog').find('.flyin .flyin-inner');
    }
    
    var qty = $pdpForm.find('input[name="Quantity"]').first().val(),
	    params = {
	        Quantity: isNaN(qty) ? '1' : qty,
	        format: 'ajax',
	        productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	    };

    progress.show($pdpMain);

    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $pdpMain.find('#product-content-wrapper'),
        callback: function () {
            initAddthisComponentAfterAjax();
            image.loadZoom();
            image.loadImageComponents();
            pdpoverlay.init();
            pdpprint.init();
            qbfornonlivetv.init();
            tabs.init();
            tooltip.init();
            carousel.init();
            swipe.init();
            wishListAJAXEvent();
            ColorNameChangeOnHover();
            $pdpMain.find('.flyin .flyin-inner').replaceWith($pdpFlyIn);            
            util.executeJqueryReadyCallbacks();
            $pdpMain.find('.pdp-product-quantity').val("1");
            tjcplusactivation.init();
            uniform();           
            util.imageAdjust();
            setTimeout(function(){
            	util.imageAdjust();
			},5000);
            var addToCart = require('./addToCart');
            addToCart();
        }
    });
    
    $(document).ajaxComplete(function(){
    	newformstyle.init();
    	progress.hide();
        if($('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper.variant-product:visible').length >= 1){
            $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('variant-product');
        }
    });

    return true;
};

function wishListAJAXEvent(){
	if ($('.pt_product-search-result').length > 0) {
		var $pdpMain = $('#search-result-items');
	} else {
		var $pdpMain = $('.pdp-main');
    }
    if($('#QuickViewDialog').is(':visible')){
		$pdpMain = $('#QuickViewDialog .pdp-main');
    }
	$pdpMain.off('click.wishlist').on('click.wishlist', '.product-wishlist-link, .product-slider-wishlist-link', function (e) {
    	e.preventDefault();
    	if($(this).hasClass('login-class')){
    		console.warn('redirect to login page....');
    		window.location.href = $(this).attr('href');
    	}else{
    		wishListAJAX(this);
    	}
    });
    newsignin.init();
}
function wishListAJAX($this){
	var url = $($this).attr('href'),
		$this = $($this),
		processClass = "";
	url = util.appendParamToURL(url,'isNewPDP', true);
	if($($this).hasClass('added-to-wishlist')){
		processClass = 'processing reverse';
	}else{
		processClass = 'processing normal';
	}
	//$($this).addClass(processClass);
	var start_time = new Date().getTime();
	
	$.ajax({
		url: url,
	    type: 'GET',
	    success: function (response) {
	    	var responseJSON = JSON.parse(response);
	    	if(responseJSON.success){
				$($this).attr('href', responseJSON.wishListURL);
				$($this).attr('class', responseJSON.wishListClass);
				$($this).find('i').attr('class', responseJSON.wishListSymbolClass);
				wishListAJAXEvent();
	    		/*var request_time = new Date().getTime() - start_time,
	    			remaining_time = 1500 - request_time;
	    		if(request_time >= 1500){
	    			$($this).removeClass(processClass);
					$($this).attr('href', responseJSON.wishListURL);
					$($this).attr('class', responseJSON.wishListClass);
					$($this).find('i').attr('class', responseJSON.wishListSymbolClass);
	    		}else{
	    			setTimeout(function(){
	    				$($this).removeClass(processClass);
						$($this).attr('href', responseJSON.wishListURL);
						$($this).attr('class', responseJSON.wishListClass);
						$($this).find('i').attr('class', responseJSON.wishListSymbolClass);
	    			}, remaining_time);
	    		}*/
	    	}
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	console.warn(jqXHR + textStatus + errorThrown);
	    	//$($this).removeClass(processClass);
	    }
	});
}

function init() {
	newsignin.init();
	var $pdpMain = $('.pdp-main');
	ColorNameChangeOnHover();
	if($('#QuickViewDialog').is(':visible')){
		$pdpMain = $('#QuickViewDialog .pdp-main');
    }
	
	// hover on swatch - should update main image with swatch image
    $pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
        var largeImg = $(this).data('lgimg'),
            $imgZoom = $pdpMain.find('.main-image'),
            $mainImage = $pdpMain.find('.primary-image');

        if (!largeImg) {
            return;
        }
        // store the old data from main image for mouseleave handler
        $(this).data('lgimg', {
            hires: $imgZoom.attr('href'),
            url: $mainImage.attr('src'),
            alt: $mainImage.attr('alt'),
            title: $mainImage.attr('title')
        });
        // set the main image
        image.setMainImage(largeImg);
    });
    
    // click on swatch - should replace product content with new variant
    $pdpMain.on('click', '.pdp-redesign-colsize .swatchanchor:not(.sticky-variant-anchor):visible', function (e) {
        e.preventDefault();
        if ($(this).parents('li').hasClass('unselectable') || $(this).parents('li').hasClass('selected')) {
            return;
        }
        updateContent(this.href);
    });

    // change drop down variation attribute - should replace product content with new variant
    $pdpMain.on('change', '.variation-select', function () {
        if ($(this).val().length === 0) {
            return;
        }
        updateContent($(this).val());
    });
    
    $pdpMain.off('click.wishlist').on('click.wishlist', '.product-wishlist-link, .product-slider-wishlist-link', function (e) {
    	e.preventDefault();
    	if($(this).hasClass('login-wish-class')){
    		console.warn('redirect to login page....');
    		window.location.href = $(this).attr('href');
    	}else{
    		wishListAJAX(this);
    	}
    });
}

function updateContentExt(href){
    return updateContent(href);
}


function ColorNameChangeOnHover(){
if($('.swatches.color').length > 0){  
   var selectedvariant = $('.color li.selected .swatchanchor').attr("data-lgimg");
	 var obj = JSON.parse(selectedvariant);  
	$('.color-name span').text(obj.color);

	  $(".swatchanchor").hover(function(){
	    var alttext = $(this).attr("data-lgimg"); 
	    var obj = JSON.parse(alttext);
	    $('.color-name span').text(obj.color);
	     
	});
	
	$('.swatchanchor').mouseleave(function(){  
	var selectedvariant = $('.color li.selected .swatchanchor').attr("data-lgimg");
	 var obj = JSON.parse(selectedvariant);  
	$('.color-name span').text(obj.color);
	});  
  }
}

// Module public functions
module.exports = {
    init: init,
    updateContent: updateContentExt,
    wishListAJAXEvent: wishListAJAXEvent,
};
