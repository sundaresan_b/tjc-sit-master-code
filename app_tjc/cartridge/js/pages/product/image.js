'use strict';

var swipe = require('./swipe');

/**
 * @description Enables the zoom viewer on the product detail page
 */
var loadZoom = function (imageURL) {
	var pageContextType = pageContext.type;
	if(pageContextType == "product"){
	    var $imgZoom = $('#pdpMainOverlay .overlay-item:visible'),
	        hiresURL;
	    
	    if (!$imgZoom.length) {
	        return;
	    }
	    if (imageURL) {
	        hiresURL = imageURL;
	    } else {
	        // Get Zoom Image URL
	        hiresURL = $('.productthumbnail.selected').data('zoom-image-url');
	    }
	    
	    // Init Zoom View
		 var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign'); 
	     if(newPDPFlag == false || newPDPFlag == "false"){
	    if (hiresURL && hiresURL !== 'null' && hiresURL.indexOf('noimagelarge') === -1) {
	        $imgZoom.zoom({
	            url: hiresURL
	        });
	    }
	  }
	}
};


/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
var setMainImage = function (imageURL, zoomImageURL) {
	var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign');
	var $container;
	if(newPDPFlag == true || newPDPFlag == "true"){
		$container = $('#pdpMain .product-primary-image .single-view .pdp-image-non_mobile img');
	}else{
		$container = $('#pdpMain .product-primary-image .single-view img');
	}
	$container.css('display', '');

	// check for url
    try {
	    if(typeof imageURL === "object"){
	    	return false;
	    }else if(imageURL.indexOf('http') < 0){
	    	return false;
	    }
    } catch (b) {
    	console.log(b);
    	return false;
    }
    
    // replace main image
    $container.attr({
        src: imageURL
    });

    var pageContextType = pageContext.type;
	if(pageContextType == "product"){
	    // re-init zoom view
	    $('#pdpMain .product-primary-image .single-view .main-image').attr({
	        href: zoomImageURL
	    });
	    loadZoom();
	}

    // hide video
    $('#pdpMain .product-primary-image .single-view .product-video-wrapper').removeClass('visible');
};

var loadImageComponents = function () {
    // Init Thumbnails
    $('#pdpMain').on('click mouseover',  '.productthumbnail:not(.video)', function (e) {
    	e.preventDefault();
    	if($(this).hasClass('dont-act')){
    		swipe.swipeSlideTo('pdpMain', $('.pdp_redesign #pdpMain li.productthumbnail').index(this));
    	}
        $(this).closest('.product-thumbnails').find('.productthumbnail.selected').removeClass('selected');
        $(this).addClass('selected');
        $('.video-text').removeClass('active');
        
        setMainImage($(this).data('main-image-url'), $(this).data('zoom-image-url'));
        $('div.magnifier, div.cursorshade, div.tracker').removeClass('visually-hidden');
        
        // show image
        $('#pdpMain .product-primary-image .single-view img').css('visibility','visible');
        
        // video effects disable
        var $playButton = $('#pdpMain .product-primary-image .single-view .playButton.mainButton');
        $playButton.removeClass('livetvvideo').removeClass('360video');
        $playButton.hide();
        //$('#pdpMain .product-primary-image .single-view .product-image').css({'filter':'unset','-webkit-filter':'unset'});
        $('.pdp_redesign .product-primary-image .single-view .pdp-image-non_mobile img').css('pointer-events','auto');
    }).on('click mouseover', '.productthumbnail.video', function (e) {
    	e.preventDefault();
    	if($(this).hasClass('dont-act')){
    		swipe.swipeSlideTo('pdpMain', $('.pdp_redesign #pdpMain li.productthumbnail').index(this));
    	}
        $(this).closest('.product-thumbnails').find('.productthumbnail.selected').removeClass('selected');
        $(this).addClass('selected');
        $('.video-text').removeClass('active');
        
        setMainImage($(this).data('zoom-image-url'), $(this).data('zoom-image-url'));
        $('div.magnifier, div.cursorshade, div.tracker').addClass('visually-hidden');
        
        // video effects enable
        var $playButton = $('#pdpMain .product-primary-image .single-view .playButton.mainButton');
        if($(this).hasClass('360video')){
        	$playButton.removeClass('livetvvideo').addClass('360video');
        }else if($(this).hasClass('livetvvideo')){
        	$playButton.removeClass('360video').addClass('livetvvideo');
        }
        $playButton.show();
        //$('#pdpMain .product-primary-image .single-view .product-image').css({'filter':'blur(1.5px)','-webkit-filter':'blur(1.5px)'});
        $('.pdp_redesign .product-primary-image .single-view .pdp-image-non_mobile img').css('pointer-events','none');
    });

    $('#pdpMain').on('click', '.primary-col .icons-product-image', function () {
        loadZoom();
    });

    $('#pdpMain').on('click', '.product-primary-image .single-view .nav-prev.button', function () {
        var thumbnails = $('#pdpMain .product-thumbnails'),
            selectedThumbnail = thumbnails.find('.productthumbnail.selected'),
            previousThumbnail = selectedThumbnail.prev(),
            isFirstThumbnail = thumbnails.find('.productthumbnail').first().hasClass('selected');

        if (isFirstThumbnail) {
            previousThumbnail = thumbnails.find('.productthumbnail').last();
            // bubbles are not visible but needed as a workaround for scrolling
            thumbnails.find('.bubble-ctr .bubble').last().click();
        } else {
            $('#pdpMain .product-thumbnails .nav-prev').click();
        }

        previousThumbnail.click();
    });

    $('#pdpMain').on('click', '.product-primary-image .single-view .nav-next.button', function () {
        var thumbnails = $('#pdpMain .product-thumbnails'),
            selectedThumbnail = thumbnails.find('.productthumbnail.selected'),
            nextThumbnail = selectedThumbnail.next(),
            isLastThumbnail = thumbnails.find('.productthumbnail').last().hasClass('selected');

        if (isLastThumbnail) {
            nextThumbnail = thumbnails.find('.productthumbnail').first();
            // bubbles are not visible but needed as a workaround for scrolling
            thumbnails.find('.bubble-ctr .bubble').first().click();
        } else {
            $('#pdpMain .product-thumbnails .nav-next').click();
        }

        nextThumbnail.click();
    });

    $('.pdp-thumbnail-slider').not('.slick-initialized').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        infinite: false,
        vertical: true,
        arrows: true,
        dots: true
    });
    
    $('.pdp-mainimage-prev-btn').on('click', function() {
    	var imageSliderLength = $('.product-thumbnails.pdp-thumb-tiles.newtemplate .pdp-thumbnail-container .pdp-thumbnail-slider .slick-list .slick-track .slick-slide').length;
        var i = parseInt($(".productthumbnail.selected").closest(".slick-slide").attr("data-slick-index")) - 1;
        if(i < 0){
        	i = imageSliderLength-1;
        }
        if($(".productthumbnail.selected").closest(".slick-slide").is(':first-child') && window.matchMedia("(min-width: 768px)").matches){
        	$('.product-thumbnails.pdp-thumb-tiles.newtemplate .pdp-thumbnail-container .pdp-thumbnail-slider .slick-list .slick-track .slick-slide:last').prependTo(".product-thumbnails.pdp-thumb-tiles.newtemplate .pdp-thumbnail-container .pdp-thumbnail-slider .slick-list .slick-track");
        }
        $('.slick-slide[data-slick-index='+ i +']').find('.productthumbnail').trigger('click');
        $('.product-thumbnails').find('.slick-prev').trigger('click');
   });

   $('.pdp-mainimage-next-btn').on('click', function() {
	   var imageSliderLength = $('.product-thumbnails.pdp-thumb-tiles.newtemplate .pdp-thumbnail-container .pdp-thumbnail-slider .slick-list .slick-track .slick-slide').length;
	   var i = parseInt($(".productthumbnail.selected").closest(".slick-slide").attr("data-slick-index")) + 1;
	   if(i >= imageSliderLength){
		   i = 0;
	   }
	   if($(".productthumbnail.selected").closest(".slick-slide").is(':last-child') && window.matchMedia("(min-width: 768px)").matches){
		   $('.product-thumbnails.pdp-thumb-tiles.newtemplate .pdp-thumbnail-container .pdp-thumbnail-slider .slick-list .slick-track .slick-slide:first').appendTo(".product-thumbnails.pdp-thumb-tiles.newtemplate .pdp-thumbnail-container .pdp-thumbnail-slider .slick-list .slick-track");
	   }
       $('.slick-slide[data-slick-index='+ i +']').find('.productthumbnail').trigger('click');
	   $('.product-thumbnails').find('.slick-next').trigger('click');
   });
   
	$('.pdp-thumbnail-container .livetvvideo').on('click',function(){
		if (window.matchMedia("(min-width: 767.5px)").matches){
			$('.livetvvideo .playButtonSymbol').trigger('click'); 
		}
	});
	$('.pdp-thumbnail-container .360video').on('click',function(){
		if (window.matchMedia("(min-width: 767.5px)").matches){
			$('.360video .playButtonSymbol').trigger('click');
		}
	});

   var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign'); 
   if(newPDPFlag == true || newPDPFlag == "true"){
	   $('.pdp_redesign div:not(.ui-dialog-content) .pdp-main').find('.single-view .pdp-main-img').imagezoomsl({ zoomrange: [1.5, 1.5] }); /*2x zoom*/
   }

};



/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
    // Load Zoom View
    loadZoom();
    loadImageComponents();
};

module.exports.loadZoom = loadZoom;
module.exports.loadImageComponents = loadImageComponents;
module.exports.setMainImage = setMainImage;
