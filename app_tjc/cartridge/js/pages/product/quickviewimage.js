'use strict';

var swipe = require('./swipe');

/**
 * @description Enables the zoom viewer on the product detail page
 */
var loadZoom = function (imageURL) {
	var pageContextType = pageContext.type;
	if(pageContextType == "product"){
	    var $imgZoom = $('#QuickViewDialogOverlay .overlay-item:visible'),
	        hiresURL;
	    
	    if (!$imgZoom.length) {
	        return;
	    }
	    if (imageURL) {
	        hiresURL = imageURL;
	    } else {
	        // Get Zoom Image URL
	        hiresURL = $('.productthumbnail.selected').data('zoom-image-url');
	    }
	    
	    // Init Zoom View
	    if (hiresURL && hiresURL !== 'null' && hiresURL.indexOf('noimagelarge') === -1) {
	        $imgZoom.zoom({
	            url: hiresURL
	        });
	    }
	}
};


/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
var setMainImage = function (imageURL, zoomImageURL) {
    $('#QuickViewDialog .product-primary-image .single-view img').css('display', '');

	// check for url
    try {
	    if(typeof imageURL === "object"){
	    	return false;
	    }else if(imageURL.indexOf('http') < 0){
	    	return false;
	    }
    } catch (b) {
    	console.log(b);
    	return false;
    }
    
    // replace main image
    $('#QuickViewDialog .product-primary-image .single-view img').attr({
        src: imageURL
    });

    var pageContextType = pageContext.type;
	if(pageContextType == "product"){
	    // re-init zoom view
	    $('#QuickViewDialog .product-primary-image .single-view .main-image').attr({
	        href: zoomImageURL
	    });
	    loadZoom();
	}

    // hide video
    $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper').removeClass('visible');
};

var loadImageComponents = function () {
    // Init Thumbnails
	
	
    $('#QuickViewDialog').on('click', '.productthumbnail:not(.video)', function (e) {
    	e.preventDefault();
        $(this).closest('.product-thumbnails').find('.productthumbnail.selected').removeClass('selected');
        $(this).addClass('selected');
        $('.video-text').removeClass('active');
        
        setMainImage($(this).data('main-image-url'), $(this).data('zoom-image-url'));
        
        // show image
        $('#QuickViewDialog .product-primary-image .single-view img').css('visibility','visible');
        
        // hide video
        $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper').css('visibility','hidden');
        $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper video').trigger('pause');
        $('#QuickViewDialog .product-primary-image .single-view .playButton').hide();
    }).on('click', '.productthumbnail.video', function (e) {
    	e.preventDefault();
        $(this).closest('.product-thumbnails').find('.productthumbnail.selected').removeClass('selected');
        $(this).addClass('selected');

        // hide image
        $('#QuickViewDialog .product-primary-image .single-view img').css('visibility','hidden');
        
        // show video
        if($(this).hasClass('360video')){
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .360video video').trigger('pause');
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .livetvvideo video').trigger('pause');
        	$('#QuickViewDialog .product-primary-image .single-view .playButton').show();
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .360video').show();
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .livetvvideo').hide();
        }else if($(this).hasClass('livetvvideo')){
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .360video video').trigger('pause');
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .livetvvideo video').trigger('pause');
        	$('#QuickViewDialog .product-primary-image .single-view .playButton').show();
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .360video').hide();
        	$('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .livetvvideo').show();
        	$('.video-text').addClass('active');
        }
        $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper').css('visibility','visible');
        $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper .overlayvideo-container1 video').off('play').on('play',function(){
        	$('#QuickViewDialog .product-primary-image .single-view .playButton').hide();
        }).off('pause').on('pause',function(){
        	var visibility = $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper').css('visibility');
        	if(visibility == "visible"){
        		$('#QuickViewDialog .product-primary-image .single-view .playButton').show();
        	}
        	setTimeout(function(){
				visibility = $('#QuickViewDialog .product-primary-image .single-view .product-video-wrapper').css('visibility');
				if(visibility == "visible"){
					//$('#QuickViewDialog .product-primary-image .single-view .playButton').show();
				}else{
					$('#QuickViewDialog .product-primary-image .single-view .playButton').hide();
				}
        	}, 3000);
        });
        $('#QuickViewDialog .product-primary-image .single-view .playButton').off('click').on('click',function(){
        	$(this).parents('.single-view').find('.product-video-wrapper .overlayvideo-container1 video:visible').get(0).play();
        	$('#QuickViewDialog .product-primary-image .single-view .playButton').hide();
        });
    });

    $('#QuickViewDialog').on('click', '.primary-col .icons-product-image', function () {
        loadZoom();
    });

    $('#QuickViewDialog').on('click', '.product-primary-image .single-view .nav-prev.button', function () {
        var thumbnails = $('#QuickViewDialog .product-thumbnails'),
            selectedThumbnail = thumbnails.find('.productthumbnail.selected'),
            previousThumbnail = selectedThumbnail.prev(),
            isFirstThumbnail = thumbnails.find('.productthumbnail').first().hasClass('selected');

        if (isFirstThumbnail) {
            previousThumbnail = thumbnails.find('.productthumbnail').last();
            // bubbles are not visible but needed as a workaround for scrolling
            thumbnails.find('.bubble-ctr .bubble').last().click();
        } else {
            $('#QuickViewDialog .product-thumbnails .nav-prev').click();
        }

        previousThumbnail.click();
    });

    $('#QuickViewDialog').on('click', '.product-primary-image .single-view .nav-next.button', function () {
        var thumbnails = $('#QuickViewDialog .product-thumbnails'),
            selectedThumbnail = thumbnails.find('.productthumbnail.selected'),
            nextThumbnail = selectedThumbnail.next(),
            isLastThumbnail = thumbnails.find('.productthumbnail').last().hasClass('selected');

        if (isLastThumbnail) {
            nextThumbnail = thumbnails.find('.productthumbnail').first();
            // bubbles are not visible but needed as a workaround for scrolling
            thumbnails.find('.bubble-ctr .bubble').first().click();
        } else {
            $('#QuickViewDialog .product-thumbnails .nav-next').click();
        }

        nextThumbnail.click();
    });

    $('#QuickViewDialog .pdp-mainimage-prev-btn').on('click', function() {
        var i = parseInt($("#QuickViewDialog .productthumbnail.selected").closest(".slick-slide").attr("data-slick-index")) - 1;
        $('#QuickViewDialog .slick-slide[data-slick-index='+ i +']').find('.productthumbnail').trigger('click');
        $('#QuickViewDialog .product-thumbnails').find('.slick-prev').trigger('click');
   });

   $('#QuickViewDialog .pdp-mainimage-next-btn').on('click', function() {
	   var i = parseInt($("#QuickViewDialog .productthumbnail.selected").closest(".slick-slide").attr("data-slick-index")) + 1;
	   $('#QuickViewDialog .slick-slide[data-slick-index='+ i +']').find('.productthumbnail').trigger('click');
	   $('#QuickViewDialog .product-thumbnails').find('.slick-next').trigger('click');
   });

};



/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
    // Load Zoom View
    loadZoom();
    loadImageComponents();
    swipe.init();

};

module.exports.loadZoom = loadZoom;
module.exports.loadImageComponents = loadImageComponents;
module.exports.setMainImage = setMainImage;
