'use strict';

var swiperPDPMain,
    swiperPDPOverlay,
    pdpInitFlag;

function init(section){
	if(section == 'pdpMain'){
		if (window.matchMedia("(max-width: 767.5px)").matches){
			$('.pdp_redesign .product-primary-image .single-view .swiper-container .swiper-wrapper').empty();
			if($('.pdp_redesign #pdpMain li.productthumbnail').length > 0){
				$('.pdp_redesign #pdpMain li.productthumbnail').each(function(index, value){
					var imageURI = $(value).data('zoom-image-url');
					if(imageURI!="" && typeof(imageURI)!='undefined'){
						var appendElementParentClass = 'swiper-slide';
						var additionalElement = '';
						if($(this).hasClass('360video')){
							appendElementParentClass += ' 360video';
							additionalElement = '<span class="playButton 360video"><span class="playButtonBackground"></span><span class="playButtonSymbol"></span></span>';
						}else if($(this).hasClass('livetvvideo')){
							appendElementParentClass += ' livetvvideo';
							additionalElement = '<span class="playButton livetvvideo"><span class="playButtonBackground"></span><span class="playButtonSymbol"></span></span>';
						}
						var appendElement = '<div class="'+appendElementParentClass+'">'+additionalElement+'<img class="clickable-image pdp-main-img" src="'+imageURI+'" data-zoom="'+imageURI+'"/></div>';
						$('.pdp_redesign .product-primary-image .single-view .swiper-container .swiper-wrapper').append(appendElement);
					}
				});
			}else{
				var imageURI = $('.pdp_redesign #pdpMain .pdp-image-non_mobile img.pdp-main-img').data('zoom');
				var appendElementParentClass = 'swiper-slide';
				var appendElement = '<div class="'+appendElementParentClass+' single-image"><img class="clickable-image pdp-main-img" src="'+imageURI+'" data-zoom="'+imageURI+'"/></div>';
				$('.pdp_redesign .product-primary-image .single-view .swiper-container .swiper-wrapper').append(appendElement);
			}
			$('.pdp_redesign .product-primary-image .single-view .swiper-slide img').css({'max-width':($('.single-view').width())+'px','margin-left':'0'});
			$('#pdpMain .product-primary-image .single-view .playButton.mainButton').css('visibility','hidden');
			$('.product-primary-image.newtemplate .single-view .pdp-image-non_mobile').hide();
			$('.product-primary-image.newtemplate .single-view .pdp-image-mobile').show();
			$('.pdp_redesign #pdpMain li.productthumbnail').addClass('dont-act');
			pdpInitFlag = 0;
			setTimeout(function(){
				swiperPDPMain = new Swiper(".pdp-main-swiper", {
					slidesPerView: 1,
					spaceBetween: 0,
					mousewheel: true,
					observer: true,
					observeParents: true,
		            preloadImages: true,
					loop: false,
					keyboard: {
		                enabled: true,
		                onlyInViewport: false
		            },
		            on:{
		            	resize: function(){
		            		if (window.matchMedia("(max-width: 767.5px)").matches){
		            			$('.pdp_redesign #pdpMain li.productthumbnail').addClass('dont-act');
		            			$('.product-primary-image.newtemplate .single-view .pdp-image-mobile').show();
		            			$('.product-primary-image.newtemplate .single-view .pdp-image-non_mobile').hide();
		            			$('#pdpMain .product-primary-image .single-view .playButton.mainButton').css('visibility','hidden');
			            		$('.pdp_redesign .product-primary-image .single-view .swiper-slide img').css({'max-width':($('.single-view').width())+'px','margin-left':'0'});
			            		swiperPDPMain.update();
			            		$('.pdp_redesign #pdpMain li.productthumbnail.selected').trigger('click');
			            		console.log('swipe slider update');
		            		}else{
		            			$('.pdp_redesign #pdpMain li.productthumbnail').removeClass('dont-act');
		            			$('.product-primary-image.newtemplate .single-view .pdp-image-mobile').hide();
		            			$('.product-primary-image.newtemplate .single-view .pdp-image-non_mobile').show();
		            			$('#pdpMain .product-primary-image .single-view .playButton.mainButton').css('visibility','visible');
		            		}
		            	},
		            	slideChange: function () {
		            		if(!$('.pdp_redesign #pdpMain li.productthumbnail').eq(swiperPDPMain.realIndex).hasClass('selected')){
		            			$('.pdp_redesign #pdpMain li.productthumbnail').eq(swiperPDPMain.realIndex).trigger('click');
		            		}
		    			},
		    			sliderMove: function() {
		    				if(pdpInitFlag < 100){
			    				$('.pdp_redesign .product-primary-image .single-view .swiper-slide img').css({'max-width':($('.single-view').width())+'px','margin-left':'0'});
			            		swiperPDPMain.update();
			            		pdpInitFlag++;
		    				}
		    			}
		            }
				});
				console.log(swiperPDPMain);
			}, 3000);
		}else{
			$('.pdp_redesign #pdpMain li.productthumbnail').removeClass('dont-act');
			$('.product-primary-image.newtemplate .single-view .pdp-image-mobile').hide();
			$('.product-primary-image.newtemplate .single-view .pdp-image-non_mobile').show();
			$('#pdpMain .product-primary-image .single-view .playButton.mainButton').css('visibility','visible');
			$('.pdp_redesign .product-primary-image .single-view .swiper-container .swiper-wrapper').empty();
		}
	}else if(section == 'pdpOverlay'){
		$('.swiper-container.pdp-overlay-swiper').css('width',$('.single-view').width()+'px');
		$('.pdp-overlay-swiper .swiper-wrapper').css('display','flex');
		$('.pdp-overlay-swiper .swiper-slide').css('display','flex');
		$('.pdp-overlay-swiper .swiper-slide img').css({'max-width':($('.single-view').width())+'px','margin-left':'0'});
		pdpInitFlag = 0;
		setTimeout(function(){
			swiperPDPOverlay = new Swiper(".pdp-overlay-swiper", {
				slidesPerView: 1,
				spaceBetween: 0,
				mousewheel: false,
				loop: false,
				keyboard: {
	                enabled: false,
	                onlyInViewport: false
	            },
	            on:{
	            	resize: function(){
	            		$('.pdp-overlay-swiper .swiper-wrapper').css('display','flex');
	            		$('.pdp-overlay-swiper .swiper-slide').css('display','flex');
	            		$('.pdp-overlay-swiper .swiper-slide img').css({'max-width':($('.single-view').width())+'px','margin-left':'0'});
	            		swiperPDPOverlay.update();
	            		console.log('swipe slider update');
	            	},
	            	slideChange: function () {
	            		//$('.swiper-container.pdp-overlay-swiper .swiper-wrapper .swiper-slide.selected').removeClass('selected');
	    				//$('.swiper-container.pdp-overlay-swiper .swiper-wrapper .swiper-slide').eq(swiperPDPOverlay.realIndex).addClass('selected');
	            		if(!$('#pdpMainOverlay li.productthumbnail').eq(swiperPDPOverlay.realIndex).hasClass('selected')){
	            			$('#pdpMainOverlay li.productthumbnail').eq(swiperPDPOverlay.realIndex).trigger('click');
	            		}
	    			},
	    			sliderMove: function() {
	    				if(pdpInitFlag < 100){
		    				$('.pdp_redesign .product-primary-image .single-view .swiper-slide img').css({'max-width':($('.single-view').width())+'px','margin-left':'0'});
		    				swiperPDPOverlay.update();
		            		pdpInitFlag++;
	    				}
	    			}
	            },
	            zoom: {
	            	minRatio: 1,
	            	maxRatio: 2
				}
			});
			console.log(swiperPDPOverlay);
			$('#pdpMainOverlay li.productthumbnail.selected').trigger('click');
		}, 500);
	}
}

function swipeSlideTo(section, index){
	if(section == 'pdpMain'){
		swiperPDPMain.slideTo(index, 1000, false);
	}else if(section == 'pdpOverlay'){
		swiperPDPOverlay.slideTo(index, 1000, false);
	}
}

module.exports = {
    init: function (section) {
    	var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign');
    	if(newPDPFlag == true || newPDPFlag == "true"){
    		init(section);
    	}
    },
    swipeSlideTo: function(section, index){
    	var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign');
    	if(newPDPFlag == true || newPDPFlag == "true"){
    		swipeSlideTo(section, index);
    	}
    }
};
