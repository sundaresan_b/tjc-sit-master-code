'use strict';

var ajax = require('./ajax'),
    util = require('./util'),
    progress = require('./progress');

var addressFields;

function fillAddressFields($fields, data) {
    /*jslint unparam: true */
    $.each(['address1', 'address2', 'address3', 'address4', 'locality', 'city', 'postCode'], function (i, attribute) {
        $fields.find('input[name$=' + attribute + ']').val(data[attribute]);
    });
    /*jslint unparam: false */
}

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Array} addresses An array of addresses
 * @param {jQueryObject} $select the select field to alter
 */
function addAddressesOptions(addresses, $select) {
    $select
        .val('')
        .click()
        .find('option[value!=""]')
        .remove();
    $.each(addresses, function (i, address) {
        $select.append($('<option/>', {
            value: i,
            data: {
                address1: address.address1,
                address2: address.address2,
                address3: address.address3,
                address4: address.address4,
                city: address.town
            }
        }).text([address.address1, address.address2, address.town].join(', ')));
    });
}


function tooglePostalcodeSearchVisibility(countrySetting,
        $postCodeFormRow, $findMyAddressButton,
        $additionalAddresses, $addressesSelectFormRow, $addressesGroup) {

    if (countrySetting.addressCheckEnabled) {
        $postCodeFormRow.show();
        $findMyAddressButton.show();
        $additionalAddresses.addClass('visually-hidden');
        $additionalAddresses.removeClass('visually-un-hidden');
        $addressesSelectFormRow.removeClass('visually-hidden');
        $additionalAddresses.find('.enable-find-my-address').show();            
    } else {
        $additionalAddresses.removeClass('visually-hidden');
        if (countrySetting.postalCodeRequired) {
            $postCodeFormRow.hide();
            $findMyAddressButton.hide();
            $additionalAddresses.find('.enable-find-my-address').hide();           
        } else {
            $postCodeFormRow.hide();
        }
        $addressesSelectFormRow.addClass('visually-hidden');
    }
    
    $addressesGroup.find('.selected-address-summary').addClass('visually-hidden');
    $addressesGroup.find('h5').addClass('visually-hidden');
    $addressesGroup.find('.selected-address-summary span').empty();
    $addressesGroup.find('.addresses-select').removeClass('loqate-select');
    
    if($('.opcAddressListModelPopup').length > 0) {
		$('.xlt-addressForm').removeClass('not-scroll');
	}
	
	$('.additional-address-fields input[id^="dwfrm_address"], .additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').on('keyup keydown change', function(){
		validateAdditionalAddressFiedinPopup(this);
	});

}

function updateBillingAddress() {
	if($('#use-billing-address').is(":checked")) {
		var addressid = $('.additional-address-fields').find("input[id$='address_addressid']").val();
		var addressidhidden = $('#dwfrm_quickbuyaddress_shippingAddress_addressid').val();
		var address1 = $('.additional-address-fields').find("input[id$='address_address1']").val();
    	var address2 = $('.additional-address-fields').find("input[id$='address_address2']").val();
    	var address3 = $('.additional-address-fields').find("input[id$='address_address3']").val();
    	var address4 = $('.additional-address-fields').find("input[id$='address_address4']").val();
    	var city = $('.additional-address-fields').find("input[id$='address_city']").val();
    	var postValue = $('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').val();
    	var addressid = $('#dwfrm_quickbuyaddress_shippingAddress_addressid').val();
    	
    	var selectedCountry = $('#dwfrm_quickbuyaddress_shippingAddress_address_country option:selected').val();
    	var selectedCountryValue = $('#uniform-dwfrm_quickbuyaddress_shippingAddress_address_country span').html();
    	
    	var selectedAddress = $('#delivery-address #customerAddress option:selected').val();
    	var selectedAddressValue = $('#delivery-address #customerAddress option:selected').html();
    	
    	
    	
    	$('.customerAddressBilling').find("input[id$='address_addressid']").val(addressid);
    	$('.customerAddressBilling').find("input[id$='address_address1']").val(address1);
    	$('.customerAddressBilling').find("input[id$='address_address2']").val(address2);
    	$('.customerAddressBilling').find("input[id$='address_address3']").val(address3);
    	$('.customerAddressBilling').find("input[id$='address_address4']").val(address4);
    	$('.customerAddressBilling').find("input[id$='address_city']").val(city);
    	$('#dwfrm_quickbuyaddress_billingAddress_address_postCode').val(postValue);
    	$('#dwfrm_quickbuyaddress_billingAddress_addressid').val(addressid);
    	$('#dwfrm_quickbuyaddress_billingAddress_address_addressid').val(addressidhidden);
    	
    	$('#dwfrm_quickbuyaddress_billingAddress_address_country option[value=' + selectedCountry + ']').attr('selected','selected');
    	$('#uniform-dwfrm_quickbuyaddress_billingAddress_address_country span').html(selectedCountryValue);
    	
    	$('.customerAddressBilling #customerAddress option[value="' + selectedAddress + '"]').prop('selected', true);
    	$('.customerAddressBilling .address-selector span').html(selectedAddressValue);
	}
}

function defaultBillingAddress(){
	if($('.customerAddressBilling .address-selector option:selected').length){
    	var addressstring = $('.customerAddressBilling .address-selector option:selected').html();
    	var addressarray = addressstring.split(',');
    	
    	if(!$('.customerAddressBilling').hasClass('address-populated')){
    	var url = Urls.getUserAddress;
     	$.ajax({
 	        url: url + "?addressID=" + addressarray[0],
 	        type: 'GET',
 	        success: function (response) {
 	        	$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_addressid').val(response.id);
	        	$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_address_addressid').val(response.id);
	        	$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_address_address1').val(response.address1);
	        	if(response.address2 == "null" || response.address2 == ""){
	        		$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_address_address2').val('');
	        	}
	        	else{
	        		$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_address_address2').val(response.address2);
	        	}
	        	$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_address_city').val(response.city);
	        	$('.customerAddressBilling').find('#dwfrm_quickbuyaddress_billingAddress_address_postCode').val(response.postalCode);
	        	$('.customerAddressBilling').addClass('address-populated');
 	        }
     	});
    	}
    	
    	
	}
	
}

function defaultShippingSddress() {
	if($('.shipping-address-selector .address-selector option:selected').length){
	    var addressstring = $('.shipping-address-selector .address-selector option:selected').html();
	    var addressarray = addressstring.split(',');
	    
	    if(!$('.shippingform').hasClass('address-populated') || $('#dwfrm_quickbuyaddress_shippingAddress_addressid').val() == ""){
	    var url = Urls.getUserAddress;
    	$.ajax({
	        url: url + "?addressID=" + addressarray[0],
	        type: 'GET',
	        success: function (response) {
        		$('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_addressid').val(response.id);
        	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_addressid').val(response.id);
        	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_address1').val(response.address1);
        	    if(response.address2 == "null" || response.address2 == ""){
        	    	$('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_address2').val('');
	        	}
	        	else{
	        		$('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_address2').val(response.address2);
	        	}       	    
        	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_city').val(response.city);
        	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').val(response.postalCode);
        	    $('.shippingform').addClass('address-populated');
	        }
    	});
	    }
	    
	    
	   /* $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_addressid').val(addressarray[0]);
	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_addressid').val(addressarray[0]);
	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_address1').val(addressarray[1]);
	    //$('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_address2').val(addressarray[2]);
	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_city').val(addressarray[2]);
	    $('.shippingform').find('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').val(addressarray[4]);*/
	}
    
    updateBillingAddress();
    
}

function sameAsShippingAddress() {
	$("select[id$='shippingAddress_address_addresses'], #delivery-address #customerAddress").on('change', function(){
    	updateBillingAddress();
    });
    
    $('.shippingform input').on('keyup', function() {
    	var addressidhidden = $('#dwfrm_quickbuyaddress_shippingAddress_addressid').val();
        $('#dwfrm_quickbuyaddress_shippingAddress_address_addressid').val(addressidhidden);
        $('#dwfrm_quickbuyaddress_billingAddress_address_addressid').val(addressidhidden);
    	updateBillingAddress();
    });
    
    $('#use-billing-address').on('change', function(){
       	 if($(this).is(":checked")) {
       		 updateBillingAddress();
       		$('.checkout-billing-address .saved-address').addClass('hidden');
       	    $('.checkout-billing-address .add-billing-address').addClass('hidden');
       	    
       	 }
       	 else {
       		/* $('.customerAddressBilling .additional-address-fields input[type=text]').each(function() {
       		      this.value = "";
       		  });*/
       		$('.shippingform').removeClass('address-populated');
       		$('.customerAddressBilling').removeClass('address-populated');
       		defaultBillingAddress();
       		 $('.checkout-billing-address .saved-address').removeClass('hidden');
       	 // Auto fucts when text box having a value 
			$(".input-text").each(function () {  
			  if( $(this).val().length > 0 ) {
			        $(this).siblings(".label").addClass('focus-active');
			        $(this).closest('.form-row').addClass('validation-success');
			    } 
			                    
			 }) 
       		
       	 }
    });
    
    $('#dwfrm_quickbuyaddress_billingAddress_addressid').on('keyup', function() {
        var address1 = $(this).val();
        $('#dwfrm_quickbuyaddress_billingAddress_address_addressid').val(address1);
    });
}


/**
 * @function
 * @description Initializes the events on the address form
 * @param {Element} $form The form which will be initialized
 */
function initializeEvents($form) {
    $form.find('select[name$=country]').each(function () {
        var $countrySelect = $(this),
            $cointainer = $countrySelect.parents('.form-row').closest('.xlt-addressForm'),
            $postCodeFormRow = $cointainer.find('.post-code-field'),  
            $findMyAddressButton = $postCodeFormRow.find('button'),
            $postCodeInput = $postCodeFormRow.find('input'),
            $addressesSelectFormRow = $cointainer.find('.addresses-select'),
            $addressesGroup = $cointainer.find('.addresses-select-group'),
            $typeMyAddressButton = $addressesSelectFormRow.find('button'),
            $additionalAddresses = $cointainer.find('.additional-address-fields'),
            selectedCountry = $countrySelect.val(),
            countrySetting = util.getCountrySettings(selectedCountry);
        $findMyAddressButton.data('searchstarted', false);

        tooglePostalcodeSearchVisibility(countrySetting,
                $postCodeFormRow, $findMyAddressButton,
                $additionalAddresses, $addressesSelectFormRow, $addressesGroup);

        $countrySelect.on('change', function () {
            selectedCountry = $countrySelect.val();
            countrySetting = util.getCountrySettings(selectedCountry);
            $findMyAddressButton.data('searchstarted', false);

            tooglePostalcodeSearchVisibility(countrySetting,
                    $postCodeFormRow, $findMyAddressButton,
                    $additionalAddresses, $addressesSelectFormRow, $addressesGroup);
        });

        $findMyAddressButton.on('click', function () {
        	if($('.quickbuy-preference-page').length){
	        	var existingalias = $('#existing-alias').val();
	        	var newalias = $(this).closest('.country-postal').find("[id$='_addressid']").val().toLowerCase();
	        	$('#dwfrm_quickbuyaddress_shippingAddress_addressid-error').remove();
	        	if(newalias.length){
	        		if(existingalias.indexOf(newalias) > -1){
	        			$('#dwfrm_quickbuyaddress_shippingAddress_addressid-error').remove();
	        			var newDiv ='<div id="dwfrm_quickbuyaddress_shippingAddress_addressid-error" class="error-msg" style="display : block">You are already using this address book alais.</div>';
		    		    $(this).closest('.country-postal').find('.address-alias .field').append(newDiv);
		    		    $(this).closest('.country-postal').find('.address-alias').addClass('validation-error');
		    		    return;
	        		}
	        	} 
	        	else{
        			$('#dwfrm_quickbuyaddress_shippingAddress_addressid-error').remove();
        			var newDiv ='<label id="dwfrm_quickbuyaddress_shippingAddress_addressid-error" class="error" for="dwfrm_quickbuyaddress_shippingAddress_addressid">Cannot be blank</label>'
	    		    $(this).closest('.country-postal').find('.address-alias .field').append(newDiv);
	    		    $(this).closest('.country-postal').find('.address-alias').addClass('validation-error');
	    		    return;
	        	}
        	}
        	
        	$('.additional-address-fields input[id^="dwfrm_address"]').on('keyup keydown change', function(){
    			validateAdditionalAddressFiedinPopup(this);
    		});
        	
        	$(this).closest('.xlt-addressForm').find('.type-own-address').remove();
        	
        	/*Remove class for creating address in OPC delivery address Popup */
        	if($('#opcDeliveryAddresses').length > 0){
        		
        		var opcexistingalias = $('#existing-alias').val();
        		var opcnewalias = $("[id$='_addressid']").val().toLowerCase();
        		
        		$('.additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').on('keyup keydown change', function(){
        			validateAdditionalAddressFiedinPopup(this);
        		});		
        		
        		if(opcnewalias.length){
	        		if(opcexistingalias.indexOf(opcnewalias) > -1){
	        			$('#addressIDerror.error-msg').remove();
	        			$('.addAddressOpc div').first().addClass('validation-error');
	        			var newDiv ='<div id="addressIDerror" class="error-msg">You are already using this address book alais.</div>';
	        			$("#addressIDerror.error-msg").show();
	        			$( ".addAddressOpc .field" ).first().append(newDiv);
		    		    return;
	        		}
	        	} 
        		
        		
        		/*$('.createOPCaddress .addOPCdeliverylocation').removeClass('visually-hidden');*/
        	}
        	
        	$('.guestusershipping .addresses-select-group .form-row-select .type-address-btn button').on('click', function() {
        		$(this).closest('.guestusershipping').find('.additional-address-fields').addClass('skip-hide');
        	});
        	
        	/*$addressesSelectFormRow.hide();
        	if(!($('.guestusershipping .additional-address-fields').hasClass('skip-hide'))) {
	            $additionalAddresses.addClass('visually-hidden');
            }*/
            progress.show();
            util.removeGeneralFormError($form);
            ajax.getJson({
                url: $findMyAddressButton.data('url'),
                data: {postCode: $postCodeInput.val()},
                callback: function (response) {
                	$addressesSelectFormRow.removeClass('visually-un-hidden');
                	$additionalAddresses.removeClass('visually-un-hidden');
                    if (response.error || response.length === 0) {
                        if (response.error) {
                            util.addGeneralFormError($form, response.error);
                        }
                        $addressesSelectFormRow.find('select').prop('disabled', true);
                        $addressesSelectFormRow.find('.form-row').removeClass('validation-error');
                        $additionalAddresses.addClass('visually-un-hidden');
                        $additionalAddresses.removeClass('visually-hidden');
                        $('<div class="type-own-address"> We could not find your address, please check your postcode or enter address below: </div>').insertBefore($additionalAddresses);
                    } else {
                    	if(!($('.guestusershipping .additional-address-fields').hasClass('skip-hide'))) {
	                        addAddressesOptions(response, $addressesSelectFormRow.find('select').prop('disabled', false));
	                        $addressesSelectFormRow.show();
	                        $addressesSelectFormRow.addClass('visually-un-hidden');
	                        $addressesSelectFormRow.removeClass('visually-hidden');
                        }
                    }
                    progress.hide();
                    $findMyAddressButton.data('searchstarted', true);
                    
                    sameAsShippingAddress();
                    
                }
            });
        });
        
        sameAsShippingAddress();

        $postCodeInput.on('change', function () {
            $findMyAddressButton.data('searchstarted', false);
        });

        $typeMyAddressButton.on('click', function () {
            $addressesSelectFormRow.find('select').prop('disabled', true);
            $addressesSelectFormRow.find('.form-row').removeClass('validation-error');
            $additionalAddresses.removeClass('visually-hidden');
            $addressesSelectFormRow.removeClass('visually-un-hidden');
            $addressesSelectFormRow.addClass('visually-hidden');
            $additionalAddresses.addClass('visually-un-hidden');            
            /*if($('#wrapper').hasClass('pt_checkout')) {
	        	$postCodeFormRow.find('.address-check-btn').css('display' , 'none');
	            $postCodeFormRow.addClass('full-width-postcode');
	            $addressesGroup.css('display' , 'none');
	            
				if($('.guestusershipping').length > 0 || $('.opcnewusershipping').length > 0) {
		            $('.shippingform .additional-address-fields').removeClass('visually-hidden');
		            $('.shippingform .address-check-btn').css('display' , 'none');
					$('.shippingform .post-code-field').addClass('full-width-postcode'); 
					$('.shippingform .addresses-select-group').css('display' , 'none');
	            }
	        }*/
	        
	        $('.additional-address-fields input[id^="dwfrm_address"], .additional-address-fields input[id^="dwfrm_checkoutaddresses_shippingAddress_address"]').on('keyup keydown change', function(){
    			validateAdditionalAddressFiedinPopup(this);
    		});
    		
        });

        $addressesSelectFormRow.find('select').on('change', function () {
            var $selectedOption = $addressesSelectFormRow.find('select option:selected');
            fillAddressFields($additionalAddresses, $selectedOption.data());
            $additionalAddresses.show();
        });
                
        $('.post-code-field input').keyup(function () {
        	postcodeval();
        });
        
        $('body').on('change', '.shipping-address-selector #customerAddress', function() {
        	$('.shippingform').removeClass('address-populated');
        	defaultShippingSddress();
        	
        });
        
        
        $('body').on('change', '.customerAddressBilling #customerAddress', function() {
        	$('.customerAddressBilling').removeClass('address-populated');
        	defaultBillingAddress();
        });
        
    });
    defaultShippingSddress();
    defaultBillingAddress();
}

function validateAdditionalAddressFiedinPopup($this){	
	var countvalue = $($this).attr('maxlength') - $($this).val().length;
	$($this).closest('.address-content').find('.addresscharacterlength .count').html(countvalue);
    if (countvalue < 1 ) {
    	$($this).closest('.address-content').find('.addresscharacterlength').addClass('alert-danger');
    }
    else {
    	$($this).closest('.address-content').find('.addresscharacterlength').removeClass('alert-danger');
    }
}

function postcodeval(){
	var postcodevalue = $('.post-code-field input').val();
    if(postcodevalue == ""){
        $('.address-check-btn button').attr('disabled','disabled');
        $('.apply-button').attr('disabled','disabled');
        $('.postcodeerror').css("display","none");
    }else{
  	  	$('.address-check-btn button').removeAttr('disabled','disabled');
  	  	$('.apply-button').removeAttr('disabled','disabled');
    }
}
function posterror(){
	$('.postcodeerror').css("display","block");
	$('.address-check-btn button').attr('disabled','disabled');
    $('.apply-button').attr('disabled','disabled');
	
}
function postsuccess(){
	
	$('.postcodeerror').css("display","none");	 
}

addressFields = {
    init: function ($form) {
        initializeEvents($form);
    },
    fillAddressFields: fillAddressFields,
    updateBillingAddress : updateBillingAddress
};

module.exports = addressFields;