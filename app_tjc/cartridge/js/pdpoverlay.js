'use strict';

var imageScript = require('./pages/product/image'),
		dialog = require('./dialog'),
		progress = require('./progress'),
		swipe = require('./pages/product/swipe');

/**
 * Finds at which element of the array has a class 'selected' and when found it is returned.
 *
 * @param {Array} productImages - Array of jQuery elements, holding the image tiles of product.
 */
function getIndexOfClassNameSelected(productImages) {
    var i;
    if (productImages.length <= 0) {
        // in case the parameter is empty, assume we want the first element (fix for TJC-1064)
        return 0;
    }
    for (i = 0; i < productImages.length; i++) {
        if (productImages[i].className.includes('selected')) {
            return i;
        }
    }
}

/*function getVideoIndex(productImages) {
    for (var i = 0; i < productImages.length; i++) {
        if (productImages[i].className.includes('video')) {
            return i;
        }
    }
    // let's hope we do not have products with more than 1000 thumbnails
    return 1000;
}*/

function pdpOverlayImages() {
    // Open the Overlay
    $(document).on('click','.overlay-open, .single-view img, .carousel-view img', function () {
        if($(this).parents('.ui-dialog-content').length >= 1){
        	return;
        }
        
    	//Shows Overlay
        $('.overlay-container').addClass('overlay-container-on').removeClass('overlay-container-off');   
        $('.overlay-container1').addClass('overlay-container-on1').removeClass('overlay-container-off1');
        if(!$('.pdp_redesign').length > 0 || $('.pdp-image-mobile').is(':visible')) {
	        $('body').css('overflow-y','hidden');
        }  
        //Block Back to Top Button
        $('#back-to-top').addClass('hide-backtotop');
        $('.backtotop').addClass('hide-backtotop');

        //Block mousewheel when overlay selector is opened
        $('body').on({
            'mousewheel': function (e) {
                if ($('.overlay-container').hasClass('overlay-container-on') !== true) {
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
            }
        });
        
        //Removing dummy list which is added in overlay.isml if product slider has video in 2nd place
        if(!$('.product-video-wrapper .overlayvideo-container1').hasClass('livetvvideo') && $('#overlay-list .coverlay-count-3').length > 0){
        	$("#overlay-list .removelist").remove();
        }

        //Holds the image containers
        var productImages = Array.from($('li.productthumbnail')),

            //Create the Image Carousel
            currentIndex = getIndexOfClassNameSelected(productImages),
            items = $('#overlay-list li'),
            itemAmt = items.length,
            item;  
            //videoindex = getVideoIndex(productImages);
            //autoSlide;
        //currentIndex = (videoindex <= currentIndex) ? currentIndex - 1 : currentIndex;
        item = $('#overlay-list li').eq(currentIndex);        
        items.hide();
        item.css('display', 'block');
        var newPDPFlag = $('.pt_product-details').hasClass('pdp_redesign');
        function cycleItems() {
            //Changing the selected image for zoom
            item.removeClass('selected');
            item = $('#overlay-list li').eq(currentIndex);
            item.addClass('selected');
            items.hide();
            item.css('display', 'block');
            var link = item.attr('href');
            //Enables zoom on the new selected image
            imageScript.loadZoom(link);          
           imageScript.loadImageComponents();         
		   if(newPDPFlag == true || newPDPFlag == "true"){
			   if(!$('.swiper-container.pdp-overlay-swiper').hasClass('.swiper-container-initialized')){
				   swipe.init('pdpOverlay');
			   }
		   }
        }
             
        if(newPDPFlag == true || newPDPFlag == "true"){
             $('.overlay-container').addClass('overlaynew');
             $('.product-img-section #thumbnails').clone().appendTo(".thumbnailrender");             
             if ($('.thumbnailrender .productthumbnail').hasClass('video')){
                 $('.thumbnailrender .productthumbnail.video').closest('div').closest('.slick-slide').remove();
             }
        }
        
        /* Overlay thumbnail click */	      
   	    $('#pdpMainOverlay').on('click',  '.productthumbnail:not(.video)', function (e) {
	    	e.preventDefault();
	        $(this).closest('.product-thumbnails').find('.productthumbnail.selected').removeClass('selected');
	        $(this).addClass('selected');
	        var imgURL = $(this).data('main-image-url');
	        $('.overlay-item.selected  img').attr('src', imgURL);
	        $('.overlay-item.selected .overlayDisplyImage').unwrap();
            $(".overlayDisplyImage").removeAttr("style");
	        if(newPDPFlag == true || newPDPFlag == "true"){	        		
	        		swipe.swipeSlideTo('pdpOverlay', $('#pdpMainOverlay #thumbnails li.productthumbnail').index(this));
			}
	    });
        

        /*autoSlide = setInterval(function () {
            console.log('autoslide');
            currentIndex += 1;
            if (currentIndex > itemAmt - 1) {
                currentIndex = 0;
            }
            cycleItems();
        }, 10000);*/
        $('.overlay-btn-next').click(function () {
            //clearInterval(autoSlide);
            currentIndex += 1;
            if (currentIndex > itemAmt - 1) {
                currentIndex = 0;
            }
            cycleItems();
        });
        $('.overlay-btn-prev').click(function () {
            //clearInterval(autoSlide);
            currentIndex -= 1;
            if (currentIndex < 0) {
                currentIndex = itemAmt - 1;
            }
            cycleItems();
        });
        // initial zoom loading
        cycleItems();
    });

    // Close the Overlay
    function closeOverlay($target) {
        // Hide Overlay
    	$('body').css('overflow-y','scroll');
        if (!$target) {
            $('.overlay-container').addClass('overlay-container-off').removeClass('overlay-container-on');           
        } else {
            if ($target.hasClass('overlay-container')) {
                $target.addClass('overlay-container-off').removeClass('overlay-container-on');
            } else {
                $target.parents('.overlay-container')
                    .addClass('overlay-container-off')
                    .removeClass('overlay-container-on');
            }
        }
        var vid = document.getElementById("360Video");
        vid.pause();       
        //Enable Back to Top Button        
        $('#back-to-top').removeClass('hide-backtotop');
        $('.backtotop').removeClass('hide-backtotop');        
    }
    
 // Close the Overlay by clincking in the black area of the container
    $('.overlay-container').on('click', function (e) {
        if (e.target !== this) {
            return;
        }

        closeOverlay($(e.target));
    });

    // Close the Overlay by clincking in the "X" button
    $('.overlay-close').on('click', function (e) {
        closeOverlay($(e.target));  
            
    });
    
     $('.overlay-box-close').on('click', function () {
	     $('.overlay-item.selected .overlayDisplyImage').unwrap();
	     $(".overlayDisplyImage").removeAttr("style");
	     $('#overlay-list li').removeClass('selected'); 
	     $('.thumbnailrender #thumbnails').remove();
     });
     
    
 // Close the Overlay1
    function closeOverlayTest($target) {
        // Hide Overlay
    	$('body').css('overflow-y','scroll');
        if (!$target) {
            $('.overlay-container1').addClass('overlay-container-off1').removeClass('overlay-container-on1');           
        } else {
            if ($target.hasClass('overlay-container1')) {
                $target.addClass('overlay-container-off1').removeClass('overlay-container-on1');
            } else {
                $target.parents('.overlay-container1')
                    .addClass('overlay-container-off1')
                    .removeClass('overlay-container-on1');
            }
        }
        var vid = document.getElementById("liveVideo");
        vid.pause();       
        //Enable Back to Top Button        
        $('#back-to-top').removeClass('hide-backtotop');
        $('.backtotop').removeClass('hide-backtotop');        
    }

    // Close the Overlay by clincking in the black area of the container
    $('.overlay-container1').on('click', function (e) {
        if (e.target !== this) {
            return;
        }

        closeOverlayTest($(e.target));
    });

    // Close the Overlay by clincking in the "X" button
    $('.overlay-close1').on('click', function (e) {
        closeOverlayTest($(e.target));       
    });
}
          
     
                     
function pdpOverlayVideo() {
    // Open the Overlay
    $('.overlayvideo-open, .product-primary-image.newtemplate .single-view .playButton').on('click', function () {
        //Shows Overlay
        var $videoContainer = $('.overlayvideo-container');
        if($(this).hasClass('360video')){
        	$videoContainer = $('.overlayvideo-container.360video');
        }else if($(this).hasClass('livetvvideo')){
        	$videoContainer = $('.overlayvideo-container.livetvvideo');
        }
        $videoContainer.addClass('overlayvideo-container-on').removeClass('overlayvideo-container-off');

        //Block Back to Top Button
        $('#back-to-top').addClass('hide-backtotop');
        $('.backtotop').addClass('hide-backtotop');

        // Enables video replay
        //$videoContainer.find('video').attr('loop', true);
        // Enables auto play when the user opens the video tab.
        //$videoContainer.find('.vjs-big-play-button').click();

        var vid = $videoContainer.find("video");
        vid.trigger('play');

        //Block mousewheel when overlay selector is opened
        $('body').on({
            'mousewheel': function (e) {
                if ($videoContainer.hasClass('overlayvideo-container-on') !== true) {
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
            }
        });
    });

    // Close the Overlay
    function closeOverlay() {
        // Hide Overlay
    	var $videoContainer = $('.overlayvideo-container');
        if($(this).hasClass('360video')){
        	$videoContainer = $('.overlayvideo-container.360video');
        }else if($(this).hasClass('livetvvideo')){
        	$videoContainer = $('.overlayvideo-container.livetvvideo');
        }
        $videoContainer.addClass('overlayvideo-container-off').removeClass('overlayvideo-container-on');
        var vid = $videoContainer.find("video");
        vid.trigger('pause');

        //Enable Back to Top Button
        $('#back-to-top').removeClass('hide-backtotop');
        $('.backtotop').removeClass('hide-backtotop');
    }

    // Close the Overlay by clincking in the black area of the container
    $('.overlayvideo-container').on('click', function (e) {
        if (e.target !== this) {
            return;
        }

        closeOverlay();
    });

    // Close the Overlay by clincking in the "X" button
    $('.overlayvideo-close').on('click', function () {
        closeOverlay();
    });
}


function pdpOverlayVideo1() {
    // Open the Overlay
    $('.overlayvideo-open1').on('click', function () {
        //Shows Overlay
        var $videoContainer = $('.overlayvideo-container1');
        $videoContainer.addClass('overlayvideo-container-on1').removeClass('overlayvideo-container-off1');

        //Block Back to Top Button
        $('#back-to-top').addClass('hide-backtotop');
        $('.backtotop').addClass('hide-backtotop');

        // Enables video replay
        //$videoContainer.find('video').attr('loop', true);
        // Enables auto play when the user opens the video tab.
        //$videoContainer.find('.vjs-big-play-button').click();

        var vid = document.getElementById("liveVideo");
        vid.play();

        //Block mousewheel when overlay selector is opened
        $('body').on({
            'mousewheel': function (e) {
                if ($videoContainer.hasClass('overlayvideo-container-on1') !== true) {
                    return;
                }
                e.preventDefault();
                e.stopPropagation();
            }
        });
    });

    // Close the Overlay
    function closeOverlay1() {
        // Hide Overlay
        $('.overlayvideo-container1').addClass('overlayvideo-container-off1').removeClass('overlayvideo-container-on1');
        var vid = document.getElementById("liveVideo");
        vid.pause();

        //Enable Back to Top Button
        $('#back-to-top').removeClass('hide-backtotop');
        $('.backtotop').removeClass('hide-backtotop');
    }

    // Close the Overlay by clincking in the black area of the container
    $('.overlayvideo-container1').on('click', function (e) {
        if (e.target !== this) {
            return;
        }

        closeOverlay1();
    });

    // Close the Overlay by clincking in the "X" button
    $('.overlayvideo-close1').on('click', function () {
        closeOverlay1();
    });
}

function showRegistrationOverlayOnConfirmation() {
    var confirmationPageCheck = $('.contenxt-confirmationpage');
    if (confirmationPageCheck.length === 1) {
        confirmationPageCheck.click();
    }
}

function pdpProductEngraving(){
    var engravingValidateFields = ["dwfrm_engraving_engraving1", "dwfrm_engraving_engraving2", "dwfrm_engraving_engraving3", "dwfrm_engraving_engraving4"];
    var $engravingForm = $('form.engraving-form:visible');
    jQuery.each(engravingValidateFields, function(i,val){
    	//$engravingForm.find("#"+val).val("");
    	if($engravingForm.find('#'+val).length >= 1){
    		$engravingForm.find('#'+val).addClass('checkMaxChar');
    		var valValue = $engravingForm.find('#'+val).val();
    		var valValueLength  = valValue.length;
    		var maxVal = $engravingForm.find('#'+val).attr('maxlength');
    		var leftValue = parseInt(maxVal) - parseInt(valValueLength);
    		$("span[for='" + val + "']").html(leftValue);
    	}
    });
    $engravingForm.find('#uniform-dwfrm_engraving_engravingtac span').removeClass("checked");
    $engravingForm.find('#dwfrm_engraving_engravingtac').prop("checked",false);
    var tacLink = $engravingForm.find('#tacLink').val();
    $engravingForm.find('.engravingtac').find('label span').addClass('tooltip');
    $engravingForm.find('.engravingtac').find('label span').attr("title","Personalised items are non-returnable. Though, you have refund/repair rights for manufactured defects. Please see <a href='"+tacLink+"' target='_blank'>terms and conditions</a> for more details.");
    
    for(var itr=0; itr<5; itr++){
    	$engravingForm.find("#engravingIconCon"+itr).insertAfter($engravingForm.find(".engravingIconCon"+itr+" .input-text"));
	}
    
    tooltipInitialize();
    engravingImageOverlay();
    
    // Getting placeholder value and append to the label
	$('.engraving-group .field input').on('focus', function() {
		$(this).closest('.engraving-group').find('.engraving-focus-label').show();
	});

	$('.engraving-group .field input').on('blur', function() { 
		$(this).closest('.engraving-group').find('.engraving-focus-label').hide();
		$('.engraving-form .form-row').removeClass('validation-success');
	});

	if($('.genderselect select.input-select').val()) {
		$('.genderselect select.input-select').trigger('blur');
	}

	$('.engraving-group .field .input-text').each(function () {
		var engravingplace = $(this).attr('placeholder');
		$(this).closest('.field').find('.new-label').text(engravingplace);
	});
    
    $('.checkMaxChar').on('change keyup keydown', function(e){
    	var valId = this.id;
    	var valValue = this.value;
    	var valValueLength  = valValue.length;
    	var maxValue = $(this).attr('maxlength');
    	//var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    	//var regex = new RegExp("^[a-zA-Z0-9 %'_\b]+$");
    	//if(regex.test(str)){}else{e.preventDefault();return false;}
    	if(valValueLength >= maxValue){$(this).val(this.value.substr(0, maxValue));}
    	valValue = this.value;
    	valValueLength  = valValue.length;
    	var leftValue = parseInt(maxValue) - parseInt(valValueLength);
    	$("span[for='" + valId + "']").html(leftValue);;
    });
}

function tooltipInitialize(){
	if (window.matchMedia("(max-width: 767px)").matches){
		$('.tooltip').each(function(index){
	        $(this).tooltip({
	            position: {
	                my: "bottom-18",
	                at: "bottom-18"
	            },
	            content: function () { 
	            	var tooltipContent = $('<div />').html( $.parseHTML( $(this).attr('title') ) );
	            	return tooltipContent; 
	            },
	            //show: "slideDown",  
	            open: function(event, ui){
	            	var _elemId = ui.tooltip.attr('id');
	            	$('.ui-tooltip').each(function(index){
	            		if(this.id!=_elemId){
	            			$(this).remove();
	            		}
	            	});
	            	var width = $(window).width(); 
	                $('.ui-tooltip').css('max-width',width+'px');
	                if(width<350){
	                	$('#'+_elemId).css('left','0px');
	                }else{
	                	$('#'+_elemId).css('left','5px');
	                }
	                $('#'+_elemId).click(function(){
	                    $(this).remove();
	                });
	            },
	            close: function(event, ui) {
	            	ui.tooltip.hover(function()  {
	                    $(this).stop(true).fadeTo(500, 1);
	                },
	                function() {
	                    $(this).remove();
	                });
	            }
	        });
	    });
	}else{
		$('.tooltip').each(function(index){
	        $(this).tooltip({
	            position: {
	                my: "left top+18",
	                at: "left top+18"
	            },
	            content: function () { 
	            	var tooltipContent = $('<div />').html( $.parseHTML( $(this).attr('title') ) );
	            	return tooltipContent; 
	            },
	            //show: "slideDown",  
	            open: function(event, ui){
	            	var _elemId = ui.tooltip.attr('id');
	            	$('.ui-tooltip').css('max-width','250px');
	            	$('#'+_elemId).click(function(){
	                    $(this).remove();
	                });
	            },
	            close: function(event, ui) {
	                ui.tooltip.hover(function()  {
	                    $(this).stop(true).fadeTo(500, 1);
	                },
	                function() {
	                    $(this).remove();
	                });
	            }
	        });
	    });
	}
}

function engravingImageOverlay(){
	$('html,body').on('click', '.field .img-edit' , function (e) {
	    e.preventDefault();
	    var engImg = $(this).attr('data-engImg');
	    var engVal = $(this).attr('data-engVal');
	    var engValFontSize = $(this).attr('data-engValFontSize');
	    var engValFontName = $(this).attr('data-engValFontName');
	    var engValFontFamily = "";
	    switch(engValFontName){
		    case "Times New Roman":
		    	engValFontFamily = "Times New Roman";
		    	break;
		    case "Arial":
		    	engValFontFamily = "Arial";
		    	break;
	    	default:
	    		engValFontFamily = "Times New Roman";
	    		break;
	    }
	    var engValFontStyl = "H";
	    var engValue = "";
	    var engValueMaxLen = 0;
	    if(engValFontSize!="" && engValFontFamily!=""){
	    	var $engravingForm = $('form.engraving-form:visible');
	    	engValueMaxLen = parseInt($engravingForm.find('#'+engVal).attr('maxlength'));
    		if(engValueMaxLen > 0 && engValueMaxLen <= 15){
    			if($engravingForm.find('#'+engVal).val() == ""){
    				$engravingForm.find('#'+engVal).css('border-color','#C20000').css('border-width','1.5px');
    				$engravingForm.find('#'+engVal+'-error').css('display','block');
					return false;
			    }else{
			    	$engravingForm.find('#'+engVal).css('border-color','#b9addb').css('border-width','1px');
			    	$engravingForm.find('#'+engVal+'-error').css('display','none');
					engValue = $engravingForm.find('#'+engVal).val();
			    }
    		}
	    }
	    progress.show();
	    var url = Urls.engravingimageoverlay;
	    dialog.open({ 
	        url: url,
	        options:{
	        	dialogClass:'engraving-image-overlay',
	        	open: function(event,ui){
	        		$('body').css('overflow','hidden');
	        		progress.show();
	        		$('.engraving-image-overlay #data-text1').html(engValue);
	        		if(engValFontStyl=="V"){
	        			$('.engraving-image-overlay #data-text1').css({'writing-mode': 'vertical-rl', 'text-orientation': 'upright', 'letter-spacing': '-10px'});
	        		}
	        		textAlignMiddle();
	        		$('.engraving-image-overlay #engravingImage').attr('src',engImg);
	        		var fontMulFact = ($('.engraving-image-overlay').css('width').slice(0,-2)/500)*7;
	        		$('.engraving-image-overlay #engravingImage').load(function(){
	        			if(engValue!=""){
	        				$('.engraving-image-overlay #data-text1').css({'display':'block'});
	        				$('.engraving-image-overlay #data-text1').css({'font-family':engValFontFamily});
	        				$('.engraving-image-overlay #data-text1').css({'font-size':(engValFontSize*fontMulFact)+'px'});
	        				setTimeout(function(){ textAlignMiddle(); }, 500);
	        			}else{
	        				$('.engraving-image-overlay #data-text1').css({'display':'none'});
	        			}
		        		progress.hide();
	        		});
	        		function textAlignMiddle(){
	        			var txtHlfWidth = ($('#data-text1').css('width').slice(0,-2)/2);
		        		var txtHlfHeight = ($('#data-text1').css('height').slice(0,-2)/2);
		        		$('.engraving-image-overlay #data-text1').css({'left':'calc(50% - '+txtHlfWidth+'px)','top':'calc(50% - '+txtHlfHeight+'px)'});
	        		}
	        	},
	        	close:function() {
	        		$('body').removeAttr("style");
	        		$('html').removeAttr("style");	
	        	}
	        }
	    });
	});
}

function warrantyMiniCart(){
	$('body').off('click').on('click', '.warranty-dialog' , function (e) {
	    e.preventDefault();
	    var isCartPage = $(this).attr('data-cartPage');
	    var proName = $(this).attr('data-productName');
	    var proImg = $(this).attr('data-productImage');
	    var productCost = $(this).attr('data-productCost');
	    productCost = productCost.replace(',','');
	    var proCost = productCost.substring(1, productCost.length);
	    var proQuantity = $(this).attr('data-productQuantity');
	    var warCost = $(this).attr('data-warrantyCost');
	    warCost = warCost.replace(',','');
	    var warDuration = $(this).attr('data-warrantyDuration');
	    var warQuantity = $(this).attr('data-warrantyQuantity');
	    var parentAddWarURI = $(this).attr('data-warrantyAddURI');
	    var proQty = ($(".pdp-mobile-only ").is(":visible"))?$('.pdp-mobile-only .pdpForm #Quantity').val():$('.sticky-desktop-check .pdpForm #Quantity').val();
	    var warQty = ($(".pdp-mobile-only ").is(":visible"))?$('.pdp-mobile-only .pdpForm #warranty-total-quantity').val():$('.sticky-desktop-check .pdpForm #warranty-total-quantity').val();
	    var warEnb = ($(".pdp-mobile-only ").is(":visible"))?$(".pdp-mobile-only input[name='warrantyEnabled']").prop("checked"):$(".sticky-desktop-check input[name='warrantyEnabled']").prop("checked");
	    var clickedClasses = this.className;
	    progress.show();
	    var url = Urls.warrantyminicart;
	    dialog.open({ 
	        url: url,
	        options:{
	        	dialogClass:'warranty-mini-cart-dialog',
	        	title: "TJC Warranty Details",
	        	open: function(event,ui){
	        		$('body').css('overflow','hidden');
	        		$('.ui-widget-overlay').on('click', function(){
	        			$('.warranty-mini-cart-dialog').find('.ui-dialog-content').dialog('close');
	        		});
	        		$('.warranty-mini-cart-dialog .warranty-Qty').hide();
	        		$('.warranty-mini-cart-dialog .warAddBtn').hide();
	        		$('.warranty-mini-cart-dialog .warQty .item-checkbox').hide();
	        		$('.warranty-mini-cart-dialog .warAddBtnLink').hide();
	        		if(clickedClasses.indexOf("explore-warranty") != -1){
	        			warQty = Math.round(warQuantity);
	        			$('.warranty-mini-cart-dialog .warAddBtnLink').attr('href',parentAddWarURI);
	        			$('.warranty-mini-cart-dialog .warAddBtnLink').show();
	        			$('.warranty-mini-cart-dialog .warQty').hide();
	        		}else if(clickedClasses.indexOf("product-name") != -1){
	        			$('.warranty-mini-cart-dialog .warQty').hide();
	        			warQty = Math.round(warQuantity);
	        		}else{
	        			if(warEnb == true){
        					if(proQty > 1){
        						$('.warranty-mini-cart-dialog .warranty-Qty').show();
        					}else{
    	        				$('.warranty-mini-cart-dialog .warQty .item-checkbox').show();
    	        				$('.warranty-mini-cart-dialog .warQty .item-checkbox span').addClass('checked');
    	        			}
		        		}else{
		        			$('.warranty-mini-cart-dialog .warAddBtn').show();
		        		}
	        		}
	        		var screenHeight = screen.height;
	        		$('.warranty-mini-cart-dialog .warrantyMiniCartBody').css('max-height',(parseFloat($('.warranty-mini-cart-dialog').css('height'))-parseFloat(90))+"px");
	        		progress.hide();
	        		$('.warranty-mini-cart-dialog .productDetails').find('img').attr('src',proImg);
	        		$('.warranty-mini-cart-dialog .productDetails').find('.name').text(proName);
	        		$('.warranty-mini-cart-dialog .productDetails .price').find('span').text(proCost);
	        		if(isCartPage=="true"){
		        		$('.warranty-mini-cart-dialog .productDetails').find('.name').append(" &nbsp;&nbsp;<span class='bold'>x "+Math.round(proQuantity)+"</span>");
		        		var productPrice = proCost*proQuantity;
		        		proCost = productPrice;
		        		$('.warranty-mini-cart-dialog .productDetails .price').find('span').text(productPrice.toFixed(2));
	        		}else{
	        			$('.warranty-mini-cart-dialog .productDetails').find('.name').append(" &nbsp;&nbsp;<span class='bold'>x "+Math.round(proQty)+"</span>");
		        		var productPrice = proCost*proQty;
		        		proCost = productPrice;
		        		$('.warranty-mini-cart-dialog .productDetails .price').find('span').text(productPrice.toFixed(2));
	        		}
	        		$('.warranty-mini-cart-dialog').find('span.warranty-duration').text(warDuration);
	        		$('.warranty-mini-cart-dialog #warranty-product-quantity').val(warQty);
	        		$('.warranty-mini-cart-dialog .warrantyDetails .name').find('.warrantyCount').text(warQty);
	        		var warrantyPrice = warCost*warQty;
	        		$('.warranty-mini-cart-dialog .warrantyDetails .price').find('span').text(warrantyPrice.toFixed(2));
	        		var totalPrice = parseFloat(warCost*warQty)+parseFloat(proCost);
	        		$('.warranty-mini-cart-dialog .totalDetails .price').find('span').text(totalPrice.toFixed(2));
	        		$('.warranty-mini-cart-dialog .warQty #pricechangefornottjcplus').find('span').text(warrantyPrice.toFixed(2));
	        		
	        		$('.warranty-mini-cart-dialog .warranty-sub').off('click').on('click',function(){
	        			var calVal = parseInt($('.warranty-mini-cart-dialog #warranty-product-quantity').val())-1;
	        			if(calVal > 0){
	        				$('.warranty-mini-cart-dialog #warranty-product-quantity').val(calVal);
	        				$('.warranty-mini-cart-dialog .warrantyDetails .name').find('.warrantyCount').text(calVal);
	        				var warrantyPrice = warCost*calVal;
	    	        		$('.warranty-mini-cart-dialog .warrantyDetails .price').find('span').text(warrantyPrice.toFixed(2));
	    	        		var totalPrice = parseFloat(warCost*calVal)+parseFloat(proCost);
	    	        		$('.warranty-mini-cart-dialog .totalDetails .price').find('span').text(totalPrice.toFixed(2));
	    	        		$('.warranty-mini-cart-dialog .warQty #pricechangefornottjcplus').find('span').text(warrantyPrice.toFixed(2));
	        			}
	        		});
	        		$('.warranty-mini-cart-dialog .warranty-add').off('click').on('click',function(){
	        			var calVal = parseInt($('#warranty-product-quantity').val())+1;
	        			if(calVal <= proQty){
	        				$('.warranty-mini-cart-dialog #warranty-product-quantity').val(calVal);
	        				$('.warranty-mini-cart-dialog .warrantyDetails .name').find('.warrantyCount').text(calVal);
	        				var warrantyPrice = warCost*calVal;
	    	        		$('.warranty-mini-cart-dialog .warrantyDetails .price').find('span').text(warrantyPrice.toFixed(2));
	    	        		var totalPrice = parseFloat(warCost*calVal)+parseFloat(proCost);
	    	        		$('.warranty-mini-cart-dialog .totalDetails .price').find('span').text(totalPrice.toFixed(2));
	    	        		$('.warranty-mini-cart-dialog .warQty #pricechangefornottjcplus').find('span').text(warrantyPrice.toFixed(2));
	        			}
	        		});
	        		
	        		$('.warranty-mini-cart-dialog .warQty .item-checkbox').off('click').on('click',function(){
	        			if($('.warranty-mini-cart-dialog .warQty .item-checkbox span').hasClass('checked')){
	        				($(".pdp-mobile-only").is(":visible"))?$(".pdp-mobile-only input[name='warrantyEnabled']").prop("checked",false):$(".sticky-desktop-check input[name='warrantyEnabled']").prop("checked",false);
	        				($(".pdp-mobile-only").is(":visible"))?$(".pdp-mobile-only input[name='warrantyEnabled']").parents('span').removeClass('checked'):$(".sticky-desktop-check input[name='warrantyEnabled']").parents('span').removeClass('checked');
	        			}else{
	        				($(".pdp-mobile-only").is(":visible"))?$(".pdp-mobile-only input[name='warrantyEnabled']").prop("checked",true):$(".sticky-desktop-check input[name='warrantyEnabled']").prop("checked",true);
	        				($(".pdp-mobile-only").is(":visible"))?$(".pdp-mobile-only input[name='warrantyEnabled']").parents('span').addClass('checked'):$(".sticky-desktop-check input[name='warrantyEnabled']").parents('span').addClass('checked');
	        			}
	        			$('.warranty-mini-cart-dialog').find('.ui-dialog-content').dialog('close');
	        		});
	        		$('.warranty-mini-cart-dialog .warUpdateBtn').off('click').on('click',function(){
	        			var warTot = $('.warranty-mini-cart-dialog #warranty-product-quantity').val();
	        			$('.warranty-count').val(warTot);
        		        $(".total-warranty-count").html('x ' + warTot);
        		        var warrantyPrice = warCost*warTot;
        		        $('.warranty-dialog .total-warranty-price').text("+ £" + warrantyPrice.toFixed(2));
	        			$('.warranty-mini-cart-dialog').find('.ui-dialog-content').dialog('close');
	        		});
	        		$('.warranty-mini-cart-dialog .warAddBtn').off('click').on('click',function(){
	        			$(".sticky-desktop-check input[name='warrantyEnabled']").parents('span').addClass("checked");
	        			$(".sticky-desktop-check input[name='warrantyEnabled']").prop("checked",true);
	        			$(".pdp-mobile-only input[name='warrantyEnabled']").parents('span').addClass("checked");
	        			$(".pdp-mobile-only input[name='warrantyEnabled']").prop("checked",true);
	        			//var warTot = 1;
	        			//$(".total-warranty-count").html('x ' + warTot);
        		        //var warrantyPrice = warCost*warTot;
        		        //$('.warranty-dialog .total-warranty-price').text("+ " + $('.warranty-dialog .total-warranty-price').text());
	        			$('.warranty-mini-cart-dialog').find('.ui-dialog-content').dialog('close');
	        		});
	        	},
	        	close:function() {
	        		$('body').removeAttr("style");
	        		$('html').removeAttr("style");	
	        	}
	        }
	    });
	});
}

function warrantyQuantitySelection(){
	/***** Start - Method for increasing/Decreasing the Product Quantity *****/

	var $pdpMain = $('#pdpMain');
	if($('#QuickViewDialog').is(':visible')){
		$pdpMain = $('#QuickViewDialog .pdp-main');
	}
	$pdpMain.find('.product-add').off('click').on('click', function(e){
	      var productqty = $pdpMain.find('.pdp-product-quantity').val(),
	      pdpProductCount = $pdpMain.find(".sticky-desktop-check .pdp-product-quantity").data('availability');
	      productqty++;
	      if(productqty <= pdpProductCount){
	    	  $pdpMain.find(".pdp-product-quantity").val(productqty);
	    	  $pdpMain.find(".sticky-desktop-check .warranty-add").trigger('click');
	          //$(".warranty-count").val(productqty);
	          changeWarrantyPrice();
	      }
	});
	$pdpMain.find('.product-sub').off('click').on('click', function(e){
	      var productqty = $pdpMain.find('.pdp-product-quantity').val();
	      productqty--;
	      if(productqty>0){
	    	  $pdpMain.find(".pdp-product-quantity").val(productqty);
	    	  $pdpMain.find(".sticky-desktop-check .warranty-sub").trigger('click');
	          //$(".warranty-count").val(productqty);
	          changeWarrantyPrice();
	      }
	});

	/***** End - Method for increasing/Decreasing the Product Quantity *****/

	/***** Start - Method for increasing/Decreasing the Warranty Quantity *****/

	$('.warranty-add').off('click').on('click', function(e){
	      var warrantyqty=$('.warranty-count').val();
	      warrantyqty++;
	      if(warrantyqty<= $('#pdpMain').find('.pdp-product-quantity').val()){
	          $(".warranty-count").val(warrantyqty);
	          changeWarrantyPrice();
	      }
	});
	$('.warranty-sub').off('click').on('click', function(e){
	      var warrantyqty=$('.warranty-count').val();
	      warrantyqty--;
	      if(warrantyqty>0){
	          $(".warranty-count").val(warrantyqty);
	          changeWarrantyPrice();
	      }
	});
	
	/***** End - Method for increasing/Decreasing the Warranty Quantity *****/
	
	/***** Start - trigger warranty program dialog *****/
	$('.rightarrow').on('click', function(){
		$(".warranty-dialog").trigger('click');
	});
	/***** End - trigger warranty program dialog *****/
	
	function changeWarrantyPrice(){
		/***** Start - change Warranty Price *****/
		var warrantyqty = $('.warranty-count').val(),
	    warrantyCost = $(".sticky-desktop-check .warranty-dialog").data('warrantycost'),
	    warrantyPrice = warrantyCost*warrantyqty; 
	    
	    if($('#pdpMain').find('.pdp-product-quantity').val() > 1){
	        $('.warranty-quantity-box').show();
	        $(".total-warranty-count").html('x ' + warrantyqty);
	        $('.warranty-dialog .total-warranty-price').text("+ £" + warrantyPrice.toFixed(2));
	    }
	    else{
	        $('.warranty-quantity-box').hide();
	        $(".total-warranty-count").html("");
	        $('.warranty-dialog .total-warranty-price').text("+ £" + warrantyPrice.toFixed(2));
	    }
	    /***** Start - change Warranty Price *****/
	}
}

// Module public functions
module.exports = {
    init: function () {
    	swipe.init('pdpMain');
        pdpOverlayImages();
        pdpOverlayVideo();
        pdpOverlayVideo1();
        showRegistrationOverlayOnConfirmation();
        pdpProductEngraving();
        warrantyMiniCart();
        warrantyQuantitySelection();
    },
    warrantyQuantitySelection: function () {
    	warrantyQuantitySelection();
    }
};
