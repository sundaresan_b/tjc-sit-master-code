'use strict';


function stickyheadermain() {
    $(window).on('scroll', function () {
    	

        var pageTop = $(window).scrollTop(),
            // widthScreenSize = $(window).width(),
            // breaksize = 768,
            breakpointScreen = 1; //30;

        // if (widthScreenSize < breaksize) {
        // breakpointScreen = 30;
        // } else {
        // breakpointScreen = 40;
        // }
        
        if($('.header-fixed.scrolled.hide').length){
            $('.pdp-sticky-on').addClass('top-stick');
        }
        else {
            $('.pdp-sticky-on').removeClass('top-stick');
        }

        if (pageTop < breakpointScreen) {
            $('.header-wrapper').addClass('not-scrolled');
            $('.header-wrapper').removeClass('scrolled');
            $('.header-fixed').addClass('not-scrolled');
            $('.header-fixed').removeClass('scrolled');
            $('.header-asset').removeClass('scrolled');
            $('.GDPR').removeClass('scrolled');
            $('.main-wrapper').addClass('not-scrolled');
            $('.main-wrapper').removeClass('scrolled');
        } else {
            $('.header-wrapper').removeClass('not-scrolled');
            $('.header-wrapper').addClass('scrolled');
            $('.header-fixed').removeClass('not-scrolled');
            $('.header-fixed').addClass('scrolled');
            $('.header-asset').addClass('scrolled');
            $('.GDPR').addClass('scrolled');
            $('.main-wrapper').removeClass('not-scrolled');
            $('.main-wrapper').addClass('scrolled');
        }
    });
    
    (function(){
        var doc = document.documentElement;
        var w = window;
        var prevScroll = w.scrollY || doc.scrollTop;
        var curScroll;
        var direction = 0;
        var prevDirection = 0;
        var header = document.getElementById('site-header');
        
        var checkScroll = function() {

          curScroll = w.scrollY || doc.scrollTop;
          if (curScroll > prevScroll) { 
            direction = 2;
          }
          else if (curScroll < prevScroll) { 
            direction = 1;
          }
          if (direction !== prevDirection) {
            toggleHeader(direction, curScroll);
          }
          prevScroll = curScroll;
        };

        var toggleHeader = function(direction, curScroll) {
          if (direction === 2 && curScroll > 52) { 
            header.classList.add('hide');
            prevDirection = direction;
          }
          else if (direction === 1) {
            header.classList.remove('hide');
          prevDirection = direction;
          }
          
        };
        window.addEventListener('scroll', checkScroll);

      })();
}

// Module public functions
module.exports = {
    init: stickyheadermain
};
