'use strict';

var imagesLoaded = require('imagesloaded'),
    quickview = require('./quickview'),
	addedModal = require('./addedmodal'),
    dialog = require('./dialog'),
    util = require('./util'),
    minicart = require('./minicart');


function gridViewToggle() {
    $('.toggle-grid').on('click', function () {
        $('.search-result-content').toggleClass('wide-tiles');
        $(this).toggleClass('wide');
    });
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
    gridViewToggle();
    $('.swatch-list').on('mouseleave', function () {
        // Restore current thumb image
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $thumb.data('current');

        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    });
    $('.swatch-list .swatch').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected')) {
            return;
        }

        var $tile = $(this).closest('.product-tile'),
            data,
            $thumb,
            currentAttrs;
        $(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
        $(this).addClass('selected');
        $tile.find('.thumb-link').attr('href', $(this).attr('href'));
        $tile.find('name-link').attr('href', $(this).attr('href'));

        data = $(this).children('img').filter(':first').data('thumb');
        $thumb = $tile.find('.product-image .thumb-link img').eq(0);
        currentAttrs = {
            src: data.src,
            alt: data.alt,
            title: data.title
        };
        $thumb.attr(currentAttrs);
        $thumb.data('current', currentAttrs);
    }).on('mouseenter', function () {
        // get current thumb details
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $(this).children('img').filter(':first').data('thumb'),
            current = $thumb.data('current');

        // If this is the first time, then record the current img
        if (!current) {
            $thumb.data('current', {
                src: $thumb[0].src,
                alt: $thumb[0].alt,
                title: $thumb[0].title
            });
        }

        // Set the tile image to the values provided on the swatch data attributes
        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    });

    $('.tiles-container .product-tile').on('click', '.thumb-link, .name-link, .view-button-link', function () {
        var $productLink = $(this),
            $tile = $productLink.parents('.product-tile'),
            cgid = $tile.data('cgid'),
            isSearch = $('.search-result-content').data('issearchwithquery');
        if (cgid) {
            $productLink[0].hash = 'cgid=' + encodeURIComponent(cgid);
        }
        if (isSearch) {
            if (cgid) {
                $productLink[0].hash += '&issearch=true';
            } else {
                $productLink[0].hash = 'issearch=true';
            }
        }
    });
}

$('.product-tile-buttons .xlt-addToCart').on('click', function(e){
	e.preventDefault();
		$.ajax({
		    type: 'GET',
		    dataType: 'html',
		    url: util.ajaxUrl(Urls.addProduct)+"&pid="+$(this).data("pid")+"&Quantity=1&cartAction=add&quickview=isQuickView",
		    success: function(response) {
		    	addedModal.init();
        	    minicart.show(response);
            }
		});
});


exports.init = function () {
    var $tiles = $('.tiles-container .product-tile');
    if ($tiles.length === 0) {
        return;
    }
    imagesLoaded('.tiles-container').on('done', function () {
        $tiles.syncHeight()
            .each(function (idx) {
                $(this).data('idx', idx);
            });
    });
    initializeEvents();
    quickview.init();
};
