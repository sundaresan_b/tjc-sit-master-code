'use strict';

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
    $('.button-container-cell:visible').off('click').on('click', '.inform-me-when-available-pdp button', function () {
            var $this = $(this),
                $informBox = $this.parents('.inform-me-when-available-pdp'),
                $formRow = $informBox.find('.form-row');
            var email = $informBox.find('input').val();
            if(email == "") {
            	$formRow.addClass('validation-error');
            	if($formRow.find('.error-msg').length == 0) {
            		$formRow.find('.field').append('<div id="-error" class="error-msg">Cannot be blank</div>');
            	}
            	return;
            }
            
            if($(".inform-me-when-available-pdp .inform-me-message-fail").length > 0){
            	$(".inform-me-when-available-pdp .inform-me-message-fail").remove();
            }
            
            $informBox.find('.error-msg').remove();
            $formRow.removeClass('validation-error');
            $.ajax({
                type: 'post',
                url: $this.data('url'),
                data: {
                    email: $informBox.find('input').val(),
                    pid: $this.data('pid')
                }
            }).done(function (response) {
                if (!response.success) {
                    $informBox.html('<div class="inform-me-message-success">' + Resources.INFORM_ME_SUCCESS + '</div>');
                } else {
                    var alreadyDisplayedMessage =
                        $informBox.find('.field .inform-me-message-fail') === 0 ? true : false;
                    if (!alreadyDisplayedMessage) {
                        $informBox.find('.field').append('<div class="inform-me-message-fail">' +
                            Resources.INFORM_ME_FAIL + '</div>');
                    }
                }
            });
        });

}

exports.init = function () {
    initializeEvents();
};