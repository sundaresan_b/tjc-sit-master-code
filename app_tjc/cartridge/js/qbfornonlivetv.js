'use strict';

var dialog = require('./dialog'),
 	util = require('./util'),
 	ajax = require('./ajax'),
 	validator = require('./validator'),
 	page = require('./page'),
 	progress = require('./progress'),
 	flyinheader = require('./flyinheader'),
 	newformstyle = require('./new-form-style'),
 	orderconfirmation = require('./pages/orderconfirmation');
 	//addressjs = require('./pages/checkout/address'),
 	//accountjs = require('./pages/account');
 	

function enableQuickBuy() {
    $('.quickbuypref-check').addClass('enabled');
    $('.change-mode').html('enabled');
    $('.quickbuy-detail').css('opacity', '1');
    $(".quickbuy-detail :input[type=text], .quickbuy-detail :input[type=submit], .quickbuy-detail :input[type=checkbox], .quickbuy-detail select").prop("disabled", false);
    $('#qbpref-enablecontent').removeClass('d-none');
    $('#isquikbuydisabled').addClass('d-none');
    $('.add-new-address').css('pointer-events','auto');
	$('.add-new-address').css('cursor','pointer');
	$(".quickbuy-detail  .styledSelect").css('pointer-events', 'pointer');
	$(".quickbuy-detail  .styledSelect").css('cursor', 'pointer');
}

function disableQuickBuy() {
    $('.quickbuypref-check').removeClass('enabled');
    $('#qbpref-disablecontent').addClass('d-none');
    $('#qbpref-enablecontent').addClass('d-none');
    $('#isquikbuydisabled').removeClass('d-none');
    $('.change-mode').html('disabled');
    $('.add-new-address').css('pointer-events','none');
    $('.add-new-address').css('cursor','default');
    $('.quickbuy-detail').css('opacity', '0.5');
    $('.quickbuytac').css('display', 'none');
    $(".quickbuy-detail :input[type=text], .quickbuy-detail :input[type=submit], .quickbuy-detail :input[type=checkbox], .quickbuy-detail select").prop("disabled", true);
    $(".quickbuy-detail  .styledSelect").css('pointer-events', 'none');
    $(".quickbuy-detail  .styledSelect").css('cursor', 'auto');
}

function addToCartValidation(){
	var isEngravingItem = $('#isEngravingItem').val();
	if(isEngravingItem == "true"){
		var errorFields = "";
	    var engravingValidateFields = ["mandatory_color", "mandatory_size", "mandatory_birthstone", "mandatory_birthflower", "mandatory_chakra", "mandatory_zodiac", "mandatory_engraving1", "mandatory_engraving2", "mandatory_engraving3", "mandatory_engraving4"];
	    var $engravingForm = $('form.engraving-form:visible');
	    jQuery.each(engravingValidateFields, function(i,val){
	    	if($engravingForm.find('#'+val).length >= 1){
	    		if($engravingForm.find('#'+val).val() == "true"){
	    			var fieldToChk = $engravingForm.find('#'+val).attr('data-validateClass');
	    			if($engravingForm.find('#'+fieldToChk).val() == ""){
	    				if($engravingForm.find('#'+fieldToChk).prop('tagName') == "SELECT"){
	    					var $selectParent = $engravingForm.find('#'+fieldToChk).parents('.form-row-select');
	    					$selectParent.find('.styledSelect').css('border-color','#C20000').css('border-width','1.5px');
	    					$engravingForm.find('#'+fieldToChk+'-error').css('display','block');
	    				}else{
	    					$engravingForm.find('#'+fieldToChk).css('border-color','#C20000').css('border-width','1.5px');
	    					$engravingForm.find('#'+fieldToChk+'-error').css('display','block');
	    				}
	    				errorFields+=fieldToChk;
	    			}else{
	    				if($engravingForm.find('#'+fieldToChk).prop('tagName') == "SELECT"){
	    					var $selectParent = $engravingForm.find('#'+fieldToChk).parents('.form-row-select');
	    					$selectParent.find('.styledSelect').css('border-color','#b9addb').css('border-width','1px');
	    					$engravingForm.find('#'+fieldToChk+'-error').css('display','none');
	    				}else{
	    					$engravingForm.find('#'+fieldToChk).css('border-color','#b9addb').css('border-width','1px');
	    					$engravingForm.find('#'+fieldToChk+'-error').css('display','none');
	    				}
	    			}
	    		}
	    	}
	    });
	    if(!$engravingForm.find('#dwfrm_engraving_engravingtac').prop("checked")){
	    	$engravingForm.find('div.checker').css('border-color','#C20000').css('border-width','1.5px');
	    	$engravingForm.find("label[for='dwfrm_engraving_engravingtac']").css('color','#C20000');
	    	var fieldToChk = "form.engraving-form:visible #dwfrm_engraving_engravingtac";
	    	errorFields+=fieldToChk;
	    }else{
	    	$engravingForm.find('div.checker').css('border-color','#b9addb').css('border-width','1px');
	    	$engravingForm.find("label[for='dwfrm_engraving_engravingtac']").css('color','#2c163d');
	    }
	    
	    if(errorFields!=""){
	    	$('input, select').on('keyup keydown change', function(){
	    		var id = $(this).attr('id'),
	    			parentClass = "form.engraving-form:visible ",
	    			mandVal = $('input[data-validateClass='+id+']').val();
	    		if(mandVal=="true"){
	    			if($(this).val() == ""){
	    				if($(this).prop('tagName') == "SELECT"){
	    					$(parentClass+'#uniform-'+id).css('border-color','#C20000').css('border-width','1.5px');
	    					$(parentClass+'.'+id+'-error').css('display','block');
	    				}else{
	    					$(this).css('border-color','#C20000').css('border-width','1.5px');
	    					$(parentClass+'.'+id+'-error').css('display','block');
	    				}
	    			}else{
	    				if($(this).prop('tagName') == "SELECT"){
	    					$(parentClass+'#uniform-'+id).css('border-color','#b9addb').css('border-width','1px');
	    					$(parentClass+'.'+id+'-error').css('display','none');
	    				}else{
	    					$(this).css('border-color','#b9addb').css('border-width','1px');
	    					$(parentClass+'.'+id+'-error').css('display','none');
	    				}
	    			}
	    		}
	    	});
	    	return false;
	    }else{
	    	return true;
	    }
	}else{
		return true;
	}
}

var plid = '',quantity = '',itemid = '',pid = '';
function initializeEvents() {
	if(customer.loggedIn == true && customer.loginStatusCode == "1"){
		if(customer.quickbuy.activated == true && customer.quickbuy.enabled == false){
			if(!$('#showActivateQBModel').hasClass('quick-buy-pref-Class')){
				$('#showActivateQBModel').addClass('quick-buy-pref-Class');
			}
			$('#showActivateQBModel').removeClass('quick-buy-signin');
		}else if(customer.quickbuy.activated == true && customer.quickbuy.enabled == true){
			if(!$('#showActivateQBModel').hasClass('quick-buy-signin')){
				$('#showActivateQBModel').addClass('quick-buy-signin');
			}
			$('#showActivateQBModel').removeClass('quick-buy-pref-Class');
		}else if(customer.quickbuy.activated == false){
			$('#showActivateQBModel').removeClass('quick-buy-signin');
			$('#showActivateQBModel').removeClass('quick-buy-pref-Class');
		}
	}else{
		if(!$('#showActivateQBModel').hasClass('quick-buy-login-class')){
			$('#showActivateQBModel').addClass('quick-buy-login-class')
		}
		$('#showActivateQBModel').removeClass('quick-buy-signin');
		$('#showActivateQBModel').removeClass('quick-buy-pref-Class');
	}
	
    showActivateQBModel();
    flyinheader.init();
    whatiscvv();
    checkFieldsIntiator();
    checkFieldsType();
    
    //Pass quickbuy=true when you click QB button in like list page to add persistent login
    $(document).off('click.persistentqbpopup').on('click.persistentqbpopup', '.wishlist-table #showActivateQBModel.quick-buy-login-class', function (e) {
    	e.preventDefault();
    	var sessionTimeout = Urls.wishlist + "?quickbuy=true";
    	window.location.href = sessionTimeout; 
    });
    
    if(!$('#use-billing-address').is(':checked')) {
    	//$('.customerAddressBilling').find('.add-billing-address').removeClass('hidden');
    	$('.customerAddressBilling').find('.saved-address').removeClass('hidden');
	}
    
    $("#dwfrm_quickbuyaddress_shippingAddress_addressid").focus(function(){
        $('#dwfrm_quickbuyaddress_shippingAddress_addressid-error').remove();
        $('#dwfrm_quickbuyaddress_shippingAddress_addressid').css('border-color','#b9addb');
        $('#dwfrm_quickbuyaddress_shippingAddress_addressid').css('color','#2c163d');
    });
    
    if($('.quickbuypref-check input').is(":checked") || ($('#isQbActivated').val() != "true") && ($('#isQbEnabled').val() != "true")) {
    	enableQuickBuy();
    }
    else{
        disableQuickBuy();
    }
    
	$('body').on('change', '#dwfrm_quickbuyaddress_customer_quickbuytac', function() {
        if($('#dwfrm_quickbuyaddress_customer_quickbuytac').is(':checked')){
        	$('#dwfrm_quickbuyaddress_customer_quickbuytac').attr('value', 'true');
          }
          else{
        	  $('#dwfrm_quickbuyaddress_customer_quickbuytac').attr('value', 'false');
          }
    });
	
	if(window.QBClick != true){
		var $addingToCartSection = $('.pdpForm'),
		    pageURL = window.location.href;
		var redirectQuickBuyActivation = util.getParamFromUrl('redirectQuickBuyActivation', pageURL),
		    quantity = util.getParamFromUrl('quantity', pageURL),
		    warrantyFlag = util.getParamFromUrl('warrantyFlag', pageURL),
		    warrantyQuantity = util.getParamFromUrl('warrantyQuantity', pageURL),
		    engravingFlag = util.getParamFromUrl('engravingFlag', pageURL),
		    engravingCount = parseInt(util.getParamFromUrl('engravingCount', pageURL));
		
		if(redirectQuickBuyActivation == 'true'){
			$addingToCartSection.find('.quantity input.pdp-product-quantity.item').val(quantity);
			if(warrantyFlag == 'true'){
				$addingToCartSection.find(".warranty-section input[name='warrantyEnabled']").prop("checked",true);
				$addingToCartSection.find('.warranty-section input.warranty-count.item').val(warrantyQuantity);
			}
			if(engravingFlag == 'true'){
				var $engravingForm = $('form.engraving-form:visible'),
    			    engravingField = $engravingForm.find('[name^="dwfrm_engraving_"]'),
    			    engravingParameterId, engravingParameterVal;
				$engravingForm.find('#dwfrm_engraving_engravingtac').prop("checked",true);
				for(var i=0; i<engravingCount; i++){
    				engravingParameterId = 'engravingField'+(i+1);
					engravingParameterVal = util.getParamFromUrl(engravingParameterId, pageURL);
					if($(engravingField[i]).prop('tagName') == 'SELECT'){
						$(engravingField[i]).find('option:selected').prop('selected',false);
						$(engravingField[i]).find('option:selected').removeAttr('selected');
						$($(engravingField[i]).find('option')[engravingParameterVal]).prop('selected',true);
						$($(engravingField[i]).find('option')[engravingParameterVal]).attr('selected',true);
					}else{
						engravingParameterVal = (engravingParameterVal == 'EMPTY')?'':engravingParameterVal;
						$(engravingField[i]).val(engravingParameterVal);
					}
				}
    		}
			
			$('#showActivateQBModel:not(.quick-buy-login-class)').trigger('click');
			window.QBClick = true;
		}
	}
}
function redirectQuickBuyPrefPage(){
	var url = Urls.quickbuyprefpage;
	url = util.appendParamsToUrl(url,{
		'plid':plid,
		'pid':pid,
		'quantity':quantity,
		'itemid':itemid,
		'pagecontext':pageContext.ns,
		'redirectQuickBuyActivation':true
	});
	window.location.href = url
}

function showActivateQBModel() {
	$(document).off('click.qblogin').on('click.qblogin', '.pdp-buttons .quick-buy-login-class' , function (e) {
	    e.preventDefault();
	    var url = Urls.quickbuysignpage;
	    pid = $(this).closest('form').find("input[name='pid']").val();
	    var $addingToCartSection = $('.pdpForm');
	    var quantity= $addingToCartSection.find('.quantity input.pdp-product-quantity.item').val(), 
	        warrantyQuantity = 0;
        var Login_PopUp_Active = window.SitePreferences.Login_PopUp_Active;
	    if(Login_PopUp_Active){
	        url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    dialog.open({ 
		        url: url,
		        options:{
		        	dialogClass:'new-sign-registration quick-buy-dialog',
		        	open: function(event,ui){
		        		validator.init();
		        		newformstyle.init();	        		
		        		
		        		$('.oAuthIcon').bind('click', function () {
		        	    	$('.OAuthProvider').val(this.id);
		        	    	$('.quickBuyClick').val(true);
		        	    });
		        		
		        		$('body').css('overflow','hidden');
		        		$('.checkemail-button button').off('click').on('click submit', function(event){
		        			event.preventDefault(); 
		        			var url = Urls.checkMailAvailabilty;
		        			var $form = $(this).closest('form');
		        			var emailid = $form.find('.email-field input').val();
		        			if ($form.find('.checkemail-field .error').length > 0){
		        				$form.find('.checkemail-field .error').remove();
		        			}
		        			var regex = validator.regex;
		        			if (emailid != '' && !regex.newEmail.test(emailid)){
		        				var newDiv ='<div class="error" style="color:#c20000;">The email address is invalid.<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			if (emailid == ''){
		        				var newDiv ='<div class="error" style="color:#c20000;">Cannot be blank<div>' 
		        				$form.find('.checkemail-field').append(newDiv);
		        				return;
		        			}
		        			url = util.appendParamToURL(url,'emailid',emailid);
		        			 $.ajax({
		        			        url: url,
		        			        type: 'POST',
		        			        success: function (response) { 
		        			        	var responseJson = JSON.parse(response);
		        			        	if (responseJson.availableEmail){
		        			        		var $form1 = $('.returning-customers form');
		        			        		if ($form1.find('.email-value').hasClass('d-none')){
		        			        			$form1.find('.email-value').text(responseJson.email).removeClass('d-none');
		        			        		} else {
		        			        			$form1.find('.email-value').text(responseJson.email);
		        			        		}
		        			        		if ($form1.find('.checkpw-content').hasClass('d-none')){
		        			        			$form1.find('.checkpw-content').removeClass('d-none');
		        			        		}
		        			        		if (!$form1.find('.checkemail-field').hasClass('d-none')){
		        			        			$form1.find('.checkemail-field').addClass('d-none');
		        			        		}
		        			        		if ($form1.find('.form-row-password').hasClass('d-none')){
		        			        			$form1.find('.form-row-password').removeClass('d-none');
		        			        		}
		        			        		if (!$form1.find('.form-row-button .checkemail-button').hasClass('d-none')){
		        			        			$form1.find('.form-row-button .checkemail-button').addClass('d-none');
		        			        		}
		        			        		if ($form1.find('.checkpw-button').hasClass('d-none')){
		        			        			$form1.find('.checkpw-button').removeClass('d-none');
		        			        		}
		        			        		if ($('.newsignin-box-inner .checkpw-title').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkpw-title').removeClass('d-none');
		        			        		}
		        			        		if (!$('.newsignin-box-inner .checkemail-title').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        			        		}
		        			        		if (!$('.newsignin-box-inner .guest-checkout-button').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .guest-checkout-button').addClass('d-none')
		        			        		}
		        			        		if (!$('.newsignin-box-inner .checkemail-content').hasClass('d-none')){
		        			        			$('.newsignin-box-inner .checkemail-content').addClass('d-none')
		        			        		}
		        			        		
		        			        		$('.newsignin-box-inner .checkemail-title').addClass('d-none')
		        			        		qbsignInForCustoer();
		        			        		qbforgotPwdClick();
		        			        		qbeditbuttonclick();
		        			        		qbenteredWrongEmail();
		        			        		qbdisguisePassword();
		        			        	} else {
		        			        		
		        			        		var url = util.appendParamsToUrl(Urls.oldRegPage,{
		        			        			'emailid':emailid,
		        			        			'fromPopUpRegister' : 'yes',
		        			        			'quickbuy':true,
		        			        			'pagecontext':pageContext.ns,
		        			        			'itemPnP':itemPnP,
		        			        			'pids':pid,
		        			        			'quantity':quantity,
		        			        			'redirectQuickBuyActivation':true
		        			        			});
		        			        		window.location.href = url;
		        			        	}
		        			        	
		        			        	$(".emailEditButton").on("click", function(){
		        			        		$('.newsignin-box-inner .checkpw-title').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-content').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-field').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkpw-button').addClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-title').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-content').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-field').removeClass('d-none');
		        			        		$('.newsignin-box-inner .checkemail-button').removeClass('d-none');
		        			        	});
		        			        	
		        			        }, 
		        			        error: function (jqXHR, textStatus, errorThrown) {console.log(jqXHR + textStatus + errorThrown); }
		        			    });
		        		});
		        	},
		        	close:function() {
		        		$('body').removeAttr("style");
		        		$('html').removeAttr("style");	
		        	}
		        }
		    });
	    }else{
	    	var addToCartValid = addToCartValidation();
	    	if(addToCartValid){
	    		var url = util.appendParamsToUrl(Urls.oldRegPage,{
	    			'quickbuy':true,
	    			'pids':pid,
	    			'pid':pid,
	    			'pagecontext':pageContext.ns,
	    			'quantity':quantity,
	    			'redirectQuickBuyActivation':true,
	    			'emailid':'',
	    			'fromPopUpRegister' : 'no'
				});
	    		var urlParams = {};
	    		if($('#isWarrantyEligible').val() == 'true'){
	    			var isWarrantySelected = $addingToCartSection.find(".warranty-section input[name='warrantyEnabled']").prop("checked");
	    			if(isWarrantySelected){
		    			urlParams.warrantyFlag = isWarrantySelected;
		    			urlParams.warrantyQuantity = $addingToCartSection.find('.warranty-section input.warranty-count.item').val();
	    			}
	    		}
	    		if($('#isEngravingItem').val() == 'true'){
	    			var $engravingForm = $('form.engraving-form:visible'),
	    			    engravingFormFieldsCount = $engravingForm.find('[name^="dwfrm_engraving_"]').length,
	    			    engravingFieldType, $engravingField, engravingParameterId, engravingFieldCount = 0, engravingTempValue;
	    			if(engravingFormFieldsCount > 0){
		    			jQuery.each($engravingForm.find('[name^="dwfrm_engraving_"]'), function(i,val){
		    				$engravingField = $(val);
		    				engravingFieldType = $engravingField.attr('type');
		    				engravingParameterId = 'engravingField'+(i+1);
		    				if(engravingFieldType != "checkbox"){
		    					if($engravingField.prop('tagName') == 'SELECT'){
		    						urlParams[engravingParameterId] = $engravingField.find('option:selected').index();
		    					}else{
		    						engravingTempValue = $engravingField.val();
		    						urlParams[engravingParameterId] = (engravingTempValue.trim() == '')?'EMPTY':engravingTempValue;
		    					}
		    					engravingFieldCount++;
		    				}
		    			});
		    			urlParams.engravingFlag = $engravingForm.find('#dwfrm_engraving_engravingtac').is(":checked");
		    			urlParams.engravingCount = engravingFieldCount;
	    			}
	    		}
	    		url = util.appendParamsToUrl(url, urlParams);
	    		window.location.href = url;
	    	}
	    }
	});
	
	$(document).off('click.qbpopup').on('click.qbpopup', '#showActivateQBModel:not(.quick-buy-login-class)', function (e) {
	    e.preventDefault();
	    
	    var addToCartValid = addToCartValidation();
	    if(!addToCartValid){return false;}
		
	    pid = $(this).closest('form').find("input[name='pid']").val();
	    plid = $(this).closest('form').find("input[name='plid']").val();
	    quantity = $(this).closest('form').find("input[name='Quantity']").val();
	    itemid = $(this).closest('form').find("input[name='itemid']").val();
	    if (itemid == undefined) {
	    	itemid = $(this).closest('form').find("input[name='uuid']").val();
	    }
		
	    var productAcilabilityQty = Number($(this).closest('form').find('.inventoryRecord').val());  
	    var qty = Number(quantity);
        if (qty > productAcilabilityQty) {
            $(this).closest('form').find('.product-non-availability').show();
        	$(this).closest('form').find('.product-non-availability').css("color", "red");
        	return;
        } else {
        	$(this).closest('form').find('.product-non-availability').hide();
        	$(this).closest('form').find('.product-non-availability').css("color", "black");
        }
	    
	   if ($(this).hasClass('quick-buy-pref-Class')){
		   redirectQuickBuyPrefPage();
		   return;
	   } else if ($(this).hasClass('quick-buy-signin')) {
			var url = Urls.QuickBuyQuickOrder;
			var urlParams = {
					Quantity : quantity,
					itemid : itemid,
					nonLiveTvProductForQB:true
			};
			if (pid != undefined) {
				urlParams.pid = pid;
				var isWarrantyEligible = $('#isWarrantyEligible').val();
				var isEngravingItem = $('#isEngravingItem').val();
				var engdata = '';
			    if(isWarrantyEligible == "true"){
			    	var $addingToCartSection = $('.pdpForm');
			    	var isWarrantySelected = $addingToCartSection.find(".warranty-section input[name='warrantyEnabled']").prop("checked");
			    	if(isWarrantySelected){
			    		urlParams.warrantyQty = $addingToCartSection.find('.warranty-section input.warranty-count.item').val();;
			    		urlParams.isWarrantySelected = isWarrantySelected;
			    		urlParams.warrantyEnabled = isWarrantyEligible;
			    	}
			    }
			    if (isEngravingItem == "true") {
			    	var $engravingForm = $('form.engraving-form:visible');
			    	engdata = $engravingForm.serialize();
				}
			} else {
				urlParams.plid = plid;
			}
		    url = util.appendParamsToUrl(url,urlParams);
		    dialog.open({
                url: url,
                data : engdata,
                options:{
                    dialogClass:'qb-livetv-dialog',
                    open: function(event,ui){
                    	progress.hide();
	        		    validator.init();
	        		    newformstyle.init();
                    	$('body').css('overflow','hidden');
                    	require('./uniform')();
                    	$('#existingCardsCVV').addClass('checkType');
                    	$('#existingCardsCVV').attr('pattern','[0-9]{1,4}');
                    	$('.qb-livetv-dialog .footer .content-heading').css('line-height','2.5rem');
                    	$('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_Paysafe').on('click', function(){
                    		$('.qb-livetv-dialog .footer .content-heading').css('line-height','2.5rem');
                    		var ordertotal = $('#paysafe_ordertotal').text();
                    		$('.updateqbtotal').html(ordertotal);
                    	});
                    	$('#dwfrm_billing_paymentMethods_selectedPaymentMethodID_budgetPay').on('click', function(){
                    		$('.qb-livetv-dialog .footer .content-heading').css('line-height','1.5rem');
                    		var ordertotal = $('#budgetpay_ordertotal').text();
                    		$('.updateqbtotal').html(ordertotal);
                    	});
    	            	$('.qb-livetv-dialog').off('click').on('click', '.confirm-qborder', function(event){
    	            		event.preventDefault();
    	            		var $form = $(this).closest('form');
    	                	//var data = $form.serialize();
    	            		var token = $('#dwfrm_billing_paymentToken').val();
    	            		//var cvv = $('#existingCardsCVV').val();
    	            		var existingCards = $('#existing-paysafe-card').val();
    	            		if($('.safe-budget').length){
    	            			var radioValue = $("input[name='dwfrm_billing_paymentMethods_selectedPaymentMethodID']:checked").val();
    	            		}
    	            		else{
    	            			var radioValue = $("input[name='dwfrm_billing_paymentMethods_selectedPaymentMethodID']").val();
    	            		}
    	            		/*if (cvv == ''){
    	        				var newDiv ='<div class="error" style="color:#c20000;">CVV cannot be blank<div>';
    	        				if($('.cvv-field').find('.error').length == 0) {
    	        					$form.find('.cvv-field').append(newDiv);
    	        				}
    	        				return;
    	        			}*/
    	            		var placeOrderURL = Urls.quickbuyPlaceOrder;
    	            		$.ajax({
    	        		        url: placeOrderURL + "?token=" + token + "&paymentMethod=" + radioValue + "&existingCards=" + existingCards,
    	        		        type: 'POST',
    	        		        beforeSend: function() { progress.show(); },
    	        		        success: function (response) {
    	        		        	progress.hide();
    	        		        	if(response.success && response.Orderno!=""){
    		        		        	if ($('#quickBuyConfirmation').hasClass('d-none')){
    		        		        		$('#quickBuyForm').addClass('d-none');
    		        		        		$('#quickBuyConfirmation').removeClass('d-none');
    		        		        		$("#qborder-number").html(response.Orderno);
    		        		        		var gtmPurchase = JSON.stringify(response.gtmPurchase),
    		        		        		gtmProducts = JSON.stringify(response.gtmProducts);
    		        		        		$("#quickBuyConfirmation #gtmDataHolder").attr('data-gtmPurchase', gtmPurchase);
    		        		        		$("#quickBuyConfirmation #gtmDataHolder").attr('data-gtmProducts', gtmProducts);
											/*window.dataLayer.push({
												'event':'purchase',
												'ecommerce': {
													'purchase': {
														'actionField': JSON.parse(gtmPurchase),
														'products': JSON.parse(gtmProducts)
													}
												}
											});*/
											orderconfirmation.init();
											dataLayer.push({'event': 'QBOrderConfirmation'});
    		        		        	}
    	        		        	}
    	        		        	else{
    	        		        		if(response.errorCode >= 0 && response.errorCode <= 11 ){     		        			
    	        		        			if(response.errorCode == 11){
    	        		        				if ($('.budgetPaySpecificError').hasClass('d-none')){
        			        		        		$('.budgetPaySpecificError').removeClass('d-none');
        			        		        	}     	        		        				
    	        		        			}
    	        		        			else{     	        		        				
    	        		        				if ($('.budgetPaySomethingError').hasClass('d-none')){
        			        		        		$('.budgetPaySomethingError').removeClass('d-none');
        			        		        	}
    	        		        			}
    	        		        			if ($('#activation-error').hasClass('d-none')){
    	        		                		$('#activation-error').removeClass('d-none');
    	        		                		$("#activation-error").html("Something didn't work. Please contact customer care on 0344 375 2525.");	     
    	        			        		}
    	        		        		}
    	        		        	}
    	        		        	$("#qbConfirm-close").on('click', function(){
    	        		        		dialog.close();
    	        		        	});
    	        		        }
    	            		});
    	            	});

    	            	$('.qb-livetv-dialog').on('click', '.settings', function(event){
    	                    var url = Urls.restoreqbbasketfromsession;
    	                    $.ajax({
    	                        url: url,
    	                        type: 'POST',
    	                        success: function (response) {
    	                            //do something
    	                        }
    	                    });
    	                });

                    },
                    close:function() {
	        			var url = Urls.restoreqbbasketfromsession; 
		        		$('body').removeAttr("style");
		        		$('html').removeAttr("style");
		        		$('body').css('overflow','x-hidden');
		        		$.ajax({
		    		        url: url,
		    		        type: 'POST',
		    		        success: function (response) {
		    		        	//do something
		    		        }
		        		});
		        		url = window.location.href.split('#')[0];
    	        		window.location.href = url.split('?')[0];
    	        	}
                  }
            });
	   } else {
		    var url = Urls.quickbuyactivatepage;
		    var urlParams = {
					Quantity : quantity,
					itemid : itemid,
					nonLiveTvProductForQB:true
			};
		    urlParams.pid = pid;
			var isWarrantyEligible = $('#isWarrantyEligible').val();
			var isEngravingItem = $('#isEngravingItem').val();
			var engdata = '';
		    if(isWarrantyEligible == "true"){
		    	var $addingToCartSection = $('.pdpForm');
		    	var isWarrantySelected = $addingToCartSection.find(".warranty-section input[name='warrantyEnabled']").prop("checked");
		    	if(isWarrantySelected){
		    		urlParams.warrantyQty = $addingToCartSection.find('.warranty-section input.warranty-count.item').val();;
		    		urlParams.isWarrantySelected = isWarrantySelected;
		    		urlParams.warrantyEnabled = isWarrantyEligible;
		    	}
		    }
		    if (isEngravingItem == "true") {
		    	var $engravingForm = $('form.engraving-form:visible');
		    	engdata = $engravingForm.serialize();
			}
		    urlParams.plid = pid;
		    urlParams.qbForNonLiveTvProduct = true;
		    urlParams.redirectQuickBuyActivation = true;
		    //url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    url = util.appendParamsToUrl(url,urlParams);
		    //url = util.appendParamToURL(url,'pageContext',pageContext.ns);
		    progress.show();
		    dialog.open({ 
		        url: url,
		        data : engdata,
		        options:{
		        	dialogClass:'showActivateQBModel',
		        	closeOnEscape: false,
		        	open: function(event,ui){ 
		        		var addressjs = require('./pages/checkout/address'),
		        	 	accountjs = require('./pages/account'),
		        	 	paysafe = require('./paysafe');
		        		progress.hide();
		        		$('body').css('overflow','hidden');
		        		$('.credit-card-box').css('max-width','unset');
		        		require('./uniform')();
		        		whatiscvv();
		        		addressjs.init();
		        		accountjs.init();
		        		validator.init();
	        		    newformstyle.init();
	        		    addWarrantyQB();
	        		    window.paysafeForQB = true;
	        		    paysafe.init();
		        	},
		        	close:function() {
	        			var url = Urls.restoreqbbasketfromsession; 
		        		$.ajax({
		    		        url: url,
		    		        type: 'POST',
		    		        success: function (response) { 
		    		        	//do something
		    		        }
		        		});
		        		url = window.location.href.split('#')[0];
    	        		window.location.href = url.split('?')[0];
		        		
		        	}
		        }
		    });
	    }
	});
}

function addWarrantyQB(){
	/*### Make Ajax call when you select the warrantyEnabled check box ###*/
	$('.order-totals-box .warranty-box #warrantyEnabled').on('click', function(){
		if($(this).is(':checked')) {
			var warrantyPrice = parseFloat($('.showActivateQBModel .activate-quickbuy-box .qbwarrantydetails .warranty-dialog').attr('data-warrantycost')),
			    url = Urls.qbAddWarranty;
			warrantyPrice = $.trim(warrantyPrice);
			url = util.appendParamsToUrl(url,{
				'pid':pid,
				'warrantyQty':quantity,
				'wprice' : warrantyPrice,
				'fromQB':true
			});
			progress.show();
			$.ajax({
				type: "POST",
				url: url,
				success: function(data) {
					try {
						progress.hide();
						$(".showActivateQBModel .activate-quickbuy-box #includeOrderBasket").html(data);
					} catch (b) {
						console.error(b);
					}
				},
				error: function(f) {
					console.error(f.statusText + " " + f.status);
				}
			});
		}
	});
}

function whatiscvv(){
	$('.whatiscvv').click(function (e) {
	    if($( "#whatiscvv-modelbox" ).hasClass('d-none')){
	    	$( "#whatiscvv-modelbox" ).removeClass('d-none'); 
	    }
	    else{
	    	$( "#whatiscvv-modelbox" ).addClass('d-none'); 
	    }
	});
}

function tooltipInitialize(){
	if (window.matchMedia("(max-width: 767px)").matches){
		$('.tooltip').each(function(index){
	        $(this).tooltip({
	            position: {
	                my: "bottom-15",
	                at: "bottom-15"
	            },
	            content: function () { return $(this).prop('title'); },
	            //show: "slideDown",  
	            open: function(event, ui){
	            	var _elemId = ui.tooltip.attr('id');
	            	$('.ui-tooltip').each(function(index){
	            		if(this.id!=_elemId){
	            			$(this).remove();
	            		}
	            	});
	            	var width = $(window).width(); 
	                $('.ui-tooltip').attr('max-width',width+'px');
	                if(width<350){
	                	$('#'+_elemId).css('left','0px');
	                }else{
	                	$('#'+_elemId).css('left','5px');
	                }
	                setTimeout(function(){ if($('#'+_elemId).is(":visible")){$('#'+_elemId).remove();} }, 5000);
	            },
	            close: function(event, ui) {
	            	ui.tooltip.hover(function()  {
	                    $(this).remove();
	                    //$(this).stop(true).fadeTo(500, 1);
	                },
	                function() {
	                    $(this).remove();
	                });
	            }
	        });
	    });
	}else{
		$('.tooltip').each(function(index){
	        $(this).tooltip({
	            position: {
	                my: "left+45 top-5",
	                at: "left+45 top-5"
	            },
	            content: function () { return $(this).prop('title'); },
	            //show: "slideDown",  
	            open: function(event, ui){
	            	var _elemId = ui.tooltip.attr('id');
	            	$('.ui-tooltip').each(function(index){
	            		if(this.id!=_elemId){
	            			$(this).remove();
	            		}
	            	});
	                $('#'+_elemId).hover(function(){
	                    $(this).remove();
	                });
	            },
	            close: function(event, ui) {
	                ui.tooltip.hover(function()  {
	                    $(this).remove();
	                    //$(this).stop(true).fadeTo(500, 1);
	                },
	                function() {
	                    $(this).remove();
	                });
	            }
	        });
	    });
	}
}

function checkFieldsIntiator(){
	$('#dwfrm_quickbuyaddress_customer_firstname').addClass('checkType');
	$('#dwfrm_quickbuyaddress_customer_firstname').attr('pattern','[A-Za-z\s]{1,50}');
	$('#dwfrm_quickbuyaddress_customer_lastname').addClass('checkType');
	$('#dwfrm_quickbuyaddress_customer_lastname').attr('pattern','[A-Za-z\s]{1,50}');
	$('#dwfrm_quickbuyaddress_customer_mobile').addClass('checkType');
	$('#dwfrm_quickbuyaddress_customer_mobile').attr('pattern','[0-9]{1,11}');
	$('#existing-paysafe-cvv').addClass('checkType');
	$('#existing-paysafe-cvv').attr('pattern','[0-9]{1,4}');
	$('#dwfrm_quickbuyaddress_billingAddress_address_postCode').addClass('checkType');
	$('#dwfrm_quickbuyaddress_billingAddress_address_postCode').attr('pattern','[A-Za-z0-9 ]{1,8}');
	$('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').addClass('checkType');
	$('#dwfrm_quickbuyaddress_shippingAddress_address_postCode').attr('pattern','[A-Za-z0-9 ]{1,8}');
	$('.mobile-no').find('.required-indicator').addClass('tooltip');
	$('.mobile-no').find('.required-indicator').attr('title','Share your phone number for a seamless shopping experience through our Website and TV.');
	$('.address-alias').find('.required-indicator').addClass('tooltip');
	$('.address-alias').find('.required-indicator').attr('title','example - home, office, etc.');
	tooltipInitialize();
}

function checkFieldsType(){
	$('form').find('input').on('keypress', function (e) {
		var keyCode = e.keyCode || e.which;
		if ($(this).is(':visible')) {
			var classes = ($(this).attr('class') != null) ? $(this).attr('class').search('checkType') : -1;
			if (classes >= 0) {
				var regexLength = this.pattern.split('{');
				var regex = new RegExp(regexLength[0]);
				var lengthVal = regexLength[1].replace('}', '');
				var lengthValues = lengthVal.split(',');
				var minLength = lengthValues[0];
				var maxLength = lengthValues[1];
				var isValid = regex.test(String.fromCharCode(keyCode));
				var isValid1 = $(this).val().length+1;
				if (!isValid) {
					//$(this).css("border-bottom", "3px solid #FF0000");
					return isValid;
				}else{
					//$(this).css("border-bottom", "3px solid #00FF00");
				}
				if (isValid1 < minLength) {
					//$(this).css("border-top", "3px solid #FF0000");
				}else{
					//$(this).css("border-top", "3px solid #00FF00");
				}
				if(isValid1 > maxLength){
					//$(this).css("border-top", "3px solid #FF0000");
					return false;
				}
			}
		}
	});
}

var qbfornonlivetv = {
	init: function () {
		initializeEvents();
	},
	addWarrantyQB: function(){
		addWarrantyQB();
	}
};
module.exports = qbfornonlivetv;
