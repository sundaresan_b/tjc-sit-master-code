'use strict';

var bonusProductsView = require('./bonus-products-view'),
    util = require('./util'),
    TPromise = require('promise');

var minicart = {
    init: function () {
        this.$el = $('.header-bag');
    },
    /**
     * @function
     * @description Shows the given content in the mini cart
     * @param {String} A HTML string with the content which will be shown
     */
    show: function (html) {
    	this.$el.find('.header-bag-link').html(html);
        this.$el.addClass('bag-with-products');
        this.init();
        bonusProductsView.loadBonusOption();
    },
    /**
     * @function
     * @description Removes an item from the minicart
     */
    removeItem: function (e) {
        var that = this,
            $button = $(e.currentTarget),
            $form = $button.closest('form'),
            action = $form.attr('action');

        TPromise.resolve($.ajax({
            type: 'POST',
            url: util.ajaxUrl(action),
            data: $form.serialize()
        })).then(function (response) {
            that.show(response);
        });
    }
};

module.exports = minicart;
