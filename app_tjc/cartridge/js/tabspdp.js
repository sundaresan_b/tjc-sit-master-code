'use strict';

function tabsrecomendation() {

    $('.section-recommendations-tabs-link').on('click', function () {
        if ($(this).hasClass('section-recommendations-tabs-link-alsolike')) {
            $('.section-recommendations-tabs-link-alsobought').removeClass('actived');
            $('.section-recommendations-containers-wrapper-alsobought').removeClass('actived');
            //
            $(this).addClass('actived');
            $('.section-recommendations-containers-wrapper-alsolike').addClass('actived');
            window.initRecommendationsCarousel();
        }
        if ($(this).hasClass('section-recommendations-tabs-link-alsobought')) {
            $('.section-recommendations-tabs-link-alsolike').removeClass('actived');
            $('.section-recommendations-containers-wrapper-alsolike').removeClass('actived');
            //
            $(this).addClass('actived');
            $('.section-recommendations-containers-wrapper-alsobought').addClass('actived');
            window.initRecommendationsCarousel();
        }
    });
}

function expandcontent() {

    var heightContent = $('.moredetais-expand').height(),
        heightContentMax = 240;

    if ($('.pdp-info-tabs-menu-details').hasClass('actived')) {

        if (heightContent > heightContentMax) {
            $('.moredetais-expand-btn').show();

        } else {
            $('.moredetais-expand-btn').hide();
        }
    }

    $('.moredetais-expand-btn').on('click', function () {
        $('.pdp-acordeon-content.pdp-acordeon-content-details.moredetais-expand')
            .find('.pdp-acordeon-content-box').toggleClass('moredetais-expand-max');
        $('.moredetais-expand-btn-wrapper').toggleClass('actived');

    });
}

// Module public functions
module.exports = {
    init: function () {
        tabsrecomendation();
        expandcontent();
    }
};
