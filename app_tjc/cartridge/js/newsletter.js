/*global cmCreateElementTag: false */
'use strict';

var util = require('./util');

var newsletter = {
    init: function () {
        var isForPopUp = ($('.email-subscription-popUp:visible').find('.footer-newsletter .newsletter-form').length > 0),
            $newsletterForm = (isForPopUp)?$('.email-subscription-popUp:visible').find('.footer-newsletter .newsletter-form'):$('.xlt-footer:visible').find('.footer-newsletter .newsletter-form'),
            postUrl = $newsletterForm.attr('action');

        $newsletterForm.find('.footer-newsletter-submit').off('click.newsletter').on('click.newsletter', function (event) {
        	var isFromPopUp = ($(event.target).closest('.email-subscription-popUp').length > 0)?true:false,
        	    $content = $(event.target).closest('.footer-newsletter-form');
        	$newsletterForm = $(event.target).closest('.newsletter-form');
            event.preventDefault();
            if ($newsletterForm.valid()) {
                $.ajax({
                    type: 'POST',
                    url: util.ajaxUrl(postUrl),
                    data: $content.find('.footer-newsletter form:visible').serialize()
                }).done(function (response) {
                    var isSuccess = $(response).filter('.footer-newsletter-message').length > 0,
                        email;
                    if (isSuccess) {
                        this.fadeOutNewsletter(response, $content);
                        util.setCookie("tjcEmailSubscription", "subscribed", 365);
                        if(isFromPopUp){
                        	$('.email-subscription-popUp .footer-newsletter-content .footer-newsletter-info').find('.initial').addClass('d-none');
                        	$('.email-subscription-popUp .footer-newsletter-content .footer-newsletter-info').find('.success').removeClass('d-none');
                        	$('.email-subscription-popUp .footer-newsletter-content .footer-newsletter-form').find('.initial').addClass('d-none');
                        	$('.email-subscription-popUp .footer-newsletter-content .footer-newsletter-form').find('.success').removeClass('d-none');
                        	$('.email-subscription-popUp .xlt-emailSubscriptionPopUp .footer-newsletter-close').off('click.close-popUp').on('click.close-popUp', function(e){
                        		$('.email-subscription-popUp .ui-dialog-titlebar-close').trigger('click');
                        	});
                        }
                        $content.find('.input-text').each(function () {
                            var $this = $(this),
                                value = $this.val();

                            if (value) {
                                email = value;
                                return false;
                            }
                        });
                        if (email && cmCreateElementTag !== undefined) {
                            cmCreateElementTag(email, 'NEWSLETTER SUBSCRIPTION');
                        }
                    } else {
                    	$content.find('.footer-newsletter').replaceWith(response);
                    }
                }.bind(this));
            }
        }.bind(this));

    },
    /**
     * @function
     * @description Fades out the newsletter
     */
    fadeOutNewsletter: function (response, $content) {
        var $newsletterForm = $content.find('.footer-newsletter');
        $newsletterForm.fadeOut('slow', function () {
            $newsletterForm.replaceWith(response);
            this.fadeInSuccessMessage($content);
        }.bind(this));

    },
    /**
     * @function
     * @description Fades in the SuccessMessage
     */
    fadeInSuccessMessage: function ($content) {
        var $newsletterMessage = $content.find('.footer-newsletter-message');
        $newsletterMessage.fadeIn('fast');

    }
};

module.exports = newsletter;
