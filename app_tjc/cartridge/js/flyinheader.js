'use strict';

var $ = require('jquery');

var initialized = false,
    $document = $(document),
    $html = $('html');

function flyinheader() {
	
	if($('#opener').length > 0){
		if ($('.header-addname .header-account-link').hasClass('flyinheader-account-trigger')){
			$('.header-addname .header-account-link').removeClass('flyinheader-account-trigger');
		}
	}

    // MENU FLYIN
    var openCloseFlyinHeader = function (open) {
        if (open) {
            $html.addClass('flyinheader-active');
            $('.header-wrapper').css('position', '').addClass('menu-opened');
            $('.header-content').css('position', 'unset');
            $('.header-menu').css('position', 'unset');
            $('.navigation').css('display', 'flex');
            if(pageContext.ns == "cart" && $('.cart-table tr').length > 0){
            	$('.wrapper.pt_cart').css('overflow','unset');
            }else{
            	$('.wrapper.pt_cart').css('overflow','hidden');
            }
        } else {
            $html.removeClass('flyinheader-active');
            $('.header-wrapper').css('position', '').removeClass('menu-opened');
            $('.header-content').css('position', '');
            $('.header-menu').css('position', '');
            $('.navigation').css('display', '');
            if(pageContext.ns == "cart" && $('.cart-table tr').length > 0){
            	$('.wrapper.pt_cart').css('overflow','unset');
            }else{
            	$('.wrapper.pt_cart').css('overflow','hidden');
            }
        }
    };

    $(document).on('click', '.flyinheader-trigger,.flyinheader-close-trigger', function (e) {
        openCloseFlyinHeader($(e.currentTarget).hasClass('flyinheader-trigger'));
    });

    window.matchMedia('(max-width: 1140px)').addListener(function (e) {
        if (!e.matches && $html.hasClass('flyinheader-active')) {
            openCloseFlyinHeader(false);
        }
    });

    // ACCOUNT FLYIN
    $document.on('click', '.flyinheader-account-trigger', function () {
    	if(pageContext.ns == "cart" && $('.cart-table tr').length > 0){
        	$('.wrapper.pt_cart').css('overflow','unset');
        }else{
        	$('.wrapper.pt_cart').css('overflow','hidden');
        }

        $('body').css('overflow', 'hidden').css('height', '100%');
        $('html').css('overflow', 'hidden').css('height', '100%');
        $('.header-wrapper').css('position', '').addClass('account-opened');
        $('.flyin-account-wrapper').css('display', 'flex');
		$('.tjc-plus-header-slot').addClass('Remove-for-acount-open');

    }).on('click', '.flyinheader-close-trigger', function () {

        $('body').css('overflow', '').css('height', '');
        $('html').css('overflow', '').css('height', '');
        $('.header-wrapper').css('position', '').removeClass('account-opened');
        $('.flyin-account-wrapper').css('display', '');
		$('.tjc-plus-header-slot').removeClass('Remove-for-acount-open');
		if(pageContext.ns == "cart" && $('.cart-table tr').length > 0){
        	$('.wrapper.pt_cart').css('overflow','unset');
        }else{
        	$('.wrapper.pt_cart').css('overflow','hidden');
        }

    });

    initialized = !initialized;

}

module.exports = {
    init: function () {
        //if (!initialized) {
            flyinheader();
        //}
    }
};
