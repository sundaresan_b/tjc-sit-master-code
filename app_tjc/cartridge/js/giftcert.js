'use strict';

var ajax = require('./ajax'),
    minicart = require('./minicart'),
    util = require('./util');

var setAddToCartHandler = function (e) {
    e.preventDefault();
    var form = $(this).closest('form'),

        options = {
            url: util.ajaxUrl(form.attr('action')),
            method: 'POST',
            cache: false,
            data: form.serialize()
        };
    /*jslint unparam: true */
    $.ajax(options).done(function (response) {
        var id,
            $errorEl;
        if (response.success) {
            ajax.load({
                url: Urls.minicartGC,
                data: {lineItemId: response.result.lineItemId},
                callback: function (resp) {
                    minicart.show(resp);
                    form.find('input,textarea').val('');
                }
            });
        } else {
            form.find('span.error').hide();
            for (id in response.errors.FormErrors) {
                if (response.errors.FormErrors.hasOwnProperty(id)) {
                    $errorEl = $('#' + id).addClass('error').removeClass('valid').next('.error');
                    if (!$errorEl || $errorEl.length === 0) {
                        $errorEl = $('<span for="' + id + '" generated="true" class="error" style=""></span>');
                        $('#' + id).after($errorEl);
                    }
                    $errorEl.text(response.errors.FormErrors[id].replace(/\\'/g, '\'')).show();
                }
            }
        }
    }).fail(function (xhr, textStatus) {
        // failed
        if (textStatus === 'parsererror') {
            /*eslint-disable no-alert */
            window.alert(Resources.BAD_RESPONSE);
            /*eslint-enable no-alert */
        } else {
            /*eslint-disable no-alert */
            window.alert(Resources.SERVER_CONNECTION_ERROR);
            /*eslint-enable no-alert */
        }
    });
    /*jslint unparam: false */
};

exports.init = function () {
    $('#AddToBasketButton').on('click', setAddToCartHandler);
};
