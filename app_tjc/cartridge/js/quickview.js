/*jslint nomen: true */
'use strict';

var dialog = require('./dialog'),
    product = require('./pages/product'),
    util = require('./util'),
    carousel = require('./carousel'),
    risingAuctions = require('./risingauctions'),
    pdpoverlay = require('./pdpoverlay'),
    validator = require('./validator'),
    quickviewimage = require('./pages/product/quickviewimage'),
    _ = require('lodash');



var makeUrl = function (url, source, productListID) {
    if (source) {
        url = util.appendParamToURL(url, 'source', source);
    }
    if (productListID) {
        url = util.appendParamToURL(url, 'productlistid', productListID);
    }
    return url;
};



/* Public Quickview Object */
/* *********************** */
var quickview = {

    init: function () {
        // keep reference to this
        var that = this;

        // bind click events to quickview links
        $('.product-tile .quickview:not(initialized), .product-tile .variant-addtocart, .complete-the-set .quickview:not(initialized)').on('click', function (e) {
            var $this = $(this),
                cgid = $this.closest('.product-tile').data('cgid'),
                options = {};

            e.preventDefault();
            if ($this.data('url')) {
                $this.attr('href', $this.data('url'));	
            }	
            options.url = $this.attr('href');
            options.source = 'quickview';
            options.classList = ($(this).parents('.complete-the-set').length >= 1)?'new_pdp-quick_view':'quick_view';

            // if cgid is present add it to the add to cart form after quickview is opened
            if (cgid) {
                options.callback = function () {
                    $('#QuickViewDialog form.pdpForm')
                        .append($('<input id="quickViewCgid" type="hidden" name="cgid" value="' + cgid + '"/>'));
                    risingAuctions.openQuickview();
                };
            } else {
                options.callback = function () {
                    risingAuctions.openQuickview();
                };
            }
            that.show(options);
        }).addClass('initialized');
    },

    onQuickviewOpen: function () {
    	carousel.init();
    	product.initializeEvents();
    	if(pageContext.type == "product"){
    		quickviewimage.loadImageComponents();
    	}
        pdpoverlay.warrantyQuantitySelection();
        require('./uniform')();
        if ($('.search-result-content').data('issearchwithquery')) {
            $('#QuickViewDialog form.pdpForm')
                .append($('<input id="quickViewIsSearch" type="hidden" name="issearch" value="true"/>'));
        }
        util.executeJqueryReadyCallbacks();
    },

    /**
     * @description show quick view dialog
     * @param {Object} options
     * @param {String} options.url - url of the product details
     * @param {String} options.source - source of the dialog to be appended to URL
     * @param {String} options.productlistid - to be appended to URL
     * @param {Function} options.callback - callback once the dialog is opened
     */
    show: function (options) {
        // check if dialog container already exists and create one if necessary
        if (!this.exists()) {
            this.$container = $('<div/>').attr('id', 'QuickViewDialog').appendTo(document.body);
        }

        // get product URL
        var quickviewURL = makeUrl(options.url, options.source, options.productlistid);

        // load data into quickview container
        dialog.open({
            target: this.$container,
            url: quickviewURL,
            options: {
                width: 706,
                dialogClass: options.classList,
                title: 'Product Quickview',
                open: function () {
                    $(window).on('resize.quickview', _.debounce(function () {
                        this.$container.dialog('option', 'position', {my: 'center', at: 'center', of: window});
                    }.bind(this), 100));
                    if (typeof options.callback === 'function') {
                        options.callback();
                    }
                    this.onQuickviewOpen();
                    validator.init();
                 $('.field input').on('blur', function() {
					  $(this).siblings(".label").removeClass('focus-active');
					      if( $(this).val().length > 0 ) {
					        $(this).siblings(".label").addClass('focus-active');   
					    } 
					});
                    
				  $('.field input').on('focus', function() {
				   $(".login-box-content .error").hide();   
				   $(this).siblings(".label").addClass('focus-active'); 
				});
                }.bind(this),
                close: function () {
                    $(window).off('resize.quickview');
                    risingAuctions.closeQuickview();
                    $('body').css('overflow-y', 'visible');
                }
            }
        });
    },

    exists: function () {
        return this.$container && (this.$container.length > 0);
    }
};
module.exports = quickview;