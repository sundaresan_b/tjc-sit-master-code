/**
 * This module is used to initialize [UniformJS]{@link https://uniformjs.com/}.
 */
'use strict';

module.exports = function () {
    var sUniformStyled = 'uniformstyled';
    $(['input:checkbox', 'input:radio', '.opc .existing-card-section #existing-paysafe-card']).each(function () {
        $(this + ':not(.' + sUniformStyled + ')').not('.no-uniform').addClass(sUniformStyled).uniform();
    });
};