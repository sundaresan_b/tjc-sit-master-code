'use strict';

/**
 * @private
 * @function
 * @description Binds event to the place holder (.blur)
 */
function initializeEvents() {
    $('.simple-search-q').on('focus', function () {
        var input = $(this);
        if (input.val() === input.attr('placeholder')) {
            input.val('');
        }
    }).on('blur', function () {
        var input = $(this);
        if (input.val() === '' || input.val() === input.attr('placeholder')) {
            input.val(input.attr('placeholder'));
        }
    }).blur();

    $('.header-search button').on('click', function (e) {
        e.preventDefault();
        var shopContext = $('body').data('shopcontext') || 'webshop',
            $searchForm = $(this).parents('form'),
            $searchInput = $(this).closest('form').find('.simple-search-q'),
            queryValue = $searchInput.val().trim(),
            queryPlaceholder = $searchInput.attr('placeholder');

        // do not fire search if we have no value or just the placeholder
        if (!queryValue.length || queryValue === queryPlaceholder) {
            return;
        }

        // submit shop context
        if (shopContext === 'webshop') {
            $searchForm.append($('<input>', {'type' : 'hidden', 'name' : 'cgid', 'value' : 'shop-navigation'}));
        } else {
            $searchForm.append($('<input>', {'type' : 'hidden', 'name' : 'cgid', 'value' : 'auction-navigation'}));
        }

        // fire search
        $searchForm.submit();
    });
}

exports.init = initializeEvents;
