'use strict';


function stickyheader() {
    $(window).on('scroll', function () {
	if($('.wrapper.pdp_redesign').length > 0){
        var pageTop = $(window).scrollTop(),
            screenWidth = $(window).width(),
            breakpointScreen = 767,
            elementPosition = 0,
            elementPositionDesktop = $('.pdp-sticky-mark-desktop:visible').offset().top,
            elementPositionMobile = $('.pdp-sticky-mark-mobile:visible').offset().top,
            selectedSize  = $('.product-variations .swatches.size .selected:visible').text(),
            selectedQuantityClassesA = '.product-add-to-cart .quantity-table',
            selectedQuantityClassesB = ' .quantity-cell select:visible option:selected',
            selectedQuantity = $(selectedQuantityClassesA + selectedQuantityClassesB).text(),
            pdpSizeInfoContainer = $('.pdp-sticky-info-size:visible'),
            pdpQuantityInfoContainer = $('.pdp-sticky-info-quant:visible');

        if (screenWidth <= breakpointScreen) {
            elementPosition = elementPositionMobile;
        } else {
            elementPosition = elementPositionDesktop;
        }

        if (pageTop >= elementPosition) {
            $('.wrapper:not(.pdp_redesign) .pdp-sticky').addClass('pdp-sticky-on').removeClass('pdp-sticky-off');

            if (selectedSize !== '') {
                pdpSizeInfoContainer.show();
                $('.pdp-sticky-size').text(selectedSize);
            } else {
                pdpSizeInfoContainer.hide();
            }

            if (selectedQuantity !== '') {
                pdpQuantityInfoContainer.show();
                $('.pdp-sticky-quantity').text(selectedQuantity);
            } else {
                pdpQuantityInfoContainer.hide();
            }
        } else {
            $('.wrapper:not(.pdp_redesign) .pdp-sticky').addClass('pdp-sticky-off').removeClass('pdp-sticky-on');
        }
        
        if (pageTop >= (elementPosition - window.innerHeight)) {
            //$('.wrapper.pdp_redesign .pdp-sticky').addClass('pdp-sticky-off').removeClass('pdp-sticky-on');
            if($('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').hasClass('cart-variants-group')){
                //$('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').addClass('crossed');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('cart-variants-group');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection a.swatchanchor:visible').removeClass('sticky-variant-anchor');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection').removeClass('sticky-selected');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size li:not(.unselectable):visible').removeClass('sticky-variant-selection');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart:visible').css('margin-top','10px');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart .pdp-sticky:visible').removeClass('remove-box-shadow');
            }else{
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart .pdp-sticky:visible').removeClass('remove-box-shadow');
            }
        }else{
        	//$('.wrapper.pdp_redesign .pdp-sticky').addClass('pdp-sticky-on').removeClass('pdp-sticky-off');
            if($('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').hasClass('cart-variants-group')){
                var height = $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').outerHeight();
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper:visible').removeClass('crossed');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size li:not(.unselectable):visible').addClass('sticky-variant-selection');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection a.swatchanchor:visible').addClass('sticky-variant-anchor');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart .pdp-sticky:visible').addClass('remove-box-shadow');
                $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .product-add-to-cart:visible').css('margin-top',(height+15)+'px');
                stickyVariantChanges();
            }
        }
        
        if ((pageTop >= elementPosition + 150) && !(pageTop >= $('.custom-slider:eq(0)').offset().top - window.innerHeight)) {
        	$('.wrapper.pdp_redesign .pdp-sticky').addClass('pdp-sticky-on').removeClass('pdp-sticky-off');
        } else {
        	$('.wrapper.pdp_redesign .pdp-sticky').addClass('pdp-sticky-off').removeClass('pdp-sticky-on');
        }
	}
    });
    
}

function stickyVariantChanges(){
    // click on swatch - should store selected variant
    $('.pdp-main').off('click.stickyanchor').on('click.stickyanchor', '.pdp-redesign-colsize .swatchanchor.sticky-variant-anchor:visible', function (e) {
        e.preventDefault();
        if ($(this).parents('li').hasClass('unselectable') || $(this).parents('li').hasClass('sticky-selected')) {
            return;
        }
        $('.pdp_redesign .pdp-container-addtocart .pdp-box-addtocart .variations-wrapper .xlt-pdpVariations .size-variations .value .swatches.size .sticky-variant-selection').removeClass('sticky-selected');
        $(this).parents('li.sticky-variant-selection').addClass('sticky-selected');
    });
}


// Module public functions
module.exports = {
    init: stickyheader,
    stickyVariantChanges: stickyVariantChanges,
};
