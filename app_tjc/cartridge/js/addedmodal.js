'use strict';

function init() {
	var url = Urls.cartcount;       

    $.ajax({            	
        url: url,
        type: 'GET',
        success: function (response) {         	    	  
    		var res = response; 
	    	$('.added-count').text(res.cartcount);
	    	$('.addedModal').show();
	    	$('.addedModal .overlay-container').removeClass('overlay-container-off');
    		$('#header').addClass('addtocart-overlay-active');

        },        

        error: function (jqXHR, textStatus, errorThrown) {

        	console.log(jqXHR + textStatus + errorThrown); 

    	}

    });

    

    // Close the Overlay by clincking in the "X" button

    $('.addedoverlay-close').on('click', function () {
        $('.addedModal').hide();
        $('.header').removeClass('addtocart-overlay-active');
        $('.addedModal .overlay-container').removeClass('overlay-container-off');
    });
    $('.addedModal .overlay-container').addClass('overlay-container-on');

}

// Module public functions
module.exports = {
    init: init
};
