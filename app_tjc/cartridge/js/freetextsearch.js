'use strict';

var freetextsearch;


function initializeEvents() {
    var $main = $('#main');
    $main.on('keyup', '.freetextsearchinput', function () {
        var searchTerm = $(this).val().trim().toLowerCase().replace(/[\-\[\]{}()*+?.,\\^$|#\s]/g, ''),
            $refinementListItems = $(this).closest('.freetextsearch').siblings('.refinement-list').children(':not(.notfound)'),
            searchTermRegEx = new RegExp('^' + searchTerm, 'i'),
            notVisibleCount = 0;

        $refinementListItems.each(function () {
            var refinementText =
                $(this).children('a').text().trim().toLowerCase().replace(/[\-\[\]{}()*+?.,\\^$|#\s]/g, '');
            if (!searchTermRegEx.test(refinementText)) {
                $(this).slideUp(0);
                notVisibleCount++;
            } else {
                $(this).slideDown(0);
            }
        });

        if (notVisibleCount >= $refinementListItems.length) {
            $($(this).closest('.freetextsearch').siblings('.refinement-list').children('.notfound')).slideDown(0);
        } else {
            $($(this).closest('.freetextsearch').siblings('.refinement-list').children('.notfound')).slideUp(0);
        }
    });
}

freetextsearch = {
    init: function () {
        initializeEvents();
    }
};

module.exports = freetextsearch;