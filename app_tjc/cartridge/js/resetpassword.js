'use strict';

function init() {
    if ($('.reset-password-modal').length > 0) {
        setTimeout(function () {
            $('.overlay-container').fadeOut();
        }, 1200);
    }
}

// Module public functions
module.exports = {
    init: function () {
        init();
    }
};

