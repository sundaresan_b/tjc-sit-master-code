'use strict';

var ajax = require('./ajax'),
    util = require('./util');

var selectedList = [],
    maxItems = 1,
    bliUUID = '';

/**
 * @private
 * @function
 * description Gets a list of bonus products related to a promoted product
 */
function getBonusProducts() {
    var o = {},
        i,
        len,
        p,
        a,
        alen,
        bp,
        opt;
    o.bonusproducts = [];

    for (i = 0, len = selectedList.length; i < len; i++) {
        p = {
            pid: selectedList[i].pid.toString(),
            qty: selectedList[i].qty,
            options: {}
        };
        bp = selectedList[i];
        if (bp.options) {
            for (a = 0, alen = bp.options.length; a < alen; a++) {
                opt = bp.options[a];
                p.options = {optionName: opt.name, optionValue: opt.value};
            }
        }
        o.bonusproducts.push({product: p});
    }
    return o;
}

var selectedItemTemplate = function (data) {
    var attributes = '',
        attrID,
        attr;
    for (attrID in data.attributes) {
        if (data.attributes.hasOwnProperty(attrID)) {
            attr = data.attributes[attrID];
            attributes += '<li data-attribute-id="' + attrID + '">\n';
            attributes += '<span class="display-name">' + attr.displayName + '</span>: ';
            attributes += '<span class="display-value">' + attr.displayValue + '</span>\n';
            attributes += '</li>';
        }
    }
    attributes += '<li class="item-qty">\n';
    attributes += '<span class="display-name">Qty</span>: ';
    attributes += '<span class="display-value">' + data.qty + '</span>';
    return [
        '<li class="selected-bonus-item" data-uuid="' + data.uuid + '" data-pid="' + data.pid + '">',
        '<i class="remove-link fa fa-remove" title="Remove this product" href="#"></i>',
        '<div class="item-name">' + data.name + '</div>',
        '<ul class="item-attributes">',
        attributes,
        '<ul>',
        '<li>'
    ].join('\n');
};

// hide swatches that are not selected or not part of a Product Variation Group
var hideSwatches = function () {
    $('.bonus-product-item .swatches li').not('.selected').not('.variation-group-value').hide();
    // prevent unselecting the selected variant
    $('.bonus-product-item .swatches .selected').on('click', function () {
        return false;
    });
};

/**
 * @private
 * @function
 * @description Updates the summary page with the selected bonus product
 */
function updateSummary() {
    var $bonusProductList = $('#bonus-product-list'),
        ulList,
        i,
        len,
        item,
        li,
        remain;
    if (selectedList.length === 0) {
        $bonusProductList.find('li.selected-bonus-item').remove();
    } else {
        ulList = $bonusProductList.find('ul.selected-bonus-items').first();
        for (i = 0, len = selectedList.length; i < len; i++) {
            item = selectedList[i];
            li = selectedItemTemplate(item);
            $(li).appendTo(ulList);
        }
    }

    // get remaining item count
    remain = maxItems - selectedList.length;
    $bonusProductList.find('.bonus-items-available').text(remain);
    if (remain <= 0) {
        $bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
    } else {
        $bonusProductList.find('.select-bonus-item').removeAttr('disabled');
    }
}

function initializeGrid() {
    var $bonusProduct = $('#bonus-product-dialog'),
        $bonusProductList = $('#bonus-product-list'),
        bliData = $bonusProductList.data('line-item-detail'),
        cartItems;
    maxItems = bliData.maxItems;
    bliUUID = bliData.uuid;

    if (bliData.itemCount >= maxItems) {
        $bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
    }

    cartItems = $bonusProductList.find('.selected-bonus-item');
    cartItems.each(function () {
        var ci = $(this),
            product = {
                uuid: ci.data('uuid'),
                pid: ci.data('pid'),
                qty: ci.find('.item-qty').text(),
                name: ci.find('.item-name').html(),
                attributes: {}
            },
            attributes = ci.find('ul.item-attributes li');
        attributes.each(function () {
            var li = $(this);
            product.attributes[li.data('attributeId')] = {
                displayName: li.children('.display-name').html(),
                displayValue: li.children('.display-value').html()
            };
        });
        selectedList.push(product);
    });

    $bonusProductList.on('click', '.bonus-product-item a[href].swatchanchor', function (e) {
        e.preventDefault();
        var url = this.href,
            $this = $(this);
        url = util.appendParamsToUrl(url, {
            'source': 'bonus',
            'format': 'ajax'
        });
        $.ajax({
            url: url,
            success: function (response) {
                $this.closest('.bonus-product-item').empty().html(response);
                hideSwatches();
            }
        });
    })
        .on('change', '.input-text', function () {
            $bonusProductList.find('.select-bonus-item').removeAttr('disabled');
            $(this).closest('.bonus-product-form').find('.quantity-error').text('');
        })
        .on('click', '.select-bonus-item', function (e) {
            e.preventDefault();
            if (selectedList.length >= maxItems) {
                $bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
                $bonusProductList.find('.bonus-items-available').text('0');
                return;
            }

            var form = $(this).closest('.bonus-product-form'),
                detail = $(this).closest('.product-detail'),
                uuid = form.find('input[name="productUUID"]').val(),
                qtyVal = form.find('input[name="Quantity"]').val(),
                qty = (isNaN(qtyVal)) ? 1 : (+qtyVal),
                product,
                optionSelects;

            if (qty > maxItems) {
                $bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
                form.find('.quantity-error').text(Resources.BONUS_PRODUCT_TOOMANY);
                return;
            }

            product = {
                uuid: uuid,
                pid: form.find('input[name="pid"]').val(),
                qty: qty,
                name: detail.find('.product-name').text(),
                attributes: detail.find('.product-variations').data('attributes'),
                options: []
            };

            optionSelects = form.find('.product-option');

            optionSelects.each(function () {
                product.options.push({
                    name: this.name,
                    value: $(this).val(),
                    display: $(this).children(':selected').first().html()
                });
            });
            selectedList.push(product);
            updateSummary();
        })
        .on('click', '.remove-link', function (e) {
            e.preventDefault();
            var container = $(this).closest('.selected-bonus-item'),
                uuid,
                i,
                len;
            if (!container.data('uuid')) {
                return;
            }

            uuid = container.data('uuid');
            len = selectedList.length;
            for (i = 0; i < len; i++) {
                if (selectedList[i].uuid === uuid) {
                    selectedList.splice(i, 1);
                    break;
                }
            }
            updateSummary();
        })
        .on('click', '.add-to-cart-bonus', function (e) {
            e.preventDefault();
            var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: bliUUID}),
                bonusProducts = getBonusProducts();
            if (bonusProducts.bonusproducts[0].product.qty > maxItems) {
                bonusProducts.bonusproducts[0].product.qty = maxItems;
            }
            // make the server call
            /*jslint unparam: true */
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                contentType: 'application/json',
                url: url,
                data: JSON.stringify(bonusProducts)
            })
                .done(function () {
                    // success
                    window.location = Urls.cartShow;
                })
                .fail(function (xhr, textStatus) {
                    // failed
                    if (textStatus === 'parsererror') {
                        /*eslint-disable no-alert */
                        window.alert(Resources.BAD_RESPONSE);
                        /*eslint-enable no-alert */
                    } else {
                        /*eslint-disable no-alert */
                        window.alert(Resources.SERVER_CONNECTION_ERROR);
                        /*eslint-enable no-alert */
                    }
                })
                .always(function () {
                    $bonusProduct.slideUp('slow');
                });
            /*jslint unparam: false */
        });
}

var bonusProductsView = {
    /**
     * @function
     * @description Open the list of bonus products selection dialog
     */
    show: function (url) {
        var $bonusProduct = $('#bonus-product-dialog');
        // create the dialog
        ajax.load({
            url: url,
            target: $bonusProduct,
            callback: function () {
                initializeGrid();
                hideSwatches();
                $bonusProduct.slideDown('fast');
                util.scrollBrowser($bonusProduct.offset().top - 50);
            }
        });
    },
    /**
     * @function
     * @description Open bonus product promo prompt dialog
     */
    loadBonusOption: function () {
        var bonusDiscountContainer = document.querySelector('.bonus-discount-container'),
            uuid = $('.bonus-product-promo').data('lineitemid'),
            url = util.appendParamsToUrl(Urls.getBonusProducts, {
                bonusDiscountLineItemUUID: uuid,
                source: 'bonus'
            });

        if (!bonusDiscountContainer) {
            return;
        }
        this.show(url);

        // get the html from minicart, then trash it
        bonusDiscountContainer.parentNode.removeChild(bonusDiscountContainer.outerHTML);
    }
};

module.exports = bonusProductsView;
