importPackage( dw.system );
// extracts xml content as object
// uses recursive processing for complex content
// looks for given attribute names (found no way to get all attributes with index and value)
function xml2object(request : XML, attributeNames : Array) : Object {
    var result : Object = {};
    for each(var requestData : XML in request.children()){
        var requestName : String = filterNamespace(requestData.name().toString(), requestData.namespace());
        var hasSubElements : Boolean = false;
        // look for attributes if requested
        var attributes = {};
        for each(var name in attributeNames) {
            var value : String = requestData.attribute(name).toString();
            if(value)
                attributes[name] = value;
        }
        var children : XMLList = requestData.children().length();
        var elements : XMLList = requestData.elements().length();
        var decendants : XMLList = requestData.descendants().length();
        var subelements = requestData.children().children().children();
        if( requestData.children().length() != requestData.descendants().length() ) {
        	hasSubElements = true;
        } else {
        	hasSubElements = false;
        }
        
        if(requestData.hasSimpleContent()) {
            requestData = { 'node_value' : requestData.toString() };
        } else {
            requestData = xml2object(requestData, attributeNames);
        }
        if(Object.keys(attributes).length)
            requestData['node_attributes'] = attributes;

        if(requestName in result) {
            if (Array.isArray(result[requestName])) {
                result[requestName].push(requestData);
            } else {
                result[requestName] = new Array(result[requestName], requestData);
            }
        } else {
        	if (!hasSubElements) {
            	if (!empty(requestData) && !empty(requestName))
            		result[requestName] = requestData;
        	} else {
            	if (!empty(requestData) && !empty(requestName))
        			result[requestName] = new Array(requestData);
        	}
        }
    }
    return result;
}

function filterNamespace(name : String, nameSpace : String) {
    return name.replace(nameSpace + '::', '');
}


/*
	Creates a Json from an XML
	It's basically working as the xml2object but doesn't consider Attributes and uses the localName() of the node to improve readability
	If your XML has attributes either work with the xml2object or extend this implementation.
	Using it on small objects shoudn't lead to problems, in terms of memory consumption.
	
*/
function createJsonFromXML( elements : XMLList){
	var properties = {};
	for each(var element in elements){
		if(element.hasSimpleContent()){
			properties[element.localName()] = element[0].toString();
		}
		else{
			var recursiveProps = createJsonFromXML(element.elements());
			if(empty(properties[element.localName()])){
				properties[element.localName()] = [];				
			}
			var tempObj = {};
			for each(var key in Object.keys(recursiveProps)){
				tempObj[key] = recursiveProps[key];
			}
			properties[element.localName()].push(tempObj);
		}
	}
	return properties;
}


