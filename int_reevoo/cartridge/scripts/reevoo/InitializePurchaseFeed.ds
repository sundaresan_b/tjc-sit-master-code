/**
* 	InitilizePurchaseFeed.ds
*
* 	It initializes the purchase feed parameters. It reads the reevoo export data object to
* 	retrieve the last run time of purchase export job. 
*
* 	@output CurrentDateTime : Date The current date time
* 	@output Separator : String The separator to be used to separate the fields
* 	@output OrderList : dw.util.SeekableIterator The order list
* 	@output OrdersCount : Number The total number of orders to export.
*
*/
importPackage( dw.system );
importPackage( dw.object );
importPackage( dw.util );
importPackage( dw.order );

function execute( pdict : PipelineDictionary ) : Number
{
	Logger.getLogger("ReevooExport")
		.info( "InitilizePurchaseFeed: ++++++++++ Start +++++++++" );
	
	var	helperCalendar : Calendar = new Calendar();
	var reevooExportJobData : CustomObject = CustomObjectMgr.getCustomObject("ReevooExportJobData", "1");
	
	if (!reevooExportJobData) {
		var	initCalendar : Calendar = new Calendar();
		initCalendar.roll(Calendar.YEAR, false);
		reevooExportJobData = CustomObjectMgr.createCustomObject("ReevooExportJobData", "1");
		reevooExportJobData.custom.productJobLastExecutionTime = initCalendar.getTime();
		reevooExportJobData.custom.purchaseJobLastExecutionTime = initCalendar.getTime();
	}	
	
	var purchaseJobLastExecutionTime : Date = reevooExportJobData.custom.purchaseJobLastExecutionTime;
	var exportPurchaseFeedByDelta : Boolean = Site.getCurrent().preferences.custom.exportReevooPurchaseFeedByDelta;
	
	var	orders : SeekableIterator = null;
	var queryString : String = "";
	var	sortString : String = "orderNo ASC";
	
	var	helperCalendar : Calendar = new Calendar();
	var currentDateTime = helperCalendar.getTime();
	
	Logger.getLogger("ReevooExport")
		.debug( "InitilizePurchaseFeed: " + 
			"Purchase Job Last Execution Time - " + purchaseJobLastExecutionTime +
			"Export Purchase Feed By Delta - " + exportPurchaseFeedByDelta );
	
	if(exportPurchaseFeedByDelta){
		queryString = "creationDate >= {0} AND creationDate <= {1}";
		orders = OrderMgr.queryOrders(queryString, sortString, purchaseJobLastExecutionTime, currentDateTime);
	} else {
		orders = OrderMgr.queryOrders(queryString, sortString);
	}
	
	Logger.getLogger("ReevooExport")
		.debug( "InitilizePurchaseFeed: The total number of orders searched : " + orders.count);
		
	pdict.CurrentDateTime = currentDateTime;
	pdict.OrderList = orders;
	pdict.OrdersCount = orders.count;
	
	pdict.Separator = ",";
	
	Logger.getLogger("ReevooExport")
		.info( "InitilizePurchaseFeed: ++++++++++ Exit +++++++++" );
		
   	return PIPELET_NEXT;
}
