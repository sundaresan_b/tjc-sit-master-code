/**
* 	UploadFeed.ds
*
* 	It upload the file to the destination FTP. 
* 
* 	The FTP details are retrieved from site preferences.
*
* 	@input FileName : String The file name
* 	@input File : String The file to upload
*
*/
importPackage( dw.system );
importPackage( dw.net );
importPackage( dw.io );

function execute( args : PipelineDictionary ) : Number
{
	Logger.getLogger("ReevooExport")
		.info( "UploadFeed: ++++++++++ Start +++++++++" );
		
	var file	 : File;
	
	var reevooSFTPURL : String = dw.system.Site.getCurrent().preferences.custom.reevooSFTPURL;
	var reevooSFTPPort : String = dw.system.Site.getCurrent().preferences.custom.reevooSFTPPort;
	var reevooSFTPUploadPath : String = dw.system.Site.getCurrent().preferences.custom.reevooSFTPUploadPath;
	var reevooSFTPUsername : String = dw.system.Site.getCurrent().preferences.custom.reevooSFTPUsername;
	var reevooSFTPPassword : String = dw.system.Site.getCurrent().preferences.custom.reevooSFTPPassword;
	var remoteFilePath = reevooSFTPUploadPath + File.SEPARATOR + args.FileName;
	
	if (reevooSFTPURL && reevooSFTPPort && reevooSFTPUploadPath && reevooSFTPUsername && reevooSFTPPassword) {
	
		Logger.getLogger("ReevooExport")
			.debug( "UploadFeed: Reevoo SFTP URL - " + reevooSFTPURL +
									"\nReevoo SFTP Port - " + reevooSFTPPort +
									"\nReevoo SFTP Upload Path - " + reevooSFTPUploadPath );
									
		var sftp : SFTPClient = new SFTPClient(); 
		sftp.setTimeout(30000);
	        
		try {
				
			file = new File(args.File);
			
			if (sftp.connect(reevooSFTPURL, reevooSFTPPort, reevooSFTPUsername, reevooSFTPPassword))		
			{	
				sftp.putBinary(remoteFilePath, file);
				
				sftp.disconnect();
			}
			else
			{
				
				Logger.getLogger("ReevooExport")
					.error( "UploadFeed: Cannot connect to FTP Server");
					
				return PIPELET_ERROR;
			}
		}
		catch( e ) {
			Logger.getLogger("ReevooExport")
				.error( "UploadFeed: Could not connect to Server: {0}",e);
				
			return PIPELET_ERROR;
		}
	}
	else {
		Logger.getLogger("ReevooExport")
			.error( "UploadFeed: Don't upload files due to missing SFTP configuration values.");	
	}
		
		Logger.getLogger("ReevooExport")
			.info( "UploadFeed: ++++++++++ Exit +++++++++" );

   return PIPELET_NEXT;
}